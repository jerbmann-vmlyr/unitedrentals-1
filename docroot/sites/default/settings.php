<?php

$databases = [];
$config_directories = [];
$settings['update_free_access'] = FALSE;
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];
$settings['entity_update_batch_size'] = 50;
$settings['install_profile'] = 'standard';

if (file_exists('/var/www/site-php')) {
  require '/var/www/site-php/unitedrentals/unitedrentals-settings.inc';
}

if (file_exists($app_root . '/' . $site_path . '/environment.settings.php')) {
  include $app_root . '/' . $site_path . '/environment.settings.php';
}

/**
 * Reset sync directory after Acquia include
 */
$config_directories[CONFIG_SYNC_DIRECTORY] = '../config/default';

/**
 * Disable checking entity access in Twig tweak functions.
 */
$settings['twig_tweak_check_access'] = FALSE;

/**
 * Load correct config split based on environment
 */
$is_acquia = isset($_ENV['AH_SITE_ENVIRONMENT']);
$is_prod = ($is_acquia && ($_ENV['AH_SITE_ENVIRONMENT'] === 'prod' || $_ENV['AH_SITE_ENVIRONMENT'] === 'prod2'));

/**
 * Default settings, override as needed.
 *
 * Override like this: $config['simplesamlphp_auth.settings']['my_setting'] = 'value';
 */
// Replaces "simplesamlphp_auth_authsource" and "ur_deployment_environment"
$config['united_rentals']['environment'] = 'local';

// Replaces "ur_central_auth_login" and "ur_central_auth_register"
$config['united_rentals']['login_path'] = 'user/login';
$config['united_rentals']['register_path'] = 'user/register';

// Replaces "ur_central_auth_forgot_password" and "ur_central_auth_change_password"
$config['united_rentals']['forgot_password_path'] = 'https://devlogin.ur.com/home/index/forgotpassword';
$config['united_rentals']['change_password_path'] = 'https://devlogin.ur.com/home/index/forgotpassword';
$config['united_rentals']['sign_in_register_path'] = '/user/login';

// Replaces "ur_urcontrol_address" and "ur_totalcontrol_address"
$config['united_rentals']['urcontrol_path'] = 'https://devurcontrol.ur.com';
$config['united_rentals']['totalcontrol_path'] = 'https://devtotalcontrol.ur.com/requisitions/entry/index';

// Settings for ALL Acquia environments (including prod)
if ($is_acquia) {
  // Replaces "simplesamlphp_auth_authsource" and "ur_deployment_environment"
  $config['united_rentals']['environment'] = $_ENV['AH_SITE_ENVIRONMENT'];
  $config['united_rentals']['sign_in_register_path'] = '/saml_login';

  // Replaces "ur_central_auth_login" and "ur_central_auth_register"
  $config['united_rentals']['login_path'] = 'saml_login';
  $config['united_rentals']['register_path'] = 'saml_login';

  // Replaces "unitedrentals_google_maps_client_id"
  if (isset($_ENV['gme-unitedrentalsinc'])) {
    $config['united_rentals']['google_maps_client_id'] = $_ENV['gme-unitedrentalsinc'];
  }

  /**
   * Enable Acquia specific services.
   */
  $settings['container_yamls'][] = DRUPAL_ROOT . '/sites/default/acquia.services.yml';
  $settings['file_private_path'] = '/mnt/files/' . $_ENV['AH_SITE_GROUP'] . '.' . $_ENV['AH_SITE_ENVIRONMENT'] . '/' . $site_path . '/files-private';
}
else {
  $settings['file_private_path'] = '../private';
}

if ($is_prod) {
  // Replaces "ur_central_auth_forgot_password" and "ur_central_auth_change_password"
  $config['united_rentals']['forgot_password_path'] = 'https://login.ur.com/home/index/forgotpassword';
  $config['united_rentals']['change_password_path'] = 'https://login.ur.com/home/index/forgotpassword';
  $config['united_rentals']['sign_in_register_path'] = '/saml_login';

  // Replaces "ur_urcontrol_address" and "ur_totalcontrol_address"
  $config['united_rentals']['urcontrol_path'] = 'https://urcontrol.ur.com';
  $config['united_rentals']['totalcontrol_path'] = 'https://totalcontrol.ur.com/requisitions/entry/index';
}

$config['system.performance']['fast_404']['exclude_paths'] = '/\/(?:styles)|(?:system\/files)\//';
$config['system.performance']['fast_404']['paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$config['system.performance']['fast_404']['html'] = '<!DOCTYPE html><html><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

$settings['maintenance_theme'] = 'unitedrentals';

// Memcached settings - Loaded from Acquia Cloud.
$settings['memcache']['extension'] = 'Memcached';

if (isset($settings['memcache']['servers'])) {
  // Memcache settings.
  $settings['cache']['default'] = 'cache.backend.memcache';
}

// SimpleSAMLphp configuration.
if (isset($_ENV['AH_SITE_NAME'])) {

  // Force server port to 443 with HTTPS environments when behind a load
  // balancer which is a requirement for SimpleSAML with ADFS when
  // providing a redirect path.
  // @see https://github.com/simplesamlphp/simplesamlphp/issues/450
  if ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] === 'on') {
    $_SERVER['SERVER_PORT'] = 443;
  }

  // Update for behind a load balancer.
  if ($_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
    $_SERVER['HTTPS'] = 'on';
    $_SERVER['SERVER_PORT'] = 443;
  }
}
  
// SAML path.
$real_path = realpath(DRUPAL_ROOT . '/../vendor/simplesamlphp/simplesamlphp');
if (is_dir($real_path)) {
  $settings['simplesamlphp_dir'] = $real_path;
}

$settings['filecache_directory']['default'] = 'public://filecache';

$settings['filecache']['directory']['default'] = 'public://filecache';
