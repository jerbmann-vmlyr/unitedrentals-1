<?php

// Set hash_salt from file.
$settings['hash_salt'] = file_get_contents($app_root . '/../config/salt.txt');

// Set config sync directory after server configuration (since Acquia overwrites
// the directory in their include files.)
// @note: Drupal suggests keeping sync directory at least 1 level below the
// site-specific config directory (IE "/config/www/default/" instead of
// "/config/www/"). This makes sure other site-specific files, like a hash-salt
// file or config-split-specific files, can be associated and read.
$config_directories[CONFIG_SYNC_DIRECTORY] = $app_root . '/../config/' . basename($site_path) . '/default';

// Setup per-environment config sync settings.
$config['config_split.config_split.remote_dev']['status'] = FALSE;
$config['config_split.config_split.remote_prod']['status'] = FALSE;
$config['config_split.config_split.remote_stage']['status'] = FALSE;
$config['config_split.config_split.local']['status'] = FALSE;

// Set Environment indicator default.
$config['environment_indicator.indicator']['bg_color'] = '#0080ff';
$config['environment_indicator.indicator']['fg_color'] = '#e6e6e6';
$config['environment_indicator.indicator']['name'] = 'Local';

// Determine passed-in environment.
$site_environment = 'local';
if (isset($_ENV['AH_SITE_ENVIRONMENT'])) {
  $config['config_split.config_split.acquia']['status'] = TRUE;
  $settings['container_yamls'][] = DRUPAL_ROOT . '/sites/acquia.services.yml';
  $site_environment = $_ENV['AH_SITE_ENVIRONMENT'];
} elseif (isset($_ENV['PANTHEON_ENVIRONMENT'])) {
  $site_environment = $_ENV['PANTHEON_ENVIRONMENT'];
} elseif (isset($_ENV['SITE_ENVIRONMENT'])) {
  $site_environment = $_ENV['SITE_ENVIRONMENT'];
} elseif (isset($_SERVER['SITE_ENVIRONMENT'])) {
  $site_environment = $_SERVER['SITE_ENVIRONMENT'];
}

// Enable appropriate config overrides.
switch ($site_environment) {
  ### Production Environments ###
  case 'prod':
    $config['config_split.config_split.prod']['status'] = TRUE;
    $config['environment_indicator.indicator']['bg_color'] = '#000000';
    $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
    $config['environment_indicator.indicator']['name'] = 'Prod';
    break;
  case 'prod2':
    $config['config_split.config_split.prod2']['status'] = TRUE;
    $config['environment_indicator.indicator']['bg_color'] = '#000000';
    $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
    $config['environment_indicator.indicator']['name'] = 'PreProd';
    break;

  ### QA Environments ###
  case 'qa':
    $config['config_split.config_split.qa']['status'] = TRUE;
    $config['environment_indicator.indicator']['bg_color'] = '#c48900';
    $config['environment_indicator.indicator']['fg_color'] = '#e6e6e6';
    $config['environment_indicator.indicator']['name'] = 'QA1';
    break;
  case 'qa2':
    $config['config_split.config_split.qa2']['status'] = TRUE;
    $config['environment_indicator.indicator']['bg_color'] = '#c48900';
    $config['environment_indicator.indicator']['fg_color'] = '#e6e6e6';
    $config['environment_indicator.indicator']['name'] = 'QA2';
    break;

  ### Test Environments ###
  case 'test':
    $config['config_split.config_split.test']['status'] = TRUE;
    $config['environment_indicator.indicator']['bg_color'] = '#008040';
    $config['environment_indicator.indicator']['fg_color'] = '#e6e6e6';
    $config['environment_indicator.indicator']['name'] = 'Stage';
    break;
  case 'test2':
    $config['config_split.config_split.sandbox']['status'] = TRUE;
    $config['environment_indicator.indicator']['bg_color'] = '#008040';
    $config['environment_indicator.indicator']['fg_color'] = '#e6e6e6';
    $config['environment_indicator.indicator']['name'] = 'Sandbox';
    break;

  ### Dev Environments ###
  case 'dev0':
    $config['config_split.config_split.dev0']['status'] = TRUE;
    $config['environment_indicator.indicator']['bg_color'] = '#ffcc66';
    $config['environment_indicator.indicator']['fg_color'] = '#333333';
    $config['environment_indicator.indicator']['name'] = 'Dev0';
    break;
  case 'dev':
    $config['config_split.config_split.dev']['status'] = TRUE;
    $config['environment_indicator.indicator']['bg_color'] = '#ffcc66';
    $config['environment_indicator.indicator']['fg_color'] = '#333333';
    $config['environment_indicator.indicator']['name'] = 'Dev1';
    break;
  case 'dev2':
    $config['config_split.config_split.dev2']['status'] = TRUE;
    $config['environment_indicator.indicator']['bg_color'] = '#ffcc66';
    $config['environment_indicator.indicator']['fg_color'] = '#333333';
    $config['environment_indicator.indicator']['name'] = 'Dev2';
    break;
  case 'dev3':
    $config['config_split.config_split.dev3']['status'] = TRUE;
    $config['environment_indicator.indicator']['bg_color'] = '#ffcc66';
    $config['environment_indicator.indicator']['fg_color'] = '#333333';
    $config['environment_indicator.indicator']['name'] = 'Dev3';
    break;

  case 'local':
    $config['config_split.config_split.local']['status'] = TRUE;
    break;
}

// Enable docksal settings overrides.
if (file_exists($app_root . '/' . $site_path . '/settings.docksal.php')) {
  include $app_root . '/' . $site_path . '/settings.docksal.php';
}

// Enable local settings overrides.
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}
