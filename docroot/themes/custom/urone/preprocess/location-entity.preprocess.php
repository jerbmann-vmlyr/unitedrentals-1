<?php

/**
 * @file
 * Preprocess functions related to location_entity entities.
 */

use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\views\Views;
use Drupal\locations\Plugin\views\field\DistanceCalculation;
use Drupal\taxonomy\Entity\Term;

/**
 * Implements hook_preprocess_location_entity().
 */
function urone_preprocess_location_entity(array &$variables) {
  $branchTypes = [
    'AR' => 'Aerial Equipment',
    'FS' => 'Fluid Solutions',
    'GR' => 'General Equipment & Tools',
    'IR' => 'Industrial',
    'PR' => 'Power & HVAC',
    'SS' => 'Reliable Onsite Services',
    'TR' => 'Trench Safety',
  ];

  $variables['businessTypes'] = $branchTypes;
  /** @var \Drupal\locations\Entity\LocationEntityInterface $location_entity */
  $location_entity = $variables['elements']['#location_entity'];
  $bundle = $location_entity->bundle();
  $variables['lat'] = $location_entity->get('field_geolocation')->getValue()[0]['lat'];
  $variables['lng'] = $location_entity->get('field_geolocation')->getValue()[0]['lng'];
  $branch_abbr = \Drupal::request()->query->get('type');
  $term = FALSE;
  if ($branch_abbr) {
    $terms = Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'location_branches', 'field_branch_code' => $branch_abbr]);
    $term = reset($terms);
  }

  $args = [];
  if ($bundle === 'state_province') {
    $address = $location_entity->get('field_address')->getValue();
    if (!empty($address)) {
      $state = $address[0]['administrative_area'];
      $args = $term ? [$state, $term->id()] : [$state];
    }

    $view = Views::getView('location_listing');
    if (is_object($view)) {
      $view->setArguments($args);
      $view->setDisplay('state_branch_listing');
      $view->preExecute();
      $view->execute();
      $renderable = $view->buildRenderable('state_branch_listing', $args);
      $variables['branch_listing'] = _urone_sort_by_distance($renderable);
    }
  }
  elseif ($bundle === 'city') {
    $args = $term ? [$term->id()] : [];

    $view = Views::getView('location_listing');
    if (is_object($view)) {
      $view->setArguments($args);
      $view->setDisplay('branch_listing');
      $view->preExecute();
      $view->execute();
      $renderable = $view->buildRenderable('branch_listing', $args);
      $variables['branch_listing'] = _urone_sort_by_distance($renderable);
    }
  }
}

/**
 * Implements hook_preprocess_location_entity().
 */
function urone_preprocess_location_entity__branch__full(array &$variables) {
  $location_entity = $variables['elements']['#location_entity'];
  $tid = $location_entity->get('field_primary_business_type')->getValue()[0]['target_id'];
  if ($tid) {
    $variables['rentOnlineLinkShow'] = TRUE;
    $term = Term::load($tid);
    $link = $term->get('field_branch_rent_online_link')->getValue();
    if (!empty($link)) {
      $variables['rentOnlineLink'] = URL::fromUri($link[0]['uri'])->toString();
    }
    else {
      $variables['rentOnlineLinkShow'] = FALSE;
    }
  }
  else {
    $variables['rentOnlineLinkShow'] = FALSE;
  }
}

/**
 * Implements hook_preprocess_location_entity().
 */
function urone_preprocess_location_entity__branch__listing(array &$variables) {
  $location = $variables['elements']['#location_entity'];
  $variables['path_alias'] = Drupal::service('path.alias_manager')->getAliasByPath('/location_entity/' . $location->id());
  if ($variables['branchType'] !== 'GR') {
    $variables['specialty'] = TRUE;
  }

  $current_location = \Drupal::routeMatch()->getParameter('location_entity');
  if (!$current_location) {
    $route = $_SERVER['HTTP_REFERER'];
    $parts = explode('/', str_replace('https://', '', $route));
    unset($parts[0]);
    if ($parts[1] === 'locations') {
      $path = '';
      foreach ($parts as $part) {
        $pieces = explode('?', $part);
        $path .= '/' . $pieces[0];
      }

      $alias = \Drupal::service('path.alias_manager')->getPathByAlias($path);
      $params = Url::fromUri("internal:" . $alias)->getRouteParameters();
      $entity_type = key($params);
      $current_location = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);
    }
  }

  if ($current_location) {
    $lat1 = $current_location->get('field_geolocation')->getValue()[0]['lat'];
    $lng1 = $current_location->get('field_geolocation')->getValue()[0]['lng'];
    $lat2 = $location->get('field_geolocation')->getValue()[0]['lat'];
    $lng2 = $location->get('field_geolocation')->getValue()[0]['lng'];
    if ($location->get('field_address')->country_code === 'US') {
      $unit = 'M';
      $unit_text = ' mi';
    }
    else {
      $unit = 'K';
      $unit_text = ' km';
    }
  }

  $variables['distance'] = DistanceCalculation::distance($lat1, $lng1, $lat2, $lng2, $unit) . $unit_text;
}

/**
 * Sort renderable array based on distance
 */
function _urone_sort_by_distance($renderable) {
  $results = [];
  $current_location = \Drupal::routeMatch()->getParameter('location_entity');
  $unit = 'M';
  $i = 0;

  foreach ($renderable['#view']->result as $value) {
    $location = $value->_entity;
    $lat1 = $current_location->get('field_geolocation')->getValue()[0]['lat'];
    $lng1 = $current_location->get('field_geolocation')->getValue()[0]['lng'];
    $lat2 = $location->get('field_geolocation')->getValue()[0]['lat'];
    $lng2 = $location->get('field_geolocation')->getValue()[0]['lng'];
    $results[$i] = DistanceCalculation::distance($lat1, $lng1, $lat2, $lng2, $unit);
    $i++;
  }

  asort($results);
  $sorted = [];
  foreach ($results as $key => $value) {
    $sorted[] = $renderable['#view']->result[$key];
  }

  $renderable['#view']->result = $sorted;

  return $renderable;
}
