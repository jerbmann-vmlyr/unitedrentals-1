<?php

/**
 * @file
 * Preprocess functions related to block entities.
 *
 * Index:
 *
 * @see urone_preprocess_block()
 */

use Drupal\Component\Utility\Html;
use Drupal\node\NodeInterface;
use Drupal\block\Entity\Block;

/**
 * @see \urone_theme_suggestions_block_alter()
 * @see \urone_preprocess_block()
 * @see \urone_preprocess_block__header_navigation()
 * @see \urone_preprocess_block__page_title_block()
 * @see \urone_preprocess_block__user_account_menu()
 * @see \urone_preprocess_block__secondary_nav_manage()
 * @see \urone_preprocess_block__secondary_nav_project_uptime()
 */

/**
 * Implements hook_theme_suggestions_HOOK_alter() for form templates.
 *
 * @param array $suggestions
 * @param array $variables
 */
function urone_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  // Block suggestions for custom block bundles.
  if (isset($variables['elements']['content']['#block_content'])) {
    /** @var \Drupal\block\Entity\Block $block */
    $block = $variables['elements']['content']['#block_content'];
    array_splice($suggestions, 1, 0, 'block__bundle__' . $block->bundle());
  }
}

/**
 * Implements hook_preprocess_block().
 */
function urone_preprocess_block(array &$variables) {

  if (isset($variables['elements']['#id'])) {
    $region = Block::load($variables['elements']['#id'])->getRegion();
    $variables['content']['#attributes']['region'] = $region;
  }

  if (isset($variables['elements']['content']['#block_content'])) {
    /** @var \Drupal\block\Entity\Block $block */
    $block = $variables['elements']['content']['#block_content'];
    $block_css = Html::cleanCssIdentifier($block->bundle());
    $block_class = 'block-' . $block_css;

    $variables['attributes']['class'][] = "{$block_class}";
    $variables['title_attributes']['class'] = "block__title {$block_class}__header";
    $variables['content_attributes']['class'] = "block__content {$block_class}__content";
    unset($variables['content']['field_media']['#theme']);

    if (isset($variables['content']['field_nested_navigation_block'])) {
      if (isset($variables['content']['field_display_parents_only'])) {
        unset($variables['content']['field_display_parents_only']);
      }
      $menu_link = [];
      $node_id = \Drupal::routeMatch()->getRawParameter('node');
      if ($node_id) {
        $menu_link_manager = \Drupal::service('plugin.manager.menu.link');
        $menu_link = $menu_link_manager->loadLinksByRoute('entity.node.canonical', ['node' => $node_id]);
      }

    }
    if (!empty($menu_link)) {
      $menu_link_child = end($menu_link);
      $child_title = $menu_link_manager->createInstance($menu_link_child->getPluginId())->getTitle();
      if (count($menu_link) === 2) {
        $menu_link = reset($menu_link);
        $title = $menu_link_manager->createInstance($menu_link->getPluginId())->getTitle();
      }
      elseif ($menu_link_child->getParent()) {
        $parents = $menu_link_manager->getParentIds($menu_link_child->getParent());
        $parent = reset($parents);
        $title = $menu_link_manager->createInstance($parent)->getTitle();
      }

      $variables['menu_title'] = isset($title) ? $title . ' - ' . $child_title : $child_title;
    }
    elseif (isset($variables['label'])) {
      $variables['menu_title'] = $variables['label'];
    }
    else {
      $variables['menu_title'] = 'Menu';
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * Provide region based menu suggestions.
 */
function urone_theme_suggestions_menu_alter(&$suggestions, array $variables) {
  if (isset($variables['attributes']['region'])) {
    $suggestions[] = 'menu__' . $variables['menu_name'] . '__' . $variables['attributes']['region'];
  }
}

/**
 * Implements hook_preprocess_block__HOOK() for header_navigation.
 */
function urone_preprocess_block__header_navigation(array &$variables) {
  $plugin_css = Html::cleanCssIdentifier($variables['plugin_id']);
  $provider_css = Html::cleanCssIdentifier($variables['configuration']['provider']);
  $block_class = 'block-' . $plugin_css;

  // Figure out title ID and set aria-related attributes.
  $title_id = $variables['attributes']['id'] . '-header';
  $variables['attributes']['role'] = 'navigation';
  $variables['attributes']['aria-labelledby'] = $title_id;
  $variables['attributes']['hidden'] = '';
  $variables['attributes']['aria-hidden'] = 'true';
  $variables['title_attributes']['id'] = $title_id;
  $variables['content_attributes']['role'] = 'dialog';
  $variables['content_attributes']['aria-labelledby'] = $title_id;

  // Add wrapper class.
  $variables['wrapper_attributes']['class'][] = "{$block_class}__wrapper";

  // Add button classes and attributes.
  $variables['button_attributes']['class'][] = "{$block_class}__button";
  $variables['button_attributes']['title'] = t('Open navigation');
  $variables['button_attributes']['aria-label'] = t('Open navigation');
  $variables['button_attributes']['data-aria-label-open'] = t('Open navigation');
  $variables['button_attributes']['data-aria-label-close'] = t('Close navigation');

  // Track to convert attributes.
  $variables['#attribute_variables'][] = 'button_attributes';
  $variables['#attribute_variables'][] = 'wrapper_attributes';
}

/**
 * Implements hook_preprocess_block__HOOK() for page_title_block.
 */
function urone_preprocess_block__page_title_block(array &$variables) {
  // Move title from content to title.
  $variables['title'] = $variables['content'];
  $variables['content'] = [];

  // Load the node entity from current route.
  $node = \Drupal::routeMatch()->getParameter('node');
  if (is_null($node)) {
    $node = \Drupal::routeMatch()->getParameter('node_preview');
  }
  if ($node instanceof NodeInterface) {
    // Make specific adjustments based on node type.
    switch ($node->bundle()) {
      case 'item':
        if (!$node->get('field_item_cat_class_code')->isEmpty()) {
          $variables['super_title'] = $node->get('field_item_cat_class_code')->view('full');
        }
        break;

      default;
        break;
    }
  }
}

/**
 * Implements hook_preprocess_block__HOOK() for user_account_menu block.
 */
function urone_preprocess_block__user_account_menu(array &$variables) {
  // Add user context to block caching.
  $variables['#cache']['contexts'][] = 'user';
}

/**
 * Implements hook_preprocess_block__HOOK() for user_account_menu block.
 */
function urone_preprocess_block__useraccountmenu_2(array &$variables) {
  // Add user context to block caching.
  $variables['#cache']['contexts'][] = 'user';
}

/**
 * Implements hook_preprocess_block__HOOK() for secondary_nav_project_uptime block.
 */
function urone_preprocess_block__secondary_nav_project_uptime(array &$variables) {
  $variables['attributes']['class'][] = 'secondary-nav';
  $variables['#attached']['library'][] = 'urone/library--secondary-nav';
}

/**
 * Implements hook_preprocess_block__HOOK() for secondary_nav_manage block.
 */
function urone_preprocess_block__secondary_nav_manage(array &$variables) {
  $variables['attributes']['class'][] = 'secondary-nav';
  $variables['#attached']['library'][] = 'urone/library--secondary-nav';
}
