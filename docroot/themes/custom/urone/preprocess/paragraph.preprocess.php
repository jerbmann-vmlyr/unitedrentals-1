<?php

/**
 * @file
 * Preprocess functions related to paragraph entities.
 *
 * Index:
 *
 * @see urone_preprocess_paragraph()
 * @see urone_preprocess_paragraph__article_carousel__full()
 * @see urone_preprocess_paragraph__accordion_group__full()
 * @see urone_preprocess_paragraph__accordion_item__full()
 * @see urone_preprocess_paragraph__card__full()
 * @see urone_preprocess_paragraph__card__item_card()
 * @see urone_preprocess_paragraph__card__sub_category_card()
 * @see urone_preprocess_paragraph__cards_wrapper__full()
 * @see urone_preprocess_paragraph__compartment__full()
 * @see urone_preprocess_paragraph__content__full()
 * @see urone_preprocess_paragraph__cta__full()
 * @see urone_preprocess_paragraph__dynamic_cta__full()
 * @see urone_preprocess_paragraph__hero__full()
 * @see urone_preprocess_paragraph__hero_slide__full()
 * @see urone_preprocess_paragraph__hero_with_icon__full()
 * @see urone_preprocess_paragraph__lead_generation_bar__full()
 * @see urone_preprocess_paragraph__list_links__full()
 * @see urone_preprocess_paragraph__listing_item__full()
 * @see urone_preprocess_paragraph__listing_wrapper__full()
 * @see urone_preprocess_paragraph__embed_iframe__full()
 * @see urone_preprocess_paragraph__featured_content__full()
 * @see urone_preprocess_paragraph__featured_media__full()
 * @see urone_preprocess_paragraph__featured_term__full()
 * @see urone_preprocess_paragraph__promo_hero__full()
 * @see urone_preprocess_paragraph__pullquote__full()
 * @see urone_preprocess_paragraph__section__full()
 * @see urone_preprocess_paragraph__slider__full()
 * @see urone_preprocess_paragraph__tabs__full()
 * @see urone_preprocess_paragraph__tabs_tab__full()
 * @see urone_preprocess_paragraph__use_case_card__full()
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Drupal\Core\Render\Element;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\file\Entity\File;

/**
 * Implements hook_preprocess_paragraph().
 */
function urone_preprocess_paragraph(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $bundle = $paragraph->bundle();
  $view_mode = $variables['view_mode'];
  $base_class = $variables['component_base_class'];

  // Initialize settings (this approach allows less IF/ELSE nesting).
  $setting_background_image_unwrap = FALSE;
  $setting_class_custom = FALSE;
  $setting_component_unwrap = FALSE;
  $setting_title_move = FALSE;

  // Toggle settings based on view-mode.
  switch ($variables['view_mode']) {
    case 'full':
      $setting_background_image_unwrap = TRUE;
      $setting_class_custom = TRUE;
      $setting_component_unwrap = TRUE;
      $setting_title_move = TRUE;
      break;
  }

  // Unset background-image field theme wrapper (to not print an empty div).
  if ($paragraph->hasField('field_background_image') && !$paragraph->get('field_background_image')->isEmpty()) {
    $variables['attributes']['class'][] = "{$base_class}--bg-image";
    unset($variables['content']['field_background_image']['#theme']);
  }

  // Add custom classes to the component.
  if ($setting_class_custom && $paragraph->hasField('field_class_custom') && !$paragraph->get('field_class_custom')->isEmpty()) {
    $field_class_custom = preg_replace('/[^a-zA-Z0-9\-_ ]/', '', $paragraph->get('field_class_custom')->value);
    $variables['attributes']['class'] = array_merge($variables['attributes']['class'], explode(' ', $field_class_custom));
  }

  // Remove component field-wrappers.
  if ($setting_component_unwrap && array_key_exists('field_paragraphs', $variables['content'])) {
    unset($variables['content']['field_paragraphs']['#theme']);
  }

  // Set title variable from fields.
  if ($setting_title_move) {
    foreach (['field_title_link', 'field_title'] as $title_fieldname) {
      if (array_key_exists($title_fieldname, $variables['content'])) {

        $variables['title'] = $variables['content'][$title_fieldname];

        unset($variables['title']['#theme']);
        unset($variables['content'][$title_fieldname]);
        break;
      }
    }
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for article carousel group,
 * full.
 */
function urone_preprocess_paragraph__article_carousel__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];

  // Display view of articles by pillar.
  if ($paragraph->hasField('field_pillars')) {
    $tid = $paragraph->get('field_pillars')->target_id;

    // Load children terms and add them as well.
    $children = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadChildren($tid, 'project_uptime_pillars');
    if (!empty($children)) {
      $tid .= '+' . implode('+', array_keys($children));
    }

    // Now add the carousel.
    $variables['carousel'] = views_embed_view('project_uptime', 'articles_by_category_block', $tid);
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for accordion group,
 * full.
 */
function urone_preprocess_paragraph__accordion_group__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Hide tab heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for accordion_item,
 * full.
 */
function urone_preprocess_paragraph__accordion_item__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Check if this accordion item should be open by default.
  $is_open = (!$paragraph->get('field_enabled')->isEmpty() && $paragraph->get('field_enabled')->value === '1');
  if ($is_open) {
    $variables['attributes']['class'][] = "{$base_class}--open";
  }

  /**
   * Based on parent entity type, remove title.
   * @see urone_preprocess_paragraph__listing_item__full()
   */
  $parent = $paragraph->getParentEntity();
  if ($parent->bundle() == 'listing_item') {
    unset($variables['content']['field_title'], $variables['title']);
  }

  if ($paragraph->get('field_title')->isEmpty()) {
    unset($variables['content']['field_title']);
  }
}

/**
 * Implements hook_preprocess_paragraph__VIEW_MODE() for compartment, full.
 */
function urone_preprocess_paragraph__compartment__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  $variables['#attribute_variables'][] = 'header_attributes';
  $variables['header_attributes']['class'][] = "{$base_class}__header";

  unset($variables['content']['field_paragraphs']['#theme']);

  // Move super title field to new variable and define new attributes for subtitle.
  if (isset($variables['content']['field_super_title']) && !empty($variables['content']['field_super_title'])) {
    $variables['#attribute_variables'][] = 'supertitle_attributes';
    $variables['supertitle_attributes']['class'][] = "{$base_class}__super-title";
    $variables['super_title'] = $variables['content']['field_super_title'];
    unset($variables['content']['field_super_title'], $variables['super_title']['#theme']);
  }

  // Move sub title field to new variable and define new attributes for subtitle.
  if (isset($variables['content']['field_subtitle']) && !empty($variables['content']['field_subtitle'])) {
    $variables['#attribute_variables'][] = 'subtitle_attributes';
    $variables['subtitle_attributes']['class'][] = "{$base_class}__sub-title";
    $variables['sub_title'] = $variables['content']['field_subtitle'];
    unset($variables['content']['field_subtitle'], $variables['sub_title']['#theme']);
  }

  // Move cta link to footer and add class.
  if (array_key_exists('field_cta', $variables['content']) && !empty($variables['content']['field_cta'])) {
    $variables['footer']['field_cta'] = $variables['content']['field_cta'];
    $variables['footer']['field_cta'][0]['#options']['attributes']['class'] = "{$base_class}__cta button";
    unset($variables['content']['field_cta'], $variables['footer']['field_cta']['#theme']);
  }

  $variables['#attached']['library'][] = 'urone/paragraph--full--compartment--layout-slider';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for content, full.
 */
function urone_preprocess_paragraph__content__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Remove the field wrappers around the copy.
  if (array_key_exists('field_copy', $variables['content']) && !empty($variables['content']['field_copy'])) {
    unset($variables['content']['field_copy']['#theme']);
  }

  // Move cta link to footer and add class.
  if (array_key_exists('field_cta_reference', $variables['content']) && !empty($variables['content']['field_cta_reference'])) {
    $variables['footer']['field_cta_reference'] = $variables['content']['field_cta_reference'];
    $variables['footer']['field_cta_reference'][0]['#options']['attributes']['class'] = "{$base_class}__footer-cta";
    unset($variables['content']['field_cta_reference'], $variables['footer']['field_cta_reference']['#theme']);
  }

  $variables['media_id'] = $paragraph->get('field_media_item')->target_id;

  // Move video field to new variable.
  if (isset($variables['content']['field_video']) && !empty($variables['content']['field_video'])) {
    $variables['video'] = $variables['content']['field_video'];
    $variables['video']['#attributes']['class'][] = "{$base_class}__video";
    unset($variables['content']['field_video'], $variables['video']['field_video']['#theme']);
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for cta, full.
 */
function urone_preprocess_paragraph__cta__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Move link to footer and add class.
  $variables['footer']['field_link'] = $variables['content']['field_link'];
  $variables['footer']['field_link'][0]['#options']['attributes']['class'] = "{$base_class}__link";
  unset($variables['content']['field_link'], $variables['footer']['field_link']['#theme']);
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for embed_iframe,
 * full.
 */
function urone_preprocess_paragraph__embed_iframe__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Track that iframe_attributes should get converted.
  $variables['#attribute_variables'][] = 'iframe_attributes';

  // Clean src input and compile style tag.
  $src = $paragraph->get('field_link')->isEmpty() ?: $paragraph->get('field_link')->uri;
  $style_tag = '';
  $height = '600px';
  if (!$paragraph->get('field_style_height')->isEmpty()) {
    $height = Html::escape($paragraph->get('field_style_height')->value);
    $style_tag .= "height: {$height}; ";
  }
  $width = '100%';
  if (!$paragraph->get('field_style_width')->isEmpty()) {
    $width = Html::escape($paragraph->get('field_style_width')->value);
    $style_tag .= "height: {$height}; ";
  }

  // Set attributes.
  $variables['iframe_attributes']['class'][] = "{$base_class}__iframe";
  $variables['iframe_attributes']['height'] = $height;
  $variables['iframe_attributes']['src'] = $src;
  $variables['iframe_attributes']['style'] = $style_tag;
  $variables['iframe_attributes']['width'] = $width;
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for featured_content, full.
 */
function urone_preprocess_paragraph__featured_content__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Determine changes based on selected layout.
  $layout = ($paragraph->get('field_layout')->isEmpty()) ? NULL : $paragraph->get('field_layout')->target_id;
  switch ($layout) {
    case 'featured_content_layout__grid':
      $variables['#attached']['library'][] = 'urone/paragraph--full--featured-content--layout-grid';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = 'teaser';
      $field_settings['label'] = 'hidden';
      break;

    case 'featured_content_layout__slider_3col':
      $variables['#attached']['library'][] = 'urone/paragraph--full--featured--content--layout-slider-3col';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = 'card';
      $field_settings['label'] = 'hidden';
      break;

    case 'featured_content_layout__slider_4col':
      $variables['#attached']['library'][] = 'urone/paragraph--full--featured--content--layout-slider-4col';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = 'card';
      $field_settings['label'] = 'hidden';
      break;
  }

  // Replace field with new settings.
  $variables['content']['field_related_content'] = $paragraph->get('field_related_content')->view($field_settings);

  // Convert field_related_content to unordered list.
  $variables['list']['#attributes']['class'][] = "{$base_class}__list";
  $variables['list']['#wrapper_attributes'] = [];
  $variables['list']['#items'] = [];
  $variables['list']['#theme'] = 'item_list';

  // Hide list content heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';

  // Run through field items'.
  foreach (Element::children($variables['content']['field_related_content']) as $delta) {
    // Add class to list-item.
    $variables['content']['field_related_content'][$delta]['#wrapper_attributes']['class'][] = "{$base_class}__list-item";
    // Add field item to list item.
    $variables['list']['#items'][] = $variables['content']['field_related_content'][$delta];
  }
  // Remove field render array.
  unset($variables['content']['field_related_content']);
}

/**
 * Implements hook_preprocess_paragraph__VIEW_MODE() for media, full.
 */
function urone_preprocess_paragraph__featured_media__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Determine changes based on selected layout.
  $layout = ($paragraph->get('field_layout')->isEmpty()) ? NULL : $paragraph->get('field_layout')->target_id;
  switch ($layout) {
    case 'featured_media__layout_divider':
      $variables['#attached']['library'][] = 'urone/paragraph--full--featured-media--layout-divider';
      break;

    case 'featured_media__layout_full':
      $variables['#attached']['library'][] = 'urone/paragraph--full--featured-media--layout-full';
      break;
  }

  // Convert field_media to unordered list.
  $variables['list']['#attributes']['class'][] = "{$base_class}__list";
  $variables['list']['#wrapper_attributes'] = [];
  $variables['list']['#items'] = [];
  $variables['list']['#theme'] = 'item_list';

  // Run through field items'.
  foreach (Element::children($variables['content']['field_media_item']) as $delta) {
    // Add class to list-item.
    $variables['content']['field_media_item'][$delta]['#wrapper_attributes']['class'][] = "{$base_class}__list-item";
    // Add field item to list item.
    $variables['list']['#items'][] = $variables['content']['field_media_item'][$delta];
  }

  // Remove field render array.
  unset($variables['content']['field_media_item']);
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for featured_term, full.
 */
function urone_preprocess_paragraph__featured_term__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Determine view mode.
  $view_mode = ($paragraph->get('field_style')->isEmpty()) ? 'default' : str_replace('term_view_mode__', '', $paragraph->get('field_style')->target_id);

  // Determine changes based on selected layout.
  $layout = ($paragraph->get('field_layout')->isEmpty()) ? NULL : $paragraph->get('field_layout')->target_id;
  switch ($layout) {
    case 'featured_content_layout__grid':
      $variables['#attached']['library'][] = 'urone/paragraph--full--featured-term--layout-grid';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = $view_mode;
      $field_settings['label'] = 'hidden';
      break;

    case 'featured_content_layout__slider_3col':
      $variables['#attached']['library'][] = 'urone/paragraph--full--featured-term--layout-slider-3col';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = $view_mode;
      $field_settings['label'] = 'hidden';
      break;

    case 'featured_content_layout__slider_4col':
      $variables['#attached']['library'][] = 'urone/paragraph--full--featured-term--layout-slider-4col';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = $view_mode;
      $field_settings['label'] = 'hidden';
      break;

    case 'featured_content_layout__list':
    default:
      $variables['#attached']['library'][] = 'urone/paragraph--full--featured-term--layout-list';
      $field_settings['type'] = 'entity_reference_label';
      $field_settings['settings']['link'] = TRUE;
      $field_settings['label'] = 'hidden';
  }

  // Replace field with new settings.
  $variables['content']['field_term'] = $paragraph->get('field_term')->view($field_settings);

  // Convert field_related_content to unordered list.
  $variables['list']['#attributes']['class'][] = "{$base_class}__list";
  $variables['list']['#wrapper_attributes'] = [];
  $variables['list']['#items'] = [];
  $variables['list']['#theme'] = 'item_list';

  // Hide list content heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';

  // Run through field items'.
  foreach (Element::children($variables['content']['field_term']) as $delta) {
    // Add class to list-item.
    $variables['content']['field_term'][$delta]['#wrapper_attributes']['class'][] = "{$base_class}__list-item";
    // Add field item to list item.
    $variables['list']['#items'][] = $variables['content']['field_term'][$delta];
  }
  // Remove field render array.
  unset($variables['content']['field_term']);
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for hero, full.
 */
function urone_preprocess_paragraph__hero__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Track whether the hero has slides.
  $variables['has_slides'] = !$paragraph->get('field_paragraphs')->isEmpty();

  // Determine if we display global search in hero.
  $display_search = (bool) $paragraph->get('field_search_takeover')->value;
  if ($display_search) {
    $variables['display_search'] = TRUE;
    $variables['#attribute_variables'][] = 'search_attributes';
    $variables['search_attributes']['class'][] = "{$base_class}--search";
    $variables['search_placeholder'] = \Drupal::config('ur_admin.settings')->get('global_search_placeholder');
  }

  // Add class if hero has lead generation bar,.
  if (!$paragraph->get('field_lead_generation_bar')->isEmpty()) {
    $variables['lead_generation_bar'] = $variables['content']['field_lead_generation_bar'];
    unset($variables['content']['field_lead_generation_bar']);
    $variables['attributes']['class'][] = "{$base_class}--lead-generation-bar";
    // Define new attributes for lead generation bar.
    $variables['#attribute_variables'][] = 'lead_attributes';
    $variables['lead_attributes']['class'][] = "{$base_class}--lead-generation-bar__wrapper";
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for hero_slide,
 * full.
 */
function urone_preprocess_paragraph__hero_slide__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Initialize variables.
  $variables['inner_attributes']['class'][] = "{$base_class}__inner";

  // Move media field to new variable.
  $variables['media'] = $variables['content']['field_media_background'];
  unset($variables['media']['#theme']);
  unset($variables['content']['field_media_background']);

  // Remove field-copy theme wrapper.
  unset($variables['content']['field_copy']['#theme']);

  // Move supertitle to header.
  if (array_key_exists('field_super_title', $variables['content']) && Element::children($variables['content']['field_super_title'])) {
    $variables['super_title'] = $variables['content']['field_super_title'];
    unset($variables['content']['field_super_title'], $variables['super_title']['#theme']);
  }

  // Move cta link to footer and add class.
  if (array_key_exists('field_link', $variables['content']) && !empty($variables['content']['field_link'])) {
    $variables['footer']['field_link'] = $variables['content']['field_link'];
    $variables['footer']['field_link'][0]['#options']['attributes']['class'] = "{$base_class}__cta";
    unset($variables['content']['field_link'], $variables['footer']['field_link']['#theme']);
  }

  // Determine changes based on selected style.
  $style = ($paragraph->get('field_style')->isEmpty()) ? NULL : $paragraph->get('field_style')->target_id;
  switch ($style) {
    case 'hero_slide__style_home':
      $variables['#attached']['library'][] = 'urone/paragraph--full--hero-slide--style-home';
      break;

    case 'hero_slide__style_primary':
      $variables['#attached']['library'][] = 'urone/paragraph--full--hero-slide--style-primary';
      break;

    case 'hero_slide__style_secondary':
      $variables['#attached']['library'][] = 'urone/paragraph--full--hero-slide--style-secondary';
      break;
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for lead_generation_bar, full.
 */
function urone_preprocess_paragraph__lead_generation_bar__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  $variables['subtitle'] = $variables['content']['field_subtitle'];
  unset($variables['subtitle']['#theme']);
  unset($variables['content']['field_subtitle']);

  // Define new attributes for subtitle.
  $variables['#attribute_variables'][] = 'subtitle_attributes';
  $variables['subtitle_attributes']['class'][] = "{$base_class}__subtitle";

  // Define new attributes for text wrapper div.
  $variables['#attribute_variables'][] = 'text_attributes';
  $variables['text_attributes']['class'][] = "{$base_class}__text";

  unset($variables['content']['field_copy']['#theme']);

  // Move media field to new variable and add class.
  if (isset($variables['content']['field_media_background']) && !empty($variables['content']['field_media_background'])) {
    $variables['media'] = $variables['content']['field_media_background'];
    $variables['media']['#attributes']['class'][] = "{$base_class}__media";
    unset($variables['content']['field_media_background'], $variables['media']['#theme']);
  }

  // Move cta link to footer and add class.
  if (array_key_exists('field_link', $variables['content']) && !empty($variables['content']['field_link'])) {
    $variables['footer']['field_link'] = $variables['content']['field_link'];
    $variables['footer']['field_link'][0]['#options']['attributes']['class'] = "{$base_class}__cta";
    unset($variables['content']['field_link'], $variables['footer']['field_link']['#theme']);
  }

  // Move phone link to footer and add class.
  if (array_key_exists('field_phone', $variables['content']) && !empty($variables['content']['field_phone'])) {
    $variables['footer']['field_phone'] = $variables['content']['field_phone'];
    $variables['footer']['field_phone'][0]['#options']['attributes']['class'] = "{$base_class}__phone";
    unset($variables['content']['field_phone'], $variables['footer']['field_phone']['#theme']);
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for list_links, full.
 */
function urone_preprocess_paragraph__list_links__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Convert field_links to unordered list.
  $variables['list']['#attributes']['class'][] = "{$base_class}__list";
  $variables['list']['#wrapper_attributes'] = [];
  $variables['list']['#items'] = [];
  $variables['list']['#theme'] = 'item_list';
  // Run through field items'.
  foreach (Element::children($variables['content']['field_links']) as $delta) {
    // Add class to list-item.
    $variables['content']['field_links'][$delta]['#wrapper_attributes']['class'][] = "{$base_class}__list-item";
    // Add field item to list item.
    $variables['list']['#items'][] = $variables['content']['field_links'][$delta];
  }
  // Remove field render array.
  unset($variables['content']['field_links']);

  // Add component library.
  $variables['#attached']['library'][] = 'urone/paragraph--full--list-links';
}

/**
 * Implements hook_preprocess_paragraph__VIEW_MODE() for pullquote, full.
 */
function urone_preprocess_paragraph__pullquote__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Remove wrapper from field_content_plain.
  unset($variables['content']['field_content_plain']['#theme']);

  // Move attribution to footer.
  if (array_key_exists('field_attribution', $variables['content']) && !empty($variables['content']['field_attribution'])) {
    $variables['footer']['field_attribution'] = $variables['content']['field_attribution'];
    unset($variables['content']['field_attribution']);
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for slider, full.
 */
function urone_preprocess_paragraph__slider__full(array &$variables) {
  // Nothing to see here.
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for tabs, full.
 */
function urone_preprocess_paragraph__tabs__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Hide tab heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';

  // Initialize Navigation variable.
  $variables['nav']['#attributes']['class'][] = "{$base_class}__nav";
  $variables['nav']['#wrapper_attributes'] = [];
  $variables['nav']['#items'] = [];
  $variables['nav']['#theme'] = 'item_list';

  // Validate and create nav from tab list.
  foreach ($paragraph->get('field_paragraphs') as $delta => $component) {
    /** @var \Drupal\entity_reference_revisions\Plugin\Field\FieldType\EntityReferenceRevisionsItem $component */
    // Check if each tabs tab has authentication visibility setting selected.
    $tabs_tab_visibility = $component->entity->get('field_authentication_visibility')->value;
    // If "show for authenticated" setting is selected and user is authenticated.
    $show_tabs_tab = $tabs_tab_visibility === '1' && \Drupal::currentUser()->isAuthenticated() === TRUE;

    // If title is set.
    if ($component->entity instanceof ParagraphInterface && !$component->entity->get('field_title')->isEmpty()) {
      // And "show for authenticated" settings criteria is met.
      if ($tabs_tab_visibility === NULL || $show_tabs_tab) {
        // Add tabs tab title as a nav item on tabs paragraph.
        $variables['nav']['#items'][] = [
          '#type' => 'link',
          '#title' => $component->entity->get('field_title')->value,
          '#url' => Url::fromUserInput("#paragraph-{$component->entity->id()}"),
          '#wrapper_attributes' => [
            'class' => ["{$base_class}__nav-item"],
          ],
        ];
      }
      // Otherwise remove it from being rendered.
      else {
        unset($variables['content']['field_paragraphs'][$delta]);
      }
    }
  }

  // Toggle version of tabs. (This is done here mainly as a placeholder for if
  // we ever need to support vertical tabs or other version. Then we can make it
  // toggleable and just add the class.)
  $tab_version = 'horizontal';
  switch ($tab_version) {
    case 'horizontal':
    default:
      $variables['attributes']['class'][] = "{$base_class}--horizontal";
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for tabs_tab, full.
 */
function urone_preprocess_paragraph__tabs_tab__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Default to tabindex -1 for accessibility. JS will initialize and update.
  $variables['attributes']['tabindex'] = '-1';
  // Hide tab heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';

  // Remove tabs tab for anonymous user.
  if (!urone_show_tabs_tab($paragraph)) {
    unset($variables['content']);
  }
}

/**
 * Determine whether to show or hide tab.
 */
function urone_show_tabs_tab($paragraph) {
  $show = TRUE;

  // Check if authentication field exists on tabs tab and is not empty.
  if ($paragraph->hasField('field_authentication_visibility') && !$paragraph->get('field_authentication_visibility')->isEmpty()) {
    // See if authentication setting is selected.
    $authentication_visibility = $paragraph->get('field_authentication_visibility')->value;
    $current_user = \Drupal::currentUser();

    // If "show for authenticated" settings is selected.
    if ($authentication_visibility === '1') {
      // Hide for anonymous user.
      if ($current_user->isAuthenticated() === FALSE) {
        $show = FALSE;
      }
    }
    // If "hide for authenticated" settings is selected.
    if ($authentication_visibility === '0') {
      // Hide for authenticated user.
      if ($current_user->isAuthenticated() === FALSE) {
        $show = TRUE;
      }
    }
  }

  return $show;
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for use case card,
 * full.
 */
function urone_preprocess_paragraph__use_case_card__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];

  // Add image.
  if ($paragraph->hasField('field_use_case_image')) {
    $image = NULL;
    $fid = $paragraph->get('field_use_case_image')->target_id;
    if (!empty($fid)) {
      $file = File::load($fid);
      if (!empty($file)) {
        $image = str_replace('public://', '/sites/default/files/', $file->getFileUri());
      }
    }
    $variables['image'] = $image;
  }

  // Add icon.
  if ($paragraph->hasField('field_svg_icon')) {
    $icon = NULL;
    $fid = $paragraph->get('field_svg_icon')->target_id;
    if (!empty($fid)) {
      $file = File::load($fid);
      if (!empty($file)) {
        $icon = str_replace('public://', '/sites/default/files/', $file->getFileUri());
      }
    }
    $variables['icon'] = $icon;
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for use case card,
 * full.
 */
function urone_preprocess_paragraph__hero_with_icon__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];

  // Add image.
  if ($paragraph->hasField('field_hero_image')) {
    $image = NULL;
    $fid = $paragraph->get('field_hero_image')->target_id;
    if (!empty($fid)) {
      $file = File::load($fid);
      if (!empty($file)) {
        $image = str_replace('public://', '/sites/default/files/', $file->getFileUri());
      }
    }
    $variables['image'] = $image;
  }

  // Add icon.
  if ($paragraph->hasField('field_svg_icon')) {
    $icon = NULL;
    $fid = $paragraph->get('field_svg_icon')->target_id;
    if (!empty($fid)) {
      $file = File::load($fid);
      if (!empty($file)) {
        $icon = str_replace('public://', '/sites/default/files/', $file->getFileUri());
      }
    }
    $variables['icon'] = $icon;
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for promo hero, full.
 */
function urone_preprocess_paragraph__promo_hero__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Remove component field-wrappers.
  if (array_key_exists('field_copy', $variables['content']) && !empty($variables['content']['field_copy'])) {
    unset($variables['content']['field_copy']['#theme']);
  }

  // Move media field to new variable.
  if (isset($variables['content']['field_media_item']) && !empty($variables['content']['field_media_item'])) {
    $variables['media'] = $variables['content']['field_media_item'];
    $variables['media']['#attributes']['class'][] = "{$base_class}__media";
    unset($variables['content']['field_media_item'], $variables['media']['#theme']);
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for dynamic cta, full.
 */
function urone_preprocess_paragraph__dynamic_cta__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Add all CTA links to cta_links variables.
  $first_link = NULL;
  if (isset($variables['content']['field_links']) && !empty($variables['content']['field_links'])) {
    $variables['cta_links'] = $variables['content']['field_links'];
    $variables['cta_links']['#attributes']['id'] = "form-select__options";
    $variables['cta_links']['#theme'] = 'select';
    $variables['cta_links']['#options'] = [];

    if ($paragraph->hasField('field_links') && !$paragraph->get('field_links')->isEmpty()) {
      foreach ($paragraph->get('field_links')->getValue() as $link) {
        $uri = $link['uri'];
        if (stripos($link['uri'], 'http:') !== FALSE || stripos($link['uri'], 'https:') !== FALSE) {
          $url = Url::fromUri($uri);
          $cta_link = $url->toString();
        }
        elseif (stripos($link['uri'], 'public:') !== FALSE || stripos($link['uri'], 'entity:') !== FALSE) {
          $url = Url::fromUri($uri);
          $cta_link = $url->toString();
        }
        else {
          $url = Url::fromUserInput($uri);
          $cta_link = $url->toString();
        }

        // Set first link uri.
        if (empty($first_link) && !empty($url)) {
          $first_link = $url;
        }

        $variables['cta_links']['#options'][$cta_link] = $link['title'];
      }
    }

    // Remove field_links from output.
    unset($variables['content']['field_links']);
  }

  // Move link to footer and add class.
  if (isset($variables['content']['field_cta']) && !empty($variables['content']['field_cta'])) {
    $variables['cta'] = $variables['content']['field_cta'];

    // Set link if we have them.
    if (isset($first_link)) {
      $variables['cta'][0]['#url'] = $first_link;
    }

    // Separate variable for input label.
    $variables['ctaText'] = $variables['content']['field_cta'];
    unset($variables['ctaText']['#theme']);

    $variables['cta'][0]['#options']['attributes']['class'] = "{$base_class}__link";
    unset($variables['content']['field_cta'], $variables['cta']['#theme']);
  }
}

/**
 * Implements hook_preprocess_paragraph__card_wrapper__full() for card, full.
 */
function urone_preprocess_paragraph__cards_wrapper__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  $variables['#attribute_variables'][] = 'cta_attributes';
  $variables['cta_attributes']['class'][] = "{$base_class}__cta";

  $variables['#attribute_variables'][] = 'collection_attributes';
  $variables['collection_attributes']['class'][] = "{$base_class}__collection";

  $variables['super_title'] = $variables['content']['field_super_title'];
  unset($variables['content']['field_super_title'], $variables['super_title']['#theme']);

  $variables['subtitle'] = $variables['content']['field_subtitle'];
  unset($variables['content']['field_subtitle'], $variables['subtitle']['#theme']);

  $variables['card_collection'] = $variables['content']['field_card_collection'];
  unset($variables['content']['field_card_collection'], $variables['card_collection']['#theme']);

  $variables['cta'] = $variables['content']['field_cta_reference'];
  unset($variables['content']['field_cta_reference'], $variables['cta']['#theme']);

  // Add conditional class to card-wrapper element if we have a card_style class set.
  if (!$paragraph->get('field_card_style')->isEmpty()) {
    $card_classy_styles = $paragraph->get('field_card_style')->referencedEntities();

    foreach ($card_classy_styles as $style) {
      $raw_classes = $style->getClasses();
      $array_classes = explode("\r\n", $raw_classes);

      if (!empty($array_classes)) {
        $current_classes = $variables['collection_attributes']['class'];
        $variables['collection_attributes']['class'] = array_merge($current_classes, $array_classes);
      }

      // Remove added classes from classy styles as this is an unwanted side-effect.
      $classes = array_diff($variables['attributes']['class'], $array_classes);
      $variables['attributes']['class'] = $classes;

      // Override view mode based on style chosen.
      switch ($style->id()) {
        case 'card__item':
          $view_mode = 'item_card';
          $field_settings['type'] = 'entity_reference_entity_view';
          $field_settings['settings']['view_mode'] = $view_mode;
          $field_settings['label'] = 'hidden';

          $variables['card_collection'] = $paragraph->get('field_card_collection')->view($field_settings);
          unset($variables['card_collection']['#theme']);
          break;

        case 'card__default':
          $view_mode = 'sub_category_card';
          $field_settings['type'] = 'entity_reference_entity_view';
          $field_settings['settings']['view_mode'] = $view_mode;
          $field_settings['label'] = 'hidden';

          $variables['card_collection'] = $paragraph->get('field_card_collection')->view($field_settings);
          unset($variables['card_collection']['#theme']);
        default;
      }
    }
  }
}

/**
 * Implements hook_preprocess_paragraph__card__full() for card, full view mode.
 */
function urone_preprocess_paragraph__card__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $style = $paragraph->get('field_style')->target_id;
  $base_class = $variables['component_base_class'];

  // Temporarily commenting out until we figure out image styles for all card styles.
  //  $parent = $paragraph->getParentEntity();
  //  if ($parent->get('field_layout')->isEmpty() || $parent->get('field_layout')->target_id === 'cards_wrapper__layout_horizontal') {
  //    $variables['content']['field_media_item'][0]['#view_mode'] = 'solutions_cards';
  //  }.

  // Define new media attributes container and class.
  $variables['#attribute_variables'][] = 'media_attributes';
  $variables['media_attributes']['class'][] = "{$base_class}__media";
  $variables['#attribute_variables'][] = 'text_attributes';
  $variables['text_attributes']['class'][] = "{$base_class}__text";
  $variables['#attribute_variables'][] = 'header_attributes';
  $variables['header-attributes'][] = "{$base_class}__header";
  $variables['#attribute_variables'][] = 'wrapper_attributes';
  $variables['wrapper_attributes']['class'][] = "{$base_class}__padding";

  // Floating Link
  // Move field_link to it's own variable.
  $variables['floating_link'] = $variables['content']['field_link'];
  unset($variables['content']['field_link'], $variables['floating_link']['#theme']);

  // Media Item.
  $variables['media_item'] = $variables['content']['field_media_item'];
  unset($variables['content']['field_media_item'], $variables['media_item']['#theme']);

  // Icon.
  $variables['media_icon'] = $variables['content']['field_media_icon'];
  unset($variables['content']['field_media_icon'], $variables['media_icon']['#theme']);

  // Super Link.
  $variables['super_link'] = $variables['content']['field_cta'];
  unset($variables['content']['field_cta'], $variables['super_link']['#theme']);

  // CTA.
  unset($variables['content']['field_cta_reference']['#theme']);

  // Unset 'field_style' to stop it from showing at the bottom of 'content'.
  unset($variables['content']['field_style']);
}

/**
 * Implements hook_preprocess_paragraph__card__item_card() for card, item card view mode.
 */
function urone_preprocess_paragraph__card__item_card(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $style = $paragraph->get('field_style')->target_id;
  $base_class = $variables['component_base_class'];

  // Define new media attributes container and class.
  $variables['#attribute_variables'][] = 'media_attributes';
  $variables['media_attributes']['class'][] = "{$base_class}__media";
  $variables['#attribute_variables'][] = 'text_attributes';
  $variables['text_attributes']['class'][] = "{$base_class}__text";
  $variables['#attribute_variables'][] = 'header_attributes';
  $variables['header-attributes'][] = "{$base_class}__header";
  $variables['#attribute_variables'][] = 'wrapper_attributes';
  $variables['wrapper_attributes']['class'][] = "{$base_class}__padding";

  $variables['title'] = $variables['content']['field_title'];
  unset($variables['content']['field_title'], $variables['title']['#theme']);

  // Floating Link
  // Move field_link to it's own variable.
  $variables['floating_link'] = $variables['content']['field_link'];
  unset($variables['content']['field_link'], $variables['floating_link']['#theme']);

  // Media Item.
  $variables['media_item'] = $variables['content']['field_media_item'];
  unset($variables['content']['field_media_item'], $variables['media_item']['#theme']);

  // Icon.
  $variables['media_icon'] = $variables['content']['field_media_icon'];
  unset($variables['content']['field_media_icon'], $variables['media_icon']['#theme']);

  // Super Link.
  $variables['super_link'] = $variables['content']['field_cta'];
  unset($variables['content']['field_cta'], $variables['super_link']['#theme']);

  // CTA.
  unset($variables['content']['field_cta_reference']['#theme']);

  // Unset 'field_style' to stop it from showing at the bottom of 'content'.
  unset($variables['content']['field_style']);
}

/**
 * Implements hook_preprocess_paragraph__card__sub_category_card() for card, sub-category card view mode.
 */
function urone_preprocess_paragraph__card__sub_category_card(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $style = $paragraph->get('field_style')->target_id;
  $base_class = $variables['component_base_class'];

  // Define new media attributes container and class.
  $variables['#attribute_variables'][] = 'media_attributes';
  $variables['media_attributes']['class'][] = "{$base_class}__media";
  $variables['#attribute_variables'][] = 'text_attributes';
  $variables['text_attributes']['class'][] = "{$base_class}__text";
  $variables['#attribute_variables'][] = 'header_attributes';
  $variables['header-attributes'][] = "{$base_class}__header";
  $variables['#attribute_variables'][] = 'wrapper_attributes';
  $variables['wrapper_attributes']['class'][] = "{$base_class}__padding";

  $variables['title'] = $variables['content']['field_title'];
  unset($variables['content']['field_title'], $variables['title']['#theme']);

  // Floating Link
  // Move field_link to it's own variable.
  $variables['floating_link'] = $variables['content']['field_link'];
  unset($variables['content']['field_link'], $variables['floating_link']['#theme']);

  // Media Item.
  $variables['media_item'] = $variables['content']['field_media_item'];
  unset($variables['content']['field_media_item'], $variables['media_item']['#theme']);

  // Icon.
  $variables['media_icon'] = $variables['content']['field_media_icon'];
  unset($variables['content']['field_media_icon'], $variables['media_icon']['#theme']);

  // Super Link.
  $variables['super_link'] = $variables['content']['field_cta'];
  unset($variables['content']['field_cta'], $variables['super_link']['#theme']);

  // CTA.
  unset($variables['content']['field_cta_reference']['#theme']);

  // Unset 'field_style' to stop it from showing at the bottom of 'content'.
  unset($variables['content']['field_style']);
}

/**
 * Implements hook_preprocess_paragraph__listing_wrapper__full() for listing wrapper.
 */
function urone_preprocess_paragraph__listing_wrapper__full(array &$variables) {
  $paragraph = $variables['paragraph'];

  $variables['listing_collection'] = $variables['content']['field_listing_item_collection'];
  unset($variables['content']['field_listing_item_collection'], $variables['listing_collection']['#theme']);

  $variables['attributes']['id'] = $paragraph->get('field_anchor_id')->value;
  unset($variables['content']['field_anchor_id']);
}

/**
 * Implements hook_preprocess_paragraph__listing_item__full() for listing item.
 */
function urone_preprocess_paragraph__listing_item__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $accordion_items = $paragraph->get('field_accordion_item')->referencedEntities();

  /** @var \Drupal\paragraphs\Entity\Paragraph $accordion_item */
  $accordion_item = reset($accordion_items);
  $base_class = $variables['component_base_class'];

  // Define new media attributes.
  $variables['#attribute_variables'][] = 'media_attributes';
  $variables['media_attributes']['class'][] = "{$base_class}__media";

  // Define accordion item attributes.
  $variables['#attribute_variables'][] = 'accordion_attributes';
  $variables['accordion_attributes']['class'][] = "{$base_class}__accordion-item";

  // Image.
  if (!$paragraph->get('field_media_item')->isEmpty()) {
    $variables['image'] = $variables['content']['field_media_item'];
    unset($variables['content']['field_media_item'], $variables['image']['#theme']);
  }

  // Subtitle.
  if (!$paragraph->get('field_subtitle')->isEmpty()) {
    $variables['subtitle'] = $variables['content']['field_subtitle'];
    unset($variables['content']['field_subtitle'], $variables['subtitle']['#theme']);
  }

  // Accordion Item.
  if (!$paragraph->get('field_accordion_item')->isEmpty()) {
    $variables['accordion_item'] = $variables['content']['field_accordion_item'];
    unset($variables['content']['field_accordion_item'], $variables['accordion_item']['#theme']);
  }

  // Accordion Item Title.
  if ($accordion_item instanceof ParagraphInterface && !$accordion_item->get('field_copy')->isEmpty()) {

    // Grabbing title values from listing item and accordion item.
    $listing_item_title = $paragraph->get('field_title')->value;
    $accordion_item_title = $accordion_item->get('field_title')->value;

    $variables['accordion_item_title'] = $accordion_item_title ? $accordion_item_title : t('Learn more about ') . $listing_item_title;
  }
}
