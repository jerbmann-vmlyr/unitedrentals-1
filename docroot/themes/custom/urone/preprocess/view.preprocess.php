<?php

/**
 * @file
 * Preprocess functions related to view entities.
 *
 * Index:
 *
 * @see urone_preprocess_views_view()
 */

use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Drupal\Component\Utility\Html;

/**
 * Implements hook_preprocess_views_view().
 */
function urone_preprocess_views_view(array &$variables) {
  // All views get the pager library.
  $variables['#attached']['library'][] = 'urone/views-pager';

  // Attach image cards library
  if ($variables['view']->name = 'Company cards') {
    $variables['#attached']['library'][] = 'urone/views--image-cards';
  }

  // Attach no favorites container library
  $variables['#attached']['library'][] = 'urone/node--card--item';
}

/**
 * Implements hook_preprocess_views_view().
 */
function urone_preprocess_views_view__item_testing__marketplace(&$variables) {
  // Grab current category / sub-category from url.
  $path = \Drupal::request()->getpathInfo();
  $urlParts = explode('/', $path);
  $currentCategory = NULL;
  $currentSubCategory = NULL;

  // Load category from alias.
  if (isset($urlParts[3])) {
    // Snag term path by alias.
    $path = \Drupal::service('path.alias_manager')->getPathByAlias('/marketplace/equipment/' . $urlParts[3]);
    if (preg_match('/taxonomy\/term\/(\d+)/', $path, $matches)) {
      $currentCategory = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($matches[1]);
      $currentCategoryObj = new \stdClass();
      $currentCategoryObj->tid = $currentCategory->id();
      $currentCategoryObj->name = $currentCategory->getName();
      $variables['categoryDescription'] = $currentCategory->get('description')->value;
      $currentCategoryObj->url = '/marketplace/equipment/' . $urlParts[3];
      $variables['currentCategory'] = $currentCategoryObj;
      $newTitle = $currentCategoryObj->name . ' ' . t('Equipment for Rent');
      $request = \Drupal::request();

      // Get data layer attribute value
      $variables['productCategory'] = $currentCategoryObj->name;

      if ($route = $request->attributes->get(RouteObjectInterface::ROUTE_OBJECT)) {
        $route->setDefault('_title', $newTitle);
      }
    }
  }

  // Load sub-category from alias.
  if (!empty($currentCategory) && isset($urlParts[3]) && isset($urlParts[4])) {
    // Snag term path by alias.
    $path = \Drupal::service('path.alias_manager')->getPathByAlias('/marketplace/equipment/' . $urlParts[3] . '/' . $urlParts[4]);
    if (preg_match('/taxonomy\/term\/(\d+)/', $path, $matches)) {
      $currentSubCategory = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($matches[1]);
      $currentSubCategoryObj = new \stdClass();
      $currentSubCategoryObj->tid = $currentSubCategory->id();
      $currentSubCategoryObj->name = $currentSubCategory->getName();
      $variables['categoryDescription'] = $currentSubCategory->get('description')->value;
      $currentSubCategoryObj->url = '/marketplace/equipment/' . $urlParts[3] . '/' . $urlParts[4];
      $variables['currentSubCategory'] = $currentSubCategoryObj;
      $newTitle = $currentSubCategoryObj->name . ' ' . t('Equipment for Rent');
      $request = \Drupal::request();

      // Get data layer attribute value
      $variables['productSubCategory'] = $currentSubCategoryObj->name;

      if ($route = $request->attributes->get(RouteObjectInterface::ROUTE_OBJECT)) {
        $route->setDefault('_title', $newTitle);
      }
    }
  }
  
  // Add selected filters.
  $variables['selectedFilters'] = [];
  if (\Drupal::request()->query->get('recently_viewed')) {
    $filter = new \stdClass();
    $filter->name = t('Recently Viewed');
    $filter->key = 'recently_viewed';
    $variables['selectedFilters'][] = $filter;
  }
  if (\Drupal::request()->query->get('favorites')) {
    $filter = new \stdClass();
    $filter->name = t('Favorites');
    $filter->key = 'favorites';
    $variables['selectedFilters'][] = $filter;
  }
  if (\Drupal::request()->query->get('equipment_filters')) {
    $equipmentFilters = explode(' ', \Drupal::request()->query->get('equipment_filters'));
    foreach ($equipmentFilters as $equipmentFilter) {
      $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($equipmentFilter);
      if (!empty($term)) {
        $filter = new \stdClass();
        $filter->name = $term->getName();
        $filter->key = 'equipment_filters';
        $filter->tid = $term->id();
        $variables['selectedFilters'][] = $filter;
      }
    }
  }
  if (\Drupal::request()->query->get('search_within')) {
    $searchWithinFilters = explode(' ', \Drupal::request()->query->get('search_within'));
    foreach ($searchWithinFilters as $searchWithinFilter) {
      $filter = new \stdClass();
      $filter->name = $searchWithinFilter;
      $filter->key = 'search_within';
      $filter->term = $searchWithinFilter;
      $variables['selectedFilters'][] = $filter;
    }
  }

  // Add results count.
  $variables['resultsCount'] = $variables['view']->total_rows;

  // Load catClasses.
  if (!empty($variables['view']->result)) {
    $catClasses = [];
    foreach ($variables['view']->result as $result) {
      if (isset($result->_entity) && !empty($result->_entity)) {
        $catClasses[] = $result->_entity->get('field_item_cat_class_code')->value;
      }
    }

    // Now add to page.
    if (!empty($catClasses)) {
      $variables['catClasses'] = implode(',', $catClasses);
    }
  }
}

/**
 * Implements template_preprocess_views_view_fields().
 */
function urone_preprocess_views_view_fields(&$variables) {
  $view = $variables['view'];
  if ($view->id() == 'item_testing' && $view->display_id = 'marketplace') {
    if (isset($variables['fields']['field_item_highlights']->content)
      && !empty((string) $variables['fields']['field_item_highlights']->content)
    ) {
      $variables['fields']['field_item_highlights']->highlights = explode(' :: ', (string) $variables['fields']['field_item_highlights']->content);
    }
    else {
      $variables['quickViewItems'] = [];
    }

    // @TODO: Rework the view so that it outputs teaser view mode and we won't have to do this.
    // Pull alt tag out of json from the formatter and set new variable.
    if (isset($variables['fields']['field_images_unlimited_1']) && !empty($variables['fields']['field_images_unlimited_1'])) {
      $json = $variables['fields']['field_images_unlimited_1']->content;
      $decoded = json_decode($json);
      if (!empty($decoded)) {
        $variables['fields']['field_images_unlimited_1']->alt = $decoded->alt;
      }
      else {
        // If it doesn't have an alt tag, fall back to node title.
        $variables['fields']['field_images_unlimited_1']->alt = $variables['fields']['title']->content;
      }
    }

    $variables['productCatClass'] = $variables['fields']['field_item_cat_class_code']->content;
    $variables['productName'] = $variables['fields']['title']->content;
    $variables['productQuantity'] = 1;
  }
}

/**
 * Implements template_preprocess_views_view__project_uptime__search_all().
 */
function urone_preprocess_views_view__project_uptime__search_all(array &$variables) {
  /** @var \Drupal\views\ViewExecutable $view */
  $view = $variables['view'];
  $id = $view->id();
  $base_class = Html::cleanCssIdentifier("view-{$id}");

  unset($variables['exposed']['#theme']);
  $variables['exposed']['#attributes']['class'][] = "{$base_class}__exposed-filter";
  $variables['exposed']['pillars']['#attributes']['class'][] = "{$base_class}__exposed_filter__pillars";
  $variables['exposed']['business-units']['#attributes']['class'][] = "{$base_class}__exposed_filter__business-units";
}

/**
 * Implements urone_preprocess_views_view__equipment_categories__similar_equipment().
 */
function urone_preprocess_views_view__equipment_categories__similar_equipment(array &$variables) {
  /** @var \Drupal\views\ViewExecutable $view */
  $view = $variables['view'];
  $id = $view->id();
  $base_class = Html::cleanCssIdentifier("view-{$id}");

  // Add library.
  $variables['#attached']['library'][] = 'urone/views--equipment-categories--similar-equipment';
}

