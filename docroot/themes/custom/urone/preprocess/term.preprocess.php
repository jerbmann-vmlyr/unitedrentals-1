<?php

/**
   * @file
   * Preprocess functions related to taxonomy terms.
   *
   * Index:
   *
   * @see vmlyrcom_preprocess_taxonomy_term()
 */

use Drupal\Core\Url;
use Drupal\views\Views;
use Drupal\block\Entity\Block;

function urone_preprocess_taxonomy_term(array &$variables) {
  /** @var \Drupal\taxonomy\Entity\Term $term **/
  $term = $variables['term'];
  $view_mode = $variables['view_mode'];
  $vocab_id = $term->bundle();

  $vocab_function = __FUNCTION__ . "__$vocab_id";
  if (function_exists($vocab_function)) {
    $vocab_function($variables);
  }

  $view_function = __FUNCTION__ . "__{$view_mode}";
  if (function_exists($view_function)) {
    $view_function($variables);
  }

  $vocab_and_view_mode_function = __FUNCTION__ . "__{$vocab_id}__{$view_mode}";
  if (function_exists($vocab_and_view_mode_function)) {
    $vocab_and_view_mode_function($variables);
  }
}

/**
 * Implements hook_preprocess_taxonomy_term().
 */
function urone_preprocess_taxonomy_term__card_tile(array &$variables) {
  unset($variables['content']['field_media_icon']['#theme']);
}

/**
 * Implements hook_preprocess_taxonomy_term().
 */
function urone_preprocess_taxonomy_term__full_bleed_card(array &$variables) {
  /** @var \Drupal\taxonomy\Entity\Term $term **/
  $term = $variables['term'];

  $ancestors = Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadAllParents($term->id());
  if (is_array($ancestors) && count($ancestors) > 1) {
    $parent = $ancestors[array_keys($ancestors)[1]];
    $options = ['absolute' => TRUE];
    $variables['parent'] = [
      'label' => $parent->label(),
      'url' => Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $parent->id()], $options),
    ];
  }
}

/**
 * Implements hook_preprocess_taxonomy_term().
 */
function urone_preprocess_taxonomy_term__categories__full(array &$variables) {
  // Add equipment_listing_siderail_block.
  $block_entity = Block::load('equipment_listing_siderail_block');
  $block = \Drupal::entityTypeManager()->getViewBuilder('block')->view($block_entity);
  $variables['equipmentListingSiderailBlock'] = $block;

  // Add marketplace display of item_testing view.
  $view = Views::getView('item_testing');
  if (is_object($view)) {
    $view->setDisplay('marketplace');
    $view->execute();

    // Render the view
    $render = $view->render();
    $variables['marketplace'] = \Drupal::service('renderer')->render($render);
  }

}
