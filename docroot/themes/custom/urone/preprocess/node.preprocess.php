<?php

/**
 * @file
 * Preprocess functions related to node entities.
 *
 * Index:
 *
 * @see urone_preprocess_node()
 * @see urone_preprocess_node__basic_page__full()
 * @see urone_preprocess_node__item__card()
 * @see urone_preprocess_node__item__full()
 * @see urone_preprocess_node__item__search_index()
 * @see urone_preprocess_node__events__full()
 * @see urone_preprocess_node__project_uptime_pages__carousel_card()
 * @see urone_preprocess_node__project_uptime_pages__full()
 * @see urone_preprocess_node__project_uptime_pages__teaser()
 */

use Drupal\Core\Link;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\user\Entity\User;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;

/**
 * Implements hook_preprocess_node().
 */
function urone_preprocess_node(array &$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];

  // Remove auto created .field__item wrapper class around paragraphs.
  if ($node->hasField('field_paragraph') && !$node->get('field_paragraph')->isEmpty()) {
    unset($variables['content']['field_paragraph']['#theme']);
  }

  // Exposing the global teaser hook.
  if (function_exists('urone_preprocess_node__global__' . $variables['view_mode'])) {
    call_user_func_array('urone_preprocess_node__global__' . $variables['view_mode'], [&$variables]);
  }

  if ($node->hasField('field_paragraph')) {
    $data = $node->get('field_paragraph');
    $value = $data->getEntity();

    // Get paragraph array
    $attached_paragraphs = $value->get('field_paragraph')->getValue();

    foreach ($attached_paragraphs as $key => $attached_paragraph) {
      // Load paragraph data of each element in the paragraph array
      $paragraph_data = Paragraph::load($attached_paragraph['target_id']);

      // Condition to unset the promo banner for TC Users
      if (!empty($paragraph_data) && $paragraph_data->hasField('field_hide_for_tc_users')) {
        $is_hide = $paragraph_data->get('field_hide_for_tc_users')->value;
        $user_roles = Drupal::currentUser()->getRoles();
        if ($is_hide == 1 && in_array('ur_total_control_account', $user_roles)) {
          unset($variables['content']['field_paragraph'][$key]);
        }
      }
    }
  }
}

/**
 * Implementation for view mode.
 */
function urone_preprocess_node__global__teaser(array &$variables) {
  $node = $variables['node'];
  $type = $node->getType();
  $id = $node->id();

  $variables['#attribute_variables'][] = 'text_attributes';
  $variables['text_attributes']['class'][] = 'node__text';
  $variables['#attribute_variables'][] = 'media_attributes';
  $variables['media_attributes']['class'][] = 'node__media';

  switch ($type) {
    case 'annual_reports_proxy_statements':
      $variables['image'] = $variables['content']['field_report_image'];
      unset($variables['content']['field_report_image'], $variables['image']['#theme']);

      $variables['file'] = $variables['content']['field_report_file'];
      unset($variables['content']['field_report_file'], $variables['file']['#theme']);
      unset($variables['content']);
      break;

    case 'awards_and_recognition':
      $variables['image'] = $variables['content']['field_awards_image'];
      unset($variables['content']['field_awards_image'], $variables['image']['#theme']);
      break;

    case 'events':
      $variables['node_url'] = Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $id);
      unset($variables['content']['field_text_plain']);
      break;

    case 'news':
      $variables['node_url'] = $node->get('field_news_external_link')->first()->getUrl();
      unset($variables['content']['field_news_external_link']);
      break;

    case 'investor_presentations':
      $variables['image'] = $variables['content']['field_presentation_image'];
      unset($variables['content']['field_presentation_image'], $variables['image']['#theme']);

      $variables['file'] = $variables['content']['field_presentation_file'];
      $variables['file'][0]['#description'] = 'PDF Download';
      unset($variables['content']['field_presentation_file'], $variables['file']['#theme']);
      break;

    case 'press_releases':
      $variables['node_url'] = Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $id);
      break;
  }
}

/**
 * Implementation for Image Cards view mode.
 */
function urone_preprocess_node__image_cards(array &$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];
  $type = $node->getType();
  $id = $node->id();
  $variables['type_label'] = $node->type->entity->label();

  // Add title.
  $variables['card_title'] = \Drupal::translation()->translate($node->getTitle());

  // Add specific classes for the date p tag.
  $variables['#attribute_variables'][] = 'date_attributes';
  $variables['date_attributes']['class'][] = "node__date";

  // Add variable for node_id to be used in aria-labelledby tag
  $variables['node_id'] = 'node__' . $id . '-title';

  // Content Specific Fields.
  switch ($type) {
    case 'press_releases':
      $variables['card_date'] = date('M j, Y', strtotime($node->field_press_release_date->value));
      $variables['card_link'] = Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $id);
      break;

    case 'investor_relations':
      $variables['card_date'] = date('M j, Y', $node->getCreatedTime());
      $variables['card_link'] = '/our-company/investor-relations';
      if (!$node->get('field_investor_relations_image')->isEmpty()) {
        $file_uri = $node->get('field_investor_relations_image')->entity->getFileUri();
        $variables['card_image'] = ImageStyle::load('medium')->buildUrl($file_uri);
        $pdf = $node->field_investor_relations_image->entity->getFileUri();
        if ($pdf) {
          $variables['card_link'] = $pdf;
        }
      }
      break;

    case 'news':
      $variables['card_date'] = $node->field_news_date->date->format('M j, Y');
      if (!$node->get('field_image')->isEmpty()) {
        $file_uri = $node->get('field_image')->entity->getFileUri();
        $variables['card_image'] = ImageStyle::load('medium')->buildUrl($file_uri);
      }
      $variables['card_link'] = $node->get('field_news_external_link')->first()->getUrl();
      break;

    case 'project_uptime_pages':
      $variables['card_date'] = date('M j, Y', $node->getCreatedTime());
      if (!$node->get('field_image')->isEmpty() && isset($variables['content']['field_image'])) {
        $variables['card_image'] = $variables['content']['field_image'];
      }
      $variables['card_link'] = Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $id);
      break;

    default:
      $variables['card_date'] = date('M j, Y', $node->getCreatedTime());
  }
}

/**
 * Implements hook_preprocess_node__VIEW_MODE() for "Card" view mode.
 */
function urone_preprocess_node__card(array &$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];

  $variables['#attribute_variables'][] = 'media_attributes';
  $variables['media_attributes']['class'][] = "node__media";

  $variables['#attribute_variables'][] = 'wrapper_attributes';
  $variables['wrapper_attributes']['class'][] = "node__padding";

  // Set floating link for card.
  $variables['floating_link'] = $node->toLink();

  // Get image for card.
  if (!$node->get('field_teaser_image')->isEmpty()) {
    $variables['media_item'] = $variables['content']['field_teaser_image'];
    unset($variables['content']['field_teaser_image'], $variables['media_item']['#theme']);
  }

  // To get the super link for the card we need to get top level parent link.
  /** @var \Drupal\Core\Menu\MenuLinkManager $menu_link_manager */
  $menu_link_manager = \Drupal::service('plugin.manager.menu.link');
  /** @var \Drupal\Core\Menu\MenuLinkInterface[] $node_links */
  $node_links = $menu_link_manager->loadLinksByRoute('entity.node.canonical', ['node' => $node->id()]);
  $node_link = reset($node_links);
  $variables['super_link'] = NULL;
  if (!empty($node_link)) {
    $currentPluginId = $node_link->getPluginId();
    $parents = $menu_link_manager->getParentIds($currentPluginId);
  
    $reversed_parents = array_reverse($parents);
    $top_parent = reset($reversed_parents);
  
    // Set title from menu link.
    $variables['title'] = $node_link->getTitle();
  
    /** @var \Drupal\Core\Menu\MenuLinkInterface $parent_link */
    $parent_link = $menu_link_manager->createInstance($top_parent);
    $variables['super_link'] = Link::fromTextAndUrl($parent_link->getTitle(), $parent_link->getUrlObject());
  }
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for item, card view mode.
 */
function urone_preprocess_node__item__card(array &$variables) {
  if (array_key_exists('field_item_cat_class_code', $variables['content']) && Element::children($variables['content']['field_item_cat_class_code'])) {
    $variables['content']['field_item_cat_class_code']['#title'] = 'Cat Class';
    $variables['cat_class'] = $variables['content']['field_item_cat_class_code'];
    unset($variables['content']['field_item_cat_class_code']);
  }

  if (array_key_exists('flag_favorites', $variables['content']) && Element::children($variables['content']['flag_favorites'])) {
    $variables['fav_link'] = $variables['content']['flag_favorites'];
    unset($variables['content']['flag_favorites']);
  }

  // Run through photos unlimited and remove all but the first.
  foreach (Element::children($variables['content']['field_images_unlimited']) as $delta) {
    if ($delta) {
      unset($variables['content']['field_images_unlimited'][$delta]);
    }
  }
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for item, card view mode.
 */
function urone_preprocess_node__item__full(array &$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];

  // Grab category.
  $categoryTarget = $node->get('field_item_category')->target_id;
  $category = urone_get_category_parent($categoryTarget, 1);
  if (!empty($category)) {
    $variables['similar_equipment'] = views_embed_view(
      'equipment_categories',
      'similar_equipment',
      $category->id()
    );
  }

  // Set up new variables.
  $variables['#attribute_variables'][] = 'aside_first_attributes';
  $variables['aside_first_attributes']['class'][] = "node__aside node__aside--first";

  $variables['#attribute_variables'][] = 'aside_second_attributes';
  $variables['aside_second_attributes']['class'][] = "node__aside node__aside--second";

  $variables['#attribute_variables'][] = 'primary_attributes';
  $variables['primary_attributes']['class'][] = "node__primary-content";

  // Move images field to new variable.
  if (isset($variables['content']['field_images_unlimited']) && !empty($variables['content']['field_images_unlimited'])) {
    $variables['image'] = $variables['content']['field_images_unlimited'];
    unset($variables['content']['field_images_unlimited']);

    // Ensure all images have alt text. If not at least add node title as alt text
    foreach (Element::children($variables['image']) as $key) {
      /** @var \Drupal\image\Plugin\Field\FieldType\ImageItem $image_item */
      $image_item = $variables['image'][$key]['#build']['item'];

      if (empty($image_item->get('alt')->getValue())) {
        $variables['image'][$key]['#build']['item']->set('alt', Drupal::translation()->translate($node->getTitle()));
      }
    }

    // Create separate thumbnail image style from image field.
    $image_settings = [
      'type' => 'image',
      'label' => 'hidden',
      'settings' => [
        'image_style' => 'square_1_1_96x96'
      ]
    ];

    $variables['thumbnails'] = $node->get('field_images_unlimited')->view($image_settings);
  }

  // Move cat class field to new variable.
  if (isset($variables['content']['field_item_cat_class_code']) && !empty($variables['content']['field_item_cat_class_code'])) {
    $variables['cat_class'] = $variables['content']['field_item_cat_class_code'];
    unset($variables['content']['field_item_cat_class_code']);
    // Get raw cat-class value for use with Vue components.
    $variables['cat_class_value'] = $node->get('field_item_cat_class_code')->value;
  }

  // Move favorites field to new variable.
  if (isset($variables['content']['flag_favorites']) && !empty($variables['content']['flag_favorites'])) {
    $variables['fav_link'] = $variables['content']['flag_favorites'];
    unset($variables['content']['flag_favorites']);
  }
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for item, card view mode.
 */
function urone_preprocess_node__item__search_index(array &$variables) {
  $node = $variables['node'];
  $variables['title'] = $node->getTitle();
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for events, full.
 */
function urone_preprocess_node__events__full(array &$variables) {
  $variables['date'] = $variables['content']['field_date_range'];
  unset($variables['content']['field_date_range'], $variables['date']['#theme']);

  $variables['location'] = $variables['content']['field_text_plain'];
  unset($variables['content']['field_text_plain'], $variables['location']['#theme']);
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for landing, full.
 */
function urone_preprocess_node__landing__full(array &$variables) {
  // Unset hero field as this is used in page template.
  // @see urone_preprocess_page for reference
  unset($variables['content']['field_hero']);
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for press releases, full.
 */
function urone_preprocess_node__press_releases__full(array &$variables) {
  $variables['date'] = $variables['content']['field_press_release_date'];
  unset($variables['content']['field_press_release_date'], $variables['date']['#theme']);

  $variables['location'] = $variables['content']['field_press_release_location'];
  unset($variables['content']['field_press_release_location'], $variables['location']['#theme']);
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for basic page, full.
 */
function urone_preprocess_node__basic_page__full(array &$variables) {
  // Unset body field as this is used in page subtitle.
  // See: urone_preprocess_block__page_title_block for reference.
  if (array_key_exists('body', $variables['content']) && !empty($variables['content']['body'])) {
    unset($variables['content']['body']);
  }
}

/**
 * Helper function that assembles a byline which includes the optional author name and date created into single render array variable.
 */
function _project_uptime_byline(array &$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];

  // Generate author byline if author info available.
  $author_name = '';
  if (!$node->get('field_author')->isEmpty() && !empty($variables['content']['field_author'])) {

    /** @var \Drupal\user\Entity\User $author */
    $author = $node->get('field_author')->referencedEntities()[0];
    if ($author !== NULL) {
      $first_name = '';
      if (!$author->get('field_first_name')->isEmpty()) {
        $first_name = $author->get('field_first_name')->value;
      }

      $last_name = '';
      if (!$author->get('field_last_name')->isEmpty()) {
        $last_name = $author->get('field_last_name')->value;
      }

      $full_name = $first_name . ' ' . $last_name;
    }
    unset($variables['content']['field_author']);
  }

  // We'll always show the created date though.
  /** @var \Drupal\Core\Datetime\DateFormatter $date_formatter */
  $date_formatter = \Drupal::service('date.formatter');
  $formatted_date = $date_formatter->format($node->getCreatedTime(), 'custom', 'F j, Y');

  // Determine if we have an author name to show with byline.
  $byline = !empty($full_name) ? \Drupal::translation()->translate('By ') . $full_name . ' | ' . $formatted_date : $formatted_date;

  $variables['byline'] = [
    '#markup' => $byline,
    '#weight' => -50,
  ];
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for project_uptime_pages, carousel_card.
 */
function urone_preprocess_node__project_uptime_pages__carousel_card(array &$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];
  $id = $node->id();
  $markup = '';

  // Creating a wrapper div specific class
  $variables['#attribute_variables'][] = 'wrapper_attributes';
  $variables['wrapper_attributes']['class'][] = 'node__padding';

  // Creating a card link.
  $variables['card_link'] = Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $id);

  // Aggregate the Pillars into single list field.
  $links = [];
  if (!$node->get('field_pillars')->isEmpty()) {
    // Get all taxonomy term entities from the field.
    $pillars = $node->get('field_pillars')->referencedEntities();
    $project_uptime_url = '/project-uptime/search';

    /** @var \Drupal\taxonomy\Entity\Term $pillar */
    foreach ($pillars as $pillar) {
      // Generate link to exposed filter on the listing pages.
      $links[] = [
        '#type' => 'link',
        '#title' => \Drupal::translation()->translate($pillar->getName()),
        '#url' => Url::fromUserInput($project_uptime_url, [
          'query' => [
            "pillars[{$pillar->id()}]" => $pillar->id()
          ]
        ]),
      ];
    }
  }

  // Create list from aggregated tags.
  if (!empty($links)) {
    $pillar_links = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $links,
      '#attributes' => [
        'class' => [
          'node__pillar-links'
        ],
      ],
    ];

    $variables['pillar_links'] = $pillar_links;
    unset($variables['content']['field_pillars']);
  }

  // Add title.
  $variables['title'] = $node->getTitle();

  // Add create date.
  $variables['post_date'] = date('M j, Y', $node->getCreatedTime());
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for project_uptime_pages, full.
 */
function urone_preprocess_node__project_uptime_pages__full(array &$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];
  $markup = '';

  _project_uptime_byline($variables);

  // Move subhead field above byline.
  $variables['subhead'] = $variables['content']['field_sub_headline'];

  // Unset subhead field from content.
  unset($variables['content']['field_sub_headline']);

  // Remove the image from content.
  unset($variables['content']['field_image']);

  // Aggregate the Pillars into single list field.
  $links = [];
  if (!$node->get('field_pillars')->isEmpty()) {
    // Get all taxonomy term entities from the field.
    $pillars = $node->get('field_pillars')->referencedEntities();
    $project_uptime_url = '/project-uptime/search';

    /** @var \Drupal\taxonomy\Entity\Term $pillar */
    foreach ($pillars as $pillar) {
      // Generate link to exposed filter on the listing pages.
      $links[] = [
        '#type' => 'link',
        '#title' => \Drupal::translation()->translate($pillar->getName()),
        '#url' => Url::fromUserInput($project_uptime_url, [
          'query' => [
            "pillars[{$pillar->id()}]" => $pillar->id()
          ]
        ]),
      ];
    }
    // Unset original field.
    unset($variables['content']['field_pillars']);
  }

  // Create list from aggregated tags.
  if (!empty($links)) {
    $pills = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#attributes' => [
        'class' => [
          'node__pills'
        ],
      ],
      '#items' => $links,
    ];

    $variables['pills'] = $pills;
  }
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for project_uptime_pages, teaser.
 */
function urone_preprocess_node__project_uptime_pages__teaser(array &$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];

  _project_uptime_byline($variables);

  // Initialize variables.
  $variables['#attribute_variables'][] = 'image_group_attributes';
  $variables['#attribute_variables'][] = 'tag_group_attributes';
  $variables['image_group_attributes']['class'][] = 'node__image-container';
  $variables['tag_group_attributes']['class'][] = 'node__tags-container';

  // Aggregate the Pillars and Business Unit tags into single list field.
  $links = [];
  $project_uptime_url = '/project-uptime/search';
  if (!$node->get('field_pillars')->isEmpty()) {
    // Get all taxonomy term entities from the field.
    $pillars = $node->get('field_pillars')->referencedEntities();
    $project_uptime_url = '/project-uptime/search';

    /** @var \Drupal\taxonomy\Entity\Term $pillar */
    foreach ($pillars as $pillar) {
      // Generate link to exposed filter on the listing pages.
      $links[] = [
        '#type' => 'link',
        '#title' => \Drupal::translation()->translate($pillar->getName()),
        '#url' => Url::fromUserInput($project_uptime_url, [
          'query' => [
            "pillars[{$pillar->id()}]" => $pillar->id()
          ]
        ]),
      ];
    }
    // Unset original field.
    unset($variables['content']['field_pillars']);
  }

  if (!$node->get('field_business_units')->isEmpty()) {
    // Get all taxonomy term entities from the field.
    $business_units = $node->get('field_business_units')->referencedEntities();

    /** @var \Drupal\taxonomy\Entity\Term $business_unit */
    foreach ($business_units as $business_unit) {
      // Generate link to exposed filter on the listing pages.
      $links[] = [
        '#type' => 'link',
        '#title' => \Drupal::translation()->translate($business_unit->getName()),
        '#url' => Url::fromUserInput($project_uptime_url, [
          'query' => [
            "business-units[{$business_unit->id()}]" => $business_unit->id()
          ]
        ]),
      ];
    }
    // Unset original field.
    unset($variables['content']['field_business_units']);
  }

  // Create list from aggregated tags.
  if (!empty($links)) {
    $tags = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#attributes' => [
        'class' => [
          'node__tags'
        ],
      ],
      '#items' => $links,
    ];

    $variables['tags'] = $tags;
  }

  /*
   * Generate "New" tag based on created time. If created within 30 days from
   * now it's considered "new".
   */
  $created = $node->getCreatedTime();
  if ($created >= strtotime('-30 days')) {
    $new = \Drupal::translation()->translate('New');
    $variables['new_tag'] = [
      '#markup' => "<span class='node__new-tag-pill'>$new</span>",
    ];
  }

  // Move image field to it's own variable.
  if (!$node->get('field_image')->isEmpty() && isset($variables['content']['field_image'])) {
    $variables['image'] = $variables['content']['field_image'];
    // Unset field wrappers around image.
    unset($variables['image']['#theme']);
    // Unset image field from content.
    unset($variables['content']['field_image']);
  }
}

function urone_get_category_parent($categoryId, $level = 0) {
  $result = NULL;
  $categoryTerm = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->load($categoryId);

  // Not empty AND we haven't hit the depth level we're looking for?
  if (!empty($categoryTerm) && $categoryTerm->get('depth_level')->value > $level) {
    // Grab the parent.
    $categoryParentId = $categoryTerm->get('parent')->target_id;
    $categoryParent = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->load($categoryParentId);

     // PARENT not empty
     if (!empty($categoryParent)) {
       $categoryParentDepth = $categoryParent->get('depth_level')->value;
       // Category parent depth match!
       if ($categoryParentDepth == $level) {
         $result = $categoryParent;
       }
       // We haven't hit the depth level we're looking for?
       else if ($categoryParentDepth > $level) {
         $result = urone_get_category_parent($categoryParentId, $level);
       }
     }
  }

  return $result;
}
