/* eslint-disable */
var tslint = require('tslint');
var templateCompiler = require('vue-template-compiler');
var path = require('path');
var chalk = require('chalk');
var globby = require('globby');
var fs = require('fs');
var { promisify } = require('util');
const read = promisify(fs.readFile);

const paths = {
  src: path.join(__dirname, '..', 'src'),
  tslint: path.join(__dirname, '..', 'tslint.json'),
  eslint: path.join(__dirname, '..', '.eslintrc.js'),
  tsconfig: path.join(__dirname, '..', 'src/tsconfig.json'),
};

const globs = {
  vue: `${paths.src}/**/*.vue`,
  js: `${paths.src}/**/*.js`,
  ts: [`${paths.src}/**/*.ts`, '!**/*.d.ts'],
};

const scripts = Promise.all([
  globby(globs.vue).then(readFileList).then(extractScriptsFromVueFiles),
  globby(globs.ts).then(readFileList).then(assignLangToFiles('ts')),
])
.then(flatten)
.then(function (files) { return files.filter(f => f.lang === 'ts'); })
.then(lintTsFiles)
.then(function (lintResults) {
  var errorCount = lintResults.errorCount;
  if (errorCount > 0) {
    lintResults.output.split('\n').forEach(function (error) {
      console.log(chalk.red(`${error}`));
    })

    console.log(chalk.bgRed.white(`💥 TS lint found ${errorCount} issue${errorCount > 1 ? 's' : ''}`));
    process.exit(1);
  }
  else {
    console.log(chalk.green(`TS linting looks good 👌`));
  }
});

function lintTsFiles(files) {
  var tsLinter = new tslint.Linter({ fix: false, project: paths.tsconfig });
  var tsLintConfig = tslint.Configuration.findConfiguration(paths.tslint).results;
  files.map(function (file) {
    return tsLinter.lint(file.filepath, file.contents, tsLintConfig);
  });
  return tsLinter.getResult();
}

function flatten(listOfLists) {
  return listOfLists.reduce(function (accum, list) {
    return accum.concat(list);
  }, []);
}

function readFile(filepath) {
  return new Promise(function(ok) {
    read(filepath, 'utf8').then(function(contents) {
      ok({ filepath, contents });
    });
  });
}

function assignLangToFiles(lang) {
  return function (files) {
    return files.map(function (file) {
      return Object.assign(file, { lang });
    });
  }
}

function readFileList(filepaths) {
  return Promise.all(filepaths.map(readFile));
}

function extractScriptsFromVueFiles(vueFiles) {
  return vueFiles.map(function (file) {
    var component = templateCompiler.parseComponent(file.contents);
    component.script = component.script || {};
    file.contents = component.script.content || '';
    file.lang = component.script.lang || 'js';
    return file;
  });
}
