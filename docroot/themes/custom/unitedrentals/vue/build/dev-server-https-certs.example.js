/**
 * Absolute paths to your local SSL cert and key.
 *
 * Instructions:
 * 1. copy this file and rename it `dev-server-https-certs.js` (delete the `.example`).
 * 2. Locate your SSL cert and key files (or create them)
 * 3. Place the absolute paths to your cert/key here.
 *
 */
module.exports = {
  cert: "/Users/rbrewer/Development/UnitedRentals/kelex.local.crt",
  key: "/Users/rbrewer/Development/UnitedRentals/kelex.local.key"
}
