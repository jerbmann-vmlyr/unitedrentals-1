/* eslint-disable import/no-extraneous-dependencies, prefer-template, no-unused-expressions */

/**
 * This is a custom version of the tslint-loader that has been modified so that
 * it can handle .vue single-file component files.
 *
 * The API is the exact same as as the original loader:
 * https://github.com/wbuchwalter/tslint-loader
 *
 * There's a bug in tslint-loader (https://github.com/wbuchwalter/tslint-loader/issues/105)
 * that causes the it to break down when it receives a vue file.
 *
 * We can delete this custom loader and replace the loader specified in the webpack
 * config with the original loader once that bug is resolved.
 *
 * [August 13, 2018]
 */
const Lint = require('tslint');
const loaderUtils = require('loader-utils');
const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const rimraf = require('rimraf');
const objectAssign = require('object-assign');
const semver = require('semver');
const templateCompiler = require('vue-template-compiler');

function resolveFile(configPath) {
  return path.isAbsolute(configPath)
    ? configPath
    : path.resolve(process.cwd(), configPath);
}

function resolveOptions(webpackInstance) {
  const tslintOptions = webpackInstance.options && webpackInstance.options.tslint ? webpackInstance.options.tslint : {};
  const query = loaderUtils.getOptions(webpackInstance);

  const options = objectAssign({}, tslintOptions, query);

  const configFile = options.configFile
    ? resolveFile(options.configFile)
    : null;

  options.formatter = options.formatter || 'custom';
  options.formattersDirectory = options.formattersDirectory || `${__dirname}/formatters/`;
  options.configuration = parseConfigFile(webpackInstance, configFile, options);
  options.tsConfigFile = options.tsConfigFile || 'tsconfig.json';
  options.fix = options.fix || false;

  return options;
}

function parseConfigFile(webpackInstance, configFile, options) {
  if (!options.configuration) {
    return Lint.Linter.findConfiguration(configFile, webpackInstance.resourcePath).results;
  }

  if (semver.satisfies(Lint.Linter.VERSION, '>=5.0.0')) {
    return Lint.Configuration.parseConfigFile(options.configuration);
  }

  return options.configuration;
}

function lint(webpackInstance, input, options) {
  const lintOptions = {
    fix: options.fix,
    formatter: options.formatter,
    formattersDirectory: options.formattersDirectory,
    rulesDirectory: '',
  };
  const bailEnabled = (webpackInstance.options && webpackInstance.options.bail === true);

  let program;
  if (options.typeCheck) {
    const tsconfigPath = resolveFile(options.tsConfigFile);
    program = Lint.Linter.createProgram(tsconfigPath);
  }

  const linter = new Lint.Linter(lintOptions, program);
  linter.lint(webpackInstance.resourcePath, input, options.configuration);
  const result = linter.getResult();
  const emitter = options.emitErrors ? webpackInstance.emitError : webpackInstance.emitWarning;

  report(result, emitter, options.failOnHint, options.fileOutput, webpackInstance.resourcePath, bailEnabled);
}

function report(result, emitter, failOnHint, fileOutputOpts, filename, bailEnabled) {
  if (result.failureCount === 0) return;
  if (result.failures && result.failures.length === 0) return;
  const err = new Error(result.output);
  delete err.stack;
  emitter(err);

  if (fileOutputOpts && fileOutputOpts.dir) {
    writeToFile(fileOutputOpts, result);
  }

  if (failOnHint) {
    let messages = '';
    if (bailEnabled) {
      messages = '\n\n' + filename + '\n' + result.output;
    }
    throw new Error('Compilation failed due to tslint errors.' + messages);
  }
}

let cleaned = false;

function writeToFile(fileOutputOpts, result) {
  if (fileOutputOpts.clean === true && cleaned === false) {
    rimraf.sync(fileOutputOpts.dir);
    cleaned = true;
  }

  if (result.failures.length) {
    mkdirp.sync(fileOutputOpts.dir);

    const relativePath = path.relative('./', result.failures[0].fileName);

    const targetPath = path.join(fileOutputOpts.dir, path.dirname(relativePath));
    mkdirp.sync(targetPath);

    const extension = fileOutputOpts.ext || 'txt';

    const targetFilePath = path.join(fileOutputOpts.dir, relativePath + '.' + extension);

    let contents = result.output;

    if (fileOutputOpts.header) {
      contents = fileOutputOpts.header + contents;
    }

    if (fileOutputOpts.footer) {
      contents += fileOutputOpts.footer;
    }

    fs.writeFileSync(targetFilePath, contents);
  }
}

module.exports = function (input, map) {
  this.cacheable && this.cacheable();
  const callback = this.async();

  if (!semver.satisfies(Lint.Linter.VERSION, '>=4.0.0')) {
    throw new Error('Tslint should be of version 4+');
  }

  const options = resolveOptions(this);
  if (/<script.*lang="ts".*>([\s|\S]*)<\/script>/.test(input)) {
    const component = templateCompiler.parseComponent(input);
    lint(this, component.script.content || '', options);
  }
  else {
    lint(this, input, options);
  }

  callback(null, input, map);
};
