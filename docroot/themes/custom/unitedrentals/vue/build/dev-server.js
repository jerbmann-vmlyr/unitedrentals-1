/* eslint-disable import/no-extraneous-dependencies, function-paren-newline, global-require, comma-dangle, no-console */
const cors = require('cors');
const https = require('https');
const fs = require('fs');
const path = require('path');
const express = require('express');
const webpack = require('webpack');
const proxyMiddleware = require('http-proxy-middleware');
const WebpackDevMiddleware = require('webpack-dev-middleware');
const WebpackHotMiddleware = require('webpack-hot-middleware');
const ConnectHistoryApiFallback = require('connect-history-api-fallback');
const config = require('./config');


const webpackConfig = process.env.NODE_ENV === 'testing'
  ? require('./webpack.prod.conf')
  : require('./webpack.dev.conf');

// default port where dev server listens for incoming traffic
const port = process.env.PORT || config.dev.port;

const app = express();
const compiler = webpack(webpackConfig);

/*
In order for Drupal to use HMR, we do 2 things
1. match the output public path to the one we defined in our hotClient (dev-client.js)
2. enable cors.
See these two threads for details:
 https://github.com/glenjamin/webpack-hot-middleware/issues/37#issuecomment-152720099
 https://stackoverflow.com/questions/7067966/how-to-allow-cors
 */
app.use(cors());


let sslOptions;
try {
  fs.readFileSync('build/dev-server-https-certs.js');
  sslOptions = require('./dev-server-https-certs.js');
}
// No certs defined by developer, will run in HTTP mode.
catch (e) {
  console.log('No HTTPS certs found');
}

const publicPath = sslOptions && !sslOptions.forceHttp
  ? 'https://localhost:8443/'
  : 'http://127.0.0.1';

const devMiddleware = WebpackDevMiddleware(compiler, {
  publicPath,
  quiet: true,
  noInfo: true,
  stats: 'minimal',
});

const hotMiddleware = WebpackHotMiddleware(compiler, {
  quiet: true,
  noInfo: true,
  stats: 'minimal',
});

// Define HTTP proxies to your custom API backend
// https://github.com/chimurai/http-proxy-middleware
const { proxyTable } = config.dev;

// proxy api requests
Object.keys(proxyTable).forEach((context) => {
  let options = proxyTable[context];
  if (typeof options === 'string') {
    options = { target: options };
  }
  app.use(proxyMiddleware(options.filter || context, options));
});

// handle fallback for HTML5 history API
app.use(ConnectHistoryApiFallback());

// serve webpack bundle output
app.use(devMiddleware);

// enable hot-reload and state-preserving compilation error display
app.use(hotMiddleware);

// serve pure static assets
const staticPath = path.posix.join(config.dev.assetsPublicPath, config.dev.assetsSubDirectory);
app.use(staticPath, express.static('./static'));

let server;

/**
 * NOTE: If you have created your own dev-server-https-certs.js file, webpack will automatically run as HTTPS.
 * To have webpack run on HTTP delete your local dev-server-https-certs.js file
 *
 * @type {http.Server}
 */
if (!sslOptions) {
  server = app.listen(port, (err) => {
    if (err) {
      console.log(err);
      return;
    }
    console.log('server running at http://127.0.0.1:8080/');
  });
}
else {
  const serverOptions = {
    cert: fs.readFileSync(sslOptions.cert),
    key: fs.readFileSync(sslOptions.key),
  };
  server = https.createServer(serverOptions, app);
  server.listen(8443, () => console.log('server running at https://localhost:8443/'));
}

const shutdown = signal => () => {
  console.log(`\nStopping the server because ${signal} was received`); // eslint-disable-line
  process.exit(1);
};

process.on('SIGINT', shutdown('SIGINT'));
process.on('SIGHUP', shutdown('SIGHUP'));
process.on('SIGTERM', shutdown('SIGTERM'));

module.exports = server;
