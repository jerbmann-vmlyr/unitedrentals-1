/* eslint-disable import/no-extraneous-dependencies, function-paren-newline, prefer-template, global-require, comma-dangle */
const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin');
const WebpackBar = require('webpackbar');

const baseWebpackConfig = require('./webpack.base.conf');
const config = require('./config');

function assetsPath(_path) {
  return path.posix.join(config.build.assetsSubDirectory, _path);
}

const webpackConfig = merge(baseWebpackConfig, {
  mode: 'production',
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, '../../dist'),
    filename: assetsPath('js/[name].js'),
    chunkFilename: assetsPath('js/[id].js'),
  },
  plugins: [
    new WebpackBar({
      name: 'webpack.PROD',
      profile: true,
      minimal: false,
    }),
    // extract css into its own file
    new MiniCssExtractPlugin({
      filename: assetsPath('css/[name].css'),
    }),
    // Compress extracted CSS. We are using this plugin so that possible
    // duplicated CSS from different components can be deduped.
    new OptimizeCSSPlugin(),
    // copy custom static assets
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, '../static'),
        to: config.build.assetsSubDirectory,
        ignore: ['.*'],
      },
    ]),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],
  optimization: {
    removeAvailableModules: true,
    removeEmptyChunks: true,
    mergeDuplicateChunks: true,
    namedChunks: true,
    runtimeChunk: {
      name: 'manifest',
    },
    splitChunks: {
      cacheGroups: {
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all',
        },
      },
    },
  },
});

module.exports = webpackConfig;
