/* eslint-disable import/no-extraneous-dependencies */
// This is the webpack config used for unit tests.
const webpack = require('webpack');
const merge = require('webpack-merge');
const WebpackBar = require('webpackbar');
const baseConfig = require('./webpack.base.conf');

const webpackConfig = merge(baseConfig, {
  devtool: '#inline-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"testing"',
      },
    }),
    new WebpackBar({ name: 'webpack.TEST' }),
  ],
});

// no need for app entry during tests
delete webpackConfig.entry;

module.exports = webpackConfig;
