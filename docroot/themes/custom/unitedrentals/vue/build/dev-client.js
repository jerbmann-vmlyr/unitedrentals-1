/* eslint-disable */
require('eventsource-polyfill');

var hmrClient = (function () {
  try {
    require('./dev-server-https-certs.js');
    return require('webpack-hot-middleware/client?noInfo=true&reload=true&path=https://localhost:8443/__webpack_hmr');
  }
  catch (e) {
    console.log(`Failed to create HTTPS HMR client: `, e);
    return require('webpack-hot-middleware/client?noInfo=true&reload=true&path=http://127.0.0.1:8080/__webpack_hmr');
  }
})();

hmrClient.subscribe(function (event) {
  if (event.action === 'reload') {
    window.location.reload();
  }
});
