module.exports = {
  build: {
    env: {
      NODE_ENV: '"production"',
    },
    assetsSubDirectory: 'vue',
    assetsPublicPath: '/themes/custom/unitedrentals/dist/',
  },
  dev: {
    env: {
      NODE_ENV: '"development"',
    },
    port: 8080,
    assetsSubDirectory: 'static',
    assetsPublicPath: 'https://localhost:8443/',
    proxyTable: {
      '/RESTAdapter/unitedrentals/*': {
        logLevel: 'debug',
        target: 'https://localhost:8443',
        changeOrigin: true,
        router: {
          'localhost:8443': 'http://34.195.98.100:50000',
        },
      },
    },
    // CSS Sourcemaps off by default because relative paths are "buggy"
    // with this option, according to the CSS-Loader README
    // (https://github.com/webpack/css-loader#sourcemaps)
    // In our experience, they generally work as expected,
    // just be aware of this issue when enabling this option.
    cssSourceMap: false,
  },
};
