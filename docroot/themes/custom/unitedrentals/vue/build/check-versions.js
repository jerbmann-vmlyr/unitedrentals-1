/* eslint-disable no-console, import/no-extraneous-dependencies */
const chalk = require('chalk');
const semver = require('semver');
const { engines } = require('../package');
const { execSync } = require('child_process');

function getEngineVersion(engine) {
  try {
    return execSync(`${engine} -v`).toString().trim();
  }
  catch (e) {
    console.log(chalk.red(`\nThis package requires ${chalk.yellow(engine)} to be installed!\n`));
    process.exit(1);
  }
}

Object.keys(engines).forEach((engineName) => {
  const versionExpected = engines[engineName];
  const versionInstalled = getEngineVersion(engineName);

  if (!semver.satisfies(versionInstalled, versionExpected)) {
    console.log(chalk.blue(`\nThis package requires ${engineName} version ${chalk.yellow(versionExpected)},`));
    console.log(chalk.blue(`and you only have version ${chalk.yellow(versionInstalled)} installed.`));
    console.log(chalk.red(`You'll need to update ${engineName} to continue.\n`));
    process.exit(1);
  }
});

console.log(chalk.green('Engine version checks look good 👌'));
