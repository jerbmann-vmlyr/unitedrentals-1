/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const threadLoader = require('thread-loader');
const config = require('./config');

const threadPoolOptions = {
  name: 'loaderWorkerPool',
};

threadLoader.warmup(threadPoolOptions, [
  // modules to preload
  'eslint-loader',
  'babel-loader',
  'ts-loader',
  'vue-loader',
]);


const isProd = process.env.NODE_ENV === 'production';

function fromProjectRoot(dir) {
  return path.join(__dirname, '..', dir);
}

function assetsPath(_path) {
  const assetsSubDirectory = isProd
    ? config.build.assetsSubDirectory
    : config.dev.assetsSubDirectory;
  return path.posix.join(assetsSubDirectory, _path);
}


module.exports = {
  entry: {
    app: ['./src/main.js'],
  },
  output: {
    path: config.build.assetsRoot,
    filename: '[name].js',
    publicPath: isProd ? config.build.assetsPublicPath : config.dev.assetsPublicPath,
  },
  resolve: {
    extensions: ['.js', '.vue', '.json', '.ts'],
    alias: {
      '@': fromProjectRoot('src'),
      vue$: 'vue/dist/vue.esm.js',
      'vuex-persist$': 'vuex-persist/dist/umd/index.js',
    },
  },
  externals: {
    leaflet: 'L',
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: 'raw-loader',
      },
      {
        test: /\.ts$/,
        loader: path.resolve(__dirname, 'tslint-loader'),
        enforce: 'pre',
        exclude: /(node_modules)/,
        options: {
          formatter: 'stylish',
          failOnHint: true,
          configFile: fromProjectRoot('tslint.json'),
          tsConfigFile: fromProjectRoot('src/tsconfig.json'),
        },
      },
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        include: [fromProjectRoot('src'), fromProjectRoot('test')],
        exclude: /node_modules/,
        options: {
          appendTsSuffixTo: [/\.vue$/],
          transpileOnly: true,
          experimentalWatchApi: true,
          configFile: path.resolve(__dirname, '..', 'src', 'tsconfig.json'),
        },
      },
      {
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        exclude: /node_modules/,
        options: {
          configFile: path.resolve(__dirname, '..', '.eslintrc.js'),
          quiet: isProd,
          failOnError: true,
        },
      },
      {
        test: /\.js$/,
        loader: 'babel-loader?cacheDirectory=true',
        exclude: file => (
          /node_modules/.test(file)
          && !/\.vue\.js/.test(file)
        ),
      },
      {
        test: /\.vue$/,
        use: !isProd
          ? ['vue-loader']
          : [{ loader: 'thread-loader', options: threadPoolOptions }, 'vue-loader'],
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        query: {
          limit: 10000,
          name: assetsPath('img/[name].[hash:7].[ext]'),
        },
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        query: {
          limit: 10000,
          name: assetsPath('fonts/[name].[hash:7].[ext]'),
        },
      },
      {
        test: /\.css$/,
        use: [
          isProd ? MiniCssExtractPlugin.loader : 'vue-style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              includePaths: [
                fromProjectRoot('../components/_patterns/00-base'),
              ],
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
    new ForkTsCheckerWebpackPlugin({
      tsconfig: path.resolve(__dirname, '..', 'src', 'tsconfig.json'),
      async: !isProd, // block the compiler when building for production
      watch: fromProjectRoot('src'),
      formatter: 'codeframe',
      vue: true,
    }),
  ],
};
