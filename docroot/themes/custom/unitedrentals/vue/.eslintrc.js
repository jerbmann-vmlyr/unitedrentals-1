// http://eslint.org/docs/user-guide/configuring/
module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: 'module',
  },
  extends: [
    /* TODO: Set vue-eslint extension level to "strongly-recommended" as part of next PER" */
    'plugin:vue/essential',
    '@vue/typescript',
    '../.eslintrc',
  ],
  // required to lint *.vue files
  plugins: [
    'vue',
  ],
  env: {
    browser: true,
  },
  globals: {
    Promise: true,
    process: true,
    google: true,
    expect: true,
    test: true,
    describe: true,
    jest: true,
    it: true,
    xit: true,
    beforeEach: true,
    beforeAll: true,
  },
  overrides: [
    {
      files: ['*.vue'],
      rules: {
        indent: 'off',
        /* TODO: Turn this back on in follow-up PR */
        'max-len': 'off',
      },
    },
  ],
  // add your custom rules here
  rules: {
    // Override silly things that throw errors instead of warnings in Development Mode
    'no-debugger': process.env.NODE_ENV === 'development' ? 1 : 2,
    'no-console': process.env.NODE_ENV === 'development' ? 1 : 2,
    'no-unused-vars': process.env.NODE_ENV === 'development' ? 1 : 2,
    'no-unreachable': process.env.NODE_ENV === 'development' ? 1 : 2,


    /*

    Mid Project Drupal radically altered its ESLint rules, introducing +2300 Errors. Hundreds
    have been fixed, but a few hundred more have been overridden due to time constraints
    (as the remaining errors must be fixed by hand).

    The following rules override Drupal 8.4 ESLint rules either because our architecture demands it
    (as is the case for No Shadow with Vuex) or because we're 'too far in' (as is the case for
    absolute imports coming last) and don't have the time to manually correct hundreds of errors.

     */

    // Allow for our Webpack configuration to import with relative dir '@/' (a Vue-CLI convention)
    'import/no-unresolved': 0,

    // Allow shadowing for Vuex Modules (The pattern requires it.)
    'no-shadow': 0,

    // Allow variables, classes, and functions to be used before they're defined
    'no-use-before-define': 0,

    // Allow import w/o extension, because we have Webpack (a Vue-CLI convention)
    'import/extensions': 0,

    // Allow bitwise operators. No one's encouraged to use them, but they're allowed.
    'no-bitwise': 0,

    // Allow absolute imports in any order (because we're too far in and don't have time to correct)
    'import/first': 0,

    // Allow longer max-line length (because we're too far in and don't have time to correct)
    'max-len': ['error', {'code': 140, 'comments': 140}],

    // Don't Warn anonymous fn's
    'func-names': 0,

    // Don't Warn for plus-plus
    'no-plusplus': 0,

    'object-curly-newline': ['error', {
      consistent: true,
    }],

    'no-else-return': 'off',

    'implicit-arrow-linebreak': 'off',

    'no-confusing-arrow': 'off',
    'function-paren-newline': 'off',

    /* TODO: Turn on the vue/* eslint rules as part of next PR */

    // 'vue/component-name-in-template-casing': ['error', 'kebab-case'],

    // 'vue/this-in-template': ['error', 'never'],

    // 'vue/html-indent': ['error', 2, {
    //   attribute: 1,
    //   baseIndent: 1,
    //   closeBracket: 0,
    //   alignAttributesVertically: true,
    //   ignores: [],
    // }],

    // 'vue/html-self-closing': ['error', {
    //   html: {
    //     void: 'never',
    //     normal: 'any',
    //     component: 'always',
    //   },
    //   svg: 'never',
    //   math: 'never',
    // }],

    // 'vue/max-attributes-per-line': 'off',

    // 'vue/script-indent': ['error', 2, {
    //   baseIndent: 1,
    // }],

    // 'vue/singleline-html-element-content-newline': 'off',
  }
};
