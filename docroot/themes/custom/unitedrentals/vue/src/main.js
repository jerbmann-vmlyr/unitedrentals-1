/* eslint-disable import/order */
import 'es6-promise/auto';
import '@babel/polyfill'; // polyfills IE 11 (some plugins use Object.assign)
import 'reflect-metadata';
import Vue from 'vue';
import { get, startsWith } from 'lodash';

import routes from '@/routes';
import store, { enableAsyncStorage } from '@/store';
import { protectedRoutes, protectedNamespaces } from '@/data/route-guards';
import App from './app';

/**
 * Plugins
 */
import VueCarousel from 'vue-carousel';
import VueMLCarousel from 'vueml-carousel';
import VueChart from 'vue-chartjs';
import * as VueGoogleMaps from 'vue2-google-maps';
import GmapCustomMarker from '@/components/map/gmap-custom-marker.vue';
import GmapCluster from 'vue2-google-maps/dist/components/cluster';
import Vuelidate from 'vuelidate';
import VuePaginate from 'vue-paginate';
import VueRouter from 'vue-router';
import Qs from 'qs';
import quark from 'quark';
import 'quark/dist/quark.css';
import VTooltip from 'v-tooltip';
import VueFindChild from '@/lib/plugins/vue-find-child';
import VueGetAllChildren from '@/lib/plugins/vue-get-all-children';
import VueGtm from 'vue-gtm';
import VueValidateChildren from '@/lib/plugins/vue-validate-children';
import VueI18nMapLabels from '@/lib/plugins/vue-i18n-map-labels';
import { UrGtmPlugin } from '@/lib/google-tag-manager';
import VueTimeago from 'vue-timeago';
import VuePopup from '@/utils/popup';
import lineClamp from 'vue-line-clamp';
import AsyncComputed from 'vue-async-computed';

Vue.use(AsyncComputed);
Vue.use(VuePopup);
Vue.use(VueCarousel);
Vue.use(VueMLCarousel);
Vue.use(VueChart);
Vue.use(quark);

const makeVueGoogleMapsConfig = () => {
  // The /locations page is being de-vued, so avoid loading our own Gmap API
  // @todo this is temporary spot for this
  const isLocationsPage = startsWith(window.location.pathname, '/locations');
  return {
    load: isLocationsPage ? false : {
      client: 'gme-unitedrentalsinc',
      libraries: 'places',
      v: '3.37',
    },
  };
};

Vue.use(VueGoogleMaps, makeVueGoogleMapsConfig());
Vue.component('GmapCustomMarker', GmapCustomMarker);
Vue.component('GmapCluster', GmapCluster);
Vue.use(Vuelidate);
Vue.use(VuePaginate);
Vue.use(VTooltip);
Vue.use(VueFindChild);
Vue.use(VueGetAllChildren);
Vue.use(VueValidateChildren);
Vue.use(VueI18nMapLabels);
Vue.use(VueTimeago, {
  name: 'timeago',
  locale: 'en-US',
  locales: {
    'en-US': [
      'just now',
      '%ss ago',
      '%sm ago',
      '%sh ago',
      '%sd ago',
      '%sw ago',
      '%sm ago',
    ],
  },
});
Vue.use(lineClamp);

/**
 * Filters
 */
import filters from '@/lib/filters';

Object.entries(filters).forEach(([name, filter]) => {
  Vue.filter(name, filter);
});

/**
 * Directives
 */
import applyClassesOnHover from '@/lib/directives/apply-classes-on-hover';
import clickOutside from '@/lib/directives/click-outside';
import openover from '@/lib/directives/openover';
import sticky from '@/lib/directives/sticky';

Vue.directive('apply-classes-on-hover', applyClassesOnHover);
Vue.directive('click-outside', clickOutside);
Vue.directive('openover', openover);
Vue.directive('sticky', sticky);

/**
 * Routing
 */

import { sync } from 'vuex-router-sync';

Vue.use(VueRouter);

const mode = window.location.pathname.includes('locations') ? 'history' : null;

const router = new VueRouter({
  routes,
  mode,
  parseQuery: Qs.parse,
  stringifyQuery(query) {
    return Qs.stringify(query, { addQueryPrefix: true });
  },
});

const deepCheck = (to, from, parent) => {
  // We have to be looking for more than just a base route...
  if (protectedRoutes.indexOf(to.name) > -1) { // this is a deep link attempt
    // Check if this is a deep link attempt
    // pass back the login route, complete with ReturnTo value
    const deepLink = `${window.location.origin}${parent}#${to.fullPath}`;
    localStorage.setItem('deepLink', deepLink);
    return `${window.location.origin}${drupalSettings.ur.sign_in_register_path}`;
  }
  return false;
};

router.beforeEach((to, from, next) => {
  // Check to see if the user is logged in
  if (get(drupalSettings, ['user', 'uid'], '') === 0) {
    // Is this a protected namespace?
    const found = protectedNamespaces.filter(namespace => window.location.pathname.indexOf(namespace) > -1);
    if (found.length) {
      const redirectUrl = deepCheck(to, from, window.location.pathname);
      if (redirectUrl) { // If we get a route string...
        window.addEventListener('DOMContentLoaded', () => {
          window.location.href = redirectUrl;
        });
        return;
      }
      // If it's a redirect, cancel the next callback, otherwise, just go...
      return redirectUrl ? next(false) : next();
    }

    next();
  }
  else { // If the user is logged in they can go wherever they want
    const willRedirect = from.fullPath === '/' && localStorage.getItem('deepLink');
    if (willRedirect) {
      const deepLink = localStorage.getItem('deepLink');
      localStorage.removeItem('deepLink');
      console.log('DEEP LINK FOUND: waiting for page load'); // eslint-disable-line
      window.addEventListener('load', () => {
        console.log('DEEP LINK FOUND: refreshing now'); // eslint-disable-line
        window.location.href = deepLink;
      });
    }
    return willRedirect ? next(false) : next();
  }
});

/**
 * If the Vuex store is using IndexedDB to store state (i.e. if session storage
 * is enabled), setup a navigation guard that makes sure async state from storage
 * is loaded into state before continuing.
 * The `storageReady` event will be emitted by the VuexPersistPlugin we wire
 * up in store/index.ts. Without this, there are issues with reactivity.
 */
if (enableAsyncStorage) {
  let storageReady = false;
  router.beforeEach((to, from, next) => {
    if (!storageReady) {
      store._vm.$root.$once('storageReady', () => {
        storageReady = true;
        next();
      });
    }
    else {
      next();
    }
  });
}

// TODO This is not a long-term fix. Adding the bogus GTM variable to remove error message.
Vue.use(VueGtm, { id: 'GTM-xxxxxxx', enabled: true, debug: false, vueRouter: router });
Vue.use(UrGtmPlugin, { store, debug: true });

// Sync up the App's current $route with a module in the Vuex store named 'route'
sync(store, router, {
  moduleName: 'route',
});

// UR-4089. Vue Routes aren't triggered in IE 11 with Manual Hash Changes.
// In our case, this means the Workplace Dashboard and Equipment
// links (rendered by Drupal) _do not update Vue Router_.
// https://github.com/vuejs/vue-router/issues/1849
// To fix this, we manually listen for Hash Change Events (ie11 only) and shove the new hash into Vue Router.
// As of July 2018, a PR to fix Vue Router has not been accepted.
// https://github.com/vuejs/vue-router/pull/1988/commits/23bfa089a0d3933896baae77d59fcfd229cf364d
const isIE11 = window.navigator.userAgent.indexOf('rv:11.0') !== -1;
const isEdge = window.navigator.userAgent.indexOf('Edge') !== -1;
if (isIE11 || isEdge) {
  window.addEventListener('hashchange', () => {
    const route = window.location.hash.split('#')[1];
    window._app.$router.push(route);
  });
}

/**
 * Configuration
 */
Vue.config.productionTip = false;
Vue.config.devtools = new RegExp('dev|qa|stage|docksal').test(window.location.host);

/**
 * Instance
 */
window._app = new Vue({
  el: '#app',
  ...App,
  router,
  store,
});
