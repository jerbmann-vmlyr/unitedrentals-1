<template>
  <div class="po-number-container" :class="requirePo === 'V' ? 'validated-po-required' : ''">
    <label class="po-label">{{ $i18n('checkout.poNumber') }}</label>
    <div class="po-inputs">
      <q-filterable-dropdown
        v-if="requirePo === 'V'"
        :has-button="false"
        :has-down-arrow="true"
        :placeholder="$i18n('checkout.poSelect')"
        :results="poNumberOptions"
        :value="value"
        :display-value="value"
        @OptionSelect="setPoNumber"
        @blur="setPoNumber">
      </q-filterable-dropdown>

      <ur-text-input
        v-else
        class="po-text-input"
        :label="$i18n('checkout.poNumber')"
        :placeholder="inputPlaceholder"
        :value="value"
        @change="setPoNumber($event)"
        @blur="setPoNumber($event)">
      </ur-text-input>

      <label v-if="!validationTypes.validatedPoRequired && poFormats.length" class="m-t-5 m-b-0 fw-light c-united-gray">
        {{ formattedPoInstructions }}
      </label>
    </div>
    <ur-error-label ref="errorLabel" v-if="$v.$invalid && vuelidateReady" :message="errorLabel"></ur-error-label>
  </div>
</template>
<script>
  // Import Components
  import UrTextInput from '@/components/common/form/ur-text-input';
  import UrErrorLabel from '@/components/common/form/ur-error-label';

  // Import Modules
  import { mapActions, mapState, mapGetters } from 'vuex';
  import types from '@/store/types';
  import moment from 'moment';
  import { debounce } from 'lodash';

  export default {
    components: {
      UrTextInput,
      UrErrorLabel,
    },
    computed: {
      ...mapGetters({
        getPoNumbersByAccountId: types.accountPoNumbers.getPoNumbersByAccountId,
      }),
      ...mapState({
        allAccounts: state => state.accounts.all,
        /*
        PO Formats are returned from one of the purchase order APIs. We filter
        responses to exclude both expired and future PO Formats, and oddballs missing a pattern.
        */
        poFormats(state) {
          return state.accountPoNumbers.formats.filter(({pattern, expirationDate, effectiveDate}) => { // eslint-disable-line
            if (!pattern) {
              return false;
            }

            const now = moment().format('X');

            return now > effectiveDate && now < expirationDate;
          });
        },
      }),
      value() {
        return this.poNumber;
      },
      /*
      PO Numbers are only applicable to users who have Validated PO Numbers.
      These are their options, and the user selects them from a Dropdown menu,
      not a text input.
      */
      poNumbers() {
        const pos = this.getPoNumbersByAccountId(this.accountId);

        // Sort PO numbers alphabetically like TC does.
        if (pos.length) {
          pos.sort((a, b) => {
            if (a.poNumber < b.poNumber) {
              return -1;
            }

            return a.poNumber > b.poNumber ? 1 : 0;
          });
        }

        return pos;
      },
      /*
      PO Number options are only applicable to Validated PO Numbers.
      The UI stubs in a "TBD" option for all users, not returned by the API.
      */
      poNumberOptions() {
        const defaultOptions = [{ label: 'TBD (add a PO # later)', value: 'TBD' }];
        const poNumberOptions = (this.poNumbers || []).map(({ poNumber }) => ({
          label: poNumber,
          value: poNumber,
        }));

        return defaultOptions.concat(poNumberOptions);
      },
      /*
      Error Labels for the PO Number field are a function of the Validation Type.
      */
      errorLabel() {
        const {
          formattedPoRequired,
          formattedPoNotRequired,
        } = this.validationTypes;

        let errorLabel = '';

        if (formattedPoRequired || formattedPoNotRequired) {
          errorLabel = this.$i18n('checkout.poNumberFormat');
        }
        else {
          // Validated or plain ol' required PO Numbers get the same error label
          errorLabel = `${this.$i18n('checkout.poNumber')} ${this.$i18n('e.required')}`;
        }

        return errorLabel;
      },
      /*
      Generates specific placeholder text for PO "Input" fields based
      off the user having PO Formats or not.
       */
      inputPlaceholder() {
        const { formattedPoRequired, formattedPoNotRequired } = this.validationTypes;
        const isFormatted = formattedPoNotRequired || formattedPoRequired;
        const i18nKey = isFormatted ? 'checkout.poEnterAFormattedPo' : 'checkout.poEnterAPo';

        return this.$i18n(i18nKey);
      },

      /*
      PO Validation types. These are a function a user's account and
      data from the purchase orders API.
      Any user will fall into one of these 5 PO Number Validation Types.
       */
      validationTypes() {
        return {
          anyPoRequired: this.requirePo === 'Y' && !this.poFormats.length,
          anyPoNotRequired: this.requirePo === 'N' && !this.poFormats.length,
          formattedPoRequired: this.requirePo === 'Y' && this.poFormats.length,
          formattedPoNotRequired: this.requirePo === 'N' && this.poFormats.length,
          validatedPoRequired: this.requirePo === 'V',
        };
      },
      /*
      Given a collection of a user's PO Formats, returns instructional text. E.g.
      "Format is ##@@-****, or #### (example: 11AA-A1A1, or 1111)."
       */
      formattedPoInstructions() {
        const formatSymbols = this.poFormats.map(({ poFormat }) => poFormat).join(', ')
          .replace(/,([^,]*)$/, `, ${this.$i18n('dashboard.or')}$1`) // replace last ", " with ", or "
          .replace(/\$/g, '@'); // Replace all "$" w/"@" (per design...)

        const formatExamples = formatSymbols
          .replace(/#/g, 1)
          .replace(/@/g, 'A')
          .replace(/\*/g, getToggledChar);

        return `
          ${this.$i18n('checkout.formatIs')} ${formatSymbols}
          (${this.$i18n('workplace.example').toLowerCase()}: ${formatExamples}).
        `;
      },
    },
    data() {
      return {
        vuelidateReady: false,
        poErrorString: '',
        vMessages: {
          value: '',
        },
      };
    },
    methods: {
      // Import Vuex actions
      ...mapActions({
        fetchPoNumbers: types.accountPoNumbers.fetchNumbers,
        fetchPoFormats: types.accountPoNumbers.fetchFormats,
      }),
      setPoNumber(event) {
        let val = event;
        if (typeof val === 'object') {
          if (typeof val === 'object') {
            val = val.target ? val.target.value : val.value;
          }
        }
        this.$emit('changePo', val);
      },
      setNoPo() {
        this.noPoRequired = !this.noPoRequired;
        this.$emit('noPoChanged', this.noPoRequired);
      },
    },
    name: 'po-number',
    props: {
      accountId: String,
      poNumber: String,
      requirePo: String,
    },
    mounted() {
      if (this.requirePo === 'V') {
        this.fetchPoNumbers(this.accountId);
      }
      if (this.requirePo === 'Y') {
        this.fetchPoFormats(this.accountId);
      }
      this.vuelidateReady = true;

      this.setPoNumber = debounce(this.setPoNumber, 500);
    },
    validations: {
      /*
      Validation Rules for the PO Number are a function of
      the user's Validation Type.
       */
      value: {
        isValid(value) {
          const {
            anyPoRequired,
            anyPoNotRequired,
            formattedPoRequired,
            formattedPoNotRequired,
            validatedPoRequired,
          } = this.validationTypes;

          if (anyPoNotRequired) {
            return true;
          }

          // Validated POs are selected from the filterable dropdown.
          // If the user made a selection then it is valid.
          // If "any po" is required then any value is valid.

          const valueExists = !!value;

          if (validatedPoRequired || anyPoRequired) {
            this.$emit('poError', { type: 'poformat', flag: !valueExists });
            return valueExists;
          }

          // Formattable PO Numbers must match the formats
          // available to the user, if the PO is required at all.
          const valueMatchesAnyFormat = this.poFormats.some(({ pattern }) => new RegExp(pattern).test(value));

          if (formattedPoRequired) {
            this.$emit('poError', { type: 'poformat', flag: !valueMatchesAnyFormat });
            return valueMatchesAnyFormat;
          }

          if (formattedPoNotRequired) {
            const flag = valueExists ? valueMatchesAnyFormat : true;
            this.$emit('poError', { type: 'poformat', flag: !flag });
            return flag;
          }
        },
      },
    },
  };
  /*
  Helper functions for Formatted PO Instructions
  */
  const charToggler = () => {
    let letterA = false;

    return () => {
      letterA = !letterA;
      return letterA ? 'A' : '1';
    };
  };

  const getToggledChar = charToggler(); // returns A then 1 then A then 1 ad nauseam

</script>
