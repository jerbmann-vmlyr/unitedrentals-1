import Vue from 'vue';
import { uid } from '@/utils';
import { replace } from 'lodash';
import { SearchByTermFilter, ListFilterType } from '@/lib/listings';

export default Vue.extend({
  name: 'searchByTerm',
  props: {
    hasGroupLabel: {
      default: true,
      required: false,
      type: Boolean,
    },
    isCollapsable: {
      required: false,
      type: Boolean,
      default: false,
    },
    isExpanded: {
      required: false,
      type: Boolean,
      default: true,
    },
    itemKey: {
      required: true,
      type: String,
    },
    itemLexicon: {
      required: true,
      type: Function,
    },
    items: {
      required: true,
      type: Array,
    },
    maxResults: {
      required: false,
      type: Number,
      default: 5,
    },
    placeholder: String,
    theme: {
      type: String,
      default: 'normal',
      validator(theme) {
        return theme === 'normal' || theme === 'global';
      },
    },
    matchingTermClickCallback: {
      required: false,
      type: Function,
    },
  },
  data() {
    return {
      hasFocus: false,
      searchText: '',
    };
  },
  computed: {
    hasOptions(): boolean {
      return this.matchingTerms.length > 0;
    },

    hasSearchText(): boolean {
      return this.searchText.length > 0;
    },

    isSearchExpanded() {
      if (!this.isCollapsable) {
        return true;
      }

      return this.isExpanded;
    },

    isNormalTheme(): boolean {
      return this.theme === 'normal';
    },

    /**
     * Given user input (searchText) and the item lexicon, this method computes filter matchingTerms
     * for the user to select from. Sorted by the number of items in which a term appears.
     */
    matchingTerms(): SearchByTermFilter[] {
      if (this.searchText.length < 2) {
        return [];
      }

      const searchText = this.searchText.toLowerCase();

      const terms: { [term: string]: string[] } =
        /* A mapping of :items to a collection of searchable terms. */
        this.items.map(item => ({
          itemKeyName: this.itemKey,
          itemKeyValue: `${item[this.itemKey]}`,
          availableTerms: this.itemLexicon(item)
            /* Sanitize the terms */
            .map(term => `${term}`.toLowerCase())
            .filter(term => term.length >= 2),
        }))
        // Filter for items whose terms contain the search text:
        .filter(item => item.availableTerms.some(term => term.includes(searchText)))
        // Modify the remaining items' terms to only contain matching terms.
        .map(item => ({
          ...item,
          matchingTerms: item.availableTerms
            .filter(term => term.includes(searchText))
            .map(term => term.replace(/,/g, '')),
        }))
        // Change the structure from [{catClass: '', terms: ['term']}] to {'term': ['catClass']}
        .reduce(
          (terms, item) => {
            item.matchingTerms.forEach(term => {
              if (typeof terms[term] === 'undefined') {
                terms[term] = [];
              }
              terms[term].push(item.itemKeyValue);
            });
            return terms;
          },
          {},
        );

      // At this stage, `terms` is an object whose keys are words from equipment titles & highlights that match
      // the user's search input.  The value for these keys is an array of catclasses containing that term.
      // Now convert the terms object back into an array for sorting (by catclass count).
      return Object.entries(terms)
        .map(([term, matchingItemKeyValues]) => ({
          type: ListFilterType.SearchByTerm,
          id: uid(),
          itemKey: this.itemKey,
          matchingItemKeyValues,
          query: this.searchText,
          term,
          asFunction: item => matchingItemKeyValues.includes(item[this.itemKey]),
        }))
        .sort((a, b) => {
          if (a.matchingItemKeyValues.length === b.matchingItemKeyValues.length) return 0;
          return a.matchingItemKeyValues.length > b.matchingItemKeyValues.length ? -1 : 1;
        })
        // Return only the top 5 matchingTerms
        .slice(0, this.maxResults);
    },

    matchingTermCount(): number {
      return this.matchingTerms.reduce((sum, match) => sum + match.matchingItemKeyValues.length, 0);
    },
  },

  methods: {
    /**
     * Formats the item name text so that the search term is highlighted where the search text
     * is found in the item name.
     */
    matchingTermLabel(item: SearchByTermFilter) {
      const activeSearchText = replace(item.term, this.searchText, `<span class="active-search-text">${this.searchText}</span>`);
      return `${activeSearchText} (${item.matchingItemKeyValues.length})`;
    },
    clearSearchText() {
      this.searchText = '';
    },
    onMatchingTermClick(term?: SearchByTermFilter) {
      // In the case that the user hit enter, we gotta make
      // sure we're emitting a valid SearchByTermFilter
      if (!!term && typeof term.asFunction === 'function') {
        if (typeof this.matchingTermClickCallback === 'function') {
          this.matchingTermClickCallback(term);
        } else {
          this.$emit('matchingTermClick', term);
        }
        this.clearSearchText();
      }
    },
    setFocus(value) {
      setTimeout(() => {
        this.hasFocus = value;
      }, value ? 0 : 250);
      if (value) {
        this.$emit('focus');
      }
      else {
        this.$emit('blur');
      }
    },
  },
});
