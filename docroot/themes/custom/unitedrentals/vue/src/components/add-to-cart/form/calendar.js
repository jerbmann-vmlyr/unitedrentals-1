import moment from 'moment';
import { get, isEmpty } from 'lodash';
import { dateFormat, dateTime, today } from '@/utils';
import { mapActions, mapState } from 'vuex';
import types from '@/store/types';

export default {
  data() {
    return {
      minDate: moment(),
    };
  },
  methods: {
    ...mapActions({
      updateDefaultStartDate: types.user.defaultStartDate,
      updateDefaultEndDate: types.user.defaultEndDate,
      updateFormField: types.cartItemForm.updateFormField,
    }),
    nextDayFor(moment) {
      return moment.add(this.nextDayBranchIsOpen, 'days');
    },
    checkWeekends() {
      if (this.branchIsOpenWeekends || this.startDateIsWeekday) return this;

      if (this.startDateIsSaturday && !this.branchIsOpenSaturday) {
        this.updateFormField({ startDate: this.startDate.add(1, 'days') });
      } // dispatch action to update?

      if (this.startDateIsSunday && !this.branchIsOpenSunday) {
        this.updateFormField({ startDate: this.startDate.add(1, 'days') });
      } // dispatch action to update?

      return this;
    },
    checkMinimumDate() {
      if (!this.minDate.isSameOrBefore(this.startDate)) {
        // (SK): Commenting this out because it sporadically sets the start date to today's date when it should -
        // remain blank
        //
        // this.updateFormField({ startDate: this.minDate });
      }
      return this;
    },
  },
  watch: {
    branch() {
      // Default time = UTC (zulu) Time
      if (this.noBranch) return moment();

      const time = moment.utc().format(dateFormat.iso);

      // Hours to adjust (4 hours from zulu to eastern, plus branch offset)
      let offset = 400 + (this.branch.timeOffset * -1);

      // Add daylight savings time if applicable
      if (!this.branch.daylightSavings) offset += 100;

      // default offset = UTC ==> only subtract
      const adjustedMoment = moment(time).subtract((offset / 100), 'hour');

      /**
       * If it's "after hours" in the time zone of
       * the branch the minimum date is tomorrow.
       * Otherwise it's still today
       */
      const lastHour = dateTime.toDecimal(get(this.branch, ['weekdayHours', 'close'], 17));

      if (adjustedMoment.hour() >= lastHour) {
        this.minDate = this.nextDayFor(adjustedMoment);
      }
      this.checkWeekends().checkMinimumDate();
    },

    branchIsOpenSaturday() {
      this.checkWeekends().checkMinimumDate();
    },
    branchIsOpenSunday() {
      this.checkWeekends().checkMinimumDate();
    },
  },
  computed: {
    ...mapState({
      startDate: state => state.cartItemForm.startDate,
      endDate: state => state.cartItemForm.endDate,
    }),
    noBranch() {
      return isEmpty(this.branch);
    },
    startDateIsSaturday() {
      return (this.startDate.isoWeekday() === 6);
    },
    startDateIsSunday() {
      return (this.startDate.isoWeekday() === 7);
    },
    startDateIsWeekday() {
      return !(this.startDateIsWeekend);
    },
    startDateIsWeekend() {
      return (this.startDateIsSunday && this.startDateIsSaturday);
    },
    branchIsOpenWeekends() {
      return (this.branchIsOpenSaturday && this.branchIsOpenSunday);
    },
    nextDayBranchIsOpen() {
      if (today.isWeekday() && today.isNot('Friday')) return 1;

      if (today.day('Friday') && this.branchIsOpenSaturday) return 1;

      if (today.day('Saturday') && this.branchIsOpenSunday) return 1;

      return today.nextWeekday();
    },
    branchIsOpenSaturday() {
      return !this.noBranch && !isEmpty(this.branch.saturdayHours);
    },
    branchIsOpenSunday() {
      return !this.noBranch && !isEmpty(this.branch.sundayHours);
    },
  },
};
