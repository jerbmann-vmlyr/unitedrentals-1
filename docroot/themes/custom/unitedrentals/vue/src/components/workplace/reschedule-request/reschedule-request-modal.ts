import Vue from 'vue';
import { mapActions, mapGetters, mapState } from 'vuex';
import { WorkplaceRouteNames } from '@/lib/workplace';
import types from '@/store/types';
import { WorkplaceRescheduleRequestState } from '@/store';
import ModalFrame from '@/components/common/ur-modal-frame.vue';
import UrErrorLabel from '@/components/common/form/ur-error-label.vue';
import UrLoadingSpinner from '@/components/common/ur-loading-spinner.vue';
import DatetimeSelect from './datetime-select.vue';
import EmailRecipients from './email-recipients.vue';
import EquipmentSelectList from './equipment-select-list.vue';
import LocationDetails from './location-details/location-details.vue';

export default Vue.extend({
  name: 'rescheduleRequestModal',
  components: {
    DatetimeSelect,
    EmailRecipients,
    EquipmentSelectList,
    LocationDetails,
    ModalFrame,
    UrErrorLabel,
    UrLoadingSpinner,
  },
  mounted() {
    this.urModal.direct = false;
    // load tcUser into store for sendPickupRequest
    this.fetchTcUser();
    if (this.equipmentForCurrentAccount.length > 0) {
      this.reset();
      this.initializeRescheduleRequest();
    }
  },
  data() {
    return {
      editingLocationDetails: false,
      requestSubmissionInProgress: false,
      requestFetchingInProgress: false,
      submissionError: '',
      bus: new Vue(),
    };
  },
  inject: ['urModal'],
  computed: {
    ...mapState <{ workplaceRescheduleRequest: WorkplaceRescheduleRequestState }>({
      requestIsInitialized: ({workplaceRescheduleRequest}) => workplaceRescheduleRequest.requestIsInitialized,
      tcUser: state => (state as  any).tcUser,
    }),
    ...mapGetters({
      accountId: types.workplace.rescheduleRequest.accountId,
      accountName: types.workplace.rescheduleRequest.accountName,
      atLeastOneEquipmentIdSelected: types.workplace.rescheduleRequest.atLeastOneEquipmentIdSelected,
      equipmentForCurrentAccount: types.workplace.equipmentForCurrentAccount,
      isExtension: types.workplace.rescheduleRequest.isExtension,
      isPickup: types.workplace.rescheduleRequest.isPickup,
      transId: types.workplace.rescheduleRequest.transId,
    }),
    enableSubmitButton(): boolean {
      return !this.requestSubmissionInProgress && this.atLeastOneEquipmentIdSelected && this.requestIsInitialized;
    },
    hasSubmissionError(): boolean {
      return !!this.submissionError && this.submissionError.length > 0;
    },
  },
  methods: {
    ...mapActions({
      initializeRescheduleRequest: types.workplace.rescheduleRequest.initializeRescheduleRequest,
      reset: types.workplace.rescheduleRequest.reset,
      sendExtensionRequest: types.workplace.rescheduleRequest.submitExtensionRequest,
      sendPickupRequest: types.workplace.rescheduleRequest.submitPickupRequest,
      fetchTcUser: types.tcUser.fetch,
      fetchEquipmentOnRent: types.workplace.fetchEquipmentOnRent,
    }),
    async runWrappedSubmission<T>(runner: () => Promise<T>): Promise<T | Error> {
      let submissionResult: T | Error;
      try {
        this.submissionError = '';
        this.requestSubmissionInProgress = true;
        this.requestFetchingInProgress = false;
        submissionResult = await runner();
      } catch (error) {
        if (error) {
          if (error.response && typeof error.response.status === 'number') {
            const {status, statusText} = error.response;
            this.submissionError = `${status}: ${statusText}`;
          } else {
            if (error.message) {
              try {
                this.submissionError = JSON.parse(error.message.substring(error.message.lastIndexOf('response:') + 9).trim()).message;
              } catch (e) {
                this.submissionError = error.message.length ?  error.message : this.$i18n('ur.formSubmissionError');
              }
            } else {
              const pos = error.lastIndexOf('response:');
              this.submissionError = pos !== -1 ? JSON.parse((error.substring(pos + 9)).trim()).message : this.$i18n('ur.formSubmissionError');
            }
          }
        } else {
          this.submissionError = this.$i18n('ur.formSubmissionError');
        }
        this.requestSubmissionInProgress = false;
        return error;
      }
      this.requestFetchingInProgress = true;
      // fetch new, uncached results
      await this.fetchEquipmentOnRent(
        { accountId: this.$store.state.user.defaultAccountId, useCache: false });
      this.requestSubmissionInProgress = false;
      this.requestFetchingInProgress = false;
      // Redirect to the confirmation modal
      this.$router.replace({
        name: WorkplaceRouteNames.ConfirmEquipmentReschedule,
        params: {...this.$route.params},
        query: {...this.$route.query},
      });
      return submissionResult;
    },
    async onSubmitExtensionClicked() {
      return this.runWrappedSubmission(() => this.sendExtensionRequest());
    },
    async onSubmitPickupClicked() {
      this.submissionError = '';
      let invalid = false;
      if (this.editingLocationDetails) {
        const saveLocationDetails = () => new Promise((resolve, reject) => {
          this.bus.$emit('saveLocationDetailsFromParent', resolve, reject);
        });
        try {
          await saveLocationDetails();
        } catch (err) {
          invalid = true;
        }
      }
      const saveEmail = () => new Promise((resolve, reject) => {
        this.bus.$emit('saveEmailFromParent', null, resolve, reject);
      });
      try {
        await saveEmail();
      } catch (err) {
        invalid = true;
      }
      if (invalid) return;
      return this.runWrappedSubmission(() => this.sendPickupRequest());
    },
    close() {
      this.urModal.close();
    },
  },
  watch: {
    equipmentForCurrentAccount(is, was) {
      if (Array.isArray(is) && is.length > 0 && !this.requestSubmissionInProgress) {
        this.reset();
        this.initializeRescheduleRequest();
      }
    },
  },
});
