import Vue from 'vue';
import { mapGetters } from 'vuex';
import { replace } from 'lodash';
import types from '@/store/types';
import { SearchHelper, Result } from '@/lib/collections';
import UrQueryLink from '@/components/common/ur-query-link.vue';
import {
  WORKPLACE_EQUIPMENT_FILTERS,
  WorkplaceEquipmentFilterId,
  WorkplaceEquipment,
  WorkplaceRouteNames,
} from '@/lib/workplace';

export default Vue.extend({
  name: 'equipmentSearch',
  components: {
    UrQueryLink,
  },
  data() {
    return {
      hasFocus: false,
      searchFilter: WORKPLACE_EQUIPMENT_FILTERS.find(f => f.id === WorkplaceEquipmentFilterId.Search)!,
      searchText: '',
    };
  },
  computed: {
    ...mapGetters({
      equipment: types.workplace.equipmentForCurrentAccount,
    }),

    hasResults(): boolean {
      return this.searchResults.length > 0;
    },

    searchHelper() {
      return new SearchHelper<WorkplaceEquipment>(
        this.searchFilter.tokenizer,
        eqp => `${eqp.equipmentId}/${eqp.transId}/${eqp.transLineId}`,
        this.equipment,
      );
    },

    searchResults(): Result[] {
      const searchText = this.searchText.toLowerCase();
      return this.searchHelper.search(searchText);
    },

    showResultsPanel(): boolean {
      return this.hasFocus && this.searchText.length >= 2;
    },
  },

  methods: {
    clearSearchText() {
      this.searchText = '';
    },
    /**
     * Formats the item name text so that the search term is highlighted where the search text
     * is found in the item name.
     */
    resultLabel({ isBroad, ...result }: Result) {
      const isActive = this.tokenIsActive(result.token);
      const count = result.keys.length;

      if (!isBroad) {
        const activeSearchText = replace(result.token, this.searchText, `<span class="fw-bold">${this.searchText}</span>`);
        return `
          <span class="fw-light">${activeSearchText} (${count})</span>
          ${isActive ? '<i class="icon icon-check"/>' : ''}
        `;
      }
      else {
        return `
          <span class="fw-light">Search text: <span class="fw-bold">${result.query}</span> (${count})</span>
          ${isActive ? '<i class="icon icon-check"/>' : ''}
        `;
      }
    },

    selectFirstResult($event) {
      if (!this.hasResults || !this.showResultsPanel) return;

      // Add the token from the first search result to the searchFilter.id
      // property of the route's query object
      const [ result ]: Result[] = this.searchResults;
      const currentTokens = this.$route.query[this.searchFilter.id] || [];

      // If the token is already active, do nothing
      if (currentTokens.includes(result.token)) return;

      // Generate a new query object with our new token applied
      // so that we can emulate a tokenClicked event
      const toggledQuery = {
        ...this.$route.query,
        [this.searchFilter.id]: [ ...currentTokens, result.token ],
      };

      // Fire off the token click handler
      this.tokenClicked({ toggledQuery }, result);
      this.setFocus(false);
    },

    setFocus(value) {
      setTimeout(() => this.hasFocus = value, value ? 0 : 250);
    },
    tokenIsActive(token: string) {
      const queryTokens = this.$route.query[this.searchFilter.id] || [];
      return queryTokens.includes(token);
    },
    tokenClicked({ toggledQuery }, { keys }: Result) {
      // If the token clicked only has one matching item,
      // and `goToDetailsOnSingleMatch` is true, redirect to equipment details
      if (this.goToDetailsOnSingleMatch && keys.length === 1) {
        /* TODO: This is a sorta hacky way of getting the equipmentId and transId */
        const [ equipmentId, transId, transLineId ] = keys[0].split('/');
        this.$router.push({
          name: WorkplaceRouteNames.EquipmentDetails,
          params: { equipmentId, transId, transLineId },
          query: toggledQuery,
        });
      }
      // Otherwise, just apply the toggle query object, and go to the equipment
      // listing if `goToListingOnMultiMatch` is true.
      // If we're changing the route name, use `push`; otherwise, use `replace`.
      else {
        this.$router[this.goToListingOnMultiMatch ? 'push' : 'replace']({
          name: this.goToListingOnMultiMatch
            ? WorkplaceRouteNames.EquipmentList
            : this.$route.name,
          query: toggledQuery,
        });
        this.clearSearchText();
      }
    },
  },
  props: {
    isFleet: Boolean,
    goToDetailsOnSingleMatch: Boolean,
    goToListingOnMultiMatch: Boolean,
    placeholder: {
      type: String,
      required: false,
      default: 'Filter by Type, Job or Person',
    },
  },
});
