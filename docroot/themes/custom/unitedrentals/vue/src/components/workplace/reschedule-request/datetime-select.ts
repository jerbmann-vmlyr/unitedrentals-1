import Vue from 'vue';
import * as moment from 'moment';
import DateTime from '@/utils/dateTime';
import { mapActions, mapState, mapGetters } from 'vuex';
import UrCalendar from '@/components/common/ur-calendar.vue';
import UrSelect from '@/components/common/form/ur-select.vue';
import types from '@/store/types';
import { WorkplaceRescheduleRequestState } from '@/store';
import { dateFormat } from '@/utils';

export default Vue.extend({
  name: 'rescheduleRequestDatetimeSelect',
  components: {
    UrCalendar,
    UrSelect,
  },
  mounted() {
    !this.isPickup ? this.mountedExtendingContractDateTime() : this.mountedRequestingPickupDateTime();
  },
  computed: {
    ...mapGetters({
      branchHoursOffset: types.workplace.rescheduleRequest.branchHoursOffset,
      isPickup: types.workplace.rescheduleRequest.isPickup,
      originalStartDateTime: types.workplace.rescheduleRequest.originalStartDateTime,
      originalReturnDateTime: types.workplace.rescheduleRequest.originalReturnDateTime,
    }),
    ...mapState<{ workplaceRescheduleRequest: WorkplaceRescheduleRequestState }>({
      branch: ({ workplaceRescheduleRequest }) => workplaceRescheduleRequest.branch,
      selectedDateTime: ({ workplaceRescheduleRequest }) => workplaceRescheduleRequest.selectedDateTime,
    }),
    isExtendingContract() {
      return !this.isPickup;
    },
    isRequestingPickup() {
      return this.isPickup;
    },
    branchNowRounded() {
      const branchNow = moment(moment.utc().subtract(this.branchHoursOffset, 'hours').format(dateFormat.iso));
      return DateTime.round(branchNow, moment.duration(30, 'minutes'), 'floor');
    },
    originalEndDate() {
      return moment(this.originalReturnDateTime);
    },
    now() {
      return moment();
    },
    tomorrow() {
      return moment().add(1, 'days');
    },
    oneDayAfterOriginalEndDate() {
      const originalEndDate = this.originalEndDate.add(1, 'days');
      originalEndDate
        .hours(this.originalStartDateTime.hours())
        .minutes(this.originalStartDateTime.minutes())
        .seconds(this.originalStartDateTime.seconds());

      return this.nearestHalfHourFor(originalEndDate);
    },
    minDate() {
      if (this.isRequestingPickup) return this.now;

      return this.originalEndDate.isBefore(this.now) ? this.tomorrow : this.oneDayAfterOriginalEndDate;
    },
    selectedDateIsToday() {
      return this.selectedDateTime && this.selectedDateTime.isSame(this.branchNowRounded, 'day');
    },
    dateTimeOptions() {
      const selectedDate = this.selectedDateTime.clone().hours(0).minutes(0).seconds(0);
      return DateTime.chunkify(selectedDate, selectedDate.clone().add(1, 'days')).map(opt => ({
        enabled: !(this.isPickup && this.selectedDateIsToday && opt.isBefore(this.branchNowRounded, 'minutes')),
        isNow: this.selectedDateIsToday && opt.isSame(this.branchNowRounded, 'minutes'),
        iso: opt.clone().toISOString(),
      }));
    },
  },
  methods: {
    ...mapActions({
      setSelectedDateTime: types.workplace.rescheduleRequest.setSelectedDateTime,
    }),
    nearestHalfHourFor(dateTime) {
      return DateTime.round(dateTime, moment.duration(30, 'minutes'), 'floor');
    },
    mountedExtendingContractDateTime() {
      if (!this.isExtendingContract) return this;

      this.originalEndDate.isBefore(this.now) ? this.setOverdueDateTime() : this.setNonOverdueDateTime();

      return this;
    },
    mountedRequestingPickupDateTime() {
      this.setSelectedDateTime(moment(this.branchNowRounded));

      return this;
    },
    setOverdueDateTime() {
      this.setDateTime(this.tomorrow, false);
    },
    setNonOverdueDateTime() {
      this.setDateTime(this.oneDayAfterOriginalEndDate, false);
    },
    onCalendarChange(maybeDateTime, ignoreTime = true) {
      this.setDateTime(maybeDateTime, true, true);
    },
    setDateTime(maybeDateTime?: string | moment.Moment, ignoreTime = false, originalTime = false) {
      const dateTime = !maybeDateTime
        ? this.branchNowRounded.clone()
        : moment.isMoment(maybeDateTime)
          ? maybeDateTime.clone()
          : moment(maybeDateTime);

      if (ignoreTime) {
        dateTime
          .hours(this.selectedDateTime.hours())
          .minutes(this.selectedDateTime.minutes())
          .seconds(this.selectedDateTime.seconds());
      }

      if (originalTime || !ignoreTime) {
        dateTime
          .hours(this.originalStartDateTime.hours())
          .minutes(this.originalStartDateTime.minutes())
          .seconds(this.originalStartDateTime.seconds());
      }

      const dateTimeFinal = this.nearestHalfHourFor(dateTime);
      this.setSelectedDateTime(dateTimeFinal);
    },
    setSelectedTime(maybeDateTime: string | moment.Moment) {
      const dateTime = moment.isMoment(maybeDateTime) ? maybeDateTime : moment(maybeDateTime);
      this.setSelectedDateTime(this.nearestHalfHourFor(dateTime));
    },
  },
  watch: {
    originalStartDateTime() {
      this.setDateTime();
    },
  },
});
