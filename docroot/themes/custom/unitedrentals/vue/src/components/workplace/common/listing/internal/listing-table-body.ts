
import Vue from 'vue';
import {
  GroupHeaderCellScope,
  ListingConfig,
  ListingConfigToken,
  ListingEmitter,
  ListingEmitterToken,
  ListingLayoutManager,
  ListingLayoutToken,
  ListingModuleState,
  ListingStateToken,
  ListingRowsToken,
  ScrollPositionY,
  ListingRow,
  RowType,
} from '../lib';
import ListingCell from './listing-cell';
import ListingGroupHeader from './listing-group-header';
import ListingTable from './listing-table';
import ListingSubtable from './listing-subtable';
import { VirtualScroll, VirtualScrollRange } from '../../../../common/virtual-scroll';

export default Vue.extend({
  name: 'listing-table-body',
  inject: {
    injectedConfig: ListingConfigToken,
    injectedEmitter: ListingEmitterToken,
    injectedLayout: ListingLayoutToken,
    injectedRows: ListingRowsToken,
    injectedState: ListingStateToken,
  },
  components: {
    ListingCell,
    ListingGroupHeader,
    ListingSubtable,
    ListingTable,
    VirtualScroll,
  },
  data() {
    const vscrollRange: VirtualScrollRange = {
      endIndex: 0,
      itemTotal: 0,
      scrollPosition: 'top',
      startIndex: 0,
    };

    return {
      collapsedIndices: [] as number[],
      linkHoverRowIndex: -1,
      mutationObserver: null as MutationObserver | null,
      rowTypes: RowType,
      vscrollRange,
    };
  },
  mounted() {
    (window as any).listingBody = this;
    const vsl: Vue = this.$refs.vsl;
    if (!(vsl instanceof Vue)) throw new Error(`Virtual scroll $ref not found!`);
    const mutationObserver = new MutationObserver(this.childListMutationCallback.bind(this));
    mutationObserver.observe(vsl.$el, { childList: true, subtree: true });
    this.mutationObserver = mutationObserver;
  },
  beforeDestroy() {
    this.layout.scrollPositionY = 'ally';
    this.mutationObserver.disconnect();
  },
  computed: {
    config(): ListingConfig {
      return (this as any).injectedConfig();
    },
    emitter(): ListingEmitter {
      return (this as any).injectedEmitter();
    },
    rows(): ListingRow[] {
      const rows: ListingRow[] = (this as any).injectedRows();
      return rows.filter(({ type, groupIndex }) =>
        type !== 'data' || !this.collapsedIndices.includes(groupIndex),
      );
    },
    layout(): ListingLayoutManager {
      return (this as any).injectedLayout();
    },
    state(): ListingModuleState {
      return (this as any).injectedState();
    },
  },
  methods: {
    childListMutationCallback(mutations: MutationRecord[], observer: MutationObserver) {
      mutations
        .filter(mut => mut.type === 'childList' && mut.addedNodes.length > 0)
        .reduce<HTMLElement[]>((subtables, { addedNodes }) => [
          ...subtables,
          ...Array.from<Element>(addedNodes as any)
            .filter(node => typeof node.querySelector === 'function')
            .map(rowElement => rowElement.querySelector(`.${this.layout.scrollLeftSyncClass}`))
            .filter((subtable): subtable is HTMLElement => subtable instanceof HTMLElement),
        ], [])
        .forEach(subtable => subtable.scrollLeft = this.layout.scrollLeft);
    },
    getRowHeight(index: number): number {
      const row: ListingRow = this.rows[index];
      switch (row && row.type || RowType.Data) {
        case RowType.GroupHeader:
          return this.config.groupHeaderHeight;
        case RowType.Data:
        default:
          return this.config.dataHeight;
      }
    },
    handleLinkMouseOver(rowIndex: number) {
      this.linkHoverRowIndex = rowIndex;
    },
    handleLinkMouseOut() {
      this.linkHoverRowIndex = -1;
    },
    handleSubtableScroll($event) {
      this.layout.handleSubtableScroll($event);
    },
    handleScrollPositionChange(positions: Record<'previous' | 'current', ScrollPositionY>) {
      this.layout.scrollPositionY = positions.current;
    },
    handleRangeChange(range: VirtualScrollRange) {
      this.vscrollRange = range;
      this.emitter({
        event: 'scrollRangeChanged',
        payload: range,
      });
    },
    groupIsCollapsed(groupIndex: number): boolean {
      return this.collapsedIndices.includes(groupIndex);
    },
    toggleCollapsed({ groupIndex }: GroupHeaderCellScope) {
      if (this.collapsedIndices.includes(groupIndex)) {
        this.collapsedIndices = this.collapsedIndices.filter(i => i !== groupIndex);
      }
      else {
        this.collapsedIndices.push(groupIndex);
      }
    },
  },
  watch: {
    '$route.query.groupBy'() {
      // clear collapsed groups on change
      this.collapsedIndices = [];
    },
  },
});
