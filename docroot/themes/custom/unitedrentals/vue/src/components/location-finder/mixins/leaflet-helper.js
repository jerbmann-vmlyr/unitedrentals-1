import L from 'leaflet';

export default {
  created() {
    L.Map.prototype.panToOffset = function (latlng, offset, options) {
      const yOffset = (-(offset.paddingBottom ? offset.paddingBottom : 0) + (offset.paddingTop ? offset.paddingTop : 0)) / 2;
      const xOffset = ((offset.paddingRight ? offset.paddingRight : 0) - (offset.paddingLeft ? offset.paddingLeft : 0)) / 2;
      const x = this.latLngToContainerPoint(latlng).x - xOffset;
      const y = this.latLngToContainerPoint(latlng).y - yOffset;
      const point = this.containerPointToLatLng([x, y]);
      return this.setView(point, this._zoom, { pan: options });
    };
  },
  data() {
    return {
      Leaflet: L,
      BranchMarker: L.Marker.extend({
        options: {
          branchId: '',
          branchIndex: 0,
        },
      }),
    };
  },
  methods: {
    // accepts either an actual full google place, which has viewport.north etc.,
    // or an autocomplete search result which has viewport.l.j etc
    // TODO UR-5260 refactor this so as never to read viewport.l.j etc, as these, prior to release 1.12, were viewport.f.b etc.
    convertPlaceToBounds(place) {
      const { viewport } = place.geometry;
      const corner1 = L.latLng(
        viewport.north ? viewport.north : viewport.na.l,
        viewport.west ? viewport.west : viewport.ga.l,
      );
      const corner2 = L.latLng(
        viewport.south ? viewport.south : viewport.na.j,
        viewport.east ? viewport.east : viewport.ga.j,
      );
      return L.latLngBounds(corner1, corner2);
    },
    convertPlaceToPoint(place) {
      const { location } = place.geometry;
      return L.latLng(location.lat, location.lng);
    },
  },
};
