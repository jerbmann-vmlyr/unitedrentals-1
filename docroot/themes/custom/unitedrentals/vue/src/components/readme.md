# Vue Component Guide

## Open Vue Component Documentation In Browser
```
  cd ~/docroot/unitedrentals/themes/custom/unitedrentals/vue && npm run docs:vue
```

## Style Guide

(1) Alphabetize component options, alphabetize nested properties, alphabetize all the things.

```javascript
export default {
  components: { /* ... */ },
  computed: {
    ...mapState({
      alpha() { /* ... */ },
      beta() { /* ... */ }
    }),
    apple() { /* ... */ },
    banana() { /* ... */ }
  },
  data(): { /* ... */ },
  destroyed: { /* ... */ },
  methods: { /* ... */ },
  mounted: { /* ... */ },
  name: '',
  props: { /* ... */ },
  watch: { /* ... */ },
}
```

(2) Favor ES6 arrow function notation to bind `this` context

```javascript
// (good)
this.$watch('foo', (foo) => this.foo = foo);

// (bad)
var vm = this;
this.$watch('blargh', function(blargh) { vm.blargh = blargh; }
```

(3) Use ES6 Imports, not commonJS

```javascript
const bad = require('bad')';
import {all}, good from 'good';
```

(4) In general, favor `watch` and `events` component option keys over `this.$watch` and `this.$on`. However if this
breaks the DRY principle don't do it.

```javascript
export default {
  // Usually Good!
  events: {
    'some-event': function(foo) { /* ... */ }
  },
  watch: {
    'my-watcher': function(bar) { /* ... */ }
  },

  // Usually Bad!
  mounted() {
    this.$on('some-event', this.coverIt);
    this.$watch('my-watcher', this.handleIt);
  },
  methods: {
    coverIt(foo) { /* ... */ }
    handleIt(bar) { /* ... */ }
  }
}
```

(5) Use Object Destructuring

```javascript
// Good
const {prop1, prop2} = myObject;

// Bad
const prop1 = myObject.prop1;
const prop2 = myObject.prop2;
```

(6) Favor `const`, then `let`. Lastly choose `var`.

(7) Use our `...mapLabels` utility inside an `i18n` field for text

```javascript
<template>
  <p>{{ i18n.hello }}</p>
</template>
<script>
data() {
  return {
    i18n: {
      ...mapLabels({ hello: 'hello world' })
    }
  }
}
</script>
```

(8) Components should dispatch actions, not commit mutations.

```javascript
// Good
this.$store.dispatch(types.sayHello, 'hello dude');

// Good
<template>
  <button @click="sayHello">hello</button>
</template>
<script>
methods: {
  ...mapActions({
    sayHello: types.sayHello
  })
}
</script>

// Bad
methods: {
  sayHello() {
    this.$store.commit(types.sayHello, 'hello dude');
  }
}
```

(9) Favor the Spread Operator over `Object.assign`, `_.extend`, or `jQuery.extend`.

```javascript
// Given
const foo = {foo: 'foo'};
const bar = {bar: 'bar'};

// Good
const baz = { ...foo, ...bar}; // a new object with props foo and bar, {foo: 'foo', bar: 'bar'}

// Bad
const doh = Object.assign(foo, bar);
const doh2 = jQuery.extend({}, foo, bar);
const doh3 = _.extend({}, foo, bar);
```
