import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

class CheckoutApi {
  error = '';
  @TrackFlight(FlightToken.fetchCheckoutChargeEstimates)
  fetchEstimates(reservationBlock, override = {}) {
    return axios.post('/api/v1/charge-estimates', reservationBlock.exportEstimateAPIParams(override))
      .then(response => {
        transformEstimatesData(response.data);
        return response;
      });
  }

  @TrackFlight(FlightToken.createTransaction)
  createTransaction(params) {
    return axios.post('/api/v1/transaction/submit', params)
      .catch(error => {
        this.error = error;
        return this;
      });
  }
}


/**
 * Prunes estimates response, and ties the response to the "reservation block"
 * that made the request.
 * @param {object} data | response data
 * @param {string} reservationBlockId | id for the reservation block that called for the estimate
 */
function transformEstimatesData(data) {
  delete data.accountId;
  delete data.branchId;
  delete data.catClassAndQty;
  delete data.catClassQtyRates;
  delete data.delivery;
  delete data.jobsiteId;
  delete data.omitNonDspRates;
  delete data.pickup;
  delete data.returnDate;
  delete data.rpp;
  delete data.startDate;
  delete data.webRates;
}

export default new CheckoutApi();
