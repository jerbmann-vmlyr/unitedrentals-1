import axios from 'axios';
import { ChargeEstimate } from '@/lib/estimates';
import { TrackFlight, FlightToken } from './flight-track';

// As of api/v2, the Charge Estimate Request object and Response object are the same shape
export interface ChargeEstimateRequest {
  // Required
  branchId: string;
  startDate: string;
  returnDate: string;
  delivery: boolean;
  pickup: boolean;
  rpp: boolean;
  items: {
    [catClass: string]: {
      quantity: number;
    };
  };

  // Required if address, city, state, & postalCode do not exist
  jobsiteId?: string;
  accountId?: string;

  // Required if jobsiteId and accountId do not exist
  address?: string;
  city?: string;
  state?: string;
  postalCode?: string;

  // Optional
  omitNonDspRates?: boolean;
  webRates?: boolean;
  deliveryCost?: number;
  pickupCost?: number;
  costOverride?: boolean;
  promotionCode?: string;
  useCache?: boolean;
}

class ChargeEstimatesApi {
  @TrackFlight(FlightToken.fetchChargeEstimates)
  async fetch(request: ChargeEstimateRequest): Promise<ChargeEstimate> {
    try {
      validateRequest(request);
      const { data } = await axios.post('/api/v2/charge-estimates', request);
      return data.data;
    }
    catch (e) {
      console.error(e);
      return Promise.reject(e);
    }
  }
}

const validateRequest = request => {
  const conditionallyRequiredKeys = request.accountId ? ['accountId', 'jobsiteId'] : ['address', 'city', 'state', 'postalCode'];
  const requiredKeys = [...conditionallyRequiredKeys, 'branchId', 'startDate', 'returnDate', 'delivery', 'pickup', 'rpp', 'items'];

  const requestKeys = Object.keys(request);

  if (!requiredKeys.every(key => requestKeys.includes(key))) {
    const missingKeys = requiredKeys.filter(key => !requestKeys.includes(key));
    throw new Error(`Will not fetch estimate. Missing required params: ${missingKeys.toString()}`);
  }
};

export default new ChargeEstimatesApi();
