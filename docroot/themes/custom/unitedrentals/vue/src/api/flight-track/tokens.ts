// Keep me alphabetized and divided by ApiClass please!

export enum FlightToken {
  // AccountsApi
  createAccount = 'createAccount',
  createFirstAccount = 'createFirstAccount',
  fetchAccount = 'fetchAccount',
  saveAccount = 'saveAccount',
  saveAccountFavorite = 'saveAccountFavorite',
  searchAccounts = 'searchAccounts',

  // AccountsPoNumbersApi
  fetchPoNumbers = 'fetchAccountPoNumbers',
  fetchPoFormats = 'fetchAccountPoFormats',

  // AccountingRequisitionCodesApi
  fetchRequisitionCodes = 'fetchRequisitionCodes',

  // AllocationCodesApi
  fetchAllocationCodes = 'fetchAllocationCodes',

  // ApproversApi
  fetchApprovers = 'fetchApprovers',

  // BusinessTypes
  fetchBusinessTypes = 'fetchBusinessTypes',

  // CartApi
  fetchCart = 'fetchCart',
  addCartItem = 'addCartItem',
  removeCartItem = 'removeCartItem',
  updateCartItem = 'updateCartItem',
  updateCartItems = 'updateCartItems',

  // Cart SaveForLaterApi
  fetchSaveForLater = 'fetchSaveForLater',
  setSaveForLater = 'setSaveForLater',

  // CatalogApi
  fetchCatClassByEquipmentId = 'fetchCatClassByEquipmentId',
  fetchEquipmentByTermId = 'fetchEquipmentByTermId',
  fetchEquipmentByCatClass = 'fetchEquipmentByCatClass',
  fetchRates = 'fetchRates',

  // ChargeEstimatesApi
  fetchChargeEstimates = 'fetchChargeEstimates',

  // CheckoutApi
  fetchCheckoutChargeEstimates = 'fetchCheckoutChargeEstimates',
  createTransaction = 'createTransaction',

  // ContractsApi
  sendExtendContractRequest = 'sendExtendContractRequest',

  // CreditCardsApi
  fetchCreditCards = 'fetchCreditCards',
  addCreditCard = 'addCreditCard',
  updateCreditCard = 'updateCreditCard',
  removeCreditCard = 'removeCreditCard',

  // FacetsApi
  fetchFacets = 'fetchFacets',
  fetchFacetDescriptions  = 'fetchFacetDescriptions',

  // FavoritesApi
  addFavorite = 'addFavorite',
  fetchFavorites = 'fetchFavorites',
  removeFavorite = 'removeFavorite',

  // GlobalSearchApi
  getGlobalSearchResults = 'getGlobalSearchResults',

  // InvoicesApi
  fetchInvoices = 'fetchInvoices',

  // JobsApi
  addJobsite = 'addJobsite',
  fetchJobsites = 'fetchJobsites',
  saveJobsite = 'saveJobsite',

  // LinkAccountsApi
  saveValidatePins = 'saveValidatePins',

  // UnlinkAccountsApi
  deleteAccount = 'deleteAccount',

  // NotificationsApi
  fetchNotifications = 'fetchNotifications',

  // OrdersApi
  fetchOrders = 'fetchOrders',

  // PardotApi
  initiatingCheckout = 'initiatingCheckout',

  // BranchesApi
  getDefaultBranches = 'getDefaultBranches',
  getBranchData = 'getBranchData',
  getNearbyBranchData = 'getNearbyBranchData',

  // PopularEquipmentTypesApi
  fetchPopularEquipmentTypes = 'fetchPopularEquipmentTypes',

  // QuotesApi
  convertQuoteToCart = 'convertQuoteToCart',
  fetchQuotes = 'fetchQuotes',
  updateJobsiteInfoOrTransactionJobId = 'updateJobsiteInfoOrTransactionJobId',
  updateQuote = 'updateQuote',

  // ReadReservationDetailsApi
  fetchReservationDetails = 'fetchReservationDetails',
  saveRequisitionJobsite = 'saveRequisitionJobsite',
  saveRequisitionTransactionDateTimeChange = 'saveRequisitionTransactionDateTimeChange',
  saveRequisitionQuantityChange = 'saveRequisitionQuantityChange',

  // RequestPickup
  fetchRequestPickupData = 'fetchRequestPickupData',

  // RequisitionsApi
  fetchRequisitions = 'fetchRequisitions',
  updateRequisition = 'update',
  updateRequisitionAndTransaction = 'updateBoth',

  // RecentlyViewedApi
  fetchRecentlyViewed = 'fetchRecentlyViewed',

  // RequestersApi
  fetchRequesters = 'fetchRequesters',

  // ReservationsApi
  fetchReservations = 'fetchReservations',

  // TcUserApi
  createTcUser = 'createTcUser',
  fetchTcUser = 'fetchTcUser',
  fetchOneTcUser = 'fetchOneTcUser',
  saveTcUser = 'saveTcUser',

  // TransactionsApi
  fetchTransactions = 'fetchTransactions',
  updateTransactionPo = 'updateTransactionPo',

  // UserApi
  getLoggedInUsername = 'getLoggedInUsername',
  getGuestLogout = 'getGuestLogout',
  fetchUserPreferences = 'fetchUserPreferences',
  saveUserPreferences = 'saveUserPreferences',

  // WorkplaceEquipmentApi
  fetchEquipmentGps = 'fetchEquipmentGps',
  fetchEquipmentOnRent = 'fetchEquipmentOnRent',
  fetchLeniencyRates = 'fetchLeniencyRates',
  sendPickupRequest = 'sendPickupRequest',
  fetchRentalCounts = 'fetchRentalCounts',

  // MessagingApi
  fetchMessageRoute = 'fetchMessageRoute',
}

export const TrackerTokenMetadataKey = 'trackerTokens';
