export * from './api.accounts';
export * from './api.workplace-equipment';

import accountingRequisitionCodes from './api.accounting-requisition-codes';
import accounts from './api.accounts';
import accountPoNumbers from './api.accounts.po-numbers';
import allocationCodes from './api.allocation-codes';
import approvers from './api.approvers';
import branches from './api.branches';
import businessTypes from './api.business-types';
import cart from './api.cart';
import cartSaveForLater from './api.save-for-later';
import catalog from './api.catalog';
import chargeEstimates from './api.charge-estimates';
import checkout from './api.checkout';
import contracts from './api.contracts';
import creditCards from './api.credit-cards';
import facets from './api.facets';
import favorites from './api.favorites';
import globalSearch from './api.global-search';
import invoices from './api.invoices';
import jobs from './api.jobs';
import linkAccounts from './api.link-accounts';
import messageRouting from './api.message-routing';
import notifications from './api.notifications';
import pardot from './api.pardot';
import popularEquipmentTypes from './api.popular-equipment-types';
import quotes from './api.quotes';
import readReservationDetails from './api.read-reservation-details';
import recentlyViewed from './api.recently-viewed';
import requesters from './api.requesters';
import requestPickup from './api.request-pickup';
import requisitions from './api.requisitions';
import reservations from './api.reservations';
import tcUser from './api.tc-user';
import transactions from './api.transactions';
import unlinkAccounts from './api.unlink-accounts';
import user from './api.user';
import workplaceEquipment from './api.workplace-equipment';
import workplacePreferences from './api.workplace-preferences';
import workplaceOrders from './api.workplace-order';

export default {
  accounts,
  accountPoNumbers,
  accountingRequisitionCodes,
  allocationCodes,
  approvers,
  branches,
  businessTypes,
  cart,
  cartSaveForLater,
  catalog,
  chargeEstimates,
  checkout,
  contracts,
  creditCards,
  facets,
  globalSearch,
  favorites,
  invoices,
  jobs,
  linkAccounts,
  messageRouting,
  notifications,
  pardot,
  popularEquipmentTypes,
  quotes,
  readReservationDetails,
  recentlyViewed,
  requesters,
  requestPickup,
  reservations,
  requisitions,
  tcUser,
  transactions,
  unlinkAccounts,
  user,
  workplaceOrders,
  workplaceEquipment,
  workplacePreferences,
};
