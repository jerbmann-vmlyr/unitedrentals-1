import * as moment from 'moment';
import * as Qs from 'qs';
import axios, { AxiosResponse } from 'axios';
import { StatusUtils } from '@/lib/statuses';
import {
  WorkplaceEquipment,
  WorkplaceEquipmentGps,
  WorkplaceRentalCounts,
  WorkplaceEquipmentUtils,
  WorkplaceEquipmentUsageByDay,
  WORKPLACE_EQUIPMENT_STATUSES,
  workplaceEquipmentStatusRules,
} from '@/lib/workplace';
import { TrackFlight, FlightToken } from './flight-track';

export interface PickupRequestRequest {
  address1: string;
  address2: string;
  city: string;
  moreInstructions: string;
  pickupDateTime: string;
  quantity: number|string;
  state: string;
  transId: string;
  transLineId: string;
  zip: string;
  guid: string;
  contact: string;
  phone: string;
  requester: string;
  equipmentLocation: string;
  keyLocation: string;
  accessTimes: string;
  emailAddresses: string;
}

export interface PickupRequestResponse {
  pickupId: string;
  transId: string;
  transLineId: string;
}

export interface EquipmentGpsRequest {
  accountId: string;
  equipmentId: string;
  fromDate?: number;
  toDate?: number;
}

export interface FetchLeniencyRatesRequest {
  contract: string,
  daysNeeded?: number,
  lineNum: string | string[],
  startDate: string,
}

/**
 * It's possible to send more data than this in the request, we're just not doing it right now
 * @TODO: add all the parameters we might use for discrete count retrieval as needed
 */
export interface WorkplaceCountsRequest {
  accountId: string;
}

/* Do not export; this interface represents the pre-normalized API response */
interface EquipmentGpsResponse {
  gps: {
    altitude: {
      meters: number | null;
    };
    datetime: string;
    latitude: string;
    longitude: string;
    usageByDay: never[] | WorkplaceEquipmentUsageByDay
  }
}

export interface FetchEquipmentOnRentRequest {
  accountId: string;
  type: number;
  transId: number;
  useCache?: 0 | 1;
}

export class WorkplaceEquipmentApi {

  @TrackFlight(FlightToken.fetchEquipmentGps)
  async fetchGps(requestParams: EquipmentGpsRequest): Promise<WorkplaceEquipmentGps[]>  {
    const params = {
      /* Default to a span of 2 weeks */
      fromDate: Number(moment.utc().subtract(14, 'days').format('YYYYMMDD')),
      toDate:  Number(moment.utc().format('YYYYMMDD')),
      ...requestParams,
    };
    const response = await axios.get('/api/v1/equipment/gps', { params });
    const data: EquipmentGpsResponse[] = response.data;

    if (!WorkplaceEquipmentApi.fetchGpsResponseIsValid(data)) {
      console.error(`Response from /api/v1/equipment/gps is not valid: `, data);
      return [];
    }
    else {
      return WorkplaceEquipmentApi.transformFetchGpsResponse(data);
    }
  }


  /**
   * 1=Open Contracts
   * 2=Open Quotes
   * 3=Open Reservations
   * 4=Closed Contracts
   * 6=Pickup Requests (must be the only type)
   * 7=Past Due (must be the only type)
   * 8=Canceled Quotes can only be grouped with type 9)
   * 9=Canceled Reservations (can only be grouped with type 8)
   */

  @TrackFlight(FlightToken.fetchEquipmentOnRent)
  async fetchEquipmentOnRent(request: FetchEquipmentOnRentRequest): Promise<WorkplaceEquipment[]> {

    const { accountId, ...params } = request;
    const { data } = await axios.get(`/api/v2/equipment/on-rent/${accountId}`, { params });

    return WorkplaceEquipmentApi.transformOnRentEquipmentResponse(data, accountId);

  }

  @TrackFlight(FlightToken.sendPickupRequest)
  async sendPickupRequest(request: PickupRequestRequest): Promise<PickupRequestResponse> {
    const { data } = await axios.post('/api/v1/pickup-request', request);

    if (WorkplaceEquipmentApi.pickupRequestResponseIsValid(data)) {
      return data;
    }
    else {
      const extractedMessage = Array.isArray(data) && data.length > 0 ? data[0] : data;
      return Promise.reject(new Error(extractedMessage));
    }
  }

  @TrackFlight(FlightToken.fetchRentalCounts)
  async fetchRentalCounts(input: WorkplaceCountsRequest): Promise<WorkplaceRentalCounts> {
    const { data } = await axios.get('/api/v1/rental/counts', { params: input});
    const emptyRentalCounts: WorkplaceRentalCounts = {
      quotes: 0,
      reservations: 0,
      onRent: 0,
      scheduledForPickup: 0,
      overdue: 0,
      allOpen: 0,
      closedContracts: 0,
      custOwn: false,
    };

    if (WorkplaceEquipmentApi.fetchRentalCountsResponseIsValid(data)) {
      return { ...emptyRentalCounts, ...data };
    }
    else {
      console.error('/api/v1/rental/counts returned invalid data, using default values instead');
      return emptyRentalCounts;
    }
  }

  @TrackFlight(FlightToken.fetchLeniencyRates)
  async fetchLeniencyRates(request: FetchLeniencyRatesRequest) {
    const query = Qs.stringify(request, { addQueryPrefix: true });

    return axios.get(`api/v1/rental/leniencyrates${query}`);
  }

  static fetchGpsResponseIsValid(data: any): data is EquipmentGpsResponse[] {
    return data !== null
      && typeof data === 'object'
      && Array.isArray(data)
      && data.every(obj => typeof obj === 'object');
  }

  static fetchRentalCountsResponseIsValid(data: any): data is WorkplaceRentalCounts {
    return data !== null
      && typeof data === 'object'
      && ['onRent', 'overdue', 'quotes', 'reservations'].every(key => Object.keys(data).includes(key));
  }

  static pickupRequestResponseIsValid(data: any): data is PickupRequestResponse {
    return data !== null
      && typeof data === 'object'
      && !Array.isArray(data)
      && typeof data.pickupId === 'string';
  }

  static transformFetchGpsResponse(data: EquipmentGpsResponse[]): WorkplaceEquipmentGps[] {
    return data
      .map(({ gps }) => gps)
      .map(({ altitude, latitude, longitude, usageByDay, ...rest }): WorkplaceEquipmentGps => {
        const valid = typeof latitude === 'string' && !isNaN(+latitude);
        return {
          ...rest,
          valid,
          altitude: !!altitude.meters ? +altitude.meters : null,
          latitude: typeof latitude === 'string' ? +latitude : null,
          longitude: typeof longitude === 'string' ? +longitude : null,
          usageByDay: Array.isArray(usageByDay) ? {} : usageByDay,
        };
      });
  }

  static transformOnRentEquipmentResponse(
    { data }: AxiosResponse<WorkplaceEquipment[]>,
    accountId: string,
  ): WorkplaceEquipment[] {
    return Object.values(data)
    /*
      Even if the call fails, or there is no listed equipment,
      the API service will still return a 200 response, with a single element
      array containing a string indicating what went wrong, e.g. "No data returned."
      We need to ignore these items.
    */
      .filter(equipment => typeof equipment === 'object' && typeof equipment.catClass === 'string')
      .reduce((allEquipment, equipment): WorkplaceEquipment[] => [
        ...allEquipment,
        {
          ...equipment,
          accountId,
          daysOnRent: WorkplaceEquipmentUtils.calculateDaysOnRent(equipment),
          // Cast rates as type Number
          dayRate: +equipment.dayRate,
          minRate: +equipment.minRate,
          monthRate: +equipment.monthRate,
          type: +equipment.type,
          weekRate: +equipment.weekRate,
          year: +equipment.year,
          // The following properties must be populated downstream using data from non-equipment APIs
          catClassIsFavorite: false,
          eqpType: null,
        },
      ], [] as WorkplaceEquipment[])
      .map(eqp => ({
        ...eqp,
        equipmentStatuses: StatusUtils.getApplicableStatuses(
          WORKPLACE_EQUIPMENT_STATUSES,
          eqp,
          workplaceEquipmentStatusRules,
        ),
      }));
  }
}


export default new WorkplaceEquipmentApi();
