/**
 * NOTE: This module is registered dynamically!
 */
import { get } from 'lodash';
import * as moment from 'moment';
import { Mutation, Module } from 'vuex';
import { Address, Contact, Jobsite } from '@/lib/common-types';
import { Branch } from '@/lib/branches';
import { StatusUtils } from '@/lib/statuses';
import { WorkplaceEquipment, WorkplaceEquipmentStatusId } from '@/lib/workplace';
import WorkplaceEquipmentApi, { PickupRequestRequest, PickupRequestResponse } from '@/api/api.workplace-equipment';
import ContractsApi, { ExtendContractRequest } from '@/api/api.contracts';
import { WorkplaceRootState } from './module.workplace';
import types from './types';
import DateTime from '@/utils/dateTime';

export enum RescheduleType {
  Pickup = 'pickup',
  Extend = 'extend',
}

export interface PickupInstructions {
  accessTimes: string;
  additional: string;
  equipmentLocation: string;
  keyLocation: string;
}

export interface EquipmentQuantities {
  [equipmentId: string]: number;
}

export interface WorkplaceRescheduleRequestState {
  address: NonNullableProps<Address>;
  branch: null | Branch;
  confirmationNumber: string;
  contact: NonNullableProps<Contact>;
  equipmentList: WorkplaceEquipment[];
  emailRecipients: string[];
  instructions: PickupInstructions;
  requestIsInitialized: boolean;
  selectedDateTime: moment.Moment;
  selectedEquipmentQtys: EquipmentQuantities;
}

const stateFactory = (): WorkplaceRescheduleRequestState => ({
  address: {
    address1: '',
    address2: '',
    city: '',
    state: '',
    zip: '',
  },
  branch: null,
  confirmationNumber: '',
  contact: {
    contact: '',
    email: '',
    mobilePhone: '',
    phone: '',
  },
  emailRecipients: [],
  equipmentList: [],
  instructions: {
    accessTimes: '',
    additional: '',
    equipmentLocation: '',
    keyLocation: '',
  },
  requestIsInitialized: false,
  selectedDateTime: DateTime.round(moment(), moment.duration(30, 'minutes'), 'ceil'),
  selectedEquipmentQtys: {},
});


export const WorkplaceRescheduleRequestModule: Module<WorkplaceRescheduleRequestState, WorkplaceRootState> = {
  state: stateFactory,

  actions: {
    async [types.workplace.rescheduleRequest.populateEquipmentListFromTransIds]({ commit, getters, state }, transIds: string[]) {
      // Pull equipment with the provided ids
      const equipmentForCurrentAccount: WorkplaceEquipment[] = getters[types.workplace.equipmentForCurrentAccount] || [];
      const mustHaveStatus = [ WorkplaceEquipmentStatusId.AdvancedPickup, WorkplaceEquipmentStatusId.PickupRequested ];
      const equipment = equipmentForCurrentAccount.filter(({ equipmentStatuses, transId }) =>
        transIds.includes(transId)
        && !StatusUtils.statusListIncludesAnyOf(mustHaveStatus, equipmentStatuses),
      );

      commit(types.workplace.rescheduleRequest.mutate, () => state.equipmentList = equipment);
      return equipment;
    },

    async [types.workplace.rescheduleRequest.initializeRescheduleRequest]({ commit, dispatch, getters, state }) {
      const transId = getters[types.workplace.rescheduleRequest.transId];
      const transLineId = getters[types.workplace.rescheduleRequest.transLineId];

      // Populate the list of equipment
      const equipment = await dispatch(types.workplace.rescheduleRequest.populateEquipmentListFromTransIds, [transId]);

      // Populate the equipment quantity tracking object, only the item for which the reschedule request has
      // been opened for (identifed by transLineId in URL) should have a quantity of greater then zero
      const equipmentQtys: EquipmentQuantities = equipment
        .map(eqp => ({
          [eqp.equipmentId]: eqp.transLineId === transLineId
            ? eqp.quantityOnRent - eqp.quantityPendingPickup
            : 0,
        }))
        .reduce((accum, qty) => ({ ...accum, ...qty }), {});

      await dispatch(types.workplace.rescheduleRequest.setSelectedEquipmentQtys, equipmentQtys);

      // Set the initial value of the selected date and time, based on the original return date and time
      const originalReturn: moment.Moment = getters[types.workplace.rescheduleRequest.originalReturnDateTime] || state.selectedDateTime;
      const initialSelectedDateTime = originalReturn.clone();
      if (getters[types.workplace.rescheduleRequest.isExtension]) {
        initialSelectedDateTime.add(1, 'days');
      }
      if (initialSelectedDateTime.isBefore(moment(), 'day')) {
        const now = moment();
        initialSelectedDateTime.year(now.year()).month(now.month()).day(now.day());
      }
      commit(types.workplace.rescheduleRequest.mutate, state => state.selectedDateTime = initialSelectedDateTime);

      // Set the initial values for pickup location and contact using the jobsite of the first item
      await dispatch(types.workplace.rescheduleRequest.setPickupAddressToJobsiteAddress);
      await dispatch(types.workplace.rescheduleRequest.setPickupContactToJobsiteContact);

      // Fetch the branch of the first item and save it to state
      const branch = await dispatch(types.branch.fetchById, equipment[0].branchId);
      commit(types.workplace.rescheduleRequest.mutate, () => state.branch = branch);

      // Fetch the leniency rates
      await dispatch(types.workplace.fetchLeniencyRates, {
        contract: transId,
        daysNeeded: moment().diff(equipment[0].startDateTime, 'days') + 62,
        lineNum: equipment.map(a => a.transLineId),
        startDate: moment(equipment[0].startDateTime).format('YYYY-MM-DD'),
      });

      // Set the request to initialized
      commit(types.workplace.rescheduleRequest.mutate, () => state.requestIsInitialized = true);
    },

    [types.workplace.rescheduleRequest.reset]({ commit, state }) {
      // tslint:disable-next-line:prefer-object-spread
      commit(types.workplace.rescheduleRequest.mutate, () => Object.assign(state, stateFactory()));
    },
    [types.workplace.rescheduleRequest.saveAddress]({ commit, state }, address: NonNullableProps<Address>) {
      commit(types.workplace.rescheduleRequest.mutate, () => state.address = address);
    },

    [types.workplace.rescheduleRequest.saveContact]({ commit, state }, contact: NonNullableProps<Contact>) {
      commit(types.workplace.rescheduleRequest.mutate, () => state.contact = contact);
    },

    [types.workplace.rescheduleRequest.saveEmailRecipients]({ commit, state }, emailRecipients: string[] = []) {
      commit(types.workplace.rescheduleRequest.mutate, () => state.emailRecipients = emailRecipients);
    },

    [types.workplace.rescheduleRequest.saveInstructions]({ commit, state }, instructions: PickupInstructions) {
      commit(types.workplace.rescheduleRequest.mutate, () => state.instructions = instructions);
    },

    async [types.workplace.rescheduleRequest.selectAllEquipment]({ state, dispatch }) {
      const equipmentQtys: EquipmentQuantities = state.equipmentList
        .map(eqp => ({ [eqp.equipmentId]: eqp.quantityOnRent - eqp.quantityPendingPickup }))
        .reduce((accum, qty) => ({ ...accum, ...qty }), {});

      await dispatch(types.workplace.rescheduleRequest.setSelectedEquipmentQtys, equipmentQtys);
    },

    async [types.workplace.rescheduleRequest.setEquipmentSelectedById]({ state, dispatch }, payload: { equipmentId: string, selected: boolean }) {
      const matchingEqp = state.equipmentList.find(eqp => eqp.equipmentId === payload.equipmentId);
      if (!matchingEqp) throw new Error(`There's not equipment item with id "${payload.equipmentId}"`);
      await dispatch(types.workplace.rescheduleRequest.setSelectedEquipmentQtyById, {
        equipmentId: payload.equipmentId,
        quantity: payload.selected ? (matchingEqp.quantityOnRent - matchingEqp.quantityPendingPickup) : 0,
      });
    },

    [types.workplace.rescheduleRequest.setSelectedEquipmentQtyById]({ state, commit }, payload: { equipmentId: string; quantity: number}) {
      // Update the quantity of the provided item
      const equipmentQtys = { ...state.selectedEquipmentQtys, [payload.equipmentId]: payload.quantity };
      commit(types.workplace.rescheduleRequest.mutate, () => state.selectedEquipmentQtys = equipmentQtys);
    },

    [types.workplace.rescheduleRequest.setSelectedEquipmentQtys]({ state, commit }, quantities: EquipmentQuantities) {
      commit(types.workplace.rescheduleRequest.mutate, () => state.selectedEquipmentQtys = quantities);
    },

    [types.workplace.rescheduleRequest.setPickupAddressToJobsiteAddress]({ commit, state }) {
      const [{ jobsite }] = state.equipmentList;
      commit(types.workplace.rescheduleRequest.mutate, () => state.address = {
        address1: jobsite.address1 || '',
        address2: jobsite.address2 || '',
        city: jobsite.city || '',
        state: jobsite.state || '',
        zip: jobsite.zip || '',
      });
    },

    [types.workplace.rescheduleRequest.setPickupContactToJobsiteContact]({ commit, state }) {
      const [{ jobsite }] = state.equipmentList;
      commit(types.workplace.rescheduleRequest.mutate, () => state.contact = {
        contact: jobsite.contact || '',
        email: jobsite.email || '',
        mobilePhone: jobsite.mobilePhone || '',
        phone: jobsite.phone || '',
      });
    },

    [types.workplace.rescheduleRequest.setSelectedDateTime]({ commit, state }, dateTime: moment.Moment) {
      commit(types.workplace.rescheduleRequest.mutate, () => state.selectedDateTime = dateTime.clone());
    },

    async [types.workplace.rescheduleRequest.submitExtensionRequest]({ state, getters }): Promise<any> {
      const selectedEquipment: WorkplaceEquipment[] = getters[types.workplace.rescheduleRequest.selectedEquipment];
      if (selectedEquipment.length < 1) {
        console.warn(`Can't send a rental extension request if there is no equipment on the contract`);
        return [];
      }

      const request: ExtendContractRequest = {
        extensionDateTime: state.selectedDateTime.clone().format('YYYY-MM-DDTHH:mm:ss'),
        requester: selectedEquipment[0].requesterName,
        transId: selectedEquipment[0].transId,
      };

      return ContractsApi.sendExtendContractRequest(request);
    },

    async [types.workplace.rescheduleRequest.submitPickupRequest]({ commit, getters, state }): Promise<PickupRequestResponse> {
      const request: PickupRequestRequest = getters[types.workplace.rescheduleRequest.pickupRequestRequest];
      const response = await WorkplaceEquipmentApi.sendPickupRequest(request);
      commit(types.workplace.rescheduleRequest.mutate, () => state.confirmationNumber = response.pickupId);
      return response;
    },

    async [types.workplace.rescheduleRequest.unselectAllEquipment]({ dispatch, state }) {
      const equipmentQtys: EquipmentQuantities = state.equipmentList
        .reduce((accum, eqp) => ({ ...accum, [eqp.equipmentId]: 0 }), {});

      await dispatch(types.workplace.rescheduleRequest.setSelectedEquipmentQtys, equipmentQtys);
    },
  },

  getters: {
    [types.workplace.rescheduleRequest.accountId](state, getters, rootState): string {
      return rootState.route.params.accountId || '';
    },
    [types.workplace.rescheduleRequest.accountName](state, getters, rootState): string {
      return get(rootState.accounts.all, [getters[types.workplace.rescheduleRequest.accountId], 'name'], '');
    },
    [types.workplace.rescheduleRequest.allEquipmentIsSelected](state): boolean {
      return Object.values(state.selectedEquipmentQtys).every(qty => qty > 0);
    },

    [types.workplace.rescheduleRequest.atLeastOneEquipmentIdSelected](state): boolean {
      return Object.values(state.selectedEquipmentQtys).some(qty => qty > 0);
    },

    [types.workplace.rescheduleRequest.branchHoursOffset]({ branch }): number {
      /** The timeOffset returned by the branch API is actually relative to UR HQ in NY */
      const correction = moment().isDST() ? 4 : 5;

      if (!branch || !Number.isSafeInteger(+branch.timeOffset)) return correction;
      const skewedOffset = Math.abs(Math.round(+branch.timeOffset / 100));
      return correction + skewedOffset;
    },

    [types.workplace.rescheduleRequest.equipmentListIsEmpty](state): boolean {
      return state.equipmentList.length < 1;
    },

    [types.workplace.rescheduleRequest.jobsite](state): Jobsite | {} {
      const { equipmentList } = state;
      const [eqp] = equipmentList;
      return eqp ? eqp.jobsite : {};
    },

    [types.workplace.rescheduleRequest.isExtension](state, getters): boolean {
      return getters[types.workplace.rescheduleRequest.rescheduleType] === RescheduleType.Extend;
    },

    [types.workplace.rescheduleRequest.isPickup](state, getters): boolean {
      return getters[types.workplace.rescheduleRequest.rescheduleType] === RescheduleType.Pickup;
    },

    [types.workplace.rescheduleRequest.originalStartDateTime](state): moment.Moment | null {
      return state.equipmentList.length > 0 && typeof state.equipmentList[0].startDateTime === 'string'
        ? moment(state.equipmentList[0].startDateTime)
        : null;
    },

    [types.workplace.rescheduleRequest.originalReturnDateTime](state): moment.Moment | null {
      return state.equipmentList.length > 0 && typeof state.equipmentList[0].returnDateTime === 'string'
        ? moment(state.equipmentList[0].returnDateTime)
        : null;
    },

    [types.workplace.rescheduleRequest.rescheduleType](state, getters, rootState): RescheduleType {
      const rescheduleType = rootState.route.params.rescheduleType as RescheduleType;
      if (!Object.values(RescheduleType).includes(rescheduleType)) {
        window.console.warn(`"${rescheduleType}" is not a valid reschedule type, we're defaulting to "${RescheduleType.Extend}"`);
        return RescheduleType.Extend;
      }
      return rescheduleType;
    },

    [types.workplace.rescheduleRequest.selectedEquipment](state): WorkplaceEquipment[] {
      return state.equipmentList.filter(({ equipmentId }) => state.selectedEquipmentQtys[equipmentId] > 0);
    },

    [types.workplace.rescheduleRequest.pickupRequestRequest](state, getters, rootState): PickupRequestRequest {
      const pickupDateTime = moment.max(state.selectedDateTime.clone(), moment()).format('YYYY-MM-DDTHH:mm:ss');
      const transId = getters[types.workplace.rescheduleRequest.transId];

      // Hyphenate the phone number because the DAL is a needy, high-maintenance monster
      // that bends the rules of common sense and logic, slowly widdling you down to a gaslit
      // stump of confusion and frustration
      // Not because it has to, but because it hates you
      const normalizedPhone = state.contact.phone.replace(/\D/g, '');
      const phone = normalizedPhone.length === 10
        ? normalizedPhone.match(/^(\d{3})(\d{3})(\d{4})$/)!.slice(1, 4).join('-')
        : '';

      // Map the equipment quantities to their transaction IDs
      const quantitiesByTransId = Object.entries(state.selectedEquipmentQtys)
        .filter(([, quantity]) => quantity > 0)
        .reduce((accum, [equipmentId, quantity]) => {
          const { transLineId } = state.equipmentList.find(eqp => eqp.equipmentId === equipmentId)!;
          return { ...accum, [transLineId]: quantity };
        }, {});

      // Build the request objects
      return {
        address1: state.address.address1,
        address2: state.address.address2,
        city: state.address.city!,
        moreInstructions: state.instructions.additional,
        pickupDateTime,
        quantity: Object.values(quantitiesByTransId).join(','),
        state: state.address.state,
        transId,
        transLineId: Object.keys(quantitiesByTransId).join(','),
        zip: state.address.zip!,
        guid: rootState.tcUser.guid,
        contact: state.contact.contact,
        phone,
        requester: state.contact.contact,
        equipmentLocation: state.instructions.equipmentLocation,
        keyLocation: state.instructions.keyLocation,
        accessTimes: state.instructions.accessTimes,
        emailAddresses: [state.contact.email, ...state.emailRecipients]
          .filter(email => typeof email === 'string' && email.length > 0)
          .join(';'),
      };
    },
    [types.workplace.rescheduleRequest.transId](state, getters, rootState): string {
      return rootState.route.params.transId || '';
    },

    [types.workplace.rescheduleRequest.transLineId](state, getters, rootState): string {
      return rootState.route.params.transLineId || '';
    },
  },

  mutations: {
    [types.workplace.rescheduleRequest.mutate](state, mutation: Mutation<WorkplaceRescheduleRequestState>) {
      mutation(state, null);
    },
  },
};
