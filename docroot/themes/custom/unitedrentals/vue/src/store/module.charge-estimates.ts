import Vue from 'vue';
import { ActionTree, MutationTree } from 'vuex';
import { ChargeEstimate } from '@/lib/estimates';
import api from '@/api';
import types from '@/store/types';

export interface ChargeEstimatesState {
  all: { [requisitionId: string]: ChargeEstimate };
  defaultVal: { [requisitionId: string]: ChargeEstimate };
  fetching: boolean;
}

const state: ChargeEstimatesState = {

  /**
   * Charge Estimates returned from a api-appliance call, keyed by branchId, startDateTime,
   * returnDateTime, delivery, urWillPickup, catClass, and quantity.
   */
  all: {},
  defaultVal: {},

  fetching: false,
};

const actions: ActionTree<ChargeEstimatesState, any> = {
  async [types.estimates.fetch]({ commit }, {
    branchId, startDateTime, returnDateTime, delivery, urWillPickup, items, requisitionId, rpp, accountId, promotionCode, jobsiteId, webRates, useCache,
  }): Promise<ChargeEstimate> {
    commit(types.estimates.loading, true);

    let data;
    try {
      data = await api.chargeEstimates.fetch({
        branchId,
        startDate: startDateTime,
        returnDate: returnDateTime,
        delivery: delivery === 'Y',
        pickup: urWillPickup === 'Y',
        items,
        rpp,
        accountId,
        promotionCode,
        jobsiteId,
        webRates,
        useCache,
      });

      commit(types.estimates.fetch, { data, requisitionId });
    }
    catch (e) {
      console.warn(e);
    }

    commit(types.estimates.loading, false);

    return data;
  },
  async [types.estimates.fetchOnly]({ commit }, input) {

    const {
      branchId,
      delivery,
      items,
      rpp,
      accountId,
      promotionCode,
      startDateTime,
      returnDateTime,
      urWillPickup,
      jobsiteId,
    } = input;

    return api.chargeEstimates.fetch({
      branchId,
      startDate: startDateTime,
      returnDate: returnDateTime,
      delivery: delivery === 'Y',
      pickup: urWillPickup === 'Y',
      items,
      rpp,
      accountId,
      promotionCode,
      jobsiteId,
    });
  },
  [types.estimates.setEmpty]({ commit }, requisitionId) {
    const data = {
      error: true,
    };
    commit(types.estimates.empty, { data, requisitionId });
  },

  async [types.estimates.fetchDefaultVal]({ commit }, {
    branchId, startDateTime, returnDateTime, delivery, urWillPickup, items, requisitionId, rpp, accountId, promotionCode, jobsiteId,
  }): Promise<ChargeEstimate> {
    commit(types.estimates.loading, true);
    let data;
    try {
      data = await api.chargeEstimates.fetch({
        branchId,
        startDate: startDateTime,
        returnDate: returnDateTime,
        delivery: delivery === 'Y',
        pickup: urWillPickup === 'Y',
        items,
        rpp,
        accountId,
        promotionCode,
        jobsiteId,
      });
      commit(types.estimates.fetchDefaultVal, { data, requisitionId });
    }
    catch (e) {
      console.warn('Failed to fetch Default Value for estimate');
    }
    commit(types.estimates.loading, false);

    return data;
  },
};

const mutations: MutationTree<ChargeEstimatesState> = {
  [types.estimates.fetch](state, { data, requisitionId }) {
    Vue.set(state.all, requisitionId, data);
  },
  [types.estimates.fetchDefaultVal](state, { data, requisitionId }) {
    Vue.set(state.defaultVal, requisitionId, data);
  },
  [types.estimates.empty](state, { data, requisitionId }) {
    Vue.set(state.all, requisitionId, data);
  },
  [types.estimates.loading](state, bool) {
    state.fetching = bool;
  },
  ['estimates/reset'](state) {
    state.all = {};
  },
};

const getters = {};

export default {
  actions,
  mutations,
  state,
  getters,
};
