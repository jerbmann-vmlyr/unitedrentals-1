// Branches Store
import {
  cloneDeep,
  isEmpty,
  findIndex,
  forEach,
  merge,
} from 'lodash';
import api from '@/api';
import calc from '@/utils/calculator';
import icons from '@/data/location-finder-icons';
import provinces from '@/data/provinces-hash.json';
import states from '@/data/states-hash.json'; // Source: https://gist.github.com/mshafrir/2646763
import types from '@/store/types';

import L from 'leaflet';
import { ApiV2Utils } from '../lib/apiv2';

const state = {
  /**
   * All property
   *
   * Contains all of the United Rentals branches
   */
  all: null,
  branchesLoaded: false,
  businessTypes: null,
  contentType: 'branch-list',
  currentMobileViewIsMapView: true,
  /**
   * Grid Size property
   *
   * The number in pixels to make the grid of each cluster. Making this larger
   * will give the appearance of one marker per state, but may not give the
   * desired look.
   */
  gridSize: 60,
  loading: true,
  map: {
    active: {
      branchId: null,
      index: null,
    },
    branchesByStateMap: [],
    branchMarkerGroups: [],
    center: { lat: 39.12330, lng: -94.5923 }, // vml
    markerCluster: {},
    markers: [],
    markersByStateMap: [],
    results: null,
    searchMarker: {},
    stateMarkerGroups: [],
    visibleMarkers: [],
    visibleMarkersUnsynced: null,
    zoom: 4,
  },
  mapCache: {},
  mapObject: null, // once the map is instantiated, state.mapObject becomes a fn that returns the leaflet maps instance
  /**
   * Key: Branch ID
   * Value: List of branch indices of nearby branches
   */
  nearbyBranchCache: {},
  place: {},
  previousContentType: 'state-list',
  previousMapListView: 'map',
  provinces,
  states,
  stateBounds: false,
  stateBoundsLoaded: false,
  search: {
    bounds: {},
    distances: [],
    filter: 'all',
    paging: {
      page: 1,
      pageBegin: 1,
      pageEnd: 25,
      pageSize: 25,
      count: 1,
    },
    position: null,
    query: '',
    results: [],
    state: null,
    order: {},
  },
  searchCache: {},
  showScroll: false,
  sync: {
    doSync: true,
    updateFrom: 'search',
  },
  previousRoute: null,
};

const mutations = {
  [types.locationFinder.enterMobileMapView](state, bool) {
    state.previousMapListView = state.currentMobileViewIsMapView;
    state.currentMobileViewIsMapView = bool;
  },

  [types.locationFinder.load](state, branches) {
    state.all = branches;
    state.branchesLoaded = true;
  },

  [types.locationFinder.loadStateBounds](state, list) {
    state.stateBounds = list;
  },

  [types.locationFinder.saveNearbyBranches](state, { branchId, nearbyBranchIds }) {
    state.nearbyBranchCache = {
      ...state.nearbyBranchCache,
      [branchId]: nearbyBranchIds,
    };
  },

  [types.locationFinder.setLoading](state, loading) {
    state.loading = loading;
  },

  [types.locationFinder.setPreviousRoute](state, route) {
    state.previousRoute = route;
  },

  [types.locationFinder.updateSearchDistances](state, distances) {
    state.search.distances = distances;
  },

  [types.locationFinder.updateSearchMarker](state) {
    if (state.search.position && state.search.position.lat) {
      if (isEmpty(state.map.searchMarker)) {
        state.map.searchMarker = L.marker(
          [state.search.position.lat, state.search.position.lng],
          { icon: L.icon(icons.searchLocation) },
        ).addTo(state.mapObject());
      }
      else {
        state.map.searchMarker.setLatLng([state.search.position.lat, state.search.position.lng]);
      }
    }
    else if (state.search.position === null && !isEmpty(state.map.searchMarker)) {
      state.map.searchMarker.remove();
      state.map.searchMarker = {};
    }
  },

  [types.locationFinder.updateVisibleMarkers](state, markers) {
    state.map.visibleMarkers = markers;
    if (state.contentType === 'state-list' && markers.length < 175) {
      state.contentType = 'branch-list';
    }
  },
  [types.businessTypes.fetch](state, businessTypes) {
    state.businessTypes = businessTypes;
  },
  /**
   * Append Branch Data mutator method
   *
   * Adds branch details to a branch.
   *
   * Note: Vue js would parse raw data.businessTypes into a ridiculously funky array of objects...
   *       When the API returns "GR,SM" Vue interprets this string as [{type: "GR"}, {type: "SM"}]
   *       When the API returns "GR" Vue interprets this as the string it is.
   *       So we manually turn it into a proper value (an array of strings) before handing it over to Vue's model.
   *
   * @param {object} state | current state
   * @param {object} payload | {index, data}
   */
  [types.locationFinder.appendBranchData](state) {
    const stateBounds = [];
    forEach(state.all, (branch, idx) => {
      // Set the business types
      if (state.all[idx].businessTypes.length <= 0
        || (state.all[idx].businessTypes.length === 1
          && !state.all[idx].businessTypes[0]
          && state.businessTypes[state.all[idx].businessType])) {
        const branchBusinessType = state.all[idx].businessType;
        state.all[idx].businessTypes[0] = state.businessTypes.find($f => $f.typeMap === branchBusinessType).typeMap;
      }
      // Set all the state boundaries
      const position = L.latLng(parseFloat(branch.latitude), parseFloat(branch.longitude));
      if (!stateBounds[branch.state.toUpperCase().replace(/\W/g, '')]) {
        stateBounds[branch.state.toUpperCase().replace(/\W/g, '')] = L.latLngBounds(position, position);
      }
      else {
        stateBounds[branch.state.toUpperCase()].extend(position);
      }
    });
    state.stateBounds = stateBounds;
  },

  /**
   * Reset Active Branch mutator method
   *
   * Sets the state's active branch to null
   *
   * @param {object} state | the current state
   */
  [types.locationFinder.resetActiveBranch](state) {
    if (!state.map.active.index) {
      return;
    }

    state.map.active = {
      branchId: null,
      index: null,
    };
  },


  [types.locationFinder.resetBranchesLoaded](state) {
    state.branchesLoaded = false;
  },

  /**
   * Set Marker To Selected State mutator method
   *
   * @param {object} state | the current state
   * @param {integer} index | the index of the branch to toggle
   */
  [types.locationFinder.setMarkerToSelectedState](state, index) {
    state.map.active.index = index;
    state.map.active.branchId = state.all[index].branchId;
  },


  [types.locationFinder.setPlace](state, payload) {
    state.place = payload.place;
  },

  [types.locationFinder.setFilter](state, filter) {
    state.search.filter = filter;
  },

  [types.locationFinder.setMap](state, payload) {
    // Cache the previous state of the map to allow users to go back
    state.mapCache = cloneDeep(state.map);

    state.map = {
      ...state.map,
      ...payload,
    };
  },

  [types.locationFinder.setSearch](state, payload) {
    state.search = {
      ...state.search,
      ...payload,
    };
  },

  [types.locationFinder.setSync](state, payload) {
    state.sync = {
      ...state.sync,
      ...payload,
    };
  },

  /**
   * Set Visible Markers mutator
   *
   * Sets the state for visible markers. An array of arrays
   * given in the format of:
   * [
   *  [0: branchIndex, 1: distance]
   *  ...
   * ]
   *
   * @param {object} state | current state
   * @param {object} payload | payload to set to visible markers
   */
  [types.locationFinder.setVisibleMarkers](state, payload) {
    if (payload === null) {
      return;
    }
    state.map.visibleMarkers = payload;
    state.map.visibleMarkerCount = payload.length;

    if (state.sync.doSync || state.sync.updateFrom === 'search') {
      state.map.visibleMarkersUnsynced = payload;
    }
  },

  [types.locationFinder.setMapObject](state, mapObject) {
    // We define a function that returns the original mapObject, unprocessed by vue.
    // (If state.mapObject is assigned mapObject directly, a stack overflow occurs.)
    state.mapObject = function () {
      return mapObject;
    };
  },

  [types.locationFinder.cacheSearch](state) {
    const { center, zoom } = state.mapObject();
    state.searchCache = { center, zoom };
  },

  [types.locationFinder.setBranchMarkerGroups](state, groups) {
    state.map.branchMarkerGroups = groups;
  },

  [types.locationFinder.setStateMarkerGroups](state, groups) {
    state.map.stateMarkerGroups = groups;
  },

  [types.locationFinder.setMarkerCluster](state, layerid) {
    state.map.markerCluster = layerid;
  },

  [types.locationFinder.setMarkers](state, markers) {
    state.map.markers = [];
    state.map.markers = markers;
  },

  [types.locationFinder.updateMarkerClusterMarkers](state) {
    if (typeof state.map.markerCluster.clearLayers !== 'function') {
      return;
    }
    state.map.markerCluster.clearLayers();
    forEach(state.map.branchMarkerGroups, (group, key) => {
      const matchesFilter = state.search.filter === 'all' || state.search.filter.toUpperCase() === key.toUpperCase();
      if (matchesFilter) {
        state.map.markerCluster.addLayer(group);
      }
    });
  },

  [types.locationFinder.updateMarkerClusterStateMarkers](state) {
    if (!state.search.state || typeof state.map.markerCluster.clearLayers !== 'function') {
      return;
    }
    state.map.markerCluster.clearLayers();
    state.map.markerCluster.addLayer(state.map.stateMarkerGroups[state.search.state.toUpperCase()]);
    const center = state.stateBounds[state.search.state.toUpperCase()].getCenter();
    state.search.position = { lat: center.lat, lng: center.lng };
  },

  [types.locationFinder.updateSearchQuery](state, payload) {
    state.search.query = payload.query;
  },

  [types.locationFinder.updateContentType](state, payload) {
    state.previousContentType = state.contentType;
    state.contentType = payload.contentType;
  },
};

/**
 * Actions
 */
const actions = {
  [types.locationFinder.activateBranchDetails]({ commit, state }, { index, branchId }) {
    if (typeof index === 'undefined' && typeof branchId === 'undefined') {
      throw new Error('locationFinder.activateBranchDetails needs an branchId or an index to be defined');
    }

    if (typeof index === 'undefined') {
      index = findIndex(state.all, branch => branch.branchId.toString().toLowerCase() === branchId.toString().toLowerCase());
    }

    if (index !== -1) {
      commit(types.locationFinder.setMarkerToSelectedState, index);
    }
  },

  [types.locationFinder.cacheSearch]({ commit }) {
    commit(types.locationFinder.cacheSearch);
  },

  [types.locationFinder.enterMobileListView]({ commit, dispatch }) {
    let dispatched = false;
    // When we first get here the map object may be null
    // if it is, the method updateVisibleMarkers will
    // throw an error and destroy the function flow
    // so we need to wait for it to load before
    // we can continue on with the rest...
    const waiter = setInterval(() => {
      if (!state.mapObject && !dispatched) {
        dispatch(types.locationFinder.updateVisibleMarkers);
        dispatched = !dispatched;
      }
      else if (!state.mapObject) {
        // eslint-disable-next-line
        console.warn('waiting for map object');
      }
      else {
        commit(types.locationFinder.enterMobileMapView, false);
        clearTimeout(waiter);
      }
    }, 500);
  },

  [types.locationFinder.enterMobileMapView]({ commit }) {
    commit(types.locationFinder.enterMobileMapView, true);
  },

  async [types.locationFinder.getNearbyBranches]({ commit, state }, branchId) {
    const branch = state.all.find(branch => branch.branchId === branchId);
    if (!branch) {
      throw new Error(`There is no branch with ID "${branchId}"!`);
    }
    const branchCoords = { lat: branch.latitude, lng: branch.longitude };
    const response = await api.branches.getNearbyBranch(branchCoords);
    const nearbyBranches = ApiV2Utils.unwrapResponseData(response)
      .filter(nearby => nearby.branchId !== branch.branchId)
      .slice(0, 4)
      .filter(({ latitude: lat, longitude: lng }) => calc.distance(branchCoords, { lat, lng }) <= 100);

    commit(types.locationFinder.saveNearbyBranches, {
      branchId,
      nearbyBranchIds: nearbyBranches.map(({ branchId }) => branchId),
    });
    return nearbyBranches;
  },

  [types.locationFinder.load]({ commit }) {
    return api.branches.getAll().then(ApiV2Utils.unwrapResponseData).then((data) => {
      commit(types.locationFinder.load, data);
      commit(types.locationFinder.appendBranchData);
    });
  },

  [types.locationFinder.getByBranchIndex]({ commit, state }, index) {
    commit(types.locationFinder.setMap, {
      center: {
        lat: parseFloat(state.all[index].latitude),
        lng: parseFloat(state.all[index].longitude),
      },
      zoom: 12,
    });
  },
  [types.locationFinder.loadStateBounds]({ commit }, bounds) {
    commit(types.locationFinder.loadStateBounds, bounds);
  },
  [types.locationFinder.resetActiveBranch]({ commit }) {
    commit(types.locationFinder.resetActiveBranch);
  },

  [types.locationFinder.setBranchMarkerGroups]({ commit }, groups) {
    commit(types.locationFinder.setBranchMarkerGroups, groups);
  },

  [types.locationFinder.setStateMarkerGroups]({ commit }, groups) {
    commit(types.locationFinder.setStateMarkerGroups, groups);
  },

  [types.locationFinder.setFilter]({ commit, dispatch }, filter) {
    commit(types.locationFinder.setFilter, filter);
    commit(types.locationFinder.updateMarkerClusterMarkers);
    dispatch(types.locationFinder.updateVisibleMarkers);
  },

  [types.locationFinder.setLoading]({ commit }, loading) {
    commit(types.locationFinder.setLoading, loading);
  },

  [types.locationFinder.setMap]({ commit }, payload) {
    commit(types.locationFinder.setMap, payload);
  },

  [types.locationFinder.setSearch]({ commit, dispatch }, payload) {
    commit(types.locationFinder.setSearch, payload);
    if ('state' in payload) {
      if (payload.state === null) {
        commit(types.locationFinder.updateMarkerClusterMarkers);
      }
      else {
        // This method also sets the search position
        commit(types.locationFinder.updateMarkerClusterStateMarkers);
        dispatch(types.locationFinder.updateSearchMarker);
        dispatch(types.locationFinder.updateSearchDistances);
      }
    }
    else if ('position' in payload) {
      commit(types.locationFinder.updateMarkerClusterMarkers);
      dispatch(types.locationFinder.updateSearchMarker);
      dispatch(types.locationFinder.updateSearchDistances);
    }
  },

  [types.locationFinder.setSync]({ commit }, payload) {
    commit(types.locationFinder.setSync, payload);
  },

  [types.locationFinder.setMarkerCluster]({ commit }, layerid) {
    commit(types.locationFinder.setMarkerCluster, layerid);
  },
  [types.locationFinder.setMarkers]({ commit }, markers) {
    commit(types.locationFinder.setMarkers, markers);
  },
  [types.locationFinder.setPreviousRoute]({ commit }, route) {
    commit(types.locationFinder.setPreviousRoute, route);
  },
  [types.locationFinder.setPlace]({ commit }, place) {
    commit(types.locationFinder.setPlace, { place });
  },
  [types.locationFinder.updateContentType]({ commit }, contentType) {
    commit(types.locationFinder.updateContentType, { contentType });
  },
  [types.locationFinder.updateSearchDistances]({ commit, state }) {
    if (state.search.position === null) {
      return;
    }
    const distances = {};
    const pos = state.search.position;
    if (state.search.position !== null && state.search.position.lat) {
      forEach(state.all, (branch, idx) => {
        distances[idx] = calc.distance(pos, { lat: branch.latitude, lng: branch.longitude }).toPrecision(3);
      });
    }
    else {
      forEach(state.all, (branch, idx) => {
        distances[idx] = -1;
      });
    }
    commit(types.locationFinder.updateSearchDistances, distances);
  },
  [types.locationFinder.updateSearchMarker]({ commit, dispatch }) {
    if (typeof state.mapObject === 'function') {
      commit(types.locationFinder.updateSearchMarker);
    }
    else {
      setTimeout(() => {
        dispatch(types.locationFinder.updateSearchMarker);
      }, 1000);
    }
  },
  [types.locationFinder.updateMarkerClusterMarkers]({ commit }) {
    commit(types.locationFinder.updateMarkerClusterMarkers);
  },
  [types.locationFinder.updateVisibleMarkers]({ commit, state, dispatch }, bounds = false) {
    if (typeof state.mapObject !== 'function') {
      setTimeout(() => {
        dispatch(types.locationFinder.updateVisibleMarkers);
      }, 500);
    }
    if (state.sync.doSync) {
      if (!bounds && state.search.state !== null) {
        bounds = state.stateBounds[state.search.state.toUpperCase()];
      }
      if (!bounds) {
        bounds = state.mapObject().getBounds();
      }

      // reset visible branches
      const visibleMarkers = [];
      const clusterMarkers = state.map.markerCluster.getLayers();
      const mapByProp = (prop, types) => types.reduce(
        (accum, obj) => ({ ...accum, [obj[prop]]: obj }),
        {},
      );
      const allBranches = mapByProp('branchId', state.all);
      forEach(clusterMarkers, (marker) => {
        if (marker.options.branchId
          && bounds.contains(marker.getLatLng())
          && (state.search.state === null
            || state.all[marker.options.branchIndex].state.toUpperCase() === state.search.state
          )
          && (state.search.filter === 'all'
            || state.search.filter === ''
            || state.search.filter === null
            || (allBranches[marker.options.branchId].businessType.toUpperCase() === state.search.filter.toUpperCase())
          )
        ) {
          visibleMarkers.push(marker.options.branchIndex);
        }
      });
      // Only take the time to sort if we meet branch listings requirement
      if (visibleMarkers.length <= 175 || state.search.state !== '') {
        if (state.search.position) {
          visibleMarkers.sort((a, b) => state.search.distances[a] - state.search.distances[b]);
        }
        else {
          const pos = state.mapObject().getCenter();
          const visibleDistances = {};
          forEach(state.map.visibleMarkers, (marker) => {
            const branch = state.all[marker];
            const d = Math.round(calc.distance(pos, { lat: branch.latitude, lng: branch.longitude }));
            visibleDistances[marker] = d;
          });
          visibleMarkers.sort((a, b) => visibleDistances[a] - visibleDistances[b]);
        }
      }

      commit(types.locationFinder.updateVisibleMarkers, visibleMarkers);
    }
  },
  [types.locationFinder.updateSearchQuery]({ commit }, query) {
    commit(types.locationFinder.updateSearchQuery, { query });
  },
};

const getters = {

  /**
   * Branches By State Map getter
   *
   * @param {object} state | locationFinder state
   * @return {*} null if branches have been loaded otherwise an array of branches by state
   */
  branchesByStateMap: (state) => {
    if (state.all === null) {
      return null;
    }

    const temp = {};
    state.all.forEach((branch, idx) => {
      // Determine the branches by state
      if (!(branch.state in temp)) { // if the state key does not exist
        temp[branch.state] = {};
        temp[branch.state][branch.branchId] = idx;
      }
      else {
        temp[branch.state][branch.branchId] = idx;
      }
    });
    return temp;
  },
  count: (state) => {
    if (state.all === null) {
      return 0;
    }
    return state.all.length;
  },
  indexByBranchId: state => (branchId) => {
    for (let i = 0; i < state.all.length; i++) {
      if (state.all[i].id.toString() === branchId.toString().toUpperCase()) {
        return i;
      }
    }
    throw new Error('No branch with that branch id');
  },
  map: state => state.map,

  [types.locationFinder.nearbyBranches]: state => (branchId) => {
    // Get the nearby branch IDS and return their matching branch objects
    const branchIds = state.nearbyBranchCache[branchId] || [];
    return state.all.filter(branch => branchIds.includes(branch.branchId));
  },

  provinces: state => state.provinces,
  search: state => state.search,
  states: state => state.states,
  statesAndProvinces: (state) => {
    merge(state.states, state.provinces);
  },
  [types.locationFinder.previousRoute]: state => state.previousRoute,
};

export default {
  state,
  getters,
  actions,
  mutations,
};
