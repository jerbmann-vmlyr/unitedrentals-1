import Vue from 'vue';
import { ActionTree, MutationTree, GetterTree } from 'vuex';
import { cloneDeep } from 'lodash';
import api, { FetchEquipmentOnRentRequest, FetchLeniencyRatesRequest } from '@/api';

import { BranchUtils } from '@/lib/branches';
import {
  Leniency, WORKPLACE_EQUIPMENT_COLUMNS, WORKPLACE_EQUIPMENT_FILTERS,
  WORKPLACE_EQUIPMENT_GROUPERS,
  WORKPLACE_EQUIPMENT_SORTERS, WORKPLACE_ORDER_COLUMNS, WORKPLACE_ORDER_FILTERS,
  WORKPLACE_ORDER_GROUPERS,
  WORKPLACE_ORDER_SORTERS,
  WorkplaceCollectionType,
  WorkplaceEquipment,
  WorkplaceEquipmentUtils, WorkplaceListingLayout,
  WorkplaceOrder, WorkplacePredefinedRoutes,
  WorkplaceQueryParams,
  WorkplaceRentalCounts,
  WorkplaceRouteNames,
} from '@/lib/workplace';

import { BranchState } from './module.branch';
import { ChargeEstimatesState } from './module.charge-estimates';
import { InFlightRequestsState } from './module.in-flight-requests';
import { RouteState } from './module.route';
import { TransactionsState } from './module.transactions';
import { ReservationDetailsState } from './module.reservation-details';
import { JobsState } from './module.jobs';
import { UserState } from './module.user';
import { FlightToken } from '@/api/flight-track';
import { AccountsState } from './module.accounts';

import types from './types';
import { ApiV2Utils } from '@/lib/apiv2/utils';
import { CatalogEquipment } from '@/lib/catalog';
import { FetchOrdersRequest } from '@/api/api.workplace-order';

export interface WorkplaceState {
  equipment: { [accountId: string]: WorkplaceEquipment[]; };
  orders: { [accountId: string]: WorkplaceOrder[]; };
  equipmentCallsCompleted: string[]; // a collection of account ids indicating a GET OnRent Equipment call has completed
  ordersCallsCompleted: string[]; // a collection of account ids indicating a GET OnRent Equipment call has completed
  leniencyRates: {
    [transId: string]: {
      [transLineId: number]: Leniency[];
    };
  };
  rentalCounts: { [accountId: string]: WorkplaceRentalCounts; };
  timeOfCall: number | null;
}

/**
 * Import this interface into workplace submodules and use it as
 * the Vuex root state
 */
export type WorkplaceRootState = {
  accounts: AccountsState,
  branch: BranchState,
  catalog: {
    equipment: { [catClass: string]: CatalogEquipment },
    [prop: string]: any,
  },
  estimates: ChargeEstimatesState,
  inFlightRequests: InFlightRequestsState,
  jobs: JobsState,
  route: RouteState & { query: WorkplaceQueryParams },
  readReservationDetails: ReservationDetailsState,
  storageReady: boolean,
  tcUser: {
    altPhone: string,
    firstName: string,
    lastName: string,
    guid: string,
    email: string,
    phone: string,
    pswdExpDays: number,
    form: any,
  },
  transactions: TransactionsState,
  user: UserState,
  userPreferences: UserState,
  workplace: WorkplaceState,
};

const state: WorkplaceState = {
  orders: {},
  equipment: {},
  equipmentCallsCompleted: [],
  ordersCallsCompleted: [],
  leniencyRates: {},
  rentalCounts: {},
  timeOfCall: null,
};

const getters: GetterTree<WorkplaceState, WorkplaceRootState> = {
  /* Current equipment cat classes (used to get equipment images for current collection) */
  [types.workplace.collectionCatClasses](state, getters) {
    const currentWorkplaceResource = getters[types.workplace.currentWorkplaceCollectionType];
    switch (currentWorkplaceResource) {
      case WorkplaceCollectionType.Orders:
        return getters[types.workplace.ordersForCurrentAccount]
          .reduce((carry, order) => [
              ...carry,
              ...order.equipment.reduce((items, item) => items.includes(item.catClass)
                ? [...items]
                : [...items, item.catClass],
                []),
              [],
            ],
            []).filter(exists => !Array.isArray(exists));
      case WorkplaceCollectionType.Equipment:
        return getters[types.workplace.equipmentForCurrentAccount]
          .reduce((carry, item) => carry.includes(item.catClass)
            ? [...carry]
            : [...carry, item.catClass],
            []);
      default:
        return [];
    }
  },
  [types.workplace.listingLayout](state, getters, rootState) {
    const { query } = rootState.route;
    return query.layout ? query.layout : WorkplaceListingLayout.Table;
  },
  /* Current Workplace Collection Filtered */
  [types.workplace.collectionFilters](state, getters) {
    const currentWorkplaceResource = getters[types.workplace.currentWorkplaceCollectionType];
    switch (currentWorkplaceResource) {
      case WorkplaceCollectionType.Equipment:
        return getters[types.workplace.equipmentListing.filters];
      case WorkplaceCollectionType.Orders:
        return getters[types.workplace.orderListing.filters];
      default:
        return window.console.error(`No Column Visibility Route For ${currentWorkplaceResource} workplace/currentWorkplaceCollectionType getter`);
    }
  },
  /* Get Current Collection Column Visibility Route Name */
  [types.workplace.collectionColumnVisibilityRouteName](state, getters) {
    const currentWorkplaceResource = getters[types.workplace.currentWorkplaceCollectionType];
    switch (currentWorkplaceResource) {
      case WorkplaceCollectionType.Equipment:
        return WorkplaceRouteNames.CustomizeEquipmentVisibility;
      case WorkplaceCollectionType.Orders:
        return WorkplaceRouteNames.CustomizeOrderVisibility;
      default:
        return window.console.error(`No Column Visibility Route For ${currentWorkplaceResource} workplace/currentWorkplaceCollectionType getter`);
    }
  },
  /* Get Current Collection Detail Page Name */
  [types.workplace.detailPage](state, getters) {
    const currentWorkplaceResource = getters[types.workplace.currentWorkplaceCollectionType];
    switch (currentWorkplaceResource) {
      case WorkplaceCollectionType.Equipment: return WorkplaceRouteNames.EquipmentDetails;
      case WorkplaceCollectionType.Orders: return WorkplaceRouteNames.OrderDetails;
      default: return WorkplaceCollectionType.Unspecified;
    }
  },
  /* Route to current collection all results */
  [types.workplace.allListingResultsRoute](state, getters) {
    const currentWorkplaceResource = getters[types.workplace.currentWorkplaceCollectionType];
    switch (currentWorkplaceResource) {
      case WorkplaceCollectionType.Equipment: return { ...WorkplacePredefinedRoutes.onRent };
      case WorkplaceCollectionType.Orders: return { ...WorkplacePredefinedRoutes.allOrders };
      default: return WorkplaceCollectionType.Unspecified;
    }
  },
  /* Get Current Collection Listing Page Name */
  [types.workplace.listingPage](state, getters) {
    const currentWorkplaceResource = getters[types.workplace.currentWorkplaceCollectionType];
    switch (currentWorkplaceResource) {
      case WorkplaceCollectionType.Equipment: return WorkplaceRouteNames.EquipmentList;
      case WorkplaceCollectionType.Orders: return WorkplaceRouteNames.OrderList;
      default: return WorkplaceCollectionType.Unspecified;
    }
  },
  /* Get Collection For Current Account */
  [types.workplace.collectionForCurrentAccount](state, getters) {
    const currentWorkplaceResource = getters[types.workplace.currentWorkplaceCollectionType];
    switch (currentWorkplaceResource) {
      case WorkplaceCollectionType.Equipment: return getters[types.workplace.equipmentForCurrentAccount];
      case WorkplaceCollectionType.Orders: return getters[types.workplace.ordersForCurrentAccount];
      default: return WorkplaceCollectionType.Unspecified;
    }
  },
  /* Get Current Columns */
  [types.workplace.columns](state, getters) {
    const currentWorkplaceResource = getters[types.workplace.currentWorkplaceCollectionType];
    switch (currentWorkplaceResource) {
      case WorkplaceCollectionType.Equipment: return WORKPLACE_EQUIPMENT_COLUMNS;
      case WorkplaceCollectionType.Orders: return WORKPLACE_ORDER_COLUMNS;
      default: return WorkplaceCollectionType.Unspecified;
    }
  },
  /* Get Current Filters */
  [types.workplace.filters](state, getters) {
    const currentWorkplaceResource = getters[types.workplace.currentWorkplaceCollectionType];
    switch (currentWorkplaceResource) {
      case WorkplaceCollectionType.Equipment: return WORKPLACE_EQUIPMENT_FILTERS;
      case WorkplaceCollectionType.Orders: return WORKPLACE_ORDER_FILTERS;
      default: return WorkplaceCollectionType.Unspecified;
    }
  },
  /* Get Current Sorters */
  [types.workplace.sorters](state, getters) {
    const currentWorkplaceResource = getters[types.workplace.currentWorkplaceCollectionType];
    switch (currentWorkplaceResource) {
      case WorkplaceCollectionType.Equipment: return WORKPLACE_EQUIPMENT_SORTERS;
      case WorkplaceCollectionType.Orders: return WORKPLACE_ORDER_SORTERS;
      default: return WorkplaceCollectionType.Unspecified;
    }
  },
  /* Get Current Groupers */
  [types.workplace.groupers](state, getters) {
    const currentWorkplaceResource = getters[types.workplace.currentWorkplaceCollectionType];
    switch (currentWorkplaceResource) {
      case WorkplaceCollectionType.Equipment: return WORKPLACE_EQUIPMENT_GROUPERS;
      case WorkplaceCollectionType.Orders: return WORKPLACE_ORDER_GROUPERS;
      default: return WorkplaceCollectionType.Unspecified;
    }
  },
  [types.workplace.currentWorkplaceCollectionType](
    state,
    getters,
    rootState,
  ): WorkplaceCollectionType {
    const resourceNameFromPath = rootState.route.path.split('/')[3];
    switch (resourceNameFromPath)  {
      case WorkplaceCollectionType.Equipment: return WorkplaceCollectionType.Equipment;
      case WorkplaceCollectionType.Orders: return WorkplaceCollectionType.Orders;
      default: return WorkplaceCollectionType.Unspecified;
    }
  },
  [types.workplace.accountId](
    state,
    getters,
    rootState,
  ): string {
    return rootState.route && rootState.route.params
      ? rootState.route.params.accountId || ''
      : '';
  },

  [types.workplace.accountIdIsValid](
    state,
    getters,
    rootState,
  ): boolean {
    const accountId: string = getters[types.workplace.accountId];
    return Object.keys(rootState.accounts.all || {}).includes(accountId);
  },

  [types.workplace.ordersForCurrentAccount](
    state,
    getters,
    rootState,
  ): WorkplaceOrder[] {
    return getters[types.workplace.accountIdIsValid]
      ? (state.orders[getters[types.workplace.accountId]] || [])
      : [];
  },

  [types.workplace.equipmentForCurrentAccount](
    state,
    getters,
    rootState,
  ): WorkplaceEquipment[] {
    return getters[types.workplace.accountIdIsValid]
      ? (state.equipment[getters[types.workplace.accountId]] || [])
      : [];
  },

  [types.workplace.equipmentOnRentRequestsPending](state, getters): boolean {
    return getters[types.inFlightRequests.tokensAreInFlight](
      FlightToken.fetchAccount,
      FlightToken.fetchEquipmentOnRent,
      FlightToken.fetchFacets,
      FlightToken.fetchTransactions,
      FlightToken.fetchReservationDetails,
    );
  },

  [types.workplace.rentalCountsInFlight](state, getters): boolean {
    return getters[types.inFlightRequests.tokensAreInFlight](FlightToken.fetchRentalCounts);
  },

  [types.workplace.rentalCountsForCurrentAccount](
    state,
    getters,
    rootState,
  ): WorkplaceRentalCounts {
    const accountId: string = getters[types.workplace.accountId];

    // Return a default object with all counts set to 0,
    // just in case the count object doesn't exist or the API failed
    return {
      quotes: 0,
      reservations: 0,
      onRent: 0,
      scheduledForPickup: 0,
      overdue: 0,
      allOpen: 0,
      closedContracts: 0,
      custOwn: false,
      ...(state.rentalCounts[accountId] || {}),
    };
  },

  /*
   @TODO: Currently this could lead to showing a flash showing prior to the call being made then
   flash back after the call finishes.
   */
  [types.workplace.currentAccountHasEquipment](state, getters, rootState): boolean {
    const equipmentCount = Object.keys(getters[types.workplace.equipmentForCurrentAccount]).length;
    const onRentRequestInFlight = getters[types.inFlightRequests.tokensAreInFlight](FlightToken.fetchEquipmentOnRent);
    return equipmentCount > 0 && !onRentRequestInFlight;
  },

  /*
   TODO this should be replaced by our In Flight module once it's been beefed up to include meta data
   (about a call having been placed and having returned)
   This fn returns TRUE if a GET OnRent Equipment API call has been made for the current account
   */
  [types.workplace.equipmentForCurrentAccountHasBeenFetched](state, getters): boolean {
    const workplaceAccountId = getters[types.workplace.accountId];
    return state.equipmentCallsCompleted.includes(workplaceAccountId);
  },

  [types.workplace.ordersRequestPending](state, getters) {
    return getters[types.inFlightRequests.tokensAreInFlight](
      FlightToken.fetchAccount,
      FlightToken.fetchOrders,
      FlightToken.fetchFacets,
      FlightToken.fetchTransactions,
      FlightToken.fetchReservationDetails,
    );
  },
};

const actions: ActionTree<WorkplaceState, any> = {
  async [types.workplace.fetchOrders](
    { commit, dispatch, getters, state, rootState },
    { accountId, skipCache, type }: FetchOrdersRequest,
  ): Promise<WorkplaceOrder[]> {
    const data = await api.workplaceOrders.fetchOrders({ accountId, type, skipCache });

    commit(types.workplace.fetchOrders, { accountId, data });
    commit(types.workplace.fetchOrdersCallCompleted, accountId);
    return data;
  },

  async [types.workplace.fetchEquipmentOnRent](
    { commit, dispatch, getters, state, rootState },
    { accountId, useCache, transId, type }: FetchEquipmentOnRentRequest,
  ): Promise<WorkplaceEquipment[]> {
    // Fan out the API calls necessary to build a list of equipment
    const [
      incompleteEquipmentData,
      favorites,
      mappedBranches,
    ] = await Promise.all([

      // Main API call for fetching equipment data
      api.workplaceEquipment.fetchEquipmentOnRent({ accountId, type, transId, useCache }),

      // Extra API calls must be made in order to properly populate each equipment object:
      // 1. favorites: used to set an equipment's "catClassIsFavorite" property
      // This fetchEquipmentOnRent action must not fail to commit equipment state, even if any of the extra calls fail,
      // so provide a fallback catch function that will return a default value in case of API failure.
      // 2. branches: used to create an item's link to branch details
      dispatch(types.favorites.fetchList)
        .then((favorites): string[] => favorites || [])
        .catch(error => {
          console.error(`Failed to fetch favorite catClasses necessary to populate equipment data. Will proceed anyway.`, error);
          return [] as string[];
        }),
      api.branches.getAll()
        .then(ApiV2Utils.unwrapResponseData)
        .then(data => {
          const result = BranchUtils.mapByBranchId(data);
          return result;
        }),
    ]);

    // We take note that equipment has been fetched for this account.
    // (TODO a beefier In-Flight Tracker module would replace this state/mutation)
    commit(types.workplace.fetchEquipmentOnRentCallCompleted, accountId);

    // Merge the incomplete equipment data and the extra API data into
    // the complete equipment set
    const equipmentData = incompleteEquipmentData.map<WorkplaceEquipment>(equipment => ({
      ...equipment,
      branch: mappedBranches[equipment.branchId] || {},
      catClassIsFavorite: favorites.includes(equipment.catClass),
      eqpType: equipment.subcategory || 'Uncategorized',
    }));

    commit(types.workplace.fetchEquipmentOnRent, {
      accountId,
      data: equipmentData,
    });

    return equipmentData;
  },

  [types.workplace.resetWorkplaceData]({ commit }) {
    commit(types.workplace.resetWorkplaceData);
  },

  async [types.workplace.updateEquipmentPo](
    { commit },
    { accountId, po, transId }: { accountId: string, po: string, transId: string },
  ) {
    const response = await api.transactions.updatePo({ po, transId, accountId });
    if (response.data.status !== 0) {
      return Promise.reject(`Update PO Failed: ${response.data.messageText || response.data}`);
    }
    commit(types.workplace.updateEquipmentPo, { accountId, po, transId });
    return response;
  },

  async [types.workplace.fetchLeniencyRates](
    { commit },
    payload: FetchLeniencyRatesRequest,
  ): Promise<object> {
    const response = await api.workplaceEquipment.fetchLeniencyRates(payload);
    commit(types.workplace.setLeniencyRates, { ...payload, leniencies: response.data });

    return response;
  },

  async [types.workplace.fetchRentalCounts](
    { commit },
    payload: { accountId: string },
  ): Promise<WorkplaceRentalCounts> {
    const counts = await api.workplaceEquipment.fetchRentalCounts(payload);
    commit(types.workplace.setRentalCounts, { ...payload, counts });
    return counts;
  },

};

const mutations: MutationTree<WorkplaceState> = {
  [types.workplace.fetchOrders](
    state,
    { accountId, data}: { accountId: string, data: WorkplaceOrder[] },
  ) {
    Vue.set(state.orders, accountId, data);
    state.timeOfCall = Date.now();
  },


  [types.workplace.fetchEquipmentOnRent](
    state,
    { accountId, data }: { accountId: string, data: WorkplaceEquipment[] },
  ) {
    Vue.set(state.equipment, accountId, data);
    state.timeOfCall = Date.now();
  },

  [types.workplace.fetchEquipmentOnRentCallCompleted](state, accountId) {
    state.equipmentCallsCompleted = [...state.equipmentCallsCompleted, accountId];
  },
  [types.workplace.fetchOrdersCallCompleted](state, accountId) {
    state.ordersCallsCompleted = [...state.ordersCallsCompleted, accountId];
  },

  [types.workplace.resetWorkplaceData](state) {
    state.orders = {};
    state.ordersCallsCompleted = [];
    state.equipment = {};
    state.equipmentCallsCompleted = [];
    state.leniencyRates = {};
    state.rentalCounts = {};
    state.timeOfCall = null;
  },

  [types.workplace.updateEquipmentPo](state, { accountId, transId, po }) {
    // Update the PO of each piece of equipment in the transaction
    Object.values(state.equipment[accountId])
      .filter(eqp => eqp.transId === transId)
      .forEach(eqp => Vue.set(eqp, 'po', po));

    Object.values(state.orders[accountId])
      .filter(order => order.orderNumber === transId)
      .forEach(order => Vue.set(order, 'po', po));
  },

  [types.workplace.setLeniencyRates](state, { leniencies }) {
    const newCopy = cloneDeep(state.leniencyRates);
    Object.keys(leniencies).forEach(transId => {
      if (!newCopy[transId]) {
        newCopy[transId] = [];
      }
      Object.keys(leniencies[transId]).forEach(lineNumber => {
        newCopy[transId][lineNumber] = Object.values(leniencies[transId][lineNumber]);
      });
    });
    Vue.set(state, 'leniencyRates', newCopy);
  },

  [types.workplace.setRentalCounts](
    state,
    { accountId, counts }: { accountId: string, counts: WorkplaceRentalCounts },
  ) {
    // filter out counts.x === null and rebuild new counts object
    const noNullCounts = Object.entries(counts)
      .filter(([key, value]) => value !== null)
      .reduce((newCounts, [k, v]) => ({ ...newCounts, [k]: v }), {});

    // merge empty object and actual non-null counts results
    const safeMergedCounts = { ...WorkplaceEquipmentUtils.getEmptyEquipmentCounts(), ...noNullCounts };
    Vue.set(state.rentalCounts, accountId, safeMergedCounts);
  },
};

export default {
  state,
  actions,
  getters,
  mutations,
};
