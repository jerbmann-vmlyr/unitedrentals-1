import * as moment from 'moment';
import { get } from 'lodash';
import { ActionTree, GetterTree, MutationTree } from 'vuex';
import { DateFormat } from '@/lib/constants';
import {
  Leniency,
  WorkplaceEquipment,
  WorkplaceEquipmentGps,
  WorkplaceEquipmentOrderStatuses,
  WorkplaceEquipmentCoordinates,
  WorkplaceEquipmentUsageByDay,
  WorkplaceEquipmentUtils,
} from '@/lib/workplace';
import { WrappedPlaceResult } from '@/lib/google-maps';
import { StatusUtils } from '@/lib/statuses';
import { Transaction, Requisition, CONTRACT_STATUSES, QUOTE_STATUSES, RESERVATION_STATUSES } from '@/lib/transactions';
import { ChargeEstimate } from '@/lib/estimates';
import api, { FetchLeniencyRatesRequest } from '@/api';
import { WorkplaceEquipmentLocationState } from './module.workplace.equipment-location';
import { WorkplaceRootState } from './module.workplace';
import types from './types';

interface WorkplaceEquipmentDetailsRootState extends WorkplaceRootState {
  workplaceEquipmentLocation: WorkplaceEquipmentLocationState;
}

export interface WorkplaceEquipmentDetailsState {
  detailItemCoordinates: WorkplaceEquipmentCoordinates;
  detailItemCoordinatesPending: boolean;
  detailItemUsageByDay: WorkplaceEquipmentUsageByDay;
  detailItem: WorkplaceEquipment;
}

const state: WorkplaceEquipmentDetailsState = {
  detailItemCoordinates: WorkplaceEquipmentUtils.getEmptyEquipmentCoordinates(),
  detailItemCoordinatesPending: false,
  detailItemUsageByDay: {},
  detailItem: {} as WorkplaceEquipment,
};

const actions: ActionTree<WorkplaceEquipmentDetailsState, WorkplaceEquipmentDetailsRootState> = {
  async [types.workplace.equipmentDetails.fetchItemCoordinates](
    { commit, dispatch, getters, rootState },
  ): Promise<WorkplaceEquipmentCoordinates> {

    commit(types.workplace.equipmentDetails.setDetailItemCoordinatesPending, true);

    const commitAndReturn = (coords: WorkplaceEquipmentCoordinates) => {
      commit(types.workplace.equipmentDetails.setDetailItemCoordinatesPending, false);
      commit(types.workplace.equipmentDetails.setItemCoordinates, coords);
      return coords;
    };

    const emptyCoordinates = WorkplaceEquipmentUtils.getEmptyEquipmentCoordinates();
    const item: WorkplaceEquipment | null = getters[types.workplace.equipmentDetails.detailItemOrNull];

    if (!item) {
      return commitAndReturn(emptyCoordinates);
    }

    /**
     * 1. Parse the item's coordinates and use them if they're valid
     * 2. If parsed coordinates aren't valid, use the jobsite lat, lng from
     *    rootState.workplaceEquipmentLocation.jobsiteLocations[accountId][jobsite.id]
     * 3. If that's not available, attempt to geocode lat, lng from jobsite string address
     * 4. If that's not available, attempt to geocode lat, lng from jobsite zip code
     * 5. If that's not available, attempt to geocode lat, lng from jobsite city
     * 6. If that's not available, attempt to geocode lat, lng from jobsite state
     * 7. If that's not available, return an empty coordinates object
     */

    // 1. Parse the item's coordinates and use them if they're valid
    const parsedCoords = WorkplaceEquipmentUtils.parseCoordinates(item);
    if (parsedCoords.valid) {
      return commitAndReturn(parsedCoords);
    }

    // 2. If parsed coordinates aren't valid, use the jobsite lat, lng
    //    from rootState.workplaceEquipmentLocation.jobsiteLocations[accountId][jobsite.id]
    const { jobsite } = item;
    const jobsiteGeolocation = get(
      rootState.workplaceEquipmentLocation.jobsiteLocations,
      [item.accountId, item.jobsite.id],
      {},
    );

    if (!!jobsiteGeolocation && typeof jobsiteGeolocation.lat === 'number') {
      return commitAndReturn({
        valid: true,
        latitude: jobsiteGeolocation.lat,
        longitude: jobsiteGeolocation.lng,
      });
    }

    // 3. If that's not available, attempt to geocode lat, lng from jobsite string address
    const address = WorkplaceEquipmentUtils.serializeJobsiteAddress(jobsite);
    let result: WrappedPlaceResult = await dispatch(
      types.googleMaps.getPlaceFromAddress,
      address,
    );

    // 4. If that's not available, attempt to geocode lat, lng from jobsite zip code
    if (result.place === null && jobsite.zip) {
      result = await dispatch(types.googleMaps.getPlaceFromAddress, jobsite.zip);
    }

    // 5. If that's not available, attempt to geocode lat, lng from jobsite city
    if (result.place === null && jobsite.city) {
      result = await dispatch(types.googleMaps.getPlaceFromAddress, jobsite.city);
    }

    // 6. If that's not available, attempt to geocode lat, lng from jobsite state
    if (result.place === null && jobsite.state) {
      result = await dispatch(types.googleMaps.getPlaceFromAddress, jobsite.state);
    }

    if (result.place !== null) {
      return commitAndReturn({
        valid: true,
        latitude: result.place.geometry.location.lat(),
        longitude: result.place.geometry.location.lng(),
      });
    }
    // 7. If that's not available, return an empty GPS object
    else {
      return commitAndReturn(emptyCoordinates);
    }
  },

  async [types.workplace.equipmentDetails.fetchItemUsage](
    { commit, getters, rootState },
  ): Promise<WorkplaceEquipmentGps[]> {
    const item: WorkplaceEquipment | null = getters[
      types.workplace.equipmentDetails.detailItemOrNull
    ];
    if (!item) return [];

    const { accountId, equipmentId } = rootState.route.params;

    // We only fetch Usage Data for Equipment that is Type 1 (Open Contract) and is GPS Enabled
    const [ gps ] = await (item.simActive
      ? api.workplaceEquipment.fetchGps({ accountId, equipmentId })
      : []);

    commit(types.workplace.equipmentDetails.setItemUsage, !!gps && gps.usageByDay || {});
    return !!gps ? [gps] : [];
  },

  async [types.workplace.equipmentDetails.getEstimates](
    { dispatch, getters },
  ): Promise<ChargeEstimate> {
    const item: WorkplaceEquipment | null = getters[
      types.workplace.equipmentDetails.detailItemOrNull
    ];

    const reservationDetails: Requisition | null = getters[
      types.workplace.equipmentDetails.reservationDetailsOrNull
    ];

    if (!item || !reservationDetails) {
      return Promise.resolve({
        delivery: false,
        pickup: false,
        rpp: false,
        branchId: '',
        accountId: '',
        jobsiteId: '',
        items: {},
        totals: {} as any,
        returnDate: '',
        startDate: '',
      });
    }

    return dispatch(types.estimates.fetch, {
      branchId: item.branchId,
      startDateTime: item.startDateTime,
      returnDateTime: item.returnDateTime,
      delivery: reservationDetails.delivery,
      urWillPickup: reservationDetails.urWillPickup,
      items: reservationDetails.items,
      requisitionId: item.requisitionId,
      rpp: reservationDetails.rpp === 'Y',
      accountId: item.accountId,
      jobsiteId: item.jobsite.id,
    });
  },

  async [types.workplace.equipmentDetails.fetchCatClassByEquipmentId]({ commit }, equipmentId): Promise<string> {
    return api.catalog.fetchCatClassByEquipmentId(equipmentId);
  },

  [types.workplace.equipmentDetails.resetItemUsage]({ commit }) {
    commit(types.workplace.equipmentDetails.setItemUsage, {});
  },

  [types.workplace.equipmentDetails.resetItemCoordinates]({ commit }) {
    commit(
      types.workplace.equipmentDetails.setItemCoordinates,
      WorkplaceEquipmentUtils.getEmptyEquipmentCoordinates(),
    );
  },
};

const getters: GetterTree<WorkplaceEquipmentDetailsState, WorkplaceRootState> = {
  [types.workplace.equipmentDetails.detailItemOrNull](
    state,
    getters,
    rootState,
  ): WorkplaceEquipment | null {
    // Pull the equipment and transaction IDs from the route params
    const { equipmentId, transId, transLineId } = rootState.route
      ? rootState.route.params
      : { equipmentId: '', transId: '', transLineId: '' };

    // Find the single equipment object that matches the params
    const equipment: WorkplaceEquipment[] = getters[
      types.workplace.equipmentForCurrentAccount
    ];
    const equipmentMatch = equipment.find(eqp =>
      eqp.equipmentId === equipmentId
      && eqp.transId === transId
      && eqp.transLineId === transLineId,
    );

    return equipmentMatch || null;
  },

  [types.workplace.equipmentDetails.estimatesOrNull](
    state,
    getters,
    rootState,
  ): ChargeEstimate | null {
    const detailItem: WorkplaceEquipment | null = getters[
      types.workplace.equipmentDetails.detailItemOrNull
    ];

    if (!detailItem) return null;

    const estimate = rootState.estimates.all[detailItem.requisitionId];

    return estimate || null;
  },

  /**
   * Returns a predefined request payload that can be used to fetch
   * the leniency rates for the currently active equipment item
   * Will return `null` if there is not currently active equipment item
   */
  [types.workplace.equipmentDetails.fetchLeniencyRatesRequestOrNull](
    state,
    getters,
    rootState,
  ): FetchLeniencyRatesRequest | null {
    const detailItem: WorkplaceEquipment | null = getters[
      types.workplace.equipmentDetails.detailItemOrNull
    ];

    const daysNeededEndDate = !detailItem
      ? moment()
      : moment().isAfter(detailItem.startDateTime)
      ? moment()
      : moment(detailItem.startDateTime);

    return !detailItem
      ? null
      : {
        contract: detailItem.transId,
        daysNeeded: daysNeededEndDate.diff(detailItem.startDateTime, 'days') + 60,
        lineNum: `${detailItem.transLineId}`,
        startDate: moment(detailItem.startDateTime).format(DateFormat.IntDate),
      };
  },

  /**
   * Extracts the leniency rates from the root workplace module for the
   * currently active equipment item
   */
  [types.workplace.equipmentDetails.leniencyRates](
    state,
    getters,
    rootState,
  ): Leniency[] {
    const detailItem: WorkplaceEquipment | null = getters[
      types.workplace.equipmentDetails.detailItemOrNull
    ];

    return !detailItem
      ? []
      : get(
      rootState.workplace.leniencyRates,
      [ detailItem.transId, detailItem.transLineId ],
      [],
    );
  },

  [types.workplace.equipmentDetails.orderStatuses](
    state,
    getters,
  ): WorkplaceEquipmentOrderStatuses {
    const detailItem: WorkplaceEquipment | null = getters[
      types.workplace.equipmentDetails.detailItemOrNull
    ];

    const reservationDetails: Requisition | null = getters[
      types.workplace.equipmentDetails.reservationDetailsOrNull
    ];

    if (!detailItem || !reservationDetails) {
      return {
        contractStatuses: [],
        quoteStatuses: [],
        reservationStatuses: [],
      };
    }
    else {
      /**
       * HACK: We have to trick the transaction status utilities into thinking we're
       * giving them a `Transaction`. To do that, we cast `detailsItem` to `any`.
       */
      const item: Transaction = { ...detailItem as any, requisitionStatusCode: reservationDetails.statusCode };
      return {
        contractStatuses: StatusUtils.getApplicableStatuses(CONTRACT_STATUSES, item),
        quoteStatuses: StatusUtils.getApplicableStatuses(QUOTE_STATUSES, item),
        reservationStatuses: StatusUtils.getApplicableStatuses(RESERVATION_STATUSES, item),
      };
    }
  },

  [types.workplace.equipmentDetails.reservationDetailsOrNull](
    state,
    getters,
    rootState,
  ): Requisition | null {
    const detailItem: WorkplaceEquipment | null = getters[types.workplace.equipmentDetails.detailItemOrNull];
    return !detailItem
      ? null
      : get(rootState.readReservationDetails.all, [detailItem.requisitionId], null);
  },
};

const mutations: MutationTree<WorkplaceEquipmentDetailsState> = {
  [types.workplace.equipmentDetails.setDetailItemCoordinatesPending](state, pending: boolean) {
    state.detailItemCoordinatesPending = pending;
  },

  [types.workplace.equipmentDetails.setItemCoordinates](state, coords: WorkplaceEquipmentCoordinates) {
    state.detailItemCoordinates = { ...coords };
  },

  [types.workplace.equipmentDetails.setItemUsage](state, usage: WorkplaceEquipmentUsageByDay) {
    state.detailItemUsageByDay = { ...usage };
  },
};

export default {
  state,
  actions,
  getters,
  mutations,
};
