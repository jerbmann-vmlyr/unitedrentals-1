import { UserProfiler } from './types';

export const userProfilers: UserProfiler[] = [
  /**
   * cartContainsItems
   * cartContainsMultipleOrders
   * cartIsEmpty
   */
  {
    watchers: [
      state => Object.values(state.cart.items),
    ],
    async getPartialProfile(store) {
      const cartSize: number = store.getters.cartSize || 0;
      return {
        cartContainsItems: cartSize > 0,
        cartIsEmpty: cartSize <= 0,
        cartContainsMultipleOrders: cartSize,
      };
    },
  },
  /**
   * favoritesExist
   */
  {
    watchers: [
      state => state.favorites.all,
    ],
    async getPartialProfile(store) {
      return {
        favoritesExist: store.state.favorites.all.length || 0,
      };
    },
  },
  /**
   * isOrsCustomer
   * isTcCustomer
   * isUrcCustomer
   */
  {
    watchers: [
      state => state.user.defaultAccountId,
      state => state.accounts.all,
    ],
    async getPartialProfile(store) {
      const allAccounts = store.state.accounts.all || {};
      const accountId = store.state.user.defaultAccountId || 0;
      const { tcLevel = '' } = allAccounts[accountId] || {};
      return {
        isOrsCustomer: Object.keys(allAccounts).length < 1,
        isTcCustomer: tcLevel === 'C',
        isUrcCustomer: tcLevel === 'E',
      };
    },
  },
  /**
   * jobsiteIsSpecified
   */
  {
    watchers: [
      state => state.user.defaultJobId,
    ],
    async getPartialProfile(store) {
      return {
        jobsiteIsSpecified: store.state.user.defaultJobId.length > 0,
      };
    },
  },
  /**
   * locationIsKnown
   */
  {
    watchers: [],
    async getPartialProfile(store) {
      if (Object.keys(store.state.user.geolocation).length !== 0) {
        return { locationIsKnown: true };
      }
      return { locationIsKnown: false };
    },
  },
  /**
   * quotesExist
   */
  {
    watchers: [
      state => state.quotes.all,
    ],
    async getPartialProfile(store) {
      return {
        quotesExist: store.getters.quotesForCurrentAccount.length || 0,
      };
    },
  },
  /**
   * reservationsExist
   */
  {
    watchers: [
      state => state.reservations.all,
    ],
    async getPartialProfile(store) {
      return {
        reservationsExist: store.getters.reservationsForCurrentAccount.length || 0,
      };
    },
  },
  /**
   * userHasMultipleAccounts
   * userHasOneAccount
   */
  {
    watchers: [
      state => Object.keys(state.accounts.all).length,
    ],
    async getPartialProfile(store) {
      const accountCount = Object.keys(store.state.accounts.all).length;
      return {
        userHasMultipleAccounts: accountCount > 1,
        userHasOneAccount: accountCount === 1,
      };
    },
  },
  /**
   * userIsAuthenticated
   */
  {
    watchers: [
      state => state.user.isLoggedIn,
    ],
    async getPartialProfile(store) {
      return {
        userIsAuthenticated: store.state.user.isLoggedIn,
      };
    },
  },
];
