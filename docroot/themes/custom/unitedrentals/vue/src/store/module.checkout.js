import Vue from 'vue';
import { dateFormat } from '@/utils';
import { cloneDeep, find, get, isEmpty } from 'lodash';
import types from '@/store/types';
import api from '@/api';

import { Progress } from '@/lib/cart';

export const makeEstimateCacheKey = apiRequestParams => JSON.stringify(apiRequestParams);

const state = {

  /**
   * Extra billing data, keyed by reservation block id's
   */
  billing: {

    // Example:
    // reservationBlockId: {
    //   accountingCodes: [],
    //   cardId: 'axdkwko38s9',
    //   coType: '',
    //   coValue: '',
    //   deliveryNote: '',
    //   poMessageDraft: {
    //    senderCode: null,
    //    message: ''
    //   },
    //   selectedPo: {},
    //   purchaseOrders: [],
    //   getPurchaseOrdersLoading: false,
    //   getPurchaseOrdersError: null,
    //   poNumber: null,
    //   poLineNumber: null,
    //   requisitionCodes: [],
    //   rpp: 'Y',
    //   transType: 'R' // or 'X'.  R is Reservation, X is Quote
    // }
  },

  /**
   * Transaction Estimates, keyed by Estimate API request params, and acts as a cache
   * for every estimates call the app makes.
   *
   * To get a reservation's current estimate, use the getEstimate getter in this module.
   */
  estimates: {},

  /**
   * Anonymous users are presented a Login or Continue modal up starting checkout.
   * We track if they've seen this modal to ensure we only show it once.
   */
  hasShownAuthenticateToContinueModal: false,

  /**
   * Transaction Results.
   */
  transactionResults: {},

  /**
   *  Copies of reservation blocks get set here, after they successfully check out.
   * (The API doesn't give us back valid transaction data, so we just copy it from what we POST).
   */
  transactionBlocks: {},

  /**
   * Stores the promotion code for use in estimates call
   */
  promotionCode: '',

  /**
   * A record of all the progresses of all the reservation blocks.
   */
  progressOfAllReservationBlocks: [],
  startUnauthenticatedCheckout: false,
  isSubmitting: false,
  isReservation: false,
  hideOverlayImmediately: false,
};

const getters = {
  [types.checkout.getEstimate]: (state, getters) => (reservationBlockId, override) => {
    // TODO these shouldn't be mixed, but for now are
    // When a transaction is created, before the reservationBlock is wiped out, the estimates get cloned
    // to the old estimates keys (reservation block ids).
    if (state.transactionBlocks[reservationBlockId]) {
      return state.estimates[reservationBlockId];
    }

    const reservationBlock = getters.reservationBlocks[reservationBlockId];

    if (!reservationBlock) {
      return {};
    }

    const apiRequestParams = reservationBlock.exportEstimateAPIParams(override);
    const accessor = makeEstimateCacheKey(apiRequestParams);

    return state.estimates[accessor] || {};
  },
  [types.checkout.hasEstimate]: (state, getters) => reservationBlockId => Object.keys(getters[types.checkout.getEstimate](reservationBlockId)).length, // eslint-disable-line

  /**
   * Returns parameters for a POST to /api/v1/transaction (to create a transaction)
   *
   * @param {object} state | vuex
   * @param {object} getters | vuex
   * @param {object} rootState | vuex
   * @return {object} parameters for a POST to /api/v1/transaction
   */
  transactionParams: (state, getters, rootState) => (reservationBlockId) => {
    const reservationBlock = getters.reservationBlocks[reservationBlockId];

    const account = get(rootState.accounts.all, [reservationBlock.accountId], {});

    const billing = state.billing[reservationBlockId];
    const jobsite = rootState.jobs.all[reservationBlock.accountId][reservationBlock.jobsiteId];
    const estimate = getters[types.checkout.getEstimate](reservationBlockId);
    const accountingCodes = billing.accountingCodes || [];
    const requisitionCodes = billing.requisitionCodes || [];

    const requester = get(rootState.requesters.all, [reservationBlock.accountId, reservationBlock.requesterId], {});
    const approver = get(rootState.approvers.all, [reservationBlock.accountId, billing.approverId], {});
    const instructions = jobsite.notes;
    return JSON.stringify({
      transaction: {
        instructions,
        branchId: reservationBlock.branchId,
        accountId: reservationBlock.accountId,
        cardId: billing.cardId,
        transType: billing.transType,
        jobsiteId: reservationBlock.jobsiteId,
        jobsiteContact: jobsite.contact || 'No Contact', // Required, but not all legacy accounts have one
        jobsitePhone: jobsite.mobilePhone ? jobsite.mobilePhone.replace(/\D/g, '') : '8165551212',
        startDateTime: reservationBlock.startMoment.format(dateFormat.iso),
        returnDateTime: reservationBlock.endMoment.format(dateFormat.iso),
        delivery: reservationBlock.delivery,
        po: billing.poNumber,
        pickup: reservationBlock.pickup,

        // This was removed to fix a bug (of sending TRUE all the time) on 3/22/2018. UR-2415.
        // was previously pickupFirm: billing.pickupFirm,
        // updated Oct 2019 when functionality re-enabled
        pickupFirm: reservationBlock.pickupFirm,

        approver: {
          approverEmail: approver.email,
          approverName: `${approver.firstName} ${approver.lastName}`,
          approverGuid: approver.guid,
        },
        requesterName: requester.name,
        requesterPhone: requester.phone,
        requester: requester.email,
        rpp: typeof billing.rpp === 'undefined' ? (account.rppDefault === 'Y') : billing.rpp,
        itemIds: reservationBlock.items.map(({ id }) => id),

        // Item Data is aggregated from the Reservation's Estimate & the Cart Item and shaped for the API
        items: Object.entries(estimate.items)
          .reduce((items, [catClass, item]) => {
            const {
              dayRate, weekRate, monthRate, minRate, quantity,
            } = item;

            // Get the notes field off the cart data. (The item's estimate
            // does not have this data.)
            const itemMatchedFromCart = reservationBlock.items.filter(i => i.catClass === catClass)[0];

            // Add the item to the list of items, shaped for the api
            items[catClass] = {
              catClass,
              quantity,
              notes: itemMatchedFromCart.notes,
              discountApplied: !isEmpty(reservationBlock.promotionCode) && !item.hasContractPricing,
              rate: {
                dayrate: dayRate,
                weekrate: weekRate,
                monthrate: monthRate,
                minrate: minRate,
              },
            };

            return items;
          }, {}),
        totals: estimate.totals,
        promotionCode: reservationBlock.promotionCode,
      },
      allocationCodes: accountingCodes,
      requisitionCodes,
    });
  },

  // Returns the estimated rates for a single item in the cart
  // Overloaded to look for a cart item or a transaction item
  // Looks through a transaction if the optional transactionId param is passed.

  // TODO
  /*
  GetEstimate and getCartItemEstimate should be reworked into 3 fns:

  getEstimateByReservationId
  getEstimateByTransactionId
  getEstimateByCartItemId

  Each of these methods should map to an esimate in state.checkout.estimates
   */
  getCartItemEstimate: (state, getters, rootState) => (cartItemId, transactionBlockId) => {
    const reservationBlock = find(getters.reservationBlocks, block => block.items.map(({ id }) => id).includes(cartItemId));

    // In this condition, we are procuring estimates for a Save for Later Item.
    if (!reservationBlock && !transactionBlockId) {
      // Assume we have to fetch the estimate for an individual item within Save For Later
      const cartItem = rootState.cartSaveForLater.items[cartItemId];

      if (!cartItem) {
        return [];
      }

      const accessor = makeEstimateCacheKey(cartItem.exportAsEstimateApiParams());
      return get(state.estimates, [accessor, 'items', cartItem.catClass], {});
    }

    const estimate = state.estimates[transactionBlockId] || getters[types.checkout.getEstimate](reservationBlock.id);

    const cartModule = rootState.cart;
    const source = get(state.transactionBlocks, [transactionBlockId], cartModule);
    const catClass = get(source.items, [cartItemId, 'catClass'], get(find(source.items, item => item.id === cartItemId), ['catClass']));

    return get(estimate, ['items', catClass], {});
  },

  // Returns the reservation block that matches the provided ID
  // WARNING: Will return null if no matching reservation is found!
  billingBlock: state => (reservationBlockId) => {
    const matchingBlockId = Object.keys(state.billing).find(id => id === reservationBlockId);
    return matchingBlockId ? state.billing[matchingBlockId] : null;
  },
  [types.checkout.promotionCodeActive]: () => window.drupalSettings.urVoucher,

  // Counts for all reservation blocks' progress
  [types.checkout.progressCounts]: (state) => {
    const defaultProgress = {
      // This level is the "form name"
      jobsite: {
        // This level is the "progress"
        [Progress.INACTIVE]: 0,
        [Progress.READY]: 0,
        [Progress.ACTIVE]: 0,
        [Progress.SUMMARY]: 0,
      },
      pickupDelivery: {
        [Progress.INACTIVE]: 0,
        [Progress.READY]: 0,
        [Progress.ACTIVE]: 0,
        [Progress.SUMMARY]: 0,
      },
      billing: {
        [Progress.INACTIVE]: 0,
        [Progress.READY]: 0,
        [Progress.ACTIVE]: 0,
        [Progress.SUMMARY]: 0,
      },
    };

    return state.progressOfAllReservationBlocks.reduce((accumulated, current) => {
      Object.entries(current.progress).forEach(([formName, progress]) => {
        accumulated[formName][progress] += 1;
      });
      return accumulated;
    }, defaultProgress);
  },
};

const actions = {
  [types.checkout.toggleSubmitting]: ({ commit }, isSubmitting) => {
    commit(types.checkout.setSubmitting, isSubmitting);
  },
  // This creates an account and jobsite for Guests or Registered Users w/ userHasNoAccount (Checkout flow.)
  /** TODO: This method needs to be rewritten */
  async [types.checkout.createAccountAndJobsite]({
    commit, dispatch, state, rootState,
  }) {
    const accountPayload = { ...rootState.accounts.form };
    const jobsitePayload = { ...rootState.jobs.form };

    // Reconcile Jobsite phone field with Account phone field (if the account phone field doesn't exist
    accountPayload.phone = accountPayload.phone || jobsitePayload.mobilePhone.replace(/[()]/g, '');

    delete accountPayload.countryCode;

    // Add the flag for an account being for an Individual or not.
    // NOTE: sending nothing for individual results in 'C'.
    // If we send TRUE for this field, rppDefault gets set to 'N'
    if (rootState.accounts.form.name.length === 0) {
      accountPayload.individual = true;

      // Then set their account name to their first/last name
      const { firstName, lastName } = rootState.tcUser.form;
      accountPayload.name = `${firstName} ${lastName}`;
    }

    return new Promise(async (resolve, reject) => {
      const { data } = state.failedJobsiteCreation
        ? { data: state.failedJobsiteCreation }
        : await api.accounts.create(accountPayload);

      // Expect data to be an integer (accountId). Errors will come in as an array with a message.
      if (Array.isArray(data)) {
        reject(new Error(`${data}`));
      }

      // The DAL/Kelex only sends us back an ID...
      const accountId = String(data);

      // Save the new Account id to the Account Form, so it can be updated during the Billing step TODO necessary?
      dispatch(types.accounts.updateFormField, { accountId });

      // Fetch the full data for the account we just created.
      // (This data is required only for subsequent reservations the user may still have on-page.)
      // TODO api/v2/accounts/create would be improved by sending back a full acct object. See our Nice to Have doc:
      // TODO https://docs.google.com/spreadsheets/d/19YkqSq5wz0Y9uVBYXiBekTjvlhG0eLPg_Jr8ciat2uI/edit#gid=2031443200
      await dispatch(types.accounts.fetch);

      // IE has a bug where the jobsite form has been reset. (Due to a mounting of jobsite.vue???)
      // This ensures we still send the form data to the Add Jobsite action
      const jobsiteFormWasReset = !rootState.jobs.form.address1;
      if (jobsiteFormWasReset) {
        Object.keys(jobsitePayload).forEach((key) => {
          dispatch(types.jobs.updateFormField, { [key]: jobsitePayload[key] });
        });
      }

      const jobsiteId = await dispatch(types.jobs.addJobsite, accountId).catch(() => {
        commit('accounts/failedJobsiteCreation', accountId);
        return reject(jobsiteId);
      });

      // Update all cart items to get the new account and jobsite
      Object.keys(rootState.cart.items).forEach((id) => {
        dispatch(types.cart.updateItem, { id, field: { accountId, jobsiteId } });
      });

      // Update the User's Default Account Id and Default Jobsite Id. This allows Guest Users
      // to go back to the marketplace and add items to their cart under this account/jobsite
      commit(types.user.setDefaultAccountId, accountId);
      dispatch(types.user.defaultJobId, jobsiteId);

      // Both Registered Users and Guests only create an Account & Jobsite during checkout.  However, they
      // may have multiple reservations.  So once their jobsite/account is created, tie ALL CART ITEMS
      // to the new job/account.
      dispatch(types.cart.updateAllItems, { accountId, jobsiteId });

      // Resolve the promise chain so the component is notified when all ajax calls
      // have completed
      resolve({ accountId, jobsiteId });
    });
  },
  [types.checkout.dispatchButtonClickType]: ({ commit }, isReservation) => {
    commit(types.checkout.setButtonClickType, isReservation);
  },
  [types.checkout.toggleHideOverlayImmedately]: ({ commit }, hideImmediately) => {
    commit(types.checkout.setHideOverlayImmediately, hideImmediately);
  },
  [types.checkout.getUpdateReservationAction]: ({ dispatch }, reservationId) => (fields) => { // eslint-disable-line
    return dispatch(types.cart.updateItemsByReservationId, { reservationId, ...fields });
  },
  async [types.checkout.fetchEstimates]({ commit, getters }, { reservationBlockId, override, noCache }) {
    noCache = !!noCache;
    // Return a cached estimate if it exists
    if (getters[types.checkout.hasEstimate](reservationBlockId) && noCache === false) {
      return Promise.resolve(getters[types.checkout.getEstimate](reservationBlockId));
    }

    const reservationBlock = getters.reservationBlocks[reservationBlockId];
    const payload = reservationBlock.exportEstimateAPIParams(override);

    // Check to see if we have the exact request already in flight. See below where an empty object is set.
    if (typeof state.estimates[makeEstimateCacheKey(payload)] !== 'undefined') {
      return Promise.resolve(getters[types.checkout.getEstimate](reservationBlockId));
    }

    // Otherwise, fetch a new estimate from the server
    const data = await api.chargeEstimates.fetch(payload);
    commit(types.checkout.fetchEstimates, { payload, data });

    return data;
  },

  async [types.checkout.fetchProjectedEstimates]({ commit, getters, state }, { reservationBlockId, override }) {
    let alreadyReturnedValue = false;

    const reservationBlock = getters.reservationBlocks[reservationBlockId];
    if (!reservationBlock) {
      return Promise.resolve({});
    }

    const payload = reservationBlock.exportEstimateAPIParams(override);

    // Return a cached estimate if it exists
    const cachedEstimate = state.estimates[makeEstimateCacheKey(payload)];
    if (Object.keys(state.estimates) === makeEstimateCacheKey(payload)) {
      alreadyReturnedValue = true;
      commit(types.checkout.fetchEstimates, { payload, cachedEstimate });

      return cachedEstimate;
    }

    // If we've gone this far, the request is going to be made. So first, set a dummy, empty object
    // to the estimate. This will prevent identical, simultaneous requests from going out while one is already in flight.
    commit(types.checkout.fetchEstimates, { payload, data: {} });

    // Now fetch the estimate
    if (typeof state.estimates[makeEstimateCacheKey(payload)] !== 'undefined' || !alreadyReturnedValue) {
      // Now fetch the estimate
      const data = await api.chargeEstimates.fetch(payload);
      commit(types.checkout.fetchEstimates, { payload, data });

      return data;
    }
  },

  [types.checkout.updatePromotionCode]({ commit }, { reservationBlockId, promotionCode }) {
    commit(types.checkout.updatePromotionCode, { reservationBlockId, promotionCode });
  },

  [types.checkout.billingAccountingCodes]({ commit }, { reservationBlockId, accountingCodes }) {
    commit(types.checkout.billingAccountingCodes, { reservationBlockId, accountingCodes });
  },

  [types.checkout.billingRequisitionCodes]({ commit }, { reservationBlockId, requisitionCodes }) {
    commit(types.checkout.billingRequisitionCodes, { reservationBlockId, requisitionCodes });
  },

  [types.checkout.billingCreditCard]({ commit }, { cardId, reservationBlockId }) {
    commit(types.checkout.billingCreditCard, { cardId, reservationBlockId });
  },

  [types.checkout.billingPoNumber]({ commit }, { reservationBlockId, poNumber }) {
    commit(types.checkout.billingPoNumber, { reservationBlockId, poNumber });
  },

  [types.checkout.billingRequester]({ commit }, { reservationBlockId, requesterId }) {
    commit(types.checkout.billingRequester, { reservationBlockId, requesterId });
  },

  [types.checkout.billingApprover]({ commit }, { reservationBlockId, approverId }) {
    commit(types.checkout.billingApprover, { reservationBlockId, approverId });
  },

  [types.checkout.billingRpp]({ commit }, { reservationBlockId, rppValue }) {
    commit(types.checkout.billingRpp, { reservationBlockId, rppValue });
  },

  [types.checkout.createTransaction]({
    commit, dispatch, getters,
  }, reservationBlockId) {
    return new Promise((resolve, reject) => {
      const params = getters.transactionParams(reservationBlockId);
      return api.checkout.createTransaction(params)
        .then((response) => {
          if (typeof response !== 'undefined') {
            if (response.error) {
              return reject(new Error(response.error));
            }
            else if (typeof response.data !== 'undefined') {
              const { data } = response;
              if (!data.requisitionId) {
                return reject(new Error(data));
              }
              commit(types.checkout.createTransaction, { reservationBlockId, data });

              // Copy the details of the reservation into a results block for display
              dispatch(types.checkout.copyReservationBlockToTransactionBlock, reservationBlockId);

              return resolve(data);
            }
          }
          return reject(new Error(null));
        });
    });
  },

  [types.checkout.billingDeliveryNote]({ commit }, note) {
    commit(types.checkout.billingDeliveryNote, note);
  },

  [types.checkout.billingTransType]({ commit }, { reservationBlockId, transType }) {
    commit(types.checkout.billingTransType, { reservationBlockId, transType });
  },

  [types.checkout.copyReservationBlockToTransactionBlock]({ commit, getters, state }, reservationBlockId) {
    const copy = cloneDeep(getters.reservationBlocks[reservationBlockId]);
    const estimate = getters[types.checkout.getEstimate](reservationBlockId);
    copy.transType = state.billing[reservationBlockId].transType;
    commit(types.checkout.copyReservationBlockToTransactionBlock, { reservationBlock: copy, estimate });
  },

  [types.checkout.startUnauthenticatedCheckout]({ commit }) {
    commit(types.checkout.startUnauthenticatedCheckout);
  },

  [types.checkout.hasShownAuthenticateToContinueModal]({ commit }, value) {
    commit(types.checkout.hasShownAuthenticateToContinueModal, value);
  },
};


const mutations = {
  [types.checkout.setSubmitting](state, isSubmitting) {
    state.isSubmitting = isSubmitting;
  },

  [types.checkout.setButtonClickType](state, isReservation) {
    state.isReservation = isReservation;
  },

  [types.checkout.setHideOverlayImmediately](state, hideOverlayImmediately) {
    state.hideOverlayImmediately = hideOverlayImmediately;
  },

  // Note that this needs to be commited by the parent billing component on mount!
  [types.checkout.billing.reset](state, { reservationBlockId }) {
    const original = get(state, ['billing', reservationBlockId], {});
    Vue.set(state.billing, reservationBlockId, { ...original });
  },

  [types.checkout.updatePromotionCode](state, { reservationBlockId, promotionCode }) {
    Vue.set(state.billing[reservationBlockId], 'promotionCode', promotionCode === null ? undefined : promotionCode);
  },

  [types.checkout.fetchEstimates](state, { payload, data }) {
    const key = makeEstimateCacheKey(payload);
    Vue.set(state.estimates, key, data);
  },

  [types.checkout.billingAccountingCodes](state, { reservationBlockId, accountingCodes }) {
    Vue.set(state.billing[reservationBlockId], 'accountingCodes', accountingCodes);
  },

  [types.checkout.billingRequisitionCodes](state, { reservationBlockId, requisitionCodes }) {
    Vue.set(state.billing[reservationBlockId], 'requisitionCodes', requisitionCodes);
  },

  [types.checkout.billingCreditCard](state, { cardId, reservationBlockId }) {
    Vue.set(state.billing[reservationBlockId], 'cardId', cardId);
  },

  [types.checkout.billingApprover](state, { reservationBlockId, approverId }) {
    Vue.set(state.billing[reservationBlockId], 'approverId', approverId);
  },

  [types.checkout.billingRequester](state, { reservationBlockId, requesterId }) {
    Vue.set(state.billing[reservationBlockId], 'requesterId', requesterId);
  },

  [types.checkout.billingPoNumber](state, { reservationBlockId, poNumber }) {
    Vue.set(state.billing[reservationBlockId], 'poNumber', poNumber);
  },

  [types.checkout.billingRpp](state, { reservationBlockId, rppValue }) {
    Vue.set(state.billing[reservationBlockId], 'rpp', rppValue);
  },

  [types.checkout.billingDeliveryNote](state, { reservationBlockId, deliveryNote }) {
    Vue.set(state.billing[reservationBlockId], 'deliveryNote', deliveryNote);
  },

  [types.checkout.billingTransType](state, { reservationBlockId, transType }) {
    Vue.set(state.billing[reservationBlockId], 'transType', transType);
  },

  [types.checkout.createTransaction](state, { reservationBlockId, data }) {
    Vue.set(state.transactionResults, reservationBlockId, data);
  },

  [types.checkout.copyReservationBlockToTransactionBlock](state, { reservationBlock, estimate }) {
    Vue.set(state.transactionBlocks, reservationBlock.id, reservationBlock);

    // This is a hack, but clone the estimate into an estimate that is keyed by the id, for easy access.
    Vue.set(state.estimates, reservationBlock.id, estimate);
  },

  [types.checkout.updateReservationBlockProgress](state, newProgress) {
    const newState = state.progressOfAllReservationBlocks.filter(({ id }) => id !== newProgress.id);
    newState.push(newProgress);
    state.progressOfAllReservationBlocks = newState;
  },

  'checkout/reset': function (state) {
    state.transactionResults = {};
    state.transactionBlocks = {};
    state.billing = {};
    state.estimates = {};
  },

  [types.checkout.startUnauthenticatedCheckout](state) {
    state.startUnauthenticatedCheckout = !state.startUnauthenticatedCheckout;
  },

  [types.checkout.hasShownAuthenticateToContinueModal](state, value) {
    state.hasShownAuthenticateToContinueModal = value;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
