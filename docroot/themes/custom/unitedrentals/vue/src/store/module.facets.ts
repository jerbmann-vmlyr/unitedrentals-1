import Vue from 'vue';

import api from '@/api';
import { isEmpty, findKey, forEach, forOwn } from 'lodash';
import types from '@/store/types';
import uid from '@/utils/uid';
import url from '@/utils/url';

const state = {

  /**
   * Page breadcrumbs (object listing page)
   */
  breadcrumbs: '',

  /**
   * Page description (object listing page)
   */
  description: '',

  /**
   * All contextual filters, keyed by filter id.
   * Eg,
   * all: {
   *   1234: {
   *     id: '1234',
   *     label: '20-40ft Boom Lifts',
   *     catClasses: ['310-4555', ...]
   *   },
   *   ...
   * }
   *
   * @type {object}
   */
  contextualFilters: {},

  /**
   * All equipment filters, keyed by filter id.
   * Eg,
   * all: {
   *   1234: {
   *     id: '1234',
   *     label: '20-40ft Boom Lifts',
   *     catClasses: ['310-4555', ...]
   *   },
   *   ...
   * }
   *
   * @type {object}
   */
  equipmentFilters: {},


  /**
   * Category/Subcategory Taxonomy structure.
   */
  categories: {},

  /**
   * The collection of active equipment/contextual filter id's
   * Eg, ['asdfkewosdkfw', ...]
   *
   * @type {string[]}
   */
  selectedFilters: [],
};

const getters = {

  /**
   * Category By CatClass getter
   *
   * Retrieves the category name given a CatClass
   * @param {object} state | state mapping
   * @param {object} getters | getters mapping
   * @return {string | boolean} the name of the category if found, false otherwise
   */
  categoryByCatClass: (state, getters) => catClass => {
    let name = false;

    forEach(state.categories, category => {
      const subCat = getters.subCategoryByCatClass(catClass, category.children);
      if (subCat) {
        ({ name } = category);
      }
    });

    return name;
  },
  categoryIdByCatClass: (state, getters) => catClass => {
    let id = false;

    forEach(state.categories, category => {
      const subCat = getters.subCategoryByCatClass(catClass, category.children);
      if (subCat) {
        ({ id } = category);
      }
    });

    return id;
  },
  categoryIdOfCurrentUrl(state) {
    const categoryComponentOfUrl = url.getCategory();
    const id = findKey(state.categories, ({ urlComponent }) => urlComponent.includes(categoryComponentOfUrl));
    return id || '';
  },
  subcategoryIdOfCurrentUrl(state, getters) {
    if (!getters.categoryIdOfCurrentUrl) {
      return '';
    }
    const subcategoryComponentOfUrl = url.getSubcategory();
    const id = findKey(state.categories[getters.categoryIdOfCurrentUrl].children, subcat =>
      subcat.urlComponent.substr(subcat.urlComponent.lastIndexOf('/') + 1).includes(subcategoryComponentOfUrl));
    return id || '';
  },

  /**
   * This Getter is only relevant for the Equipment Listing pages.
   * "Equipment Attributes" is jargon for groups of equipment-filters.
   * This component renders groups of filters for either
   *   a) the current subcategory (if it's currently a subcategory page)
   *   or b) ALL subcategories of the current category (when the user isn't on a subcat page)
   * @param {object} state from from vuex
   * @param {object} getters from vuex
   * @return {object|undefined} template data for group names & filters. Returns Undefined
   */
  equipmentAttributes(state, getters) {
    if (isEmpty(state.categories)) {
      return {};
    }

    const currentCategory = state.categories[getters.categoryIdOfCurrentUrl];

    // User is not on an Equipment Listing page. No work needs to be done, and this Getter is irrelevant.
    // The Cart page currently calls api.facets.fetch, which populates state.categories, and thus
    // requires this Guard.
    if (!currentCategory) {
      return;
    }

    const currentSubcategory = getters.subcategoryIdOfCurrentUrl
      ? currentCategory.children[getters.subcategoryIdOfCurrentUrl]
      : {};

    let equipmentAttributes = {};

    if (isEmpty(currentSubcategory)) {
      forOwn(currentCategory.children, ({ children }) => {
        equipmentAttributes = { ...equipmentAttributes, ...children };
      });
    }
    else {
      equipmentAttributes = currentCategory.children[getters.subcategoryIdOfCurrentUrl].children;
    }

    return equipmentAttributes;
  },

  /**
   * Sub-category By CatClass getter
   *
   * Returns the name of the sub-category given a category id and cat-class
   * @param {state} state | state mapping
   * @return {string | boolean} name of the found subcategory, false otherwise
   */
  subCategoryByCatClass: state => (catClass, categories = state.categories) => {
    /*
     Collapse the categories into a 1-level list
     Save the business logic for later
     */

    const flattenCategories = cats => [
      ...cats,
      ...cats
        .map(cat => flattenCategories(Object.values(cat.children)))
        .reduce((accum, c) => (Array.isArray(c) ? [...accum, ...c] : [...accum, c]), []),
    ];

    /*
     Flatten then filter out the categories that don't have a catClasses
     list or whose catClasses list doesn't contain the catClass parameter
     */
    const matchingCategories =
      flattenCategories(Object.values(categories))
        .filter(cat => Array.isArray(cat.catClasses) && cat.catClasses.includes(catClass));
    return matchingCategories.length > 0
      ? matchingCategories[0].name
      : false;
  },

  /**
   * Sub-category Object By CatClass getter
   *
   * Returns the name of the sub-category given a category id and cat-class
   * @param {state} state | state mapping
   * @return {object} object of the found subcategory, false otherwise
   */
  subCategoryObjByCatClass: state => (catClass, categories = state.categories) => {
    /*
     Collapse the categories into a 1-level list
     Save the business logic for later
     */
    const flattenCategories = cats => [
      ...cats,
      ...cats
        .map(cat => flattenCategories(Object.values(cat.children)))
        .reduce((accum, c) => (Array.isArray(c) ? [...accum, ...c] : [...accum, c]), []),
    ];

    /*
     Flatten then filter out the categories that don't have a catClasses
     list or whose catClasses list doesn't contain the catClass parameter
     */
    const matchingCategories =
      flattenCategories(Object.values(categories))
        .filter(cat => Array.isArray(cat.catClasses) && cat.catClasses.includes(catClass));
    return matchingCategories.length > 0
      ? matchingCategories[0]
      : false;
  },

};

const actions = {
  [types.facets.collapse]({ commit }) {
    commit(types.facets.collapse);
  },
  [types.facets.createContextualFilter]({ commit }, filter) {
    const unique = uid();
    filter.id = unique;
    commit(types.facets.createContextualFilter, filter);
    return unique;
  },
  [types.facets.createEquipmentFilter]({ commit }, filter) {
    commit(types.facets.createEquipmentFilter, filter);
  },
  [types.facets.fetch]({ commit }) {
    return api.facets.fetch()
      .then(({ data }) => {
        commit(types.facets.fetch, data.result);
        return data.result;
      });
  },
  [types.facets.fetchDescription]({ commit }, { categoryTid, subcategoryTid }) {
    return api.facets.fetchDescription(categoryTid, subcategoryTid)
      .then(({ data }) => {
        commit(types.facets.fetchDescription, data.result);
        commit(types.facets.fetchBreadcrumbs, data.breadcrumbs);
      });
  },
  [types.facets.removeFilter]({ commit }, id) {
    commit(types.facets.removeFilter, id);
  },
  [types.facets.removeAllFilters]({ commit }) {
    commit(types.facets.removeAllFilters);
  },
  [types.facets.toggle]({ commit }, idx) {
    commit(types.facets.toggle, idx);
  },
};

const mutations = {
  [types.facets.createContextualFilter](state, filter) {
    Vue.set(state.contextualFilters, filter.id, filter);
    state.selectedFilters.push(filter.id);
  },
  [types.facets.createEquipmentFilter](state, filter) {
    Vue.set(state.equipmentFilters, filter.id, filter);
    state.selectedFilters.push(filter.id);
  },
  [types.facets.fetch](state, categories) {
    state.categories = categories;
  },

  [types.facets.fetchDescription](state, description) {
    state.description = description;
  },
  [types.facets.fetchBreadcrumbs](state, breadcrumbs) {
      state.breadcrumbs = breadcrumbs;
  },

  /**
   * Removes a given filter from the selectedFilters array
   * @param {object} state | current state
   * @param {string} id | id of facet to remove
   */
  [types.facets.removeFilter](state, id) {
    state.selectedFilters = state.selectedFilters.filter(filterId => filterId !== id);
  },

  /**
   * Removes all selected filters
   * @param {object} state | current state
   */
  [types.facets.removeAllFilters](state) {
    state.selectedFilters = [];
  },
};

export default {
  actions,
  getters,
  mutations,
  state,
};
