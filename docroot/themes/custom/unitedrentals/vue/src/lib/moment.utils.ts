import * as moment from 'moment';
import { DateFormat } from '@/lib/constants';

export class MomentUtils {
  static formatAs(
    format: DateFormat,
    dateString: string | moment.Moment,
  ): string {
    return moment(dateString).format(format);
  }

  static daysFromNow(dateString: string | moment.Moment): number {
    return moment(dateString).diff(moment(), 'days', true) || 0;
  }

  static happensWithinThreeDays(dateString: string): boolean {
    const diff = MomentUtils.daysFromNow(dateString);
    return diff >= 0 && diff < 3;
  }

  static happensInNextSixMonths(dateString: string): boolean {
    return moment(dateString).isBetween(moment().startOf('month'), moment().add(5, 'months').endOf('month'));
  }

  static happensThisWeek(dateString: string): boolean {
    // https://stackoverflow.com/questions/47097216/momentjs-get-if-date-is-this-week-this-month-or-this-year
    return moment(dateString).isSame(new Date(), 'week');
  }

  static happensThisMonth(dateString: string): boolean {
    return moment(dateString).isSame(new Date(), 'month');
  }

  static happensInThePast(
    dateString: string,
    precision: moment.unitOfTime.StartOf = 'hours',
  ): boolean {
    return parseInt(dateString, 10) ? moment(dateString).isBefore(moment(), precision) : false;
  }

  static happensTodayOrInThePast(dateString: string): boolean {
    return parseInt(dateString, 10) ? moment(dateString).isSameOrBefore(moment(), 'day') : false;
  }

  static happensInTheFuture(dateString: string): boolean {
    return parseInt(dateString, 10) ? moment(dateString).isAfter(moment(), 'day') : false;
  }

  static happensTodayOrInTheFuture(dateString: string): boolean {
    return parseInt(dateString, 10) ? moment(dateString).isSameOrAfter(moment(), 'day') : false;
  }

  static happensInOverAWeek(dateString: string): boolean {
    return MomentUtils.daysFromNow(dateString) > 7;
  }

  static happensAfterToday(dateString: string): boolean {
    return MomentUtils.daysFromNow(dateString) >= 1;
  }

  static happeningNow(fromDateString: string, toDateString): boolean {
    return MomentUtils.happensTodayOrInThePast(fromDateString)
      && MomentUtils.happensTodayOrInTheFuture(toDateString);
  }
}
