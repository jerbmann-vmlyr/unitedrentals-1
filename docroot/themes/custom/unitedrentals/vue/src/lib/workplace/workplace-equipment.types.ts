import { Status } from '@/lib/statuses';
/**
 * TODO: The following type definitions are derived from raw API
 * response data and as such, are almost certainly incomplete or
 * innaccurate. When API documentation for the frontend does become
 * available, these should be updated accordingly.
 *
 * In the interim, to prevent unnecessary usage friction,
 * the types should be provided with an index signature.
 * (i.e. [prop: string]: any )
 */
import * as common from '../common-types';
import { TransactionType, ContractStatusId, QuoteStatusId, ReservationStatusId } from '@/lib/transactions';
import { WorkplaceEquipmentStatus } from './equipment-statuses';
import { Branch } from '../branches';
import { BusinessType } from '@/lib/business-types';

export interface WorkplaceEquipment {
  active: null;
  approverGuid: string|'';
  approverName: string|'';
  branchId: string|'';
  catClass: string;
  comment: null;
  cumulativeOperatingHours?: {
    hours: number;
  };
  currency: common.CurrencyCode;
  customerJobId: string|'';
  customerName: string;
  custOwn: boolean;
  dalDescription: string;
  dayRate: number; // must be converted from string!!
  description: '';
  equipmentId: string;
  gps: null;
  gpsUtilization: WorkplaceEquipmentGpsUtilization;
  id: null;
  jobsite: common.Jobsite;
  lastBilledDate: string;
  latitude: string;
  leniencyStartDate: string;
  leniencyEndDate: string;
  longitude: string;
  make: string|'';
  minRate: number; // must be converted from string!!
  model: string|'';
  monthRate: number; // must be converted from string!!
  nextPickupDateTime: string|null;
  orderedBy: string;
  pendingPickup: boolean;
  phone: string;
  pickup: boolean;
  pickupDateTime: null|string;
  pickupId: string;
  pickupType: WorkplaceEquipmentPickupType|'';
  po: string;
  quantity: number;
  quantityNextPickup: number;
  quantityOnRent: number;
  quantityPendingPickup: number;
  quantityPickedUp: number;
  quantityReturned: number;
  rate: WorkplaceEquipmentRate|null;
  remainingQty: null;
  rentalAmount: string;
  requesterGuid: string|'';
  requesterName: string|'';
  requisitionId: string;
  returnDateTime: string;
  returnSeq: null;
  rpp: boolean;
  scheduledPickupQty: null;
  serial: string|'';
  simActive: boolean;
  startDateTime: string;
  status: string;
  subcategory: string|''; // The Drupal Subcategory taxonomy for the equipment item.
  title: string;
  transId: string;
  transLineId: string;
  transType: TransactionType;
  type: number; // must be converted from string!
  urDeliver: boolean;
  urEquipmentId: '';
  usageToday: number,
  weekRate: number; // must be converted from string!!!
  year: number; // must be converted from string!!!

  // The following properties must be generated within the API
  // transform pipeline. A raw WorkplaceEquipment object that is returned
  // by an equipment API will not have them.
  accountId: string;
  daysOnRent: number | null;
  equipmentStatuses: WorkplaceEquipmentStatus[];

  // The following properties must be added using data from non-equipment APIs
  branch: Branch;
  catClassIsFavorite: boolean; // default to false, then populated using favorites data
  eqpType: string|null; // default to null, then populated using facets data
  businessTypes: BusinessType;
}

export interface WorkplaceEquipmentRate {
  branchId: string|null;
  catClass: string;
  dayRate: number; // must be converted from string!!!
  weekRate: number;
  monthRate: number;
  minRate: number;
  minRateDuration: string|number;
  rateType: null;
  expirationDate: null;
  currency: null;
  rpp: null;
  dspRateCatalog: string;
  deliveryCharge: number|null;
  rateTiers: any[];
  rateZone: null;
  contractPricing: boolean;
}

export interface WorkplaceEquipmentCoordinates {
  latitude: number | null;
  longitude: number | null;
  valid: boolean;
}

export interface WorkplaceEquipmentGps extends WorkplaceEquipmentCoordinates {
  altitude: number | null; // must be extract from an object!
  datetime: string | null;
  usageByDay?: WorkplaceEquipmentUsageByDay; // might need to be converted from empty array!
}

export enum WorkplaceEquipmentGpsUtilization {
  /**
   * Equipment does not have GPS reporting capabilities
   */
  NA = 'N/A',
  /**
   * Equipment hasn’t pinged in the last 24 hours, but pinged within last 5 days
   */
  NoSignal = 'no-signal',
  /**
   * Equipment hasn’t pinged in 5 or more days
   */
  Offline = 'offline',
  /**
   * Equipment pinged within the last 24 hours and hasn’t been used in more than
   * 3 days (lastMeterChange >= 3)
   */
  Idle = 'idle',
  /**
   * Equipment pinged within the last 24 hours, used within the last 3 days
   * (lastMeterChange < 3), and hasn’t been used today (usageToday === 0)
   */
  Online = 'online',
  /**
   * Equipment pinged within the last 24 hours, used within the last 3 days
   * (lastMeterChange < 3), and has been used today between 0-8 hours
   */
  Active = 'active',
  /**
   * Equipment pinged within the last 24 hours, used within the last 3 days
   * (lastMeterChange < 3), and it has been used over 8 hours
   */
  Overtime = 'overtime',
}

export interface WorkplaceEquipmentUsageByDay {
  [dateTimeKey: string]: WorkplaceEquipmentUsage;
}

export interface WorkplaceEquipmentUsage {
  days: number;
  hours: number;
  minutes: number;
}

export enum WorkplaceEquipmentPickupType {
  Advanced = 'Advanced',
  Scheduled = 'Scheduled',
}

export enum WorkplaceEquipmentOrderType {
  Contract = 'contract',
  Reservation = 'reservation',
  Quote = 'quote',
}

/**
 * The annotations for each type are the annotations from the endpoint documentation provided by DAL.
 * See https://testmobiledal.ur.com/dashboard/docs#  - Rental/Counts for more information on the
 * acceptable parameters for the request, and the expected return object provided by DAL.
 */
export interface WorkplaceRentalCounts {
  quotes: number;	// Quotes
  reservations: number;	// Reservations
  onRent: number;	// Equipment on Rent RHTYPE='O' with pickup date greater than current date
  scheduledForPickup: number;	// Scheduled for Pickup where	RHTYPE='O' with pendingStatus
  overdue: number;	// Overdue equipments where	RHTYPE='O' with pending status and overdue date
  allOpen: number;	// All Open Contracts where	RHTYPE is 'X','R' or 'O' (includes all contracts that are Quotes, Reservations and Open)
  closedContracts: number; // Closed contracts from RACHDRFL & RACDETFL
  custOwn: boolean;	// Whether customer owned is included in counts	1
}

export type WorkplaceEquipmentContractStatus = Status<ContractStatusId>;
export type WorkplaceEquipmentQuoteStatus = Status<QuoteStatusId>;
export type WorkplaceEquipmentReservationStatus = Status<ReservationStatusId>;
export interface WorkplaceEquipmentOrderStatuses {
  contractStatuses: WorkplaceEquipmentContractStatus[];
  quoteStatuses: WorkplaceEquipmentQuoteStatus[];
  reservationStatuses: WorkplaceEquipmentReservationStatus[];
}

export enum WorkplaceEquipmentActionTypes {
  ExtendContract = 'extendContract',
  Favorite = 'favorite',
  PrintDetails = 'printDetails',
  RequestPickup = 'requestPickup',
  RentAnother = 'rentAnother',
  UpdatePo = 'updatePo',
  ViewDetails = 'viewDetails',
  EditQuote = 'editQuote',
  ConvertQuote = 'convertQuote',
}
