import { SortDirection } from '@/lib/collections';
import { WorkplaceEquipmentGrouperId, WorkplaceEquipmentSorterId } from '../equipment-collection';
import { WorkplaceOrderGrouperId, WorkplaceOrderSorterId } from '../order-collection';


export enum WorkplaceRouteNames {
  Unauth = 'workplace:unauth',
  Default = 'workplace:default',
  Dashboard = 'workplace:dashboard',
  Equipment = 'workplace:equipment:default',
  EquipmentDetails = 'workplace:equipment:details',
  EquipmentList = 'workplace:equipment:list',
  OrderDetails = 'workplace:order:details',
  OrderList = 'workplace:order:list',
  CustomizeOrderVisibility = 'workplace:order:customize',
  Messaging = 'worksplace:messaging',
  NoAccount = 'workplace:no-account',
  NoEquipment = 'workplace:no-equipment',
  Root = 'workplace:default',
  UpdateEquipmentPo = 'workplace:equipment:update-po',
  RequestEquipmentReschedule = 'workplace:equipment:request-reschedule',
  ConfirmEquipmentReschedule = 'workplace:equipment:confirm-reschedule',
  RequestPickup = 'workplace:pickup:request-reschedule',
  ConfirmPickup = 'workplace:pickup:confirm-reschedule',
  CustomizeEquipmentVisibility = 'workplace:equipment:customize',
}

export interface WorkplaceQueryParams {
  groupBy: WorkplaceEquipmentGrouperId|WorkplaceOrderGrouperId;
  sortBy: WorkplaceEquipmentSorterId|WorkplaceOrderSorterId;
  sortDirection: SortDirection;
  [param: string]: any;
}
