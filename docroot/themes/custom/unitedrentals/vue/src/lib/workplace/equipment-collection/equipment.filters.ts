import { uniqBy } from 'lodash';
import moment from 'moment';
import { FilterType, Filter, ParsedFilter } from '@/lib/collections';
import { StatusUtils } from '@/lib/statuses';
import {
  WorkplaceEquipment,
  WorkplaceEquipmentOrderType,
  WorkplaceEquipmentGpsUtilization as Utilization,
} from '../workplace-equipment.types';
import { WorkplaceEquipmentStatusId, WorkplaceEquipmentStatusUtils } from '../equipment-statuses';

export enum WorkplaceEquipmentFilterId {
  /* Simple  */ Favorites = 'favorites',
  /* Static  */ EquipmentStatus = 'equipmentStatus',
  /* Static  */ OrderType = 'orderType',
  /* Static  */ GPS = 'gps',
  /* Dynamic */ EquipmentType = 'equipmentType',
  /* Dynamic */ Jobsite = 'jobsite',
  /* Dynamic */ Requester = 'requester',
  /* Dynamic */ Po = 'poNumber',
  /* Dynamic */ TransId = 'transId',
  /* Search  */ Search = 'search',
}

export type WorkplaceEquipmentFilter = Filter<WorkplaceEquipment, WorkplaceEquipmentFilterId>;
export type ParsedWorkplaceEquipmentFilter = ParsedFilter<WorkplaceEquipment, WorkplaceEquipmentFilterId>;

export const WORKPLACE_EQUIPMENT_FILTERS: WorkplaceEquipmentFilter[] = [
  {
    type: FilterType.Dynamic,
    id: WorkplaceEquipmentFilterId.Jobsite,
    label: 'Jobsite',
    filterFactory: queryValue => eqp => eqp.jobsite.id === queryValue,
    valueFactory: eqps => uniqBy(
      eqps.map(({ jobsite, customerJobId }) => ({
        queryValue: jobsite.id,
        label: [
          jobsite.name,
          customerJobId.length > 0
            ? customerJobId
            : `${jobsite.city}, ${jobsite.state}`,
        ],
        filter: eqp => eqp.jobsite.id === jobsite.id,
      })),
      value => value.queryValue,
    ),
  },
  {
    type: FilterType.Static,
    id: WorkplaceEquipmentFilterId.OrderType,
    label: 'Transaction Type',
    filters: [
      { // Quotes
        queryValue: WorkplaceEquipmentOrderType.Quote,
        label: 'Quote',
        filter: WorkplaceEquipmentStatusUtils.equipmentIsQuote,
      },
      { // Reservations
        queryValue: WorkplaceEquipmentOrderType.Reservation,
        label: 'Reservation',
        filter: WorkplaceEquipmentStatusUtils.equipmentIsReservation,
      },
      { // Contracts
        queryValue: WorkplaceEquipmentOrderType.Contract,
        label: 'Contract',
        filter: WorkplaceEquipmentStatusUtils.equipmentIsContract,
      },
    ],
  },
  {
    type: FilterType.Dynamic,
    id: WorkplaceEquipmentFilterId.TransId,
    label: 'Transaction #',
    filterFactory: queryValue => eqp => eqp.transId === queryValue,
    valueFactory: eqps => uniqBy(
      eqps.map(({ transId }) => ({
        queryValue: transId,
        label: transId,
        filter: eqp => eqp.transId === transId,
      })),
      value => value.queryValue,
    ),
  },
  {
    type: FilterType.Dynamic,
    id: WorkplaceEquipmentFilterId.Requester,
    label: 'Requester',
    filterFactory: queryValue => eqp => eqp.requesterName === queryValue,
    valueFactory: eqps => uniqBy(
      eqps
        .filter(({ requesterName }) => !!requesterName && requesterName.length > 0)
        .map(({ requesterName }) => ({
          queryValue: requesterName,
          label: requesterName,
          filter: eqp => eqp.requesterName === requesterName,
        })),
      value => value.queryValue,
    ),
  },
  {
    type: FilterType.Dynamic,
    id: WorkplaceEquipmentFilterId.EquipmentType,
    label: 'Equipment Type',
    filterFactory: queryValue => eqp => eqp.eqpType === queryValue,
    valueFactory: eqps => uniqBy(
      eqps
        .filter(({ eqpType }) => typeof eqpType === 'string' && eqpType.length > 0)
        .map(({ eqpType }) => ({
          queryValue: eqpType!,
          label: eqpType!,
          filter: eqp => eqp.eqpType === eqpType,
        })),
      value => value.queryValue,
    ),
  },
  {
    type: FilterType.Static,
    id: WorkplaceEquipmentFilterId.EquipmentStatus,
    label: 'Equipment Status',
    filters: [
      { /* Quoted */
        queryValue: WorkplaceEquipmentStatusId.Quote,
        label: 'Quoted',
        filter: ({ equipmentStatuses }) => StatusUtils.statusListIncludesAnyOf(
          WorkplaceEquipmentStatusId.Quote,
          equipmentStatuses,
        ),
      },
      { /* Reserved for Branch Pickup */
        queryValue: WorkplaceEquipmentStatusId.ReservedForBranchPickup,
        label: 'Reserved for Branch Pickup',
        filter: ({ equipmentStatuses }) => StatusUtils.statusListIncludesAnyOf(
          WorkplaceEquipmentStatusId.ReservedForBranchPickup,
          equipmentStatuses,
        ),
      },
      { /* Reserved for Branch Delivery */
        queryValue: WorkplaceEquipmentStatusId.ReservedForDelivery,
        label: 'Reserved for Delivery',
        filter: ({ equipmentStatuses }) => StatusUtils.statusListIncludesAnyOf(
          WorkplaceEquipmentStatusId.ReservedForDelivery,
          equipmentStatuses,
        ),
      },
      { /* On Rent */
        queryValue: WorkplaceEquipmentStatusId.OnRent,
        label: 'On Rent',
        filter: ({ equipmentStatuses }) => StatusUtils.statusListIncludesAnyOf(
          WorkplaceEquipmentStatusId.OnRent,
          equipmentStatuses,
        ),
      },
      { /* Pending Due */
        queryValue: WorkplaceEquipmentStatusId.DueSoon,
        label: 'Pending Due',
        filter: ({ equipmentStatuses }) => StatusUtils.statusListIncludesAnyOf(
          WorkplaceEquipmentStatusId.DueSoon,
          equipmentStatuses,
        ),
      },
      { /* Overdue */
        queryValue: WorkplaceEquipmentStatusId.Overdue,
        label: 'Overdue',
        filter: ({ equipmentStatuses }) => StatusUtils.statusListIncludesAnyOf(
          WorkplaceEquipmentStatusId.Overdue,
          equipmentStatuses,
        ),
      },
      { /* In Leniency */
        queryValue: WorkplaceEquipmentStatusId.InLeniency,
        label: 'In Leniency',
        filter: ({ equipmentStatuses }) => StatusUtils.statusListIncludesAnyOf(
          WorkplaceEquipmentStatusId.InLeniency,
          equipmentStatuses,
        ),
      },
      { /* Pickup Requested */
        queryValue: WorkplaceEquipmentStatusId.PickupRequested,
        label: 'Pickup Requested',
        filter: ({ equipmentStatuses }) => StatusUtils.statusListIncludesAnyOf(
          WorkplaceEquipmentStatusId.PickupRequested,
          equipmentStatuses,
        ),
      },
      { /* Advanced Pickup */
        queryValue: WorkplaceEquipmentStatusId.AdvancedPickup,
        label: 'Advanced Pickup',
        filter: ({ equipmentStatuses }) => StatusUtils.statusListIncludesAnyOf(
          WorkplaceEquipmentStatusId.AdvancedPickup,
          equipmentStatuses,
        ),
      },
    ],
  },
  {
    type: FilterType.Static,
    id: WorkplaceEquipmentFilterId.GPS,
    label: 'GPS',
    filters: [
      {
        queryValue: Utilization.Idle,
        label: 'Idle',
        filter: ({ gpsUtilization }) => gpsUtilization === Utilization.Idle,
      },
      {
        queryValue: Utilization.Online,
        label: 'Online',
        filter: ({ gpsUtilization }) => gpsUtilization === Utilization.Online,
      },
      {
        queryValue: Utilization.Active,
        label: 'Active',
        filter: ({ gpsUtilization }) => gpsUtilization === Utilization.Active,
      },
      {
        queryValue: Utilization.Overtime,
        label: 'Overtime',
        filter: ({ gpsUtilization }) => gpsUtilization === Utilization.Overtime,
      },
      {
        queryValue: Utilization.NoSignal,
        label: 'No Signal (<5 days)',
        filter: ({ gpsUtilization }) => gpsUtilization === Utilization.NoSignal,
      },
      {
        queryValue: Utilization.Offline,
        label: 'No Signal (5+ days)',
        filter: ({ gpsUtilization }) => gpsUtilization === Utilization.Offline,
      },
      {
        queryValue: Utilization.NA,
        label: 'N/A',
        filter: ({ gpsUtilization }) => gpsUtilization === Utilization.NA,
      },
    ],
  },
  {
    type: FilterType.Dynamic,
    id: WorkplaceEquipmentFilterId.Po,
    label: 'PO',
    filterFactory: queryValue => eqp => eqp.po === queryValue,
    valueFactory: eqps => uniqBy(
      eqps
        .filter(({ po }) => !!po && po.length > 0)
        .map(({ po }) => ({
          queryValue: po,
          label: po,
          filter: eqp => eqp.po === po,
        })),
      value => value.queryValue,
    ),
  },
  {
    type: FilterType.Simple,
    id: WorkplaceEquipmentFilterId.Favorites,
    queryValue: true,
    label: 'My Favorites',
    filter: ({ catClassIsFavorite }) => catClassIsFavorite,
  },
  {
    type: FilterType.Search,
    id: WorkplaceEquipmentFilterId.Search,
    label: 'Search Term',
    tokenizer: ({ jobsite, ...eq }) => [
        eq.approverName,
        eq.catClass,
        eq.customerJobId || null,
        eq.dalDescription,
        eq.equipmentId,
        eq.po,
        eq.requesterName,
        eq.title,
        eq.transId,
        moment(eq.startDateTime).format('MM/DD/YY'),
        moment(eq.returnDateTime).format('MM/DD/YY'),
        '$' + eq.weekRate || eq.weekRate,
        '$' + eq.monthRate || eq.monthRate,
        '$' + eq.dayRate || eq.dayRate,
        jobsite.city,
        jobsite.id,
        jobsite.name,
        jobsite.state,
        jobsite.address1,
        jobsite.zip,
      ]
      .filter((term): term is string => term !== null)
      .map(term => `${term}`.toLowerCase()),
  },
];
