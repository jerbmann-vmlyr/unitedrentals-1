import { Colors } from '@/lib/constants';
import { WorkplaceEquipmentStatusId } from '../equipment-statuses';

export class WorkplaceEquipmentMapsUtils {

  static getMapMarkerScale(
    ratio = 0,
  ): number {
    if (ratio < 0.25) return 10;
    else if (ratio >= 0.25 && ratio < 0.5) return 15;
    else if (ratio >= 0.5 && ratio < 0.75) return 20;
    else return 25;
  }

  static getMapMarkerColorByStatus(
    statusId: WorkplaceEquipmentStatusId,
  ): Colors {
    switch (statusId) {
      case WorkplaceEquipmentStatusId.AdvancedPickup: return Colors.BlueLowlight;
      case WorkplaceEquipmentStatusId.PickupRequested: return Colors.Gray38;
      case WorkplaceEquipmentStatusId.ReservedForBranchPickup: return Colors.Gray38;
      default: return Colors.Red;
    }
  }
}
