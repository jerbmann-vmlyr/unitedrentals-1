import { WorkplaceEquipmentStatusId } from '../equipment-statuses';
import { Jobsite } from '@/lib/common-types';

export interface WorkplaceEquipmentLocation {
  place?: google.maps.places.PlaceResult;
  lat: number;
  lng: number;
  address: string;
}

export interface WorkplaceEquipmentJobsiteLocation extends WorkplaceEquipmentLocation {
  accountId: string;
  jobsite: Jobsite;
}

export interface WorkplaceEquipmentJobsiteLocationMap {
  [jobsiteId: string]: WorkplaceEquipmentJobsiteLocation;
}

export interface WorkplaceEquipmentJobsiteCluster extends WorkplaceEquipmentJobsiteLocation {
  statusPopulations: { [S in WorkplaceEquipmentStatusId]?: number; };
  population: number;
}

export interface WorkplaceEquipmentJobsiteMapMarkerOptions extends google.maps.MarkerOptions {
  options: {
    status: WorkplaceEquipmentStatusId;
    statusPopulation: number;
    jobsite: Jobsite;
  }
}

export interface WorkplaceEquipmentJobsiteClusterWithMarkers extends WorkplaceEquipmentJobsiteCluster {
  markers: WorkplaceEquipmentJobsiteMapMarkerOptions[];
}
