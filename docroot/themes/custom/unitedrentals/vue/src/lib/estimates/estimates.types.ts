export interface ChargeEstimate {
  branchId: string;
  startDate: string;
  returnDate: string;
  delivery: boolean;
  pickup: boolean;
  rpp: boolean;
  items: {
    [catClass: string]: EstimateItem;
  };
  totals: EstimateTotals;

  // Available if address, city, state, & postalCode do not exist
  jobsiteId?: string;
  accountId?: string;

  // Available if jobsiteId and accountId do not exist
  address?: string;
  city?: string;
  state?: string;
  postalCode?: string;
}

export interface EstimateItem {
  quantity: number;
  minRate: number;
  dayRate: number;
  weekRate: number;
  monthRate: number;
  total: number;
  individualTotal: number;
  rateType: string;
  rateTypeCode: string;
  hasContractPricing: boolean;
}

export interface EstimateTotals {
  totalRentalAmount: number;
  environmentFee: number;
  tax: number;
  damageWaiverFee: number;
  deliveryFee: number;
  pickupFee: number;
  authAmount: number;	// Total Authorization Amount
  miscellaneousFees: number;
}
