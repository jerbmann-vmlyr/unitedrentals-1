import api from '@/api';

export default class GooglePlaces {
  constructor() {
    this.autocompleteService = new google.maps.places.AutocompleteService();
    this.placesService = new google.maps.places.PlacesService(document.createElement('div'));
    this.geo = new google.maps.Geocoder();
    this.geolocateOptions = {
      timeout: 30000,
      enableHighAccuracy: true,
    };
    this.addressKeys = {
      street_number: 'number',
      route: 'street',
      locality: 'city',
      administrative_area_level_1: 'state',
      administrative_area_level_2: 'county',
      administrative_area_level_3: 'township',
      country: 'country',
      postal_code: 'zip',
      postal_code_suffix: 'zipSuffix',
    };
    this.branchApi = api.branches;
    this.autoCompleteOptions = {};
  }

  /**
   * Gets a location predictions based on an address string.
   *
   * @param {string} address - Address string to use for lookup
   * @return {promise<array>} - An array of autocomplete predictions
   */
  getPredictionsFromAddress(address) {
    return new Promise((resolve, reject) => {
      const options = this.autoCompleteOptions;
      options.input = GooglePlaces.sanitizeInput(address);
      this.autocompleteService.getPlacePredictions(options, (predictions, status) => {
        if (status !== 'OK' || !predictions) {
          reject(new Error(`Failed to get place for: ${address}`));
          return;
        }
        resolve(predictions);
      });
    });
  }

  /**
   * Takes a response object from google places api and returns an address.
   *
   * @param {string} place - A google place object
   * @return {promise<string>} - An address string for search inputs
   */
  getAddressFromPlace(place) {
    return new Promise((resolve) => {
      this.getAddressComponentsFromPlace(place)
        .then(components => resolve(`${components.city}${components.state
        || components.zip ? ', ' : ''} ${components.state
        || ''} ${components.zip || ''}`));
    });
  }

  /**
   * Gets a google place object from an address String
   *
   * @param {string} address - Address string to use for lookup
   * @return {promise<object>} - A google place object
   */
  getPlaceFromAddress(address) {
    return new Promise((resolve, reject) => {
      this.getPredictionsFromAddress(address)
        .then((predictions) => {
          // Added most basic fields from https://developers.google.com/places/web-service/details
          const fields = ['address_component', 'adr_address', 'formatted_address', 'geometry', 'icon', 'name',
            'place_id', 'plus_code', 'type', 'url', 'utc_offset', 'vicinity'];
          const placeId = predictions[0].place_id;
          this.placesService.getDetails({ placeId, fields }, (place, status) => {
            if (status !== 'OK') {
              reject(new Error(`Failed to get place for: ${address}`));
              return;
            }
            resolve(place);
          });
        })
        .catch((e) => {
          reject(e);
        });
    });
  }

  /**
   * Gets a google place from a google prediction
   *
   * @param {object} prediction - a google prediction object
   * @return {promise<object>} - a google place object
   */
  getPlaceFromPrediction(prediction) {
    return new Promise((resolve) => {
      const fields = ['address_component', 'adr_address', 'formatted_address', 'geometry', 'icon', 'name',
        'place_id', 'plus_code', 'type', 'url', 'utc_offset', 'vicinity'];
      const placeId = prediction.place_id;
      this.placesService.getDetails({ placeId, fields }, (place) => {
        resolve(place);
      });
    });
  }

  /**
   * Accepts a set of lat-long coordinates and returns an address string for use
   * in search boxes.
   *
   * @param {object} coords - { latitude, longitude }
   * @return {promise<object>} - A google place object
   */
  getPlaceFromLatLong(coords) {
    return new Promise((resolve) => {
      const latLng = new google.maps.LatLng(parseFloat(coords.latitude), parseFloat(coords.longitude));
      this.geo.geocode({ latLng }, (results, status) => {
        if (status !== google.maps.GeocoderStatus.OK) {
          resolve('');
        }

        try {
          // Return the first item in the list
          resolve(results[0]);
        }
        catch (error) {
          resolve('');
        }
      });
    });
  }

  /**
   * Gets list of nearby branches sorted by distance from the api for a given place
   *
   * @param {object} place - A google place object
   * @return {promise<object>} - A branch object
   */
  getNearbyBranchesFromPlace(place) {
    return new Promise((resolve) => {
      const { lat, lng } = place.geometry.location;
      this.branchApi.getNearby({ lat, lng })
        .then(({ data }) => {
          resolve(data.results.results);
        });
    });
  }

  /**
   * Makes the api call for getting a branch using the lat lng from a place object
   *
   * @param {object} place - A google place object
   * @return {promise<object>} - A branch object
   */
  getBranchFromPlace(place) {
    return new Promise((resolve) => {
      this.getNearbyBranchesFromPlace(place)
        .then((branches) => {
          if (branches.length > 0) {
            this.branchApi.getBranchData(branches[0].branchId)
              .then(({ data }) => resolve(data[0]));
          }
          else {
            resolve(`No branch found for ${place}`);
          }
        });
    });
  }

  /**
   * Gets a branch from the api based on an address String
   *
   * @param {string} address - Address string to use for lookup
   * @return {promise<object>} - A branch object
   */
  getBranchFromAddress(address) {
    return new Promise((resolve) => {
      this.getPlaceFromAddress(address)
        .then((place) => {
          resolve(this.getBranchFromPlace(place));
        });
    });
  }

  /**
   * Takes a place object and returns and object with usable address components.
   *
   * @param {array} place - A google place object
   * @return {promise<object>} - An object containing user-friendly address attributes
   */
  getAddressComponentsFromPlace(place) {
    return new Promise((resolve) => {
      const addressParts = {};

      Promise.all(place.address_components.map(part => new Promise((resolve) => {
        if (this.addressKeys[part.types[0]] !== undefined) {
          const ap = this.addressKeys[part.types[0]];
          addressParts[ap] = part.short_name;
        }
        resolve(true);
      })))
        .then(resolve(addressParts));
    });
  }

  /**
   * Strips extra characters from the zip code for use in google api
   *
   * @param {string} input - The address string to be checked
   * @return {string} - The altered address string
   */
  static sanitizeInput(input) {
    const hasUsZipPlusFour = /(\d{5}-\d{4})/.test(input);

    if (hasUsZipPlusFour) {
      input = input.replace(/(\d{5}-\d{4})/, match => match.substring(0, 5));
    }

    // strip out "undefined"
    input = input.replace(/undefined/g, '').trim();

    return input;
  }

  /**
   * Calculate the distance, in miles, between to LatLng Objects
   *
   * @param {Object} pointA - a latlng object
   * @param {Object} pointB - a latlng object
   * @return {Number} distance in miles between pointA and pointB
   */
  static distanceBetween(pointA, pointB) {
    const R = 6378137; // Earthâ€™s mean radius in meter
    const dLat = this.rad(pointB.lat() - pointA.lat());
    const dLong = this.rad(pointB.lng() - pointA.lng());
    const a = (Math.sin(dLat / 2) * Math.sin(dLat / 2))
      + (Math.cos(this.rad(pointA.lat())) * Math.cos(this.rad(pointB.lat()))
      * Math.sin(dLong / 2) * Math.sin(dLong / 2));
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;
    return d * 0.000621371; // returns the distance in miles
  }

  /**
   * Helper for distanceBetween
   * @param {Number} x - a number to convert to radians
   * @return {Number} - return the radians
   */
  static rad(x) {
    return (x * Math.PI) / 180;
  }
}
