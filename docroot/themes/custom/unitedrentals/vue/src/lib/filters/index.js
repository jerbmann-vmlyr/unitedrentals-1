import { isNil } from 'lodash';
import currency from './currency';
import decimalCurrency from './decimal-currency';
import currencyDefaultZero from './currency-default-zero';
import ellipses from './ellipses';
import * as momentFormats from './moment-formats';
import { ccMonthYear, ccTypeName, ccTypeImg } from './credit-card-helpers.js';
import phoneNumber, { telLink } from './phone-number.js';
import uppercase from './uppercase';
import { datesToLeniencyText } from './leniency-text';
import numberCommaFormat from './format-number.js';

/* Helper to avoid making computeds for loading text. Instead of computed of:
 *    _.get(this.item, 'target', 'Loading target info...')
 * you can do it inline in template:
 *    <p>{{ item.target | ifNil('Loading target info...') }}
 */
export const ifNil = (v, defaultV) => isNil(v) ? defaultV : v;

export default {
  datesToLeniencyText,
  currency,
  currencyDefaultZero,
  decimalCurrency,
  ellipses,
  ...momentFormats,
  numberCommaFormat,
  ccMonthYear,
  ccTypeName,
  ccTypeImg,
  phoneNumber,
  telLink,
  uppercase,
  ifNil,
};
