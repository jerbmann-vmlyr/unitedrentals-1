/**
 * Vue Moment/Date Filters.
 *
 * TODO fix moment's deprecated usage warning
 *
 * Architecture (Requirements):
 * 1. Filters should process either a Moment Object or a Date String.
 * 2. Date Format Constants should be authored in `@/utils/dateFormat.js`, exposing them for use
 *    outside a Template.
 */

import moment from 'moment';
import { dateFormat } from '@/utils';
import DateTime from '@/utils/dateTime';

function sanitized(givenMoment) {
  if (typeof givenMoment === 'undefined' || givenMoment === null) {
    return moment();
  }
  if (typeof givenMoment.format === 'undefined') {
    return moment(givenMoment);
  }
  return givenMoment;
}
const momentSanitized = sanitized;

const schedules = DateTime.unitsAmPm(48, 30);

const meridian = {
  12: 'Morning',
  15: 'Afternoon',
  19: 'Evening',
  24: 'Night',
};

/**
 * NON-Moment time manipulations
 * @param {string} t formatted HH:mm
 * @return {string} time formatted h:mm A
 */
const amPmFormat = t => moment(t, 'HH:mm').format('h:mm A');

/**
 * Convert a moment (or date string) into a MM/DD/YYYY "Slash Format".
 * @param {moment|date} m - either a moment object or `new Date()`
 * @return {string} date formatted to MM/DD/YYYY
 */
const momentSlashFormat = m => sanitized(m).format(dateFormat.slash);
const momentShortSlashFormat = m => sanitized(m).format(dateFormat.shortSlash);
const momentSlashYYFormat = m => sanitized(m).format(dateFormat.rentalTimeline);
const momentShortTime = m => sanitized(m).format(dateFormat.shortTime);
const momentCalendarMonthFormat = m => sanitized(m).format('MMM');
const momentCalendarDayFormat = m => sanitized(m).format('D');
const momentFriendlyFormat = m => sanitized(m).format(dateFormat.friendly);
const momentFriendlyHourFormat = m => (m === '00:00' ? '' : schedules[sanitized(m).format('HHmm')]);
const momentFriendlyRoundedDownHourFormat = m => DateTime.round(m, moment.duration(30, 'minutes'), 'floor').format('h:mma');
const momentFriendlyMeridianFormat = (m) => {
  const tempHour = sanitized(m).hour();
  const possibles = Object.keys(meridian).filter(ea => ea >= tempHour);
  return meridian[possibles[possibles.length - 1]];
};
const momentMonthDayYearWithNameFormat = m => sanitized(m).format('MMM DD[,] YYYY');
const momentMonthDayWithNameFormat = m => sanitized(m).format('MMM DD');

export {
  amPmFormat,
  momentSanitized,
  momentCalendarDayFormat,
  momentCalendarMonthFormat,
  momentFriendlyFormat,
  momentFriendlyHourFormat,
  momentFriendlyRoundedDownHourFormat,
  momentShortTime,
  momentShortSlashFormat,
  momentSlashFormat,
  momentSlashYYFormat,
  momentMonthDayYearWithNameFormat,
  momentFriendlyMeridianFormat,
  momentMonthDayWithNameFormat,
};
