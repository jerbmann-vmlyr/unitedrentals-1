# Front End Architecture

## Components & The State Tree

The front end is built upon **custom components** *(as implemented by Vue.js)*. At their best, our components are
light, specialized, and generally fulfill a single responsibility.

The state of these components, on the highest, most important level (**Application State**) is determined by the *Application State
Tree* (**Vuex Store**).
  > Our Vuex Store is a dictionary of information, and is available to any Component
via Vuex's **mapState**/**mapGetters/mapActions** utilities.

Take, for example, a user's location. A user can set this location through any number of `<input>` fields found
across the site&mdash;all of which push their value to a *ratesPlace* field in our Vuex Store. As one `<input>`
gets used, all others will reflect the same value.

But our State Tree holds so much more than just a snapshot of the world. It "grows" with each ajax request.
Entire listings of equipment, store branches, and more get appended to the State Tree.
Thus over time, as the user explores more and more of the site, our State Tree **(Vuex Store)** will continue to grow.

> Currently we do not limit the size of our modules though this is certainly worth investigating.

The State Tree is the single source of truth for our Components. And because we use it to model
not only highly dynamic state, like the filters a user currently has activated, but also to record resources from pulled in
via ajax requests (such as an equipment navigation panel), our app becomes snappier when users revisit
something.

We then go one step further, and persist certain branches of our Vuex Store in the browser's Session Storage,
providing an "optimistic snappiness" across page loads. (In other words, our front end renders data from session
storage, but will override this rendering with the most current data from the backend, as it gets it.)

## Open Vue Component Documentation In Browser
```
  cd ~/docroot/unitedrentals/themes/custom/unitedrentals/vue && npm run docs:vue
```
