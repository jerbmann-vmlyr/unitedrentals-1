import parseToUrl from '@/utils/parseToUrl';

describe('parseToUrl', () => {
  it('should parse an object into a url formatted string', () => {
    // Invoke the unit being tested as necessary
    const object = {
      first: 'value',
      second: 'value',
    };

    const parsed = parseToUrl(object);

    // Check the results; "expect" and toEqual are Jasmine methods.
    expect(parsed).toEqual('?first=value&second=value');
  });
});
