import 'es6-promise/auto';
import '@babel/polyfill';
import 'reflect-metadata';
import 'jest-localstorage-mock';

Object.defineProperties(global, {
  Drupal: {
    value: {
      t: str => str,
      formatPlural: str => str,
    },
  },
  drupalSettings: {
    value: {},
  },
});

Object.defineProperty((global as any).document, 'documentElement', {
  value: {
    scrollIntoView: () => true,
  },
});
