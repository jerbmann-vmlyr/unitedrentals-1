# Testing

To run unit tests once, run `npm run unit`

To run unit tests continuously, run `npm run unit:continuous`

To run unit tests continuously and run the dev command at the same time, run `npm run tdd`

To learn more about how to write tests, start [here](https://vue-test-utils.vuejs.org/guides/#common-tips).

## Application Idioms

Each .vue file should have it's own .spec.js file barrelled right beside it (in the same directory).
Other than the suffix, the file names should be identical.

Each test should have a single expectation.

Focus  on testing the surface areas of the component: its input and output over interaction.  

## Troubleshooting

## Test Accounts

Using ur.vml.7, the following accounts should be set up like so:

* rppDefault = "N": Technomedia Solutions, Account# 1029707
* rppDefault = "Y":

* has Credit Limit: Technomedia Solutions, Account# 1029707
* no Credit Limit (Cash/Credit Card Account):

* requirePO = "Y": Technomedia Solutions, Account# 1029707
* requirePO = "N":

