<?php

/**
 * @file
 * Theme crossover theme file.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Drupal\Core\Template\Attribute;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Render\Element;

/**
 * Implements hook_preprocess_hook() for paragraph.
 */
function unitedrentals_preprocess_paragraph(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $bundle = $paragraph->bundle();
  $view_mode = $variables['view_mode'];
  $base_class = $variables['component_base_class'];

  // Initialize settings (this approach allows less IF/ELSE nesting).
  $setting_background_image_unwrap = FALSE;
  $setting_class_custom = FALSE;
  $setting_component_unwrap = FALSE;
  $setting_title_move = FALSE;

  // Toggle settings based on view-mode.
  switch ($variables['view_mode']) {
    case 'full':
      $setting_background_image_unwrap = TRUE;
      $setting_class_custom = TRUE;
      $setting_component_unwrap = TRUE;
      $setting_title_move = TRUE;
      // Attach theme-crossover styles.
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__node__full';
      break;
  }

  // Unset background-image field theme wrapper (to not print an empty div).
  if ($setting_background_image_unwrap && array_key_exists('field_background_image', $variables['content'])) {
    unset($variables['content']['field_background_image']);
  }

  // Add custom classes to the component.
  if ($setting_class_custom && $paragraph->hasField('field_class_custom') && !$paragraph->get('field_class_custom')->isEmpty()) {
    $field_class_custom = preg_replace('/[^a-zA-Z0-9\-_ ]/', '', $paragraph->get('field_class_custom')->value);
    $variables['attributes']['class'] = array_merge($variables['attributes']['class'], explode(' ', $field_class_custom));
  }

  // Remove component field-wrappers.
  if ($setting_component_unwrap && array_key_exists('field_paragraphs', $variables['content'])) {
    unset($variables['content']['field_paragraphs']['#theme']);
  }

  // Set title variable from fields.
  if ($setting_title_move) {
    foreach (['field_title_link', 'field_title'] as $title_fieldname) {
      if (array_key_exists($title_fieldname, $variables['content'])) {

        $variables['title'] = $variables['content'][$title_fieldname];

        unset($variables['title']['#theme']);
        unset($variables['content'][$title_fieldname]);
        break;
      }
    }
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for accordion group,
 * full.
 */
function unitedrentals_preprocess_paragraph__accordion_group__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];

  // Hide tab heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';

  // Attach theme-crossover styles.
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__accordion';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for accordion_item,
 * full.
 */
function unitedrentals_preprocess_paragraph__accordion_item__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  if (isset($variables['component_base_class'])) {
    $base_class = $variables['component_base_class'];
  }

  // Check if this accordion item should be open by default.
  $is_open = (!$paragraph->get('field_enabled')->isEmpty() && $paragraph->get('field_enabled')->value === '1');
  if ($is_open) {
    $variables['attributes']['class'][] = "{$base_class}--open";
  }

  // Attach theme-crossover styles.
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__accordion';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE() for article carousel group,
 * full.
 */
function unitedrentals_preprocess_paragraph__article_carousel(array &$variables) {
  $view_modes = [
    'full',
    'project_uptime_landing_page',
  ];

  if (in_array($variables['view_mode'], $view_modes)) {
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $variables['paragraph'];
  
    // Move title to it's own variable.
    if (array_key_exists('field_title', $variables['content']) && Element::children($variables['content']['field_title'])) {
      $variables['title'] = $variables['content']['field_title'];
      unset($variables['content']['field_title']);
    }
  
    // Display view of articles by pillar.
    if ($paragraph->hasField('field_pillars')) {
      $tid = $paragraph->get('field_pillars')->target_id;
  
      // Load children terms and add them as well.
      $children = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadChildren($tid, 'project_uptime_pillars');
      if (!empty($children)) {
        $tid .= '+' . implode('+', array_keys($children));
      }
  
      // Now add the carousel.
      $variables['carousel'] = views_embed_view('project_uptime', 'articles_by_category_block', $tid);
    }
  }
}

/**
 * Implements hook_preprocess_field().
 */
function unitedrentals_preprocess_field(array &$variables) {
  if (!isset($variables['attributes']['class']) || !is_array($variables['attributes']['class'])) {
    $variables['attributes']['class'] = [];
  }

  if (isset($variables['field_name']) && !empty($variables['field_name'])
    && !in_array($variables['field_name'], $variables['attributes']['class'])
  ) {
    $variables['attributes']['class'][] = 'field--name-' . Html::getClass($variables['field_name']);
  }

  if (isset($variables['field_type']) && !empty($variables['field_type'])
    && !in_array($variables['field_type'], $variables['attributes']['class'])
  ) {
    $variables['attributes']['class'][] = 'field--type-' . Html::getClass($variables['field_type']);
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE() for image_divider.
 */
function unitedrentals_preprocess_paragraph__image_divider(array &$variables) {
  $variables['content_attributes']['class'][] = 'image-divider';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for list_links, full.
 */
function unitedrentals_preprocess_paragraph__list_links__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Convert field_links to unordered list.
  $variables['list']['#attributes']['class'][] = "{$base_class}__list";
  $variables['list']['#wrapper_attributes'] = [];
  $variables['list']['#items'] = [];
  $variables['list']['#theme'] = 'item_list';
  // Run through field items'.
  foreach (Element::children($variables['content']['field_links']) as $delta) {
    // Add class to list-item.
    $variables['content']['field_links'][$delta]['#wrapper_attributes']['class'][] = "{$base_class}__list-item";
    // Add field item to list item.
    $variables['list']['#items'][] = $variables['content']['field_links'][$delta];
  }
  // Remove field render array.
  unset($variables['content']['field_links']);

  // Add component library.
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__list_links';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for pullquote,
 * full.
 */
function unitedrentals_preprocess_paragraph__pullquote__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Remove wrapper from field_content_plain.
  unset($variables['content']['field_content_plain']['#theme']);

  // Hide tab heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';

  // Move attribution to footer.
  if (array_key_exists('field_attribution', $variables['content']) && !empty($variables['content']['field_attribution'])) {
    $variables['footer']['field_attribution'] = $variables['content']['field_attribution'];
    unset($variables['content']['field_attribution']);
  }

  // Attach theme-crossover styles.
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__pullquote';
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for project update, carousel_card.
 */
function unitedrentals_preprocess_node__project_uptime_pages__carousel_card(array &$variables) {
  $node = $variables['node'];

  // Add title.
  $variables['title'] = $node->getTitle();

  // Add category.
  $pillars = $node->get('field_pillars')->getValue();
  $variables['category_ids'] = [];
  $i = 0;
  foreach ($pillars as $pillar) {
    $url = Url::fromUserInput('/project-uptime/search',
      [
        'query' => [
          "pillars[{$pillar['target_id']}]" => $pillar['target_id']
        ],
      ]
    );
    /** @var \Drupal\taxonomy\Entity\Term $categoryObj */
    $categoryObj = Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($pillar['target_id']);
    $variables['categories'][$i] = [
      'path' => $url->toString(),
      'name' => $categoryObj->getName(),
    ];
    $i++;
  }
  $variables['categories'][$i - 1]['last'] = TRUE;

  // Add create date.
  $variables['post_date'] = date('M j, Y', $node->getCreatedTime());
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for project uptime pages, teaser.
 */
function unitedrentals_preprocess_node__project_uptime_pages__teaser(&$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];

  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__node__teaser__project_uptime_pages';

  // Initialize variables.
  $variables['#attribute_variables'][] = 'image_group_attributes';
  $variables['#attribute_variables'][] = 'tag_group_attributes';
  $variables['image_group_attributes']['class'][] = 'node__image-container';
  $variables['tag_group_attributes']['class'][] = 'node__tags-container';

  // Aggregate the Pillars and Business Unit tags into single list field.
  $links = [];
  $project_uptime_url = '/project-uptime/search';
  if (!$node->get('field_pillars')->isEmpty()) {
    // Get all taxonomy term entities from the field.
    $pillars = $node->get('field_pillars')->referencedEntities();

    /** @var \Drupal\taxonomy\Entity\Term $pillar */
    foreach ($pillars as $pillar) {
      // Generate link to exposed filter on the listing pages.
      $links[] = [
        '#type' => 'link',
        '#title' => \Drupal::translation()->translate($pillar->getName()),
        '#url' => Url::fromUserInput($project_uptime_url, [
          'query' => [
            "pillars[{$pillar->id()}]" => $pillar->id()
          ]
        ]),
      ];
    }
    // Unset original field.
    unset($variables['content']['field_pillars']);
  }

  if (!$node->get('field_business_units')->isEmpty()) {
    // Get all taxonomy term entities from the field.
    $business_units = $node->get('field_business_units')->referencedEntities();

    /** @var \Drupal\taxonomy\Entity\Term $business_unit */
    foreach ($business_units as $business_unit) {
      // Generate link to exposed filter on the listing pages.
      $links[] = [
        '#type' => 'link',
        '#title' => \Drupal::translation()->translate($business_unit->getName()),
        '#url' => Url::fromUserInput($project_uptime_url, [
          'query' => [
            "business-units[{$business_unit->id()}]" => $business_unit->id()
          ]
        ]),
      ];
    }
    // Unset original field.
    unset($variables['content']['field_business_units']);
  }


  // Create list from aggregated tags.
  if (!empty($links)) {
    $tags = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#attributes' => [
        'class' => [
          'node__tags'
        ],
      ],
      '#items' => $links,
    ];

    $variables['tags'] = $tags;
  }

  /*
   * Generate "New" tag based on created time. If created within 30 days from
   * now it's considered "new".
   */
  $created = $node->getCreatedTime();
  if ($created >= strtotime('-30 days')) {
    $new = \Drupal::translation()->translate('New');
    $variables['new_tag'] = [
      '#markup' => "<span class='node__new-tag-pill'>$new</span>",
    ];
  }

  // Generate author byline if author info available.
  $author_name = '';
  if (!$node->get('field_author')->isEmpty() && !empty($variables['content']['field_author'])) {

    $author = $node->get('field_author')->referencedEntities();
    if (!empty($author)) {
      /** @var \Drupal\user\Entity\User $author */
      $author = reset($author);
      $author_name = $author->getUsername();
    }
    unset($variables['content']['field_author']);
  }

  // Add byline.
  project_uptime_byline($variables);

  // Move image field to it's own variable.
  if (!$node->get('field_image')->isEmpty() && isset($variables['content']['field_image'])) {
    $variables['image'] = $variables['content']['field_image'];
    // Unset field wrappers around image.
    unset($variables['image']['#theme']);
    // Unset image field from content
    unset($variables['content']['field_image']);
  }
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for project uptime pages, full.
 */
function unitedrentals_preprocess_node__project_uptime_pages__full(&$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];
  $markup = '';

  project_uptime_byline($variables);

  $variables['image_group_attributes']['class'][] = 'node__image-container';

  // Move subhead field above byline
  $variables['subhead'] = $variables['content']['field_sub_headline'];

  // Unset subhead field from content
  unset($variables['content']['field_sub_headline']);

  // Remove the image from content since it was moved above
  unset($variables['content']['field_image']);

  // Aggregate the Pillars and Business Unit tags into single list field.
  $links = [];
  $project_uptime_url = '/project-uptime/search';
  if (!$node->get('field_pillars')->isEmpty()) {
    // Get all taxonomy term entities from the field.
    $pillars = $node->get('field_pillars')->referencedEntities();

    /** @var \Drupal\taxonomy\Entity\Term $pillar */
    foreach ($pillars as $pillar) {
      // Generate link to exposed filter on the listing pages.
      $links[] = [
        '#type' => 'link',
        '#title' => \Drupal::translation()->translate($pillar->getName()),
        '#url' => Url::fromUserInput($project_uptime_url, [
          'query' => [
            "pillars[{$pillar->id()}]" => $pillar->id()
          ]
        ]),
        '#attributes' => [
          'class' => [
            'tag'
          ]
        ]
      ];
    }
    // Unset original field.
    unset($variables['content']['field_pillars']);
  }

  if (!$node->get('field_business_units')->isEmpty()) {
    // Get all taxonomy term entities from the field.
    $business_units = $node->get('field_business_units')->referencedEntities();

    /** @var \Drupal\taxonomy\Entity\Term $business_unit */
    foreach ($business_units as $business_unit) {
      // Generate link to exposed filter on the listing pages.
      $links[] = [
        '#type' => 'link',
        '#title' => \Drupal::translation()->translate($business_unit->getName()),
        '#url' => Url::fromUserInput($project_uptime_url, [
          'query' => [
            "business-units[{$business_unit->id()}]" => $business_unit->id()
          ]
        ]),
        '#attributes' => [
          'class' => [
            'tag'
          ]
        ]
      ];
    }
    // Unset original field.
    unset($variables['content']['field_business_units']);
  }


  // Create list from aggregated tags.
  if (!empty($links)) {
    $tags = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#attributes' => [
        'class' => [
          'project-uptime-article__tags'
        ],
      ],
      '#items' => $links,
    ];

    $variables['tags'] = $tags;
  }

  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__node__full__project_uptime_pages';
}

/**
 * Implements hook_preprocess_block__BLOCK_ID() for helpfulness rating block.
 */
function unitedrentals_preprocess_block__helpfulness_rating_block(&$variables) {
  // Add block wrappers.
  $variables['jsa_block_wrap_open'] = FALSE;
  $variables['jsa_block_wrap_close'] = TRUE;

  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__block__helpfulness_rating_block';
}

/**
 * Implements hook_preprocess_block__BLOCK_ID() for helpfulness rating block.
 */
function unitedrentals_preprocess_block__block_content_promo_banner(&$variables) {
  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__block__block_content__promo_banner';
}

/**
 * Implements hook_preprocess_block__BLOCK_ID() for helpfulness rating block.
 */
function unitedrentals_preprocess_block__views_block__equipment_categories_tile_cards(&$variables) {
  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__block__views__views_block__equipment_categories_tile_cards';
}

/**
 * Implements hook_preprocess_block__BLOCK_ID() for helpfulness rating block.
 */
function unitedrentals_preprocess_block__views_block__equipment_categories_default_cards(&$variables) {
  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__block__views__views_block__equipment_categories_default_cards';
}

/**
 * Implements hook_preprocess_block__BLOCK_ID() for project uptime social sharing block.
 */
function unitedrentals_preprocess_block__project_uptime_social_sharing(&$variables) {
  // add class
  $variables['attributes']['class'][] = 'block-social-sharing-block';
}

/**
 * Implements hook_preprocess_block__BLOCK_ID() for helpfulness rating block.
 */
function unitedrentals_preprocess_block__pardotprojectuptimesubscribeblock(&$variables) {
  // Add block prefix.
  $variables['jsa_block_wrap_open'] = TRUE;
  $variables['jsa_block_wrap_close'] = FALSE;

  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__block__project_uptime_subscribe_block';
}

/**
 * Helper function that assembles a byline which includes the optional author name and date created into single render array variable.
 */
function project_uptime_byline(array &$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];

  // Generate author byline if author info available.
  $author_name = '';
  if (!$node->get('field_author')->isEmpty() && !empty($variables['content']['field_author'])) {

    $author = $node->get('field_author')->referencedEntities();
    if (!empty($author)) {
      /** @var \Drupal\user\Entity\User $author */
      $author = reset($author);
      $author_name = $author->getUsername();
    }
    unset($variables['content']['field_author']);
  }

  // We'll always show the created date though.
  /** @var \Drupal\Core\Datetime\DateFormatter $date_formatter */
  $date_formatter = \Drupal::service('date.formatter');
  $formatted_date = $date_formatter->format($node->getCreatedTime(), 'custom', 'M j, Y');

  // Determine if we have an author name to show with byline.
  $byline = !empty($author_name)
    ? t('By') . ' ' . $author_name . ' | ' . $formatted_date
    : $formatted_date;

  $variables['byline'] = [
    '#markup' => $byline,
    '#weight' => -50,
  ];
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for content, full.
 */
function unitedrentals_preprocess_paragraph__content__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Remove the field wrappers around the copy
  if (array_key_exists('field_copy', $variables['content']) && !empty($variables['content']['field_copy'])) {
    unset($variables['content']['field_copy']['#theme']);
  }

  // Remove the field wrappers around the copy
  if (array_key_exists('field_media_item', $variables['content']) && !empty($variables['content']['field_media_item'])) {
    unset($variables['content']['field_media_item']['#theme']);
  }

  // Move cta link to footer and add class.
  if (array_key_exists('field_cta_reference', $variables['content']) && !empty($variables['content']['field_cta_reference'])) {
    $variables['footer']['field_cta_reference'] = $variables['content']['field_cta_reference'];
    $variables['footer']['field_cta_reference'][0]['#options']['attributes']['class'] = "{$base_class}__footer-cta";
    unset($variables['content']['field_cta_reference'], $variables['footer']['field_cta_reference']['#theme']);
  }

  // Move media field to new variable.
  if (isset($variables['content']['field_media_item']) && !empty($variables['content']['field_media_item'])) {
    $variables['media'] = $variables['content']['field_media_item'];
    $variables['media']['#attributes']['class'][] = "{$base_class}__media";
    unset($variables['content']['field_media_item']);

    // Add a wrapper to each item if it has a url present.
    if (isset($variables['media']['#items'])) {
      $field_values = $variables['media']['#items']->getValue();
      foreach (Element::children($variables['media']) as $delta) {
        if (isset($field_values[$delta]['url']) && !empty($field_values[$delta]['url'])) {
          $url = Url::fromUri($field_values[$delta]['url']);
          $variables['media'][$delta]['#prefix'] = "<a href=\"{$url->toString()}\">";
          $variables['media'][$delta]['#suffix'] = '</a>';
        }
      }
    }
  }

  // Move video field to new variable.
  if (isset($variables['content']['field_video']) && !empty($variables['content']['field_video'])) {
    $variables['video'] = $variables['content']['field_video'];
    $variables['video']['#attributes']['class'][] = "{$base_class}__video";
    unset($variables['content']['field_video'], $variables['video']['field_video']['#theme']);
  }

  // attach library.
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__content';
}  

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for cta, full.
 */
function unitedrentals_preprocess_paragraph__cta__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Move link to footer and add class.
  $variables['footer']['field_link'] = $variables['content']['field_link'];
  $variables['footer']['field_link'][0]['#options']['attributes']['class'] = "{$base_class}__link";
  unset($variables['content']['field_link'], $variables['footer']['field_link']['#theme']);

  // attach library.
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__cta';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for hero, full.
 */
function unitedrentals_preprocess_paragraph__hero__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Track whether the hero has slides.
  $variables['has_slides'] = !$paragraph->get('field_paragraphs')->isEmpty();

  // Track whether to add the global search component to the hero.
  $variables['has_search'] = FALSE;
  $layout = ($paragraph->get('field_style')->isEmpty()) ? NULL : $paragraph->get('field_style')->target_id;
  switch ($layout) {
    case 'hero__with_search':
      $variables['has_search'] = TRUE;
      $variables['search_placeholder'] = \Drupal::config('ur_admin.settings')->get('global_search_placeholder');
      break;
  }

  // attach library.
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__hero';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for hero_slide,
 * full.
 */
function unitedrentals_preprocess_paragraph__hero_slide__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Initialize variables.
  $variables['inner_attributes']['class'][] = "{$base_class}__inner";

  // Move media field to new variable.
  $variables['media'] = $variables['content']['field_media_background'];
  unset($variables['media']['#theme']);
  unset($variables['content']['field_media_background']);

  // attach library.
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__hero_slide';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__BLOCK_ID() for pagetitle block,
 * full.
 */
function unitedrentals_preprocess_block__page_title_block(array &$variables) {
  // Move title from content to title.
  $variables['title'] = $variables['content'];
  $variables['content'] = []; 

  // Load the node entity from current route.
  $node = \Drupal::routeMatch()->getParameter('node');
  if (is_null($node)) {
    $node = \Drupal::routeMatch()->getParameter('node_preview');
  }
  if ($node instanceof NodeInterface) {

    // Get subtitle.
    if ($node->hasField('field_subtitle') && !$node->get('field_subtitle')->isEmpty()) {
      $variables['content']['subtitle'] = $node->get('field_subtitle')->view('full');
    }   

    // Get description.
    if ($node->hasField('body') && !$node->get('body')->isEmpty()) {
      $variables['content']['body'] = $node->get('body')->view('full');
    }   

    // Get hero to replace header.
    if ($node->hasField('field_image') && !$node->get('field_image')->isEmpty()) {
      $variables['hero'] = $node->get('field_image')->view('full');
      unset($variables['hero']['#theme']);
    }   
  }
}

/**
 * Implements hook_preprocessnode__BUNDLE__VIEW_MODE() for landing,
 * full.
 */
function unitedrentals_preprocess_node__landing__full(array &$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];

  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__node__full__landing';
}

/**
 * @file
 * Preprocess functions related to media entities.
 *
 * Index:
 * @see unitedrentals_preprocess_media()
 * @see unitedrentals_preprocess_media__remote_video__hero()
 */

/**
 * Implements hook_preprocess_media().
 */
function unitedrentals_preprocess_media(array &$variables) {
  /** @var \Drupal\media\MediaInterface $media */
  $media = $variables['media'];


  // Track that iframe_attributes should get converted.
  $variables['#attribute_variables'][] = 'media_attributes';

  // Add base classes.
  $variables['media_attributes']['class'][] = 'media__media';

  // Move respective media field into its own variable.
  $field_name = NULL;
  switch ($media->bundle()) {
    case 'audio':
      $field_name = 'field_media_audio_file';
      break;

    case 'file':
      $field_name = 'field_media_file';
      break;

    case 'image':
      $field_name = 'field_media_image';
      break;

    case 'icon':
      $field_name = 'field_media_image';
      break;

    case 'remote_video':
      $field_name = 'field_media_video_embed_field';
      break;

    case 'video':
      $field_name = 'field_media_video_file';
      break;
  }
  if (!is_null($field_name) && array_key_exists($field_name, $variables['content'])) {
    $variables['media_embed'] = $variables['content'][$field_name];
    unset($variables['media_embed']['#theme']);
    unset($variables['content'][$field_name]);
  }

}

/**
 * Implements hook_preprocess_media__BUNDLE__VIEW_MODE() for remote_video, hero.
 */
function unitedrentals_preprocess_media__remote_video__hero(array &$variables) {
  /** @var \Drupal\media\MediaInterface $media */
  $media = $variables['media'];

  // Make video autoplay, loop and disabling any controls and branding.
  if (isset($variables['media_embed'][0]['children']['#provider'])) {
    // Get provider information.
    $provider = $variables['media_embed'][0]['children']['#provider'];
    $provider_definition = \Drupal::service('video_embed_field.provider_manager')->getDefinition($provider);
    $provider_class = $provider_definition['class'];
    $provider_id = $provider_class::getIdFromInput($variables['media_embed']['#items']->first()->value);

    // Make modifications to the embed based on provider.
    switch ($provider) {
      case 'vimeo':
        $variables['media_embed'][0]['children']['#attributes']['title'] = $media->label();
        $variables['media_embed'][0]['children']['#query']['autoplay'] = 1;
        $variables['media_embed'][0]['children']['#query']['background'] = 1;
        $variables['media_embed'][0]['children']['#query']['loop'] = 1;
        $variables['media_embed'][0]['children']['#query']['muted'] = 1;
        break;

      case 'youtube':
        $variables['media_embed'][0]['children']['#attributes']['title'] = $media->label();
        $variables['media_embed'][0]['children']['#attributes']['tabindex'] = '-1';
        $variables['media_embed'][0]['children']['#query']['autoplay'] = 1;
        $variables['media_embed'][0]['children']['#query']['showinfo'] = 0;
        $variables['media_embed'][0]['children']['#query']['controls'] = 0;
        $variables['media_embed'][0]['children']['#query']['mute'] = 1;
        $variables['media_embed'][0]['children']['#query']['disablekb'] = 1;
        $variables['media_embed'][0]['children']['#query']['fs'] = 0;
        $variables['media_embed'][0]['children']['#query']['mute'] = 1;
        $variables['media_embed'][0]['children']['#query']['loop'] = 1;
        $variables['media_embed'][0]['children']['#query']['modestbranding'] = 1;
        $variables['media_embed'][0]['children']['#query']['playlist'] = $provider_id;
        break;
    }
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for card, full.
 */
function unitedrentals_preprocess_paragraph__card__full(array &$variables) {
  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__card__full';

  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];

  // Move link to it's own variable.
  if (!$paragraph->get('field_link')->isEmpty()) {
    $variables['url'] = $paragraph->get('field_link')->first()->getUrl()->toString();
    unset($variables['content']['field_link']);
  }

  // Move media item to it's own variable.
  if (!$paragraph->get('field_media_item')->isEmpty()) {
    $variables['media'] = $variables['content']['field_media_item'];
    unset($variables['content']['field_media_item']);
  }

  // Move field_cta (super title) to it's own variable.
  if (!$paragraph->get('field_cta')->isEmpty()) {
    $variables['super_title'] = $variables['content']['field_cta'];
    unset($variables['content']['field_cta']);
  }

}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for cards_wrapper, full.
 */
function unitedrentals_preprocess_paragraph__cards_wrapper__full(array &$variables) {
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__cards_wrapper';
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

//  $variables['content']['field_paragraphs']

  // Convert field_related_content to unordered list.
  $variables['list']['#attributes']['class'][] = "{$base_class}__list";
  $variables['list']['#wrapper_attributes'] = [];
  $variables['list']['#items'] = [];
  $variables['list']['#theme'] = 'item_list';

  // Hide list content heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';

  // Run through field items'.
  foreach (Element::children($variables['content']['field_paragraphs']) as $delta) {
    // Add class to list-item.
    $variables['content']['field_paragraphs'][$delta]['#wrapper_attributes']['class'][] = "{$base_class}__list-item";
    // Add field item to list item.
    $variables['list']['#items'][] = $variables['content']['field_paragraphs'][$delta];
  }
  // Remove field render array.
  unset($variables['content']['field_paragraphs']);
}

/**
 * Implements hook_preprocess_paragraph__VIEW_MODE() for compartment, full.
 */
function unitedrentals_preprocess_paragraph__compartment__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  $variables['super_title'] = $variables['content']['field_super_title'];
  unset($variables['content']['field_super_title']);

  $variables['sub_title'] = $variables['content']['field_subtitle'];
  unset($variables['content']['field_subtitle']);

  // Move cta link to footer and add class.
  if (array_key_exists('field_cta', $variables['content']) && !empty($variables['content']['field_cta'])) {
    $variables['footer']['field_cta'] = $variables['content']['field_cta'];
    $variables['footer']['field_cta'][0]['#options']['attributes']['class'] = "{$base_class}__cta";
    unset($variables['content']['field_cta'], $variables['footer']['field_cta']['#theme']);
  }

  $field_settings['type'] = 'entity_reference_entity_view';
  $field_settings['settings']['view_mode'] = 'teaser';
  $field_settings['label'] = 'hidden';

  // attach libraries
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__compartment';
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__compartment__layout_slider';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for embed_iframe,
 * full.
 */
function unitedrentals_preprocess_paragraph__embed_iframe__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Track that iframe_attributes should get converted.
  $variables['#attribute_variables'][] = 'iframe_attributes';

  // Clean src input and compile style tag.
  $src = $paragraph->get('field_link')->isEmpty() ?: $paragraph->get('field_link')->uri;
  $style_tag = '';
  $height = '600px';
  if (!$paragraph->get('field_style_height')->isEmpty()) {
    $height = Html::escape($paragraph->get('field_style_height')->value);
    $style_tag .= "height: {$height}; ";
  }
  $width = '100%';
  if (!$paragraph->get('field_style_width')->isEmpty()) {
    $width = Html::escape($paragraph->get('field_style_width')->value);
    $style_tag .= "height: {$height}; ";
  }

  // Set attributes.
  $variables['iframe_attributes']['class'][] = "{$base_class}__iframe";
  $variables['iframe_attributes']['height'] = $height;
  $variables['iframe_attributes']['src'] = $src;
  $variables['iframe_attributes']['style'] = $style_tag;
  $variables['iframe_attributes']['width'] = $width;
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for featured_content, full.
 */
function unitedrentals_preprocess_paragraph__featured_content__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Determine changes based on selected layout.
  $layout = ($paragraph->get('field_layout')->isEmpty()) ? NULL : $paragraph->get('field_layout')->target_id;
  switch ($layout) {
    case 'featured_content_layout__grid':
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__featured_content__layout_grid';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = 'teaser';
      $field_settings['label'] = 'hidden';
      break;

    case 'featured_content_layout__masonry':
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__featured_content__layout_masonry';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = 'masonry';
      $field_settings['label'] = 'hidden';
      break;

    case 'featured_content_layout__list':
    default:
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__featured_content__layout_list';
      $field_settings['type'] = 'entity_reference_label';
      $field_settings['settings']['link'] = TRUE;
      $field_settings['label'] = 'hidden';
  }

  // Replace field with new settings.
  $variables['content']['field_related_content'] = $paragraph->get('field_related_content')->view($field_settings);

  // Convert field_related_content to unordered list.
  $variables['list']['#attributes']['class'][] = "{$base_class}__list";
  $variables['list']['#wrapper_attributes'] = [];
  $variables['list']['#items'] = [];
  $variables['list']['#theme'] = 'item_list';

  // Hide list content heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';

  // Run through field items'.
  foreach (Element::children($variables['content']['field_related_content']) as $delta) {
    // Add class to list-item.
    $variables['content']['field_related_content'][$delta]['#wrapper_attributes']['class'][] = "{$base_class}__list-item";
    // Add field item to list item.
    $variables['list']['#items'][] = $variables['content']['field_related_content'][$delta];
  }
  // Remove field render array.
  unset($variables['content']['field_related_content']);
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for featured_term, full.
 */
function unitedrentals_preprocess_paragraph__featured_term__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Determine view mode.
  $view_mode = ($paragraph->get('field_style')->isEmpty()) ? 'default' : str_replace('term_view_mode__', '', $paragraph->get('field_style')->target_id);

  // Determine changes based on selected layout.
  $layout = ($paragraph->get('field_layout')->isEmpty()) ? NULL : $paragraph->get('field_layout')->target_id;
  switch ($layout) {
    case 'featured_content_layout__grid':
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__featured_term__layout_grid';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = $view_mode;
      $field_settings['label'] = 'hidden';
      break;

    case 'featured_content_layout__slider':
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__featured_term__layout_slider';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = $view_mode;
      $field_settings['label'] = 'hidden';
      break;

    case 'featured_content_layout__slider_3col':
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__featured_term__layout_slider_3col';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = $view_mode;
      $field_settings['label'] = 'hidden';
      break;

    case 'featured_content_layout__slider_4col':
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__featured_term__layout_slider_4col';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = $view_mode;
      $field_settings['label'] = 'hidden';
      break;

    case 'featured_content_layout__list':
    default:
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__featured_term__layout_list';
      $field_settings['type'] = 'entity_reference_label';
      $field_settings['settings']['link'] = TRUE;
      $field_settings['label'] = 'hidden';
  }

  // Replace field with new settings.
  $variables['content']['field_term'] = $paragraph->get('field_term')->view($field_settings);

  // Convert field_related_content to unordered list.
  $variables['list']['#attributes']['class'][] = "{$base_class}__list";
  $variables['list']['#wrapper_attributes'] = [];
  $variables['list']['#items'] = [];
  $variables['list']['#theme'] = 'item_list';

  // Hide list content heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';

  // Run through field items'.
  foreach (Element::children($variables['content']['field_term']) as $delta) {
    // Add class to list-item.
    $variables['content']['field_term'][$delta]['#wrapper_attributes']['class'][] = "{$base_class}__list-item";
    // Add field item to list item.
    $variables['list']['#items'][] = $variables['content']['field_term'][$delta];
  }
  // Remove field render array.
  unset($variables['content']['field_term']);
}

/**
 * Implements hook_preprocess_paragraph__VIEW_MODE() for media, full.
 */
function unitedrentals_preprocess_paragraph__featured_media__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Determine changes based on selected layout.
  $layout = ($paragraph->get('field_layout')->isEmpty()) ? NULL : $paragraph->get('field_layout')->target_id;
  switch ($layout) {
    case 'media_layout__grid':
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__media__layout_grid';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = 'teaser';
      $field_settings['label'] = 'hidden';
      break;

    case 'media_layout__masonry':
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__media__layout_masonry';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = 'masonry';
      $field_settings['label'] = 'hidden';
      break;

    case 'media_layout__slider':
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__media__layout_slider';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = 'embed';
      $field_settings['label'] = 'hidden';
      break;

    case 'media_layout__full':
    default:
      $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__media__layout_full';
      $field_settings['type'] = 'entity_reference_entity_view';
      $field_settings['settings']['view_mode'] = 'embed';
      $field_settings['label'] = 'hidden';
      break;
  }

  // Replace field with new settings.
  $variables['content']['field_media_item'] = $paragraph->get('field_media_item')->view($field_settings);

  // Convert field_media to unordered list.
  $variables['list']['#attributes']['class'][] = "{$base_class}__list";
  $variables['list']['#wrapper_attributes'] = [];
  $variables['list']['#items'] = [];
  $variables['list']['#theme'] = 'item_list';

  // Run through field items'.
  foreach (Element::children($variables['content']['field_media_item']) as $delta) {
    // Add class to list-item.
    $variables['content']['field_media_item'][$delta]['#wrapper_attributes']['class'][] = "{$base_class}__list-item";
    // Add field item to list item.
    $variables['list']['#items'][] = $variables['content']['field_media_item'][$delta];
  }

  // Remove field render array.
  unset($variables['content']['field_media_item']);
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for item, card view mode.
 */
function unitedrentals_preprocess_node__item__card(array &$variables) {
  if (array_key_exists('field_item_cat_class_code', $variables['content']) && Element::children($variables['content']['field_item_cat_class_code'])) {
    $variables['content']['field_item_cat_class_code']['#title'] = 'Cat Class';
    $variables['cat_class'] = $variables['content']['field_item_cat_class_code'];
    unset($variables['content']['field_item_cat_class_code']);
  }

  if (array_key_exists('flag_favorites', $variables['content']) && Element::children($variables['content']['flag_favorites'])) {
    $variables['fav_link'] = $variables['content']['flag_favorites'];
    unset($variables['content']['flag_favorites']);
  }

  // Run through photos unlimited and remove all but the first.
  foreach (Element::children($variables['content']['field_images_unlimited']) as $delta) {
    if ($delta) {
      unset($variables['content']['field_images_unlimited'][$delta]);
    }
  }

  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__node__card__item';
}

/**
 * Implements hook_preprocess_node__BUNDLE__VIEW_MODE() for styleguide, full.
 */
function unitedrentals_preprocess_node__styleguide__full(array &$variables) {
  // Unset body field as this is used in page subtitle.
  // See: unitedrentals_preprocess_block__page_title_block for reference.
  if (array_key_exists('body', $variables['content']) && !empty($variables['content']['body'])) {
    unset($variables['content']['body']);
  }
}

function unitedrentals_preprocess_taxonomy_term(array &$variables) {
  /** @var \Drupal\taxonomy\Entity\Term $term **/
  $term = $variables['term'];
  $view_mode = $variables['view_mode'];
  $vocab_id = $term->getVocabularyId();

  $vocab_function = __FUNCTION__ . "__$vocab_id";
  if (function_exists($vocab_function)) {
    $vocab_function($variables);
  }

  $view_function = __FUNCTION__ . "__{$view_mode}";
  if (function_exists($view_function)) {
    $view_function($variables);
  }

  $vocab_and_view_mode_function = __FUNCTION__ . "__{$vocab_id}__{$view_mode}";
  if (function_exists($vocab_and_view_mode_function)) {
    $vocab_and_view_mode_function($variables);
  }
}

/**
 * Implements hook_preprocess_taxonomy_term().
 */
function unitedrentals_preprocess_taxonomy_term__card(array &$variables) {
  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__taxonomy_term__card';
}

/**
 * Implements hook_preprocess_taxonomy_term().
 */
function unitedrentals_preprocess_taxonomy_term__card_tile(array &$variables) {
  unset($variables['content']['field_media_icon']['#theme']);

  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__taxonomy_term__card_tile';
}

/**
 * Implements hook_preprocess_taxonomy_term().
 */
function unitedrentals_preprocess_taxonomy_term__full_bleed_card(array &$variables) {
  /** @var \Drupal\taxonomy\Entity\Term $term **/
  $term = $variables['term'];

  $ancestors = Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadAllParents($term->id());
  if (is_array($ancestors) && count($ancestors) > 1) {
    $parent = $ancestors[array_keys($ancestors)[1]];
    $options = ['absolute' => TRUE];
    $variables['parent'] = [
      'label' => $parent->label(),
      'url' => Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $parent->id()], $options),
    ];
  }

  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__taxonomy_term__full_bleed_card';
}

/**
 * Implements hook_preprocess_views_view().
 */
function unitedrentals_preprocess_views_view__item_cards__favorites(array &$variables) {
  // All views get the pager library.
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__view__item_cards__favorites';
}

function unitedrentals_preprocess_views_view__company_cards(array &$variables) {
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__views__image_cards';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for tabs, full.
 */
function unitedrentals_preprocess_paragraph__tabs__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Hide tab heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';

  // Initialize Navigation variable.
  $variables['nav']['#attributes']['class'][] = "{$base_class}__nav";
  $variables['nav']['#wrapper_attributes'] = [];
  $variables['nav']['#items'] = [];
  $variables['nav']['#theme'] = 'item_list';

  // Validate and create nav from tab list.
  foreach ($paragraph->get('field_paragraphs') as $delta => $component) {
    /** @var \Drupal\entity_reference_revisions\Plugin\Field\FieldType\EntityReferenceRevisionsItem $component */

    // Add tab to nav if title is set.
    if ($component->entity instanceof ParagraphInterface && !$component->entity->get('field_title')->isEmpty()
      && unitedrentals_show_tab($component->entity)
    ) {
      $variables['nav']['#items'][] = [
        '#type' => 'link',
        '#title' => $component->entity->get('field_title')->value,
        '#url' => Url::fromUserInput("#paragraph-{$component->entity->id()}"),
        '#wrapper_attributes' => [
          'class' => ["{$base_class}__nav-item"],
        ],
      ];
    }
    // Otherwise remove it from being rendered.
    else {
      unset($variables['content']['field_paragraphs'][$delta]);
    }
  }

  // Toggle version of tabs. (This is done here mainly as a placeholder for if
  // we ever need to support vertical tabs or other version. Then we can make it
  // toggleable and just add the class.)
  $tab_version = 'horizontal';
  switch ($tab_version) {
    case 'horizontal':
    default:
      $variables['attributes']['class'][] = "{$base_class}--horizontal";
  }

  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__tabs';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for tabs_tab, full.
 */
function unitedrentals_preprocess_paragraph__tabs_tab__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Default to tabindex -1 for accessibility. JS will initialize and update.
  $variables['attributes']['tabindex'] = '-1';
  // Hide tab heading. This is rendered and visually hidden for accessibility.
  $variables['title_attributes']['class'][] = 'visually-hidden';

  // Limit tab visibility based on roles.
  if (!unitedrentals_show_tab($paragraph)) {
    unset($variables['content']);
  }
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for tabs_tab, full.
 */
function unitedrentals_preprocess_paragraph__promo_hero__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Remove component field-wrappers.
  if (array_key_exists('field_copy', $variables['content']) && !empty($variables['content']['field_copy'])) {
    unset($variables['content']['field_copy']['#theme']);
  }

  // Move media field to new variable.
  if (isset($variables['content']['field_media_item']) && !empty($variables['content']['field_media_item'])) {
    $variables['media'] = $variables['content']['field_media_item'];
    $variables['media']['#attributes']['class'][] = "{$base_class}__media";
    unset($variables['content']['field_media_item'], $variables['media']['#theme']);
  }
  
  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__promo_hero';
}

/**
 * Implements hook_preprocess_paragraph__BUNDLE__VIEW_MODE() for dynamic cta, full.
 */
function unitedrentals_preprocess_paragraph__dynamic_cta__full(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $base_class = $variables['component_base_class'];

  // Add all CTA links to cta_links variables.
  $first_link = NULL;
  if (isset($variables['content']['field_links']) && !empty($variables['content']['field_links'])) {
    $variables['cta_links'] = $variables['content']['field_links'];
    $variables['cta_links']['#theme'] = 'select';
    $variables['cta_links']['#options'] = [];
    if ($paragraph->hasField('field_links') && !$paragraph->get('field_links')->isEmpty()) {
      foreach ($paragraph->get('field_links')->getValue() as $link) {
        $uri = $link['uri'];
        if (stripos($link['uri'], 'http:') !== FALSE || stripos($link['uri'], 'https:') !== FALSE) {
          $url = Url::fromUri($uri);
          $cta_link = $url->toString();
        }
        elseif (stripos($link['uri'], 'public:') !== FALSE || stripos($link['uri'], 'entity:') !== FALSE) {
          $url = Url::fromUri($uri);
          $cta_link = $url->toString();
        }
        else {
          $url = Url::fromUserInput($uri);
          $cta_link = $url->toString();
        }

        // Set first link uri.
        if (empty($first_link) && !empty($url)) {
          $first_link = $url;
        }

        $variables['cta_links']['#options'][$cta_link] = $link['title'];
      }
    }

    // Remove field_links from output.
    unset($variables['content']['field_links']);
  }

  // Move link to footer and add class.
  if (isset($variables['content']['field_cta']) && !empty($variables['content']['field_cta'])) {
    $variables['cta'] = $variables['content']['field_cta'];

    // Set link if we have them.
    if (isset($first_link)) {
      $variables['cta'][0]['#url'] = $first_link;
    }

    $variables['cta'][0]['#options']['attributes']['class'] = "{$base_class}__link";
    unset($variables['content']['field_cta'], $variables['cta']['#theme']);
  }

  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__paragraph__full__dynamic_cta';
}

/**
 * Implementation for Full Bleed Card view mode.
 */
function unitedrentals_preprocess_node__full_bleed_card(array &$variables) {
  $node = $variables['node'];
  $type = $node->getType();
  $id = $node->id();
  $variables['type_label'] = $node->type->entity->label();

  // Add title.
  $variables['card_title'] = \Drupal::translation()->translate($node->getTitle());

  // Content Specific Fields
  switch ($type) {
    case 'press_releases':
      $variables['card_date'] = date('M j, Y', strtotime($node->field_press_release_date->value));
      $variables['card_link'] = Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $id);
      break;
    case 'investor_relations':
      $variables['card_date'] = date('M j, Y', $node->getCreatedTime());
      $variables['card_link'] = '/our-company/investor-relations';
      $variables['card_image'] = ImageStyle::load('hero_bp_small_large')->buildUrl($node->field_investor_relations_image->entity->getFileUri());
      $pdf = $node->field_investor_relations_image->entity->getFileUri();
      if ($pdf) {
        $variables['card_link'] = $pdf;
      }   
      break;
    case 'news':
      $variables['card_date'] = $node->field_news_date->date->format('M j, Y');
      $variables['card_image'] = ImageStyle::load('hero_bp_small_large')->buildUrl($node->field_image->entity->getFileUri());
      $variables['card_link'] = $node->get('field_news_external_link')->first()->getUrl();
      break;
    case 'project_uptime_pages':
      $variables['card_date'] = date('M j, Y', $node->getCreatedTime());
      if (!$node->get('field_image')->isEmpty() && isset($variables['content']['field_image'])) {
        $variables['card_image'] = $variables['content']['field_image'];
      }
      $variables['card_link'] = Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $id);
      break;
    default:
      $variables['card_date'] = date('M j, Y', $node->getCreatedTime());
  }
    
  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__node__full_bleed_card';
}

/**
 * Implementation for Image Cards view mode.
 */
function unitedrentals_preprocess_node__image_cards(array &$variables) {

  $node = $variables['node'];
  $type = $node->getType();
  $id = $node->id();
  $variables['type_label'] = $node->type->entity->label();

  // Add title.
  $variables['card_title'] = \Drupal::translation()->translate($node->getTitle());

  // Content Specific Fields
  switch ($type) {
    case 'press_releases':
      $variables['card_date'] = date('M j, Y', strtotime($node->field_press_release_date->value));
      $variables['card_link'] = Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $id);
      break;
    case 'investor_relations':
      $variables['card_date'] = date('M j, Y', $node->getCreatedTime());
      $variables['card_link'] = '/our-company/investor-relations';
      $variables['card_image'] = ImageStyle::load('hero_bp_small_large')->buildUrl($node->field_investor_relations_image->entity->getFileUri());
      $pdf = $node->field_investor_relations_image->entity->getFileUri();
      if ($pdf) {
        $variables['card_link'] = $pdf;
      }   
      break;
    case 'news':
      $variables['card_date'] = $node->field_news_date->date->format('M j, Y');
      $variables['card_image'] = ImageStyle::load('hero_bp_small_large')->buildUrl($node->field_image->entity->getFileUri());
      $variables['card_link'] = $node->get('field_news_external_link')->first()->getUrl();
      break;
    case 'project_uptime_pages':
      $variables['card_date'] = date('M j, Y', $node->getCreatedTime());
      if (!$node->get('field_image')->isEmpty() && isset($variables['content']['field_image'])) {
        $variables['card_image'] = $variables['content']['field_image'];
      }
      $variables['card_link'] = Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $id);
      break;
    default:
      $variables['card_date'] = date('M j, Y', $node->getCreatedTime());
  }

  // attach library
  $variables['#attached']['library'][] = 'unitedrentals/theme_crossover__node__image_cards';
}

/**
 * Determine whether to show or hide tab.
 */
function unitedrentals_show_tab($paragraph) {
  $show = TRUE;
  if ($paragraph->hasField('field_limit_visibility_by_role')
    && !$paragraph->get('field_limit_visibility_by_role')->isEmpty()
  ) {
    $visibility_by_role = $paragraph->get('field_limit_visibility_by_role')->getValue();
    $user_roles = \Drupal::currentUser()->getRoles();
    $show = FALSE;

    // Administrator is all powerful. Stop here.
    if (\Drupal::currentUser()->id() == 1 || in_array('administrator', $user_roles)) {
      $show = TRUE;
    }

    // Check other roles.
    else {
      foreach ($visibility_by_role as $index => $role_target) {
        $role = $role_target['target_id'];
        // If we have a match set show to true and break out.
        if (in_array($role, $user_roles)) {
          $show = TRUE;
          break;
        }
      }
    }
  }

  return $show;
}
