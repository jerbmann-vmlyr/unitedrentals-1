(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.ieobjectfit = {
    attach: function (context, settings) {
      var $applyObjectFit = [
        '.component-content--style-brand .media--type-image',
        '.paragraph--type-promo-hero .media--type-image',
        '.node--view-mode-image-cards .node__content .field--type-image'
      ];

      // if IE or EDGE <15
      // disabling linter for IE check to make sure this doesn't happen on pages that can use object-fit
      if (document.createElement('div').style.objectFit === undefined) { // eslint-disable-line no-undefined
        $($applyObjectFit.join()).each(function () {
          var $container = $(this);
          var $imgUrl = $container.find('img').attr('src');

          if ($imgUrl) {
            $container.css({
              'background-image': 'url(' + $imgUrl + ')',
              'background-position': 'center center',
              'background-size': 'cover',
              'bottom': '0',
              'left': '0',
              'position': 'absolute',
              'right': '0',
              'top': '0'
            });
            $container.find('img').css('opacity', 0);
          }
        });
      }
    }
  };
})(jQuery, Drupal);

//# sourceMappingURL=../../maps/libraries/helper/helper--ie-object-fit.js.map
