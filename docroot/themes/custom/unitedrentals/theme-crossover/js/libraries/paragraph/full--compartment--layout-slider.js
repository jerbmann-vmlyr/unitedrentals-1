(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.compartmentslider = {
    attach: function (context, settings) {

      // When the slide is changing
      function playPauseVideo(slick, control) {
        var currentSlide;
        var player;

        currentSlide = slick.find('.slick-current');
        player = currentSlide.find('iframe').get(0);

        switch (control) {
          case 'play':
            // When we tell youtube api to play, pause the slider (will disable autoplay).
            slick.slick('slickPause');
            // Uncomment to have video play, but muted. Will save sanity when testing video.
            // postMessageToPlayer(player, {
            //   'event': 'command',
            //   'func': 'mute'
            // });
            postMessageToPlayer(player, {
              event: 'command',
              func: 'playVideo'
            });
            break;
            // Not using, but could make use of this later.
          case 'pause':
            postMessageToPlayer(player, {
              event: 'command',
              func: 'pauseVideo'
            });
            break;
          case 'stop':
            // When we tell youtube api to stop, play the slider (will start autoplay).
            slick.slick('slickPlay');
            postMessageToPlayer(player, {
              event: 'command',
              func: 'stopVideo'
            });
            break;
        }
      }

      // POST commands to YouTube or Vimeo API
      function postMessageToPlayer(player, command) {
        if (player == null || command == null) return; // eslint-disable-line curly
        player.contentWindow.postMessage(JSON.stringify(command), '*');
      }

      $('.ca-component-compartment--slider').each(function (index) {
        var $slick_slider = $(this).find('.component-compartment__content > div');

        // Before the slider changes to a new slide stop any video.
        $slick_slider.on('beforeChange', function (event, slick) {
          slick = $(slick.$slider);
          playPauseVideo(slick, 'stop');
        });

        var $settings = {
          arrows: false,
          centerMode: false,
          dots: false,
          infinite: true,
          autoplay: false,
          mobileFirst: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplaySpeed: 8000,
          responsive: [
            {
              breakpoint: 1023,
              settings: {
                arrows: true,
                autoplay: true
              }
            }
          ]
        };

        $slick_slider.slick($settings);

      });

      var video_wrapper = $('.component-content__video-wrapper');

      // On click of the video wrapper we want to...
      video_wrapper.on('click', function (e) {
        // Toggle that the slide is playing video.
        $(this).parents('.component-content').toggleClass('jsa-video-playing', true);
        // Grab the slick slider
        var $slick_slider = $(this).parents('.ca-component-compartment--slider').find('.component-compartment__content > div');
        // and tell it to play video.
        playPauseVideo($slick_slider, 'play');
      });

      // If we click anywhere else
      $(document).mouseup(function (e) {
        if (!video_wrapper.is(e.target) // Check if the target of the click isn't the container...
            && video_wrapper.has(e.target).length === 0 // ... nor a descendant of the container
            && $('.component-content').hasClass('jsa-video-playing')) { // ... and component has the video playing class
          // Remove class and stop the video.
          $('.component-content').removeClass('jsa-video-playing');
          var $slick_slider = $('.ca-component-compartment--slider .component-compartment__content > div');
          playPauseVideo($slick_slider, 'stop');
        }
      });
    }
  };
})(jQuery, Drupal);

//# sourceMappingURL=../../maps/libraries/paragraph/full--compartment--layout-slider.js.map
