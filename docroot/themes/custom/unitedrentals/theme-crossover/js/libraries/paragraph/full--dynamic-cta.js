(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.dynamicCta = {
    attach: function (context, settings) {
      $('.cta-links select', '.component-dynamic-cta').once('dynamic-cta-select').on('change', function () {
        $('a.component-dynamic-cta__link', '.component-dynamic-cta').attr('href', $(this).val());
      });
    }
  };

})(jQuery, Drupal);

//# sourceMappingURL=../../maps/libraries/paragraph/full--dynamic-cta.js.map
