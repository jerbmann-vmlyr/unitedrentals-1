(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.featuredtermslider3col = {
    attach: function (context, settings) {
      $('.component-featured-content--slider-3col').each(function (index) {
        var $slick_slider = $(this).find('.component-featured-term__list');
        var $settings = {
          arrows: true,
          centerMode: false,
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 4,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            },
            {
              breakpoint: 768,
              settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 2
              }
            }
          ]
        };

        $slick_slider.slick($settings);

      });
    }
  };
})(jQuery, Drupal);

//# sourceMappingURL=../../maps/libraries/paragraph/full--featured-term--layout-slider-3col.js.map
