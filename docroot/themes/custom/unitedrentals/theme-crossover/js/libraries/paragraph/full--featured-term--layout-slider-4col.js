(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.featuredtermslider4col = {
    attach: function (context, settings) {
      $('.component-featured-content--slider-4col').each(function (index) {
        var $slick_slider = $(this).find('.component-featured-term__list');
        var $settings = {
          arrows: false,
          centerMode: false,
          infinite: false,
          mobileFirst: true,
          slidesToShow: 2,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 1023,
              settings: {
                arrows: true,
                slidesToShow: 3
              }
            },
            {
              breakpoint: 1439,
              settings: {
                arrows: true,
                slidesToShow: 4
              }
            }
          ]
        };

        $slick_slider.slick($settings);

      });
    }
  };
})(jQuery, Drupal);

//# sourceMappingURL=../../maps/libraries/paragraph/full--featured-term--layout-slider-4col.js.map
