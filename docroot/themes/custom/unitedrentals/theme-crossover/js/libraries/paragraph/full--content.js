/**
 * This looks for images within a responsive-background-image data attribute div
 * and pulls child image source and makes it a background image on the div.
 * This works with responsive images as well as it ties into the image's load
 * event causing the background image to be updated as well at the various
 * breakpoints.
 *
 * Followed mostly from this tutorial:
 * https://aclaes.com/responsive-background-images-with-srcset-and-sizes/
 */

// (function ($, Drupal) {
//
//   'use strict';
//
//   Drupal.behaviors.componentContentVideo = {
//     attach: function (context) {
//
//       $('.component-content__video-wrapper').on('click', function (e) {
//
//         // Grab unique id of iframe
//         var iframeID = $(this).find('iframe')[0].id;
//
//         // Append unique id of iframe after 'jsa-video-playing' class
//         // If multiple embeded iframes are present, this provides unique classes for each iframe
//         $(this).parents('.component-content').toggleClass('jsa-video-playing' + '-' + iframeID, true);
//
//         // Get the src out of the iframe and set it to autoplay.
//         // NOTE: this will not work for mobile devices as they do not have the autoplay feature.
//         var src = $(this).find('.component-content__video iframe')[0].src.split('?')[0];
//         $(this).find('.component-content__video iframe')[0].src = src + '?autoplay=1&rel=0&start=0&frameborder=0&enablejsapi=1';
//
//         // Turn off slick slider autoplay when the slick section containing the video-wrapper is active
//         // This keeps the slider from autoplaying no matter how many clicks there are to the video wrapper
//         if ($(this).parents('section').has('.slick-current .slick-active')) {
//           var $slick_slider = $(this).parents('.ca-component-compartment--slider').find('.component-compartment__content > div');
//           $slick_slider.slick('slickSetOption', {autoplay: false}, true); // the last param is `refresh`
//         }
//
//         // Remove 'jsa-video-playing' class on click outside of '.component-content__video-wrapper'
//         $(document).mouseup(function (e) {
//           if (!$('.component-content__video-wrapper').is(e.target) // if the target of the click isn't the container...
//               && $('.component-content__video-wrapper').has(e.target).length === 0) { // ... nor a descendant of the container
//             $('.component-content').removeClass('jsa-video-playing' + '-' + iframeID);
//
//             // Get src out of the iframe without autoplay
//             var srcReset = $(this).find('.component-content__video iframe')[0].src.split('?')[0];
//
//             // Reset iframe to src without autoplay
//             $('.video-player').each(function () {
//               $(this).attr('src', srcReset);
//             });
//           }
//         });
//       });
//     }
//   };
//
// })(jQuery, Drupal);
