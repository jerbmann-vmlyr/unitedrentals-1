(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.companycardslide = {
    attach: function (context, settings) {
      $('.view-company-cards').each(function (index) {
        var $window = $(window);
        var $slick_slider = $(this).find('.view-content');
        var $settings = {
          arrows: false,
          infinite: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 5000,
              settings: 'unslick'
            },
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1
              }
            }
          ]
        };

        function breakpointSlider() {
          if ($window.width() >= 1024) {
            if ($slick_slider.hasClass('slick-initialized')) {
              $slick_slider.removeClass('slick-initialized');
            }
          }
          else {
            if (!$slick_slider.hasClass('slick-initialized')) {
              return $slick_slider.slick($settings);
            }
          }
        }

        $window.on('load', function () {
          breakpointSlider();
        });

        $window.on('resize', function () {
          breakpointSlider();
        });
      });
    }
  };
})(jQuery, Drupal);

//# sourceMappingURL=../../maps/libraries/block/company-cards--image-cards.js.map
