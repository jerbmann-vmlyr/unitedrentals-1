(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.equipmentdefaultcardslide = {
    attach: function (context, settings) {

      $('.block-views-blockequipment-categories-default-cards').each(function (index) {
        var $slick_slider = $(this).find('.view-content');
        var $settings = {
          arrows: false,
          centerMode: false,
          infinite: false,
          mobileFirst: true,
          slidesToShow: 2,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 1023,
              settings: {
                arrows: true,
                slidesToShow: 3
              }
            },
            {
              breakpoint: 1439,
              settings: {
                arrows: true,
                slidesToShow: 4
              }
            }
          ]
        };

        $slick_slider.slick($settings);

      });
    }
  };
})(jQuery, Drupal);

//# sourceMappingURL=../../maps/libraries/block/equipment-categories--default-cards.js.map
