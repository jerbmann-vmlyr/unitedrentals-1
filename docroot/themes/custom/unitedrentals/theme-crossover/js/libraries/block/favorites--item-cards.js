(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.ajaxrefreshfavorites = {
    attach: function (context, settings) {
      var viewName = 'item_cards';
      // Your views should be Ajax enabled
      var instances = Drupal.views.instances;
      var myViews;

      $.each(instances, function getInstance(index, element) {
        if (element.settings.view_name === viewName) {
          myViews = '.js-view-dom-id-' + element.settings.view_dom_id;
        }
      });

      $(document).once('ajax').ajaxSuccess(function (event, xhr, settings) {
        if (settings.url.indexOf('flag/unflag/favorites') >= 0 || settings.url.indexOf('flag/flag/favorites') >= 0) {
          if (typeof myViews !== 'undefined') {
            $(myViews).trigger('RefreshView');
          }
        }
      });
    }
  };

  function makeAddToCartButton(catClass) {
    return '<a href="#/add-to-cart/' + catClass + '?direct=true" class="js-item-card__cta">Add to Cart</a>';
  }

  function extendCardFooter(itemIndex, element) {
    var $footer = $(element);
    var data = $footer.find('.field--name-field-item-cat-class-code > div');
    if (data.length === 2) {
      var markup = makeAddToCartButton(data[1].textContent);
      $footer.append(markup);
    }
  }

  Drupal.behaviors.favoriteitemcardslide = {
    attach: function (context, settings) {

      $('.block-views-blockitem-cards-favorites').each(function (index) {

        // Add an "Add to Card" button to each card footer
        $(this).find('.view-content .node--type-item .node__footer').map(extendCardFooter);

        var $slick_slider = $(this).find('.view-content');
        var $settings = {
          arrows: false,
          centerMode: false,
          infinite: false,
          mobileFirst: true,
          slidesToShow: 2,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 1023,
              settings: {
                arrows: true,
                slidesToShow: 3
              }
            },
            {
              breakpoint: 1439,
              settings: {
                arrows: true,
                slidesToShow: 4
              }
            }
          ]
        };

        $slick_slider.slick($settings);

      });
    }
  };

})(jQuery, Drupal);

//# sourceMappingURL=../../maps/libraries/block/favorites--item-cards.js.map
