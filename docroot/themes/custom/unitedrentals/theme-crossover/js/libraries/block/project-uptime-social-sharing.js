(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.socialSharing = {
    attach: function (context, settings) {
      $('.block-social-sharing-block', context).once('socialShare').each(function () {
        // Figure out the size of the block
        var contentHeight = $('.block-system-main-block').outerHeight();
        var socialBlockHeight = $('.block-social-sharing-block').height();

        // Get value of the url
        var url = window.location.href;

        // Create a hidden input
        $('.copy-link').after('<input class="jsa-temp-value jsa-temp-value--hidden" type="text" value="Hello World">');

        // Set the value of the input to the url
        $('.jsa-temp-value').val(url);

        // ON click copy the url to clipboard
        $('.copy-link').click(function () {
          $('.jsa-temp-value').focus();
          $('.jsa-temp-value').select();
          document.execCommand('copy');
        });

        // When we scroll,
        // IF scroll position is greater than or equal to the size of the content block
        // Add class when it's at the end of the content block
        // ELSE remove class
        $(window).scroll(function () {
          if ($(window).scrollTop() >= contentHeight) {
            $('.block-social-sharing-block').addClass('jsa-block-social-sharing-block--reset');
            $('.block-social-sharing-block').removeClass('jsa-social-block-fixed');
          }
          else {
            $('.block-social-sharing-block').removeClass('jsa-block-social-sharing-block--reset');
          }
        });

        // IF scroll position is greater than or equal to the height of the social sharing block + the padding between the picture and the title
        // add special class to keep it fixed when scrolling through content
        // ELSE take away the special class so it can reposition to under the content block
        $(window).scroll(function () {
          if ($(window).scrollTop() >= socialBlockHeight && $(window).scrollTop() <= contentHeight) {
            $('.block-social-sharing-block').addClass('jsa-social-block-fixed');
          }
          else {
            $('.block-social-sharing-block').removeClass('jsa-social-block-fixed');
          }
        });
      });

      $('.print-link').once('print').click(function () {
        window.print();
      });
    }
  };

})(jQuery, Drupal);

//# sourceMappingURL=../../maps/libraries/block/project-uptime-social-sharing.js.map
