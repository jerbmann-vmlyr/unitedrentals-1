module.exports = function (casper, scenario, vp) {
  casper.evaluate(function () {
    // Your web-app is now loaded. Edit here to simulate user interacions or other state changes.
    document.querySelector('.mobile-menu__holder').style.cssText = 'position: relative; height: 100%;';
    document.querySelector('.mobile-menu__content').style.cssText = 'overflow: hidden;';
    document.querySelector('html').classList.add('mobile-menu-visible');
    document.querySelector('body').style.cssText = 'overflow: auto;'
    document.querySelector('html').style.cssText = 'overflow: auto;'
  });
  casper.wait(500);
  console.log('onReady.js has run for: ', vp.name);
};
