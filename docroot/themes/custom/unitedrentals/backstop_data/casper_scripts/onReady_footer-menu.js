module.exports = function (casper, scenario, vp) {
  casper.evaluate(function () {
    // Your web-app is now loaded. Edit here to simulate user interacions or other state changes.
    document.querySelector('.footer__primary-navigation .menu__item--below:nth-child(1)').classList.add('is-open');
  });
  // casper.click('.footer__primary-navigation .menu__item--below:nth-child(1)');
  casper.wait(1000);
  console.log('onReady.js has run for: ', vp.name);
};
