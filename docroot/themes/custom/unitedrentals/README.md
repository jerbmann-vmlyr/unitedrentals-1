# United Rentals - Theme notes

_If you're here for installation and theme setup instructions, scroll to the First time setup section near the bottom._

* * *

## Brief overview:
The UR project was designed with the Atomic Design pattern in mind. If you're unaware of what Atomic Design is please review this [link](http://bradfrost.com/blog/post/atomic-web-design/) to familiarize yourself with some of the concepts and jargon.

* * *

Now that you have an idea of what Atomic Design is, you'll want to understand the tools used to help build the theme. Behold [Pattern Lab](http://patternlab.io/). Pattern Lab is a tool that combines all the principles of Atomic Design into a nice little package. You should play around with the demo they have to get an idea of how Pattern Lab works ([demo here](http://demo.patternlab.io/)).

* * *

> #### That's cool and all, but what's Pattern Lab got to do with me?

So what does Pattern Lab have to do with the theme you ask? Well, the idea is that Pattern Lab and our Drupal theme are one in the same. The idea being that you can style everything in Pattern Lab, following all the principles of atomic design, adjust some Drupal templates here, add a couple classes there and voilà you have your site styled.

So how do we marry Pattern Lab and Drupal?

The answer to that is with the Drupal [Emulsify theme](https://www.drupal.org/project/emulsify). Emulsify is the Drupal theme that we are using (renamed unitedrentals), but it includes the Pattern Lab project within it and is responsible for handling the styles within Pattern Lab as well as the Drupal site.

Emulsify lets us bridge the gap from Atomic Design patterns to the realities of the Drupal site.

* * *

## First-time setup
If this is your first time setting up the theme here are the instructions for getting everything set up so that you can start styling.

**Requirements**
[NVM](https://github.com/creationix/nvm)
Node 8.11
NPM 5.6

_**Install**_
Install NVM by following the instructions above.  Then use NVM to install Node and NPM.

### Setup/installing dependancies
In your terminal navigate to `docroot/themes/custom/unitedrentals`
`npm install && (cd vue && npm install)`

### Self signed cert generation and configuration
1. Follow the instructions [here](https://devcenter.heroku.com/articles/ssl-certificate-self) to generate a key and certificate somewhere under your home directory. Note the path for later.
2. Start from the `docroot/themes/custom/unitedrentals` directory.
3. Run `cp vue/build/dev-server-https-certs.example.js vue/build/dev-server-https-certs.js`.
4. Edit vue/build/dev-server-https-certs.js and update the paths from step 1.

### Local Development Process w/ Watcher
Start from the `docroot/themes/custom/unitedrentals` directory.

1. Edit `sites/default/settings.local.php` and make sure this line is included: `$settings['app_dev_server'] = TRUE;` at the bottom.
2. Run `npm ci && npm run theme:build && npm run pl:build` to build the styles.
3. Then go into the Vue directory: `cd vue`.
4. Run `npm ci` to install node modules.
5. Run `npm run dev` to run the local webpack dev server. This will lock your current terminal window.
6. Visit [here](https://localhost:8443/app.js) in a browser and accept security warnings.

Alternatively you can use the theme-build script: `./theme-build` and that will take care of all of that.

### Static Build Process, avoid using for local development
Start from the `docroot/themes/custom/unitedrentals` directory.

1. Edit `sites/default/settings.local.php` and make sure this line is included: `$settings['app_dev_server'] = FALSE;` at the bottom.
2. Run `npm ci && npm run theme:build && npm run pl:build` to build the styles.
3. Then go into the Vue directory: `cd vue`.
4. Run `npm ci && npm run build` to install node modules and build the js files.

### Build & Watch Process: _Theme styles and pattern lab_

This process will build assets on the fly as you make changes. From the same folder you setup everything above.

 `npm run theme:watch`

Your browser should have opened up a page from the local server for Pattern Lab at http://localhost:3000/pattern-lab/public/. This is the Pattern Lab instance that is included with our theme. You can also access Pattern Lab from {http://your-kelex-project.dev}/themes/custom/unitedrentals/pattern-lab/public/ as well.

##### For more detailed instructions about what each npm script does see the NPM Scripts section below

* * *

### NPM Scripts: _Theme styles and pattern lab_

`npm run theme:watch` compiles styles and watches for changes.

`npm run vue:watch` compiles vue and watches for changes.

`npm run theme:build` compiles styles

`npm run vue:build` compiles vue

`npm run pl:build` builds pattern lab

`npm run setup` Downloads all npm dependencies and pattern lab dependencies.

`npm run build:all` Builds the styles, pattern lab, and vue. (Runs `theme:build`, `pl:build` and `vue:build`)

`npm run ci:complete` Runs `setup` and `build:all`

`npm run test:css` Runs css regression tests. Results will be in command line.

`npm run test:report` Opens a pretty html version of the report in your browser.


### NPM Scripts: _Vue.js_

`npm run dev` compiles vue and watches for changes.

`npm run build` compiles vue

`npm run unit` runs vue unit tests.

`npm run e2e` runs vue e2e tests

`npm run test` runs both unit and e2e tests

`npm run unit` runs unit tests

`npm run unit:continuous` runs unit tests on file change

`npm run lint` runs vue components through the eslint or tslint (depending on component lang/type attributes)

`npm run check-versions` verifies version of npm and node

`npm run eslint` runs eslint on components/files

`npm run tslint` runs tslint on components/files

`npm run gulp` used by jenkins (?)

`npm run nvm-switch` used by jenkins (?)

`npm run precommit` used by git to run tests/linters before a commit

`npm run prepush` used by git to run tests/linters before a push

`npm run postinstall` verifies versions of npm/node




## CSS Testing
You'll need firefox (the browser) installed on your machine, so if it isn't go ahead and do that.

Before running tests you'll need to configure the slimerjs maximum firefox version. Running `npm install` resets this 
configuration every time so you have to reset it every time you run `npm install`.
Go to `/<path to project>/unitedrentals/docroot/themes/custom/unitedrentals/node_modules/slimerjs/src/application.ini` 
and set MaxVersion to the version of your firefox browser (+1).  My ff browser is currently at 54.x so I set the 
MaxVersion=55

To run all the css tests
`npm run test:css`

Tests are setup in `backstop.json` at the same level as this readme file. In the setup file each test has a label 
(`global_header` for example). To run the test for only one of the sections. *(notice the second set of `--`)*
`npm run test:css -- --filter=global_header`

To update a specific test's reference image (the screenshot backstop tests against)
`npm run test:reference -- --filter=global_header`

**Do NOT** update test reference without a filter.  Otherwise you'll update all of the acceptance criteria for every 
test and all accountability is lost

You're all set to run front end regression tests!
One thing to note is that backstop compares png images in `<theme>/backstop_data/bitmaps_reference` to png images in
`<theme>/backstop_data/bitmaps_test`.  If nothing exists in bitmap_test then you'll need to have `npm run theme:watch`
running in another terminal window for backstop to capture a test screenshot.


* Currently the location finder pulls branches from branches.json
 