<?php
/**
 * @file
 * Add "render" filter for Pattern Lab.
 *
 * Bring Drupal filters in just so Pattern Lab doesn't bork.
 */

$filter = new Twig_SimpleFilter('render', function($arg) {
  // Check for a numeric zero int or float.
  if ($arg === 0 || $arg === 0.0) {
    return 0;
  }

  // Return early for NULL and empty arrays.
  if ($arg == NULL) {
    return NULL;
  }

  // Optimize for scalars as it is likely they come from the escape filter.
  if (is_scalar($arg)) {
    return $arg;
  }

  if (is_object($arg)) {
    $this->bubbleArgMetadata($arg);
    if ($arg instanceof RenderableInterface) {
      $arg = $arg->toRenderable();
    }
    elseif (method_exists($arg, '__toString')) {
      return (string) $arg;
    }
    // You can't throw exceptions in the magic PHP __toString() methods, see
    // http://php.net/manual/language.oop5.magic.php#object.tostring so
    // we also support a toString method.
    elseif (method_exists($arg, 'toString')) {
      return $arg->toString();
    }
    else {
      throw new \Exception('Object of type ' . get_class($arg) . ' cannot be printed.');
    }
  }

  // This is a render array, with special simple cases already handled.
  // Early return if this element was pre-rendered (no need to re-render).
  if (isset($arg['#printed']) && $arg['#printed'] == TRUE && isset($arg['#markup']) && strlen($arg['#markup']) > 0) {
    return $arg['#markup'];
  }
  $arg['#printed'] = FALSE;
  return $this->renderer->render($arg);
});
