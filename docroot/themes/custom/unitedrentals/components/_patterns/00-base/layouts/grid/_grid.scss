$_columns-default: 12;
$_gutter-large: 16px;
$_gutter-small: 8px;
$_padding-large: 5vw;
$_padding-small: 3vw;

/// Mixin - Grid:
/// Defines the container grid styles.
/// Add to the container that holds the grid columns.
/// @param {string} $nested [grid] - Class name of nested grid if there is one.
/// @group Grid
/// @example scss
///   .grid {
///     @include grid(class-name);
///   }
@mixin grid($nested: grid) {
  width: 100%;
  display: flex;
  padding: 0 $_padding-small;

  @include breakpoint($break-medium) {
    padding: 0 $_padding-large;
  }

  .#{$nested} {
    padding: 0;
  }
}

/// Mixin - Column:
/// Defines the grid column styles.
/// Add to each of the columns within a grid.
/// @group Grid
/// @example scss
///   .column {
///     @include column;
///   }
@mixin column {
  padding: 0 $_gutter-small/2;
  flex: 1 0 auto; // flex-basis auto works for every browser except IE
  max-width: 100%; // needed for Edge

  &--full-width {
    flex-basis: 100%;
    padding: 0 $_gutter-small/2;
  }

  &:first-child {
    padding-left: 0;
  }

  &:last-child {
    padding-right: 0;
  }

  @include breakpoint($break-small) {
    padding: 0 $_gutter-large/2;
  }

  /** IE10+ CSS styles go here
   *
   * IE hack, only IE recognizes this media query
   * Necessary because IE handles flex-basis differently
   */
  @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
    // flex-basis 0% is required for IE. IE requires a unit in flex-basis.
    flex: 1 0 0%;
  }
}

/// Mixin - Set Column:
/// Defines the width of each of the columns
/// @param {number} $span - Number of columns to use
/// @param {number} $total [$_columns-default] - Total number of columns
/// @group Grid
/// @example scss
///   .group-left {
///     @include setColumn(4, 12);
///   }
///   .group-right {
///     @include setColumn(8, 12);
///   }
@mixin setColumn($span, $total: $_columns-default) {
  flex-basis: _calcWidth($span, $total);
  max-width: _calcWidth($span, $total);
}

/// Function - _calcWidth
/// Calculates the width of the column
/// @param {number} $span - Number of columns to use
/// @param {number} $total - Total number of columns
/// @group Grid
/// @example scss
///   max-width: _calcWidth($span, $total);
@function _calcWidth($span, $total) {
  $multiplier: 100 / $total;
  @return ($span * $multiplier) * 1%;
}

.grid {
  @include grid;

  &.no-padding {
    padding: 0;
  }

  &.collapse .column {
    padding: 0 !important;
  }

  &.row-wrap {
    flex-flow: row wrap;
  }

  &.columns {
    flex-flow: column;
  }

  .grow-1 {
    flex-grow: 1;
  }
}

.column {
  @include column;
}

.column--quarter {
  @include column;

  @include breakpoint($break-small) {
    @include setColumn(1, 4);
  }
}

// sets a 50% wide column
.column--half {
  @include setColumn(1, 2);
}

.column--third {
  @include setColumn(1, 3);
}

.column--two-thirds {
  @include setColumn(2, 3);
}

.column--three-quarters {
  @include column;

  @include breakpoint($break-small) {
    @include setColumn(3, 4);
  }
}

.row {
  display: flex;
  flex-wrap: wrap;
  width: 100%;
}

// media query mixin
@mixin break($size) {
  @media (min-width: map-get($ur-breakpoints, $size)) {
    @content;
  }
}

// loop over the breakpoints
@each $key, $value in $ur-breakpoints {
  @for $i from 1 through $_columns-default {
    .col#{$key}-#{$i} {
      flex: 0 0 100%;
      @include break($key) {
        flex: 0 0 #{$i / $_columns-default * 100%};
      }
    }
  }
}

@mixin create-mq($breakpoint, $flag) {
  @if ($breakpoint == 0) {
    @content;
  } @else {
    @media screen and (#{$flag}-width: $breakpoint) {
      @content;
    }
  }
}

@mixin create-hide-classes($modifier, $grid__cols, $breakpoint) {
  @include create-mq($breakpoint, "max") {
    @for $i from 1 through $grid__cols {
      &.hide#{$modifier} {
        display: none;
      }
    }
  }
}

@each $modifier, $breakpoint in $ur-breakpoints {
  @include create-hide-classes($modifier, $_columns-default, $breakpoint);
}

.f-row {
  margin-left: 10px;
  margin-right: 10px;

  @include breakpoint($_break-small) {
    margin-left: 24px;
    margin-right: 24px;
  }

  @include breakpoint($_break-medium) {
    margin-left: 56px;
    margin-right: 56px;
  }

  @include breakpoint($_break-large) {
    margin-left: auto;
    margin-right: auto;
  }
}

.f-row--no-margin {
  margin: 0;
  max-width: none;

  @include breakpoint($_break_large) {
    margin: auto;
    max-width: 1440px;
  }
}

.column-padding {
  padding-left: 10px;
  padding-right: 10px;

  @include breakpoint($_break-small) {
    padding-left: 24px;
    padding-right: 24px;
  }

  @include breakpoint($_break-medium) {
    padding-left: 56px;
    padding-right: 56px;
  }

  @include breakpoint($_break-large) {
    padding-left: 72px;
    padding-right: 72px;
  }
}

.column-padding-left {
  padding-left: 10px;

  @include breakpoint($_break-small) {
    padding-left: 24px;
  }

  @include breakpoint($_break-medium) {
    padding-left: 56px;
  }

  @include breakpoint($_break-large) {
    padding-left: 72px;
  }
}

.column-padding-right {
  padding-right: 10px;

  @include breakpoint($_break-small) {
    padding-right: 24px;
  }

  @include breakpoint($_break-medium) {
    padding-right: 56px;
  }

  @include breakpoint($_break-large) {
    padding-right: 72px;
  }
}

.small-column-padding-left {
  @media (max-width: $_break-small) {
    padding-left: 10px;
  }
}

.column-padding-hero-header-no {
  padding-left: 0;
  padding-top: 30px;
  height: 0;
  flex-basis: 0;
}

.column-padding-hero-input {


  @include breakpoint(max-width $break-xsmall - 1) {

  }

  @include breakpoint($break-small) {
    margin-right: 50px;
  }

  @include breakpoint($break-medium) {
    max-width: 100%;
  }

}


.row-margin-y {
  margin-top: 16px;
  margin-bottom: 16px;
}
