---
title: Colors
---
Colors below are automatically pulled from `_01-variables.scss` and can be used like so:

```scss
.class {
  color: $blue;
}
```
