document.addEventListener('DOMContentLoaded', function (e) {
  'use strict';

  var elementHtml = document.querySelector('html');

  /*
   All version of IE do not support object-fit so the test
   below is necessary for styling fallbacks.
   */
  if ('objectFit' in document.documentElement.style === false) {
    elementHtml.classList.add('no-object-fit');
  }

  /**
   * Detects if browser is IE11 or Edge
   * If IE11 or Edge apply style 'overflow-x: hidden' to body HTML element
   */
  var ua = window.navigator.userAgent;
  if (ua.indexOf('Trident') || ua.indexOf('Edge')) {
    document.getElementsByTagName('body')[0].classList.add('no-scroll-x');
  }

  var mobileMenuOpen = document.querySelector('header .mobile-menu__open');
  if (mobileMenuOpen) {
    mobileMenuOpen.addEventListener('click', function (e) {
      e.preventDefault();
      elementHtml.classList.add('mobile-menu-visible');
    });
  }

  var mobileMenuClose = document.querySelectorAll('.mobile-menu__close,.menu__link#account-selector');

  if (mobileMenuClose) {
    // Cannot use .forEach because IE11 doesn't support it.
    for (var i = 0; i < mobileMenuClose.length; i++) {
      var el = mobileMenuClose[i];
      el.addEventListener('click', function (e) {
        e.preventDefault();
        elementHtml.classList.remove('mobile-menu-visible');
      });
    }
  }

  var mobileMenuCover = document.querySelector('.mobile-menu__cover');

  if (mobileMenuCover) {
    mobileMenuCover.addEventListener('click', function (e) {
      e.preventDefault();
      elementHtml.classList.remove('mobile-menu-visible');
    });
  }

  var headerDropdown = document.querySelectorAll('header .header__specialty-navigation .menu__item--below > .menu__link');

  if (headerDropdown) {
    for (var i = 0; i < headerDropdown.length; i++) {
      headerDropdown[i].addEventListener('click', function (e) {
        e.preventDefault();
        closeHeaderMenus(this.parentNode);
        this.parentNode.classList.toggle('show-dropdown');
      });
    }
  }

  function closeHeaderMenus(clickedMenuParent) {
    var openMenus = document.querySelectorAll('header .menu__item.show-dropdown');
    for (var i = 0; i< openMenus.length; i++) {
      if (openMenus[i] !== clickedMenuParent) {
        openMenus[i].classList.remove('show-dropdown');
      }
    }
  }

  var secondaryDropdown = document.querySelectorAll('header .header__secondary-navigation .menu__item--is-section-label > .menu__link');

  if (secondaryDropdown) {
    for (var i = 0; i < secondaryDropdown.length; i++) {
      secondaryDropdown[i].addEventListener('click', function(e) {
        e.preventDefault();
        closeSecondaryMenus(this.parentNode);
        this.parentNode.classList.toggle('show-dropdown');
      });
    }
  }

  function closeSecondaryMenus(clickedMenuParent) {
    var openMenus = document.querySelectorAll('header .menu__item.show-dropdown');
    for (var i = 0; i< openMenus.length; i++) {
      if (openMenus[i] !== clickedMenuParent) {
        openMenus[i].classList.remove('show-dropdown');
      }
    }
  }

  // Mobile "Other Platforms" section
  var mobileOtherPlatforms = document.querySelectorAll('.mobile-menu__holder .menu__item--is-section-label');

  if (mobileOtherPlatforms) {
    for (var i = 0; i < mobileOtherPlatforms.length; i++) {
      mobileOtherPlatforms[i].addEventListener('click', function(e) {
        e.preventDefault();
        this.classList.toggle('show-dropdown');
      });
    }
  }

  var mobileOtherPlatformLinks = document.querySelectorAll('.mobile-menu__holder .menu__item--is-section-label > ul > li > a')

  if (mobileOtherPlatformLinks) {
    for (var i = 0; i < mobileOtherPlatformLinks.length; i++) {
      mobileOtherPlatformLinks[i].addEventListener('click', function(e) {
        e.stopPropagation();
      });
    }
  }

  var searchOpen = document.querySelector('header .icon-search');

  if (searchOpen) {
    searchOpen.addEventListener('click', function (e) {
      e.preventDefault();
      elementHtml.classList.add('global-search-visible');
      document.querySelector('.global-search__search-box > input').focus();
    });
  }

  var searchClose = document.querySelector('.global-search__close');

  if (searchClose) {
    searchClose.addEventListener('click', function (e) {
      e.preventDefault();
      elementHtml.classList.remove('global-search-visible');

      // Unfortunately we went VanillaJS here instead of Vue.
      // And we need to do some Vue State work...
      // Closing this Location Search widget when you either don't have a Location or Cleared Your Last Location
      // must trigger a reset of the Rates Place Branch.  This is for the MELP (Mktpl Eqp Listing Pg)
      var ratesPlace = _app.$store.state.user.ratesPlace;
      var noRatesPlace = Object.keys(ratesPlace).length === 0;
      if (noRatesPlace) {
        _app.$store.commit('user/clearRatesPlaceBranch');
      }
    });
  }

  function removeExpandedSelector(targetItemSelector, targetItemExpandedSelector) {
    var targetItems = document.querySelectorAll(targetItemSelector);
    if (targetItems) {
      for (i = 0; i < targetItems.length; i++) {
        targetItems[i].classList.remove(targetItemExpandedSelector);
      }
    }
  }

  function accordionify(targetItemSelector, targetItemExpandedSelector) {
    var targetItems = document.querySelectorAll(targetItemSelector);
    if (targetItems) {
      for (var j = 0; j < targetItems.length; j++) {
        targetItems[j].addEventListener('click', function (e) {
          if (!this.classList.contains(targetItemExpandedSelector)) {
            removeExpandedSelector(targetItemSelector, targetItemExpandedSelector);
            this.classList.toggle(targetItemExpandedSelector);
          }
          else {
            this.classList.remove(targetItemExpandedSelector);
          }
        });
      }
    }
  }

  accordionify('footer .footer__primary-navigation .menu__item--below', 'is-open');
  accordionify('.faq-row', 'is-open');

  // Set the default left sidebar parent to be open by default
  var leftRailItems = document.querySelectorAll('.nested-navigation .menu__item--below, .nested-navigation .menu__item a.is-active');

  if (leftRailItems) {
    for (i = 0; i < leftRailItems.length; i++) {
      if (leftRailItems[i].classList.contains('menu__item--active')) {
        leftRailItems[i].classList.add('is-open');
      }
    }

    for (i = 0; i < leftRailItems.length; i++) {
      if (leftRailItems[i].classList.contains('is-active')) {
        leftRailItems[i].parentNode.classList.add('is-open');
      }
    }
  }

  // Toggle opening/closing left rail categories, need to target the <a> tag here
  // since we still want the sub links to direct a user to the correct place and not
  var leftRailLinks = document.querySelectorAll('.nested-navigation .menu__item--below > a');

  if (leftRailLinks) {
    for (var i = 0; i < leftRailLinks.length; i++) {
      leftRailLinks[i].addEventListener('click', function (e) {
        e.preventDefault();
        this.parentNode.classList.toggle('is-open');
      });
    }
  }

  var messageClose = document.querySelectorAll('.message .icon-close-circle');

  function findAncestor(el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls)) {
      ; // eslint-disable-line no-extra-semi
    }
    return el;
  }

  if (messageClose) {
    for (i = 0; i < messageClose.length; i++) {
      messageClose[i].addEventListener('click', function (e) {
        var m = findAncestor(this, 'message');
        m.parentNode.removeChild(m);
      });
    }
  }

  var nestedNavigationItems = document.querySelectorAll('.nested-navigation__active-title');

  if (nestedNavigationItems) {
    for (var i = 0; i < nestedNavigationItems.length; i++) {
      nestedNavigationItems[i].addEventListener('click', function (e) {
        var container = findAncestor(this, 'nested-navigation__active-title-container');
        container.parentElement.classList.toggle('is-open');
      });
    }
  }

  var mobileNestedNavigation = document.querySelectorAll('.mobile-nested-navigation .nested-navigation .menu__item--below a.is-active');

  if (mobileNestedNavigation) {
    for (var i = 0; i < mobileNestedNavigation.length; i++) {
      var parent = findAncestor(mobileNestedNavigation[i], 'menu__item--below');
      parent.classList.toggle('is-open');
    }
  }

  var categoryMenuOpen = document.querySelector('.category-menu__title');

  if (categoryMenuOpen) {
    categoryMenuOpen.addEventListener('click', function (e) {
      e.preventDefault();
      elementHtml.classList.add('category-menu-visible');
    });
  }

  var categoryMenuClose = document.querySelector('.category-menu__bar--first');

  if (categoryMenuClose) {
    categoryMenuClose.addEventListener('click', function (e) {
      e.preventDefault();
      elementHtml.classList.remove('category-menu-visible');
    });
  }

  // parent link in mobile should link to parent category
  var categoryMenuDetails = document.querySelectorAll('.category-menu__menu .menu > li');
  var parentCategoryMenuDetails = document.querySelectorAll('.category-menu__menu .menu > li > a');

  if (parentCategoryMenuDetails) {
    for (var i = 0; i < parentCategoryMenuDetails.length; i++) {
      parentCategoryMenuDetails[i].addEventListener('click', function (e) {
        e.stopImmediatePropagation();
      });
    }
  }

  if (categoryMenuDetails) {
    for (var i = 0; i < categoryMenuDetails.length; i++) {
      categoryMenuDetails[i].addEventListener('click', function (e) {
        // e.preventDefault();
        this.classList.add('category-menu-details-active');
        elementHtml.classList.add('category-menu-details-visible');
      });
    }
  }

  var categoryMenuDetailsClose = document.querySelector('.category-menu__bar--second');

  if (categoryMenuDetailsClose) {
    categoryMenuDetailsClose.addEventListener('click', function (e) {
      // e.preventDefault();
      elementHtml.classList.remove('category-menu-details-visible');

      document.querySelector('.category-menu-details-active').classList.remove('category-menu-details-active');
    });
  }

  var elementSocialHolder = document.querySelector('.project-uptime-article__social-holder');
  var isVisible;

  function isAnyPartOfElementInViewport(el) {
    var rect = el.getBoundingClientRect();
    var windowHeight = window.innerHeight || document.documentElement.clientHeight;
    var windowWidth = window.innerWidth || document.documentElement.clientWidth;
    var vertInView = rect.top <= windowHeight && rect.top + rect.height >= 0;
    var horInView = rect.left <= windowWidth && rect.left + rect.width >= 0;

    return vertInView && horInView;
  }

  var eventListenerHandler = function handler() {
    if (elementSocialHolder) {
      if (isAnyPartOfElementInViewport(elementSocialHolder)) {
        if (!isVisible) {
          elementHtml.classList.add('social-in-viewport');
          isVisible = true;
        }
      } else {
        if (isVisible) {
          elementHtml.classList.remove('social-in-viewport');
          isVisible = false;
        }
      }
    }
  };

  addEventListener('DOMContentLoaded', eventListenerHandler, false);
  addEventListener('load', eventListenerHandler, false);
  addEventListener('scroll', eventListenerHandler, false);
  addEventListener('resize', eventListenerHandler, false);


  // Set 'document' to 'context' if Drupal
  var accordionItem = document.querySelectorAll('.accordion__list-dl .dl-term');
  var accordionDef = document.querySelectorAll('.accordion__list-dl .dl-definition');
  var accordionItemRoot = document.querySelectorAll('.accordion__list-dl');

  // If javascript, hide accordion definition on load
  function jsCheck() {
    for (var i = 0; i < accordionDef.length; i++) {
      if (accordionDef[i].classList) {
        accordionDef[i].classList.add('active');
      }
      else {
        accordionDef[i].className += ' active';
      }
    }
  }

  jsCheck();

  // Accordion Toggle
  // Mobile Click Menu Transition
  for (var i = 0; i < accordionItem.length; i++) {
    accordionItem[i].addEventListener('click', function (e) {
      var className = 'is-active';
      // Add is-active class
      if (this.classList) {
        this.classList.toggle(className);
      }
      else {
        var classes = this.className.split(' ');
        var existingIndex = classes.indexOf(className);

        if (existingIndex >= 0) {
          classes.splice(existingIndex, 1);
        }
        else {
          classes.push(className);
        }
        this.className = classes.join(' ');
      }
      e.preventDefault();
    });
  }

  // Hide accordion sub rows if empty
  for (var i = 0; i < accordionItemRoot.length; i++) {
    if (accordionItemRoot[i].getElementsByClassName('text-long').length === 0) {
      accordionItemRoot[i].style.display = 'none';
    }
  }

  var leftmenuEquipmentLinks = document.getElementById("block-hierarchicaltaxonomymenu-categories");
  if (leftmenuEquipmentLinks) {
    var menuElements = leftmenuEquipmentLinks.getElementsByClassName('menu-item--expanded');
    for (var i = 0; i < menuElements.length; i++) {
      menuElements[i].addEventListener('click', function (e) {
        document.getElementById('block-hierarchicaltaxonomymenu-categories').classList.add('hideCatMenuOnClick');
      });
    }
  }

  // Remove empty token string from global search page
  var searchToken = document.getElementById('search-token-term')
  if (searchToken) {
    if ((searchToken.innerHTML || '').replace(/^\s+/,'')  === '[current-page:query:search]') {
      searchToken.innerHTML = '';
    }
  }

  // Mobile menu user section
  var mobileMenuUser = document.querySelector('.user-logged-in .mobile-menu__holder #login');
  if (mobileMenuUser) {

    mobileMenuUser.addEventListener('click', function(e){
      e.preventDefault();
      document.querySelector('.mobile-menu__holder').classList.toggle('show-mobile-user-options');
    });
  }

  // Microsite Smooth scrolling functionality. Desktop version.
  var anchorlinks = document.querySelectorAll(
    '#block-unitedrentals-main-menu a[href*="#"]'
  );
  for (var l = 0; l < anchorlinks.length; l++) {
    anchorlinks[l].addEventListener('click', function(event) {
      var targetElement = event.target || event.srcElement;
      var hashval = targetElement.hash;
      var target = document.querySelector(hashval);
      if (target) {
        target.scrollIntoView({
          behavior: 'smooth',
          block: 'start'
        });
        history.pushState(null, null, hashval);
        event.preventDefault();
      }
    })
  }

  // Microsite Smooth scrolling functionality. Mobile version.
  var anchorlinks = document.querySelectorAll(
    '#block-unitedrentals-mobile-menu a[href*="#"]'
  );
  for (var l = 0; l < anchorlinks.length; l++) {
    anchorlinks[l].addEventListener('click', function(event) {
      var targetElement = event.target || event.srcElement;
      var hashval = targetElement.hash;
      var target = document.querySelector(hashval);

      if (target) {

        // We're triggering inside mobile menu.
        elementHtml.classList.remove('mobile-menu-visible');

        setTimeout(function () {
          target.scrollIntoView({
            behavior: 'smooth',
            block: 'start'
          });
          history.pushState(null, null, hashval);
        }, 500);

        event.preventDefault();
      }
    })
  }

  setTimeout(function() {
    if (location.hash) {
      /* we need to scroll to the top of the window first,
       because the browser will always jump to the anchor
       first before JavaScript is ready, thanks Stack Overflow:
       http://stackoverflow.com/a/3659116
       */
      window.scrollTo(0, 0);
      var hashval = location.hash.split('#');
      hashval = hashval[1].replace(/\//g, '');
      if (hashval) {
        try {
          var target = document.querySelector('#' + hashval);
          if (target) {
            target.scrollIntoView({
              behavior: 'smooth',
              block: 'start'
            });
          }
        }
        catch (e) {
          // No-op. Vue.js will generate hashval's that we should not try to scroll into view
        }
      }
    }
  }, 1);

  // Tab selector defaults.
  var firstTab = document.querySelector('.faq-tabs .faq-tab-button');
  if (firstTab) {
    firstTab.classList.add('active');
    var firstTabSection = document.querySelector(firstTab.getAttribute('data-tab'));
    if (firstTabSection) {
      firstTabSection.classList.add('selected');
    }
  }

  // FAQ Tab functionality. Dropdown.
  var faqSelect = document.querySelector('select.faq-select');
  if (faqSelect) {
    faqSelect.addEventListener('change', function(event){
      var targetElement = event.target || event.srcElement;
      // Hide all tabs.
      var allTabs = document.querySelectorAll('.faq-tab');
      if (allTabs) {
        for (var i = 0; i < allTabs.length; i++) {
          allTabs[i].classList.remove('selected');
        }
        document.querySelector('#' + targetElement.value).classList.add('selected');
      }
    });
  }

  // FAQ Tab functionality. Tabs.
  var faqTabs = document.querySelectorAll('.faq-tabs .faq-tab-button');
  if (faqTabs) {
    for (var i = 0; i < faqTabs.length; i++) {
      faqTabs[i].addEventListener('click', function (event) {
        var targetElement = event.target || event.srcElement;
        // Hide all 'active' tab buttons.
        var allTabButtons = document.querySelectorAll('.faq-tabs .faq-tab-button');
        for (var j = 0; j < allTabButtons.length; j++) {
          allTabButtons[j].classList.remove('active');
        }
        targetElement.classList.add('active');
        // Hide all tabs.
        var allTabs = document.querySelectorAll('.faq-tab');
        for (var k = 0; k < allTabs.length; k++) {
          allTabs[k].classList.remove('selected');
        }
        document.querySelector(targetElement.getAttribute('data-tab')).classList.add('selected');
        event.preventDefault();
      });
    }
  }

  // Category Tab selector defaults.
  var firstTab = document.querySelector('.faq-cat-tabs .faq-tab-button');
  if (firstTab) {
    firstTab.classList.add('active');
    var faqlist = document.querySelectorAll('[data-category="' + firstTab.getAttribute('data-tab') + '"]');
    if (faqlist) {
      for (var p = 0; p < faqlist.length; p++) {
        faqlist[p].parentNode.classList.add('show');
      }
    }
  }

  // Category - FAQ Tab functionality. Dropdown.
  var faqSelect = document.querySelector('select.faq-cat-select');
  if (faqSelect) {
    faqSelect.addEventListener('change', function(event){
      var targetElement = event.target || event.srcElement;
      // Hide all FAQs.
      var allTabs = document.querySelectorAll('.faq-large__list .faq-row');
      for (var k = 0; k < allTabs.length; k++) {
        allTabs[k].classList.remove('is-open');
        allTabs[k].classList.remove('show');
      }
      // Show selected FAQs.
      var faqlist = document.querySelectorAll('[data-category="' + targetElement.value + '"]');
      if (faqlist) {
        for (var p = 0; p < faqlist.length; p++) {
          faqlist[p].parentNode.classList.add('show');
        }
      }
    });
  }

  // Category - FAQ Tab functionality. Tabs.
  var faqTabs = document.querySelectorAll('.faq-cat-tabs .faq-tab-button');
  if (faqTabs) {
    for (var i = 0; i < faqTabs.length; i++) {
      faqTabs[i].addEventListener('click', function (event) {
        var targetElement = event.target || event.srcElement;
        // Hide all 'active' tab buttons.
        var allTabButtons = document.querySelectorAll('.faq-cat-tabs .faq-tab-button');
        for (var j = 0; j < allTabButtons.length; j++) {
          allTabButtons[j].classList.remove('active');
        }
        targetElement.classList.add('active');
        // Hide all FAQs.
        var allTabs = document.querySelectorAll('.faq-large__list .faq-row');
        for (var k = 0; k < allTabs.length; k++) {
          allTabs[k].classList.remove('is-open');
          allTabs[k].classList.remove('show');
        }
        // Show selected FAQs.
        var faqlist = document.querySelectorAll('[data-category="' + targetElement.getAttribute('data-tab') + '"]');
        if (faqlist) {
          for (var p = 0; p < faqlist.length; p++) {
            faqlist[p].parentNode.classList.add('show');
          }
        }
        event.preventDefault();
      });
    }
  }

  // Microsite Smooth scrolling functionality. Desktop version.
  var anchorlinks = document.querySelectorAll(
    '#block-unitedrentals-main-menu a[href*="#"]'
  );
  for (var l = 0; l < anchorlinks.length; l++) {
    anchorlinks[l].addEventListener('click', function(event) {
      var targetElement = event.target || event.srcElement;
      var hashval = targetElement.hash;
      // Match only on valid HTML id selector.
      if (hashval.match('^#[a-zA-Z].[a-zA-Z0-9\\-\\:\\.\\_]*$')) {
        var target = document.querySelector(hashval);
        if (target) {
          target.scrollIntoView({
            behavior: 'smooth',
            block: 'start'
          });
          history.pushState(null, null, hashval);
          event.preventDefault();
        }
      }
    })
  }

  // Microsite Smooth scrolling functionality. Mobile version.
  var anchorlinks = document.querySelectorAll(
    '#block-unitedrentals-mobile-menu a[href*="#"]'
  );
  for (var l = 0; l < anchorlinks.length; l++) {
    anchorlinks[l].addEventListener('click', function(event) {
      var targetElement = event.target || event.srcElement;
      var hashval = targetElement.hash;
      // Match only on valid HTML id selector.
      if (hashval.match('^#[a-zA-Z].[a-zA-Z0-9\\-\\:\\.\\_]*$')) {
        var target = document.querySelector(hashval);
        if (target) {
          // We're triggering inside mobile menu.
          elementHtml.classList.remove('mobile-menu-visible');

          setTimeout(function () {
            target.scrollIntoView({
              behavior: 'smooth',
              block: 'start'
            });
            history.pushState(null, null, hashval);
          }, 500);

          event.preventDefault();
        }
      }
    })
  }

  setTimeout(function() {
    if (location.hash) {
      /* we need to scroll to the top of the window first,
       because the browser will always jump to the anchor
       first before JavaScript is ready, thanks Stack Overflow:
       http://stackoverflow.com/a/3659116
       */
      window.scrollTo(0, 0);
      var hashval = location.hash.split('#');
      hashval = hashval[1].replace(/\W/g, '');
      if (hashval) {
        var target = document.querySelector('#' + hashval);
        if (target) {
          target.scrollIntoView({
            behavior: 'smooth',
            block: 'start'
          });
        }
      }
    }
  }, 1);

  // Tab selector defaults.
  var firstTab = document.querySelector('.faq-tabs .faq-tab-button');
  if (firstTab) {
    firstTab.classList.add('active');
    var firstTabSection = document.querySelector(firstTab.getAttribute('data-tab'));
    if (firstTabSection) {
      firstTabSection.classList.add('selected');
    }
  }

  // FAQ Tab functionality. Dropdown.
  var faqSelect = document.querySelector('select.faq-select');
  if (faqSelect) {
    faqSelect.addEventListener('change', function(event){
      var targetElement = event.target || event.srcElement;
      // Hide all tabs.
      var allTabs = document.querySelectorAll('.faq-tab');
      if (allTabs) {
        for (var i = 0; i < allTabs.length; i++) {
          allTabs[i].classList.remove('selected');
        }
        document.querySelector('#' + targetElement.value).classList.add('selected');
      }
    });
  }

  // FAQ Tab functionality. Tabs.
  var faqTabs = document.querySelectorAll('.faq-tabs .faq-tab-button');
  if (faqTabs) {
    for (var i = 0; i < faqTabs.length; i++) {
      faqTabs[i].addEventListener('click', function (event) {
        var targetElement = event.target || event.srcElement;
        // Hide all 'active' tab buttons.
        var allTabButtons = document.querySelectorAll('.faq-tabs .faq-tab-button');
        for (var j = 0; j < allTabButtons.length; j++) {
          allTabButtons[j].classList.remove('active');
        }
        targetElement.classList.add('active');
        // Hide all tabs.
        var allTabs = document.querySelectorAll('.faq-tab');
        for (var k = 0; k < allTabs.length; k++) {
          allTabs[k].classList.remove('selected');
        }
        document.querySelector(targetElement.getAttribute('data-tab')).classList.add('selected');
        event.preventDefault();
      });
    }
  }

  // Category Tab selector defaults.
  var firstTab = document.querySelector('.faq-cat-tabs .faq-tab-button');
  if (firstTab) {
    firstTab.classList.add('active');
    var faqlist = document.querySelectorAll('[data-category="' + firstTab.getAttribute('data-tab') + '"]');
    if (faqlist) {
      for (var p = 0; p < faqlist.length; p++) {
        faqlist[p].parentNode.classList.add('show');
      }
    }
  }

  // Category - FAQ Tab functionality. Dropdown.
  var faqSelect = document.querySelector('select.faq-cat-select');
  if (faqSelect) {
    faqSelect.addEventListener('change', function(event){
      var targetElement = event.target || event.srcElement;
      // Hide all FAQs.
      var allTabs = document.querySelectorAll('.faq-large__list .faq-row');
      for (var k = 0; k < allTabs.length; k++) {
        allTabs[k].classList.remove('is-open');
        allTabs[k].classList.remove('show');
      }
      // Show selected FAQs.
      var faqlist = document.querySelectorAll('[data-category="' + targetElement.value + '"]');
      if (faqlist) {
        for (var p = 0; p < faqlist.length; p++) {
          faqlist[p].parentNode.classList.add('show');
        }
      }
    });
  }

  // Category - FAQ Tab functionality. Tabs.
  var faqTabs = document.querySelectorAll('.faq-cat-tabs .faq-tab-button');
  if (faqTabs) {
    for (var i = 0; i < faqTabs.length; i++) {
      faqTabs[i].addEventListener('click', function (event) {
        var targetElement = event.target || event.srcElement;
        // Hide all 'active' tab buttons.
        var allTabButtons = document.querySelectorAll('.faq-cat-tabs .faq-tab-button');
        for (var j = 0; j < allTabButtons.length; j++) {
          allTabButtons[j].classList.remove('active');
        }
        targetElement.classList.add('active');
        // Hide all FAQs.
        var allTabs = document.querySelectorAll('.faq-large__list .faq-row');
        for (var k = 0; k < allTabs.length; k++) {
          allTabs[k].classList.remove('is-open');
          allTabs[k].classList.remove('show');
        }
        // Show selected FAQs.
        var faqlist = document.querySelectorAll('[data-category="' + targetElement.getAttribute('data-tab') + '"]');
        if (faqlist) {
          for (var p = 0; p < faqlist.length; p++) {
            faqlist[p].parentNode.classList.add('show');
          }
        }
        event.preventDefault();
      });
    }
  }
  if (!document.querySelector('.header__secondary-navigation').firstElementChild){
    document.querySelector('.header__secondary-navigation').style.display = 'none'
  }

  if(
    drupalSettings &&
    drupalSettings.ur &&
    drupalSettings.ur.primaryRole &&
    drupalSettings.ur.primaryRole.length > 0
  ) {
    if(window.dataLayer) {
      dataLayer.push({
        'event': 'defineUser',
        'userRole': drupalSettings.ur.primaryRole,
      });
    }
  }

}, false);
