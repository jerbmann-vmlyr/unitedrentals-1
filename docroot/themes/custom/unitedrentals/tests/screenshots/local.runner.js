#!/usr/bin/env node

var Nightwatch = require('nightwatch');
var browserstack = require('browserstack-local');
var bs_local;
var api_key = require(process.env.PWD + '/browserstack-key');

try {

  if (!api_key.key) {
    console.log('You need to set your api key in the browserstack-keys.json file'); // eslint-disable-line no-console
    throw 'No api key';
  }

  if (!process.env.TEST_NAME) {
    console.log('You need to set the test using the environment variable "TEST_NAME".'); // eslint-disable-line no-console
    throw 'No test name';
  }

  process.env.TIMESTAMP = Date.now();
  process.env.BROWSERSTACK_ACCESS_KEY = api_key.key;
  process.env.BROWSERSTACK_USERNAME = api_key.user;

  process.mainModule.filename = './node_modules/nightwatch/bin/nightwatch';
  // Code to start browserstack local before start of test
  console.log('Connecting local'); // eslint-disable-line no-console
  Nightwatch.bs_local = bs_local = new browserstack.Local();
  bs_local.start({key: process.env.BROWSERSTACK_ACCESS_KEY}, function (error) {
    'use strict';

    if (error) {
      throw error;
    }

    console.log('Connected. Now testing...'); // eslint-disable-line no-console
    Nightwatch.cli(function (argv) {
      Nightwatch.CliRunner(argv)
        .setup(null, function () {
          // Code to stop browserstack local after end of parallel test
          bs_local.stop(function () {});
        })
        .runTests(function () {
          // Code to stop browserstack local after end of single test
          bs_local.stop(function () {});
        });
    });
  });
}
catch (ex) {
  console.log('There was an error while starting the test runner:\n\n'); // eslint-disable-line no-console
  process.stderr.write(ex.stack + '\n');
  process.exit(2); // eslint-disable-line no-process-exit
}
