var currTest = process.env.TEST_NAME;
var currTestPath = 'tests/screenshots/specs/' + currTest;

var nightwatch_config = {
  src_folders: [currTestPath],

  selenium: {
    start_process: false,
    host: 'hub-cloud.browserstack.com',
    port: 80
  },

  common_capabilities: {
    'build': 'nightwatch-browserstack',
    'browserstack.user': process.env.BROWSERSTACK_USERNAME || 'BROWSERSTACK_USERNAME',
    'browserstack.key': process.env.BROWSERSTACK_ACCESS_KEY || 'BROWSERSTACK_ACCESS_KEY',
    'browserstack.debug': true,
    'browserstack.local': true,
    'globals': {
      timestamp: process.env.TIMESTAMP,
      current_test: currTest
    }
  },

  test_settings: {
    default: {},
    edge: {
      desiredCapabilities: {
        os: 'Windows',
        os_version: '10',
        browser: 'Edge',
        browser_version: '14.0',
        resolution: '1600x1200'
      }
    },
    ie11: {
      desiredCapabilities: {
        os: 'Windows',
        os_version: '10',
        browser: 'IE',
        browser_version: '11.0',
        resolution: '1600x1200'
      }
    },
    chrome: {
      desiredCapabilities: {
        os: 'Windows',
        os_version: '10',
        browser: 'Chrome',
        browser_version: '58.0',
        resolution: '1600x1200'
      }
    },
    firefox: {
      desiredCapabilities: {
        os: 'Windows',
        os_version: '10',
        browser: 'Firefox',
        browser_version: '53.0',
        resolution: '1600x1200'
      }
    },
    safari: {
      desiredCapabilities: {
        os: 'OS X',
        os_version: 'Sierra',
        browser: 'Safari',
        browser_version: '10.1',
        resolution: '1600x1200'
      }
    },
    iphone: {
      desiredCapabilities: {
        browserName: 'iPhone',
        platform: 'MAC',
        device: 'iPhone 6S'
      }
    },
    ipad: {
      desiredCapabilities: {
        browserName: 'iPad',
        platform: 'MAC',
        device: 'iPad Air 2'
      }
    },
    android: {
      desiredCapabilities: {
        browserName: 'android',
        platform: 'ANDROID',
        device: 'Samsung Galaxy S5'
      }
    }
  }
};

// Code to support common capabilites
for (var i in nightwatch_config.test_settings) {
  if (nightwatch_config.test_settings.hasOwnProperty(i)) {
    var config = nightwatch_config.test_settings[i];
    config['selenium_host'] = nightwatch_config.selenium.host;
    config['selenium_port'] = nightwatch_config.selenium.port;
    config['desiredCapabilities'] = config['desiredCapabilities'] || {};
    for (var j in nightwatch_config.common_capabilities) {
      if (nightwatch_config.test_settings.hasOwnProperty(i)) {
        config['desiredCapabilities'][j] = config['desiredCapabilities'][j] || nightwatch_config.common_capabilities[j];
      }
    }
  }
}

module.exports = nightwatch_config;
