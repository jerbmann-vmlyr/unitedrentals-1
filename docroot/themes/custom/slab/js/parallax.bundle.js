(function (factory) {
  typeof define === 'function' && define.amd ? define(factory) :
  factory();
}(function () { 'use strict';

  /**
   * The .parallax-wrapper class imposes the CSS `perspective` property,
   * which creates a new CSS stacking context and therefore breaks any fixed
   * position elements.
   *
   * The <call-us> component's position is fixed. This patch pulls that block out
   * its current wrapper and moves it to the parent wrapper.
   */
  var moveCallUsComponent = function moveCallUsComponent() {
    // If something goes wrong here, it must not effect the execution of the calling script!
    try {
      var callUsEl = document.getElementById('block-urcallusblock');
      var parentNode = callUsEl.parentNode;
      parentNode.removeChild(callUsEl);
      parentNode.parentNode.parentNode.appendChild(callUsEl);
    } catch (error) {
      console.warn("The move <call-us> patch failed.", error);
    }
  };

  /**
   * An iframe and img tag placed by marketing scripts are forcing a
   * double scrollbar to appear. This patch addes a css style block to make
   * those tags position: fixed; so it doesnt overflow the screen.
   */
  var fixMarketingTags = function fixMarketingTags() {
    // If something goes wrong here, it must not effect the execution of the calling script!
    try {
      var fixMarketingCss = document.createElement('style');
      fixMarketingCss.type = 'text/css';
      var fixMarketingStyles = 'body > iframe, body > img { position: fixed; }';

      if (fixMarketingCss.styleSheet) {
        fixMarketingCss.styleSheet.cssText = fixMarketingStyles;
      } else {
        fixMarketingCss.appendChild(document.createTextNode(fixMarketingStyles));
      }

      document.querySelector('head').appendChild(fixMarketingCss);
    } catch (error) {
      console.error('Fix marketing CSS patch failed.', error);
    }
  };

  var enableParallax = function enableParallax() {
    // Add the .parallax-wrapper class to the root #app element
    document.getElementById('app').classList.add('parallax-wrapper');
    /**
     * Apply a fix for the Firefox perspective bug
     * http://blog.joelambert.co.uk/2012/02/13/3d-transformations-with-firefox-10/
     *
     * Set the transform-style to `preserve-3d` of all of the elements from the
     * parent `.parallax-wrapper` down to the element with `.parallax` class.
     */
    // document.querySelectorAll('.parallax').forEach(el => {
    //   let parent = el.parentElement;
    //   while (parent && !parent.classList.contains('parallax-wrapper')) {
    //     parent.style.MozTransformStyle = 'preserve-3d';
    //     parent = parent.parentElement;
    //   }
    // });

    /**
     * The bit below is a complete hack. Firefox, iOS and Edge both claim to
     * support certin css properties but do not. Using crappy feature detection
     * and UA sniffing to blunt force them into looking decent.
     * I am sorry for hurting your eyes.
     */

    var parallax = document.querySelector('.parallax');
    if (parallax) {
      if (typeof parallax.style.MozTransform === 'string' || typeof parallax.style.msGridRow === 'string' || /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
        document.querySelectorAll('.parallax').forEach(function (el) {
          el.style.transform = 'none';
        });
      } // Apply patches
    }

    moveCallUsComponent();
    fixMarketingTags();
  };

  if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window) {
    window.addEventListener('load', enableParallax);
  }

}));
