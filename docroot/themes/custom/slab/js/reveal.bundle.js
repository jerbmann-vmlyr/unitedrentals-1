(function (factory) {
  typeof define === 'function' && define.amd ? define(factory) :
  factory();
}(function () { 'use strict';

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArrayLimit(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance");
  }

  var isDesktop = function isDesktop() {
    return matchMedia('(min-width: 768px)').matches;
  };

  var getEffectTrigger = function getEffectTrigger(effectName, el) {
    var trigger = el.getAttribute("".concat(effectName, "-trigger")) || 'load';

    if (!['load', 'scroll'].includes(trigger)) {
      throw new Error("\"".concat(trigger, "\" is not a valid value for the [").concat(effectName, "-trigger] attribute!"));
    }

    return trigger;
  };

  var getScrollTargetAttribute = function getScrollTargetAttribute(effectName, el) {
    var target = el.getAttribute("".concat(effectName, "-trigger-scroll-target")) || 'self';

    if (!['self', 'parent', 'root'].includes(target)) {
      throw new Error("\"".concat(target, "\" is not a valid value for the [").concat(effectName, "-trigger-scroll-target] attribute!"));
    }

    return target;
  };

  var getScrollThreshold = function getScrollThreshold(effectName, el) {
    var parsedThreshold = Number.parseInt(el.getAttribute("".concat(effectName, "-trigger-scroll-threshold")), 10);
    return !isNaN(parsedThreshold) && parsedThreshold >= 0 && parsedThreshold <= 100 ? parsedThreshold : 100;
  };

  var getUnitDelay = function getUnitDelay(effectName, el) {
    var _replace$split$map$ma = (el.getAttribute("".concat(effectName, "-delay")) || '').replace(/ /g, '').split(',').map(function (delayStr) {
      return Number.parseInt(delayStr, 10);
    }).map(function (parsedDelay) {
      return !isNaN(parsedDelay) && parsedDelay > 0 ? parsedDelay : 0;
    }),
        _replace$split$map$ma2 = _slicedToArray(_replace$split$map$ma, 2),
        desktopDelay = _replace$split$map$ma2[0],
        _replace$split$map$ma3 = _replace$split$map$ma2[1],
        mobileDelay = _replace$split$map$ma3 === void 0 ? desktopDelay : _replace$split$map$ma3;

    return isDesktop() ? desktopDelay : mobileDelay;
  };
  /**
   * interface EffectDefinition {
   *   name: string;
   *   className: string;
   *   duration: number; // milliseconds
   *   defaultConfig: object;
   * }
   *
   * interface EffectRunner extends EffectDefinition {
   *   el: HTMLElement;
   *   rootEl: HTMLElement;
   *   unitDelay: number; // 0 - N
   *   direction: 'up' | 'down' | 'left' | 'right';
   *   trigger: 'load' | 'scroll';
   *   scrollTarget: 'self' | 'parent' | 'root;
   *   scrollThreshold: number; // 0 - 100
   *   run: () => void;
   *   runImmediate: () => void;
   * }
   */


  var createEffectRunner = function createEffectRunner(effectDefinition, rootEl) {
    return function (el) {
      var unitDelay = getUnitDelay(effectDefinition.name, el);
      var trigger = getEffectTrigger(effectDefinition.name, el);
      var scrollThreshold = getScrollThreshold(effectDefinition.name, el) / 100;

      var scrollTarget = function () {
        switch (getScrollTargetAttribute(effectDefinition.name, el)) {
          case 'root':
            return rootEl;

          case 'parent':
            return el.parentElement;

          default:
            return el;
        }
      }();

      return _objectSpread({}, effectDefinition.effectDefaults, {
        el: el,
        run: function run() {
          return setTimeout(function () {
            el.classList.add(effectDefinition.className);
          }, unitDelay * effectDefinition.duration);
        },
        runImmediate: function runImmediate() {
          return el.classList.add(effectDefinition.className);
        },
        rootEl: rootEl,
        scrollTarget: scrollTarget,
        scrollThreshold: scrollThreshold,
        trigger: trigger,
        unitDelay: unitDelay
      });
    };
  };

  var attachIntersectionObservers = function attachIntersectionObservers(effectRunner) {
    var observerCallback = function observerCallback(_ref, observer) {
      var _ref2 = _slicedToArray(_ref, 1),
          entry = _ref2[0];

      /**
       * Run effect if:
       * - The scroll threshold has been crossed
       * - The bounding rectangle of the target is above the top of the viewport,
       *   this happens when the site is reloaded while the user has already
       *   scrolled some amount down the page
       */
      var thresholdCrossed = entry && entry.intersectionRatio >= effectRunner.scrollThreshold;
      var aboveViewport = entry && entry.boundingClientRect.top < 0;

      if (thresholdCrossed || aboveViewport) {
        /** Once the effect has been run, no need to observe anymore; disconnect the observer */
        observer.disconnect();
        if (aboveViewport) effectRunner.runImmediate();else effectRunner.run();
      }
    };

    var observerOptions = {
      threshold: effectRunner.scrollThreshold
    };
    var observer = new IntersectionObserver(observerCallback, observerOptions);
    observer.observe(effectRunner.scrollTarget);
  }; // eslint-disable-next-line import/prefer-default-export


  var initEffects = function initEffects(effectDefinition) {
    return function (rootEl) {
      /**
       * Create the effect runners for rootEl and its children
       */
      var effectRunners = Array.from(rootEl.querySelectorAll("[".concat(effectDefinition.name, "]"))).map(createEffectRunner(effectDefinition, rootEl));
      /**
       * Automatically run the effects of the elements whose trigger is on "load"
       */

      effectRunners.filter(function (_ref3) {
        var trigger = _ref3.trigger;
        return trigger === 'load';
      }).forEach(function (_ref4) {
        var run = _ref4.run;
        return run();
      });
      /**
       * Filter down to the effect runners whose trigger is on "scroll"
       * and finally have their intersection observers connected
       */

      effectRunners.filter(function (_ref5) {
        var trigger = _ref5.trigger;
        return trigger === 'scroll';
      }).forEach(attachIntersectionObservers);
    };
  };

  var revealEffect = {
    name: 'reveal',
    className: 'reveal',
    duration: 333,
    effectDefaults: {
      trigger: 'load',
      unitDelay: 0
    }
  };

  var initReveal = function initReveal() {
    return document.querySelectorAll('[component-root]').forEach(initEffects(revealEffect));
  };

  if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window) {
    window.addEventListener('load', initReveal);
  } else {
    document.querySelector('html').classList.add('no-intersection-observer');
  }

}));
