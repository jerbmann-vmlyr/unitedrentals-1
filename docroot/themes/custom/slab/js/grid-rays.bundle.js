(function (factory) {
  typeof define === 'function' && define.amd ? define(factory) :
  factory();
}(function () { 'use strict';

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
  }

  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

      return arr2;
    }
  }

  function _iterableToArray(iter) {
    if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
  }

  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance");
  }

  /**
   * TODO!
   *
   * These are copied from tailwind.config.js and they should ultimately be defined
   * and imported from a site-wide shared file.
   */
  // eslint-disable-next-line import/prefer-default-export
  var COLORS = {
    transparent: 'transparent',
    // Blue
    blue: '#0050a9',
    'blue-lowlight': '#002f55',
    'blue-highlight': '#008cff',
    'blue-subtle': '#edf8ff',
    // Orange
    orange: '#f99222',
    'orange-lowlight': '#b03712',
    'orange-highlight': '#ffcf3d',
    'orange-subtle': '#fff2ec',
    // Bright Red
    'bright-red': '#e10000',
    'bright-red-lowlight': '#840000',
    'bright-red-highlight': '#ff4f4f',
    'bright-red-subtle': '#ffecec',
    // Green
    green: '#48bd00',
    'green-lowlight': '#237000',
    // Grey
    white: '#ffffff',
    'grey-2': '#f9f9f9',
    'grey-5': '#f2f2f2',
    'grey-11': '#dde0e2',
    'grey-38': '#91969b',
    'grey-69': '#414b50',
    black: '#000000',
    text: '#414b50'
  };

  var MINIMUM_RAY_SPACING = 220;
  var DEFAULT_RAY_OPTIONS = {
    beamColor: COLORS['grey-38'],
    trackColor: COLORS['grey-5']
  };
  var svgNS = 'http://www.w3.org/2000/svg';

  var createSvg = function createSvg() {
    var svg = document.createElementNS(svgNS, 'svg');
    svg.setAttribute('version', '1.1');
    svg.setAttribute('xmlns', svgNS);
    svg.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
    svg.setAttribute('xml:space', 'preserve');
    svg.setAttribute('preserveAspectRatio', 'none');
    svg.classList.add('grid-rays');
    return svg;
  };

  var getBeamColorFromAttribute = function getBeamColorFromAttribute(el) {
    var color = el.getAttribute('grid-rays-beam-color') || 'grey-11';

    if (!Object.keys(COLORS).includes(color)) {
      throw new Error("\"".concat(color, "\" is not a valid value for the [grid-rays-beam-color] attribute!"));
    }

    return COLORS[color];
  };

  var horizontalPathFactory = function horizontalPathFactory(w) {
    return function (y) {
      var path = document.createElementNS(svgNS, 'path');
      path.setAttribute('d', "M 0 ".concat(y, " h ").concat(w));
      return path;
    };
  };

  var verticalPathFactory = function verticalPathFactory(h) {
    return function (x) {
      var path = document.createElementNS(svgNS, 'path');
      path.setAttribute('d', "M ".concat(x, " 0 v ").concat(h, " 0"));
      return path;
    };
  };
  /**
   * @interface RayOptions {
   *  beamColor: string;
   *  trackColor: string;
   * }
   */


  var createRayGroup = function createRayGroup(position, pathCreator, rayOptions) {
    var g = document.createElementNS(svgNS, 'g');
    g.classList.add('ray');
    var track = pathCreator(position);
    track.classList.add('track');
    track.setAttribute('stroke', rayOptions.trackColor);
    var beam = pathCreator(position);
    beam.classList.add('beam');
    beam.setAttribute('stroke', rayOptions.beamColor);
    g.appendChild(track);
    g.appendChild(beam);
    return g;
  };

  var getRayCounts = function getRayCounts(gridParent) {
    var parsedHorizontalCount = Number.parseInt(gridParent.getAttribute('grid-rays-horizontal-count'), 10);
    var parsedVerticalCount = Number.parseInt(gridParent.getAttribute('grid-rays-vertical-count'), 10);
    return {
      horizontalRayCount: !isNaN(parsedHorizontalCount) && parsedHorizontalCount > 0 ? parsedHorizontalCount : 0,
      verticalRayCount: !isNaN(parsedVerticalCount) && parsedVerticalCount > 0 ? parsedVerticalCount : 0
    };
  };

  var resizeViewBox = function resizeViewBox(grid) {
    var _grid$parentElement$g = grid.parentElement.getBoundingClientRect(),
        height = _grid$parentElement$g.height,
        width = _grid$parentElement$g.width;

    grid.setAttributeNS(svgNS, 'viewBox', "0 0 ".concat(Math.ceil(width), " ").concat(Math.ceil(height)));
    return grid;
  };

  var initRays = function initRays() {
    document.querySelectorAll('[grid-rays]').forEach(function (root) {
      var grid = createSvg();
      root.prepend(grid);
      resizeViewBox(grid);

      var rayOptions = _objectSpread({}, DEFAULT_RAY_OPTIONS, {
        beamColor: getBeamColorFromAttribute(root)
      });

      var _grid$getBoundingClie = grid.getBoundingClientRect(),
          height = _grid$getBoundingClie.height,
          width = _grid$getBoundingClie.width;

      var _getRayCounts = getRayCounts(root),
          horizontalRayCount = _getRayCounts.horizontalRayCount,
          verticalRayCount = _getRayCounts.verticalRayCount;

      if (horizontalRayCount > 0) {
        var createHorizontalPath = horizontalPathFactory(width);
        var ySpacing = Math.max(height / horizontalRayCount, MINIMUM_RAY_SPACING);

        _toConsumableArray(Array(horizontalRayCount)).map(function (_, i) {
          return i * ySpacing + ySpacing / 2;
        }).filter(function (y) {
          return y < height;
        }).forEach(function (y) {
          grid.appendChild(createRayGroup(y, createHorizontalPath, rayOptions));
        });
      }

      if (verticalRayCount > 0) {
        var createVerticalPath = verticalPathFactory(height);
        var xSpacing = Math.max(width / verticalRayCount, MINIMUM_RAY_SPACING);

        _toConsumableArray(Array(verticalRayCount)).map(function (_, i) {
          return i * xSpacing + xSpacing / 2;
        }).filter(function (x) {
          return x < width;
        }).forEach(function (x) {
          grid.appendChild(createRayGroup(x, createVerticalPath, rayOptions));
        });
      }
    });
  };

  if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window) {
    window.addEventListener('load', initRays);
  }

}));
