/* something */
(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.show_featured_slide = {
    attach: function(context, drupalSettings) {
      $('.microsite-slide', context).once('slide-clicked').on('click', function() {
        // Get the hash value for the carousel to scope the click event to just "this" slideshow.
        var slideshowId = $(this).parents('.microsite-carousel').attr('data-slideshow-id');

        // Set active classes for the current slide.
        $('.microsite-carousel[data-slideshow-id=' + slideshowId + '] .microsite-slide').removeClass('is-active');

        $(this).addClass('is-active');

        // Set video back to not autoplay
        if ($('.microsite-carousel[data-slideshow-id=' + slideshowId + '] .microsite-slide--media-container.video-playing').length) {
          var src = $('.microsite-carousel[data-slideshow-id=' + slideshowId + ']  .microsite-slide--media-container.video-playing').find('.microsite-slide--video iframe')[0].src.split('?')[0];
          $('.microsite-carousel[data-slideshow-id=' + slideshowId + ']  .microsite-slide--media-container.video-playing .microsite-slide--video iframe')[0].src = src + "?autoplay=0&rel=0&start=0&frameborder=0";
        }

        // Remove class that shows the video.
        $('.microsite-carousel[data-slideshow-id=' + slideshowId + ']  .microsite-slide--media-container.video-playing').removeClass('video-playing');

        // Remove all active slides
        $('.microsite-carousel[data-slideshow-id=' + slideshowId + '] .microsite-slide--featured-slide').removeClass('is-active');

        // Show the slide that was clicked.
        var slideId = $(this).data('slide-id');
        $('.microsite-carousel[data-slideshow-id=' + slideshowId + ']  .microsite-slide--featured-slide[data-slide-id="' + slideId + '"]').addClass('is-active');
      });
    }
  };

  Drupal.behaviors.play_featured_slide = {
    attach: function(context, drupalSettings) {
      $('.microsite-slide--media-container', context).once('featured-clicked').on('click', function() {
        // Hide the image, show the video
        $(this).toggleClass('video-playing');

        // Get the src out of the iframe and set it to autoplay.
        // NOTE: this will not work for mobile devices as they do not have the autoplay feature.
        var src = $(this).find('.microsite-slide--video iframe')[0].src.split('?')[0];
        $(this).find('.microsite-slide--video iframe')[0].src = src + "?autoplay=1&rel=0&start=0&frameborder=0";
      });
    }
  };
})(jQuery, Drupal, drupalSettings);