/**
 * This looks for images within a responsive-background-image data attribute div
 * and pulls child image source and makes it a background image on the div.
 * This works with responsive images as well as it ties into the image's load
 * event causing the background image to be updated as well at the various
 * breakpoints.
 *
 * Followed mostly from this tutorial:
 * https://aclaes.com/responsive-background-images-with-srcset-and-sizes/
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.microsite_responsive_hero_images = {
    attach: function (context) {
      var elements = $('[responsive-hero-image]', context);

      for (var i = 0; i < elements.length; i++) {
        new Drupal.ResponsiveBackgroundHero(elements[i]);
      }

      $('.microsite-hero--media-container').once('hero-container').on('click', function(e) {
        $(this).toggleClass('video-playing');

        // Get the src out of the iframe and set it to autoplay.
        // NOTE: this will not work for mobile devices as they do not have the autoplay feature.
        var src = $(this).find('.microsite-hero--video iframe')[0].src.split('?')[0];
        $(this).find('.microsite-hero--video iframe')[0].src = src + "?autoplay=1&rel=0&start=0&frameborder=0&enablejsapi=1";
      });
    }
  };

  Drupal.ResponsiveBackgroundHero = function(element) {
    this.element = element;
    this.img = element.querySelector('img');
    this.src = '';

    this.img.addEventListener('load', function() {
      this.update();
    }.bind(this));

    if (this.img.complete) {
      this.update();
    }
  };

  Drupal.ResponsiveBackgroundHero.prototype.update = function() {
    var src = typeof this.img.currentSrc !== 'undefined' ? this.img.currentSrc : this.img.src;
    if (this.src !== src) {
      this.src = src;
      var gradient = drupalSettings.microsite.hero_gradient_string;
      this.element.style.backgroundImage = gradient + ", url(" + this.src + ")";

    }
  };

})(jQuery, Drupal, drupalSettings);
