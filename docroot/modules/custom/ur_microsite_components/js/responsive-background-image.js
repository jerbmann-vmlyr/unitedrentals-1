/**
 * This looks for images within a responsive-background-image data attribute div
 * and pulls child image source and makes it a background image on the div.
 * This works with responsive images as well as it ties into the image's load
 * event causing the background image to be updated as well at the various
 * breakpoints.
 *
 * Followed mostly from this tutorial:
 * https://aclaes.com/responsive-background-images-with-srcset-and-sizes/
 */
(function (Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.microsite_responsive_background_images = {
    attach: function (context) {
      var elements = document.querySelectorAll('[responsive-background-image]', context);

      for (var i = 0; i < elements.length; i++) {
        new Drupal.ResponsiveBackgroundImage(elements[i]);
      }
    }
  };

  Drupal.ResponsiveBackgroundImage = function(element) {
    this.element = element;
    this.img = element.querySelector('img');
    this.src = '';

    this.img.addEventListener('load', function() {
      this.update();
    }.bind(this));

    if (this.img.complete) {
      this.update();
    }
  };

  Drupal.ResponsiveBackgroundImage.prototype.update = function() {
    var src = typeof this.img.currentSrc !== 'undefined' ? this.img.currentSrc : this.img.src;
    if (this.src !== src) {
      this.src = src;
      var gradient = drupalSettings.microsite.section_gradient_string;
      this.element.style.backgroundImage = gradient + ", url(" + this.src + ")";
    }
  };

})(Drupal, drupalSettings);