## The Purpose of the Frontend API Module

The primary logic purpose of this module is to have all front-end routes being set in here. That way everyone that is trying to find a route goes to one place to figure out where to go from there in the codebase.

All routes for AJAX calls that the front-end uses should end up being defined in this modules routing file.

The secondary logic is that when we are using the server to make calls to external APIs (almost exclusively using the API Appliance anyway) then the controllers for that and for other actions can be housed here.

These are architecture decisions meant to ease future development.


## Caching

We are currently caching the accounts at a max-age of one week (604800 seconds). 

In the future, we might look into creating an admin setting/dashboard for invalidating caches for different api endpoints.



## Routing Configuration

```
ur_probox.get_details: // Unique identifier for the endpoint typically following api_name.controller.action
  path: '/api/probox/details' // the endpoint to route
  methods: [GET] // allowed methods
  defaults:
    _controller: '\Drupal\ur_frontend_api\Controller\ProBoxController::details'
    _title: 'ProBox Details'
  requirements:
    _permission: 'access content' // permissions needed to access endpoint
```


## Traits

We are now starting to use PHP Traits to implement shared functionality. Normally we would have stuck with the singular form of naming. However, the word "trait" is a PHP keyword and cannot be used in namespacing. For that reason, and because the class autoloader requires the namespace path to match the file path, we had to use the plural form for doing traits.

