<?php

namespace Drupal\ur_frontend_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for access to the API.
 */
class APIController extends ControllerBase {
  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  public $request;

  /**
   * Class constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request stack.
   */
  public function __construct(RequestStack $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack')
    );
  }

  /**
   * Helper method to retrieve all the parameters of a given GET or POST request.
   *
   * @return array|mixed|resource|string
   */

  /**
   * @return array|mixed|resource|string
   */
  public function paramsGetOrPost() {
    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $this->request->getCurrentRequest();
    $method = $request->getMethod();
    $params = [];

    switch ($method) {
      case "GET":
        // If a GET, return all query parameters.
        /** @var array $params */
        $params = $request->query->all();
        break;

      case "POST":
        // If a POST, return the POST body as an array.
        $params = $request->getContent();
        $params = json_decode($params);
        $params = (array) $params;
        break;

    }

    return $params;
  }

  /**
   * @inheritdoc
   */
  public function checkRequestedMethod($methodName) {
    if (method_exists($this, $methodName)) {
      return $methodName;
    }

    throw new \Exception('Method not available.');
  }

}
