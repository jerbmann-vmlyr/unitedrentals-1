<?php

namespace Drupal\ur_frontend_api\Controller;

use Drupal\ur_appliance\Provider\DAL;
use Drupal\Core\Controller\ControllerBase;
use Drupal\ur_frontend_api\Traits\ApiTrait;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DalRentalController.
 *
 * @package Drupal\ur_frontend_api\Controller
 */
class DalRentalController extends ControllerBase {

  use ApiTrait;

  /**
   * Respond to the rental/leniencyrates endpoint.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The Leniency Rates.
   */
  public function getLeniencyRates() {
    $response = new JsonResponse();
    $params = $this->paramsGetOrPost();

    $params['startDate'] = date('Ymd', strtotime($params['startDate']));
    $lineNumbers = $params['lineNum'];
    if (!is_array($lineNumbers)) {
      $lineNumbers = [$params['lineNum']];
    }

    // The Dal only supports 999 days worth of rates.
    if (array_key_exists('daysNeeded', $params)) {
      $params['daysNeeded'] = $params['daysNeeded'] <= 999 ? $params['daysNeeded'] : 999;
    }

    $cacheParams = $params;
    unset($cacheParams['lineNum']);

    $results = [];
    try {
      // Grab the current leniency rates.
      foreach ($lineNumbers as $lineNumber) {
        $cacheParams['lineNumber'] = $lineNumber;
        if ($cache = UrCache::get($cacheParams)) {
          $results[$params['contract']][$lineNumber] = $cache->data;
        }
        else {
          $params['lineNum'] = $lineNumber;
          $rates = DAL::Rental(TRUE)->LeniencyRates()->retrieve($params);
          $filteredRates = array_filter($rates, function ($a) {
            return $a['startDate'] != $a['endDate'] && $a['rate'] > 0;
          });
          $results[$params['contract']][$lineNumber] = array_values($filteredRates);
          // Now cache the results.
          UrCache::set($cacheParams, $filteredRates, FALSE, UrCache::DAY);
        }
      }
    }
    catch (\Exception $e) {
      $result = [$e->getMessage()];
      $response->setStatusCode('500');
    }

    $response->setData($results);

    return $response;
  }

}
