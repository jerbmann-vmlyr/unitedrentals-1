<?php

namespace Drupal\ur_frontend_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Masterminds\HTML5\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\ur_appliance\Provider\Placeable;

/**
 * {@inheritdoc}
 */
class ProBoxController extends ControllerBase {
  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  public $request;

  /**
   * Class constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request stack.
   */
  public function __construct(RequestStack $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function details() {
    $response = new JsonResponse();

    $data = [];
    $storage = \Drupal::entityTypeManager()->getStorage('node');

    $uid = \Drupal::currentUser()->id();

    // Retrieve the default proboxes.
    $default_probox_ids = \Drupal::entityQuery('node')
      ->condition('type', 'probox')
      ->condition('field_prebuilt', TRUE)
      ->execute();

    $default_proboxes = $storage->loadMultiple($default_probox_ids);
    $data['default'] = $default_proboxes;

    if ($uid) {
      // Retrieve the user proboxes.
      $user_probox_ids = \Drupal::entityQuery('node')
        ->condition('type', 'probox')
        ->condition('uid', $uid)
        ->execute();
      $user_proboxes = $storage->loadMultiple($user_probox_ids);
      $data['user'] = $user_proboxes;
    }

    $fields = [
      'uuid',
      'title',
      'uid',
      'created',
      'changed',
      'field_crew_size',
    // Notes.
      'field_plain_long'
    ];

    $response->setData($data);
    return $response;
  }

}
