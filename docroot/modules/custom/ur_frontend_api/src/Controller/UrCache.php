<?php

namespace Drupal\ur_frontend_api\Controller;

use Drupal\Core\Cache\Cache;
use function GuzzleHttp\Psr7\build_query;

/**
 * Class UrCache.
 *
 * @package Drupal\ur_frontend_api\Controller
 */
class UrCache {
  const PERMANENT = Cache::PERMANENT;
  const YEAR = 31449600;
  const MONTH = 2419200;
  const WEEK = 604800;
  const DAY = 86400;
  const HOUR = 3600;
  const MINUTE = 60;
  const FIVE_MINUTES = 300;
  const TEN_MINUTES = 600;
  const EIGHT_HOURS = 28800;

  /**
   * Loads Drupal's cache tool (or we can swap it out here for something else).
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   */
  public static function cache() {
    return \Drupal::cache();
  }

  /**
   * Builds a basic expiration time.
   *
   * @param int $seconds
   *   - The amount of time, in seconds, to expire the cache.
   *   One of the following values:
   *   - CacheBackendInterface::CACHE_PERMANENT: Indicates that the item should
   *     not be removed unless it is deleted explicitly.
   *   - Number of seconds: Indicates that the item will be considered invalid
   *     after this amount of time, i.e. it will not be returned by get() unless
   *     $allow_invalid has been set to TRUE. When the item has expired, it may
   *     be permanently deleted by the garbage collector at any time.
   *
   * @return int
   */
  public static function expire($seconds) {
    if ($seconds == self::PERMANENT) {
      return self::PERMANENT;
    }

    return time() + $seconds;
  }

  /**
   * Builds a cache ID from a list of parameters and the name of the calling
   * class and method.
   *
   * @param $params
   *   - Params to build cache ID (and load the data).
   * @param $cacheToUser
   *   - Should this be cached to the user?
   * @param $traceLevel
   *   - Level the trace will be at.
   *
   * @return string
   */
  public static function id($params, $cacheToUser = FALSE, $traceLevel = 1) {
    // Detect calling function and class.
    $trace = debug_backtrace();
    $baseName = '';

    // Use the trace to build the name.
    if (!empty($trace[$traceLevel])) {
      $caller = $trace[$traceLevel];
      $caller_array = explode('\\', $caller['class']);
      $class = array_pop($caller_array);
      $baseName = "$class|{$caller['function']}";
    }

    $paramString = build_query($params);
    $result = "$baseName|$paramString";

    // Tie this to the user, unless anonymous.
    if ($cacheToUser && !\Drupal::currentUser()->isAnonymous()) {
      $userId = \Drupal::currentUser()->id();
      $result = "UID:$userId|$result";
    }

    return $result;
  }

  /**
   * Attempts to retrieve the cache and builds the cache name dynamically from
   * a list of parameters and the name of the calling class/method.
   *
   * @param array $params
   *   - The parameters were used to load the data.
   * @param bool $cacheToUser
   *   - Should this be cached to the user?
   * @param bool $allowInvalid
   *
   * @return false|\stdClass
   */
  public static function get($params, $cacheToUser = FALSE, $allowInvalid = FALSE) {
    $cid = self::id($params, $cacheToUser, 2);
    return self::cache()->get($cid, $allowInvalid);
  }

  /**
   * Attempts to retrieve the cache and builds the cache name dynamically from
   * a list of parameters and the name of the calling class/method. It, finally,
   * pulls the data from the data element and returns that.
   *
   * @param array $params
   *   - The parameters were used to load the data.
   * @param bool $cacheToUser
   *   - Should this be cached to the user?
   * @param bool $allowInvalid
   *
   * @return bool
   */
  public static function getData($params, $cacheToUser = FALSE, $allowInvalid = FALSE) {
    $cache = self::get($params, $cacheToUser, $allowInvalid);

    if (isset($cache->data)) {
      return $cache->data;
    }

    return FALSE;
  }

  /**
   * Sets some data in a cache. Builds the cache ID itself. It also converts
   * an expiration of seconds into a timestamp.
   *
   * @param array $params
   *   - The parameters were used to load the data.
   * @param mixed $data
   *   - The data to store in the cache.
   *   Some storage engines only allow objects up to a maximum of 1MB in size to
   *   be stored by default. When caching large arrays or similar, take care to
   *   ensure $data does not exceed this size.
   * @param bool $cacheToUser
   *   - Should this be tied to the user.
   * @param int $expire
   *   - The expiration time, in seconds.
   *   One of the following values:
   *   - CacheBackendInterface::CACHE_PERMANENT: Indicates that the item should
   *     not be removed unless it is deleted explicitly.
   *   - Number of seconds: Indicates that the item will be considered invalid
   *     after this amount of time, i.e. it will not be returned by get() unless
   *     $allow_invalid has been set to TRUE. When the item has expired, it may
   *     be permanently deleted by the garbage collector at any time.
   * @param array $tags
   *
   * @return mixed
   */
  public static function set(array $params, $data, $cacheToUser = FALSE, $expire = Cache::PERMANENT,
  array $tags = []) {
    $cid = self::id($params, $cacheToUser, 2);

    // Simplifies how we do the expiration.
    $expire = self::expire($expire);

    if ($cacheToUser) {
      $userId = \Drupal::currentUser()->id();
      $tags[] = "user:$userId";
    }

    return self::cache()->set($cid, $data, $expire, $tags);
  }

  /**
   * Deletes a bit of cache. Builds the name for the cache.
   *
   * @param array $params
   * @param bool $cacheToUser
   *
   * @return mixed
   */
  public static function delete(array $params, $cacheToUser = FALSE) {
    $cid = self::id($params, $cacheToUser, 2);
    return self::cache()->delete($cid);
  }

  /**
   * Invalidates a bit of cache. Builds the name for the cache.
   *
   * @param string $cid
   *   - Cache ID.
   *
   * @return mixed
   */
  public static function invalidate($cid) {
    return self::cache()->invalidate($cid);
  }

  /**
   * Invalidates a bit of cache. Builds the name for the cache.
   *
   * @param array $params
   * @param bool $cacheToUser
   *
   * @return mixed
   */
  public static function invalidateByAutoId(array $params, $cacheToUser = FALSE) {
    $cid = self::id($params, $cacheToUser, 2);
    return self::cache()->invalidate($cid);
  }

}
