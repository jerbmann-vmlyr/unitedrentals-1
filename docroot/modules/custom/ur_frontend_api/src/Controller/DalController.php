<?php

namespace Drupal\ur_frontend_api\Controller;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Entity\EntityStorageException;
use Exception;
use Drupal\ur_appliance\Data\DAL\Estimate;
use Drupal\user\Entity\User;
use Drupal\Core\Controller\ControllerBase;
use Drupal\ur_cart\Controller\CartController;
use Drupal\ur_frontend_api\Traits\ApiTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\ur_appliance\Data\DAL\Jobsite;
use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\Provider\DAL;
use Drupal\ur_utils\Providers\Cache\UrCacheProvider;
use Drupal\ur_appliance\Data\DAL\TcUser;

/**
 * Class DalController.
 *
 * @package Drupal\ur_frontend_api\Controller
 */
class DalController extends ControllerBase {
  use ApiTrait;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  public $request;

  /**
   * Class constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request stack.
   */
  public function __construct(RequestStack $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('request_stack')
    );
  }

  /**
   * Retrieves the user's guid.
   *
   * @return array|mixed|null|\Drupal\ur_appliance\Data\DAL\TcUser
   *   The user's guid or error message from API call.
   */
  public function getUserGuid() {
    $email = \Drupal::currentUser()->getEmail();
    $cacheParams = ['email' => $email];
    $guid = NULL;

    // Get TcUser Data for the current logged in user and match with the DAL.
    try {
      if ($cache = UrCache::get($cacheParams, TRUE)) {
        $guid = $cache->data;
      }
      else {
        $guid = DAL::Customer(TRUE)->TcUser()->readSimple($email);
        UrCache::set($cacheParams, $guid, TRUE, UrCache::MONTH);
      }
    }
    catch (Exception $e) {
      return [$e->getMessage()];
    }

    return $guid;
  }

  /**
   * Moved this into it's own function to improve cacheability.
   *
   * @return array|mixed|null|\Drupal\ur_appliance\Data\DAL\TcUser
   *   The TC User object or error message from api call.
   */
  public function getTcUserData() {
    $email = \Drupal::currentUser()->getEmail();
    $cacheParams = ['email' => $email];
    $result = NULL;

    // Get TcUser Data for the current logged in user and match with the DAL.
    try {
      if ($cache = UrCache::get($cacheParams, TRUE)) {
        $result = $cache->data;
      }
      else {
        $result = DAL::Customer(TRUE)
          ->TcUser()
          ->getTcUserEndpoint()
          ->read($email);
        UrCache::set($cacheParams, $result, TRUE, UrCache::WEEK);
      }
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    return $result;
  }

  /**
   * Retrieves rates for given cat-classes.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The results for retrieving rates per cat-class.
   *
   * @throws Exception
   */
  public function getRates(array $paramsIn = [], $json = TRUE) {
    $params = $this->paramsGetOrPost();
    if (!empty($paramsIn)) {
      $params = array_merge($params, $paramsIn);
    }
    $result = NULL;

    try {
      if ($cache = UrCache::get($params)) {
        $result = $cache->data;
      }
      else {
        // Grab the current rates.
        $result = DAL::Rental(TRUE)->Rates()->retrieve($params);
        $result = \Drupal::service('ur_location_filter.handler')->filterItemRates($params['branchId'], $result);

        // Now cache the results.
        UrCache::set($params, $result, FALSE, UrCache::DAY);
      }
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    if (!$json) {
      return $result;
    }

    $response = new JsonResponse();
    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves all accounts for a given email.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The exported account objects from calling TcUser and customer/accounts.
   *
   * @throws Exception
   */
  public function accounts() {
    $result = NULL;
    $userId = \Drupal::currentUser()->id();
    $response = new JsonResponse();

    try {
      $cache = UrCache::get([], TRUE);

      if (!empty($cache)
          && (!isset($cache->data['error']) || $cache->data['error'] == FALSE)) {
        $result = $cache->data;
      }
      else {
        $email = \Drupal::currentUser()->getEmail();
        $result = DAL::Customer(TRUE)->TcUser()->read($email);

        // Cache result for user, set cache expiration to an hour.
        UrCache::set([], $result, TRUE, UrCache::HOUR, ["user:$userId"]);
      }
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
      $response->setStatusCode(204);
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Creates a new account in rental man.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The account ID from the newly created account.
   */
  public function postAccount() {
    $result = NULL;
    $params = $this->paramsGetOrPost();

    try {
      $result = DAL::Customer(TRUE)->Account()->create($params);

      /*
       * After posting account we need to invalidate the user's accounts cache
       * so that the newly created account will appear in the list on next call
       */
      $userId = \Drupal::currentUser()->id();
      UrCache::invalidate("UID:$userId|DalController|accounts|");
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response = new JsonResponse($result);
    return $response;
  }

  /**
   * Calls the api-appliance to update account information.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   TRUE or FALSE based on if updating the account was successful.
   */
  public function updateAccount() {
    $result = NULL;
    $params = $this->paramsGetOrPost();

    try {
      $result = DAL::Customer(TRUE)->Account()->update($params);

      /*
       * After updating account we need to invalidate the user's accounts cache
       * so that the updated account will be reflected on the next account GET
       */
      $userId = \Drupal::currentUser()->id();
      UrCache::invalidate("UID:$userId|DalController|accounts|");
    }
    catch (Exception $e) {
      $result = [
        'error' => TRUE,
        'message' => $e->getMessage(),
      ];
    }

    $response = new JsonResponse($result);
    return $response;
  }

  /**
   * Creates a dummy account used from anonymous checkout.
   *
   * Handles creating a first Account to act as a placeholder for user's who
   * currently do not have accounts, but are a registered UR user and are going
   * through the checkout process.
   *
   * At this step in the checkout process a user would have filled out
   * information for a jobsite, but we need to have an account to create a
   * jobsite. This will handle both using data from the jobsite. Then later in
   * the process the user can update their account information and all will be
   * right with the world.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The account ID and jobsite ID of the newly created account and jobsite.
   *
   * @throws Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function firstAccount() {
    $result = NULL;
    $jobsiteInfo = $this->paramsGetOrPost();
    $jobsite = new Jobsite($jobsiteInfo);

    try {
      // Now create an account with data from the jobsite.
      $fauxData = [
        'name' => 'New Account',
        'address1' => $jobsite->getAddress1(),
        'address2' => $jobsite->getAddress2(),
        'city' => $jobsite->getCity(),
        'state' => $jobsite->getState(),
        'zip' => $jobsite->getZip(),
        'phone' => $jobsite->getMobilePhone(),
        'email' => $jobsite->getEmail(),
      ];

      // This returns the newly created account ID.
      $accountId = DAL::Customer(TRUE)->Account()->create($fauxData);

      // Now submit the jobsite.
      $jobsiteInfo['accountId'] = (string) $accountId;
      $jobsite = DAL::Customer()->Jobsite()->create($jobsiteInfo);

      // Now we have created a new account, linked it to the user, and created a
      // jobsite. Send back the accountId and jobsiteId to front-end.
      $result = [
        'accountId' => $accountId,
        'jobsiteId' => $jobsite['id'],
      ];
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response = new JsonResponse($result);
    return $response;
  }

  /**
   * Retrieves availability status for equipment from the DAL.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   List of availablity status of equipment from rental man.
   *
   * @throws Exception
   */
  public function availability(array $paramsIn = [], $json = TRUE) {
    $params = $this->paramsGetOrPost();
    if (!empty($paramsIn)) {
      $params = array_merge($params, $paramsIn);
    }
    $result = NULL;

    try {
      if ($cache = UrCache::get($params)) {
        $result = $cache->data;
      }
      else {
        $result = DAL::Equipment(TRUE)->Availability()->read($params);

        // Cache result for user, set cache expiration to 5 minutes.
        UrCache::set($params, $result, FALSE, UrCache::FIVE_MINUTES);
      }
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    if (!$json) {
      return $result;
    }

    $response = new JsonResponse();
    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves a TcUser object from the DAL.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Array containing TcUser data.
   *
   * @throws \InvalidArgumentException|\LogicException
   *   Exception passed from DAL TcUser::read method.
   */
  public function tcuserSimple() {
    $response = new JsonResponse();
    $params = $this->paramsGetOrPost();
    $result = NULL;
    $id = NULL;
    $altAuth = FALSE;

    try {
      if (!empty($params['id'])) {
        $id = $params['id'];
      }
      else {
        $id = \Drupal::currentUser()->getEmail();
      }

      if (!empty($params['useAltAuth'])) {
        $altAuth = $params['useAltAuth'];
      }

      if (empty($id)) {
        throw new Exception('No ID passed in and unable to load an ID.');
      }

      $result = DAL::Customer(TRUE)->TcUser()->read($id, TRUE, $altAuth);
    }
    catch (Exception $e) {
      $result = $e->getMessage();
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves all Accounts from a TcUser.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The list of the Accounts retrieved from the DAL.
   *
   * @throws Exception
   */
  public function tcusers() {
    $response = new JsonResponse();
    $params = $this->paramsGetOrPost();
    $result = NULL;

    $id = $params['id'];

    if (!$id) {
      $id = \Drupal::currentUser()->getEmail();
    }

    try {
      $result = DAL::Customer(TRUE)->TcUser()->read($id);
    }
    catch (Exception $e) {
      $result = $e->getMessage();
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves all requesters for a specific account.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The list of requesters from the DAL.
   *
   * @throws Exception
   */
  public function requesters() {
    $params = $this->paramsGetOrPost();
    $accountId = $params['accountId'];
    $cacheParams = ['accountId' => $accountId];
    $result = NULL;

    // Read the requester data.
    try {
      if ($cache = UrCache::get($cacheParams, TRUE)) {
        $result = $cache->data;
      }
      else {
        $result = DAL::Customer(TRUE)->Requesters()->read($accountId);
        UrCache::set($cacheParams, $result, TRUE, UrCache::DAY);
      }
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    // Get the TC User Data that we will end up using now.
    $tcUserData = $this->getTcUserData();

    // Get the TcUser guid.
    $guid = NULL;
    if (!empty($tcUserData) && $tcUserData instanceof TcUser) {
      $guid = $tcUserData->getGuid();
    }

    /*
     * Add the guid to the zero index if it is not in the data array, then
     * pass the other data properties of the user as well. Do this if we got no
     * requesters back as well.
     */
    if (!empty($guid) && (empty($result) || !in_array($guid, $result))) {
      $newEntry = [
        'guid' => $guid,
        'email' => \Drupal::currentUser()->getEmail(),
        'name' => $tcUserData->getFirstName() . ' ' . $tcUserData->getLastName()
      ];

      array_unshift($result, $newEntry);
    }

    $response = new JsonResponse($result);

    return $response;
  }

  /**
   * Retrieves approves for a specific account from the DAL.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The list of approvers from the DAL.
   *
   * @throws Exception
   */
  public function approvers() {
    $result = NULL;
    $params = $this->paramsGetOrPost();
    $accountId = $params['accountId'];
    $cacheParams = ['accountId' => $accountId];

    try {
      if ($cache = UrCache::get($cacheParams, TRUE)) {
        $result = $cache->data;
      }
      else {
        $result = DAL::Customer(TRUE)->Approvers()->read($accountId);
        UrCache::set($cacheParams, $result, TRUE, UrCache::DAY);
      }
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response = new JsonResponse($result);

    return $response;
  }

  /**
   * Retrieves all credit cards from the DAL.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The returned credit card data.
   *
   * @throws Exception
   */
  public function creditCards() {
    $response = new JsonResponse();
    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->CreditCard()->readAll();
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Credit Cards Add method.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The newly created credit card ID.
   */
  public function creditCardsAdd() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();
    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->CreditCard()->create($data);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Credit Cards Update method.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The updated card object.
   */
  public function creditCardsUpdate() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();
    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->CreditCard()->update($data);
    }
    catch (Exception $e) {
      $result = [$e->getmessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Credit Cards Delete method.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The DAL Result response for deleted Credit Card as JSON.
   */
  public function creditCardsDelete($cardId) {
    $response = new JsonResponse();
    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->CreditCard()->delete($cardId);
    }
    catch (Exception $e) {
      $result = [$e->getmessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Creates a new Rental Transaction.
   *
   * @TODO: Talk to patrick about calling other order comments endpoint and
   *  various other endpoints that need to happen in succession. This could
   *  probably go away as we have the "submitTransaction" method.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The completed transaction object.
   *
   * @throws \InvalidArgumentException
   * @throws \LogicException
   */
  public function postTransaction() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();
    $result = NULL;

    try {
      $result = DAL::Rental(TRUE)->Transactions()->post($data);
    }
    catch (\Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves the allocation/requisition codes for a given account.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The returned allocation/requisition code data.
   *
   * @throws Exception
   */
  public function allocationCodes() {
    $response = new JsonResponse();
    $result = NULL;
    $accountId = $this->request->getCurrentRequest()->query->get('accountId');
    $codeType = $this->request->getCurrentRequest()->query->get('codeType');

    if ($accountId == NULL || $codeType == NULL) {
      $response->setStatusCode(400);
      $response->setData([
        'error' => TRUE,
        'message' => 'accountId or codeType cannot be null.',
      ]);

      return $response;
    }

    try {
      $result = DAL::Customer(TRUE)
        ->AllocationCodes()
        ->readAll($accountId, $codeType);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Reads all jobsites.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   All jobsites for a given account
   *
   * @throws Exception
   */
  public function jobsites() {
    $response = new JsonResponse();
    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->Jobsite()->read(
        $this->request->getCurrentRequest()->query->all()
      );
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Creates a new JobSite.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The newly created Jobsite object.
   */
  public function postJobsite() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();

    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->Jobsite()->create($data);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Updates a JobSite.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   An updated jobsite object after having been updated.
   */
  public function updateJobsite() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();

    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->Jobsite()->update($data);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Creates a new TcUsers.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The newly created TC User's guid.
   *
   * @throws \LogicException
   */
  public function postTcUser() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();
    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->TcUser()->create($data);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Updates a new TcUsers.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The updated TC user object or error message from customer/tcuser PUT.
   *
   * @throws \LogicException
   */
  public function updateTcUser() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();
    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->TcUser()->update($data);

      if (array_key_exists('email', $data)) {
        $uid = \Drupal::currentUser()->id();
        $user = User::load($uid);
        $user->setEmail($data['email'])
          ->setUsername($data['email'])
          ->save();
      }
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves all contracts for a given accountId.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The result of the rental/transactions endpoint or error message.
   *
   * @throws Exception
   */
  public function getAllContracts() {
    $response = new JsonResponse();
    $params = $this->paramsGetOrPost();
    $result = NULL;

    try {
      if ($cache = UrCache::get($params, TRUE)) {
        $result = $cache->data;
      }
      else {
        $result = DAL::Rental(TRUE)->Contracts()->readAll($params);
        UrCache::set($params, $result, TRUE, UrCache::HOUR);
      }
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Updates a Contract with a new end date.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   An updated transaction object after having been updated.
   */
  public function updateContract() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();

    $result = NULL;

    try {
      $result = DAL::Rental(TRUE)->Contracts()->update($data);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves all transactions for a given account.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The result of the rental/transactions endpoint or error message.
   *
   * @throws Exception
   */
  public function getAllTransactions() {
    // This functions is for all the transactions for a specific accountId not the transId.
    $response = new JsonResponse();
    $params = $this->paramsGetOrPost();
    $result = NULL;

    try {
      // Assigned the code in purpose because of the type keyword on js side.
      // TODO: Find a better variable and way to assign this value instead of copying the parameter.
      $params['type'] = $params['transType'];
      $result = DAL::Rental(TRUE)->Transactions()->readAll($params);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * This is the method that submits a finalized transaction.
   *
   * Submits, the transaction, the comments, and allocation/requisition codes.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The completed transaction object or error message.
   *
   * @throws Exception
   */
  public function submitTransaction() {
    $response = new JsonResponse();

    // Grab admin config.
    $adminConfig = \Drupal::config('ur_admin.settings');
    if (!empty($adminConfig)) {
      // Check time to see if this transaction POST is outside the rate limit expiration.
      $rateLimitingEnabled = (bool) $adminConfig->get('checkout_transaction_rate_limit_enabled');
      if ($rateLimitingEnabled && isset($_SESSION['transaction_post_expiration'])
        && time() <= $_SESSION['transaction_post_expiration']
      ) {
        // Code 429 is Rate Limit Exceeded.
        $response->setStatusCode(429);
        // TODO: Set actual response body, spec says to state how long until rate limit is released.
        $result = 'Submit Transaction: Rate limit exceeded';
        // TODO: Log a better error.
        \Drupal::logger('frontend_api')->error($result);

        return $response;
      }
    }

    $params = $this->request->getCurrentRequest()->getContent();

    /*
     * Need to build an associative array here rather than just returning an
     * array with objects in it
     */
    $data = json_decode($params, TRUE);

    /*
     * ----------------------------------------------------------
     * We NEED the user's email... by user I mean the user that
     * is currently logged in, and making the current request
     *
     * THIS IS REQUIRED
     * We MUST have the email sent along with the record, and the
     * email MUST be a current, valid email address for the user
     * or that user will not be able to retrieve these records
     */
    $data['transaction']['email'] = \Drupal::currentUser()->getEmail();
    /*
     * ----------------------------------------------------------
     */

    if (!empty($data['transaction']['promotionCode'])) {
      $discountEquipment = [];
      foreach ($data['transaction']['items'] as $item) {
        if ($item['discountApplied']) {
          $discountEquipment[] = $item['catClass'];
        }
      }
      $data['transaction']['genComments'] = 'Discount code ' . $data['transaction']['promotionCode'] . ' applied to items: ' . implode(", ", $discountEquipment);
    }

    try {
      // We don't want this as an array just yet.
      $order = DAL::Rental()->Transactions()->submit($data);

      // Have admin config?
      if (!empty($adminConfig)) {
        // Is rate limiting enabled?
        $rateLimitingEnabled = (bool) $adminConfig->get('checkout_transaction_rate_limit_enabled');
        if ($rateLimitingEnabled) {
          // Grab rate limiting expiration.
          $rateLimitingExpiration = $adminConfig->get('checkout_transaction_rate_limit_expiration');
          // Default rate limit for 30 seconds.
          if ($rateLimitingExpiration === NULL) {
            $rateLimitingExpiration = 30;
          }
          $_SESSION['transaction_post_expiration'] = time() + $rateLimitingExpiration;
        }
      }
    }
    catch (Exception $e) {
      $order = [$e->getMessage()];
    }

    if ($order instanceof Order && $order->getId()) {
      $response->setData($order->export());

      /*
       * Apply discount if coupon/voucher is submitted by user
       */
      if (isset($data['transaction']['promotionCode']) && !is_null($data['transaction']['promotionCode'])) {
        $voucherCode = $data['transaction']['promotionCode'];
      }

      if (!empty($voucherCode)) {
        /*
         * If customer entered voucher/coupon, redeem it now.
         */
        $moduleHandler = \Drupal::service('module_handler');

        if ($moduleHandler->moduleExists('ur_voucher')) {
          /** @var \Drupal\ur_voucher\VoucherController $voucherHandler */
          // \Drupal::service('ur_api_dataservice')->getPlugin('Voucher');.
          $voucherHandler = \Drupal::service('ur_voucher.handler');
          $voucherParams = $voucherHandler->setVoucherParams($data['transaction'], $order);
          $voucherHandler->redeemCode($voucherCode, $voucherParams);
        }
      }

      // Now send transaction complete to Pardot.
      $cart = CartController::grabCurrentCart();
      if ($cart != NULL) {
        \Drupal::service('ur_pardot.pardot')->sendCompletedSale($order->getId(), $cart->id());

        /*
         * Now do cart and checkout cleanup - We have to specify which items
         * for multi-transaction carts.
         * Pull out the cart items that were specifically checked out with.
         */
        $itemIds = $data['transaction']['itemIds'];
        CartController::cleanup($itemIds);
      }
      $this->invalidateOnRentCache('1,2,3', $data['transaction']['accountId'], NULL, FALSE);
      $this->invalidateOnRentCache('6', $data['transaction']['accountId'], NULL, FALSE);
    }
    else {
      $response->setData($e->getMessage());
    }

    return $response;
  }

  /**
   * Updates a Transaction.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   An updated transaction object after having been updated.
   */
  public function updateTransaction() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();

    $result = NULL;

    try {
      // Updates the transaction PO Number if user changed it in CONVERT QUOTE Form
      // AND CONVERTS a quote to a reservation...
      // It's an update call, correct... BUT Reason?
      // PO Number change cannot be done while converting a quote,
      // so multiple calls are needed to convert a quote PLUS changing PO Number or only convert a quote.
      if ($data['updateType'] == 'reserve') {
        // Handles the PO number edit (if any changes made) and convert the quote.
        $result = DAL::Rental(TRUE)->Transactions()->convertQuote($data);
        $this->invalidateOnRentCache('1,2,3', $data['id'], $data['quoteId'], FALSE);
        $this->invalidateOnRentCache('6', $data['id'], $data['quoteId'], FALSE);
        $this->invalidateOnRentCache('1,2,3', $data['id'], $data['quoteId'], TRUE);
        $this->invalidateOnRentCache('6', $data['id'], $data['quoteId'], TRUE);
      }
      // Only updates transaction and requisition. NO CONVERT HAPPENS IN ELSE STATEMENT.
      else {
        $result = DAL::Rental(TRUE)->Transactions()->update($data);
        if ($data['po']) {
          $this->updateOnRentCache('po', $data['po'], '1,2,3', $data['accountId'], $data['transId'], FALSE);
          $this->updateOnRentCache('po', $data['po'], '6', $data['accountId'], $data['transId'], FALSE);
          $this->updateOnRentCache('po', $data['po'], '1,2,3', $data['accountId'], $data['transId'], TRUE);
          $this->updateOnRentCache('po', $data['po'], '6', $data['accountId'], $data['transId'], TRUE);
        }
      }
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Updates the cache for this specific account and user for the onRent data call used on the manage equipment and orders pages.
   *
   * @param string $paramName
   *   - The name of the parameter to update.
   * @param mixed $paramValue
   *   - The value of the parameter to update.
   * @param mixed $types
   *   - The transaction types to be cleared.
   * @param mixed $accountId
   *   - The account id for this transaction.
   * @param mixed $transId
   *   - The trans id for this transaction.
   * @param bool $includeTransId
   *   - Indicates if the transId should be included as a request param. This is to facilitate the separate caches
   *    for all on rent transactions and single transactions.
   */
  public function updateOnRentCache($paramName, $paramValue, $types, $accountId, $transId, $includeTransId) {
    $cid = $this->buildOnRentCid($types, $accountId, $transId, $includeTransId);
    if ($cacheItem = UrCacheProvider::getByCacheId($cid, 'ur_db_api')) {
      foreach ($cacheItem as &$obj) {
        if ($obj->transId == $transId) {
          $obj->$paramName = $paramValue;
        }
      }
      UrCacheProvider::setByCacheId($cacheItem, $cid, 'ur_db_api', 86400);
    }
  }

  /**
   * Invalidates the cache for this specific account and user for the onRent data call used on the manage equipment and orders pages.
   *
   * @param mixed $types
   *   - The transaction types to be cleared.
   * @param mixed $accountId
   *   - The account id for this transaction.
   * @param mixed $transId
   *   - The trans id for this transaction.
   * @param mixed $includeTransId
   *   - Indicates if the transId should be included as a request param. This is to facilitate the separate caches
   *    for all on rent transactions and single transactions.
   */
  public function invalidateOnRentCache($types, $accountId, $transId, $includeTransId) {
    $cid = $this->buildOnRentCid($types, $accountId, $transId, $includeTransId);
    UrCacheProvider::invalidateByCid($cid, 'ur_db_api');
  }

  /**
   * Builds the cid needed for cache calls.
   *
   * @param mixed $types
   *   - The transaction types to be cleared.
   * @param mixed $accountId
   *   - The account id for this transaction.
   * @param mixed $transId
   *   - The trans id for this transaction.
   * @param mixed $includeTransId
   *   - Indicates if the transId should be included as a request param. This is to facilitate the separate caches
   *    for all on rent transactions and single transactions.
   *
   * @return string
   *   Generated onRent CID.
   */
  public function buildOnRentCid($types, $accountId, $transId, $includeTransId) {
    $userId = \Drupal::currentUser()->id();
    $ids[0] = 'id';
    $ids[1] = 'types';
    $ids[2] = 'transId';

    $settings['key'] = "equipment-on-rent";
    $settings['idFields'] = $ids;
    $settings['uidKey'] = 'uid:' . $userId;
    $settings['baseName'] = 'EquipmentOnRentPlugin|index';

    $requestParams['id'] = $accountId;
    $requestParams['types'] = $types;
    $requestParams['transId'] = $includeTransId ? $transId : NULL;

    return UrCacheProvider::generateId($requestParams, $settings);
  }

  /**
   * Updates a Requisition.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   An updated requisition object after having been updated.
   */
  public function updateRequisition() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();

    $result = NULL;

    try {
      $result = DAL::Rental(TRUE)->Requisitions()->update($data);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Updates a Requisition and Transaction Start and End DateTime.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   An updated requisition object after having been updated.
   */
  public function updateTransactionEditForms() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();

    $result = NULL;

    try {
      $result = DAL::Rental(TRUE)->Transactions()->updateTransactionEditForms($data);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Updates a Requisition and Transaction's JobId.
   *
   * AND/OR updates the jobId's contact info in rental/requisition endpoint
   *   OR creates a new Jobsite AND assigns the id to the requisition and transaction.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   An updated requisition object after having been updated.
   */
  public function quoteJobsiteInfoUpdate() {
    $response = new JsonResponse();
    $data = $this->paramsGetOrPost();

    $result = NULL;

    try {
      if ($data['jobsiteQuoteEditFlag'] === TRUE) {
        $result_requisition = DAL::Rental(TRUE)->Requisitions()->update($data);

        if (isset($result_requisition->status) && $result_requisition->status == 0) {

          if ($data['transId'] != "") {
            $data['updateType'] = 'transaction';
            $result_transaction = DAL::Rental(TRUE)->Transactions()->update($data);
          }

          $data['status'] = 'A';
          $result = DAL::Customer(TRUE)->Jobsite()->update($data);
        }
      }
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves the Contract PDF from the DAL for a user to see.
   *
   * @return array
   *   The PDF file for viewing
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws Exception
   */
  public function viewPdf() {
    $transId = $this->request->getCurrentRequest()->get('transId');

    if (empty($transId)) {
      throw new NotFoundHttpException();
    }

    $data = ['id' => $transId];
    $result = DAL::File()->ContractFile()->read($data);

    if (!empty($result)) {
      // Result is an array, grab the first file.
      /** @var \Drupal\ur_appliance\Data\DAL\File $file */
      $file = $result[0];
      $pdf = base64_decode($file->getFile());

      header('Content-Type: application/pdf; charset=utf-8');
      print $pdf;
      exit;
    }

    return ['#markup' => 'This reservation agreement could not be loaded.'];
  }

  /**
   * Retrieves all reservations for a given user.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The result of the /customer/reservations API call.
   *
   * @throws Exception
   */
  public function reservations() {
    $response = new JsonResponse();
    $params = $this->validateGetParams($this->request->getCurrentRequest()->query->all());
    $params['guid'] = $this->getUserGuid();
    $params['openQuotes'] = 'E';
    $params['openRsvReq'] = 'I';
    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->Reservation()->read($params);
    }
    catch (Exception $e) {
      $result['exception'] = $e->getMessage();
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves quotes for a given user guid.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The result of calling for a user's quotes.
   *
   * @throws Exception
   */
  public function quotes() {
    $response = new JsonResponse();
    $params = $this->validateGetParams($this->request->getCurrentRequest()->query->all());
    $params['guid'] = $this->getUserGuid();
    $params['openQuotes'] = 'I';
    $params['openRsvReq'] = 'E';
    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->Quote()->read($params);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves Requisition Codes from the api-appliance.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The results of calling the combination of customer/allocationrequisitioncodetitles
   *   and rental/transactioncostallocations endpoints.
   *
   * @throws Exception
   */
  public function requisitionCodes() {
    $accountId = $this->request->getCurrentRequest()->get('accountId');
    $transId = $this->request->getCurrentRequest()->get('transId');
    $response = new JsonResponse();
    $result = NULL;

    try {
      $result = DAL::Rental(TRUE)->RequisitionCodes()->readCodes($accountId, $transId);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves charge estimates from the api-appliance.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The results of calling the /rental/chargeestimates endpoint.
   *
   * @throws Exception
   */
  public function estimates() {
    $response = new JsonResponse();
    $params = $this->paramsGetOrPost();
    $results = NULL;
    $id = $params['accountId'];

    try {
      $estimate = DAL::Rental()->Estimates()->read($params);
    }
    catch (Exception $e) {
      $results = [$e->getMessage()];
      $response->setData($results);

      return $response;
    }

    /*
     * Apply discount if coupon/voucher is submitted by user
     */
    if (isset($params['promotionCode']) && !is_null($params['promotionCode'])) {
      $voucherCode = $params['promotionCode'];
      $orgRentAmt = $this->getOriginalItemTotals($estimate);
    }

    if (!empty($estimate->getTotals()) && !empty($voucherCode)) {
      $totals = $estimate->getTotals();
      $moduleHandler = \Drupal::service('module_handler');

      if ($moduleHandler->moduleExists('ur_voucher')) {
        /** @var \Drupal\ur_voucher\VoucherController $voucherHandler */
        $voucherHandler = \Drupal::service('ur_voucher.handler');
        $voucherParams = $voucherHandler->setVoucherParams($params);
        $voucher = $voucherHandler->processVoucher($voucherCode, $voucherParams, $estimate, $params, $totals);
        $voucherResponse = $voucher['voucherResponse'];
        $valid = $voucher['validity'];
        $new_params = $voucher['params'];

        if ($valid) {
          $estimate = DAL::Rental()->Estimates()->read($new_params);
        }
      }
    }

    $results = $estimate->export();

    /*
     * If coupon entered, return response in results
     */
    if (!empty($voucherResponse)) {
      $results['voucher_status'] = $valid;
      $results['voucher_msg'] = $voucherResponse;
      $results['voucher_orgamts'] = $this->getOriginalTotals($totals, $orgRentAmt);
    }

    $response->setData($results);

    return $response;
  }

  /**
   * Helper method to get item amounts on original estimate call.
   *
   * @param \Drupal\ur_appliance\Data\DAL\Estimate $estimate
   *   Estimate object.
   *
   * @return array
   *   Array of original item values for catClass and orgRentAmt.
   */
  public function getOriginalItemTotals(Estimate $estimate) {
    $orgItemValues = [];
    $items = $estimate->getItems();

    foreach ($items as $item) {
      $orgItemValues[$item->getCatClass()]['catClass'] = $item->getCatClass();
      $orgItemValues[$item->getCatClass()]['orgRentAmt'] = $item->getRentalAmount();
    }

    return $orgItemValues;
  }

  /**
   * Helper method for vouchers to find difference between old and new estimates.
   *
   * @params array $totals
   * @params array $voucherTotals
   *
   * @return array
   *   Returns array of total rent/pickup/delivery difference
   */
  public function getOriginalTotals($totals, $orgRentAmt) {
    $orgpuamt = $totals->totpuamt;
    $orgdelamt = $totals->totdelamt;

    return [
      'orgitemamt' => $orgRentAmt,
      'orgpuamt' => $orgpuamt,
      'orgdelamt' => $orgdelamt
    ];
  }

  /**
   * Retrieves individual reservation information through api-appliance.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The details of the individual reservartion
   *
   * @throws Exception
   */
  public function readReservationDetails() {
    $params = $this->paramsGetOrPost();
    $response = new JsonResponse();
    $result = NULL;

    try {
      $result = DAL::Rental(TRUE)->Transactions()->readDetails($params);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Gets one account's details.
   *
   * Post parameters:
   *   When sending multiple, you can send as an array or comma-separated string.
   *
   *   Possible fields/parameters:
   *     Field            - Required - Type     - Allow Multiple? - Description
   *     -----------------------------------------------------------------------
   *     accountId        - Yes      - Int      - Yes
   *     jobId            - No       - String   - Yes
   *     catClass         - No       - String   - Yes
   *     alias            - No       - String   - Yes - Customer assigned alias
   *     equipmentId      - No       - String   - Yes
   *     urEquipmentId    - No       - String   - Yes
   *     catalogGroup     - No       - String   - No
   *     description      - No       - String   - No
   *     includeDisabled  - No       - Bool     - No - Flag to include Disabled Equipment
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response from the customer/equipment API call or error message.
   *
   * @throws Exception
   */
  public function equipment() {
    $params = $this->paramsGetOrPost();
    $response = new JsonResponse();
    $result = NULL;

    try {
      $result = DAL::Customer(TRUE)->Equipment()->read($params);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves last year's worth of invoices through the rental/invoices API endpoint.
   *
   * We need to get the params including the accountId, then retrieves invoice data from DAL.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The exported equipment details or error message from api call.
   */
  public function getInvoices() {
    $params = $this->paramsGetOrPost();
    $response = new JsonResponse();
    $result = NULL;

    if (!isset($params['accountId'])) {
      $result = ['Missing required account ID.'];
    }
    else {
      try {
        $result = DAL::Rental(TRUE)->Invoices()->getInvoices($params);
      }
      catch (Exception $e) {
        $result = [$e->getMessage()];
      }
    }

    $response->setData($result);
    UrCache::set($params, $result, TRUE, UrCache::DAY);

    return $response;
  }

  /**
   * Retrieves equipment on rent through the rental/transactions API endpoint.
   *
   * Collects all rental transactions of type 1, 2, 3, 6.
   * These must be collected in three separate batches due to DAL restrictions
   * as shown in the $types array below.
   * So, we loop through and collect each batch then collate them
   * together to return a single bundle of all.
   * We also get the Cat-Classes on each transaction, then retrieve Drupal
   * data foreach piece of equipment based on Cat-Class.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The exported equipment details or error message from api call.
   */
  public function equipmentOnRent() {
    $params = $this->paramsGetOrPost();
    $response = new JsonResponse();
    $result = NULL;

    if (isset($params['transType'])) {
      $params['type'] = $params['transType'];
    }

    $skipCache = isset($params['skipCache']) ? (bool) $params['skipCache'] : FALSE;
    unset($params['skipCache']);

    // Get any preliminary checks out of the way first.
    if (!isset($params['accountId'])) {
      $result = ['Missing required account ID.'];
    }
    else {
      // Honor incoming $params['type'] where possible.
      // Explode on the comma to break request up into batches for asynchronous processing.
      // Else pull all the preferred types.
      if (isset($params['type'])) {
        $params['types'] = explode(',', $params['type']);
      }
      else {
        $params['types'] = ['1', '2', '3', '6'];
      }

      try {
        if (!$skipCache && $cache = UrCache::get($params, TRUE)) {
          $result = $cache->data;
        }
        else {
          $result = DAL::Rental()->Transactions()->readAllByItem(
            $params,
            $orderFields = [
              'leniencyStartDate',
              'leniencyEndDate',
              'branchId',
              'currency',
              'customerName',
              'simActive',
            ]
          );

          UrCache::set($params, $result, TRUE, UrCache::HOUR);

          if (!empty($result)) {
            $result = DAL::Customer(TRUE)->Equipment()->export($result);
          }
        }
      }
      catch (Exception $e) {
        $result = [$e->getMessage()];
      }
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Retrieves all equipment through several API calls.
   *
   * TODO: This will always timeout until we implement some sort of caching.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Retrieves all equipment (customer owned, and on rent) through a couple
   *   API calls and concatenates the responses together for ease of use.
   */
  public function allEquipment() {
    $params = $this->paramsGetOrPost();
    $response = new JsonResponse();
    $result = NULL;

    try {
      $result = DAL::Customer()->Equipment()->readCombined($params);

      if (!empty($result)) {
        /** @var \Drupal\ur_appliance\Data\DAL\Item $item */
        foreach ($result as $item) {
          $item->loadDrupalData();
        }

        $result = DAL::Customer(TRUE)->Equipment()->export($result);
      }
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Gets one's equipment GPS details.
   *
   * Sample call:
   *   curl -X GET 'https://kelex.local/api/v1/equipment/gps?accountId=858702&fromDate=20170901&toDate=20170910&includeHistory=Y&includeChildren=N'
   *
   *   Possible fields/parameters:
   *     Field            - Required - Type     - Allow Multiple? - Description
   *     -----------------------------------------------------------------------
   *     accountId        - Yes      - Int      - No
   *     equipmentId      - No       - String   - No
   *     fromDate         - No       - Int      - No  - Sample format: 20170915
   *     toDate           - No       - Int      - No  - Sample format: 20170928
   *     includeHistory   - No       - Bool     - No  - Flag to include history records
   *     includeChildren  - No       - Bool     - No  - Flag to include child accounts.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The GPS data for equipment or error message from API call.
   *
   * @throws \LogicException
   */
  public function equipmentGps() {
    $params = $this->paramsGetOrPost();
    $response = new JsonResponse();
    $result = NULL;

    // Default it to include usage history.
    if (empty($params['includeHistory'])) {
      $params['includeHistory'] = 'Y';
    }

    // Default to not include child account results.
    if (empty($params['includeChildren'])) {
      $params['includeChildren'] = 'N';
    }

    try {
      $result = DAL::Equipment(TRUE)->Gps()->read($params);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Validates the pin number to the account and links the account to the TcUser.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   True or false if we were able to validate and link the account.
   */
  public function linkAccount() {
    $params = $this->paramsGetOrPost();
    $response = new JsonResponse();
    $result = NULL;

    if (empty($params['accountId']) || empty($params['pin'])) {
      $result = ['Account Id and Pin required.'];
    }
    else {
      try {
        $result = DAL::Customer()->Account()->validateAndLink($params);
        $userId = \Drupal::currentUser()->id();
        $cid = "UID:$userId|DalController|accounts|";
        \Drupal::cache()->invalidate($cid);
      }
      catch (Exception $e) {
        $result = [$e->getMessage()];
      }
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Deletes specified account(s) associated with the TcUser.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   True or false if we were able to delete the account.
   */
  public function deleteAccounts() {
    $params = $this->paramsGetOrPost();
    $response = new JsonResponse();
    $result = NULL;

    if (empty($params['accountId'])) {
      $result = ['Account ID can not be blank.'];
    }
    else {
      try {
        $result = DAL::Customer()->Account()->deleteAccounts($params);
      }
      catch (Exception $e) {
        $result = [$e->getMessage()];
      }
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Gets the sum of invoices outstanding.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   array - ['total' => number, 'more' => 'bool]
   */
  public function totalInvoicesOutstanding() {
    $params = $this->paramsGetOrPost();
    $response = new JsonResponse();
    $result = NULL;

    try {
      $result = DAL::Rental(TRUE)->Invoices()->outstandingInvoices($params);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    UrCache::set($params, $result, TRUE, UrCache::DAY);

    return $response;
  }

  /**
   * Returns Pickup Requests for the supplied params.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns Pickup Request objects from DAL.
   */
  public function pickupRequest() {
    $params = $this->paramsGetOrPost();
    $response = new JsonResponse();
    $result = NULL;

    try {
      $result = DAL::Rental(TRUE)->PickupRequests()->post($params);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Returns Order Comments.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns the Order Comments object from the DAL.
   */
  public function orderComments() {
    $params = $this->paramsGetOrPost();
    $response = new JsonResponse();
    $result = NULL;

    try {
      $result = DAL::Order(TRUE)->Comment()->read($params);
    }
    catch (Exception $e) {
      $result = [$e->getMessage()];
    }

    $response->setData($result);
    return $response;
  }

  /**
   * Validates the params supplied by GET.
   *
   * @param array $params
   *   The params array supplied via http.
   *
   * @return array
   *   Returns the valid params.
   */
  private function validateGetParams(array $params) {
    $notEmptyParams = [];

    foreach ($params as $key => $param) {
      if (strlen(trim((string) $param)) > 0) {
        $notEmptyParams[$key] = $param;
      }
    }

    return $notEmptyParams;
  }

  /**
   * Returns the nearby Branch Offices.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns an array of Branch objects that fit the params.
   */
  public function getNearbyBranch() {
    $params = $this->request->getCurrentRequest()->query->all();

    $params['status'] = 'A';
    $params['distance'] = 0;
    $params['unit'] = 'M';
    $params['min'] = 1;
    $params['types'] = 'GR,AR';
    $params['webDisplay'] = 'true';
    $params['limit'] = 1;
    $params['onlineOrders'] = 'true';
    $result = NULL;

    try {
      $resultArr = DAL::Service(TRUE)->Branch()->readDistances($params);
      $result = $resultArr[0];

      // To maintain consistency with other parts of the DAL change to 'branchId' from 'id'.
      $result['branchId'] = $result['id'];
      unset($result['id']);
    }
    catch (Exception $e) {
      $result['message'] = $e->getMessage();
    }

    return new JsonResponse($result);
  }

  /**
   * Returns results counts.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON response.
   */
  public function counts() {
    $response = new JsonResponse();
    $params = $this->paramsGetOrPost();
    $result = NULL;
    $id = NULL;

    try {
      if (empty($params['accountId'])) {
        throw new Exception('No account ID passed.');
      }
      $result = DAL::Rental(TRUE)->Counts()->read($params);
    }
    catch (Exception $e) {
      $result = $e->getMessage();
    }

    $response->setData($result);

    return $response;
  }

  /**
   * Creates TC User, updates the account, and then finally retrieves SSO token.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response for created and signed in user.
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function createTcUserAndSignIn() {
    $data = $this->paramsGetOrPost();
    $code = 200;
    $response = [
      'message' => '',
      'error' => FALSE,
    ];

    // Check if password will be correct before we create the user.
    if (isset($data['password']) && !empty($data['password'])) {
      /*
       * Ensure password is:
       * - 8 characters
       * - Contains at least 1 uppercase
       * - Contains at least 1 number
       */
      $pattern = '/^\S*(?=\S{8,})(?=\S*[A-Z])(?=\S*[\d])\S*$/';
      $required = preg_match($pattern, $data['password']);

      if (!$required) {
        $response['message'] = 'Password must contain an uppercase, a number and be at least 8 characters long.';
        $response['error'] = TRUE;
        return new JsonResponse($response, 400);
      }
    }
    else {
      $response['message'] = 'Password is required to create a new AD Account';
      $response['error'] = TRUE;
      return new JsonResponse($response, 400);
    }

    // Create guest's TC User, this will create the AD account with a randomly
    // generated password by DAL.
    try {
      $guid = DAL::Customer()->TcUser()->create($data);
      $_SESSION['ur_guid'] = $guid;
      $response['guid'] = $guid;
    }
    catch (DalException $e) {
      $response['message'] = $e->getMessage();
      $response['error'] = TRUE;
      return new JsonResponse($response, 400);
    }

    // Get SSO user token from new TC User.
    try {
      DAL::Service()->getAdapter()->getUserToken($data['email']);
    }
    catch (DalException $e) {
      $response['message'] = $e->getMessage();
      $response['error'] = TRUE;
      return new JsonResponse($response, 400);
    }

    // Now update the active directory (AD) account with the supplied password.
    $ad_data = [
      'id' => $data['email'],
      'password' => $data['password'],
    ];

    try {
      $success = DAL::Customer()->Account()->updateAdAccount($ad_data);

      if ($success == FALSE) {
        $response['message'] = 'Unable to update AD Account.';
        $response['error'] = TRUE;
        $code = 400;
        return new JsonResponse($response, $code);
      }
    }
    catch (Exception $e) {
      $response['message'] = $e->getMessage();
      $response['error'] = TRUE;
      $code = 400;
      return new JsonResponse($response, $code);
    }

    // Now at this step the TC User should exist with the supplied password.
    // Create the Drupal user and log them in.
    $user = $this->createUser($data['email'], $data['password'], $guid);
    // Now login the new user.
    user_login_finalize($user);

    // Next page refresh the user should be logged in.
    return new JsonResponse($response, $code);
  }

  /**
   * Creates a user.
   *
   * @param mixed $email
   *   The user's email.
   * @param mixed $password
   *   The user's password.
   * @param mixed $guid
   *   The user's guid.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\user\Entity\User
   *   The created user object.
   */
  private function createUser($email, $password, $guid) {
    $user = User::create();
    $user->setEmail($email);
    $user->setUsername($email);
    $user->setPassword($password);
    $user->enforceIsNew();
    $user->activate();
    $user->set('init', $email);

    try {
      $user->save();
    }
    catch (EntityStorageException $e) {
      \Drupal::logger('user')->notice('Failed creating user during checkout: %s', ['%s' => $e->getMessage()]);
    }

    /* @var \Drupal\externalauth\ExternalAuth  $externalauth */
    $externalauth = \Drupal::service('externalauth.externalauth');
    $externalauth->linkExistingAccount($guid, 'simplesamlphp_auth', $user);

    \Drupal::logger('user')->notice('New user %email created during checkout', ['%email' => $user->getUsername()]);

    // Now return the user.
    return $user;
  }

  /**
   * Updates an AD Account.
   */
  public function updateAdAccount() {
    $data = $this->paramsGetOrPost();
    $error = FALSE;
    $message = 'Success';
    $code = 200;

    try {
      $success = DAL::Customer()->Account()->updateAdAccount($data);

      if ($success == FALSE) {
        $error = TRUE;
        $message = 'Unable to update AD account.';
        $code = 400;
      }
    }
    catch (Exception $e) {
      $error = TRUE;
      $message = $e->getMessage();
      $code = 400;
    }

    return new JsonResponse(['message' => $message, 'error' => $error], $code);
  }

}
