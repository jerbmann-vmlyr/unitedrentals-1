<?php

namespace Drupal\ur_frontend_api\Controller;

use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Controller\ControllerBase;
use Drupal\ur_frontend_api\Traits\ApiTrait;
use Symfony\Component\HttpFoundation\JsonResponse;


class CatalogController extends ControllerBase {

  use ApiTrait;

  public function getPopularTypes() {

    $cid = 'apiPopularEquipmentTypes';
    if ($cache = \Drupal::cache()->get($cid)) {
      return $cache->data;
    }

    $ids = \Drupal::entityQuery('taxonomy_term')
      ->condition('field_popular', 1)
      ->execute();
    $terms = Term::loadMultiple($ids);

    $output = [];

    foreach ($terms as $term) {
      $termImage = NULL;
      if (isset($term->get('field_image')->entity->url)
        && !empty($term->get('field_image')->entity->url)
      ) {
        $termImage = $term->get('field_image')->entity->url->getString();
      }
      $record = [
        'name' => $term->getName(),
        'image' => $termImage,
        'weight' => $term->get('field_weight')->getString(),
        'description' => $term->get('description')->getString(),
        'url' => $term->toUrl()->toString(),
      ];

      $output[] = $record;
    }

    usort($output, function ($a, $b) {
      return $a['weight'] > $b['weight'];
    });

    $response = new JsonResponse();
    $response->setData($output);

    // Cache the response for faster subsequent queries.
    $expireIn24Hours = time() + (1 * 24 * 60 * 60);
    \Drupal::cache()->set($cid, $response, $expireIn24Hours);

    return $response;
  }

}
