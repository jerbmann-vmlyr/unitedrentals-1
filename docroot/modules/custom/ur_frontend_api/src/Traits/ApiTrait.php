<?php

namespace Drupal\ur_frontend_api\Traits;

/**
 * ApiTrait.
 */
trait ApiTrait {

  /**
   * {@inheritdoc}
   */
  protected function getRequest() {
    return \Drupal::request();
  }

  /**
   * {@inheritdoc}
   * Get URL parameters based on if they are coming in via POST or GET request
   * methods.
   *
   * @throws \LogicException
   */
  public function paramsGetOrPost() {
    // Load from POST.
    $params = $this->getRequest()->getContent();
    $params = json_decode($params);
    $params = (array) $params;

    // If nothing, there we assume it's a GET.
    if (empty($params)) {
      $params = $this->getRequest()->query->all();
    }

    return $params;
  }

}
