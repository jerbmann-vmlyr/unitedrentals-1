<?php

namespace Drupal\ur_frontend_api;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * KelexResponse.
 */
class KelexResponse extends JsonResponse {

  protected $messages;
  protected $success;
  protected $response;

  /**
   * {@inheritdoc}
   */
  public function __construct($data = NULL, $status = 200, array $headers = []) {
    parent::__construct($data, $status, $headers);

    /** @var bool success */
    $this->success = TRUE;
    $this->messages = [];
  }

  /**
   * {@inheritdoc}
   */
  public function wasSuccessful() {
    return $this->success;
  }

  /**
   * {@inheritdoc}
   */
  public function setSuccess($status) {
    $this->success = $status;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessages() {
    return $this->messages;
  }

  /**
   * {@inheritdoc}
   */
  public function setMessages($messages) {
    if (!is_array($messages)) {
      throw new \InvalidArgumentException("messages must be an array");
    }

    $this->messages = $messages;
  }

  /**
   * @TODO: Implement a way to validate we recieved correct params
   * @param $data
   */
  public function formatResponse($data) {
    $this->setData(
      [
        "success" => $this->wasSuccessful(),
        "messages" => $this->getMessages(),
        "response" => $data,
      ]
    );
  }

}
