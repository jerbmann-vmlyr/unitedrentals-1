<?php

namespace Drupal\ur_favorites\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Lock\NullLockBackend;
use Drupal\Core\Session\AccountInterface;
use Drupal\flag\FlagService;
use Drupal\Core\Controller\ControllerBase;
use Drupal\ur_service_layer\Entity\Item;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The favorites controller.
 *
 * Class FavoritesController.
 *
 * @package Drupal\ur_favorites\Controller
 */
class FavoritesController extends ControllerBase {

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * EntityTypeManger definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal flag service.
   *
   * @var \Drupal\flag\FlagService
   */
  protected $flagService;

  /**
   * Flag Id definition.
   *
   * @var string
   */
  protected $flagId = 'favorites';

  /**
   * {@inheritdoc}
   */
  public function __construct(FlagService $flag_service, RequestStack $request, EntityTypeManager $entity_type_manager) {
    $this->flagService = $flag_service;
    $this->request = $request;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('flag'),
      $container->get('request_stack'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * The view favorites action endpoint.
   *
   * @return array
   *   Returns the view for this action.
   */
  public function viewFavorites() {
    $build = [
      '#theme' => 'favorites_display',
      '#type' => 'markup',
      '#attached' => [
        'drupalSettings' => [
          'vueRouter' => TRUE,
        ],
      ],
    ];
    return $build;
  }

  /**
   * Retrieve all flagged items for a given user.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The favorites json endpoint.
   */
  public function retrieveFavorites($json = TRUE) {
    $data = $this->buildFavorites();

    // Add to SESSION.
    if (isset($data['favorites'])) {
      $_SESSION['favorite_equipment'] = $data['favorites'];
    }

    // If not JSON, return the data response.
    if (!$json) {
      return $data;
    }

    // Build JsonResponse instead.
    $response = new JsonResponse();
    $response->setData($data);

    return $response;
  }

  /**
   * Flag an individual item for a user by cat-class.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Favorite items endpoint.
   */
  public function favoriteItem() {
    $response = new JsonResponse();

    $query = $this->request->getCurrentRequest()->get('cat-class');
    $cat_class = Xss::filter($query);
    if (empty($cat_class)) {
      $data = 'Cannot favorite an item without a cat-class';
      $response->setData($data);
      return $response;
    }

    $flag = $this->flagService->getFlagById($this->flagId);

    /** @var \Drupal\ur_service_layer\Entity\Item $item */
    $item = \Drupal::service('ur_service_layer.entity_item');
    $entity = $item->findItembyCatClass($cat_class);
    $this->flagService->flag($flag, $entity, $this->currentUser());

    // After flagging the item, rebuild the list for the frontend.
    $data = $this->buildFavorites();

    $response->setData($data);

    return $response;
  }

  /**
   * Unflag an individual item for a user by cat-class.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The unfavorite item json response.
   */
  public function unfavoriteItem() {
    $response = new JsonResponse();

    $query = $this->request->getCurrentRequest()->get('cat-class');
    $cat_class = Xss::filter($query);

    if (empty($cat_class)) {
      $data['message'] = 'Cannot unfavorite an item without a cat-class';
      $response->setData($data);
      return $response;
    }

    $flag = $this->flagService->getFlagById($this->flagId);

    /** @var \Drupal\ur_service_layer\Entity\Item $item */
    $item = \Drupal::service('ur_service_layer.entity_item');

    $entity = $item->findItembyCatClass($cat_class);

    if ($entity !== FALSE) {
      $this->flagService->unflag($flag, $entity, $this->currentUser());
      // After unflagging the item, rebuild the list for the frontend.
      $data = $this->buildFavorites();

      $response->setData($data);
    }

    return $response;
  }

  /**
   * Helper method to retrieve the array of Cat-Class favorites.
   *
   * @return array
   *   An array of favorite cat-class codes.
   */
  protected function buildFavorites() {
    $data = [];
    if (!isset($data['favorites'])) {
      $data['favorites'] = [];
      $flagged_content = $this->getUserFlag('node', $this->currentUser());

      foreach ($flagged_content as $flag) {
        // @var \Drupal\Core\Entity\EntityInterface
        $entity = $this->entityTypeManager->getStorage('node')->load($flag->entity_id);
        $cat_class = $entity->get('field_item_cat_class_code')->getValue();
        $cat_class = $cat_class[0]['value'];
        $data['favorites'][] = $cat_class;
      }
    }
    return $data;
  }

  /**
   * Lots of functionality stolen from the D7 implementation.
   *
   * This is implementation of flag_get_user_flags() (Amazed this isn't in D8 flag module anywhere).
   *
   * See documentation: http://www.drupalcontrib.org/api/drupal/contributions!flag!flag.module/function/flag_get_user_flags/7
   *
   * Gets all content that has been flagged by a specific user.
   *
   * @param string $entity_type
   *   The entity type we're getting.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   * @param null|string $session_id
   *   The session id.
   */
  protected function getUserFlag($entity_type, AccountInterface $account, $session_id = NULL) {
    $flagged_content = &drupal_static(__FUNCTION__);

    $uid = !isset($uid) ? $account->id() : 0;

    if (!isset($flagged_content)) {
      $flagged_content = [];

      // TODO: Make this query more "Drupal-y".
      $result = db_select('flagging', 'fc')
        ->fields('fc')
        ->condition('entity_type', $entity_type)
        ->condition('uid', $uid)
        ->execute();
      foreach ($result as $flagging_data) {
        $flagged_content[] = $flagging_data;
      }
    }
    return $flagged_content;

  }

}
