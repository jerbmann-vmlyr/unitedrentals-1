<?php

namespace Drupal\ur_release\Plugin\Block;

use Drupal\ur_release\ReleaseInformation;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'UR Release' Block.
 *
 * @Block(
 *   id = "ur_release_block",
 *   admin_label = @Translation("UR Release block"),
 *   category = @Translation("UR"),
 * )
 */
class ReleaseBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $releaseInformation = new ReleaseInformation();
    $env = $releaseInformation->deployedEnv();
    $displayEnv = ($env === '{$_SERVER["AH_SITE_ENVIRONMENT"]}') ? '( local dev )' : $releaseInformation->deployedEnvTranslation($env);
    $tag = empty($_SERVER["AH_SITE_ENVIRONMENT"]) ? $releaseInformation->currentGitBranch() : $releaseInformation->deployedTag($env) ?: t('( none )');
    $label = empty($_SERVER["AH_SITE_ENVIRONMENT"]) ? 'Local Branch' : 'Release Tag';

    return [
      '#theme' => 'ur_release_block_template',
      '#attached' => [
        'library' => [
          'ur_release/ur_release_block',
        ],
      ],
      '#release_tag' => $tag,
      '#current_env' => $displayEnv,
      '#release_label' => $label,
    ];
  }

}
