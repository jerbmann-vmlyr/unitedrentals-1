<?php

namespace Drupal\ur_release;


use Drupal\Core\Cache\NullBackend;

class ReleaseInformation {
  /**
   * Gets the contents of the deployed_tag file and returns it as block content.
   *
   * @param string $env
   *   The name of the deployed env.
   *
   * @return string
   *   Returns '(unknown)' if it can't access the deployed_tag file.
   */
  public function deployedTag($env) {
    $tag = @file_get_contents($this->deployedTagPath($env));
    return $tag ?: '(unknown)';
  }

  /**
   * Returns the computed env.
   *
   * @return string
   *   Env name.
   */
  public function deployedEnv() {
    if (empty($_SERVER['AH_SITE_ENVIRONMENT'])) {
      $env = '{$_SERVER["AH_SITE_ENVIRONMENT"]}';
    }
    else {
      $env = $_SERVER['AH_SITE_ENVIRONMENT'];
    }

    return $env;
  }

  /**
   * Returns the computed path to the deployed_tag file.
   *
   * @param string $env
   *   The name of the deployed env.
   *
   * @return string
   *   Server path string.
   */
  public function deployedTagPath($env) {
    if (!isset($_ENV['HOME'])) {
      $_ENV['HOME'] = '';
    }

    return $_ENV['HOME'] . '/' . $env . '/deployed_tag';
  }

  /**
   * Returns a "translated" server name based on new server structure.
   *
   * @param $env
   *   The Acquia environment name provided by Acquia.
   *
   * @return string
   *   New Acquia env name.
   */
  public function deployedEnvTranslation($env) {
    $acquiaEnvironmentNames = [
      'dev0' => 'Dev0',
      'dev' => 'Dev1',
      'dev2' => 'Dev2',
      'dev3' => 'Dev3',
      'qa' => 'Qa1',
      'qa2' => 'Qa2',
      'test' => 'Stage',
      'test2' => 'Sandbox',
      'prod' => 'Prod',
      'prod2' => 'Preprod',
    ];

    return $acquiaEnvironmentNames[$env];
  }

  /**
   * Returns locally checked out git branch.
   *
   * @return null|string
   *   Branch name.
   */
  public function currentGitBranch() {
    $shellOutput = [];
    exec('git branch | ' . "grep ' * '", $shellOutput);
    foreach ($shellOutput as $line) {
      if (strpos($line, '* ') !== FALSE) {
        return trim(strtolower(str_replace('* ', '', $line)));
      }
    }

    return NULL;
  }

}
