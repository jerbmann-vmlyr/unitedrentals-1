var egainDockChat = egainDockChat || {};
egainDockChat.Template = 'United_Rentals';
egainDockChat.VAName = 'UR';

if(!egainDockChat.launchChat){
  egainDockChat.CallQueue = egainDockChat.CallQueue || [];
  egainDockChat.CallQueue.push({name:'launchChat', args:[]});
}else{
  egainDockChat.launchChat();
}