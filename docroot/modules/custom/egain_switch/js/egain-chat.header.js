/* eslint-disable */

/*
   VML Notes:
   This script must be loaded in the HEAD.
   EGain Chat will only work in one environment.
   At the time of its introduction, EGain is being configured
   to run in Stage, then when ready, Prod. Once in Prod, do not
   expect it to work in any other environment.
*/

(function($, Drupal) {
  Drupal.behaviors.egain_activate = {
    attach: function (context, settings) {
      var EG_ACT_ID = settings.EG_ACT_ID;
      (function(e,f)
      {var d,c,b,a=e.createElement("iframe");
      a.src="about:blank";
      a.title="egot iframe";
      a.id="egot_iframe";
      (a.frameElement||a).style.cssText="width:0;height:0;border:0;position:fixed";
      b=e.getElementsByTagName("script");
      b=b[b.length-1];b.parentNode.insertBefore(a,b);
      try{c=a.contentWindow.document}
      catch(g){d=e.domain,a.src="javascript:var d=document.open();d.domain='"+d+"';void(0);",
        c=a.contentWindow.document}c.open()._d=function(){var a=this.createElement("script");
        d&&(this.domain=d);a.src=f;this.isEGFIF= !0;this.body.appendChild(a)};
      c.write('<body onload="document._d();">');
      c.close()})(document,"//analytics.analytics-egain.com/onetag/"+EG_ACT_ID);
    }
  };
})(jQuery, Drupal);
