var egainDockChat = egainDockChat || {};
egainDockChat.Template = 'United_Rentals_Stage';
egainDockChat.VAName = 'UR';

if(!egainDockChat.launchChat){
  egainDockChat.CallQueue = egainDockChat.CallQueue || [];
  egainDockChat.CallQueue.push({name:'launchChat', args:[]});
}else{
  egainDockChat.launchChat();
}
