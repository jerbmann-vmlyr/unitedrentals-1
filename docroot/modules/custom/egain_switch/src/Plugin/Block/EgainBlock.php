<?php

namespace Drupal\egain_switch\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'EgainBlock' block.
 *
 * @Block(
 *  id = "egain_block",
 *  admin_label = @Translation("Egain Block"),
 * )
 */
class EgainBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];

    // Disable egain entirely for locations pages.
    $current_path = \Drupal::service('path.current')->getPath();
    if (strpos($current_path, 'locations') !== FALSE) {
      return $build;
    }

    /*
     * Makes use of Acquia's environment variables. If we're on production, add
     * the production version of the library for egain.
     * Else, add the stage library.
     */
    if (isset($_ENV['AH_SITE_ENVIRONMENT'])) {
      // Acquia environment variable exists.
      if ($_ENV['AH_SITE_ENVIRONMENT'] === 'prod') {
        // We're on prod, use prod library.
        $build['#attached']['library'][] = 'egain_switch/egain_chat_prod';
        // @TODO: This should be a config setting.
        $eg_act_id = 'EG62179745';
        $build['#attached']['drupalSettings']['EG_ACT_ID'] = $eg_act_id;
      }
      else {
        // We're not on prod, use stage library.
        $build['#attached']['library'][] = 'egain_switch/egain_chat_stage';
        // @TODO: This should be a config setting.
        $eg_act_id = 'EG28766776';
        $build['#attached']['drupalSettings']['EG_ACT_ID'] = $eg_act_id;
      }
    }
    else {
      // We're not in Acquia, continue to load the stage library.
      $build['#attached']['library'][] = 'egain_switch/egain_chat_stage';
      // @TODO: This should be a config setting.
      $eg_act_id = 'EG28766776';
      $build['#attached']['drupalSettings']['EG_ACT_ID'] = $eg_act_id;
    }
    return $build;
  }

}
