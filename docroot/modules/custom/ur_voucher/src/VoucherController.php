<?php

namespace Drupal\ur_voucher;

use Voucherify\VoucherifyClient;
use Voucherify\ClientException;
use Drupal\ur_appliance\Data\DAL\Estimate;
use Drupal\ur_appliance\Data\DAL\Order;

/**
 * Class DefaultController.
 *
 * @package Drupal\ur_voucher
 */
class VoucherController {

  /**
   * Validate voucher code sent.
   *
   * @param string $code
   *   Voucher code provided by customers.
   * @param array $voucherParams
   *   Voucher parameters.
   *
   * @return object
   *   Returns voucher object and whether or not it is valid
   */
  public function validateCode($code, $voucherParams) {
    $client = $this->createVoucherClient();

    // Validate customer entered code.
    try {
      $status = $client->validations->validate($code, $voucherParams);
    }
    catch (ClientException $e) {
      $error = $e->getError();
      return $error;
    }

    return $status;
  }

  /**
   * Get the details of the Voucher so we know what line item to apply discount to.
   *
   * @param string $code
   *   Voucher code provided by customer.
   *
   * @return mixed
   *   Returns voucher details including amount/percent to subtract from transaction
   */
  public function getVoucherDetails($code) {
    $client = $this->createVoucherClient();
    $voucherDetails = $client->vouchers->get($code);

    // TODO: This is NOT good, need to find a better way to access the object.
    $conditions = $voucherDetails->validation_rules->skus->conditions;
    $convert_conditions = (object) $conditions;
    $array_conditions = (array) $convert_conditions;
    $voucherType = $array_conditions['$is'][0]->source_id;

    return $voucherType;
  }

  /**
   * Validate voucher code sent.
   *
   * @param string $voucherCode
   *   Voucher code provided by customer.
   * @param array $voucherParams
   *   Voucher parameters.
   * @param \Drupal\ur_appliance\Data\DAL\Estimate $estimate
   *   Original charge estimates object to be used in calculations.
   * @param array $params
   *   Parameters in original charge estimates call.
   * @param object $totals
   *   Returned totals from original estimates call.
   *
   * @return array
   *   Returns array of Voucher response, validity of voucher, updated params for estimates call
   */
  public function processVoucher($voucherCode, $voucherParams, Estimate $estimate, $params, $totals) {
    $voucher = $this->validateCode($voucherCode, $voucherParams);
    $valid = $voucher->valid;

    /*
     * Stop process if API ID/Key fails
     */
    if ($voucher->code == 401) {
      return [
        'voucherResponse' => 'Voucher Authentication Issue. Please try again later',
        'validity' => FALSE,
        'params' => $params
      ];
    }

    if (!$valid) {
      // Set reason in response.
      $reason = $voucher->reason;
      $voucherResponse = ['status' => $reason];
      $updated_params = $params;
    }
    else {
      $voucherType = $this->getVoucherDetails($voucherCode);
      $discount_type = $voucher->discount->type;
      $discount = $this->formatDiscountNumber($voucher, $discount_type);

      // TODO: Switch cases with actual sku codes.
      switch ($voucherType) {
        case 'pickup':
          if ($params['pickup'] == 'Y') {
            $updated_params = $this->applyPickupDiscount($discount, $discount_type, $params, $totals);
          }
          if ($params['delivery'] == 'Y') {
            $updated_params = $this->applyDeliveryDiscount($discount, $discount_type, $updated_params, $totals);
          }
          break;

        case 'rates':
          $updated_params = $this->applyRatesDiscount($discount, $discount_type, $params, $estimate);
          break;

        case 'all':
          $updated_params = $this->applyRatesDiscount($discount, $discount_type, $params, $estimate);
          if ($params['pickup'] == 'Y') {
            $updated_params = $this->applyPickupDiscount($discount, $discount_type, $updated_params, $totals);
          }
          if ($params['delivery'] == 'Y') {
            $updated_params = $this->applyDeliveryDiscount($discount, $discount_type, $updated_params, $totals);
          }
          break;
      }

      // Set response to success.
      $voucherResponse = [
        'status' => 'Success!',
        'discount_type' => $discount_type,
        'amount' => $discount
      ];
    }

    return [
      'voucherResponse' => $voucherResponse,
      'validity' => $valid,
      'params' => $updated_params
    ];
  }

  /**
   * Apply discount to item rates.
   *
   * @param int $discount
   *   Amount of discount.
   * @param string $discount_type
   *   Discount type (dollar amount vs. percent)
   * @param array $params
   *   Parameters from original estimates call.
   * @param \Drupal\ur_appliance\Data\DAL\Estimate $estimate
   *   Estimate object from original estimates call.
   *
   * @return mixed
   *   Return updated parameters for new estimates call
   */
  public function applyRatesDiscount($discount, $discount_type, $params, Estimate $estimate) {
    $items = $estimate->getItems();
    $rates_quantities = [];

    foreach ($items as $item) {
      $item_details = [];
      $catClass = $item->getCatClass();
      $item_details['catClass'] = $catClass;
      $item_details['qty'] = $item->getQuantity();
      $minRate = $item->getRate()->getMinRate();
      $dayRate = $item->getRate()->getDayRate();
      $weekRate = $item->getRate()->getWeekRate();
      $monthRate = $item->getRate()->getMonthRate();

      /*
       * Now set new parameters for the estimates call
       */
      switch ($discount_type) {
        /*
         * The AMOUNT case should never happen. It is included only as a safeguard in case
         * a rates voucher is created as an amount discount instead of a percent discount.
         */
        case 'AMOUNT':
          $item_details['minRate'] = $minRate - $discount;
          $item_details['dayRate'] = $dayRate - $discount;
          $item_details['weekRate'] = $weekRate - $discount;
          $item_details['monthRate'] = $monthRate - $discount;
          break;

        case 'PERCENT':
          $item_details['minRate'] = $minRate - ($minRate * $discount);
          $item_details['dayRate'] = $dayRate - ($dayRate * $discount);
          $item_details['weekRate'] = $weekRate - ($weekRate * $discount);
          $item_details['monthRate'] = $monthRate - ($monthRate * $discount);
          break;
      }

      $rates_quantities[] = $item_details;
    }

    $params['catClassQtyRates'] = $rates_quantities;
    $params['webRates'] = FALSE;

    return $params;
  }

  /**
   * Apply delivery discount.
   *
   * @param int $discount
   *   Discount amount to apply.
   * @param string $discount_type
   *   Type of discount (dollar amount vs. percentage)
   * @param array $params
   *   Parameters of original estimates call.
   * @param array $totals
   *   Returned totals from original estimates call.
   *
   * @return mixed
   *   Return parameters to use in updated estimates call
   */
  public function applyDeliveryDiscount($discount, $discount_type, $params, $totals) {

    switch ($discount_type) {
      case 'AMOUNT':
        $split_discount = $discount / 2;
        $updated_delivery = $totals->totdelamt - $split_discount;
        break;

      case 'PERCENT':
        $updated_delivery = $totals->totdelamt - ($totals->totdelamt * $discount);
        break;
    }

    $params['deliveryCost'] = $updated_delivery;
    $params['costOverride'] = 'Y';

    return $params;
  }

  /**
   * Apply pickup discount.
   *
   * @param int $discount
   *   Discount amount to apply.
   * @param string $discount_type
   *   Type of discount (dollar amount vs. percentage)
   * @param array $params
   *   Parameters of original estimates call.
   * @param array $totals
   *   Returned totals from original estimates call.
   *
   * @return mixed
   *   Return parameters to use in updated estimates call
   */
  public function applyPickupDiscount($discount, $discount_type, $params, $totals) {

    switch ($discount_type) {
      case 'AMOUNT':
        $split_discount = $discount / 2;
        $updated_picked = $totals->totpuamt - $split_discount;
        break;

      case 'PERCENT':
        $updated_picked = $totals->totpuamt - ($totals->totpuamt * $discount);
        break;
    }

    $params['pickupCost'] = $updated_picked;
    $params['costOverride'] = 'Y';

    return $params;
  }

  /**
   * Redeem voucher code.
   *
   * @param string $voucherCode
   *   Voucher code provided by customer.
   * @param array $voucherParams
   *   Voucher parameters.
   *
   * @return object
   *   Returns redeemed voucher object
   */
  public function redeemCode($voucherCode, $voucherParams) {
    $client = $this->createVoucherClient();

    try {
      $redeemed = $client->redemptions->redeem($voucherCode, $voucherParams);
    }
    catch (ClientException $e) {
      $error = $e->getError();
      return $error;
    }

    return $redeemed;
  }

  /**
   * Format as dollar amount or % depending on discount type.
   *
   * @params object $voucher
   *  Voucher object
   * @params string $discount_type
   *  Discount type (dollar amount vs. percentage)
   *
   * @return int
   *   Returns formatted number
   */
  public function formatDiscountNumber($voucher, $discount_type) {
    switch ($discount_type) {
      case 'AMOUNT':
        $discount = number_format($voucher->discount->amount_off / 100, 2);
        break;

      case 'PERCENT':
        $discount = number_format($voucher->discount->percent_off / 100, 2);
        break;
    }

    return $discount;
  }

  /**
   * Set hardcoded voucher parameters.
   *
   * @param array $params
   *   Parameters from original estimates call.
   * @param \Drupal\ur_appliance\Data\DAL\Order $order
   *   Order object from submit transaction call.
   *
   * @return array
   *   Returns Voucher parameters to check validity
   */
  public function setVoucherParams(array $params, Order $order = NULL) {
    $items = [];
    $items[] = ['sku_id' => 'rates', 'quantity' => 1];
    $items[] = ['sku_id' => 'all', 'quantity' => 1];
    // TODO: Replace sku_id with actual product values.
    if ($params['pickup'] == 'Y' || $params['pickup'] === TRUE) {
      $items[] = ['sku_id' => 'pickup', 'quantity' => 1];
    }

    if ($params['delivery'] == 'Y' || $params['delivery'] === TRUE) {
      $items[] = ['sku_id' => 'delivery', 'quantity' => 1];
    }

    if ($order) {
      $metadata = ['reqId' => $order->getRequisitionId()];
      $voucherParams = ['metadata' => $metadata, 'order' => ['items' => $items]];
    }
    else {
      $voucherParams = ['order' => ['items' => $items]];
    }

    return $voucherParams;
  }

  /**
   * Create VoucherClient with apiId and apiKey.
   *
   * @return object
   *   Returns VoucherifyClient object
   */
  public function createVoucherClient() {
    $config = $config = \Drupal::config('ur_voucher.settings');
    $apiID = $config->get('apiId');
    $apiKey = $config->get('apiKey');
    $apiVersion = $config->get('apiVersion') ?? NULL;
    $apiUrl = $config->get('apiUrl');
    $client = new VoucherifyClient($apiID, $apiKey, $apiVersion, $apiUrl);

    return $client;
  }

}
