<?php

namespace Drupal\ur_voucher\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VoucherifySettingsForm.
 *
 * @package Drupal\ur_voucher\Form
 */
class VoucherifySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'voucherify_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_voucher.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_voucher.settings');

    $form['apiId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Voucherify API ID'),
      '#description' => $this->t('The Voucherify API ID (obtain from the Voucherify Dashboard)'),
      '#maxlength' => 120,
      '#size' => 64,
      '#default_value' => $config->get('apiId'),
      '#required' => TRUE,
    ];

    $form['apiKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Voucherify API Key'),
      '#description' => $this->t('The Voucherify API secret key (obtain from the Voucherify Dashboard)'),
      '#maxlength' => 120,
      '#size' => 64,
      '#default_value' => $config->get('apiKey'),
      '#required' => TRUE,
    ];

    $form['apiVersion'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Voucherify API Version'),
      '#description' => $this->t('The Voucherify API version (leave blank to use latest version)'),
      '#maxlength' => 120,
      '#size' => 64,
      '#default_value' => $config->get('apiVersion'),
      '#required' => FALSE,
    ];

    $form['apiUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Voucherify API Url'),
      '#description' => $this->t('The Voucherify API base endpoint url'),
      '#maxlength' => 120,
      '#size' => 64,
      '#default_value' => $config->get('apiUrl'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_voucher.settings');

    $config
      ->set('apiId', $form_state->getValue('apiId'))
      ->save();

    $config
      ->set('apiKey', $form_state->getValue('apiKey'))
      ->save();

    $config
      ->set('apiVersion', $form_state->getValue('apiVersion'))
      ->save();

    $config
      ->set('apiUrl', $form_state->getValue('apiUrl'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
