<?php

namespace Drupal\ur_activity_preferences\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the activity preferences module.
 */
class ActivityPreferencesController extends ControllerBase {

  public function viewActivityPreferences() {

    return [
      '#type' => 'markup',
      '#theme' => 'ur_activity_preferences',
      '#attached' => [
        'drupalSettings' => [
          'vueRouter' => TRUE,
        ],
      ],
    ];
  }

}
