<?php

namespace Drupal\ur_activity_preferences\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 * @package Drupal\ur_activity_preferences\Form;
 */
class ActivityPreferencesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activity_preferences_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_activity_preferences.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_activity_preferences.settings');

    $form['tab'] = array(
      '#type' => 'vertical_tabs',
    );

    // Activity Preferences Fieldsets --------------------------------------------------------------------------------------------
    $form['following'] = array(
      '#type' => 'details',
      '#title' => t('Following'),
      '#weight' => 1,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#group' => 'tab',
    );

    $form['activity_types'] = array(
      '#type' => 'details',
      '#title' => t('Activity Types'),
      '#weight' => 6,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'tab',
    );

    $form['notifications'] = array(
      '#type' => 'details',
      '#title' => t('Notifications'),
      '#weight' => 7,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'tab',
    );

    // Following Section Settings ---------------------------------------------------------------------------------------------------

    // Auto Follow Options
    $form['following']['following_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Short description of the Following section.'),
      '#default_value' => $this->t(''),
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => FALSE,
    ];

    $form['following']['follow_title_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title - Follow Option 1'),
      '#description' => $this->t('This is the text that will be used as the title for Follow Option 1.'),
      '#default_value' => !empty($config->get('follow_title_1'))
                          ? $config->get('follow_title_1')
                          : 'Smart',
      '#attributes' => array('maxlength' => 30, 'size' => 30),
      '#required' => TRUE,
    ];

    $form['following']['follow_body_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body - Follow Option 1'),
      '#description' => $this->t('This is the text that will be used as the body for Follow Option 1.'),
      '#default_value' => !empty($config->get('follow_body_1'))
                          ? $config->get('follow_body_1')
                          : 'Follows order & jobsites that you create or manage.',
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => TRUE,
    ];

    $form['following']['follow_title_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title - Follow Option 2'),
      '#description' => $this->t('This is the text that will be used as the title for Follow Option 2.'),
      '#default_value' => !empty($config->get('follow_title_2'))
                          ? $config->get('follow_title_2')
                          : 'All',
      '#attributes' => array('maxlength' => 30, 'size' => 30),
      '#required' => TRUE,
    ];

    $form['following']['follow_body_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body - Follow Option 2'),
      '#description' => $this->t('This is the text that will be used as the body for Follow Option 2.'),
      '#default_value' => !empty($config->get('follow_body_2'))
                          ? $config->get('follow_body_2')
                          : 'Follows order & jobsites related to your account.',
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => TRUE,
    ];

    $form['following']['follow_title_3'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title - Follow Option 3'),
      '#description' => $this->t('This is the text that will be used as the title for Follow Option 3.'),
      '#default_value' => !empty($config->get('follow_title_3'))
                          ? $config->get('follow_title_3')
                          : 'Off',
      '#attributes' => array('maxlength' => 30, 'size' => 30),
      '#required' => TRUE,
    ];

    $form['following']['follow_body_3'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body - Follow Option 3'),
      '#description' => $this->t('This is the text that will be used as the body for Follow Option 3.'),
      '#default_value' => !empty($config->get('follow_body_3'))
                          ? $config->get('follow_body_3')
                          : 'Manually select which orders & jobsites you follow.',
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => TRUE,
    ];

    // Manage CTA Copy
    $form['following']['manage_cta'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Manage CTA'),
      '#description' => $this->t('This is the text that will be used inside of the Manage button. The button launches the modal to select orders & jobsites to follow.'),
      '#default_value' => !empty($config->get('manage_cta'))
                          ? $config->get('manage_cta')
                          : 'Select Orders & Jobsites to Follow',
      '#default_value' => $this->t('Select Orders & Jobsites to Follow'),
      '#attributes' => array('maxlength' => 30, 'size' => 30),
      '#required' => TRUE,
    ];

    // Activity Types Section Settings -------------------------------------------------------------------------------------------------
    $form['activity_types']['activity_types_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Short description of the Activity Types section.'),
      '#default_value' => !empty($config->get('activity_types_description'))
                          ? $config->get('activity_types_description')
                          : 'Choose the activity type you would like to see for orders & jobsites you follow.',
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => FALSE,
    ];

    $form['activity_types']['activity_type_title_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title - Activity Type 1'),
      '#description' => $this->t('This is the text that will be used as the title for Activity Type 1.'),
      '#default_value' => !empty($config->get('activity_type_title_1'))
                          ? $config->get('activity_type_title_1')
                          : 'Rental Status',
      '#attributes' => array('maxlength' => 30, 'size' => 30),
      '#required' => TRUE,
    ];

    $form['activity_types']['activity_type_body_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body - Activity Type 1'),
      '#description' => $this->t('This is the text that will be used as the body for Activity Type 1.'),
      '#default_value' => !empty($config->get('activity_type_body_1'))
                          ? $config->get('activity_type_body_1')
                          : 'Keep track of you rental requests with notifications when new quotes & reservations are submitted and updates as items are returned.',
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => TRUE,
    ];

    $form['activity_types']['activity_type_title_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title - Activity Type 2'),
      '#description' => $this->t('This is the text that will be used as the title for Activity Type 2.'),
      '#default_value' => !empty($config->get('activity_type_title_2'))
                          ? $config->get('activity_type_title_2')
                          : 'Fleet Management',
      '#attributes' => array('maxlength' => 30, 'size' => 30),
      '#required' => TRUE,
    ];

    $form['activity_types']['activity_type_body_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body - Activity Type 2'),
      '#description' => $this->t('This is the text that will be used as the body for Activity Type 2.'),
      '#default_value' => !empty($config->get('activity_type_body_2'))
                          ? $config->get('activity_type_body_2')
                          : 'Maximize productivity and equipment utilization with notifications informing where and how equipment is being used and which items are due.',
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => TRUE,
    ];

    $form['activity_types']['activity_type_title_3'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title - Activity Type 3'),
      '#description' => $this->t('This is the text that will be used as the title for Activity Type 3.'),
      '#default_value' => !empty($config->get('activity_type_title_3'))
                          ? $config->get('activity_type_title_3')
                          : 'Deliveries & Pickups',
      '#attributes' => array('maxlength' => 30, 'size' => 30),
      '#required' => TRUE,
    ];

    $form['activity_types']['activity_type_body_3'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body - Activity Type 3'),
      '#description' => $this->t('This is the body that will be used as the title for Activity Type 3.'),
      '#default_value' => !empty($config->get('activity_type_body_3'))
                          ? $config->get('activity_type_body_3')
                          : 'Monitor and support all equipment entering and leaving your jobsites with notifications and reminders when items are scheduled.',
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => TRUE,
    ];

    $form['activity_types']['activity_type_title_4'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title - Activity Type 4'),
      '#description' => $this->t('This is the text that will be used as the title for Activity Type 4.'),
      '#default_value' => !empty($config->get('activity_type_title_4'))
                          ? $config->get('activity_type_title_4')
                          : 'Billing & Payments',
      '#attributes' => array('maxlength' => 30, 'size' => 30),
      '#required' => TRUE,
    ];

    $form['activity_types']['activity_type_body_4'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body - Activity Type 4'),
      '#description' => $this->t('This is the text that will be used as the body for Activity Type 4.'),
      '#default_value' => !empty($config->get('activity_type_body_4'))
                          ? $config->get('activity_type_body_4')
                          : 'Ensure your organization stays current and up-to-date with notifications to help you monitor POs and invoices.',
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => TRUE,
    ];

    $form['activity_types']['courtesy_reminders'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Courtesy Reminders Description'),
      '#description' => $this->t('Short description about Courtesy Reminders.'),
      '#default_value' => !empty($config->get('courtesy_reminders'))
                          ? $config->get('courtesy_reminders')
                          : 'Courtesy reminders are basic notifications that include Order Confirmation, Equipment Due Soon, and Rental Overdue.',
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => TRUE,
    ];

    // Notifications Section Settings -----------------------------------------------------------------------------------------------------
    $form['notifications']['delivery_method'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delivery Method Description'),
      '#description' => $this->t('Short description about Delivery Method options.'),
      '#default_value' => !empty($config->get('delivery_method'))
                          ? $config->get('delivery_method')
                          : 'Choose how you receive notifications for orders & jobsites you follow.',
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => TRUE,
    ];

    $form['notifications']['mobile_push_notifications_link'] = [
      '#type' => 'url',
      '#title' => $this->t('Mobile Push Notifications Link'),
      '#description' => $this->t('This is the URL "Download our mobile app" will link to.'),
      '#default_value' => !empty($config->get('mobile_push_notifications_link'))
                          ? $config->get('mobile_push_notifications_link')
                          : 'https://unitedrentals.com',
      '#attributes' => array('maxlength' => 50, 'size' => 30),
      '#required' => TRUE,
    ];

    $form['notifications']['mobile_push_notifications'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mobile Push Notifications Description'),
      '#description' => $this->t('Short description about Mobile Push Notifications.'),
      '#default_value' => !empty($config->get('mobile_push_notifications'))
                          ? $config->get('mobile_push_notifications')
                          : 'for iOS and Android to enable mobile push notifications.',
      '#attributes' => array('maxlength' => 255, 'size' => 64),
      '#required' => TRUE,
    ];

    $form['notifications']['sms_urgent_alerts'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SMS Urget Alerts'),
      '#description' => $this->t('Short description about SMS Urget Alerts preference.'),
      '#default_value' => !empty($config->get('sms_urgent_alerts'))
                          ? $config->get('sms_urgent_alerts')
                          : 'Urgent alerts include Overdue Rental, Invalid PO Number, and Low Equipment Utilization.',
      '#attributes' => array('maxlength' => 50, 'size' => 64),
      '#required' => TRUE,
    ];

    $form['notifications']['email_promos'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Promotions & Special Offers'),
      '#description' => $this->t('Short description about Email Promotions & Special Offers preference.'),
      '#default_value' => !empty($config->get('email_promos'))
                          ? $config->get('email_promos')
                          : "We'll send you promoties & special offers via email.",
      '#attributes' => array('maxlength' => 50, 'size' => 64),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_activity_preferences.settings');

    $config
      ->set('following', $form_state->getValue('following'))
      ->set('activity_types', $form_state->getValue('activity_types'))
      ->set('notifications', $form_state->getValue('notifications'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
