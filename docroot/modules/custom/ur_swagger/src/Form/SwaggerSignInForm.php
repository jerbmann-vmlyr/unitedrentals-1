<?php

namespace Drupal\ur_swagger\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class SwaggerSignInForm extends FormBase {

  public function getFormId() {
    return 'ur_swagger.sign_in_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['login_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test User Email Address (user login)'),
      '#size' => 64,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }


  public function submitForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('login_email');

    if (!empty($email)) {
      try {
        $authPlugin = \Drupal::service('ur_api_dataservice')
          ->getPlugin('SSOAuthentication');
        $authPlugin->authenticate($email);
        return TRUE;
      }
      catch (\Exception $e) {
        $token = NULL;
      }
    }
  }
}