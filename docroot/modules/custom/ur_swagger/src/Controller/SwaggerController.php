<?php

namespace Drupal\ur_swagger\Controller;

use Drupal\Core\Controller\ControllerBase;

class SwaggerController extends ControllerBase {

  /**
   * View workplace.
   */
  public function load() {
    return [
      '#type' => 'markup',
      '#theme' => 'ur_swagger',
      '#attached' => [
        'library' => [
          'unitedrentals/swagger_internal'
        ]
      ],
    ];
  }

}