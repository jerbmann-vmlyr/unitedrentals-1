<?php

namespace Drupal\urone_user_import\Commands;

use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drush\Backend\BackendPathEvaluator;
use Drush\Commands\DrushCommands;
use Drush\Commands\kit_drush_commands\Util\EnabledModulesTrait;
use Drush\Commands\kit_drush_commands\Util\EnvironmentsTrait;
use Drush\Commands\kit_drush_commands\Util\WriteWrapperTrait;
use Drush\Drush;
use Drush\SiteAlias\HostPath;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class UroneUserImportCommands extends DrushCommands implements SiteAliasManagerAwareInterface {
  use EnabledModulesTrait;
  use EnvironmentsTrait;
  use SiteAliasManagerAwareTrait;
  use WriteWrapperTrait;
  /**
   * The path evaluator.
   *
   * @var \Drush\Backend\BackendPathEvaluator
   */
  protected $pathEvaluator;

  /**
   * UroneUserImportCommands constructor.
   */
  public function __construct() {
    $this->pathEvaluator = new BackendPathEvaluator();
    DrushCommands::__construct();
  }

  /**
   * Import Kelex users to the new UROne theme refactor.
   *
   * @param null|string $site
   *   The site to perform the operation on.
   * @param null|string $environment_from
   *   The environment to perform the operation as.
   * @param null|string $environment_to
   *   The environment to perform the operation to.
   * @param null|array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   *
   * @throws \Exception
   *   If anything wrong happens in file I/O.
   *
   * @command urone-user-import:import-kelex-users
   * @aliases import-kelex-users
   */
  public function importKelexUsers($site = NULL, $environment_from = NULL, $environment_to = NULL, $options = ['option-name' => 'default']) {
    // Determine local site docroot.
    $local_alias_id = "@{$site}.local";
    $evaluatedPath = HostPath::create($this->siteAliasManager(), $local_alias_id);
    $this->pathEvaluator->evaluate($evaluatedPath);
    $docroot = $evaluatedPath->fullyQualifiedPath();

    // Determine dump directory, dump filename, and dump file path.
    $dump_dir = '../database_backups';
    $dump_dir_abs = $docroot . '/' . $dump_dir;
    $dump_file_name_from = 'users_' . $site . '.' . $environment_from . '.export.sql.gz';
    $dump_file_name_to = 'users_' . $site . '.' . $environment_to . '.backup.sql.gz';
    $dump_file_abs_from = $dump_dir_abs . '/' . $dump_file_name_from;
    $dump_file_abs_to = $dump_dir_abs . '/' . $dump_file_name_to;

    // Determine which tables are associated with users.
    $user_tables = implode(',', [
      'users',
      'users_data',
      'users_field_data',
      'users_roles',
      'user__roles',
      'user__field_last_name',
      'user__field_job_title',
      'user__field_first_name',
      'flag',
      'flag_counts',
      'flag_types',
      'flagging',
      'cart',
      'cart_item'
    ]);

    // Backup the tables.
    $this->io()->newLine();
    $this->io()->title("Backup User Tables from: {$site}.{$environment_to}");
    $process = Drush::process("drush @{$site}.{$environment_to} sql:dump --gzip --tables-list='{$user_tables}' > {$dump_file_abs_to}");
    $success = ($this->io()->isVerbose()) ? $process->run($process->showRealtime()) : $process->run();

    // Check exit code.
    if ($success === 0) {
      $this->logger()->success(dt('Successful backup of user tables.'));
    }
    else {
      $this->logger()->error(dt('Unable to backup user tables.'));
    }

    // Export the tables.
    $this->io()->newLine();
    $this->io()->title("Exporting User Tables from: {$site}.{$environment_from}");
    $process = Drush::process("drush @{$site}.{$environment_from} sql:dump --gzip --tables-list='{$user_tables}' > {$dump_file_abs_from}");
    $success = ($this->io()->isVerbose()) ? $process->run($process->showRealtime()) : $process->run();

    // Check exit code.
    if ($success === 0) {
      $this->logger()->success(dt('Successfully exported Kelex user tables.'));
    }
    else {
      $this->logger()->error(dt('Unable to export Kelex user tables.'));
    }

    // Import the tables.
    $this->io()->newLine();
    $this->io()->title("Importing User Tables to: {$site}.{$environment_to}");
    $process = $this->processManager()->shell("gunzip -c {$dump_file_abs_from} | drush @{$site}.{$environment_to} sqlc");
    $success = ($this->io()->isVerbose()) ? $process->run($process->showRealtime()) : $process->run();

    // Check exit code.
    if ($success === 0) {
      $this->logger()->success(dt('Successfully imported Kelex user tables.'));
    }
    else {
      $this->logger()->error(dt('Unable to import Kelex user tables.'));
    }
  }

}
