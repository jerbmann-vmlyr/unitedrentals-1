<?php

namespace Drupal\ur_list;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the storage handler class for List entities.
 *
 * This extends the base storage class, adding required special handling for
 * List entities.
 *
 * @ingroup ur_list
 */
interface BaseListStorageInterface extends ContentEntityStorageInterface {
  // @TODO - Do things in here.
}
