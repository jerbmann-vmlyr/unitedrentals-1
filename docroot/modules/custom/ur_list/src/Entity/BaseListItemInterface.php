<?php

namespace Drupal\ur_list\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining List Item entities.
 *
 * @ingroup ur_list
 */
interface BaseListItemInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the List Item name.
   *
   * @return string
   *   Name of the List Item.
   */
  public function getName();

  /**
   * Sets the List Item name.
   *
   * @param string $name
   *   The List Item name.
   *
   * @return \Drupal\ur_list\Entity\BaseListItemInterface
   *   The called List Item entity.
   */
  public function setName($name);

  /**
   * Gets the List Item creation timestamp.
   *
   * @return int
   *   Creation timestamp of the List Item.
   */
  public function getCreatedTime();

  /**
   * Sets the List Item creation timestamp.
   *
   * @param int $timestamp
   *   The List Item creation timestamp.
   *
   * @return \Drupal\ur_list\Entity\BaseListItemInterface
   *   The called List Item entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the List Item published status indicator.
   *
   * Unpublished List Item are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the List Item is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a List Item.
   *
   * @param bool $published
   *   TRUE to set this List Item to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\ur_list\Entity\BaseListItemInterface
   *   The called List Item entity.
   */
  public function setPublished($published);

}
