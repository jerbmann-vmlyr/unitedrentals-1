<?php

namespace Drupal\workplace\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class WorkplaceController.
 *
 * @package Drupal\workplace\Controller
 */
class WorkplaceController extends ControllerBase {

  /**
   * View workplace.
   */
  public function viewWorkplace() {
    /*
     * We allow all users including anonymous to access workplace.
     */
    return [
      '#type' => 'markup',
      '#theme' => 'workplace',
      '#attached' => [
        'drupalSettings' => [
          'vueRouter' => TRUE,
        ],
      ],
    ];
  }
  /**
   * View pickup.
   */
  public function viewPickup() {
    return [
      '#type' => 'markup',
      '#theme' => 'pickup',
      '#attached' => [
        'drupalSettings' => [
          'vueRouter' => TRUE,
        ],
      ],
    ];
  }

}
