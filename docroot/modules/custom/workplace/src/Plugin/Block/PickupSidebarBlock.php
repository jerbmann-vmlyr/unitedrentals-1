<?php

namespace Drupal\workplace\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\Entity\File;

/**
 * Provides a 'Pickup Sidebar Block' Block.
 *
 * @Block(
 *   id = "pickup_sidebar_block",
 *   admin_label = @Translation("Pickup Sidebar Block"),
 *   category = @Translation("Workplace"),
 * )
 */
class PickupSidebarBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $image = $this->configuration['image'];
    if (!empty($image[0])) {
      if ($file = File::load($image[0])) {
        $build['image'] = [
          '#theme' => 'image',
          '#uri' => $file->getFileUri(),
        ];
      }
    }
    return $build;
  }
  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['image'] = [
      '#type' => 'managed_file',
      '#upload_location' => 'public://module-images/pickup-sidebar',
      '#title' => t('Image'),
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg']
      ],
      '#default_value' => isset($this->configuration['image']) ? $this->configuration['image'] : '',
      '#description' => t('The image to appear in pickup sidebar'),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Save image as permanent.
    $image = $form_state->getValue('image');
    if ($image != $this->configuration['image']) {
      if (!empty($image[0])) {
        $file = File::load($image[0]);
        $file->setPermanent();
        $file->save();
      }
    }
    $this->configuration['image'] = $form_state->getValue('image');
  }

}
