<?php

namespace Drupal\recently_viewed\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RecentlyViewedController.
 *
 * @package Drupal\recently_viewed\Controller
 */
class RecentlyViewedController extends ControllerBase {

  /**
   * EntityTypeManger definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Route for retrieving the cat-classes of recently viewed items.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Array of cat-classes of recently viewed items.
   */
  public function retrieveRecentlyViewedItems($json = TRUE) {
    $data = $this->buildRecentlyViewed();

    // Add to SESSION.
    if (isset($data['recently_viewed'])) {
      $_SESSION['recently_viewed_equipment'] = $data['recently_viewed'];
    }

    // If not JSON, return the data response.
    if (!$json) {
      return $data;
    }

    // Build JsonResponse instead.
    $response = new JsonResponse();
    $response->setData($data);

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Helper method to retrieve the array of Cat-Class Recently Viewed Items.
   *
   * @return array
   *   An array of recently viewed cat-class codes
   */
  protected function buildRecentlyViewed() {
    $data = [];
    $session = \Drupal::service('session');

    if (!isset($data['recently_viewed'])) {
      $data['recently_viewed'] = [];
      $query = \Drupal::database()->select('recently_viewed', 'rv');

      if (\Drupal::currentUser()->isAuthenticated()) {
        $query->condition('uid', \Drupal::currentUser()->id());
      }
      else {
        $query->condition('sid', $session->getId());
      }

      $query->condition('type', 'item');
      $query->fields('rv');
      $query->orderBy('timestamp', 'DESC');
      $result = $query->execute()->fetchAll();

      /*
       * Now that we have the entity IDs for all the Items we've recently viewed
       * load them up and get the cat-class field value.
       */
      foreach ($result as $rv) {
        /** @var \Drupal\Core\Entity\EntityInterface $entity */
        $entity = $this->entityTypeManager->getStorage('node')->load($rv->entity_id);
        $cat_class = $entity->get('field_item_cat_class_code')->value;
        $data['recently_viewed'][] = $cat_class;
      }

    }

    return $data;
  }

}
