<?php

namespace Drupal\recently_viewed\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Menu per role administration form.
 *
 * @package Drupal\menu_per_role\Form
 */
class RecentlyViewedAdminSettings extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The entity display repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['recently_viewed.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'recently_viewed_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Load all node bundles and display modes
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $displays = $this->entityDisplayRepository->getViewModes('node');
    $config = $this->config('recently_viewed.settings');

    $form['#tree'] = TRUE;

    $form['rv_config'] = [
      '#type' => 'fieldset',
      '#title' => t('Recently Viewed Config'),
    ];

    // @TODO: Find some solution other than session_api
     $form['rv_config']['expiration_time'] = array(
        '#title' => 'Expiration Time',
        '#description' => $this->t('The timestamp interval at which recently viewed items will no longer be considered "recent". (e.g. 30 days = 2,592,000 seconds) '),
        '#type' => 'textfield',
        '#default_value' => !empty($config->get('expiration_timestamp')) ? $config->get('expiration_timestamp') : '2592000',
      );

    foreach ($node_types as $bundle => $node_type) {

      $form['rv_config'][$bundle] = [
        '#type' => 'details',
      // '#collapsible' => TRUE,.
        '#open' => !empty($config->get($bundle)) ? TRUE : FALSE,
        '#title' => $this->t('Recently Viewed ' . $node_type->label() . ' config'),
      ];

      $form['rv_config'][$bundle]['enable'] = [
        '#type' => 'checkbox',
        '#description' => $this->t('Would you like to start tracking this node bundle?'),
        '#title' => $this->t('Enable'),
        '#default_value' => !empty($config->get($bundle . '.enabled')) ? $config->get($bundle . '.enabled') : FALSE,
      ];

      $form['rv_config'][$bundle]['max_record'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Max Record for Recently Viewed @entity', ['@entity' => $node_type->label()]),
        '#default_value' => !empty($config->get($bundle . '.max')) ? $config->get($bundle . '.max') : 10,
      ];

      $form['rv_config'][$bundle]['view_mode'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('View mode for tracking'),
        '#description' => $this->t('Which view mode(s) would  you like to track recently viewed nodes for?'),
        '#default_value' => !empty($config->get($bundle . '.view_mode')) ? $config->get($bundle . '.view_mode') : ['full' => 'full'],
        '#options' => isset($displays) ? array_combine(array_keys($displays), array_keys($displays)) : [],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $values = $form_state->getValues();

    // Check if timestamp is numeric
    if (!is_numeric($values['rv_config']['expiration_time']) || $values['rv_config']['expiration_time'] < 1) {
      $form_state->setErrorByName('expiration_time', 'Expiration Timestamp must be a positive integer value');
    }

    /*
     * Now that we've validated that expiration time is a number, remove it
     * from the $values so we do not get any warnings about illegal offsets as
     * the rest of the values will be arrays and will have a "max_record" key
     */
    unset($values['rv_config']['expiration_time']);

    // Check if the max_record field is numeric.
    foreach ($values['rv_config'] as $key => $config) {
      if (!is_numeric($config['max_record']) || $config['max_record'] < 1) {
        $form_state->setErrorByName('rv_config][' . $key . '][max_record', $this->t('%field must be a positive integer value.', ['%field' => $key]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('recently_viewed.settings');
    $values = $form_state->getValues();

    $config->set('expiration_timestamp', $values['rv_config']['expiration_time'])->save();

    foreach ($values['rv_config'] as $node_type => $settings) {
      if ($settings['enable'] === 1) {
        $config->set($node_type . '.enabled', $settings['enable'])
          ->set($node_type . '.max', $settings['max_record'])
          ->set($node_type . '.view_mode', $settings['view_mode'])
          ->save();
      }
    }

    parent::submitForm($form, $form_state);
  }

}
