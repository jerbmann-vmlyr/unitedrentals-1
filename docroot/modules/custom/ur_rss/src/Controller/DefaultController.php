<?php

namespace Drupal\ur_rss\Controller;

use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Class DefaultController.
 *
 * @package Drupal\ur_rss\Controller
 */
class DefaultController extends ControllerBase {
  /**
   * Full array of objects containing item info.
   *
   * @var array
   */
  private $itemList;

  /**
   * List of ID's of all items found.
   *
   * @var array
   */
  public $itemIdList;

  /**
   * {@inheritdoc}
   */
  public function productFeed() {
    $this->getFeedItems();
    return $this->outputFeed();
  }

  /**
   * {@inheritdoc}
   */
  public function productFeed2() {
    $this->getSecondaryFeedItems();
    return $this->outputFeed();
  }

  /**
   * {@inheritdoc}
   */
  public function outputFeed() {
    // Output Content-Type 'text/xml; charset=utf-8' header.
    $feed = $this->createFeed();

    $basicResponse = new Response($feed, Response::HTTP_OK, [
      'content-type' => 'application/xml',
    // Do not index the sitemap itself.
      'X-Robots-Tag' => 'noindex',
    ]);

    return $basicResponse;
  }

  /**
   * Gets the primary feed items.
   *
   * @throws \Drupal\Core\Database\DatabaseExceptionWrapper
   * @throws \Drupal\Core\Database\IntegrityConstraintViolationException
   * @throws \InvalidArgumentException
   */
  public function getFeedItems() {
    $this->itemList = \Drupal::database()->query('
      SELECT 
        nfd.nid AS ID,
        nfd.title AS title,
        main.field_item_cat_class_code_value AS catClassCode,
        t1.field_plain_long_value AS description,
        files.uri AS image_path,
        google.field_google_product_code_value AS google_code,
        IF (nfqo.entity_id IS NOT NULL, nfqo.field_quantity_owned_value, 0) as quantity_owned,
        IF (nfqor.entity_id IS NOT NULL, nfqor.field_quantity_on_rent_value, 0) as quantity_on_rent,
        IF (finro.entity_id IS NOT NULL AND finro.field_item_not_rentable_online_value = 1, 0, 1) as rentable_online,
        IF (fds.entity_id IS NOT NULL, fds.field_dal_status_value, 0) as dal_status,
        IF (fddic.entity_id IS NOT NULL, fddic.field_dal_display_in_catalog_value, 0) as dal_display_in_catalog,
        fic.field_item_category_target_id as category,
        IF (fid.entity_id IS NOT NULL, fid.field_item_division_value, \'N/A\') as division
      FROM node__field_item_cat_class_code AS main
        INNER JOIN node_field_data AS nfd
          ON nfd.nid = main.entity_id
        LEFT JOIN node__field_plain_long AS t1
          ON t1.entity_id = main.entity_id
        LEFT JOIN node__field_plain_long AS t2
          ON t2.entity_id = main.entity_id
        LEFT JOIN node__field_images_unlimited AS images
          ON images.entity_id = main.entity_id
        LEFT JOIN file_managed AS files
          ON images.field_images_unlimited_target_id = files.fid
        LEFT JOIN node__field_google_product_code AS google
          ON google.entity_id = nfd.nid
        LEFT JOIN node__field_quantity_owned AS nfqo
          ON nfqo.entity_id = main.entity_id
        LEFT JOIN node__field_quantity_on_rent AS nfqor
          ON nfqor.entity_id = main.entity_id
        LEFT JOIN node__field_item_category AS fic
          ON fic.entity_id = main.entity_id
        LEFT JOIN node__field_item_division AS fid
          ON fid.entity_id = main.entity_id
        LEFT JOIN node__field_item_not_rentable_online AS finro
          ON finro.entity_id = main.entity_id
        LEFT JOIN node__field_dal_status AS fds
          ON fds.entity_id = main.entity_id
        LEFT JOIN node__field_dal_display_in_catalog AS fddic
          ON fddic.entity_id = main.entity_id
      WHERE nfd.status = 1 AND t2.langcode = \'en\'
    ')->fetchAllAssoc('ID');

    $this->itemIdList = array_keys($this->itemList);
  }

  /**
   * Gets the secondary feed items.
   *
   * @throws \Drupal\Core\Database\DatabaseExceptionWrapper
   * @throws \Drupal\Core\Database\IntegrityConstraintViolationException
   * @throws \InvalidArgumentException
   */
  public function getSecondaryFeedItems() {
    $this->itemList = \Drupal::database()->query('
      SELECT 
        nfd.nid AS ID,
        nfd.title AS title,
        main.field_item_cat_class_code_value AS catClassCode,
        t2.field_plain_long_value AS description,
        files.uri AS image_path,
        google.field_google_product_code_value AS google_code,
        IF (nfqo.entity_id IS NOT NULL, nfqo.field_quantity_owned_value, 0) as quantity_owned,
        IF (nfqor.entity_id IS NOT NULL, nfqor.field_quantity_on_rent_value, 0) as quantity_on_rent,
        IF (finro.entity_id IS NOT NULL AND finro.field_item_not_rentable_online_value = 1, 0, 1) as rentable_online,
        IF (fds.entity_id IS NOT NULL, fds.field_dal_status_value, 0) as dal_status,
        IF (fddic.entity_id IS NOT NULL, fddic.field_dal_display_in_catalog_value, 0) as dal_display_in_catalog,
        fic.field_item_category_target_id as category,
        IF (fid.entity_id IS NOT NULL, fid.field_item_division_value, \'N/A\') as division
      FROM node__field_item_cat_class_code AS main
        INNER JOIN node_field_data AS nfd
          ON nfd.nid = main.entity_id
        LEFT JOIN node__field_plain_long AS t2
          ON t2.entity_id = main.entity_id
        LEFT JOIN node__field_images_unlimited AS images
          ON images.entity_id = main.entity_id AND images.delta = 0
        LEFT JOIN file_managed AS files
          ON images.field_images_unlimited_target_id = files.fid
        LEFT JOIN node__field_google_product_code AS google
          ON google.entity_id = nfd.nid
        LEFT JOIN node__field_quantity_owned AS nfqo
          ON nfqo.entity_id = main.entity_id
        LEFT JOIN node__field_quantity_on_rent AS nfqor
          ON nfqor.entity_id = main.entity_id
        LEFT JOIN node__field_item_category AS fic
          ON fic.entity_id = main.entity_id
        LEFT JOIN node__field_item_division AS fid
          ON fid.entity_id = main.entity_id
        LEFT JOIN node__field_item_not_rentable_online AS finro
          ON finro.entity_id = main.entity_id
        LEFT JOIN node__field_dal_status AS fds
          ON fds.entity_id = main.entity_id
        LEFT JOIN node__field_dal_display_in_catalog AS fddic
          ON fddic.entity_id = main.entity_id
      WHERE nfd.status = 1
      ORDER BY description DESC
    ')->fetchAllAssoc('ID');

    $this->itemIdList = array_keys($this->itemList);
  }

  /**
   * Create the string that adds the description element to an RSS item.
   *
   * @param $item
   *
   * @return string
   *
   * @throws \Drupal\Core\Database\DatabaseExceptionWrapper
   * @throws \Drupal\Core\Database\IntegrityConstraintViolationException
   * @throws \InvalidArgumentException
   */
  private function buildProductDescription($item) {
    /**
     * Build in this order:
     *    Check for description already attached to item
     *    If description is empty, fetch Highlights and build using that. (not common)
     *    If highlights are empty, check title which all items MUST have and use that. (even less common)
     */
    $item->description = '';

    if ($item->description !== '') {
      return '<g:description><![CDATA[' . $item->description . ']]></g:description>' . "\n";
    }

    /** @var array $highlights */
    $highlights = \Drupal::database()->query("SELECT field_item_highlights_value AS highlight FROM node__field_item_highlights WHERE langcode = 'en' AND deleted = 0 AND entity_id = {$item->ID}")->fetchAll();

    if (!empty($highlights)) {
      foreach ($highlights as $row) {
        $item->description .= $row->highlight . '. ';
      }

      return '<g:description><![CDATA[' . $item->description . ']]></g:description>' . "\n";
    }

    return '<g:description><![CDATA[' . $item->title . ']]></g:description>' . "\n";
  }

  /**
   * Create the string that adds the image link element to an RSS item.
   *
   * @param $item
   *
   * @return string
   */
  private function getImageLink($item) {
    if ($item->image_path !== NULL) {
      return '<g:image_link>' . file_create_url($item->image_path) . '</g:image_link>' . "\n";
    }

    return '';
  }

  /**
   * Create the string that adds the title element to an RSS item.
   *
   * @param $title
   *
   * @return string
   */
  private function buildTitle($title) {
    if ($title !== NULL) {
      return '<g:title><![CDATA[' . $title . ']]></g:title>' . "\n";
    }

    return '';
  }

  /**
   * Create the RSS Feed string and output it.
   *
   * @return string
   */
  public function createFeed() {
    global $base_url;

    // Load categories.
    $categories = [];
    $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree(('categories'));
    foreach ($tree as $term) {
      $categories[$term->tid] = [
        'tid' => $term->tid,
        'name' => $term->name,
        'depth' => $term->depth,
        'parents' => $term->parents,
      ];
    }

    // @TODO: Might have to figure out the D8 version of this header...
    // drupal_add_http_header('Content-Type', 'text/xml; charset=utf-8');

    $xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
    $xml .= '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">' . "\n";
    $xml .= '<channel>' . "\n";
    $xml .= '<title>' . t('United Rentals Product Feed') . "</title>\n";
    $xml .= '<link>' . $base_url . t('feeds/products') . "</link>\n";
    $xml .= '<description>' . t('A feed for Google Product Integration') . "</description>\n";

    // Get RSS channel items.
    foreach ($this->itemList as $id => $item) {
      $url = Url::fromRoute('entity.node.canonical', ['node' => $id])->toString();

      // Determine availability.
      $availability = 'in stock';
      $quantityAvailable = ($item->quantity_owned - $item->quantity_on_rent);

      if ($quantityAvailable <= 0 ) {
        $availability = 'out of stock';
      }

      // Determine division.
      $division = 'N/A';

      switch ($item->division) {
        case 'PH':
          $division = 'Power/HVAC';
          break;

        case 'PS':
          $division = 'Pump Solutions';
          break;

        case 'TL':
          $division = 'Tools &amp; Industrial';
          break;

        case 'TS':
          $division = 'Trench Safety';
          break;

        default:
            $division = 'N/A';
            break;
      }

      // Determine Category and Sub Category.
      $subCategory = $this->getCategory($item->category, $categories, 1);
      $category = $this->getCategory($item->category, $categories);

      // Get the XML.
      $xml .= '<item>' . "\n";
      $xml .= '<cat_class>' . $item->catClassCode . '</cat_class>' . "\n";
      $xml .= '<g:id>' . $id . '</g:id>' . "\n";
      $xml .= $this->buildTitle($item->title);
      $xml .= '<g:link>' . $base_url . $url . '</g:link>' . "\n";
      $xml .= '<g:price>0.00</g:price>' . "\n";
      $xml .= '<g:adult>FALSE</g:adult>' . "\n";

      // Default for google_code.
      if (empty($item->google_code)) {
        $item->google_code = 114;
      }

      $xml .= '<g:google_product_category>' . $item->google_code . '</g:google_product_category>' . "\n";
      $xml .= '<g:shipping>0.00</g:shipping>' . "\n";
      $xml .= '<g:availability>' . $availability . '</g:availability>' . "\n";
      $xml .= '<g:condition>used</g:condition>' . "\n";
      $xml .= '<g:identifier_exists>no</g:identifier_exists>' . "\n";
      $xml .= $this->buildProductDescription($item);
      $xml .= $this->getImageLink($item);
      $xml .= '<division>' . $division . '</division>' . "\n";
      $xml .= '<quantity_owned>' . $item->quantity_owned . '</quantity_owned>' . "\n";
      $xml .= '<quantity_on_rent>' . $item->quantity_on_rent . '</quantity_on_rent>' . "\n";
      $xml .= '<quantity_available>' . $quantityAvailable . '</quantity_available>' . "\n";
      $xml .= '<rentable_online>' . $item->rentable_online . '</rentable_online>' . "\n";
      $xml .= '<dal_status>' . $item->dal_status . '</dal_status>' . "\n";
      $xml .= '<dal_display_in_catalog>' . $item->dal_display_in_catalog . '</dal_display_in_catalog>' . "\n";
      $xml .= '<category>' . $category . '</category>' . "\n";
      $xml .= '<sub_category>' . $subCategory . '</sub_category>' . "\n";
      $xml .= '</item>' . "\n";
    }

    $xml .= '</channel>';
    $xml .= '</rss>';

    return $xml;
  }

  /**
   * Retrieve category and sub-category for term item.
   */
  private function getCategory($categoryId, array $categories, $depth = 0) {
    $category = NULL;
    if (isset($categories[$categoryId])) {
      $childCategory = $categories[$categoryId];
      while ($childCategory['depth'] > $depth && isset($childCategory['parents'][0])) {
        $parentId = $childCategory['parents'][0];
        if (isset($categories[$parentId])) {
          $childCategory = $categories[$parentId];
        }
      }

      // Make sure depth matches.
      if ($childCategory['depth'] == $depth) {
        $category = htmlentities($childCategory['name'], ENT_XML1);
      }
    }

    return $category;
  }

}
