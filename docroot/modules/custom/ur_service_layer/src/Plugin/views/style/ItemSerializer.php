<?php

namespace Drupal\ur_service_layer\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;

/**
 * The style plugin for serialized output formats.
 *
 * Add separator in csv file so Microsoft Word knows how to open it.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "item_serializer",
 *   title = @Translation("Item Serializer"),
 *   help = @Translation("Serializes views row data using the Serializer component."),
 *   display_types = {"data"}
 * )
 */
class ItemSerializer extends Serializer {

}
