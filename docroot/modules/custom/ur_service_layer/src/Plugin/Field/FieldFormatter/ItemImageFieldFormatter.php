<?php

namespace Drupal\ur_service_layer\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * .
 *
 * @FieldFormatter(
 *   id = "item_image_formatter",
 *   label = @Translation("Item Image Rest Formatter"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ItemImageFieldFormatter extends ImageFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $files = $this->getEntitiesToView($items, $langcode);

    $image_style_setting = $this->getSetting('image_style');

    // Determine if Image style is required.
    $image_style = NULL;
    if (!empty($image_style_setting)) {
      $image_style = entity_load('image_style', $image_style_setting);
    }

    // Foreach ($items as $delta => $item) {.
    foreach ($files as $delta => $file) {

      $image_uri = $file->getFileUri();

      if ($image_style) {
        $absolute_path = $this->imageStyleStorage->load($image_style->getName())->buildUrl($image_uri);
      }
      else {
        // Get absolute path for original image.
        $absolute_path = Url::fromUri(file_create_url($image_uri))->getUri();
      }
      if (isset($file->_referringItem)) {
        $imgRef = $file->_referringItem;
        $imgFields = $imgRef->getValue();
        $alt = $imgFields['alt'];
      }
      $value = ['link' => $absolute_path, 'alt' => isset($alt) ? $alt : ''];
      $elements[$delta] = [
        '#markup' => json_encode($value, JSON_UNESCAPED_SLASHES | JSON_HEX_APOS | JSON_HEX_QUOT),
      ];
    }
    return $elements;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('link_generator'),
      $container->get('entity.manager')->getStorage('image_style')
    );
  }

}
