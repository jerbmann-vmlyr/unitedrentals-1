<?php

namespace Drupal\ur_service_layer\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class Item.
 *
 * @uses \Drupal\Core\Entity\EntityTypeManger
 * @package Drupal\ur_service_layer\Entity
 * @version 1.1  */
class Item {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   *   Our entity manager instance.
   */
  protected $entityTypeManager;

  /**
   * Item constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Dependency injected by drupal ci.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Helper method to return entity found by searching on Cat-Class code.
   *
   * @param string $catClass
   *   The cat-class code to find an "item" entity by - "item", referring to a piece of equipment.
   *
   * @return \Drupal\Core\Entity\EntityInterface|false
   *   The entity object found
   */
  public function findItembyCatClass($catClass) {
    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'item');
    $query->condition('field_item_cat_class_code', $catClass);
    $entity_ids = $query->execute();
    $entity = $this->entityTypeManager->getStorage('node')->load(reset($entity_ids));

    if ($entity !== NULL) {
      return $entity;
    }

    return FALSE;
  }

}
