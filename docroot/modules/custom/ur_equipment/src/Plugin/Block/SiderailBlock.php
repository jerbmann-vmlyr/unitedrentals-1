<?php

namespace Drupal\ur_equipment\Plugin\Block;

use Drupal\block\Entity\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilder;
use Drupal\block_content\Entity\BlockContent;
use Drupal\node\NodeInterface;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\ur_facets\Controller\FacetController;
use Drupal\recently_viewed\Controller\RecentlyViewedController;
use Drupal\ur_favorites\Controller\FavoritesController;
use Drupal\views\Views;
use Drupal\ur_frontend_api_v2\Models\EquipmentOwned;
use Drupal\ur_user\Controller\UserController;

/**
 * Provides a 'Siderail' block.
 *
 * @Block(
 *  id = "equipment_listing_siderail_block",
 *  admin_label = @Translation("Equipment Listing - Siderail block"),
 * )
 */
class SiderailBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Grab current category / sub-category from url.
    $path = \Drupal::request()->getpathInfo();
    $urlParts = explode('/', $path);
    $currentCategory = NULL;
    $currentSubCategory = NULL;

    // Load categories.
    $categories = $this->getCategories();

    // Load category from alias.
    if (isset($urlParts[3])) {
      // Snag term path by alias.
      $path = \Drupal::service('path.alias_manager')->getPathByAlias('/marketplace/equipment/' . $urlParts[3]);
      if (preg_match('/taxonomy\/term\/(\d+)/', $path, $matches) && isset($categories[$matches[1]])) {
        $currentCategory = $categories[$matches[1]];
      }
    }

    // Load sub-category from alias.
    if (!empty($currentCategory) && isset($urlParts[3]) && isset($urlParts[4])) {
      // Snag term path by alias.
      $path = \Drupal::service('path.alias_manager')->getPathByAlias('/marketplace/equipment/' . $urlParts[3] . '/' . $urlParts[4]);
      if (preg_match('/taxonomy\/term\/(\d+)/', $path, $matches) 
        && isset($currentCategory->children[$matches[1]])
      ) {
        $currentSubCategory = $currentCategory->children[$matches[1]];
      }
    }

    // Determine equipment categories.
    $catClassesAvailableInCategory = [];
    $equipmentAttributes = [];
    $categorySelected = $currentCategory;
    if (!empty($currentSubCategory)) {
      $categorySelected = $currentSubCategory;
    }

    // Add catClasses.
    $this->addCatClasses($catClassesAvailableInCategory, $categorySelected, $equipmentAttributes);

    // Filter by owned equipment.
    $catClassesOwned = [];
    $userController = new UserController();
    $branchId = $userController->read('ratesPlaceBranch', FALSE);
    if (empty($branchId) && isset($_GET['branchId'])) {
      $branchId = filter_var($_GET['branchId'], FILTER_SANITIZE_STRING);
      if (isset($branchId) && !empty($branchId)) {
        $userController->update('ratesPlaceBranch', $branchId);
      }
    } 
    if (isset($branchId) && !empty($branchId)) {
      $availableEquipment = EquipmentOwned::read($branchId, $catClass = NULL);
      foreach ($availableEquipment as $equip) {
        if (in_array($equip->catClass, $catClassesAvailableInCategory)) {
          $catClassesOwned[] = $equip->catClass;
        }
      }
    }
    else {
      $catClassesOwned = $catClassesAvailableInCategory;
    }

    // Remove filters that aren't owned.
    if (!empty($catClassesOwned) && !empty($equipmentAttributes)) {
      foreach ($equipmentAttributes as $parentIndex => &$parentAttribute) {
        if (!empty($parentAttribute->children)) {
          foreach ($parentAttribute->children as $childIndex => &$attribute) {
            $owned = FALSE;
            if (!empty($attribute->catClasses)) {
              foreach ($attribute->catClasses as $catIndex => $catClass) {
                if (in_array($catClass, $catClassesOwned)) {
                  $owned = TRUE;
                }
                else {
                  // Remove cat class from count.
                  unset($attribute->catClasses[$catIndex]);
                }
              }
            }
    
            // None owned? Remove it entirely.
            if (!$owned) {
              unset($parentAttribute->children[$childIndex]);
            }
          }
        }
        else {
          unset($equipmentAttributes[$parentIndex]);
        }
      }
    }

    // Determine selected filters.
    $selectedFilters = [];
    if (isset($urlParts[5])) {
      $selectedFilters = explode('+', str_replace([' ', '%20'], '+', $urlParts[5]));
    }
    elseif (!empty(\Drupal::request()->query->has('equipment_filters'))) {
      $selectedFilters = explode('+', str_replace([' ', '%20'], '+', \Drupal::request()->query->get('equipment_filters')));
    }

    // Is the user logged in?
    $isLoggedIn = (\Drupal::currentUser()->id() > 0);

    // Grab recently viewed controller and pull back recently viewed results.
    $recentlyViewedCount = 0;
    $recentlyViewedController = RecentlyViewedController::create(\Drupal::getContainer());
    $recentlyViewed = $recentlyViewedController->retrieveRecentlyViewedItems(FALSE);

    // Filter by what equipment is available for branch.
    $recentlyViewedCount = 0;
    if (!empty($recentlyViewed) && isset($recentlyViewed['recently_viewed'])) {
      if (!empty($catClassesOwned)) {
        foreach ($recentlyViewed['recently_viewed'] as $catClass) {
          if (in_array($catClass, $catClassesOwned)) {
            $recentlyViewedCount++;
          }
        }
      }
    }

    // Grab favorites controller and pull back user favorites.
    $favoritesCount = 0;
    if ($isLoggedIn) {
      $favoritesController = FavoritesController::create(\Drupal::getContainer());
      $favorites = $favoritesController->retrieveFavorites(FALSE);

      // Do we have favorites?
      if (!empty($favorites) && isset($favorites['favorites'])) {
        if (!empty($catClassesOwned)) {
          foreach ($favorites['favorites'] as $catClass) {
            if (in_array($catClass, $catClassesOwned)) {
              $favoritesCount++;
            }
          }
        }
      }
    }
    
    // Determine urls for recently view and favorites.
    $query = \Drupal::request()->query->all();

    // Add recently viewed to query.
    $recentlyViewedParams = $query;
    $recentlyViewedParams['recently_viewed'] = 1;
    $recentlyViewedUrl = '?' . http_build_query($recentlyViewedParams);
    
    // Add favorites to query.
    $favoritesParams = $query;
    $favoritesParams['favorites'] = 1;
    $favoritesUrl = '?' . http_build_query($favoritesParams);

    return [
      '#theme' => 'equipment_listing_siderail',
      '#currentCategory' => $currentCategory,
      '#currentSubCategory' => $currentSubCategory,
      '#categories' => $categories,
      '#recentlyViewedCount' => $recentlyViewedCount,
      '#recentlyViewedUrl' => $recentlyViewedUrl,
      '#favoritesCount' => $favoritesCount,
      '#favoritesUrl' => $favoritesUrl,
      '#isLoggedIn' => $isLoggedIn,
      '#equipmentAttributes' => $equipmentAttributes,
      '#selectedFilters' => $selectedFilters,
      '#cache' => [
        'max-age' => 0
      ],
      '#attached' => [
        'library' => [
          'ur_equipment/main',
        ],
      ],
    ];
  }
  
  /**
   * Recursive method for grabbing cat classes.
   */
  protected function addCatClasses(array &$catClasses, $category, array &$equipmentAttributes = []) {
    // Add catClasses.
    if (!empty($category) && isset($category->catClasses) && is_array($category->catClasses)) {
      foreach ($category->catClasses as $catClass) {
        if (!in_array($catClass, $catClasses)) {
          $catClasses[] = $catClass;
        }
      }
    }

    // Traverse children.
    if (isset($category->children) && !empty($category->children)) {
      // Need to filter out what's not actually available.
      foreach ($category->children as &$child) {
        if (!in_array($child, $equipmentAttributes) && !empty($child->children)) {
          $equipmentAttributes[] = $child;
        }
        $this->addCatClasses($catClasses, $child, $equipmentAttributes);
      }
    }
  }

  /**
   * Grab the categories from facet controller.
   */
  protected function getCategories() {
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('categories');
    return FacetController::termTreeBuild($terms, 0, NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Don't allow caching this block.
    return 0;
  }

}
