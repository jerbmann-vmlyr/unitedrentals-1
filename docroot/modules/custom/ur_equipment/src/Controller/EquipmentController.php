<?php

namespace Drupal\ur_equipment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class EquipmentController.
 *
 * @package Drupal\ur_equipment\Controller
 */
class EquipmentController extends ControllerBase {

  /**
   * Equipment.
   *
   * @return array
   *   Return markup needed for Vue component.
   */
  public function content() {
    return [
      '#type' => 'markup',
      '#markup' => '<h1>' . $this->t('Browse Equipment & Tool Rentals') . '</h1>',
      '#attached' => [
        'library' => [
          'core/drupal',
        ],
      ],
    ];
  }

  /**
   * Gets the redirect link to pivot from cat class to equipment view page.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirects to view equipment page.
   */
  public function getEquipmentPageByCatClass() {

    try {

      $catClass = \Drupal::request()->get('catClass');

      /** @var \Drupal\ur_service_layer\Entity\Item $itemService */
      $itemService = \Drupal::service('ur_service_layer.entity_item');

      /** @var \Drupal\Core\Entity\EntityInterface $item */
      $item = $itemService->findItembyCatClass($catClass);

      if ($item->toUrl()->toString() === '') {
        throw \Exception('Missing url configuration for node');
      }
      else {

        $url = $item->toUrl()->toString();

      }
      return new RedirectResponse($url);

    }
    catch (\Exception $e) {

      return (new JsonResponse())->setData([$e->getMessage()]);
    }

  }

  public function equipmentNotFound() {
    return [
      '#children'=>'<div class="dashboard"><equipment-catalog-item-not-found></equipment-catalog-item-not-found></div>'
    ];
  }

}

