  (function ($) {
  'use strict';

  var pageRegex = new RegExp('[\?&]?page=[0-9\+]+[&]?');
  var buildFilters = function() {
    var searchParts = location.search.replace('?', '').split('&');
    $('.equipment-listing-filter').each(function() {
      var hasEquipmentFilters = false;
      for (var i in searchParts) {
        searchParts[i] = searchParts[i].split('=');
        if (searchParts[i].length == 2 && searchParts[i][0] == 'equipment_filters') {
          hasEquipmentFilters = true;
          if ($(this).is(':checked')) {
            if (searchParts[i][1].indexOf('+' + $(this).data('tid')) === -1
              && searchParts[i][1].indexOf($(this).data('tid') + '+') === -1
              && searchParts[i][1] != $(this).data('tid')
            ) {
              searchParts[i][1] += '+' + $(this).data('tid');
            }
          }
          else {
            var regex = new RegExp('[+]?' + $(this).data('tid'), 'g');
            searchParts[i][1] = searchParts[i][1].replace(regex, '');
          }
        }

        // Make sure we strip the whole param out of url when empty.
        if (searchParts[i][1] === '' || searchParts[i][0] === 'page') {
          searchParts.splice(i, 1);
        }
        else {
          // Rebuild search parts string.
          searchParts[i] = searchParts[i].join('=');
          searchParts[i] = searchParts[i].replace('=+', '=').replace('++', '+');
        }
      }

      // No equipment_filters?
      if (!hasEquipmentFilters && $(this).is(':checked')) {
        var index = searchParts.length;
        searchParts[index] = [];
        searchParts[index][0] = 'equipment_filters';
        searchParts[index][1] = $(this).data('tid');
        searchParts[index] = searchParts[index].join('=');
      }
    });

    return searchParts;
  };

  Drupal.behaviors.ur_equipment = {
    attach: function(context, drupalSettings) {
      $('.viewMore_link > a.view-more-toggle').once('view-more-toggle-click').on('click', function() {
        if (!$(this).hasClass('view-less')) {
          $(this).removeClass('view-more').addClass('view-less');
          $(this).text(Drupal.t('View Less'));
          $('.navigation-panel .navigation-panel__content.hidden').removeClass('hidden').addClass('visible');
        }
        else {
          $(this).removeClass('view-less').addClass('view-more');
          $(this).text(Drupal.t('View More'));
          $('.navigation-panel .navigation-panel__content.visible').removeClass('visible').addClass('hidden');
        }
        return false;
      });

      $('.more_less_toggle > a.toggle').once('more-less-toggle-click').on('click', function() {
        if (!$(this).hasClass('view-less')) {
          $(this).removeClass('view-more').addClass('view-less');
          $(this).text(Drupal.t('Show Less'));
          $($(this).data('target')).removeClass('less').addClass('more');
        }
        else {
          $(this).removeClass('view-less').addClass('view-more');
          $(this).text(Drupal.t('Show More'));
          $($(this).data('target')).removeClass('more').addClass('less');
        }
        return false;
      });

      // Add collapse toggle.
      $('.collapse-toggle').once('collapse-toggle-click').on('click', function() {
        if (!$(this).hasClass('collapsed')) {
          if (typeof $(this).data('trigger') !== 'undefined' && $(this).data('trigger') !== null
            && !$($(this).data('trigger')).first().hasClass('collapsed')
          ) {
            $($(this).data('trigger')).first().trigger('click');
          }
          $(this).removeClass('expanded').addClass('collapsed');

          // Update icon.
          if ($(this).hasClass('has-icon')) {
            $(this).siblings('.icon').addClass('rotate');
          } else if ($(this).hasClass('icon')) {
            $(this).addClass('rotate');
          }

          if ($(this).text() == Drupal.t('Collapse')) {
            $(this).text(Drupal.t('Expand'));
            $(this).parent().parent().siblings('.viewMore_link').removeClass('visible').addClass('hidden');
          }
          $($(this).data('target')).removeClass('visible').addClass('hidden');
        }
        else {
          if (typeof $(this).data('trigger') !== 'undefined' && $(this).data('trigger') !== null
            && $($(this).data('trigger')).first().hasClass('collapsed')
          ) {
            $($(this).data('trigger')).first().trigger('click');
          }
          $(this).removeClass('collapsed').addClass('expanded');

          // Update icon.
          if ($(this).hasClass('has-icon')) {
            $(this).siblings('.icon').removeClass('rotate');
          } else if ($(this).hasClass('icon')) {
            $(this).removeClass('rotate');
          }

          if ($(this).text() == Drupal.t('Expand')) {
            $(this).text(Drupal.t('Collapse'));
            $(this).parent().parent().siblings('.viewMore_link').removeClass('hidden').addClass('visible');
          }
          $($(this).data('target')).removeClass('hidden').addClass('visible');
        }
        return false;
      });

      // Click functionality for search term filters.
      $('.search-term-badge').once('search-term-badge-click').on('click', function() {
        var key = $(this).data('key');
        var tid = $(this).data('tid');
        var term = $(this).data('term');
        $(this).remove();
        if (location.search.includes(key)) {
          // Special equipment_filters stuff.
          if ((key == 'equipment_filters' && tid) || (key == 'search_within' && term)) {
            var tidOrTerm = tid;
            if (term) {
              tidOrTerm = term;
            }
            var searchParts = location.search.replace('?', '').split('&');
            for (var i in searchParts) {
              searchParts[i] = searchParts[i].split('=');
              if (searchParts[i].length == 2 && searchParts[i][0] == key) {
                var regex = new RegExp('[+]?' + tidOrTerm, 'g');
                searchParts[i][1] = searchParts[i][1].replace(regex, '');
              }

              // Make sure we strip the whole param out of url when empty.
              if (searchParts[i][1] === '') {
                searchParts.splice(i, 1);
              }
              else {
                // Rebuild search parts string.
                searchParts[i] = searchParts[i].join('=');
                searchParts[i] = searchParts[i].replace('=+', '=').replace('++', '+');
              }
            }
            location.search = searchParts.join('&').replace(pageRegex, '');
          }
          else {
            location.search = location.search.replace('?' + key + '=1', '')
              .replace('&' + key + '=1', '')
              .replace(pageRegex, '');
          }
        }
      });
      $('.search-term-badge-container .clear-all').once('search-term-badge-clear-all-click').on('click', function() {
        $('.search-term-badge').remove();
        location.search = location.search.replace('?recently_viewed=1', '')
          .replace('&recently_viewed=1', '')
          .replace('?favorites=1', '')
          .replace('&favorites=1', '')
          .replace(new RegExp('[\?&]?equipment_filters=[0-9\+]+[&]?'), '')
          .replace(new RegExp('[\?&]?search_within=[a-zA-Z0-9\+]+[&]?'), '')
          .replace(pageRegex, '');
      });

      // Change functionality for equipment-listing-filter.
      var timeoutId;
      $('.equipment-listing-filter').each(function() {
        $(this).once('equipment-listing-filter-change').on('change', function() {
          clearTimeout(timeoutId);

          // Wait 50ms in case someone gets click happy.
          timeoutId = setTimeout(function() {
            location.search = buildFilters().join('&')
            clearTimeout(timeoutId);
          }, 50);
        });
      });

      // Quickview toggle.
      $('.ur-quick-view-toggle').once('ur-quick-view-toggle-click').on('click', function() {
        var catClass = $(this).data('cat-class');
        if (typeof catClass !== 'undefined' && catClass != '') {
          $('#ur-quick-view-' + catClass).toggleClass('visible');
          if ($(this).hasClass('icon')) {
            $(this).toggleClass('rotate');
          }
          else {
            $(this).siblings('.icon').toggleClass('rotate');
          }
        }
      });

      // Mobile filter / sort buttons.
      $('.filter-sort-buttons .filters__button').once('filter-sort-buttons-click').on('click', function() {
        $('.filter-sort-buttons .filters__button').toggleClass('is-active');
        $('.block-equipment-listing-siderail-block').toggleClass('visible');
      });
    }
  }
})(jQuery);
