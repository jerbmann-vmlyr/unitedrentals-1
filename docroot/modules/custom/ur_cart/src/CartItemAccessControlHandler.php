<?php

namespace Drupal\ur_cart;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Cart Item entity.
 *
 * @see \Drupal\ur_cart\Entity\BaseListItem.
 */
class CartItemAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\ur_cart\Entity\BaseListItemInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cart item entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published cart item entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit cart item entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete cart item entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cart item entities');
  }

}
