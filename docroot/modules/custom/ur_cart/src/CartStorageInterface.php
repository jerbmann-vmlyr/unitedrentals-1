<?php

namespace Drupal\ur_cart;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the storage handler class for Cart entities.
 *
 * This extends the base storage class, adding required special handling for
 * Cart entities.
 *
 * @ingroup ur_cart
 */
interface CartStorageInterface extends ContentEntityStorageInterface {

  // @TODO - Do things in here.

}
