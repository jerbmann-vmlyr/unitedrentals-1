<?php

namespace Drupal\ur_cart;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the storage handler class for Cart entities.
 *
 * This extends the base storage class, adding required special handling for
 * Cart entities.
 *
 * @ingroup ur_cart
 */
class CartStorage extends SqlContentEntityStorage implements CartStorageInterface {

  /**
   * The current user (Anonymous or Authenticated).
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * This is a factory method that returns a new instance of this object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The dependency injection container service.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type manager service.
   *
   * @see \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   *
   * @return static
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('database'),
      $container->get('entity.manager'),
      $container->get('cache.entity'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * CartStorage constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity Type service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current_user service.
   *
   * @TODO: Fix this to modify buildQuery and any caching that we can.
   * The docblock above the Cart entity should lead the way to this silly thing.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    Connection $database,
    EntityManagerInterface $entity_manager,
    CacheBackendInterface $cache,
    LanguageManagerInterface $language_manager,
    EntityTypeManager $entity_type_manager,
    AccountProxy $current_user) {
    $this->entityTypeManager = $entity_type_manager;

    $this->currentUser = $current_user;

    $entity_type->set('persistent_cache', FALSE);

    parent::__construct($entity_type, $database, $entity_manager, $cache, $language_manager);
  }

  /**
   * Overriding parent::buildQuery().
   *
   * @param array|null $ids
   *   Array of Cart IDs or null.
   * @param bool $revision_id
   *   False if no revision ID is passed.
   *
   * @return \Drupal\Core\Database\Query\Select
   *   The Database select query we are going to load entities with.
   *
   * @throws \Exception
   */
  protected function buildQuery($ids, $revision_id = FALSE) {
    /** @var \Drupal\Core\Database\Query\Select $query */
    $query = parent::buildQuery($ids, $revision_id);
    $query->orderBy('changed', 'DESC');

    /*
     * Always only return one item (the most recently changed) unless we specify
     * IDs to load.
     */
    if (empty($ids)) {
      $query->range(0, 1);
    }

    /** @var \Drupal\Core\Session\SessionManager $sessionManager */
    $sessionManager = \Drupal::service('session_manager');

    $sid = $sessionManager->getId();
    $userId = $this->currentUser->id();

    // For anonymous users we only care about matching the session id.
    if ($userId == 0) {
      $query->condition('session_id', $sid);
    }
    /*
     * For authenticated users, the session id changes on login so it is
     * unusable to find a cart created before login.
     * We still need to be able to pull carts for user_id 0 so we can merge a
     * previously saved anonymous cart.
     */
    else {
      $orConditionGroup = $query->orConditionGroup()
        ->condition('user_id', $userId)
        ->condition('user_id', 0);

      $query->condition($orConditionGroup);
    }

    return $query;
  }

  /**
   * Loads a single Cart entity.
   *
   * Entity API load() method in D8 does not include $conditions[] as it did
   * in D7. However it does include a loadByProperties() method which will be
   * perfect for this.
   *
   * @see \Drupal\Core\Entity\EntityInterface
   */
  public function loadSingle($conditions, $id = FALSE) {
    // If we do have an $id, let's load the entity and return.
    if ($id !== FALSE) {
      return parent::load($id);
    }

    /** @var \Drupal\Core\Session\SessionManager $session_manager */
    $session_manager = \Drupal::service('session_manager');

    /*
     * If the session isn't started, start one
     *
     * @TODO: This still will not return TRUE for $session_manager->isStarted()
     * on first page load, however after that it works.
     */
    if ($this->currentUser->isAnonymous() && !isset($_SESSION['session_started'])) {
      return NULL;
    }

    if ($this->currentUser->isAuthenticated()) {
      $userId = $this->currentUser->id();
      $conditions['user_id'] = $userId;
    }

    if ($this->currentUser->isAnonymous()) {
      // Sets session and cookie variables.
      $this->setAnonymousVariables();

      // Try to load cart for anonymous via session.
      if (isset($_SESSION['anon_cart_id']) && !empty($_SESSION['anon_cart_id'])) {
        $conditions['id'] = $_SESSION['anon_cart_id'];
      }
      // Try to load cart for anonymous via cookie.
      elseif ($this->hasAnonymousCookie()) {
        $conditions['id'] = $this->getAnonymousCookie();
      }
      // Not in session or cookie, use default behavior.
      else {
        $conditions['session_id'] = $session_manager->getId();
      }
    }

    // This could return an array of Cart objects or an empty array.
    $entities = parent::loadByProperties($conditions);

    /** @var \Drupal\ur_cart\Entity\Cart $entity */
    return $entities;
  }

  /**
   * Helper to finish build Cart entity before returning.
   *
   * After we load up Cart entities, we want to get any associated CartItems for
   * the particular cart.
   *
   * @param array $entities
   *   An array of Cart entities.
   */
  public function postLoad(array &$entities) {
    // parent::postLoad($entities);
    /** @var \Drupal\ur_cart\CartItemStorage $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('cart_item');

    foreach ($entities as $cart) {
      $storage->conditions['cartId'] = $cart->id();
      // Pass the cart id as a condition for the query to get the corresponding cart items.
      $cart_items = $storage->loadCartItems(FALSE, ['cartId' => $cart->id()]);
      $cart->setCartItems($cart_items);

      // If the user is anonymous, track the current cart id in session for potential merging.
      if (\Drupal::currentUser()->isAnonymous() && empty($_SESSION['anon_cart_id'])) {
        $_SESSION['anon_cart_id'] = $cart->id();
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $values = []) {
    // Create the cart && cartSavedForLater entity and attach.
    $cart = parent::create($values);
    $saved_cart = parent::create($values);
    $saved_cart->set('saved_for_later', TRUE);
    return $cart;
  }

  /**
   * Delete all cart items prior to deleting this entity.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The Entity Storage object.
   * @param array $entities
   *   An array of entities keyed by entity ID.
   *
   * @TODO: Clear out CartItems from cart, then delete Cart entity
   *
   * Although there is no parent method for preDelete(), the parent::delete()
   * does allow for the call of a preDelete() method for each entity_type class.
   * @see \Drupal\Core\Entity\EntityStorageBase::delete()
   */
  public function preDelete(EntityStorageInterface $storage, array $entities) {
    // Delete all CartItems attached to this cart before we delete the Cart entity.
    // This will ensure there will be no orphaned CartItems in the database.
    $result = $this->clear($entities);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $entities) {
    // TODO: Change the autogenerated stub.
    return parent::delete($entities);
  }

  /**
   * Overriding the default parent::save().
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The Cart entity we are saving.
   *
   * @TODO: Hash out the save operation.
   * Brought everything over from ORS, commenting out what I believe we won't
   * need.
   *
   * @return bool|int
   *   False if saving failed, an integer success value.
   */
  public function save(EntityInterface $entity) {
    $time = time();
    /** @var \Drupal\Core\Session\AccountProxy $user */
    $user = $this->currentUser;

    if (empty($entity->uid) && !empty($user->id())) {
      $entity->uid = $user->id();
    }

    if (empty($entity->created)) {
      $entity->created = $time;
    }

    if (empty($entity->sid)) {
      // Note the session id changes when a user goes from anonymous to authenticated.
      $entity->sid = session_id();
    }

    if (empty($entity->changed) || empty($entity->is_new) || !empty($entity->{$this->idKey})) {
      // Then we need to update the changed timestamp.
      $entity->changed = $time;
    }

    // Process UR-flavored ISO-8601 times into UTC timestamps for the database.
    if (!empty($entity->date_from)) {
      // TODO: bring ur_date_convert method()
      // $entity->date_from = ur_date_convert($entity->date_from);.
    }

    if (!empty($entity->date_to)) {
      // TODO: bring ur_date_convert method()
      // $entity->date_to = ur_date_convert($entity->date_to);.
    }

    if (!empty($entity->date_to) && $entity->date_from > $entity->date_to) {
      // TODO: Fix calls to watchdog
      // watchdog('CartController', 'Updating To date to avoid saving cart with newer From date', array(), WATCHDOG_NOTICE);.
      $entity->date_to = $entity->date_from;
    }

    // session_store_cart_data($entity); will we need this?
    return parent::save($entity);
  }

  /**
   * If we're saving a Cart, we probably need to save the CartItems as well.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage object.
   *
   * @TODO: Setup pre-save operations
   */
  public function preSave(EntityStorageInterface $storage) {
    // Loop over CartItems in the cart, calling save() method individually.
  }

  /**
   * Creates a cart with the values passed, saves the cart, and returns the Cart.
   *
   * @param array $values
   *   An array of key, value pairs to create a new Cart entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Returns a newly created Cart entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createAndSave(array $values) {
    $entity = $this->create($values);
    $status = $this->save($entity);
    $acceptedStatuses = [SAVED_NEW, SAVED_UPDATED];

    if (in_array($status, $acceptedStatuses)) {
      /*
       * After creating a new cart, store the id in session if the user is
       * anonymous so we can  merge later.
       */
      if (\Drupal::currentUser()->isAnonymous()) {
        if (empty($_SESSION['anon_cart_id'])) {
          $_SESSION['anon_cart_id'] = $entity->id();
        }

        // Save id in cookie for anonymous users too.
        if (!$this->hasAnonymousCookie($entity->id())) {
          $this->setAnonymousCookie($entity->id());
        }
      }

      return $entity;
    }

    throw new EntityStorageException('There was an error creating the entity.');
  }

  /**
   * Cleans data arrays to avoid unwanted errors.
   *
   * @param array $data
   *   The data that we would like cleaned up.
   *
   * @return \stdClass
   *   An object with cleaned values.
   *
   * @throws \Exception
   */
  private function cleanData(array $data) {
    if (empty($data)) {
      return new \stdClass();
    }

    $data = (array) $data;
    $tempData = new \stdClass();

    /*
     * Clean the data values to avoid the following error:
     *    PHP Fatal error: Cannot access property started with '\0'
     */
    foreach ($data as $index => $value) {
      if ($index == "\0" || $index == '\0'
        || empty($index) || ord($index[0]) === 0) {
        continue;
      }

      if ($value == "\0" || $value == '\0') {
        continue;
      }

      $tempData->$index = $value;
    }

    return $tempData;
  }

  /**
   * Retrieves anonymous cookie.
   *
   * @TODO all these values should be leveraging the following Drupal service
   * (instead of setting cookies and session data directly):
   * https://api.drupal.org/api/drupal/core!modules!user!src!PrivateTempStore.php/class/PrivateTempStore/8.2.x
   */
  public function getAnonymousCookie() {
    $cookie = NULL;
    if ($this->hasAnonymousCookie()) {
      $cookie = $_COOKIE['anon_cart_id'];
    }

    return $cookie;
  }

  /**
   * Checks to see if we have anonymous cookie set.
   *
   * @TODO all these values should be leveraging the following Drupal service
   * (instead of setting cookies and session data directly):
   * https://api.drupal.org/api/drupal/core!modules!user!src!PrivateTempStore.php/class/PrivateTempStore/8.2.x
   */
  public function hasAnonymousCookie($cartId = NULL) {
    $hasCookie = FALSE;
    if (isset($_COOKIE['anon_cart_id']) && !empty($_COOKIE['anon_cart_id'])) {
      $hasCookie = TRUE;
      if (!empty($cartId) && $cartId != $_COOKIE['anon_cart_id']) {
        $hasCookie = FALSE;
      }
    }

    return $hasCookie;
  }

  /**
   * Sets anonymous cookie.
   *
   * @TODO all these values should be leveraging the following Drupal service
   * (instead of setting cookies and session data directly):
   * https://api.drupal.org/api/drupal/core!modules!user!src!PrivateTempStore.php/class/PrivateTempStore/8.2.x
   */
  protected function setAnonymousCookie($cartId) {
    setcookie('anon_cart_id', $cartId, strtotime('+30 days'), '/');
  }

  /**
   * Sets anonymous variables.
   *
   * @TODO all these values should be leveraging the following Drupal service
   * (instead of setting cookies and session data directly):
   * https://api.drupal.org/api/drupal/core!modules!user!src!PrivateTempStore.php/class/PrivateTempStore/8.2.x
   */
  protected function setAnonymousVariables() {
    // Cart id for anonymous is in session.
    if (isset($_SESSION['anon_cart_id']) && !empty($_SESSION['anon_cart_id'])) {
      // Set the cookie if we have it in session.
      if (!$this->hasAnonymousCookie($_SESSION['anon_cart_id'])) {
        $this->setAnonymousCookie($_SESSION['anon_cart_id']);
      }
    }
    // Cart id for anonymous is in cookie.
    elseif ($this->hasAnonymousCookie()) {
      // Set the session variable if we have it in cookie.
      if (!isset($_SESSION['anon_cart_id']) || empty($_SESSION['anon_cart_id'])
        || $_SESSION['anon_cart_id'] != $this->getAnonymousCookie()
      ) {
        $_SESSION['anon_cart_id'] = $this->getAnonymousCookie();
      }
    }
  }

}
