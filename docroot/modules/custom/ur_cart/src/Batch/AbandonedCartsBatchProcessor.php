<?php

namespace Drupal\ur_cart\Batch;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerManager;
use Drupal\Core\Queue\SuspendQueueException;

define("NUMBER_OF_BATCHES", 10);
/**
 * @inheritdoc
 */
class AbandonedCartsBatchProcessor {

  /**
   * @return array The definition of the batch job
   */
  public function createBatch() {
    $batch = [
      'title' => t('Deleting Carts...'),
      'operations' => [],
      'finished' => 'Drupal\ur_cart\Batch\AbandonedCartsBatchProcessor::batchFinished',
    ];

    $batch['operations'][] = [
      'Drupal\ur_cart\Batch\AbandonedCartsBatchProcessor::preImport',
      []
    ];

    $batch['operations'][] = [
      'Drupal\ur_cart\Batch\AbandonedCartsBatchProcessor::getAndQueueCartData',
      []
    ];

    for ($i = 0; $i < NUMBER_OF_BATCHES; $i++) {
      $batch['operations'][] = [
        'Drupal\ur_cart\Batch\AbandonedCartsBatchProcessor::batchProcess',
        []
      ];
    }

    return $batch;
  }

  /**
   * @param $context
   *   array Access to message
   *   The first step in the batch process.
   *   Because the next step can take a while, this step ensures the user sees
   *   a message about that before the next step starts.
   */
  public function preImport(&$context) {
    $message = 'Getting abandoned carts, this could take a while...';
    \Drupal::logger('abandoned_carts')->info($message);
    $context['message'] = t($message);
  }

  /**
   * The second step in the batch process.  It gets the abandoned cart information
   * and queues it for processing in the batch.
   * @param $context
   *   array Access to message
   * @throws \Exception
   */
  public function getAndQueueCartData(&$context) {
    \Drupal::logger('abandoned_carts')->info('Getting abandoned carts');
    $config = \Drupal::config('ur_cart.settings');
    $days = $config->get('abandoned_carts_max_age_in_days');
    $daysAgo = strtotime("-$days day");
    set_time_limit(240);
    $query = \Drupal::entityQuery('cart');
    $orGroup = $query->orConditionGroup()
      ->condition('saved_for_later', 'false', '=')
      ->condition('saved_for_later', NULL, 'IS NULL');
    $query->condition('changed', $daysAgo, '<');
    $query->condition($orGroup);
    $carts = $query->execute();

    if (count($carts) > 0) {
      /** @var \Drupal\Core\Queue\QueueFactory $queueFactory */
      $queueFactory = \Drupal::service('queue');

      /** @var \Drupal\Core\Queue\QueueInterface $queue */
      $queue = $queueFactory->get('carts_queue');

      // In case old items are not all processed from previous run, empty queue.
      $queue->deleteQueue();

      \Drupal::logger('abandoned_carts')->info('Queueing abandoned cart data for deletion');

      foreach ($carts as $cart) {
        $item = new \stdClass();
        $item->cart = $cart;
        $queue->createItem($item);
      }

      $message = 'Finished getting cart data. There are ' . $queue->numberOfItems() . ' carts to delete';
      \Drupal::logger('abandoned_carts')->info($message);
      $context['message'] = t($message);
      $maxBatchSize = ceil($queue->numberOfItems() / NUMBER_OF_BATCHES);
      $context['results']['maxBatchSize'] = $maxBatchSize;
    }
  }

  /**
   * @param $context
   *   array Provides access to the number of carts to import between calls to batchProcess
   *   Process and import a batch of carts from the queue, runs NUMBER_OF_BATCHES times
   */
  public static function batchProcess(&$context) {
    \Drupal::logger('abandoned_carts')
      ->info('Abandoned Cart delete batch process step starting');

    /** @var \Drupal\Core\Queue\QueueFactory $queueFactory */
    $queueFactory = \Drupal::service('queue');

    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = $queueFactory->get('carts_queue');

    /** @var \Drupal\Core\Queue\QueueWorkerManager $queueManager */
    $queueManager = \Drupal::service('plugin.manager.queue_worker');
    $queueWorker = $queueManager->createInstance('carts_queue');
    $currentBatchSize = min($queue->numberOfItems(), $context['results']['maxBatchSize']);
    $numProcessed = 0;
    for ($i = 0; $i < $currentBatchSize; $i++) {
      if ($item = $queue->claimItem()) {
        try {
          $queueWorker->processItem($item);
          $queue->deleteItem($item);
          $numProcessed++;
        }
        catch (SuspendQueueException $e) {
          $queue->releaseItem($item);
          break;
        }
        catch (\Exception $e) {
          $message = $e->getMessage();
          \Drupal::logger('abandoned_carts')
            ->error('Error deleting carts %message', ['%message' => $message]);
          $queue->releaseItem($item);
        }
      }
    }

    \Drupal::logger('abandoned_carts')
      ->info('Abandoned Cart delete batch process step finished');
    $context['message'] =
      t('%numProcessed Abandoned Carts deleted, %numberInQueue more remain',
        [
    '%numberInQueue' => $queue->numberOfItems(),
          '%numProcessed' => $numProcessed
]);
  }

  /**
   * Batch finished callback.
   */
  public static function batchFinished($success, $results, $operations) {
    $message = "";
    if ($success) {
      $message = "Batch Process Finished. The Abandoned Carts were deleted successfully.";
      \Drupal::messenger()->addMessage(t($message));
      \Drupal::logger('abandoned_carts')->info($message);
    }
    else {
      $error_operation = reset($operations);
      $message = t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE)
      ]);
      \Drupal::messenger()->addMessage(t($message));
      \Drupal::logger('abandoned_carts')->error($message);
    }
  }

}
