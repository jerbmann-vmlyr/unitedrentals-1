<?php

namespace Drupal\ur_cart\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\ur_cart\Entity\Cart;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @inheritdoc
 */
class AbandonedCartsWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    \Drupal::logger('abandoned_carts')
      ->info('Abandoned Carts Worker created');
    return new static([], $plugin_id, $plugin_definition);
  }

  /**
   * Processes a single item of Queue.
   *
   * @param $item
   *   mixed The cart data to be processed
   *
   * @throws \Exception
   */
  public function processItem($item) {
    // This can be initiated manually via Configuration->United Rentals Settings->Administer Abandoned Carts or drush cart-delete.
    if ($cartId = $item->data->cart) {
      $this->deleteCart($cartId);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function deleteCart($cartId) {
    \Drupal::logger('abandoned_carts')
      ->debug('Abandoned Carts Worker processing cart id: %cart_id', ['%cart_id' => $cartId]);
    $connection = \Drupal::database();
    $this->deleteCartItems($cartId, $connection);
    // Using connection->delete instead of EntityQuery because of special logic in CartStorage::buildQuery.
    $connection->delete('cart')->condition('id', $cartId)->execute();
    \Drupal::logger('abandoned_carts')
      ->debug('Abandoned Carts Worker deleted cart id: %cart_id', ['%cart_id' => $cartId]);
  }

  /**
   * {@inheritdoc}
   */
  protected function deleteCartItems($cartId, $connection) {
    // Using connection->delete instead of EntityQuery because of special logic in CartStorage::buildQuery.
    $numDeleted = $connection->delete('cart_item')->condition('cartId', $cartId)->execute();
    \Drupal::logger('abandoned_carts')
      ->debug('Abandoned Carts Worker deleted %numDeleted cart items for %cart_id',
        ['%cart_id' => $cartId, '%numDeleted' => $numDeleted]);
  }

}
