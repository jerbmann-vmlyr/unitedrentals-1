<?php

namespace Drupal\ur_cart\Plugin\QueueWorker;

/**
 *
 * @QueueWorker(
 * id = "carts_queue",
 * title = "Cron Abandoned Carts Worker",
 * cron = {"time" = 10}
 * )
 */
class CronAbandonedCartsWorker extends AbandonedCartsWorker {

}
