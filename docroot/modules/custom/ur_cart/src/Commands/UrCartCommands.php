<?php

namespace Drupal\ur_cart\Commands;

use Drush\Commands\DrushCommands;
use Drupal\ur_cart\Batch\AbandonedCartsBatchProcessor;

/**
 * Class UrCartCommands
 * @package Drupal\ur_cart\Commands
 */
class UrCartCommands extends DrushCommands {

  /**
   * Delete abandoned carts
   *
   * @command ur_cart:delete-abandoned-carts
   * @aliases ur-delete-carts
   * @validate-module-enabled ur_cart
   */
  public function deleteAbandonedCarts() {
    $this->output()->writeln(dt('Delete Carts started via Drush'));
    $this->logger()->info(dt('Delete Carts started via Drush'));

    $batchProcessor = new AbandonedCartsBatchProcessor();
    $batch = $batchProcessor->createBatch();
    batch_set($batch);
    drush_backend_batch_process();

    $this->output()->writeln(dt('Batch Process Finished'));
  }

}
