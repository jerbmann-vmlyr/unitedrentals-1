<?php

namespace Drupal\ur_cart\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\ur_cart\Controller\CartController;
use Drupal\ur_list\Entity\BaseListItem;

/**
 * Defines the Cart Item entity.
 *
 * @ingroup ur_cart
 *
 * @ContentEntityType(
 *   id = "cart_item",
 *   label = @Translation("Cart Item"),
 *   handlers = {
 *     "storage" = "Drupal\ur_cart\CartItemStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ur_cart\CartItemListBuilder",
 *     "views_data" = "Drupal\ur_cart\Entity\CartItemViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\ur_cart\Form\CartItemForm",
 *       "add" = "Drupal\ur_cart\Form\CartItemForm",
 *       "edit" = "Drupal\ur_cart\Form\CartItemForm",
 *       "delete" = "Drupal\ur_cart\Form\CartItemDeleteForm",
 *     },
 *     "access" = "Drupal\ur_cart\CartItemAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\ur_cart\CartItemHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "cart_item",
 *   admin_permission = "adminis¶ter cart item entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/cart_item/{cart_item}",
 *     "add-form" = "/admin/structure/cart_item/add",
 *     "edit-form" = "/admin/structure/cart_item/{cart_item}/edit",
 *     "delete-form" = "/admin/structure/cart_item/{cart_item}/delete",
 *     "collection" = "/admin/structure/cart_item",
 *   },
 *   field_ui_base_route = "cart_item.settings"
 * )
 */
class CartItem extends BaseListItem implements CartItemInterface {

  /**
   * Account ID getter.
   *
   * @return string
   *   The account ID.
   */
  public function getAccountId() {
    return $this->get('accountId')->value;
  }

  /**
   * Available getter.
   *
   * @return bool
   *   Whether this cart item is available for checkout.
   */
  public function isAvailable() {
    return $this->get('available')->value;
  }

  /**
   * The branch ID for this cart item.
   *
   * @return string
   *   The branch ID.
   */
  public function getBranchId() {
    return $this->get('branchId')->value;
  }

  /**
   * Cart ID getter.
   *
   * @return int
   *   The cart ID this item belongs to.
   */
  public function getCartId() {
    return $this->get('cartId')->value;
  }

  /**
   * Sets the Cart ID property.
   *
   * @param int $cartId
   *   The new cart id.
   *
   * @return \Drupal\ur_cart\Entity\CartItem
   *   This object.
   */
  public function setCartId($cartId) {
    return $this->set('cartId', $cartId);
  }

  /**
   * The cat-class of the cart item.
   *
   * @return string
   *   The cat-class code.
   */
  public function getCatClass() {
    return $this->get('catClass')->value;
  }

  /**
   * Whether or not this cart item is getting delivered.
   *
   * @return bool
   *   True if delivery, false if not.
   */
  public function isDelivery() {
    return $this->get('delivery')->value;
  }

  /**
   * The jobsite this cart item will be going to.
   *
   * @return string
   *   The jobsite ID.
   */
  public function getJobsiteId() {
    return $this->get('jobsiteId')->value;
  }

  /**
   * The location at which we are purchasing from.
   *
   * @return string
   *   An address string.
   */
  public function getLocationString() {
    return $this->get('locationString')->value;
  }

  /**
   * The comments getter.
   *
   * @return string
   *   User notes about the specific cart item.
   */
  public function getNotes() {
    return $this->get('notes')->value;
  }

  /**
   * The changed getter.
   *
   * @return string
   *   Changed UNIX Timestamp.
   */
  public function getChanged() {
    return $this->get('changed')->value;
  }

  /**
   * Whether this item is being picked up by UR.
   *
   * @TODO: Convert "1"/"0" string to boolean TRUE/FALSE
   *
   * @return bool
   *   True if yes, false if no.
   */
  public function isPickup() {
    return $this->get('pickup')->value;
  }

  /**
   * Project ID getter. (Currently not in use)
   *
   * @return string
   *   The project ID.
   */
  public function getProjectId() {
    return $this->get('projectId')->value;
  }

  /**
   * Quantity getter.
   *
   * @return int
   *   The quantity of items for this particular cart item.
   */
  public function getQuantity() {
    return $this->get('quantity')->value;
  }

  /**
   * Requester ID getter.
   *
   * @return int
   *   The requester's ID from the DAL.
   */
  public function getRequesterId() {
    return $this->get('requesterId')->value;
  }

  /**
   * Start Date getter.
   *
   * @TODO: Here we could pass a boolean to format the date a particular way
   *
   * @return \Drupal\ur_cart\Entity\datetime
   *   The date timestamp start date for this cart item.
   */
  public function getStartDate() {
    return $this->get('startDate')->value;
  }

  /**
   * End Date getter.
   *
   * @TODO: Here we could pass a boolean to format the date a particular way
   *
   * @return \Drupal\ur_cart\Entity\datetime
   *   The date timestamp end date for this cart item.
   */
  public function getEndDate() {
    return $this->get('endDate')->value;
  }

  /**
   * Helper method for formatting dates.
   *
   * @param string $date
   *   The date string to filter.
   *
   * @return string
   *   The formatted date string.
   */
  public function filterDateSave($date) {
    if (!empty($date) && strlen($date) > 19) {
      $date = substr($date, 0, 19);
    }

    return $date;
  }

  /**
   * Sets the start date property.
   *
   * @param string $date
   *   The start date string.
   *
   * @return \Drupal\ur_cart\Entity\CartItem
   *   This object.
   */
  public function setStartDate($date) {
    $this->set('startDate', $this->filterDateSave($date));
    return $this;
  }

  /**
   * Sets the changed property to now.
   *
   * @return \Drupal\ur_cart\Entity\CartItem
   *   This object.
   */
  public function setChanged() {
    $this->set('changed', time());
    return $this;
  }

  /**
   * End date setter.
   *
   * @param string $date
   *   The end date string.
   *
   * @return \Drupal\ur_cart\Entity\CartItem
   *   This object.
   */
  public function setEndDate($date) {
    $this->set('endDate', $this->filterDateSave($date));
    return $this;
  }

  /**
   * Helper method to set both start and end dates.
   *
   * Run this to make sure that we filter the dates properly before saving.
   */
  public function reSaveDates() {
    $this->setStartDate($this->getStartDate());
    $this->setEndDate($this->getEndDate());
  }

  /**
   * When we go to array we want formatted dates for the front-end.
   *
   * @return array
   *   The array of values that were converted on this object.
   */
  public function toArray() {
    $values = parent::toArray();
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    $this->reSaveDates();
    parent::preSave($storage);
  }

  /**
   * Assembles fields and updates the values for the current item.
   *
   * @param array $values
   *   The values we are updating.
   *
   * @throws \Exception
   */
  public function assembleAndUpdateFields(array $values) {
    self::assembleFields($values);
    $this->updateFields($values);
    $this->save();
  }

  /**
   * Helper to assemble values on the cart item entity.
   *
   * @param array $values
   *   (Passed by reference)
   */
  public static function assembleFields(array &$values) {
    if (empty($values['cartId'])) {
      /** @var \Drupal\ur_cart\Entity\Cart $cart */
      $cart = CartController::grabCurrentCart();
      $values['cartId'] = $cart->id();
    }

    if (empty($values['available'])) {
      // @TODO: Eventually this needs to be tied to DAL data
      $values['available'] = 1;
    }
  }

  /**
   * Updates the fields with new values that are passed in.
   *
   * @param array $values
   *   The key-value pair of values we are updating.
   *
   * @throws \Exception
   */
  public function updateFields(array $values) {
    // Foreach of those key => value pairs, re-set or update the value.
    foreach ($values as $name => $value) {
      $this->set($name, $value);
    }
  }

  /**
   * Helper to determine if we can merge a cart item into this cart item.
   *
   * Jobsite ID,Start Date, End Date, and Cat Class all need to be equal to merge.
   *
   * @param CartItem $mergeItem
   *   The Cart Item we are attempting to merge into this one.
   *
   * @return bool
   *   True if we can merge the item, false if we can not.
   */
  public function canMerge(CartItem $mergeItem) {
    /*
     * We have a logic problem with the quantity here. Because Drupal loads the
     * merge item by ID, it essentially replaces the item. This means that when
     * you try to add the two quantities together you actually get the new
     * quantity twice.
     *
     * Thus this merge operation SHOULD NOT be run when the
     * current item and the merge item have the same Item ID.
     *
     * Instead, we need to make sure that quantity increment works upon loading.
     */
    return ($this->id() != $mergeItem->id()
      && $this->getJobsiteId() === $mergeItem->getJobsiteId()
      && $this->getStartDate() === $mergeItem->getStartDate()
      && $this->getEndDate() === $mergeItem->getEndDate()
      && $this->getCatClass() === $mergeItem->getCatClass());
  }

  /**
   * Merges a cart item with this cart item.
   *
   * @param CartItem $mergeItem
   *   The Cart Item to merge with this Cart Item.
   *
   * @throws \InvalidArgumentException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function merge(CartItem $mergeItem) {
    $newQuantity = $this->getQuantity() + $mergeItem->getQuantity();
    $this->set('quantity', $newQuantity);
    $this->set('notes', $mergeItem->getNotes());

    $this->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    /*
     * asterisk means required when saving
     * id
     * accountId
     * available - @TODO: Ensure synchronicity with charge estimates.
     * branchId
     * cartId *
     * catClass *
     * delivery
     * jobsiteId
     * locationString
     * notes
     * pickup
     * projectId
     * quantity
     * requesterId
     * startDate
     * endDate
     */

    $fields['accountId'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Account ID'))
      ->setDescription(t('The selected account ID.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'max_length' => 7,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    $fields['available'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is this available or not currently?'))
      ->setDescription(t('Indicates that the current item is available. When FALSE the item cannot be checked out.'))
      ->setTranslatable(FALSE)
      ->setReadOnly(TRUE);

    $fields['branchId'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Branch ID'))
      ->setDescription(t('The ID of the selected branch.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'max_length' => 3,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    $fields['cartId'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Cart ID'))
      ->setDescription(t('The ID of the associated cart.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'max_length' => 10,
      ])
      ->setDefaultValue(NULL);

    $fields['catClass'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Cat Class Code'))
      ->setDescription(t('The Category-Class code of the current item.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'max_length' => 8,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    $fields['delivery'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Delivery for this item?'))
      ->setTranslatable(FALSE)
      ->setDescription(t('Is this item being delivered .'));

    $fields['jobsiteId'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Jobsite ID'))
      ->setDescription(t('The selected jobsite ID.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'max_length' => 30,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    $fields['locationString'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Location String'))
      ->setDescription(t('A user selected location string consisting of City, State, and Zip.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'max_length' => 128,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    $fields['notes'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Item notes'))
      ->setDescription(t('Notes about this item.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'max_length' => 128,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    $fields['pickup'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is this item being picked up at a United Rentals branch?'))
      ->setTranslatable(FALSE)
      ->setDescription(t('Indicates that the current item is available. When FALSE the item cannot be checked out.'));

    $fields['pickupFirm'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Pickup Firm'))
      ->setDescription(t('True if the customer has a requested a firm pickup time.'))
      ->setDefaultValue(FALSE);

    $fields['projectId'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The selected project ID.'))
      ->setTranslatable(FALSE)
      ->setSettings([
    // Length is unknown so I am just making it long.
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    $fields['quantity'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Quantity'))
      ->setDescription(t('The count of this item in the cart.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'max_length' => 10,
      ])
      ->setDefaultValue(1);

    $fields['requesterId'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Requester ID'))
      ->setDescription(t('The ID of the requester of this item.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    $fields['startDate'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Start Date'))
      ->setTranslatable(FALSE)
      ->setDescription(t('The date/time that the item(s) will be delivered.'));

    $fields['endDate'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('End Date'))
      ->setTranslatable(FALSE)
      ->setDescription(t('The date/time that the item(s) will be returned.'));

    return $fields;
  }

}
