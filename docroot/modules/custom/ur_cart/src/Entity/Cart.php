<?php

namespace Drupal\ur_cart\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\ur_list\Entity\BaseList;

/**
 * Defines the Cart entity.
 *
 * @ingroup ur_cart
 *
 * @ContentEntityType(
 *   id = "cart",
 *   label = @Translation("Cart"),
 *   handlers = {
 *     "storage" = "Drupal\ur_cart\CartStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ur_cart\CartListBuilder",
 *     "views_data" = "Drupal\ur_cart\Entity\CartViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\ur_cart\Form\CartForm",
 *       "add" = "Drupal\ur_cart\Form\CartForm",
 *       "edit" = "Drupal\ur_cart\Form\CartForm",
 *       "delete" = "Drupal\ur_cart\Form\CartDeleteForm",
 *     },
 *     "access" = "Drupal\ur_cart\CartAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\ur_cart\CartHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "cart",
 *   admin_permission = "administer cart entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/cart/{cart}",
 *     "add-form" = "/admin/structure/cart/add",
 *     "edit-form" = "/admin/structure/cart/{cart}/edit",
 *     "delete-form" = "/admin/structure/cart/{cart}/delete",
 *     "collection" = "/admin/structure/cart",
 *   },
 *   field_ui_base_route = "cart.settings"
 * )
 *
 * @TODO - Read up on changing the Entity Type and then modifying the Entity
 *    Storage, which will extend the \Drupal\Core\Entity\Sql\SqlContentEntityStorage
 *    class so that we can modify the buildQuery method and any caching.
 *
 * https://www.drupalwatchdog.com/blog/2015/3/entity-storage-drupal-8-way
 */
class Cart extends BaseList implements CartInterface {
  /**
   * An array of Cart Items that belong to this Cart object.
   *
   * @var array\Drupal\ur_cart\Entity\CartItem*/
  protected $cartItems = [];

  /**
   * Getter for Cart Items.
   */
  public function getCartItems() {
    return $this->cartItems;
  }

  /**
   * Cart Item setter.
   *
   * @param array $cart_items
   *   The array of Cart Items to set on the Cart object.
   */
  public function setCartItems(array $cart_items) {
    $this->cartItems = $cart_items;
  }

  /**
   * Adds a cart item to the cart.
   *
   * Checks each existing item to see if they can be merged first.
   *
   * @param CartItem $cart_item
   *   The Cart Item object we are adding to this Cart.
   */
  public function addCartItem(CartItem $cart_item) {
    if (!empty($this->cartItems)) {
      /** @var \Drupal\ur_cart\Entity\CartItem $item */
      foreach ($this->cartItems as $item) {
        // Check if the two cart items can be merged.
        if ($item->canMerge($cart_item)) {
          $item->merge($cart_item);

          // Delete the extra cart item.
          if ($item->id() !== $cart_item->id()) {
            $cart_item->delete();
          }
        }
        else {
          $this->cartItems[$cart_item->id()] = $cart_item;
        }
      }
    }
    else {
      $this->cartItems[$cart_item->id()] = $cart_item;
    }
  }

  /**
   * Removes a Cart Item from the cart.
   */
  public function removeCartItem(CartItem $cart_item) {
    unset($this->cartItems[$cart_item->id()]);
  }

  /**
   * Gets a specific Cart Item from the cart.
   */
  public function retrieveCartItem($cart_item_id) {
    return $this->cartItems[$cart_item_id];
  }

  /**
   * Cart ID getter.
   */
  public function getCartId() {
    return $this->get('id')->value;
  }

  /**
   * Branch ID getter.
   */
  public function getBranchId() {
    return $this->get('branch_id')->value;
  }

  /**
   * Requester ID getter.
   */
  public function getRequesterId() {
    return $this->get('requester_id')->value;
  }

  /**
   * Account ID getter.
   */
  public function getAccountId() {
    return $this->get('account_id')->value;
  }

  /**
   * Jobsite ID getter.
   */
  public function getJobsiteId() {
    return $this->get('jobsite_id')->value;
  }

  /**
   * Project ID getter.
   */
  public function getProjectId() {
    return $this->get('project_id')->value;
  }

  /**
   * Start date getter.
   */
  public function getStartDate() {
    return $this->get('start_date')->value;
  }

  /**
   * End date getter.
   */
  public function getEndDate() {
    return $this->get('end_date')->value;
  }

  /**
   * Is this a Saved For Later cart?
   *
   * @return bool
   *   The value from the saved_for_later property.
   */
  public function isSavedForLater() {
    return $this->get('saved_for_later')->value;
  }

  /**
   * Set this as a Saved For Later cart?
   *
   * @return bool
   *   Success or Failuer
   */
  public function toggleSavedForLater() {
    if ($this->isSavedForLater()) {
      return $this->set('saved_for_later', FALSE);
    }
    return $this->set('saved_for_later', TRUE);
  }

  /**
   * Started Checkout getter.
   *
   * @TODO: Convert "1"/"0" to TRUE/FALSE
   *
   * @return string
   *   The value from the started_checkout property.
   */
  public function hasStartedCheckout() {
    return $this->get('started_checkout')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    /*
     * Inherited Fields:
     *   cart_id (id)
     *   uid (user_id)
     *   sid (session_id)
     *   created
     *   changed
     *
     * Default values stored here:
     *   date_from
     *   date_to
     *   branch_id
     *   account_id
     *   jobsite_id
     *   project_id
     *   requester_id - Defaults to user guid (until told otherwise by UR IT)
     *   saved_for_later
     */

    // Name of cart (for future multi-cart)
    $fields['branch_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Branch ID'))
      ->setDescription(t('The ID of the selected branch.'))
      ->setSettings([
        'max_length' => 3,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    // Name of cart (for future multi-cart)
    $fields['requester_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Requester ID'))
      ->setDescription(t('The DAL guid of the requester of this cart.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    $fields['account_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Account ID'))
      ->setDescription(t('The selected account ID.'))
      ->setSettings([
        'unsigned' => TRUE,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    $fields['jobsite_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Jobsite ID'))
      ->setDescription(t('The selected jobsite ID.'))
      ->setSettings([
        'max_length' => 30,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The selected project ID.'))
      ->setSettings([
    // Length is unknown so I am just making it long.
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL);

    $fields['saved_for_later'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Saved For Later'))
      ->setDescription(t('This cart is used to hold saved-for-later items for the cart owner.'))
      ->setDefaultValue(FALSE);

    $fields['start_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Start Date'))
      ->setDescription(t('The date/time that the item(s) will be delivered.'));

    $fields['end_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('End Date'))
      ->setDescription(t('The date/time that the item(s) will be returned.'));

    $fields['started_checkout'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Has Started Checkout'))
      ->setDescription(t('True if the cart has started into the checkout flow. False by default.'))
      ->setDefaultValue(FALSE);

    return $fields;
  }

}
