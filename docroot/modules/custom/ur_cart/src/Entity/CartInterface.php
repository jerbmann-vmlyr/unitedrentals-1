<?php

namespace Drupal\ur_cart\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cart entities.
 *
 * @ingroup ur_cart
 */
interface CartInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Cart name.
   *
   * @return string
   *   Name of the Cart.
   */
  public function getName();

  /**
   * Sets the Cart name.
   *
   * @param string $name
   *   The Cart name.
   *
   * @return \Drupal\ur_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setName($name);

  /**
   * Gets the Cart creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cart.
   */
  public function getCreatedTime();

  /**
   * Sets the Cart creation timestamp.
   *
   * @param int $timestamp
   *   The Cart creation timestamp.
   *
   * @return \Drupal\ur_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Cart published status indicator.
   *
   * Unpublished Cart are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Cart is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Cart.
   *
   * @param bool $published
   *   TRUE to set this Cart to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\ur_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setPublished($published);

}
