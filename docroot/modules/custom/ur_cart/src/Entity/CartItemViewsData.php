<?php

namespace Drupal\ur_cart\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Cart Item entities.
 */
class CartItemViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
