<?php

namespace Drupal\ur_cart\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cart Item entities.
 *
 * @ingroup ur_cart
 */
interface CartItemInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Cart Item name.
   *
   * @return string
   *   Name of the Cart Item.
   */
  public function getName();

  /**
   * Sets the Cart Item name.
   *
   * @param string $name
   *   The Cart Item name.
   *
   * @return \Drupal\ur_cart\Entity\BaseListItemInterface
   *   The called Cart Item entity.
   */
  public function setName($name);

  /**
   * Gets the Cart Item creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cart Item.
   */
  public function getCreatedTime();

  /**
   * Sets the Cart Item creation timestamp.
   *
   * @param int $timestamp
   *   The Cart Item creation timestamp.
   *
   * @return \Drupal\ur_cart\Entity\BaseListItemInterface
   *   The called Cart Item entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Cart Item published status indicator.
   *
   * Unpublished Cart Item are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Cart Item is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Cart Item.
   *
   * @param bool $published
   *   TRUE to set this Cart Item to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\ur_cart\Entity\BaseListItemInterface
   *   The called Cart Item entity.
   */
  public function setPublished($published);

}
