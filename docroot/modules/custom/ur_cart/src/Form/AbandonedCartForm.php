<?php

namespace Drupal\ur_cart\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ur_cart\Batch\AbandonedCartsBatchProcessor;

/**
 * Class AbandonedCartForm.
 *
 * @package Drupal\ur_cart\Form
 */
class AbandonedCartForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'abandoned_cart_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_cart.settings');
    $maxAge = $config->get('abandoned_carts_max_age_in_days');
    $form['help'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Submitting this form will delete abandoned shopping carts more than %maxAge days old.<br><br>', ['%maxAge' => $maxAge])
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Delete Abandoned Carts',

    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batchProcessor = new AbandonedCartsBatchProcessor();
    $batch = $batchProcessor->createBatch();
    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_cart.settings'];
  }

}
