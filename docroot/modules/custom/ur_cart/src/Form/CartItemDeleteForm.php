<?php

namespace Drupal\ur_cart\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cart Item entities.
 *
 * @ingroup ur_cart
 */
class CartItemDeleteForm extends ContentEntityDeleteForm {


}
