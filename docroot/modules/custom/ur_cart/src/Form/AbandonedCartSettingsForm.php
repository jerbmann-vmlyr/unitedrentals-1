<?php

namespace Drupal\ur_cart\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AbandonedCartSettingsForm.
 *
 * @package Drupal\ur_cart\Form
 */
class AbandonedCartSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'abandoned_cart_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_cart.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_cart.settings');
    $form['abandoned_carts_max_age_in_days'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cart maximum age'),
      '#description' => $this->t('The maximum age in days before abandoned shopping carts are deleted'),
      '#default_value' => $config->get('abandoned_carts_max_age_in_days'),
      '#maxlength' => 4,
      '#size' => 5,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_cart.settings');
    $config
      ->set('abandoned_carts_max_age_in_days', $form_state->getValue('abandoned_carts_max_age_in_days'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
