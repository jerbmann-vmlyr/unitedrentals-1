<?php

namespace Drupal\ur_cart\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cart entities.
 *
 * @ingroup ur_cart
 */
class CartDeleteForm extends ContentEntityDeleteForm {


}
