<?php

namespace Drupal\ur_cart;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Cart entity.
 *
 * @see \Drupal\ur_cart\Entity\Cart.
 */
class CartAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\ur_cart\Entity\CartInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cart entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published cart entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit cart entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete cart entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cart entities');
  }

}
