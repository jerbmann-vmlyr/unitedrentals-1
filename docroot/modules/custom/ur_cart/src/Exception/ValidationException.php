<?php

namespace Drupal\ur_cart\Exception;

/**
 * Class DalException.
 *
 * The basic exceptions for the DAL Appliance.
 *
 * @package Drupal\ur_appliance\Exception\DAL
 */
class ValidationException extends \Exception {

  /**
   * ValidationException constructor.
   */
  public function __construct($message = '', $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }

}
