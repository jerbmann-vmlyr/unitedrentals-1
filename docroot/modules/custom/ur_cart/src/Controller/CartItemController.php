<?php

namespace Drupal\ur_cart\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\ur_cart\Entity\CartItem;
use Drupal\ur_cart\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\ur_frontend_api\Traits\ApiTrait;

/**
 * Class CartItemController.
 *
 * @package Drupal\ur_cart\Controller
 */
class CartItemController extends ControllerBase {
  use ApiTrait;

  /**
   * Creates a new CartItem entity.
   *
   * @param bool $cart_id
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The Cart Item list
   */
  public function addItem($cart_id = FALSE):?JsonResponse {
    $response = new JsonResponse();
    $params = $this->paramsGetOrPost();

    /** @var \Drupal\ur_cart\Entity\Cart $cart */
    $cart = CartController::grabCurrentCart($params, $cart_id);

    if (empty($cart)) {
      $cart = CartController::createDefault($params['saveForLater']);
    }

    // Get POST body, convert it to an array.
    $content = \Drupal::request()->getContent();
    $values = json_decode($content);
    $values = (array) $values;
    $cart_items = [];
    $errorMsg = NULL;

    // This could return a string message designating that we are invalid.
    try {
      $values = $this->validateValues($values);

      // Now create and save a new CartItem.
      /** @var \Drupal\ur_cart\Entity\CartItem $cart_item */
      $cart_item = $this->createAndSave($values);

      // Add the CartItem to the array of CartItems.
      // @TODO: Talk to Patrick about how to update the CartController's static cart with this
      $cart->addCartItem($cart_item);

      // Now save the cart to update the "changed" property.
      $cart->save();

      // Now that we've added a CartItem, return the CartItem list response.
      $cart_items = CartController::buildItemsArray($cart);
    }
    catch (ValidationException $e) {
      $errorMsg = $e->getMessage();
    }
    catch (\Exception $e) {
      $errorMsg = $e->getMessage();
    }

    if (empty($errorMsg)) {
      if (!empty($cart)) {
        $response->setData($cart_items);
      }
      else {
        $response->setData([
          'error' => TRUE,
          'message' => 'No cart was found.'
        ]);
      }
    }
    else {
      $response->setData([
        'error' => TRUE,
        'message' => $errorMsg
      ]);
    }

    return $response;
  }

  /**
   * Moves a single CartItem either From or To the saved-for-later Cart.
   *
   * @param int $cart_item_id
   *   The ID of the CartItem to update.
   * @param string $action
   *   Either of the following strings from arg(4) of the URL path:
   *      'move-to-save-for-later'
   *      'move-to-active-cart'.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The Cart Item list
   *
   * @throws \Exception
   */
  public function cartSwap($cart_item_id, $action):?JsonResponse {
    $response = new JsonResponse();

    // No real need to call paramsGetOrPost at this time
    // $params = $this->paramsGetOrPost();
    $params = [];

    // To move an item from Saved-For-Later to Active
    // Get the Active Cart.
    $cartTo = CartController::grabCurrentCart($params, FALSE);

    // Get the Saved-For-Later Cart.
    $cartFrom = CartController::grabCurrentCart(['saveForLater' => 1], FALSE);

    if ($action == 'move-to-save-for-later') {
      // Switch $cartTo and $cartFrom to handle moving an item from Active to Saved-For-Later.
      $tmp = $cartTo;
      $cartTo = $cartFrom;
      $cartFrom = $tmp;
    }

    // Get the $cart_item and instantiate housekeeping vars.
    /** @var \Drupal\ur_cart\Entity\CartItem $cart_item */
    $cart_item = $cartFrom->retrieveCartItem($cart_item_id);
    $cart_items = NULL;
    $errorMsg = NULL;

    try {
      // Swap the $cart_item->cartId.

      $cart_item->setChanged();
      $cart_item->save();

      $cartTo->addCartItem($cart_item);
      $cartTo->save();

      // Return updated cart.
      $cartTo = CartController::loadCart($cartTo->id());

      // Reset Static Carts.
      $cart_items = CartController::buildItemsArray($cartFrom);
      $cartFrom = CartController::updateStaticCarts($cartFrom);

      $cart_items = CartController::buildItemsArray($cartTo);
      $cartTo = CartController::updateStaticCarts($cartTo);

    }
    catch (ValidationException $e) {
      $errorMsg = $e->getMessage();
    }

    if (!empty($errorMsg)) {
      $response->setData([
        'error' => TRUE,
        'message' => $errorMsg
      ]);
    }
    elseif (!empty($cartTo)) {
      $response->setData($cart_items);
    }
    else {
      $response->setData([
        'error' => TRUE,
        'message' => 'No cart was found.'
      ]);
    }

    return $response;
  }

  /**
   * Updates a single CartItem.
   *
   * @param int $cart_item_id
   *   The ID of the CartItem to update.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The Cart Item list
   *
   * @throws \Exception
   */
  public function updateItem($cart_item_id) {
    $response = new JsonResponse();
    $params = $this->paramsGetOrPost();

    $cart_item = $this->loadItem($cart_item_id);
    $cart_id = $cart_item->getCartId();
    $params['cartId'] = empty($params['cartId']) ? $cart_id : $params['cartId'];

    // This may be active or saveForLater cart.
    /** @var \Drupal\ur_cart\Entity\Cart $cart */
    $cart = CartController::grabCurrentCart($params, $cart_id);

    if ($cart) {
      /** @var \Drupal\ur_cart\Entity\CartItem $cart_item */
      $cart_item = $cart->retrieveCartItem($cart_item_id);
    }

    // Get POST body, convert it to an array.
    $cart_items = NULL;
    $errorMsg = NULL;

    try {
      // Validate the values before submission.
      $params = $this->validateValues($params);

      // Put everything together.
      $cart_item->assembleAndUpdateFields($params);

      // Need to update the Cart's items array.
      $cart->addCartItem($cart_item);

      // Now save the cart to update the "changed" property.
      $cart->save();

      // Return updated cart.
      $cart_items = CartController::buildItemsArray($cart);
    }
    catch (ValidationException $e) {
      $errorMsg = $e->getMessage();
    }

    if (!empty($errorMsg)) {
      $response->setData([
        'error' => TRUE,
        'message' => $errorMsg
      ]);
    }
    elseif (!empty($cart)) {
      $response->setData($cart_items);
    }
    else {
      $response->setData([
        'error' => TRUE,
        'message' => 'No cart was found.'
      ]);
    }

    return $response;
  }

  /**
   * Update Items method.
   *
   * Updates the corresponding cart items based off POST data sent.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   all cart items
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateItems() {
    $new_cart_items = json_decode(\Drupal::request()->getContent());

    $response = new JsonResponse();
    $params = $this->paramsGetOrPost();

    /** @var \Drupal\ur_cart\Entity\Cart $cart */
    $cart = CartController::grabCurrentCart($params, FALSE);

    try {
      if (\is_array($new_cart_items) && !empty($new_cart_items)) {
        foreach ($new_cart_items as $new_cart_item) {
          /** @var \Drupal\ur_cart\Entity\CartItem $cart_item */
          $old_cart_item = $cart->retrieveCartItem($new_cart_item->id);

          // Validate the values before submission.
          $new_cart_values = $this->validateValues($new_cart_item);

          // Convert old item to new item.
          $old_cart_item->assembleAndUpdateFields($new_cart_values);

          // Need to update the Cart's items array.
          $cart->addCartItem($old_cart_item);

          // Now save the cart to update the "changed" property.
          $cart->save();
        }
      }
    }
    catch (ValidationException $e) {
      $response->setData([
        'error' => TRUE,
        'message' => $e->getMessage()
      ]);
    }

    if (!empty($cart)) {
      $response->setData(CartController::buildItemsArray($cart));
    }
    else {
      $response->setData([
        'error' => TRUE,
        'message' => 'No cart was found.'
      ]);
    }

    return $response;
  }

  /**
   * Loads a single item from the entity storage.
   *
   * @param int $id
   *   The CartItem entity ID to load up.
   *
   * @return CartItem|null
   *   Loads a cart item from storage.
   */
  public static function loadItem($id):?CartItem {
    /** @var \Drupal\ur_cart\CartItemStorage $controller */
    $controller = \Drupal::entityTypeManager();
    $storage = $controller->getStorage('cart_item');
    $result = $storage->load($id);

    return $result;
  }

  /**
   * Deletes a CartItem.
   *
   * @param int $cart_item_id
   *   The CartItem to delete.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response from deleting an item from the cart.
   *
   * @throws \Exception
   */
  public function removeItem($cart_item_id):?JsonResponse {
    $response = new JsonResponse();
    $cart_item = CartItemController::loadItem($cart_item_id);

    /** @var \Drupal\ur_cart\Entity\Cart $cart */
    $cart_id = $cart_item->getCartId();
    $cart = CartController::loadCart($cart_id);
    $storage = \Drupal::entityTypeManager()->getStorage('cart_item');

    // @TODO: Talk to Patrick about how to update the CartController's static cart with this
    // First thing, remove it from the current cart's array of items.
    $cart->removeCartItem($cart_item);

    // Now save the cart to update the "changed" property.
    $cart->save();

    // Now to actually delete the CartItem. Load up the CartItem in question.
    $cart_item = $storage->load($cart_item_id);

    // Means we didn't have a CartItem with that ID, something is weird.
    if ($cart_item == FALSE) {
      $response->setData([
        'error' => TRUE,
        'message' => 'Cannot find CartItem with id' . $cart_item_id,
      ]);

      return $response;
    }

    // Actually delete the entity now.
    $storage->delete([$cart_item]);

    // Now that we've deleted the CartItem, return the updated CartItem list.
    $cart_items = CartController::buildItemsArray($cart);

    if (!empty($cart)) {
      $response->setData($cart_items);
    }
    else {
      $response->setData(['error' => TRUE, 'message' => 'No cart was found.']);
    }

    return $response;
  }

  /**
   * Creates, saves and returns a CartItem entity.
   *
   * @param array $values
   *   The array of POST body contents of values to use for the CartItem.
   *
   * @return CartItem
   *   A CartItem entity
   *
   * @throws \Exception
   */
  protected function createAndSave(array $values):?CartItem {
    /** @var \Drupal\ur_cart\CartItemStorage $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('cart_item');

    // Assemble the rest of the values before creating a CartItem.
    CartItem::assembleFields($values);

    $entity = $storage->create($values);
    $status = $storage->save($entity);
    $acceptedStatuses = [SAVED_NEW, SAVED_UPDATED];

    if (\in_array($status, $acceptedStatuses, FALSE)) {
      return $entity;
    }

    throw new EntityStorageException('There was an error creating the CartItem.');
  }

  /**
   * Ensure we have at least the minimum amount of values needed for a CartItem.
   *
   * @param string|array|\stdClass $values
   *   - An array of key => value pairs.
   *
   * @return array|string
   *   The array of values after validation has been applied.
   *
   * @throws ValidationException
   */
  protected function validateValues($values) {
    if (\is_string($values)) {
      $values = [$values];
    }

    $values = (array) $values;

    // These 3 fields are the minimum amount of info we need to store a CartItem.
    if (!isset($values['catClass']) || empty($values['catClass'])) {
      throw new ValidationException('Cat-Class is required');
    }

    if (!isset($values['quantity']) || empty($values['quantity'])) {
      throw new ValidationException('Quantity is required');
    }

    if (!isset($values['branchId']) || empty($values['branchId'])) {
      throw new ValidationException('Branch ID is required');
    }

    // Ensure that if we have a startDate we also have an endDate and vice versa.
    if (!isset($values['startDate'], $values['endDate'])) {
      throw new ValidationException(
        'Start Date and End Date are required'
      );
    }

    $startDate = strtotime($values['startDate']);
    $endDate = strtotime($values['endDate']);

    if ($startDate > $endDate) {
      // Means our endDate comes before our startDate, throw error.
      throw new ValidationException('End Date must be after the Start Date');
    }

    return $values;
  }

}
