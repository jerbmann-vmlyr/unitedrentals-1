<?php

namespace Drupal\ur_cart\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\ur_cart\Entity\Cart;
use Drupal\ur_frontend_api\Traits\ApiTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CartController.
 *
 * @package Drupal\ur_cart\Controller
 */
class CartController extends ControllerBase {
  use ApiTrait;
  /**
   * The cart object.
   *
   * @var \Drupal\ur_cart\Entity\CartInterface
   */
  private static $cart;


  /**
   * The Saved For Later cart object.
   *
   * @var \Drupal\ur_cart\Entity\CartInterface
   */
  private static $cartSavedForLater;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;


  /**
   * CartController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager service.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Cleans up the current user's cart.
   *
   * Deletes all the items from it and then deletes the cart itself.
   *
   * @param array $itemIds
   *   - An array of the Cart Item IDs to eliminate.
   *
   * @throws \Exception
   */
  public static function cleanup(array $itemIds) {
    if (empty($itemIds) || !is_array($itemIds)) {
      throw new \Exception(
        'You must give an array of Cart Item Ids to the cart cleanup for it to operate.'
      );
    }

    /** @var \Drupal\ur_cart\Entity\Cart $cart */
    // Get the current cart.
    $cart = self::grabCurrentCart();

    if ($cart != NULL) {
      // Load items, delete them, then delete the cart.
      $items = $cart->getCartItems();

      /** @var \Drupal\ur_cart\Entity\CartItem $item */
      foreach ($items as $idx => $item) {
        // Only eliminate items in the list passed in.
        if (in_array($item->id(), $itemIds)) {
          $item->delete();
          // Remove from the list.
          unset($items[$idx]);
        }
      }

      // Only delete the cart if there are no more items left in it.
      if (empty($items)) {
        // Now delete the cart.
        $cart->delete();
      }
    }
  }

  /**
     * Displays the contents of the cart page.
     *
     * This is denoted with the <cart> tag so that Vue.js may take over.
     *
     * @return array
     *   The render array of contents for the cart page.
     */
    public function cartContent() {
      return [
        '#type' => 'markup',
        '#markup' => '<div id="cartLoading" class="grid cart">
                        <div class="cart__header">
                          <h1>Updating your cart</h1>
                          <h2 class="heading-2--thin">Please wait while we update your reservations</h2>
                          <span class="spinner" />
                        </div>
                        <style>
                          #cartLoading .cart__header {padding: 2% 0;}
                          #cartLoading .cart__header h1 {margin: .67em 0;}
                          #cartLoading .cart__header h2 {font-weight:300;}
                        </style>
                      </div>
                      <checkout></checkout>',
        '#allowed_tags' => ['checkout', 'div', 'h1', 'h2', 'span', 'style'],
        '#attached' => [
          'drupalSettings' => [
            'vueRouter' => TRUE,
          ],
        ],
      ];
    }

  /**
   * This is the default endpoint for the frontend to retrieve Cart information.
   *
   * All cart info will be sent to the frontend from here. From here we need
   * to determine whether we need to create a new cart, or load up an existing
   * one for the user. Cart object will include it's cart items.
   *
   * @param int $cart_id
   *   Cart ID to load, otherwise loads the currently cached Cart object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The array of values from getting the cart.
   *
   * @throws \InvalidArgumentException
   * @throws \Exception
   */
  public function getCart($cart_id = FALSE) {
    $response = new JsonResponse();
    $params = $this->paramsGetOrPost();
    /** @var \Drupal\ur_cart\Entity\Cart $cart */
    $cart = self::grabCurrentCart($params, $cart_id);

    if (!empty($cart)) {
      $cart_id = ($cart_id) ? $cart_id : $cart->getCartId();
      $ids = ['cart_id' => $cart_id];
      $cart_items = self::buildItemsArray($cart);

      $cart_meta = self::getMetaData($cart);

      $response->setData(array_merge($ids, $cart_meta, $cart_items));
    }
    else {
      $response->setData(['error' => TRUE, 'message' => 'No cart was found.']);
    }

    return $response;
  }

  /**
   * Recall the requested cart from static cache or load one if passed an $id.
   *
   * @param array $params
   *   An array of various flags.
   * @param bool|int $id
   *   The Cart ID to load, otherwise this will load.
   *
   * @return \Drupal\ur_cart\Entity\CartInterface
   *   The Cart object for the current user.
   */
  public static function grabCurrentCart(array $params = [], $id = FALSE) {
    // Do we have the cartId?  Then just load it and return ...
    if (!empty($id)) {
      $cart = self::loadCart($id);
      self::updateStaticCarts($cart);
      return $cart;
    }
    // Are we after the SavedForLater cart?
    if (!empty($params['saveForLater'])) {
      if (empty(static::$cartSavedForLater) || $id !== FALSE) {
        // grabSavedForLaterCart() called sortCartTypes() which updated this static var.
        $carts = self::grabSavedForLaterCart($id);
      }
      return static::$cartSavedForLater;
    }

    // Nope, we want the active cart.
    if (empty(static::$cart) || $id !== FALSE) {
      $result = self::loadCart($id);
      if ($result != NULL) {
        $carts = self::sortCartTypes($result);
      }
    }

    // sortCartTypes updated this static var.
    return static::$cart;
  }

  /**
   * Recall the saved_for_later cart from static cache if available.
   *
   * Find the saved_for_later cart and save to static cache if available;
   * Instantiate a new saved_for_later cart and save to static cache if not available;
   * Load a saved_for_later cart if passed an $id that correlates to a SavedForLater cart.
   *
   * @param bool|int $id
   *   The Cart ID to load, otherwise this will load.
   *
   * @return bool|EntityInterface|mixed
   *   The Cart object for the current user.
   */
  public static function grabSavedForLaterCart($id = FALSE) {
    $result = self::loadCart($id);
    $carts = self::sortCartTypes($result, TRUE);
    // Uh oh:  We may have accounts with pre-existing cart with no cartSavedForLater.
    // Handle that here.
    if (empty($carts)) {
      // Is there an existing active cart?
      $cartsActive = self::sortCartTypes($result, FALSE);
      // Yes, so let's create their saved_for_later cart as well.
      if ($cartsActive) {
        $carts = self::createDefault(TRUE);
      }
    }
    return $carts;
  }

  /**
   * Takes an array of cart objects, returns only the carts of the requested type (active or saved-for-later).
   *
   * @param array $carts
   *   Array of Cart Objects.
   * @param bool|int $save_for_later
   *   Flag that indicates which type of carts to return.
   *
   * @return mixed
   *   Subset of Cart Objects either normal or the saved_for_later cart
   */
  public static function sortCartTypes(array $carts, $save_for_later = FALSE) {
    $savedCarts = [];
    $normalCarts = [];
    foreach ($carts as $id => $cart) {
      // Check saved_for_later bool and return only the ones that match our request.
      if ($cart->get('saved_for_later')->value) {
        $savedCarts[$id] = $cart;
      }
      else {
        $normalCarts[$id] = $cart;
      }
    }

    static::$cartSavedForLater = reset($savedCarts);
    static::$cart = reset($normalCarts);
    if ($save_for_later) {
      return $savedCarts;
    }
    return $normalCarts;
  }

  /**
   * Loads the current account's fully loaded Cart object.
   *
   * @param int $cart_id
   *   The Cart ID of the Cart to load.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   A Cart entity or null if one not found.
   */
  public static function loadCart($cart_id) {
    /** @var \Drupal\ur_cart\CartStorage $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('cart');
    $storage->resetCache([$cart_id]);

    return $storage->loadSingle([], $cart_id);
  }

  /**
   * Merges a stored anonymoous cart into the user's current cart upon login.
   *
   * @param int $merge_cart_id
   *   The Cart ID to merge into this Cart.
   */
  public static function mergeCart($merge_cart_id) {
    $uid = \Drupal::currentUser()->id();
    $cart = self::grabCurrentCart();

    if (empty($cart)) {
      $cart = self::createDefault();
    }

    /** @var \Drupal\ur_cart\Entity\Cart $cart */
    $merge_cart = self::loadCart($merge_cart_id);

    // For each item in the cart to merge add it to the authenticated cart/user and save it.
    foreach ($merge_cart->getCartItems() as $item) {
      $cart->addCartItem($item);
      $item->set('cartId', $cart->id());
      $item->set('user_id', $uid);
      $item->save();
    }

    $cart->save();

    // Delete the anonymous cart, we no longer need it.
    $merge_cart->delete();
  }

  /**
   * Assembles a multidimensional array of CartItems from a Cart.
   *
   * This is mainly used for formatting purposes when we send data to the
   * front-end.
   *
   * @param \Drupal\ur_cart\Entity\Cart $cart
   *   The (optional) cart to use for assembling the items.
   *
   * @return array
   *   A multidimensional array of CartItems
   */
  public static function buildItemsArray(Cart $cart = NULL) {
    if ($cart == NULL) {
      $cart = self::grabCurrentCart();
    }

    if (empty($cart)) {
      return [];
    }

    $cart_items = [
      'items' => [],
    ];

    /** @var \Drupal\ur_cart\Entity\CartItem $item */
    // TODO: Convert boolean values to TRUE and FALSE rather than "1" and "0".
    foreach ($cart->getCartItems() as $item) {
      $cart_items['items'][$item->id()] = $item->toArray();
    }

    return $cart_items;
  }

  /**
   * Returns meta data specific to the cart.
   *
   * @param \Drupal\ur_cart\Entity\Cart|null $cart
   *   The cart to retrieve meta data for, else we look up the current cart.
   *
   * @return array
   *   The meta data about the cart in a flat array.
   */
  public static function getMetaData(Cart $cart = NULL) {
    if ($cart == NULL) {
      $cart = self::grabCurrentCart();
    }

    if (empty($cart)) {
      return [];
    }

    // TODO: Determine any other meta data to send to front-end.
    $meta = [
      'startedCheckout' => $cart->hasStartedCheckout(),
      'isSavedForLater' => $cart->isSavedForLater(),
    ];

    return $meta;
  }

  /**
   * Loads a single item from the entity storage.
   *
   * @param int $id
   *   The CartItem entity ID to load up.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Loads a cart item from storage.
   */
  public static function loadItem($id) {
    /** @var \Drupal\ur_cart\CartItemStorage $controller */
    $controller = \Drupal::entityTypeManager()->getStorage('cart_item');
    $controller->resetCache([$id]);
    return $controller->load($id);
  }

  /**
   * Creates a default Cart entity if one does not already exist.
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface
   *   False if we could not retrieve a cart, otherwise a Cart entity.
   */
  public static function createDefault($saved_for_later = FALSE) {
    /** @var \Drupal\Core\Session\AccountInterface $account */
    $account = \Drupal::currentUser();

    /** @var \Drupal\Core\Session\SessionManager $session_manager */
    $session_manager = \Drupal::service('session_manager');

    $uid = 0;
    $time = time();

    if (is_object($account) && !empty($account->id())) {
      $uid = $account->id();
    }

    if (empty($_SESSION['session_started'])) {
      $session_manager->start();
      $_SESSION['session_started'] = TRUE;
    }

    $data = [
      'uid' => $uid,
      'sid' => $session_manager->getId(),
      'created' => $time,
      'changed' => $time,
      'saved_for_later' => $saved_for_later,
    ];

    if ($saved_for_later) {
      static::$cartSavedForLater = self::retrieveCart($data);

      // @NOTE: We WANT to get back the full cart on create.
      return static::$cartSavedForLater;
    }
    else {
      static::$cart = self::retrieveCart($data);

      // @NOTE: We WANT to get back the full cart on create.
      return static::$cart;
    }
  }

  /**
   * Replacement of createAndSave() from ORS.
   *
   * This attempts to load the latest before creating a brand new one.
   *
   * @param array $data
   *   Entity properties to search for a Cart on.
   *
   * @return bool|EntityInterface
   *   False if not found or a default Cart entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected static function retrieveCart(array $data) {
    /** @var \Drupal\ur_cart\CartStorage $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('cart');
    $conditions = [
      'saved_for_later' => [$data['saved_for_later']],
    ];

    /** @var \Drupal\ur_cart\Entity\Cart $cart */
    // @TODO Change if allowing for multi-cart.
    $cart = $storage->loadSingle($conditions, FALSE);

    if (empty($cart)) {
      // Means we didn't find a cart, create one.
      $cart = $storage->createAndSave($data);

      if ($data['saved_for_later']) {
        $cart = $cart->toggleSavedForLater();
      }
    }

    return $cart;
  }

  /**
   * Submits to Pardot what we've started checkout.
   *
   * This is used by the front-end to notify that the user has started checkout.
   * Doesn't return, just submits to pardot. It has to return something or drupal throws a 500 error.
   */
  public function startCheckoutPardot() {
    /** @var \Drupal\ur_cart\Entity\Cart $cart */
    $cart = CartController::grabCurrentCart();
    if ($cart) {
      $cart->set('started_checkout', TRUE)->save();
      \Drupal::service('ur_pardot.pardot')->sendCheckoutStart($cart->id());
    }

    return new JsonResponse();
  }

  /**
   * Utility function that updates the appropriate static cart on demand.
   *
   * @param \Drupal\ur_cart\Entity\Cart $newCart
   *   The cart object that should be saved to the static cart var.
   *
   * @return \Drupal\ur_cart\Entity\CartInterface
   *   The updated static cart.
   */
  public static function updateStaticCarts(Cart $newCart) {

    if (!empty($newCart)) {
      if ($newCart->isSavedForLater()) {
        static::$cartSavedForLater = $newCart;
        return static::$cartSavedForLater;
      }
      else {
        static::$cart = $newCart;
        return static::$cart;
      }
    }
    else {
      // No cart passed in.
      return NULL;
    }
  }

}
