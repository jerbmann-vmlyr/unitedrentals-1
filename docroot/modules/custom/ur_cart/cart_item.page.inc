<?php

/**
 * @file
 * Contains cart_item.page.inc.
 *
 * Page callback for Cart Item entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Cart Item templates.
 *
 * Default template: cart_item.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_cart_item(array &$variables) {
  // Fetch CartItem Entity Object.
  $cart_item = $variables['elements']['#cart_item'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
