<?php

namespace Drupal\cke_animated_text\Plugin\Filter;

use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;

/**
 * @Filter(
 *   id = "filter_cke_animated_text",
 *   title = @Translation("CKE Animated Text Filter"),
 *   description = @Translation("Allow CK Editor text to be delimited for animation underline"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterCKEAnimatedText extends FilterBase {

  public function process($text, $langcode) {

    $replace = 'emphasis emphasis-trigger="scroll" emphasis-trigger-scroll-target="parent"';
    $filtered_text = str_replace('class="orange-underline"', $replace, $text);

    return new FilterProcessResult($filtered_text);

  }

}
