<?php

namespace Drupal\ur_helpcta\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Provides a UR Help and Contact CTA.
 *
 * @Block(
 *   id="ur_helpcta_block",
 *   admin_label=@Translation("Help & Contact CTA"),
 *   category = @Translation("UR"),
 *   )
 */
class URHelpCTABlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $image = isset($config['ur_helpcta_image'][0])
      ? File::load($config['ur_helpcta_image'][0])->url()
      : '';

    return [
      '#theme' => 'ur_helpcta',
      '#headingText' => $config['ur_helpcta_heading'],
      '#primaryLinkText' => $config['ur_helpcta_linkprimarytext'],
      '#primaryLink' => $config['ur_helpcta_linkprimary'],
      '#primaryLinkAddin' => $config['ur_helpcta_linkprimaryaddin'],
      '#secondaryLinkText' => $config['ur_helpcta_linksecondarytext'],
      '#secondaryLink' => $config['ur_helpcta_linksecondary'],
      '#secondaryLinkAddin' => $config['ur_helpcta_linksecondaryaddin'],
      '#image' => $image,
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['ur_helpcta_heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Call To Action Text'),
      '#description' => $this->t('The main text that will appear in the CTA block'),
      '#default_value' => isset($config['ur_helpcta_heading']) ? $config['ur_helpcta_heading'] : '',
    ];

    $form['ur_helpcta_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Background Image'),
      '#upload_location' => 'public://module-images/ur_helpcta',
      '#description' => $this->t('The background image that will appear in the CTA Block'),
      '#default_value' => isset($config['ur_helpcta_image']) ? $config['ur_helpcta_image'] : '',
    ];

    $form['ur_helpcta_linkprimarytext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Primary Button Text'),
      '#description' => $this->t('The First CTA Button Text'),
      '#default_value' => isset($config['ur_helpcta_linkprimarytext']) ? $config['ur_helpcta_linkprimarytext'] : '',
    ];

    $form['ur_helpcta_linkprimary'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Primary Link'),
      '#description' => $this->t('The First CTA Link'),
      '#default_value' => isset($config['ur_helpcta_linkprimary']) ? $config['ur_helpcta_linkprimary'] : '',
    ];

    $form['ur_helpcta_linkprimaryaddin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Primary Link Addin'),
      '#description' => $this->t('Addin for the primary button - i.e. javascript action'),
      '#default_value' => isset($config['ur_helpcta_linkprimaryaddin']) ? $config['ur_helpcta_linkprimaryaddin'] : '',
    ];

    $form['ur_helpcta_linksecondarytext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secondary Button Text'),
      '#description' => $this->t('The Second CTA Button Text'),
      '#default_value' => isset($config['ur_helpcta_linksecondarytext']) ? $config['ur_helpcta_linksecondarytext'] : '',
    ];

    $form['ur_helpcta_linksecondary'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secondary Link'),
      '#description' => $this->t('The Secondary CtA Link'),
      '#default_value' => isset($config['ur_helpcta_linksecondary']) ? $config['ur_helpcta_linksecondary'] : '',
    ];

    $form['ur_helpcta_linksecondaryaddin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secondary Link Addin'),
      '#description' => $this->t('Addin for the secondary button - i.e. javascript action'),
      '#default_value' => isset($config['ur_helpcta_linksecondaryaddin']) ? $config['ur_helpcta_linksecondaryaddin'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['ur_helpcta_heading'] = $form_state->getValue('ur_helpcta_heading');
    $this->configuration['ur_helpcta_image'] = $form_state->getValue('ur_helpcta_image');
    $this->configuration['ur_helpcta_linkprimary'] = $form_state->getValue('ur_helpcta_linkprimary');
    $this->configuration['ur_helpcta_linksecondary'] = $form_state->getValue('ur_helpcta_linksecondary');
    $this->configuration['ur_helpcta_linkprimarytext'] = $form_state->getValue('ur_helpcta_linkprimarytext');
    $this->configuration['ur_helpcta_linksecondarytext'] = $form_state->getValue('ur_helpcta_linksecondarytext');
    $this->configuration['ur_helpcta_linkprimaryaddin'] = $form_state->getValue('ur_helpcta_linkprimaryaddin');
    $this->configuration['ur_helpcta_linksecondaryaddin'] = $form_state->getValue('ur_helpcta_linksecondaryaddin');
    $image = $form_state->getValue('ur_helpcta_image');
    $file = File::load($image[0]);
    $file->setPermanent();
    $file->save();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_config = \Drupal::config('ur_helpcta.settings');

    return [
      'ur_helpcta_title' => $default_config->get('name'),
      'ur_helpcta_heading' => $default_config->get('heading'),
      'ur_helpcta_linkprimary' => $default_config->get('primarylink'),
      'ur_helpcta_linksecondary' => $default_config->get('secondarylink'),
      'ur_helpcta_linkprimarytext' => $default_config->get('primarylinktext'),
      'ur_helpcta_linksecondarytext' => $default_config->get('secondarylinktext'),
      'ur_helpcta_linkprimaryaddin' => $default_config->get('primarylinkaddin'),
      'ur_helpcta_linksecondaryaddin' => $default_config->get('secondarylinkaddin'),
    ];
  }

}
