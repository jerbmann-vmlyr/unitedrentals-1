<?php

namespace Drupal\ur_admin\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides a 'UrCallUsBlock' block.
 *
 * @Block(
 *  id = "ur_call_us",
 *  admin_label = @Translation("UR Call Us Block"),
 * )
 */
class UrCallUsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('ur_admin.settings');
    $general_inquiries = $config->get('general_inquiries_number');
    $rental_inquiries = $config->get('rental_inquiries_number');

    // Generate URL for locations page
    $url = Url::fromRoute('locations.default_controller');
    $link = Link::fromTextAndUrl('Find the closest branch', $url);

    return [
      '#theme' => 'ur_call_us',
      '#call_us_number' => $general_inquiries,
      '#link' => $link->toString(),
      '#attached' => [
        'library' => [
          'urone/block--call-us-block',
        ],
      ],
    ];
  }

}
