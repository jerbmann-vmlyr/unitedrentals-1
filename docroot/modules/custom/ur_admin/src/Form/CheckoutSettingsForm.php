<?php

namespace Drupal\ur_admin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CheckoutSettingsForm.
 *
 * Provides an administration form for checkout settings.
 *
 * @package Drupal\ur_frontend_api\Form
 */
class CheckoutSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ur_checkout_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_admin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    // Is rate limiting enabled?
    $enabled = 0;
    if ($config->get('checkout_transaction_rate_limit_enabled')) {
      $enabled = $config->get('checkout_transaction_rate_limit_enabled');
    }
    $form['checkout_transaction_rate_limit_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Checkout Transaction Rate Limiting Enabled?'),
      '#default_value' => $enabled,
    ];

    // What's the expiration in seconds for rate limiting?
    $expiration = 30;
    if ($config->get('checkout_transaction_rate_limit_expiration')) {
      $expiration = $config->get('checkout_transaction_rate_limit_expiration');
    }
    $form['checkout_transaction_rate_limit_expiration'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Checkout Transaction Rate Limit Expiration'),
      '#description' => $this->t(
        'Seconds required between checkout transaction calls before rate limiting occurs.'
      ),
      '#default_value' => $expiration,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    // Save rate limit expiration.
    $config->set(
      'checkout_transaction_rate_limit_expiration',
      $form_state->getValue('checkout_transaction_rate_limit_expiration')
    )->save();

    $config->set(
      'checkout_transaction_rate_limit_expiration',
      trim($form_state->getValue('checkout_transaction_rate_limit_expiration'))
    )->save();

    // Set rate limit enabled.
    $config->set(
      'checkout_transaction_rate_limit_enabled',
      $form_state->getValue('checkout_transaction_rate_limit_enabled')
    )->save();

    $config->set(
      'checkout_transaction_rate_limit_enabled',
      trim($form_state->getValue('checkout_transaction_rate_limit_enabled'))
    )->save();

    parent::submitForm($form, $form_state);
  }

}
