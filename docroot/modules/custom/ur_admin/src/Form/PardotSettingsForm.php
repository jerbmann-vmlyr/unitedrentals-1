<?php

namespace Drupal\ur_admin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PardotSettingsForm.
 *
 * @package Drupal\ur_admin\Form
 */
class PardotSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pardot_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_admin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    $form['pardot_form_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pardot Form Link'),
      '#description' => $this->t('This is the form URL used to submit abandoned carts/checkouts to Pardot.'),
      '#default_value' => $config->get('pardot_form_link'),
      '#maxlength' => 255,
      '#size' => 64,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');
    $config
      ->set('pardot_form_link', $form_state->getValue('pardot_form_link'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
