<?php

namespace Drupal\ur_admin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GlobalSearchSettingsForm.
 *
 * @package Drupal\ur_admin\Form
 */
class GlobalSearchSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'global_search_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_admin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    $form['global_search_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global Search Placeholder'),
      '#description' => $this->t('This is the placeholder text that will be used inside of the global search input field.'),
      '#default_value' => $config->get('global_search_placeholder'),
      '#maxlength' => 255,
      '#size' => 64,
      '#required' => TRUE,
    ];

    $form['global_search_index'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global Search Index'),
      '#description' => $this->t('The machine name for the index to use in global searches.'),
      '#default_value' => $config->get('global_search_index'),
      '#maxlength' => 255,
      '#size' => 64,
      '#required' => TRUE,
    ];

    $form['global_search_highlight_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global Search Highlighter Prefix'),
      '#description' => $this->t('HTML prepended to highlighted search fragments'),
      '#default_value' => $config->get('global_search_highlight_prefix'),
      '#maxlength' => 255,
      '#size' => 64,
      '#required' => FALSE,
    ];

    $form['global_search_highlight_suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global Search Highlighter Suffix'),
      '#description' => $this->t('HTML appended to highlighted search fragments'),
      '#default_value' => $config->get('global_search_highlight_suffix'),
      '#maxlength' => 255,
      '#size' => 64,
      '#required' => FALSE,
    ];

    $form['global_search_max_results_group'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global Search Max Results Per Group'),
      '#description' => $this->t('Maximum number of results displayed per group'),
      '#default_value' => $config->get('global_search_max_results_group'),
      '#maxlength' => 10,
      '#size' => 5,
      '#required' => FALSE,
    ];

    $form['global_search_max_results_query'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global Search Max Results Per Query'),
      '#description' => $this->t('Maximum number of results pulled for Solr queries'),
      '#default_value' => $config->get('global_search_max_results_query'),
      '#maxlength' => 10,
      '#size' => 5,
      '#required' => FALSE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');
    $config
      ->set('global_search_placeholder', $form_state->getValue('global_search_placeholder'))
      ->set('global_search_index', $form_state->getValue('global_search_index'))
      ->set('global_search_highlight_prefix', $form_state->getValue('global_search_highlight_prefix'))
      ->set('global_search_highlight_suffix', $form_state->getValue('global_search_highlight_suffix'))
      ->set('global_search_max_results_group', $form_state->getValue('global_search_max_results_group'))
      ->set('global_search_max_results_query', $form_state->getValue('global_search_max_results_query'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
