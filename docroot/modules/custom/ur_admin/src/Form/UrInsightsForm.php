<?php

namespace Drupal\ur_admin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UrInsightsForm.
 *
 * Provides an administration form for setting New Relic's account ID and insert
 * key into config.
 *
 * @see \Drupal\ur_api_dataservice\InsightsLogger
 *
 * @package Drupal\ur_frontend_api\Form
 */
class UrInsightsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ur_new_relic_insights_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_admin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    $form['new_relic_account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('New Relic Account ID'),
      '#description' => $this->t(
        'The account ID assigned by new relic.'
      ),
      '#default_value' => $config->get('new_relic_account_id'),
      '#required' => TRUE,
    ];

    $form['new_relic_insert_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('New Relic Insert Key'),
      '#description' => $this->t(
        'The API key to use for New Relic Insights. Visit https://insights.newrelic.com/accounts/{your_account_id}/manage/api_keys to find your key.'
      ),
      '#default_value' => $config->get('new_relic_insert_key'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    $config->set(
      'new_relic_account_id',
      $form_state->getValue('new_relic_account_id')
    )->save();

    $config->set(
      'new_relic_insert_key',
      trim($form_state->getValue('new_relic_insert_key'))
    )->save();

    parent::submitForm($form, $form_state);
  }

}
