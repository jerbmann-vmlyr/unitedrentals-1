<?php

namespace Drupal\ur_admin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CallControlSettingsForm.
 *
 * @package Drupal\ur_admin\Form
 */
class CallControlSettingsForm extends ConfigFormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'customer_service_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_admin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    $form['general_inquiries_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('General Inquiries Number'),
      '#description' => $this->t('The customer service number for General Inquiries displayed in the "Call Us" control. Please enter the number in the following format <em>111.111.1111</em>.'),
      '#maxlength' => 12,
      '#size' => 64,
      '#default_value' => $config->get('general_inquiries_number'),
      '#required' => TRUE,
    ];

    $form['rental_inquiries_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rental Inquiries Number'),
      '#description' => $this->t('The customer service number for Rental Equipment Inquiries displayed in the "Call Us" control. Please enter the number in the following format <em>111.111.1111</em>.'),
      '#maxlength' => 12,
      '#size' => 64,
      '#default_value' => $config->get('rental_inquiries_number'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    $config
      ->set('general_inquiries_number', $form_state->getValue('general_inquiries_number'))
      ->save();

    $config
      ->set('rental_inquiries_number', $form_state->getValue('rental_inquiries_number'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
