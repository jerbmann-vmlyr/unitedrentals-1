<?php

namespace Drupal\ur_admin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TranslationSettingsForm.
 *
 * @package Drupal\ur_admin\Form
 */
class TranslationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'translation_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_admin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    $form['english_translation_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('English Translation URL'),
      '#description' => $this->t('The base url to be used for english translations without the trailing "/"'),
      '#maxlength' => 120,
      '#size' => 64,
      '#default_value' => $config->get('english_translation_url'),
      '#required' => TRUE,
    ];

    $form['spanish_translation_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Spanish Translation URL'),
      '#description' => $this->t('The base url to be used for spanish translations without the trailing "/"'),
      '#maxlength' => 120,
      '#size' => 64,
      '#default_value' => $config->get('spanish_translation_url'),
      '#required' => TRUE,
    ];

    $form['french_translation_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('French Translation URL'),
      '#description' => $this->t('The base url to be used for french translations without the trailing "/"'),
      '#maxlength' => 120,
      '#size' => 64,
      '#default_value' => $config->get('french_translation_url'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    $config
      ->set('english_translation_url', $form_state->getValue('english_translation_url'))
      ->save();

    $config
      ->set('spanish_translation_url', $form_state->getValue('spanish_translation_url'))
      ->save();

    $config
      ->set('french_translation_url', $form_state->getValue('french_translation_url'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
