<?php

namespace Drupal\ur_admin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ApiSettingsForm.
 *
 * @package Drupal\ur_frontend_api\Form
 */
class ApiSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'frontend_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_admin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    $form['enable_branch_filtering'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable branch filtering?'),
      '#description' => $this->t(
        'Check this box to enable branch filtering.'
      ),
      '#default_value' => $config->get('enable_branch_filtering'),
    ];

    $form['acquired_company_codes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Acquired Company Codes'),
      '#description' => $this->t(
        'A comma-separated list of acquired company codes we want to exclude from results for rates and charge estimates.'
      ),
      '#maxlength' => 20,
      '#size' => 64,
      '#default_value' => $config->get('filters.acquired_company_codes'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    $config->set(
      'enable_branch_filtering',
      $form_state->getValue('enable_branch_filtering')
    )->save();

    $config->set(
      'filters.acquired_company_codes',
      trim($form_state->getValue('acquired_company_codes'))
    )->save();

    parent::submitForm($form, $form_state);
  }

}
