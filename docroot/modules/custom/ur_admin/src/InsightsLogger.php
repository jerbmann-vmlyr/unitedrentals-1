<?php

namespace Drupal\ur_admin;

use Drupal\Core\Url;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class InsightsLogger.
 *
 * This service class is used to submit custom data to New Relic Insights API.
 *
 * @see https://docs.newrelic.com/docs/insights/insights-data-sources/custom-data/insert-custom-events-insights-api
 */
class InsightsLogger {

  /**
   * The HTTP client send data to insights with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The base URL to New Relic insights events API.
   *
   * @var string
   */
  protected $baseUrl = 'https://insights-collector.newrelic.com';

  /**
   * The New Relic account ID.
   *
   * @var string
   */
  protected $accountId = '';

  /**
   * InsightsLogger constructor.
   *
   * @param \GuzzleHttp\Client $client
   */
  public function __construct(Client $client) {
    $this->client = $client;
    $this->accountId = \Drupal::config('ur_admin.settings')->get('new_relic_account_id');
  }

  /**
   * Submits log detail information to New Relic Insights API.
   *
   * Visit https://docs.newrelic.com/docs/insights/insights-data-sources/custom-data/insert-custom-events-insights-api for more info.
   *
   * @param array $log_details
   *   Array of key-value pairs to submit to New Relic. Needs to follow the
   *   limit and restricted characters set by New Relic:
   *   (https://docs.newrelic.com/docs/insights/insights-data-sources/custom-data/insert-custom-events-insights-api#limits).
   *
   * @return bool
   *   True if successfully sent, false if it didn't submit properly.
   */
  public function logInsights(array $log_details) {
    // Need this array of options to submit to New Relic.
    $options = [
      'headers' => [
        'X-Insert-Key' => \Drupal::config('ur_admin.settings')->get('new_relic_insert_key'),
        'Content-Type' => 'application/json',
      ],
      "json" => [
      // This is the major key we will use in NR.
        "eventType" => 'DalError',
      ],
    ];

    // The "json" is the request body to submit to New Relic Insights.
    $options['json'] = array_merge($options['json'], $log_details);

    try {
      // Now submit to New Relic with our data.
      $response = $this->client->request('POST', $this->buildInsightsUrl(), $options);
    }
    catch (GuzzleException $e) {
      \Drupal::logger('insights_logger')->notice($e->getMessage());
      return FALSE;
    }

    if ($response->getStatusCode() == 200) {
      // Means it succeeded. New Relic only returns { "success": true }.
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Builds the correct New Relic Insights URL for posting custom events.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   *   The string of the constructed URL
   */
  private function buildInsightsUrl() {
    $url_string = $this->baseUrl . '/v1/accounts/' . $this->accountId . '/events';

    $url = Url::fromUri($url_string);

    return $url->toString();
  }

}
