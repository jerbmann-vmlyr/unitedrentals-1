<?php

namespace Drupal\match_4xx\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class MydataForm.
 *
 * @package Drupal\mydata\Form
 */
class MatchDataForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'match_4xx_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $conn = Database::getConnection();
    $record = [];
    if (isset($_GET['num'])) {
      $query = $conn->select('match_4xx', 'm')
        ->condition('id', $_GET['num'])
        ->fields('m');
      $record = $query->execute()->fetchAssoc();
    }
    $form['#theme'] = isset($_GET['num']) ? 'match_4xx_edit' : 'match_4xx_create';
    $form['urlpartial'] = [
      '#type' => 'textfield',
      '#title' => t('URL Match String'),
      '#required' => TRUE,
      '#default_value' => (isset($record['urlpartial']) && $_GET['num']) ? $record['urlpartial'] : '',
    ];
    $form['priority'] = [
      '#type' => 'textfield',
      '#title' => t('Priority'),
      '#default_value' => (isset($record['priority']) && $_GET['num']) ? $record['priority'] : '',
    ];
    $form['errorType'] = [
      '#type' => 'select',
      '#title' => t('Error Type'),
      '#default_value' => (isset($record['errorType']) && $_GET['num']) ? $record['errorType'] : '',
      '#empty_option' => t('All Errors'),
      '#empty_value' => '0',
      '#options' => [
        '401' => t('401 Errors'),
        '403' => t('403 Errors'),
        '404' => t('404 Errors'),
      ],
    ];
    $form['nodeId'] = [
      '#type' => 'textfield',
      '#title' => t('Node ID (optional):'),
      '#required' => FALSE,
      '#default_value' => (isset($record['nodeId']) && $_GET['num']) ? $record['nodeId'] : '',
    ];
    $form['controllerMethod'] = [
      '#type' => 'textfield',
      '#title' => t('Controller::Method'),
      '#required' => FALSE,
      '#default_value' => (isset($record['controllerMethod']) && $_GET['num']) ? $record['controllerMethod'] : '',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Save',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Check for the presence of either a node id or a controller::method string
    if ($form_state->getValue('nodeId') == "" && $form_state->getValue('controllerMethod') == "") {
      $form_state->setErrorByName('nodeId', $this->t('Node ID or Controller::Method is required.'));
    }

    // Check for a correct controller method

    if ($form_state->getValue('controllerMethod') != "") {
      list($class, $method) = explode("::", $form_state->getValue('controllerMethod'));

      if (!class_exists($class)) {
        $form_state->setErrorByName('controllerMethod', $this->t("Class $class does not exist."));
      }
      if (!method_exists($class, $method)) {
        $form_state->setErrorByName('controllerMethod', $this->t("Method $method does not exist on Class $class"));
      }
    }


    // Check for a valid Node Id

    if ($form_state->getValue('nodeId') != "") {
      $node = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->load($form_state->getValue('nodeId'));
      if (!$node) {
        $form_state->setErrorByName('nodeId', $this->t('That Node ID does not exist.'));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field = $form_state->getValues();
    $urlpartial = $field['urlpartial'];
    $priority = $field['priority'];
    $errorType = $field['errorType'];
    $nodeId = $field['nodeId'];
    $controllerMethod = $field['controllerMethod'];
    if ($nodeId == '') {
      $nodeId = 0;
    }
    if (isset($_GET['num'])) {
      $field = [
        'urlpartial' => $urlpartial,
        'priority' => $priority,
        'errorType' => $errorType,
        'nodeId' => $nodeId,
        'controllerMethod' => $controllerMethod,
      ];
      $query = \Drupal::database();
      $query->update('match_4xx')
        ->fields($field)
        ->condition('id', $_GET['num'])
        ->execute();
      \Drupal::messenger()->addMessage("succesfully updated");
      $form_state->setRedirect('match_4xx.display_table_controller_display');
    }
    else {
      $field = [
        'urlpartial' => $urlpartial,
        'priority' => $priority,
        'errorType' => $errorType,
        'nodeId' => $nodeId,
        'controllerMethod' => $controllerMethod,
      ];
      $query = \Drupal::database();
      $query->insert('match_4xx')->fields($field)->execute();
      \Drupal::messenger()->addMessage("succesfully saved");
      $response = new RedirectResponse("/admin/config/match_4xx");
      $response->send();
    }
  }

}
