<?php

namespace Drupal\match_4xx\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {
  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {

    if ($route = $collection->get('system.401')) {
      $route->setDefaults(array(
        '_controller' => '\Drupal\match_4xx\Controller\ErrorController::on401',
      ));
    }

    if ($route = $collection->get('system.403')) {
      $route->setDefaults(array(
        '_controller' => '\Drupal\match_4xx\Controller\ErrorController::on403',
      ));
    }

    if ($route = $collection->get('system.404')) {
      $route->setDefaults(array(
        '_controller' => '\Drupal\match_4xx\Controller\ErrorController::on404',
      ));
    }
  }

}
