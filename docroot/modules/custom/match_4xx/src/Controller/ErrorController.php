<?php

namespace Drupal\match_4xx\Controller;

use Drupal\Core\Database\Database;
use Drupal\system\Controller\Http4xxController;

class ErrorController {

  public function on401() {
    $current_path = \Drupal::service('path.current')->getPath();
    $body = $this->getErrorContent($current_path, 401);
    $build = [
      '#children' => $body,
    ];
    return $build;
  }

  public function on403() {
    $current_path = \Drupal::service('path.current')->getPath();
    $body = $this->getErrorContent($current_path, 403);
    $build = [
      '#children' => $body,
    ];
    return $build;
  }

  public function on404() {
    $current_path = \Drupal::service('path.current')->getPath();
    $body = $this->getErrorContent($current_path, 404);
    $build = [
      '#children' => $body,
    ];
    return $build;
  }

  public function getErrorContent($path, $code = 404) {

    // first, we try to find a match in the database
    $connection = Database::getConnection();
    $partials = $connection->query("SELECT * FROM {match_4xx} WHERE errorType in (0, $code) order by priority desc");
    foreach ($partials as $partial) {
      if (strpos(strtolower($path), strtolower($partial->urlpartial)) === 0) {
        if ($partial->nodeId > 0) {
          $node = \Drupal::entityTypeManager()
            ->getStorage('node')
            ->load($partial->nodeId);
          return $node->body->value;
        }
        if (!empty($partial->controllerMethod)) {
          $class = explode("::", $partial->controllerMethod)[0];
          $method = explode("::", $partial->controllerMethod)[1];
          $controller = new $class();
          $response = $controller->$method();
          return render($response);
        }
      }
    }

    // finally, we fall back to the default handler
    $controller = new Http4xxController();
    switch ($code) {
      case 401:
        return render($controller->on401());
        break;
      case 403:
        return render($controller->on403());
        break;
      case 404:
        $response = render($controller->on404());
        return $response;
        break;
    }

    return render($controller->on404());

  }

}
