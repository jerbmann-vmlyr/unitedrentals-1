<?php

namespace Drupal\match_4xx\Controller;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DisplayTableController.
 *
 * @package Drupal\mydata\Controller
 */
class DisplayTableController extends ControllerBase {

  /**
   * @return array
   */
  public function getContent() {
    // First we'll tell the user what's going on. This content can be found
    // in the twig template file: templates/match-4xx-description.html.twig.
    $build = [
      'description' => [
        '#theme' => 'match_4xx_list',
        '#description' => 'Listing of all Match_4xx settings',
        '#attributes' => [],
      ],
    ];
    return $build;
  }

  /**
   * Display.
   *
   * @return array
   *   Return Build array.
   */
  public function display() {

    // create table header
    $header_table = [
      'priority' => t('Priority'),
      'urlpartial' => t('URL Match String'),
      'errorType' => t('Error Type'),
      'nodeId' => t('Node ID'),
      'controllerMethod' => t('Class::Method'),
      '',
      '',
    ];
    // select records from table
    $query = \Drupal::database()->select('match_4xx', 'm');
    $query->fields('m', [
      'id',
      'priority',
      'urlpartial',
      'errorType',
      'nodeId',
      'controllerMethod',
    ]);
    $results = $query->orderBy('urlpartial', 'asc')
      ->orderBy('priority', 'DESC')
      ->execute()
      ->fetchAll();
    $rows = [];
    foreach ($results as $data) {
      $deleteUrl = Url::fromUserInput('/admin/config/match_4xx/delete/' . $data->id);
      $editUrl = Url::fromUserInput('/admin/config/match_4xx/form?num=' . $data->id);
      // print the data from table
      $rows[] = [
        'priority' => $data->priority,
        'urlpartial' => $data->urlpartial,
        'errorType' => $data->errorType != "" ? $data->errorType : 'All Types',
        'nodeId' => $data->nodeId > 0 ? $data->nodeId : '',
        'controllerMethod' => $data->controllerMethod,
        Link::fromTextAndUrl('Edit', $editUrl),
        Link::fromTextAndUrl('Delete', $deleteUrl),
      ];
    }


    // display data in site
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header_table,
      '#rows' => $rows,
      '#empty' => t('No Records found'),
    ];
    $form['#theme'] = 'match_4xx_list';
    return $form;
  }

}
