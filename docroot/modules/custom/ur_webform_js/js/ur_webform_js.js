(function($, Drupal, drupalSettings) {
  var initialized;
  function init(settings) {
    if (!initialized) {
      initialized = true;
      if (
        settings &&
        settings.ur_webform_js &&
        settings.ur_webform_js.form &&
        settings.ur_webform_js.form.id
      ) {
        $('#' + settings.ur_webform_js.form.id).submit(function() {
          if (window && window.dataLayer) {
            dataLayer.push({
              'event': 'GAEvent',
              'eventCategory': 'contact',
              'eventAction': 'submit_form',
              'eventLabel': settings.ur_webform_js.form.title,
              'eventValue': undefined
            });
          }
        });
      }
    }
  }
  Drupal.behaviors.ur_webform_js = {
    attach: function(context, settings) {
      init(settings);
    },
  };
})(jQuery, Drupal);
