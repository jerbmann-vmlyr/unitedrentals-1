<?php

namespace Drupal\ur_third_party_modules_control\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ThirdPartyModulesForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ur_third_party_modules_control_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ur_third_party_modules_control.admin_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_third_party_modules_control.admin_settings');

    $form['modules_state'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Third party modules control on/off'),
      '#description' => $this->t('Select and save the check box to unistall third party modules <br> Unselect and save the check box to install third party modules'),
      '#default_value' => $config->get('modules_state'),
    ];
    return parent::buildForm($form, $form_state);
  }

 /**
  * {@inheritdoc}
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('ur_third_party_modules_control.admin_settings')
        ->set('modules_state', $form_state->getValue('modules_state'))
        ->save();

      $module_data = \Drupal::config('core.extension')->get('module');

    if ($form_state->getValue('modules_state') == 1) {
     unset($module_data['ur_optin_monster'], $module_data['marketplace_waypoints'], $module_data['google_tag'], $module_data['egain_switch']);
     \Drupal::configFactory()->getEditable('core.extension')->set('module', $module_data)->save();
    }
    else {
     $new_module_data = array_merge(['ur_optin_monster' => 0, 'marketplace_waypoints' => 0, 'google_tag' => 0, 'egain_switch' => 0], $module_data);
     \Drupal::configFactory()->getEditable('core.extension')->set('module', $new_module_data)->save();
    }
    parent::submitForm($form, $form_state);
  }

}
