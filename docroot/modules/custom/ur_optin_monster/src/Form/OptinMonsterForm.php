<?php

namespace Drupal\ur_optin_monster\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class OptinMonsterForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ur_optin_monster_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ur_optin_monster.admin_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_optin_monster.admin_settings');

    if ($config->get('om_js') == '') {

      $this->config('ur_optin_monster.admin_settings')
        ->set('om_js', $config->get('ur_optin_monster_js'))
        ->save();
        $default_js = $config->get('ur_optin_monster_js');
    }
    else {
      $default_js = $config->get('om_js');
    }
    $form['om_js'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Optin Monster Js'),
      '#default_value' => $default_js,
    ];

    return parent::buildForm($form, $form_state);
  }

 /**
  * {@inheritdoc}
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

      $this->config('ur_optin_monster.admin_settings')
        ->set('om_js', $form_state->getValue('om_js'))
        ->save();
    parent::submitForm($form, $form_state);
  }

}
