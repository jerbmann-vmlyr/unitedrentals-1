<?php

namespace Drupal\ur_facets\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\ur_frontend_api\Controller\UrCache;
use Drupal\ur_appliance\Data\DAL\Branch;
use Drupal\ur_appliance\Provider\DAL;
use Drupal\Core\Routing\RouteMatch;

/**
 * FacetController file.
 */
class FacetController extends ControllerBase {
  public static $categoryCatClassMap = [];

  /**
   * Get all Category taxonomy terms.
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse - Cached "Category" taxonomy tree.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function content() {
    $vocabs = Vocabulary::loadMultiple();
    $vid = $vocabs['categories']->id();

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    $termTree = self::termTreeBuild($terms, 0, NULL);

    $data = [
      'result' => $termTree,
    ];

    // Cache for a day based on the whole url.
    $data['#cache'] = [
      'max-age' => 86400,
      'contexts' => [
        'url',
      ],
    ];

    $response = new CacheableJsonResponse($data);
    $response->addCacheableDependency(CacheableMetadata::createFromRenderArray($data));

    return $response;
  }

  /**
   * Grabs the description field for a given taxonomy term.
   *
   * @param int $categoryTid
   *   - Taxonomy term id.
   * @param int $subcategoryTid
   *   - Taxonomy term id.
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse
   *   - Cached taxonomy term description value.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function description($categoryTid = 0, $subcategoryTid = 0) {
    $vocabs = Vocabulary::loadMultiple();
    $vid = $vocabs['categories']->id();
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

    $category = array_filter($terms, function ($term) use ($categoryTid) {
      return $term->tid == $categoryTid;
    });

    $category = reset($category);

    $subcategory = array_filter($terms, function ($term) use ($subcategoryTid) {
      return $term->tid == $subcategoryTid;
    });

    $subcategory = reset($subcategory);
    $msg = '';
    $str_breadcrumb = '';

    if (!empty($subcategory->description__value)) {
      $msg = $subcategory->description__value;
      $breadcrumbs = $this->buildBreadcrumbs($subcategory);
      $str_breadcrumb = $this->renderBreadcrumbs($breadcrumbs);
    }
    elseif (isset($category)) {
      $msg = $category->description__value;
      $breadcrumbs = $this->buildBreadcrumbs($category);
      $str_breadcrumb = $this->renderBreadcrumbs($breadcrumbs);
    }

    $msg = trim(preg_replace('/\s+/', ' ', $msg));

    $data = [
      'result' => $msg,
      'breadcrumbs' => $str_breadcrumb,
    ];

    // Cache for a day based on the url.
    $data['#cache'] = [
      'max-age' => 86400,
      'contexts' => [
        'url',
      ],
    ];

    $response = new CacheableJsonResponse($data);
    $response->addCacheableDependency(CacheableMetadata::createFromRenderArray($data));

    return $response;
  }

  /**
   * Helper method to build breadcrumbs to be returned in description()
   *
   * Utilizes Drupal core breadcrumbs.
   *
   * @param $category
   *
   * @return \Drupal\Core\Breadcrumb\Breadcrumb
   */
  private function buildBreadcrumbs($category) {

    $term_category = Term::load($category->tid);
    $routeName = $term_category->urlInfo()->getRouteName();
    $routeParameters = ['taxonomy_term' => $category->tid];
    $route = \Drupal::service('router.route_provider')->getRouteByName($routeName);
    $route_match = new RouteMatch($routeName, $route, $routeParameters);
    $term_builder = \Drupal::service('ur_facets.breadcrumb');
    $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/taxonomy/term/' . $category->tid);
    $term_builder->context->setPathInfo($alias);
    $breadcrumbs = $term_builder->build($route_match);

    return $breadcrumbs;
  }

  /**
   * Helper method that renders built breadcrumbs as string.
   *
   * @param $breadcrumbs
   *
   * @return string
   */
  private function renderBreadcrumbs($breadcrumbs) {
    $r_breadcrumbs = $breadcrumbs->toRenderable();
    $renderer = \Drupal::service('renderer');
    $str_breadcrumbs = $renderer->renderPlain($r_breadcrumbs);
    return $str_breadcrumbs;
  }

  /**
   * Helper method that moves 'Equipment' Link to proper position in breadcrumbs.
   *
   * @param $breadcrumbs
   *
   * @return array
   */
  private function moveEquipementBreadcrumb($breadcrumbs, $a, $b) {
    $out = array_splice($breadcrumbs, $a, 1);
    array_splice($breadcrumbs, $b, 0, $out);

    return $breadcrumbs;
  }

  /**
   * Helper method to build an array of taxonomy terms and it's children.
   *
   * @param $terms
   * @param int $depth
   * @param null $parentId
   *
   * @return array
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public static function termTreeBuild($terms, $depth = 0, $parentId = NULL) {
    $termTree = [];

    foreach ($terms as $term) {
      if ($parentId != NULL && $term->parents[0] != $parentId) {
        continue;
      }

      if ($term->depth == $depth) {
        $term->children = self::termTreeBuild($terms, $depth + 1, $term->tid);
        $term->catClasses = self::findCategoryCatClasses($term->tid);
        $term->description = $term->description__value;

        // Due to drupal's route system, entities cannot share the same alias as
        // our Vue.js routes. @see Url Alias patterns for Categories.
        $alias_path = \Drupal::service('path.alias_manager')->getAliasByPath('/taxonomy/term/' . $term->tid);

        $url_component = preg_replace('/_catalog$/', '', $alias_path);
        $term->urlComponent = $url_component;

        unset($term->vid, $term->langcode, $term->description__value,
          $term->description__format, $term->weight, $term->changed, $term->default_langcode);

        $termTree[$term->tid] = $term;
      }
    }

    return $termTree;
  }

  /**
   * Finds all nodes that have a matching cat-class to taxonomy terms.
   *
   * @param $tid
   *
   * @return bool|mixed
   */
  public static function findCategoryCatClasses($tid) {
    if ($cache = UrCache::get(['tid' => $tid])) {
      $termMatchingNodes = $cache->data;

      if (!empty($termMatchingNodes)) {
        return $termMatchingNodes;
      }
    }

    if (empty(self::$categoryCatClassMap)) {
      $connection = \Drupal::database();
      $catClassMap = array();

      $query = $connection->select('node__field_item_category', 'ic');
      $query->join('node__field_item_cat_class_code', 'cc', 'ic.entity_id = cc.entity_id');
      $query->join('node_field_data', 'nfd', 'ic.entity_id = nfd.nid');
      $query->condition('nfd.status', 1);
      $query->fields('ic', ['field_item_category_target_id']);
      $query->fields('cc', ['field_item_cat_class_code_value']);
      $queryResult = $query->execute()->fetchAll();

      foreach ($queryResult as $row) {
        $catClassMap[$row->field_item_category_target_id][] = $row->field_item_cat_class_code_value;
      }

      self::$categoryCatClassMap = $catClassMap;
    }

    if (isset(self::$categoryCatClassMap[$tid])) {
      // Cache this result for a LONG while.
      UrCache::set([], self::$categoryCatClassMap[$tid], FALSE, UrCache::MONTH);

      return self::$categoryCatClassMap[$tid];
    }

    return FALSE;
  }

}
