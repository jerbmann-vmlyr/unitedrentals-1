(function ($, Drupal) {
  Drupal.behaviors.ur_views_spl_dropdown = {
    attach: function (context, settings) {

      if (settings.ur_views_spl_dropdown) {
        var FormID = settings.ur_views_spl_dropdown.FormId;

        // On change of this we will make the exposed form field values change.
        jQuery('#edit-custom-date-range').change(function() {

          var selectedVal = jQuery('#edit-custom-date-range option:selected').val();

          if(selectedVal !== 'select') {
            var splitededVal= selectedVal.split(":");
            var minValue = splitededVal[0];
            var maxValue = splitededVal[1];
            jQuery('#edit-field-press-release-date-value-min').val(minValue);
            jQuery('#edit-field-press-release-date-value-max').val(maxValue);

            jQuery('#edit-created-min').val(minValue);
            jQuery('#edit-created-max').val(maxValue);

            jQuery('#edit-field-date-range-value-min').val(minValue);
            jQuery('#edit-field-date-range-value-max').val(maxValue);

            // Programmatically submit the form
            jQuery( "#"+FormID ).submit();
          }

        });
      }
    }
  };
})(jQuery, Drupal);