<?php

namespace Drupal\ur__global\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension {
  public function getFilters() {
    return [
      new TwigFilter('amPmFormat', [$this, 'formatAmPm']),
    ];
  }

  public function formatAmPm($time) {
    if (!is_numeric($time)) {
      $time = strtotime($time);
    }
    return date('g:i A', $time);
  }
}
