<?php

namespace Drupal\ur__global\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for access to the API.
 */
class GlobalController extends ControllerBase {
  /**
   * @var RequestStack
   * @var AccountProxyInterface
   */
  protected $request;
  protected $user;

  /**
   * Class constructor.
   *
   * @param RequestStack $request
   *   Request stack.
   * @param AccountProxyInterface $user
   */
  public function __construct(RequestStack $request, AccountProxyInterface $user) {
    $this->request = $request;
    $this->user = $user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get('current_user')
    );
  }

  /**
   * Checks if user is authenticated and passes them to proper redirect based on redirectPath variable
   *
   * @return RedirectResponse
   */
  public function redirectUserToAuth() {
    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $this->request->getCurrentRequest();
    $params = $request->query->all();
    $response = new RedirectResponse(Url::fromUri('internal:/')->toString());
    $response->headers->set('Cache-Control', 'no-cache, no-store must-validate');

    // If redirectPath is empty, or user attempts to create redirect loop, redirect to homepage.
    if (empty($params['redirectPath']) || $params['redirectPath'] == $request->getPathInfo()) {
      return $response;
    }

    // If the user is authenticated pass them on to the redirectUri.
    if ($this->user->isAuthenticated()) {
      $response->setTargetUrl(Url::fromUri('internal:' . $params['redirectPath'])->toString());
    }
    // If the user is anonymous, route them through saml_login to hit the SSO server.
    else {
      $redirect_to = "internal:/saml_login?ReturnTo=" . urlencode($request->getSchemeAndHttpHost() . $params['redirectPath']);
      $response->setTargetUrl(Url::fromUri($redirect_to)->toString());
    }

    return $response;
  }
  
}
