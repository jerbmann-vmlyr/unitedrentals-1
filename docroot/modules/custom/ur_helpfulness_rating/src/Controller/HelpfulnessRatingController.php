<?php

namespace Drupal\ur_helpfulness_rating\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Helpfulness Rating controller.
 */
class HelpfulnessRatingController extends ControllerBase {

  /**
   * Receives the vote from the end user.
   */
  public function vote() {
    // Grab params.
    $vote = \Drupal::request()->get('vote');
    $bundle = \Drupal::request()->get('bundle');
    $entityId = \Drupal::request()->get('entity_id');

    // Grab token for user.
    $token = $this->getToken();

    // Store the vote.
    $message = $this->t('Thank you for voting!');
    $this->storeVote($token, $bundle, $entityId, $vote);

    // Send back thank you response.
    $response = new AjaxResponse();

    // Update message.
    $selector = '.helpfulness-rating-block .helpfulness-rating-message';
    $content = [
      '#markup' => $message,
    ];
    $response->addCommand(new HtmlCommand($selector, $content));

    // Update totals.
    $yesVotes = $this->getVotes($bundle, $entityId, TRUE);
    $totalVotes = $this->getVotes($bundle, $entityId);
    $selector = '.helpfulness-rating-block .helpfulness-rating-total';
    $content = [
      '#markup' => $this->t("{$yesVotes} out of {$totalVotes} found this helpful"),
    ];
    $response->addCommand(new HtmlCommand($selector, $content));

    return $response;
  }

  /**
   * Retrieves the token for the current user.
   */
  public function getToken() {
    $token = NULL;
    $uid = \Drupal::currentUser()->id();

    // Try the cookie first.
    if (isset($_COOKIE['ur_helpfulness_rating_token_' . $uid])
      && !empty($_COOKIE['ur_helpfulness_rating_token_' . $uid])
    ) {
      $token = $_COOKIE['ur_helpfulness_rating_token_' . $uid];
    }
    
    // Try based on user next.
    elseif (!empty($uid)) {
      $token = $this->getTokenByUser($uid);
    }

    // Generate token if we still don't have one.
    if (empty($token)) {
      $token = $this->generateToken($uid);
    }

    return $token;
  }

  /**
   * Retrieve votes.
   */
  public function getVotes($bundle, $entityId, $yesOnly = FALSE) {
    // Retrieve votes from database for bundle / entity combo.
    $fields = [
      'vote_id',
    ];
    $connection = \Drupal::database();
    $query = $connection->select('helpfulness_vote', 'hv')
      ->fields('hv', $fields)
      ->condition('hv.bundle', $bundle)
      ->condition('hv.entity_id', $entityId);
    if ($yesOnly) {
      $query->condition('hv.vote', 1);
    }

    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Generates the user token.
   */
  protected function generateToken($uid = NULL) {
    // Generate the token.
    $ip = \Drupal::request()->getClientIp();
    $salt = '=Un1t3d=R3nt4L$=';
    $time = time();
    $token = md5("{$salt}.{$time}.{$ip}.{$salt}");

    // Set expiration to 1 year from now.
    $expiration = time() + 60 * 60 * 24 * 365;

    // Add token to COOKIE.
    setcookie('ur_helpfulness_rating_token_' . $uid, $token, $expiration, '/');

    // Make sure we don't save for uid of 0 in the database.
    if ($uid <= 0) {
      $uid = NULL;
    }

    // Save token in database.
    $fields = [
      'uid' => $uid,
      'token' => $token,
    ];
    $connection = \Drupal::database();
    $result = $connection->insert('helpfulness_token', array_keys($fields))
      ->fields($fields)
      ->execute();

    return $token;
  }

  /**
   * Retrieve user token.
   */
  protected function getTokenByUser($uid) {
    $token = NULL;

    // Retrieve token from database for user.
    $fields = [
      'token',
    ];
    $connection = \Drupal::database();
    $result = $connection->select('helpfulness_token', 'ht')
      ->fields('ht', $fields)
      ->condition('ht.uid', $uid)
      ->range(0, 1)
      ->execute();
    
    // Grab the token value.
    if (!empty($result)) {
      $token = $result->fetchField(0);

      // Add token to COOKIE.
      if (!isset($_COOKIE['ur_helpfulness_rating_token_' . $uid])
        || empty($_COOKIE['ur_helpfulness_rating_token_' . $uid])
      ) {
        // Set expiration to 1 year from now.
        $expiration = time() + 60 * 60 * 24 * 365;
        setcookie('ur_helpfulness_rating_token_' . $uid, $token, $expiration, '/');
      }
    }

    return $token;
  }

  /**
   * Retrieve token id by raw value.
   */
  protected function getTokenIdByToken($token) {
    $tokenId = NULL;

    // Retrieve token id from database for token raw value.
    $fields = [
      'token_id',
    ];
    $connection = \Drupal::database();
    $result = $connection->select('helpfulness_token', 'ht')
      ->fields('ht', $fields)
      ->condition('ht.token', $token)
      ->range(0, 1)
      ->execute();
    
    // Grab the token id.
    if (!empty($result)) {
      $tokenId = $result->fetchField(0);
    }

    return $tokenId;
  }

  /**
   * Retrieve entity vote for user.
   */
  protected function getVoteIdByTokenId($tokenId, $bundle, $entityId) {
    $voteId = NULL;

    // Retrieve vote id from database for token id.
    $fields = [
      'vote_id',
    ];
    $connection = \Drupal::database();
    $result = $connection->select('helpfulness_vote', 'hv')
      ->fields('hv', $fields)
      ->condition('hv.token_id', $tokenId)
      ->condition('hv.bundle', $bundle)
      ->condition('hv.entity_id', $entityId)
      ->range(0, 1)
      ->execute();
    
    // Grab the vote id value.
    if (!empty($result)) {
      $voteId = $result->fetchField(0);
    }

    return $voteId;
  }

  /**
   * Save vote.
   */
  protected function storeVote($token, $bundle, $entityId, $vote) {
    // Save vote in database.
    $tokenId = $this->getTokenIdByToken($token);
    $result = NULL;
    if (!empty($tokenId) && !empty($bundle) && !empty($entityId)) {
      $fields = [
        'token_id' => $tokenId,
        'bundle' => $bundle,
        'entity_id' => $entityId,
        'vote' => $vote,
      ];

      // Update vote if we already have one for token.
      $voteId = $this->getVoteIdByTokenId($tokenId, $bundle, $entityId);
      $connection = \Drupal::database();
      $wasUpdate = FALSE;
      if (!empty($voteId)) {
        $wasUpdate = TRUE;
        $result = $connection->update('helpfulness_vote', array_keys($fields))
          ->fields($fields)
          ->condition('vote_id', $voteId)
          ->execute();
      }
      else {
        $result = $connection->insert('helpfulness_vote', array_keys($fields))
          ->fields($fields)
          ->execute();
      }
    }

    return $wasUpdate;
  }

}
