<?php

namespace Drupal\ur_helpfulness_rating\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\NodeInterface;

/**
 * Provides a 'HelpfulnessRatingBlock' block.
 *
 * @Block(
 *  id = "helpfulness_rating_block",
 *  admin_label = @Translation("Helpfulness Rating Block"),
 * )
 */
class HelpfulnessRatingBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Pre-load the token.
    \Drupal::service('helpfulness_rating')->getToken();

    // Determine entity type ( only checks for node right now ).
    $node = \Drupal::routeMatch()->getParameter('node');
    $entityId = NULL;
    $bundle = NULL;
    if ($node instanceof NodeInterface) {
      $entityId = $node->id();
      $bundle = 'node';
    }

    // Grab votes.
    $yesVotes = \Drupal::service('helpfulness_rating')->getVotes($bundle, $entityId, TRUE);
    $totalVotes = \Drupal::service('helpfulness_rating')->getVotes($bundle, $entityId);

    $build = [
      '#theme' => 'ur_helpfulness_rating_block',
      '#helpfulness_question' => $this->t('Was this article helpful?'),
      '#yes_votes' => $yesVotes,
      '#total_votes' => $totalVotes,
      '#bundle' => $bundle,
      '#entity_id' => $entityId,
      // Should make this config at some point?
      '#jsa_block_wrap_open' => FALSE,
      '#jsa_block_wrap_close' => TRUE,
      '#cache' => [
        'max-age' => 0
      ],
    ];
    $build['#attached']['library'][] = 'ur_helpfulness_rating/helpfulness-rating';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Don't allow caching this block.
    return 0;
  }

}
