/* something */
(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.helpfulness_rating = {
    attach: function(context, drupalSettings) {
      $('a.helpfulness-rating-button').each(function() {
        $(this).once('helpfulness-rating-button').bind('click', function() {
          var endpoint = Drupal.url('ur/helpfulness-rating/vote');
          var vote = parseInt($(this).data('vote'));
          var bundle = $(this).parent().data('bundle');
          var entityId = parseInt($(this).parent().data('entity-id'));
          $.ajax({
            url: endpoint,
            type: "POST",
            data: {
              vote: vote,
              bundle: bundle,
              entity_id: entityId
            },
            dataType: "json",
            success: function(data) {
              $('.helpfulness-rating-block .helpfulness-rating-message').html(data[0].data);
              $('.helpfulness-rating-block .helpfulness-rating-total').html(data[1].data);
            }
          });

          return false;
        });
      });
    }
  };
})(jQuery, Drupal, drupalSettings);