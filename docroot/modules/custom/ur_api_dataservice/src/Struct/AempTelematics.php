<?php

namespace Drupal\ur_api_dataservice\Struct;

class AempTelematics extends \stdClass {
  public $altitude; // (object) Location->Altitude->Meters
  public $datetime; // (string) CumulativeOperatingHours->datetime
  public $equipment; // (array) EquipmentHeader || Equipment List
  public $hoursUsed; // (string) CumulativeOperatingHours->Hour
  public $latitude; // (number) Location->Latitude
  public $longitude; // (number) Location->Longitude
  public $usageByDay; // (array) Assembled from Location
}
