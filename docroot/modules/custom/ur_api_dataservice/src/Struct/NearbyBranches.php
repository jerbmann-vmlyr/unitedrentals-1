<?php

namespace Drupal\ur_api_dataservice\Struct;

class NearbyBranches extends \stdClass {

  public $city;
  public $state;
  public $zip;
  public $lat;
  public $long;
  public $limit;
  public $min;
  public $distance;
  public $unit;
  public $sameCountryCode;

}
