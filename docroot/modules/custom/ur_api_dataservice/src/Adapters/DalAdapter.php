<?php

namespace Drupal\ur_api_dataservice\Adapters;

use GuzzleHttp\Client;

use Drupal\ur_api_dataservice\Traits\RetrievesDrupalSetting;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Namshi\Cuzzle\Formatter\CurlFormatter;
use Drupal\ur_api_dataservice\Exceptions\DalException;

/**
 * Class DalAdapter.
 *
 * This is the class that interfaces with the DAL.
 *
 * @package Drupal\ur_api_dataservice\Adapters
 */
class DalAdapter {

  use RetrievesDrupalSetting;

  /**
   * @var string
   */
  private $baseUrl;

  /**
   * @var string
   */
  private $method = 'GET';

  /**
   * @var string
   */
  private $uri = '';

  /**
   * @var string
   */
  private $requestBody = '';

  /**
   * @var null|array
   */
  private $requestJsonParams;

  /**
   * @var array|null
   */
  public $parameters;

  /**
   * @var string
   */
  private $executeError;

  /**
   * @var LoggerInterface
   */
  private $logger;

  /**
   * @var string
   */
  private $token;

  /**
   * @var
   */
  private $headers;

  /**
   * @var string
   */
  private $responseType = 'JSON';

  private $options = [];

  /**
   * DalAdapter constructor.
   *
   * @throws \Exception
   *
   * @TODO: Refactor getDrupalConfig to use Drupal native methods
   */
  public function __construct() {
    $this->baseUrl = self::getDrupalConfig('base_url');
  }

  /**
   * Sets the endpoint.
   *
   * @param $endpoint
   */
  public function setEndpoint($endpoint) {
    $this->uri = $this->baseUrl . $endpoint;
  }

  /**
   * Sets the Request Body.
   *
   * @param $requestBody
   */
  public function setRequestBody($requestBody) {
    $this->requestBody = $requestBody;
  }

  /**
   * Sets the method.
   *
   * @param $method
   */
  public function setMethod($method) {
    $this->method = $method;
  }

  public function setResponseType($type) {
    $this->responseType = $type;
  }

  /**
   * Adds parameters to get sent along.
   *
   * @param $field
   *   The parameter field name.
   *
   * @param $value
   *   The parameter value.
   */
  public function addParameter($field, $value) {
    $this->parameters[$field] = $value;
  }

  /**
   * Sets the authorization token.
   *
   * @param $token
   */
  public function setAuthToken($token) {
    $this->addHeader('Authorization', $token);
  }

  /**
   * Adds a header to the list of headers.
   *
   * @param $header
   * @param $value
   */
  public function addHeader($header, $value) {
    $this->headers[$header] = $value;
  }

  /**
   * Formulates and executes request to the DAL.
   *
   * @return mixed|null
   *   JSON decoded response.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   */
  public function execute() {
    $query = urldecode(empty($this->parameters) ? '' : http_build_query($this->parameters));
    $uri = $this->uri . '?' . $query;
    $options = $this->getOptions();
    $client = new Client($options);
    $requestOptions = $this->getRequestOptions();

    try {
      $response = $client->request($this->method, $uri, $requestOptions);
    }
    catch (ClientException $e) {
      // @TODO: Need to implement blacklisting of requests in case of sensitive data, i.e credit cards
      throw new DalException($e->getMessage(), $e->getCode(), $e->getRequest(), $e->getResponse());
    }
    catch (ServerException $e) {
      throw new DalException($e->getMessage(), $e->getCode(), $e->getRequest(), $e->getResponse());
    }

    $decodedResponse = $this->getDecodedResponse($response);

    if (isset($decodedResponse->status) && $decodedResponse->status === 1 && strtolower($decodedResponse->messageText) !== 'no records found') {
      throw new DalException('Dal Error', 400);
    }

    $this->resetDefaults();
    return $decodedResponse;
  }

  /**
   * Takes a response object and decodes the body.
   *
   * @param null $response
   * @return mixed|null
   */
  public function getDecodedResponse($response = NULL) {
    if ($response !== NULL) {
      if ($this->responseType === 'JSON') {
        return json_decode($response->getBody());
      }
      if ($this->responseType === 'XML') {
        return (string) $response->getBody();
      }
    }

    return NULL;
  }

  /**
   * Gets the options array for the client.
   *
   * @return array
   */
  public function getOptions() {
    $options = [];
    $options['headers'] = $this->headers;

    return $options;
  }

  /**
   * Gets the options array for the request.
   *
   * @return array
   */
  public function getRequestOptions() {
    $options = $this->options;

    if (!empty($this->requestBody)) {
      $options['json'] = $this->requestBody;
    }

    return $options;
  }

  /**
   * @param $options
   */
  public function setRequestOptions(array $options) {
    $this->options = $options;
  }

  /**
   * Reset the adapter defaults for the next call.
   */
  public function resetDefaults() {
    $this->parameters = [];
    $this->requestBody = '';
    $this->method = 'GET';
  }

}
