<?php

namespace Drupal\ur_api_dataservice\Data;

use Drupal\ur_api_dataservice\Traits\GetsParameters;

class CatClassData extends BaseData {

  use GetsParameters;

  public static $parameters = [
    // Cat Class ID: Example: 200-3000, Single item or comma-separated list. Empty returns all Cat/Classes
    'id' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Status: A=Active, D=Deactivated
    'status' => [
      'type' => self::IS_BOOLEAN,
      'required' => [],
      'possible' => [],
      'imports' => [
        'A' => TRUE,
        'D' => FALSE,
      ],
      'mapsTo' => 'active',
      'enabled' => TRUE,
    ],
    // Bool   Get bulk items  TRUE
    'bulk' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => 'bulkItem',
      'enabled' => TRUE,
    ],
    // Int Count of Reserved Equipments Available only when includeReservationCount =  TRUE
    'countReservations' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Cat Class Name: SCISSOR LIFT 19' ELECTRIC
    'desc' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => 'description',
      'enabled' => TRUE,
    ],
    // Display in Catalog: Y/N
    'dspInCatalog' => [
      'type' => self::IS_STRING, // @TODO: Should this be a bool?
      'required' => [self::READ],
      'possible' => [],
      // @TODO: Get all potential values from API. Only seeing "Y" or "N" currently
      'imports' => [
        'Y' => TRUE,
        'U' => TRUE,
        'N' => FALSE,
      ],
      'mapsTo' => 'displayInCatalog',
      'enabled' => TRUE,
    ],
    // Feature 1: Heavy lift capacity of 500 lbs and up. (Notes: Available when includeMetadata= TRUE)
    'feature1' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Feature 2: Narrow width for excellent maneuverability
    'feature2' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Feature 3: Clean, fume-free operation
    'feature3' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Bool   For Sale  TRUE Default is FALSE. Note: Returns the Qty for Sale at the company level
    'forSale' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Bool   Returns the Qty for Sale at the location(s)  TRUE Default is FALSE
    'forSaleAtLoc' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Additional Grouping location Options: location, none (default)
    'groupBy' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Path to the Cat-Class image: /server/img/300-2000.png
    'imgpath' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => 'imageURL',
      'enabled' => TRUE,
    ],
    // Bool   Include Feature Fields  TRUE Default =  TRUE
    'includeMetadata' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Bool   Include Non Rental  TRUE Default =  TRUE
    'includeNonRental' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Bool   Include Count of Reserved equipments at the location  TRUE Default = FALSE
    'includeReservationCount' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Int
    'limit' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Location: Example: 601 - Single branch id or comma-separated list.
    'location' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Location of Cat/Class: 263
    'locationId' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Mapping Level: 1010
    'maplevel' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => 'mapLevel',
      'enabled' => TRUE,
    ],
    // Int   Miles Used when viewLevel is set to M
    'miles' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Int Quantity For Sale Returns the Quantity for Sale at the company level, if forSale is set to  TRUE
    'qtyForSale' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Int Quantity For Sale at Location(s) Returns the Quantity for Sale at the Location(s), if forSaleAtLoc is set to  TRUE
    'qtyForSaleAtLoc' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Int   Quantity Inside Shop
    'qtyInsideShop' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Int   Quantity Missing
    'qtyMissing' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Int   Quantity On Rent
    'qtyOnRent' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Int   Quantity Outside Shop
    'qtyOutsideShop' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Int   Quantity Owned
    'qtyOwned' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Int   Quantity Pickup
    'qtyPickup' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Int   Quantity Rental Purchase
    'qtyRentalPurchase' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Int   Quantity Transferred Out
    'qtyTransferredOut' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // Bool   Is Equipment Shift Capable? False
    'shiftCapable' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    // View Level: H View Options are: B - Branch (default), D - District, H - Hub, M - Miles
    'viewLevel' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
  ];

}
