<?php

namespace Drupal\ur_api_dataservice\Data;

abstract class BaseData {

  public $assembled;

  // Constants describing parameter type.
  const IS_BOOLEAN = 'bool';
  const IS_STRING = 'string';
  const IS_NUMERIC = 'numeric';
  const IS_ARRAY = 'array';
  const IS_JSON = 'json';

  // Constants describing CRUD methods the Plugin parameters could be used in.
  const INDEX = 'index';
  const CREATE = 'create';
  const READ = 'read';
  const UPDATE = 'update';
  const DELETE = 'delete';

  public function __construct($verb = NULL) {
    /** TODO: Eventually assemble will "assemble" the currently needed set of parameters */
    $this->assembled = $this->assemble();
  }

  protected function assemble() {
    // Always return the possibles...
    // Always return the required for this verb...
    return array_filter(static::getParams(), function ($e) {
      return $e['enabled'];
    });
  }

  abstract protected static function getParams();

}
