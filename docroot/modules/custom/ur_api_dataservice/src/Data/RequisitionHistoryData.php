<?php

namespace Drupal\ur_api_dataservice\Data;

use Drupal\ur_api_dataservice\Traits\GetsParameters;

class RequisitionHistoryData extends BaseData {

  use GetsParameters;

  public static $parameters = [
    'requestId' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'statusCode' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'orderStatus' => [
      'type' => self::IS_BOOLEAN,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
  ];

}
