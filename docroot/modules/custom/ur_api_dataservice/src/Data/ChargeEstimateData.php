<?php

namespace Drupal\ur_api_dataservice\Data;

use Drupal\ur_api_dataservice\Traits\GetsParameters;

class ChargeEstimateData extends BaseData {

  use GetsParameters;

  public static $parameters = [
    'accountId' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'address' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'branchId' => [
      'type' => self::IS_STRING,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => '',
      'pattern' => '/^[A-Za-z0-9]{3}$/',
      // @TODO: Build trait to validate pattern
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'city' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'costOverride' => [
      'type' => self::IS_BOOLEAN,
      'required' => [],
      'possible' => [],
      'imports' => 'YN',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'delivery' => [
      'type' => self::IS_BOOLEAN,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => 'YN',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'deliveryCost' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'items' => [
      'type' => self::IS_ARRAY,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'jobsiteId' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'omitNonDspRates' => [
      'type' => self::IS_BOOLEAN,
      'required' => [],
      'possible' => [],
      'imports' => 'YN',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'pickup' => [
      'type' => self::IS_BOOLEAN,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => 'YN',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'pickupCost' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'postalCode' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'returnDate' => [
      'type' => self::IS_STRING,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'rpp' => [
      'type' => self::IS_BOOLEAN,
      'required' => [],
      'possible' => [],
      'imports' => 'YN',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'startDate' => [
      'type' => self::IS_STRING,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'state' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'webRates' => [
      'type' => self::IS_BOOLEAN,
      'required' => [],
      'possible' => [],
      'imports' => 'YN',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
  ];

}
