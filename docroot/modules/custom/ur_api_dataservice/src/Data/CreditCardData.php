<?php

namespace Drupal\ur_api_dataservice\Data;

use Drupal\ur_api_dataservice\Traits\GetsParameters;

class CreditCardData extends BaseData {

  use GetsParameters;

  public static $parameters = [
    'id' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => 'cardId',
      'enabled' => TRUE,
    ],
    'accountId' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [self::INDEX, self::CREATE],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'action' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [self::UPDATE],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'cardHolder' => [
      'type' => self::IS_STRING,
      'required' => [self::CREATE, self::UPDATE],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'cardId' => [
      'type' => self::IS_STRING,
      'required' => [self::UPDATE],
      'possible' => [],
      'imports' => '',
      'mapsTo' => 'id',
      'enabled' => TRUE,
    ],
    'cardName' => [
      'type' => self::IS_STRING,
      'required' => [self::CREATE, self::UPDATE],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'ccNum4' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'ccNumber' => [
      'type' => self::IS_STRING,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'cardType' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'ccType' => [
      'type' => self::IS_STRING,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'cvv' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [self::CREATE, self::UPDATE],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'defaultCard' => [
      'type' => self::IS_BOOLEAN,
      'required' => [self::CREATE, self::UPDATE],
      'possible' => [],
      'imports' => 'YN',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'expireDate' => [
      'type' => self::IS_NUMERIC,
      'required' => [self::CREATE, self::UPDATE],
      'possible' => [],
      'imports' => 'N',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'guid' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'status' => [
      'type' => self::IS_BOOLEAN,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => [
        'A' => TRUE,
        'D' => FALSE,
      ],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
  ];

}
