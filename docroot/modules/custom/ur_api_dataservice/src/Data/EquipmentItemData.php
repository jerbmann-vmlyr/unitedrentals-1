<?php

namespace Drupal\ur_api_dataservice\Data;

use Drupal\ur_api_dataservice\Traits\GetsParameters;

class EquipmentItemData extends BaseData {

  use GetsParameters;

  public static $parameters = [
    'id' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'status' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => [
        'A' => TRUE,
        'D' => FALSE,
      ],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
  ];
}
