<?php

namespace Drupal\ur_api_dataservice\Data;

use Drupal\ur_api_dataservice\Traits\GetsParameters;

class TestData extends BaseData {

  use GetsParameters;

  public static $parameters = [
    'description' => [
      'type' => self::IS_STRING,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => [
        '1' => 'Amazing',
        '2' => 'Astounding',
        '3' => 'Spectacular',
      ],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'testJunkDiscardVariable' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => 'YN',
      'mapsTo' => NULL,
      'enabled' => FALSE,
    ],
  ];

}
