<?php

namespace Drupal\ur_api_dataservice\Data;

use Drupal\ur_api_dataservice\Traits\GetsParameters;

class TelematicsData extends BaseData {

  use GetsParameters;

  public static $parameters = [
    'accountId' => [
      'type' => self::IS_STRING,
      'required' => [self::INDEX],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'altitude' => [
      'type' => self::IS_ARRAY,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'datetime' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'equipmentId' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'fromDate' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'includeChildren' => [
      'type' => self::IS_BOOLEAN,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'includeHistory' => [
      'type' => self::IS_BOOLEAN,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
  ];

}
