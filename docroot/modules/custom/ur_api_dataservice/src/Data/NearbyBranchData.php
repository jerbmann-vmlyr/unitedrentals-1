<?php

namespace Drupal\ur_api_dataservice\Data;

use Drupal\ur_api_dataservice\Traits\GetsParameters;

class NearbyBranchData extends BaseData {

  public static $parameters = [
    'city' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'distance' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'lat' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => [],
      'mapsTo' => 'latitude',
      'enabled' => TRUE,
    ],
    'limit' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'long' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => [],
      'mapsTo' => 'longitude',
      'enabled' => TRUE,
    ],
    'min' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'state' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'unit' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'zip' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [self::INDEX],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
  ];

  use GetsParameters;

}
