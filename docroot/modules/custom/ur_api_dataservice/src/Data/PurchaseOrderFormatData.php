<?php

namespace Drupal\ur_api_dataservice\Data;

use Drupal\ur_api_dataservice\Traits\GetsParameters;

class PurchaseOrderFormatData extends BaseData {

  use GetsParameters;

  public static $parameters = [
    'accountId' => [
      'type' => self::IS_STRING,
      'required' => [self::INDEX],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
  ];

}
