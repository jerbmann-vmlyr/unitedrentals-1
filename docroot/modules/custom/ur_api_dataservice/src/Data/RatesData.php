<?php

namespace Drupal\ur_api_dataservice\Data;

use Drupal\ur_api_dataservice\Traits\GetsParameters;

class RatesData extends BaseData {

  use GetsParameters;

  public static $parameters = [
    'accountId' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'jobsiteId' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'catClass' => [
      'type' => self::IS_STRING,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'branchId' => [
      'type' => self::IS_STRING,
      'required' => [self::CREATE],
      'possible' => [],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'startDate' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'deliveryDistance' => [
      'type' => self::IS_NUMERIC,
      'required' => [],
      'possible' => [],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'includeRateTiers' => [
      'type' => self::IS_BOOLEAN,
      'required' => [],
      'possible' => [],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'webRates' => [
      'type' => self::IS_BOOLEAN,
      'required' => [],
      'possible' => [],
      'imports' => [],
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ]
  ];

}
