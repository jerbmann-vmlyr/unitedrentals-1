<?php

namespace Drupal\ur_api_dataservice\Data;

/**
 * Class ContractData
 * Since this is a type of RentalTransaction we are using
 * the RentalTransactionData members as the values here
 *
 * @package Drupal\ur_api_dataservice\Data
 */
class ContractData extends RentalTransactionData {}
