<?php

namespace Drupal\ur_api_dataservice\Data;

use Drupal\ur_api_dataservice\Traits\GetsParameters;

class EquipmentOwnedData extends BaseData {

  use GetsParameters;

  public static $parameters = [
    'lat' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [self::READ],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'long' => [
      'type' => self::IS_STRING,
      'required' => [],
      'possible' => [self::READ],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'branchId' => [
      'type' => self::IS_STRING,
      'required' => [self::READ],
      'possible' => '',
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
    'catClass' => [
      'type' => self::IS_ARRAY,
      'required' => [self::READ],
      'possible' => [],
      'imports' => '',
      'mapsTo' => NULL,
      'enabled' => TRUE,
    ],
  ];

}
