<?php

namespace Drupal\ur_api_dataservice\Data;

class ParameterData {

  protected function __construct() {}

  protected function __clone() {}

  /**
   * @throws \Exception
   */
  public function __wakeup() {
    throw new \Exception('Cannot unserialize singleton');
  }

  public static function build($set) {
    $plugin = NULL;
    $namespacedName = "Drupal\ur_api_dataservice\Data\\" . $set . 'Data';

    if (class_exists($namespacedName)) {
      $data = new $namespacedName();
    }

    return $data;
  }

}
