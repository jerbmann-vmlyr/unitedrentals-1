<?php

namespace Drupal\ur_api_dataservice\Plugins;

use Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException;
use Drupal\ur_api_dataservice\Traits\ArrayUtilities;
use Drupal\ur_api_dataservice\Traits\AuthenticationTrait;
use Drupal\ur_api_dataservice\Traits\NormalizesData;
use Drupal\ur_api_dataservice\Adapters\DalAdapter;
use Drupal\ur_api_dataservice\Data\ParameterData;
use Drupal\ur_api_dataservice\Traits\ReadsParameters;
use Drupal\ur_api_dataservice\Traits\TransformsXML;
use Drupal\ur_api_dataservice\Traits\VerifiesParameters;
use Drupal\ur_api_dataservice\Struct\AempTelematics;
use Drupal\ur_frontend_api_v2\Models\Geodata;

/**
 * Class GeodataPlugin
 *
 * @package Drupal\ur_api_dataservice\Plugins
 */
class GeodataPlugin extends BasePlugin {

  use AuthenticationTrait;
  use NormalizesData;
  use ReadsParameters;
  use VerifiesParameters;
  use TransformsXML;
  use ArrayUtilities;

  /**
   * The unique "key" for this plugin
   *
   * @var string
   */
  protected $uKey;

  /**
   * HTTP methods allowed by this endpoint.
   *
   * @var array
   */
  protected $allowed = ['index'];

  /**
   * The endpoint we need to approach / consume.
   *
   * @var string
   */
  private $endpoint;

  /**
   * A calculation of the total hours an item has been used.
   *
   * @var string
   */
  private $totalHoursUsed;

  /**
   * Cache overrides -----------------------------------------.
   */
  public function __construct($model) {
    parent::__construct($model);

    /**
     * This plugin uses more than one adapter. Which adapter is
     * determined by the type of request being made, so we
     * set the adapter in the method being called upon
     */
  }

  protected function getTelematics($vars) {
    if (!$vars['accountId']) {
      throw new MissingRequiredParametersException('Telematics requires accountId and/or equipmentId');
    }

    /**
     * IF there is an equipment ID in the request vars then we're looking up the
     * history of a single piece of equipment. The shape of the data returning
     * for an account, versus history of a single piece of equipment is the
     * same, so we need to set up a way to know the difference between the
     * two different request types for importing the results correctly.
     */
    $singleItem = FALSE;
    if ($vars['equipmentId']) {
      $singleItem = TRUE;
    }

    $this->adapter = new DalAdapter();
    $this->endpoint = '/gps/aemptelematics';

    /**
     * uKey was not set when we constructed the class because
     * we're allowing many different types of geodata to be
     * used, so we need to set up the parameters array...
     */
    $built = ParameterData::build('Telematics');
    $this->parameters = $built->assembled;
    $this->adapter->setMethod('GET');
    $this->adapter->setEndpoint($this->endpoint);
    $this->adapter->setResponseType('XML');

    $token = $this->getAuthenticationUserToken();
    $this->adapter->setAuthToken($token);

    if ($vars) {
      $this->verifyRequiredParameters($vars, 'index');
    }
    foreach ($vars as $key => $val) {
      $this->adapter->addParameter($key, $val);
    }
    /** TODO: Implement the new cache utility for storing this record */

    $rawResult = $this->adapter->execute();
    $parsed = $this->parseXmlResponse($rawResult);
    $processed = $this->processXmlAttributes($parsed);

    // placeholder for output...
    $translated = [];
    /**
     * There are three different kinds of results available:
     *
     * 1) A single piece of equipment's current status
     * 2) A single piece of equipment's history
     * 3) An entire account's equipment
     *    Each equipment item in the
     *    account the current status
     *    - NOT a status history
     */
    $equipmentData = $processed['Equipment'];

    // The entire account's equipment...
    if (!$singleItem && !$this->array_is_assc($equipmentData)) {
      foreach ($equipmentData as $ed) {
        if ($t = $this->importTelematics($ed)) {
          array_push($translated, $t);
        }
      }
    }
    // A single piece of equipment without history...
    elseif ($singleItem && $this->array_is_assc($equipmentData)) {
      if ($t = $this->importTelematics($equipmentData)) {
        $translated = $t;
      }
    }
    // A single piece of equipment with history...
    else {
      $temp = [];
      $snapshot = date('Y-m-d', strtotime($processed['snapshotTime']));
      foreach ($equipmentData as $ed) {
        if ($t = $this->importTelematics($ed)) {
          array_push($temp, $t);
        }
      }
      // Now we need to consolidate the values into a single member...
      $translated = new AempTelematics();
      $translated->altitude = $temp[0]->altitude;
      $translated->datetime = $snapshot;
      $translated->equipment = $temp[0]->equipment;
      $translated->latitude = $temp[0]->latitude;
      $translated->longitude = $temp[0]->longitude;

      $translated->usageByDay = $this->consolidateTelematics($temp);
      $translated->hoursUsed = $this->totalHoursUsed;
    }
    $geodata = new Geodata();
    $geodata->geodata = $translated;

    return $geodata;
  }

  protected function getNearbyBranches($vars) {

    $this->adapter = new DalAdapter();
    $this->endpoint = '/location/branchdistances';

    /**
     * uKey was not set when we constructed the class because
     * we're allowing many different types of geodata to be
     * used, so we need to set up the parameters array...
     */
    $built = ParameterData::build('NearbyBranch');
    $this->parameters = $built->assembled;
    $this->adapter->setMethod('GET');
    $this->adapter->setEndpoint($this->endpoint);
    $this->adapter->setResponseType('JSON');

    $token = $this->getAuthenticationWORSToken();
    $this->adapter->setAuthToken($token);

    foreach ($vars as $key => $val) {
      $this->adapter->addParameter($key, $val);
    }
    /** TODO: Implement the new cache utility for storing this record */

    return $this->adapter->execute();

  }

  /**
   * Get the geodata based on the type of data requested
   *
   * @param int $type
   * @param array $vars
   *
   * @return array|mixed
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   */
  public function index($type, $vars = NULL) {
    switch ($type) {
      case 'telematics':
        return $this->getTelematics($vars);
        break;
      case 'branches':
        return $this->getNearbyBranches($vars);
        break;
    }

  }

  protected function importTelematics($data) {
    $telematics = new AempTelematics();

    if (isset($data['EquipmentHeader']['id'])) {
      $telematics->equipment = $data['EquipmentHeader']['id'];
    }

    if (isset($data['EquipmentHeader']['EquipmentID'])) {
      $telematics->equipment = $data['EquipmentHeader']['EquipmentID'];
    }

    $telematics->datetime = $data['CumulativeOperatingHours']['datetime'];
    $telematics->hoursUsed = $data['CumulativeOperatingHours']['Hour'];

    $equipmentLocation = $data['Location'];
    if (!empty($equipmentLocation['Altitude'][ucfirst($equipmentLocation['AltitudeUnits'])])) {
      $telematics->altitude = [$equipmentLocation['AltitudeUnits'] => $equipmentLocation['Altitude'][ucfirst($equipmentLocation['AltitudeUnits'])]];
    }
    else {
      $telematics->altitude = 0;
    }

    $telematics->latitude = $equipmentLocation['Latitude'];
    $telematics->longitude = $equipmentLocation['Longitude'];

    return $telematics;
  }

  /**
   * Consolidate telematics into one unified array
   *
   * @param $data
   * @return array
   *
   * @throws \Exception
   */
  private function consolidateTelematics($data) {
    // Multi-location history imports.
    $locationDateTime = NULL;
    $dayUsage = [];

    $accumulated = [];
    foreach ($data as $item) {
      $currentDateTime = new \DateTime($item->datetime);
      $currentTimestamp = $currentDateTime->getTimestamp();

      // Track most recent location and calculate operating hours.
      if ($locationDateTime < $currentTimestamp) {
        $locationDateTime = $currentTimestamp;
      }

      // Set the day start time.
      $currentDay = $currentDateTime->format('Y-m-d');
      $currentHours = trim($item->hoursUsed);

      // Skip an empty entry.
      if (!(strlen($currentHours) > 1) || $currentHours == 'P') {
        continue;
      }

      if (!isset($accumulated[$currentDay])) {
        // Added dates for reference later on
        $accumulated[$currentDay] = (object) [
          'startHours' => $currentHours,
          'startDate' => $currentDateTime,
          'endHours' => '',
          'endDate' => '',
        ];
      }
      elseif (!empty($currentHours)) {
        // This assumes that the last record will be the most recent.
        $accumulated[$currentDay]->endHours = $currentHours;
        $accumulated[$currentDay]->endDate = $currentDateTime;
      }
    }

    // Now calculate total hours.
    foreach ($accumulated as $key => $val) {
      if (empty($val->endHours)) {
        continue;
      }
      $dayUsage[$key] = $this->calculateAndAddUsage($val->startHours, $val->endHours);
    }

    return $dayUsage;
  }

  /**
   * Gets the calculated usage and then record it.
   *
   * @param $day
   * @param string $startHours
   * @param string $endHours
   *
   * @return array
   *
   * @throws \Exception
   */
  private function calculateAndAddUsage($startHours, $endHours) {
    $usage = $this->calculateUsage($startHours, $endHours);
    $this->totalHoursUsed = $this->totalHoursUsed + $usage['hours'];
    $this->totalHoursUsed = $this->totalHoursUsed + ($usage['minutes'] / 60);
    return $usage;
  }

  /**
   * Calculate the usage between two intervals.
   *
   * @param string $startHours
   * @param string $endHours
   *
   * @return array
   *
   * @throws \Exception
   */
  private function calculateUsage($startHours, $endHours) {
    $startDate = $this->getIntervalDate($startHours);
    $endDate = $this->getIntervalDate($endHours);

    return [
      'days' => (int) $endDate->diff($startDate)->format('%d'),
      'hours' => (int) $endDate->diff($startDate)->format('%h'),
      'minutes' => (int) $endDate->diff($startDate)->format('%i'),
    ];
  }

  /**
   * Get a date object from an interval given.
   *
   * @param string $interval
   *
   * @return \DateTime
   *
   * @throws \Exception
   */
  private function getIntervalDate($interval) {
    $intervalObj = new \DateInterval($interval);
    $dateObj = new \DateTime('00:00');
    $dateObj->add($intervalObj);

    return $dateObj;
  }

}
