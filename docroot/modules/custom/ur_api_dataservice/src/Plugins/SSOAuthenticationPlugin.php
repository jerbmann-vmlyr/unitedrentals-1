<?php

namespace Drupal\ur_api_dataservice\Plugins;

use Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException;

use Drupal\ur_api_dataservice\Adapters\DalAdapter;
use Drupal\ur_api_dataservice\Traits\RetrievesDrupalSetting;

/**
 * Class SSOAuthenticationPlugin.
 *
 * @package Drupal\ur_api_dataservice\Plugins
 */
class SSOAuthenticationPlugin extends BasePlugin {

  use RetrievesDrupalSetting;

  private $dalEndpoint = '/ssologins';

  /**
   * SSOAuthenticationPlugin constructor.
   *
   * @throws \Exception
   */
  public function __construct() {
    parent::__construct();

    $this->adapter = new DalAdapter();
    $this->adapter->setEndpoint($this->dalEndpoint);
  }

  /**
   * Returns the common auth configured token.
   *
   * @return array|mixed|null
   *   The token from drupal config.
   * @throws \Exception
   *
   * @TODO: Refactor getDrupalConfig to use Drupal native methods
   */
  public static function getAuthenticationWORSToken() {
    return self::getDrupalConfig('auth_common');
  }

  /**
   * Authenticates the user via SSO.
   *
   * @param $email
   *   The email address we are attempting to log in with.
   *
   * @return bool
   *   True if SSO succeeded.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Exception
   */
  public function authenticate($email) {
    $token = $this->getAuthenticationWORSToken();
    $this->adapter->setAuthToken($token);

    // @TODO: Probably should validate this is an email address.
    $this->adapter->addParameter('id', $email);

    try {
      $response = $this->adapter->execute();
    }
    catch (\Exception $e) {
      throw new SSOAuthenticationException($e->getMessage(), 500);
    }

    $_SESSION['ur_dal_user_token'] = $response->result->token;
    return TRUE;
  }

}
