<?php

namespace Drupal\ur_api_dataservice\Plugins;

use Drupal\ur_frontend_api_v2\Models\ChargeEstimate;
use Drupal\ur_frontend_api_v2\Models\Voucher;
use Voucherify\VoucherifyClient;
use Voucherify\ClientException;

/**
 * Class VoucherPlugin.
 *
 * @package Drupal\ur_api_dataservice\Plugins
 */
class VoucherPlugin extends BasePlugin {

  protected static $config;
  protected static $apiId;
  protected static $apiKey;
  protected static $apiVersion;
  protected static $apiUrl;

  public function __construct($model) {

    parent::__construct($model);

    static::$config = \Drupal::config('ur_voucher.settings');
    static::$apiId = static::$config->get('apiId');
    static::$apiKey = static::$config->get('apiKey');
    static::$apiVersion = static::$config->get('apiVersion') ?? NULL;
    static::$apiUrl = static::$config->get('apiUrl');

  }

  /**
   * @param string $voucherCode
   * @return array|false|mixed|object
   */
  public function retrieveVoucher($voucherCode) {

    $client = new VoucherifyClient(static::$apiId, static::$apiKey, static::$apiVersion, static::$apiUrl);
    $voucher = new Voucher();

    try {

      // get voucher
      $voucherifyResponse = $client->validations->validate($voucherCode);

      // populate Voucher model object
      if ($voucherifyResponse->valid) {
        $voucher->status = TRUE;
        $voucher->code = $voucherifyResponse->code;
        $voucher->discount = $voucherifyResponse->discount;
        $voucher->metadata = $voucherifyResponse->metadata;
      }
      else {
        $voucher->status = FALSE;
        $voucher->errorMessage = $voucherifyResponse->reason;
      }

    }
    catch (ClientException $e) {
      $voucher->status = FALSE;
      // todo: real error message from i18n
      $voucher->errorMessage = 'ERROR of some sort';
    }

    return $voucher;

  }

  /**
   * @param \Drupal\ur_frontend_api_v2\Models\ChargeEstimate $chargeEstimate
   * @param \Drupal\ur_frontend_api_v2\Models\Voucher $voucher
   * @return \Drupal\ur_frontend_api_v2\Models\ChargeEstimate
   */
  public function applyVoucher(ChargeEstimate $chargeEstimate, Voucher $voucher): ChargeEstimate {

    // todo: if needed, apply discount only to items whose catClass matches a
    // todo: list of classes returned as metadata by Voucherify (specified in admin)
    switch ($voucher->discount->type) {

      case 'PERCENT':

        foreach ($chargeEstimate->items as &$item) {

          // percentage comes back from Voucherify as integer
          // divide by 100 and ensure result is a decimal between 0 and 1
          // set to 0 (no discount) if error
          $discountPercentage = $voucher->discount->percent_off / 100;
          $discountPercentage = ($discountPercentage > 0 && $discountPercentage < 1) ? $discountPercentage : 0;

          // maximum comes back from Voucherify in cents, only if it exists in
          // admin, null otherwise; if null, set to the item rate
          $discountMaximum = $voucher->discount->amount_limit / 100;

          if (!$item->hasContractPricing) {
            // apply discount equal to lesser of percent or maximum
            $item->minRate -= min($item->minRate * $discountPercentage, $discountMaximum ?: $item->minRate);
            $item->dayRate -= min($item->dayRate * $discountPercentage, $discountMaximum ?: $item->dayRate);
            $item->weekRate -= min($item->weekRate * $discountPercentage, $discountMaximum ?: $item->weekRate);
            $item->monthRate -= min($item->monthRate * $discountPercentage, $discountMaximum ?: $item->monthRate);
          }
        }

        break;

      case 'AMOUNT':

        foreach ($chargeEstimate->items as &$item) {

          // amount comes back from Voucherify in cents
          // divide by 100 to convert to dollars
          $discountAmount = $voucher->discount->amount_off / 100;

          // apply discount equal to discountAmount, but not to exceed item rate
          $item->minRate -= $item->minRate - $discountAmount > 0 ? $discountAmount : 0;
          $item->dayRate -= $item->dayRate - $discountAmount > 0 ? $discountAmount : 0;
          $item->weekRate -= $item->weekRate - $discountAmount > 0 ? $discountAmount : 0;
          $item->monthRate -= $item->monthRate - $discountAmount > 0 ? $discountAmount : 0;

        }

        break;

      }

      return $chargeEstimate;

  }

}
