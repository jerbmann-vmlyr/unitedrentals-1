<?php

namespace Drupal\ur_api_dataservice\Plugins;

use Drupal;
use Doctrine\Common\Collections\ArrayCollection;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\ur_api_dataservice\Exceptions\EndpointException;
use Drupal\ur_api_dataservice\Data\ParameterData;
use Drupal\ur_utils\Traits\UrCacheSettings;

/**
 * Class BasePlugin
 *
 * This is the base class used for all Plugins.
 *
 * @TODO: Make an interface for this OR simply use it as the base for a class of
 *        Plugins that are extended, then turn this BasePlugin into
 *        "BaseDalPlugin" or extend it in "BaseDalPlugin".
 *
 * @package Drupal\ur_api_dataservice\Plugins
 */
abstract class BasePlugin {

  use UrCacheSettings;

  /**
   * The unique "key" for this plugin
   *
   * @var string
   */
  protected $uKey;

  /**
   * Placeholder for the current method used for this instance.
   *
   * @var string
   */
  protected $method = '';

  /**
   * The data model that will be passed back and forth with frontend_api.
   *
   * @var string
   */
  protected $dataModel;

  /**
   * A list of the properties for the current data model.
   *
   * @var array
   */
  protected $dataModelProperties;

  /**
   * The parameters specific to this plugin.
   *
   * @var array|NULL
   *
   * Example:
   * $parameters = [
   *   'id' => [
   *     'type' => 'string',
   *     'required' => ['read'],
   *     'possible' => [],
   *     'imports' => '',
   *     'mapsTo' => NULL,
   *   ],
   *   'status' => [
   *     'type' => 'string',
   *     'required' => [],
   *     'possible' => [],
   *     'imports' => [
   *       'A' => TRUE,
   *       'D' => FALSE,
   *     ],
   *     'mapsTo' => 'active',
   *   ],
   * ]
   */
  protected $parameters;

  /**
   * HTTP methods allowed by this endpoint.
   */
  protected $allowedMethods = [];

  /**
   * Placeholder for the data adapter to use
   */
  protected $adapter;

  /**
   * @var array
   */
  protected $modelProperties = [];

  /**
   * Array of plugins that OrderPlugin will depend on
   * @var [] => converted to ArrayCollection on construct
   */
  protected $plugins = [];

  /**
   * BasePlugin constructor.
   *
   * @param null|string $model
   */
  public function __construct($model = NULL) {
    $this->registerPlugins()
      ->setModel($model);

    $this->modelProperties = $this->getModelProperties();

    /**  TODO: move all "parameter" functions into the ParameterData factory */
    if ($this->uKey) {
      $built = ParameterData::build($this->uKey);
      $this->parameters = $built->assembled;
    }

    // @TODO: Look to add the DalAdapter here.
    /*
     * @TODO: Probably a better idea to create a DalPlugin that extends BasePlugin
     *        and then have all the plugins that use the Dal extend that. Simple!
     */
  }

  /**
   * Set the model for this plugin.
   *
   * Only sets it if the model specified exists.
   *
   * @param null|string $model
   */
  public function setModel($model = NULL):void {
    if (!empty($model)) {
      if (is_string($model) && class_exists($model)) {
        $this->dataModel = $model;
      }
      elseif (is_object($model)) {
        $class = get_class($model);
        if (!empty($class) && class_exists($class)) {
          $this->dataModel = $class;
        }
      }
    }
  }

  /**
   * Ensure that the http verb is allowed on the DAL endpoint.
   *
   * @return bool
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\EndpointException
   */
  protected function allow():?bool {
    if (!\in_array($this->adapter->method, $this->allowedMethods)) {
      throw new EndpointException('Error: ' . $this->method . ' is not allowed for this endpoint.');
    }

    return TRUE;
  }

  /**
   * Export.
   *
   * Export the parameterized data from Kelex -> to -> DAL
   * See \Drupal\ur_appliance\Plugins\src\Contract.
   */
  protected function export() {
    // Use as needed
  }

  /**
   * Import.
   *
   * Import the normalized data from DAL -> to -> Kelex
   * See \Drupal\ur_appliance\Plugins\src\Contract.
   *
   * @param $data
   * @param bool $importSingle
   * @return array|mixed
   */
  public function import($data, $importSingle = FALSE) {
    // @TODO Update this method to use new normalization/parameterization stuff
    $out = [];

    if ($importSingle) {
      return $this->fillModel($data);
    }

    if (\is_array($data)) {
      foreach ($data as $d) {
        // Apply all the normalizations (import transformations) for this method.
        $out[] = $this->fillModel($d);
      }
    }
    elseif (\is_object($data)) {
      $out[] = $this->fillModel($data);
    }

    return $out;
  }

  /**
   * FillModel.
   *
   * Fill a new instance of the data model with the matching data.
   * Except for simple instances, you'll probably want to override this
   * method in your plugin.
   *
   * @param $data
   * @return mixed
   */
  public function fillModel($data) {
    $data = (array) $this->normalize($data);

    $this->fillData($data, $item);
    $this->fillMappedData($data, $item);

    return $item;
  }

  /**
   * Gets the names of all the properties of the class.
   *
   * @param $class
   * @return array
   */
  public static function getClassProperties($class):?array {
    $keys = [];
    $classVars = get_class_vars($class);

    if (!empty($classVars) && is_array($classVars)) {
      $keys = array_keys($classVars);
    }

    return $keys;
  }

  /**
   * Gets a list of the model properties.
   *
   * @param bool $resetCache
   * @return array
   */
  public function getModelProperties($resetCache = FALSE):?array {
    // Cache the properties
    if ($resetCache || empty($this->dataModelProperties)) {
      $dataModel = $this->dataModel;
      $properties = self::getClassProperties($dataModel);

      $this->dataModelProperties = $properties;
    }

    return $this->dataModelProperties;
  }


  /**
   * Checks to see if the model has a specific property set.
   *
   * @param $property
   * @return bool
   */
  public function modelHasProperty($property):bool {
    return \in_array($property, $this->modelProperties);
  }

  /**
   * Fill in the data that has a one-to-one mapping with the model.
   *
   * @param $data
   *   - The incoming data.
   *
   * @param $item
   *   - The current instance of the model. Passed by reference.
   */
  public function fillData($data, &$item):void {
    // Make sure $data is always an array
    $data = (array) $data;

    // The model/class we are filling against
    if (!is_object($item)) {
      $item = new $this->dataModel();
    }

    // Pull in data with one-to-one mapping in the data and model
    foreach ($this->modelProperties as $key) {
      if (isset($data[$key])) {
        $item->{$key} = $data[$key];
      }
    }
  }

  /**
   * For fields that do not match one to one in the model, check the incoming
   * data for known parameters that map to a field of a different name and
   * populate them accordingly.
   *
   * @param $data
   *   - The data from the data source, used to populate the model.
   *
   * @param $item
   *   - The active instance of the model. Passed by reference.
   */
  public function fillMappedData($data, &$item):void {
    // Fill in data that maps to somewhere else in the model
    foreach ($this->parameters as $field => $settings) {
      if (!empty($settings['mapsTo'])) {
        $modelKey = $settings['mapsTo'];

        // Verify we have the field in incoming data and that the model still
        // accepts the property.
        if (isset($data[$field]) && $this->modelHasProperty($modelKey)) {
          $item->$modelKey = $data[$field];
        }
      }
    }
  }

  /**
   * ParseResponse.
   *
   * @param $response
   * @return mixed
   */
  public function parseResponse($response) {
    $body = $response->result;
    return $body;
  }

  /**
   * Base normalize method - should be overridden by a trait in the specific
   * plugin.
   *
   * @param $data
   *
   * @return mixed
   */
  public function normalize($data) {
    return $data;
  }

  /**
   * Base parameterize method - should be overridden by a trait in the specific
   * plugin.
   *
   * @param $data
   *
   * @return mixed
   */
  public function parameterize($data) {
    return $data;
  }

  /**
   * @param $key
   * @return mixed
   * @throws PluginException
   * @throws \ReflectionException
   */
  protected function plugin($key)
  {
    if ($this->plugins->containsKey($key))
      return $this->plugins->get($key);

    $code = 500;
    $class = (new \ReflectionClass($this))->getShortName() . '.php';
    $registeredPlugins = implode(', ', $this->plugins->getKeys());
    $message = "\n\n
      ~~~ Plugin Key, {$key}, Is Not Registered to {$class} Properly ~~~\n
          Problem: {$class} has not registered {$key} Plug In \n
          Missing Plugin key: {$key} \n
          Possible Problem Solution: Add {$key} to {$class} \$plugins property array (as a str) \n \n
    
          Currently Registered Plugins that {$class} has access to: \n 
          {$registeredPlugins} \n\n
      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ";

    throw new PluginException($message, $code);
  }

  /**
   * Register Plugins and transform self::$plugins to ArrayCollection where
   * key = pluginKey
   * value = plugin registered in service container
   * @return $this
   */
  protected function registerPlugins()
  {
    $plugins = array_map(function($plugin) {
      return Drupal::service('ur_api_dataservice')->getPlugin($plugin);
    }, $this->plugins);

    $this->plugins = new ArrayCollection(
      array_combine(
        $this->plugins,
        $plugins
      )
    );

    return $this;
  }

}
