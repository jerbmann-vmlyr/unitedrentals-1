<?php

namespace Drupal\ur_api_dataservice\Interfaces;

/**
 * Interface CacheItemInterface.
 *
 * @package Drupal\ur_api_dataservice\Interfaces
 */
interface CacheItemInterface {

  /**
   * @param $data
   * @param $parameters
   * @param $cacheSettings
   *
   * @return mixed
   */
  public static function store($data, $parameters, $cacheSettings);

  /**
   * @param $itemId
   * @param $cacheSettings
   *
   * @return mixed
   */
  public static function get($itemId, $cacheSettings);

}
