<?php

namespace Drupal\ur_api_dataservice\Interfaces;

/**
 * Interface CacheCollectionInterface.
 *
 * @package Drupal\ur_api_dataservice\Interfaces
 */
interface CacheCollectionInterface {

  /**
   * Find a cache collection from parameters.
   *
   * @param $parameters
   * @param $cacheSettings
   *
   * @return mixed
   */
  public static function find($parameters, $cacheSettings);

  /**
   * Stores a cache collection from parameters.
   *
   * @param $dataSet
   * @param $parameters
   * @param $cacheSettings
   *
   * @return mixed
   */
  public static function store($dataSet, $parameters, $cacheSettings);

}
