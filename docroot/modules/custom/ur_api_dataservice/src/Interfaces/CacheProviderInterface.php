<?php

namespace Drupal\ur_api_dataservice\Interfaces;

/**
 * Interface CacheProviderInterface
 *
 * @package Drupal\ur_api_dataservice\Interfaces
 */
interface CacheProviderInterface {

  /**
   * Retrieve collection of cached data.
   *
   * @param $user
   * @param $parameters
   * @param $cacheSettings
   *
   * @return mixed
   */
  public function getCollection($user, $parameters, $cacheSettings);

}
