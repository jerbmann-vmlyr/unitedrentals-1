<?php

namespace Drupal\ur_api_dataservice\tests\Unit;

use Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException;
use Drupal\ur_api_dataservice\Traits\ReadsParameters;
use Drupal\ur_api_dataservice\Traits\VerifiesParameters;
use Drupal\Tests\UnitTestCase;

/**
 * Class VerifiesParametersTraitTest
 *
 * @package Drupal\ur_api_dataservice\tests\Unit
 */
class VerifiesParametersTraitTest extends UnitTestCase {

  use ReadsParameters;
  use VerifiesParameters;

  /**
   * Tests to check if it throws an exception with missing parameters.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   */
  public function testItThrowsExceptionOnMissingParameters() {
    $data = ['notARequiredFieldToBeSeen' => TRUE];
    $this->parameters = [
      'requiredField' => [
        'required' => ['create', 'read', 'update', 'delete'],
      ],
    ];

    $this->expectException(MissingRequiredParametersException::class);
    $result = $this->verifyRequiredParameters($data, 'read');
  }

  /**
   * Test that it doesn't throw an exception when the parameters exist.
   * 
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   */
  public function testDoesNotThrowExceptionWhenParametersExist() {
    $data = ['requiredField' => TRUE];
    $this->parameters = [
      'requiredField' => [
        'required' => ['create', 'read', 'update', 'delete'],
      ],
    ];

    $result = $this->verifyRequiredParameters($data, 'read');
    $this->assertTrue($result);
  }

}
