<?php

namespace Drupal\ur_api_dataservice\tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\ur_api_dataservice\Traits\ParameterizesData;

/**
 * Class ParameterizesDataTraitTest
 *
 * @package Drupal\ur_api_dataservice\tests\Unit
 */
class ParameterizesDataTraitTest extends UnitTestCase {

  public $parameters = [
    'description' => [
      'type' => 'string',
      'required' => ['create'],
      'possible' => [],
      'imports' => [
        '1' => 'Amazing',
        '2' => 'Astounding',
        '3' => 'Spectacular',
      ],
    ],
    'testNumericField' => [
      'type' => 'string',
      'required' => [],
      'possible' => [],
      'imports' => 'N',
    ],
    'testTrueFalseField' => [
      'type' => 'string',
      'required' => [],
      'possible' => [],
      'imports' => 'TF',
    ],
    'testTrueFalse10Field' => [
      'type' => 'string',
      'required' => [],
      'possible' => [],
      'imports' => '10',
    ],
    'testTrueFalseYNField' => [
      'type' => 'string',
      'required' => [],
      'possible' => [],
      'imports' => 'YN',
    ],
  ];

  use ParameterizesData;

  /**
   * Test it converts generics.
   */
  public function testItConvertsGenerics() {
    $test = ['description' => 'Astounding', 'anotherThing' => TRUE];
    $result = $this->parameterize($test);

    $this->assertArrayEquals([
      'description' => '2',
      'anotherThing' => TRUE,
    ], $result);
  }

  /**
   * Test it can convert boolean-ish values to  TRUE booleans.
   */
  public function testItConvertsBooleans() {
    $test = [
      'testTrueFalseField' => TRUE,
    ];
    $result = $this->parameterize($test);

    $this->assertArrayEquals([
      'testTrueFalseField' => ' TRUE',
    ], $result);

    $test = [
      'testTrueFalseField' => FALSE,
    ];
    $result = $this->parameterize($test);

    $this->assertArrayEquals([
      'testTrueFalseField' => 'FALSE',
    ], $result);
  }

  /**
   * Test it can convert 1/0 to  TRUE/FALSE boolean values.
   */
  public function testItConverts1sAnd0sToBooleans() {
    $test = [
      'testTrueFalse10Field' => TRUE,
    ];
    $result = $this->parameterize($test);

    $this->assertArrayEquals([
      'testTrueFalse10Field' => '0',
    ], $result);

    $test = [
      'testTrueFalse10Field' => FALSE,
    ];
    $result = $this->parameterize($test);

    $this->assertArrayEquals([
      'testTrueFalse10Field' => '1',
    ], $result);
  }

  /**
   * Test it can convert "Y"/"N" strings to boolean values.
   */
  public function testItConvertsYsAndNsToBooleans() {
    $test = [
      'testTrueFalseYNField' => TRUE,
    ];
    $result = $this->parameterize($test);

    $this->assertArrayEquals([
      'testTrueFalseYNField' => 'Y',
    ], $result);

    $test = [
      'testTrueFalseYNField' => FALSE,
    ];
    $result = $this->parameterize($test);

    $this->assertArrayEquals([
      'testTrueFalseYNField' => 'N',
    ], $result);
  }

}
