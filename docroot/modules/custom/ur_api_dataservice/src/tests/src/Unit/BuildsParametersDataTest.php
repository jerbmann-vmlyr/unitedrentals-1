<?php

namespace Drupal\ur_api_dataservice\tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\ur_api_dataservice\Data\ParameterData;

class BuildsParametersDataTest extends UnitTestCase {

  protected $parameters;

  public function testItBuildsParameters() {
    $data = ParameterData::build('TestData');
    $this->parameters = $data->assembled;

    $expected = [
      'description' => [
        'type' => 'string',
        'required' => ['create'],
        'possible' => [],
        'imports' => [
          '1' => 'Amazing',
          '2' => 'Astounding',
          '3' => 'Spectacular',
        ],
        'enabled' => TRUE,
      ]
    ];

    $this->assertArrayEquals($expected, $this->parameters);
  }

}
