<?php

namespace Drupal\ur_api_dataservice\tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\ur_api_dataservice\Traits\CalculatesStatuses;
use Drupal\webform\Plugin\WebformElement\DateTime;

class CalculatesStatusesTraitTest extends UnitTestCase {

  use CalculatesStatuses;

  /**
   * Type 1
   * Possible values:
   *  ['OnRent'] // is NOT overdue, is NOT pending due, is NOT in leniency
   *  ['Overdue', 'OnRent'] // is NOT pending due, is NOT in leniency
   *  ['PendingDue', 'OnRent'] // is NOT overdue, is NOT in leniency
   *  ['OnRent', 'InLeniency'] // is NOT overdue, is NOT pending due
   *  ['Overdue', 'OnRent', 'InLeniency'] // is NOT pending due
   *  ['PendingDue', 'OnRent', 'InLeniency'] // is NOT overdue
   *
   * Type 2
   * Possible values:
   *  ['Quote']
   *
   * Type 3
   * Possible values:
   *  ['ReservedForDelivery']
   *  ['ReservedForBranchPickup']
   *
   * Type 4
   * Possible values:
   *  ['OffRent']
   *
   * Type 6
   * Possible values:
   *  ['PickupRequested']
   *  ['PickupRequested', 'InLeniency']
   *  ['AdvancedPickup', 'OnRent']
   *  ['AdvancedPickup', 'Overdue', 'OnRent']
   *  ['AdvancedPickup', 'Overdue', 'OnRent', 'InLeniency']
   *  ['AdvancedPickup', 'PendingDue', 'OnRent']
   *  ['AdvancedPickup', 'PendingDue', 'OnRent', 'InLeniency']
   */
  protected $type1_1;
  protected $type1_2;
  protected $type1_3;
  protected $type1_4;
  protected $type1_5;
  protected $type1_6;
  protected $type2_1;
  protected $type3_1;
  protected $type3_2;
  protected $type4_1;
  protected $type6_1;
  protected $type6_2;
  protected $type6_3;
  protected $type6_4;
  protected $type6_5;
  protected $type6_6;
  protected $type6_7;

  public function mutateTime($time, $mode, $interval = NULL) {
    $new = clone $time;
    if ($mode == 'add') {
      $out = $new->add(new \DateInterval($interval));
    }
    if ($mode == 'sub') {
      $out = $new->sub(new \DateInterval($interval));
    }
    return $out;
  }

  public function setUp() {
    parent::setUp();

    // Go get the raw test data...
    $json = \file_get_contents("./Data/transactions.json");
    $rawData = \json_decode(file_get_contents("./Data/transactions.json"));

    $now = new \DateTime();
    $now->setTimezone(new \DateTimeZone('UTC'));
    $past = $this->mutateTime($now, 'sub', 'P10D')->format('Y-m-d H:i:s');
    $nearPast = $this->mutateTime($now, 'sub', 'P1D')->format('Y-m-d H:i:s');
    $midPast = $this->mutateTime($now, 'sub', 'P5D')->format('Y-m-d H:i:s');
    $future = $this->mutateTime($now, 'add', 'P10D')->format('Y-m-d H:i:s');
    $nearFuture = $this->mutateTime($now, 'add', 'P1D')->format('Y-m-d H:i:s');
    $midFuture = $this->mutateTime($now, 'add', 'P5D')->format('Y-m-d H:i:s');

    /** Trans Type 1 */
    $transType1 = $rawData->type1;

    // Type 1 : 1
    $this->type1_1 = clone $transType1;
    $this->type1_1->startDateTime = $past; // Happens in the past
    $this->type1_1->returnDateTime = $future; // Happens in the future, more than three days

    // Type 1 : 2
    $this->type1_2 = clone $transType1;
    $this->type1_2->startDateTime = $past; // Happens in the past
    $this->type1_2->returnDateTime = $nearPast; // Happens in the past

    // Type 1 : 3
    $this->type1_3 = clone $transType1;
    $this->type1_3->startDateTime = $past; // Happens in the past
    $this->type1_3->returnDateTime = $nearFuture; // Happens within three days

    // Type 1 : 4
    $this->type1_4 = clone $transType1;
    $this->type1_4->startDateTime = $past; // Happens in the past
    $this->type1_4->returnDateTime = $future; // Happens in the future
    $this->type1_4->leniencyStartDate = $midPast; // Happens in the past
    $this->type1_4->leniencyEndDate = $midFuture; // Happens in the future

    // Type 1 : 5
    $this->type1_5 = clone $transType1;
    $this->type1_5->startDateTime = $past; // Happens in the past
    $this->type1_5->returnDateTime = $midPast; // Happens in the past
    $this->type1_5->leniencyStartDate = $midPast; // Happens in the past
    $this->type1_5->leniencyEndDate = $midFuture; // Happens in the future

    // Type 1 : 6
    $this->type1_6 = clone $transType1;
    $this->type1_6->startDateTime = $past; // Happens in the past
    $this->type1_6->returnDateTime = $nearFuture; // Happens within three days
    $this->type1_6->leniencyStartDate = $midPast; // Happens in the past
    $this->type1_6->leniencyEndDate = $midFuture; // Happens in the future

    /** Trans Type 2 */
    $transType2 = $rawData->type2;

    //  Type 2 : 1
    $this->type2_1 = clone $transType2;

    /** Trans Type 3 */
    $transType3 = $rawData->type3;

    // Type 3 : 1
    $this->type3_1 = clone $transType3;
    $this->type3_1->urDeliver = TRUE;

    // Type 3 : 2
    $this->type3_2 = clone $transType3;
    $this->type3_2->urDeliver = FALSE;

    /** Trans Type 4 */
    $transType4 = $rawData->type4;

    // Type 4 : 1
    $this->type4_1 = clone $transType4;

    /** Trans Type 6 */
    $transType6 = $rawData->type6;

    // Type 6 : 1
    $this->type6_1 = clone $transType6;
    $this->type6_1->pickupType = 'scheduled';

    // Type 6 : 2
    $this->type6_2 = clone $transType6;
    $this->type6_2->pickupType = 'scheduled';

    // Type 6 : 3
    $this->type6_3 = clone $transType6;
    $this->type6_3->pickupType = 'advanced';
    $this->type6_3->startDateTime = $nearPast; // Happens in the past
    $this->type6_3->returnDateTime = $future; // Happens in the future, more than three days

    // Type 6 : 4
    $this->type6_4 = clone $transType6;
    $this->type6_4->pickupType = 'advanced';
    $this->type6_4->startDateTime = $past; // Happens in the past
    $this->type6_4->returnDateTime = $nearPast; // Happens in the past

    // Type 6 : 5
    $this->type6_5 = clone $transType6;
    $this->type6_5->pickupType = 'advanced';
    $this->type6_5->startDateTime = $past; // Happens in the past
    $this->type6_5->returnDateTime = $midPast; // Happens in the past
    $this->type6_5->leniencyStartDate = $midPast; // Happens in the past
    $this->type6_5->leniencyEndDate = $midFuture; // Happens in the future

    // Type 6 : 6
    $this->type6_6 = clone $transType6;
    $this->type6_6->pickupType = 'advanced';
    $this->type6_6->startDateTime = $past; // Happens in the past
    $this->type6_6->returnDateTime = $nearFuture; // Happens within three days
    $this->type6_6->leniencyStartDate = "";
    $this->type6_6->leniencyEndDate = "";

    // Type 6 : 7
    $this->type6_7 = clone $transType6;
    $this->type6_7->pickupType = 'advanced';
    $this->type6_7->startDateTime = $past; // Happens in the past
    $this->type6_7->returnDateTime = $nearFuture; // Happens within three days
    $this->type6_7->leniencyStartDate = $midPast; // Happens in the past
    $this->type6_7->leniencyEndDate = $midFuture; // Happens in the future
  }

  public function testType1_1() {
    $result = $this->parseTransaction($this->type1_1);
    $this->assertArrayEquals(['OnRent'], $result);
  }

  public function testType1_2() {
    $result = $this->parseTransaction($this->type1_2);
    $this->assertArrayEquals(['Overdue', 'OnRent'], $result);
  }

  public function testType1_3() {
    $result = $this->parseTransaction($this->type1_3);
    $this->assertArrayEquals(['PendingDue', 'OnRent'], $result);
  }

  public function testType1_4() {
    $result = $this->parseTransaction($this->type1_4);
    $this->assertArrayEquals(['OnRent', 'InLeniency'], $result);
  }

  public function testType1_5() {
    $result = $this->parseTransaction($this->type1_5);
    $this->assertArrayEquals(['Overdue', 'OnRent', 'InLeniency'], $result);
  }

  public function testType1_6() {
    $result = $this->parseTransaction($this->type1_6);
    $this->assertArrayEquals(['PendingDue', 'OnRent', 'InLeniency'], $result);
  }

  public function testType2_1() {
    $result = $this->parseTransaction($this->type2_1);
    $this->assertArrayEquals(['Quote'], $result);
  }

  public function testType3_1() {
    $result = $this->parseTransaction($this->type3_1);
    $this->assertArrayEquals(['ReservedForDelivery'], $result);
  }

  public function testType3_2() {
    $result = $this->parseTransaction($this->type3_2);
    $this->assertArrayEquals(['ReservedForBranchPickup'], $result);
  }

  public function testType4_1() {
    $result = $this->parseTransaction($this->type4_1);
    $this->assertArrayEquals(['OffRent'], $result);
  }

  public function testType6_1() {
    $result = $this->parseTransaction($this->type6_1);
    $this->assertArrayEquals(['PickupRequested'], $result);
  }

  public function testType6_2() {
    $result = $this->parseTransaction($this->type6_2);
    $this->assertArrayEquals(['PickupRequested', 'InLeniency'], $result);
  }

  public function testType6_3() {
    $result = $this->parseTransaction($this->type6_3);
    $this->assertArrayEquals(['AdvancedPickup', 'OnRent'], $result);
  }

  public function testType6_4() {
    $result = $this->parseTransaction($this->type6_4);
    $this->assertArrayEquals(['AdvancedPickup', 'Overdue', 'OnRent'], $result);
  }

  public function testType6_5() {
    $result = $this->parseTransaction($this->type6_5);
    $this->assertArrayEquals(['AdvancedPickup', 'Overdue', 'OnRent', 'InLeniency'], $result);
  }

  public function testType6_6() {
    $result = $this->parseTransaction($this->type6_6);
    $this->assertArrayEquals(['AdvancedPickup', 'PendingDue', 'OnRent'], $result);
  }

  public function testType6_7() {
    $result = $this->parseTransaction($this->type6_7);
    $this->assertArrayEquals(['AdvancedPickup', 'PendingDue', 'OnRent', 'InLeniency'], $result);
  }

  protected function parseTransaction($data) {
    return $this->setStatus($data);
  }

}
