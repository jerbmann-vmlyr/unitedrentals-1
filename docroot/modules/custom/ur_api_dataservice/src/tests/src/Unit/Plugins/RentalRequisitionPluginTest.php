<?php

namespace Drupal\Tests\ur_api_dataservice\Unit\Plugins;

use Drupal\ur_frontend_api_v2\Models\RentalRequisition;

class RentalRequisitionPluginTest extends BasePluginTest {

  public $model;

  public $testData = '{
    "status": 0,
    "messageId": null,
    "messageText": null,
    "request": {
        "id": [
            "18"
        ],
        "status": [
            "RR"
        ],
        "createdguid": "",
        "approverguid": "",
        "reqguid": "",
        "apprinclude": "",
        "apprexclude": "",
        "catClass": "",
        "equipmentId": "",
        "jobId": "",
        "alias": "",
        "lineSeq": null,
        "includecustcodes": "",
        "excludecustcodes": "",
        "requestId": "",
        "returnSeq": ""
    },
    "result": [
        {
            "requestId": "A001457529",
            "branchId": "A77",
            "accountId": "18",
            "statusCode": "RR",
            "statusCodeDesc": "Reservation Requested",
            "statusCodeDescCoe": "",
            "poNumber": "PO998887",
            "reqGuid": "a06da162-b8f8-c542-93b6-1e7056248e78",
            "reqContact": "CURTIS12 LEFEBVRE",
            "reqPhone": "503-565-6555",
            "reqEmail": "USER10@TOTALCONTROL.NET",
            "jobContact": "RICHARD KING",
            "jobPhone": "910-237-3729",
            "jobEmail": "ASDF@ASER.COM",
            "rpp": "N",
            "reqDateTime": "2018-11-21T13:46:00",
            "jobId": "65",
            "jobsiteName": "CAROLINA BAYS EXT",
            "jobsiteAddress1": "X",
            "jobsiteCity": "MYRTLE BEACH",
            "jobsiteState": "SC",
            "customerJobId": "",
            "startDateTime": "2018-11-28T10:00:00",
            "endDateTime": "2019-04-03T13:00:00",
            "delivery": "N",
            "pickup": "N",
            "approverGuid": "3a365995-60fb-0048-99f9-8a5c4b73d085",
            "approverName": "CURTIS 001 TEST",
            "createdGuid": "0e35d520-1297-d44f-8bc1-3a9ae944f648",
            "createdName": "UR VML 7",
            "timeStamp": "2018-11-21 13:46:15",
            "equipmentId": "",
            "miscChg": "0",
            "transChg": "0",
            "make": "",
            "model": "",
            "serial": "",
            "alias": "",
            "catClass": "310-4051",
            "catclassDesc": "BOOM 43-46\' ARTICULATING ELECTRIC",
            "qty": "1",
            "outsourced": "",
            "equipmentType": "",
            "remainingQty": "1",
            "scheduledPickupQty": "0",
            "reservationQuoteId": "",
            "quoteId": "",
            "seqNum": "0",
            "lineSeq": "0",
            "lineNum": "2",
            "custOwn": "N",
            "coeStatusCode": "",
            "bulk": false,
            "meter": "0",
            "dayRate": "0",
            "weekRate": "0",
            "monthRate": "0",
            "totalAmount": "0",
            "vendorName": ""
        },
        {
            "requestId": "A001457529",
            "branchId": "A77",
            "accountId": "18",
            "statusCode": "RR",
            "statusCodeDesc": "Reservation Requested",
            "statusCodeDescCoe": "",
            "poNumber": "PO998887",
            "reqGuid": "a06da162-b8f8-c542-93b6-1e7056248e78",
            "reqContact": "CURTIS12 LEFEBVRE",
            "reqPhone": "503-565-6555",
            "reqEmail": "USER10@TOTALCONTROL.NET",
            "jobContact": "RICHARD KING",
            "jobPhone": "910-237-3729",
            "jobEmail": "ASDF@ASER.COM",
            "rpp": "N",
            "reqDateTime": "2018-11-21T13:46:00",
            "jobId": "65",
            "jobsiteName": "CAROLINA BAYS EXT",
            "jobsiteAddress1": "X",
            "jobsiteCity": "MYRTLE BEACH",
            "jobsiteState": "SC",
            "customerJobId": "",
            "startDateTime": "2018-11-28T10:00:00",
            "endDateTime": "2019-04-03T13:00:00",
            "delivery": "N",
            "pickup": "N",
            "approverGuid": "3a365995-60fb-0048-99f9-8a5c4b73d085",
            "approverName": "CURTIS 001 TEST",
            "createdGuid": "0e35d520-1297-d44f-8bc1-3a9ae944f648",
            "createdName": "UR VML 7",
            "timeStamp": "2018-11-21 13:46:15",
            "equipmentId": "",
            "miscChg": "0",
            "transChg": "0",
            "make": "",
            "model": "",
            "serial": "",
            "alias": "",
            "catClass": "310-4050",
            "catclassDesc": "BOOM 40-46\' ARTICULATING ELEC NARROW",
            "qty": "2",
            "outsourced": "",
            "equipmentType": "",
            "remainingQty": "2",
            "scheduledPickupQty": "0",
            "reservationQuoteId": "",
            "quoteId": "",
            "seqNum": "0",
            "lineSeq": "0",
            "lineNum": "1",
            "custOwn": "N",
            "coeStatusCode": "",
            "bulk": false,
            "meter": "0",
            "dayRate": "0",
            "weekRate": "0",
            "monthRate": "0",
            "totalAmount": "0",
            "vendorName": ""
        }
    ],
    "cached": false,
    "recordCount": 2
}';

  public function __construct() {
    $this->model = new RentalRequisition();

    // Authorize the user with a pre-set test email...
    $authPlugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('SSOAuthentication');
    $authPlugin->authenticate('ur.vml.7@gmail.com');
  }

  public function testImport() {
    // Read requires id...
    $id = '18';
    $status = 'RR';

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RentalRequisition');
    $test = $plugin->import($this->testData, FALSE);
    print_r($test);
  }

}
