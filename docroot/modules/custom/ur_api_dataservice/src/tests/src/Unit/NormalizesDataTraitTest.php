<?php

namespace Drupal\ur_api_dataservice\tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\ur_api_dataservice\Traits\NormalizesData;

/**
 * Class NormalizesDataTraitTest
 *
 * @package Drupal\ur_api_dataservice\tests\Unit
 */
class NormalizesDataTraitTest extends UnitTestCase {

  use NormalizesData;

  public $parameters = [
    'testGenericField' => [
      'type' => 'string',
      'required' => ['create'],
      'possible' => [],
      'imports' => [
        '1' => 'Amazing',
        '2' => 'Astounding',
        '3' => 'Spectacular',
      ],
    ],
    'testNumericField' => [
      'type' => 'string',
      'required' => [],
      'possible' => [],
      'imports' => 'N',
    ],
    'testTrueFalseField' => [
      'type' => 'string',
      'required' => [],
      'possible' => [],
      'imports' => 'TF',
    ],
    'testTrueFalse10Field' => [
      'type' => 'string',
      'required' => [],
      'possible' => [],
      'imports' => '10',
    ],
  ];

  /**
   * Test this can convert generic
   */
  public function testItConvertsGenerics() {
    $test = [
      'testGenericField' => '1',
    ];
    $result = $this->normalize($test);

    $this->assertArrayEquals([
      'testGenericField' => 'Amazing',
    ], $result);
  }

  /**
   * Test it converts string boolean values to  TRUE boolean value
   */
  public function testItConvertsBooleans() {
    $test = [
      'testTrueFalseField' => 'True',
    ];
    $result = $this->normalize($test);

    $this->assertArrayEquals([
      'testTrueFalseField' => TRUE,
    ], $result);
  }

  /**
   * Test it can convert "0" to  TRUE boolean value
   */
  public function testItConverts1sAnd0sToBooleans() {
    $test = [
      'testTrueFalse10Field' => '0',
    ];
    $result = $this->normalize($test);

    $this->assertArrayEquals([
      'testTrueFalse10Field' => TRUE,
    ], $result);
  }

}
