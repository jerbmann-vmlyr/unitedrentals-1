<?php

namespace Drupal\ur_api_dataservice\tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\ur_api_dataservice\Traits\ReadsParameters;

/**
 * Class ReadsParametersTraitTest
 *
 * @package Drupal\ur_api_dataservice\tests\Unit
 */
class ReadsParametersTraitTest extends UnitTestCase {

  use ReadsParameters;

  /**
   * Test if requiredParameters works.
   */
  public function testItReadsRequiredParameters() {
    $this->parameters = [
      'requiredItem' => [
        'required' => ['read', 'update', 'delete'],
      ],
      'notRequiredItem' => [
        'required' => [],
      ],
      'secondRequiredItem' => [
        'required' => ['update', 'delete'],
      ],
    ];

    $required = $this->getRequiredParameters('update');

    $this->assertEquals(['requiredItem', 'secondRequiredItem'], $required);
  }

  /**
   * Test if possibleParameters works.
   */
  public function testItReadsPossibleParameters() {
    $this->parameters = [
      'possibleItem' => [
        'possible' => ['read', 'update', 'delete'],
      ],
      'notPossibleItem' => [
        'possible' => ['create'],
      ],
      'possibleItem2' => [
        'possible' => ['update', 'delete'],
      ],
    ];

    $possible = $this->getPossibleParameters('update');

    $this->assertEquals(['possibleItem', 'possibleItem2'], $possible);
  }

  /**
   * The requiredParameters and possibleParameters should both return an empty
   * array if the calling class does not have a parameters property or the
   * property is empty.
   */
  public function testItReturnsEmptyArrayOnNoParameters() {
    $this->assertEquals([], $this->getPossibleParameters('update'));
    $this->assertEquals([], $this->getRequiredParameters('update'));
    $this->parameters = [];
    $this->assertEquals([], $this->getPossibleParameters('update'));
    $this->assertEquals([], $this->getRequiredParameters('update'));
  }

}
