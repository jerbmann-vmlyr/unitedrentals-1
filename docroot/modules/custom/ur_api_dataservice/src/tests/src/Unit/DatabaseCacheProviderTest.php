<?php

namespace Drupal\ur_api_dataservice\tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\ur_api_dataservice\Providers\Cache\Database\DatabaseCachingProvider;

/**
 * Class DatabaseCacheProviderTest
 *
 * @package Drupal\ur_api_dataservice\tests\Unit
 */
class DatabaseCacheProviderTest extends UnitTestCase {

  /**
   * Test it builds custom keys
   */
  public function testItBuildsCustomKeys() {
    $testData = [
      'field1' => '123456',
      'field2' => '654321',
      'field3' => '134625',
    ];

    $idKeys = ['field1', 'field3'];
    $provider = new DatabaseCachingProvider();
    $response = $provider->getKeyFromData($idKeys, $testData);

    $this->assertEquals('123456|134625', $response);
  }

}
