<?php

namespace Drupal\ur_api_dataservice\tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\ur_api_dataservice\Traits\TimeTransformations;

class TimeTransformationsTraitTest extends UnitTestCase {

  use TimeTransformations;

  public function testIsHappeningNow() {
    $date = new \DateTime();
    $dateString = $date->format('Y-m-d H:i:s');

    $result = $this->happeningNow($dateString, $dateString);
    $this->assertEquals(TRUE, $result);
  }

  public function testIsNotHappeningNow() {
    $date = new \DateTime();
    $fromDateString = $date->format('Y-m-d H:i:s');
    $toDate = $date->add(new \DateInterval('P4D'));
    $toDateString = $toDate->format('Y-m-d H:i:s');

    $result = $this->happeningNow($fromDateString, $toDateString);
    $this->assertEquals(FALSE, $result);
  }

  public function testHappensInThePast() {
    $date = new \DateTime();
    $toDate = $date->sub(new \DateInterval('P10D'));
    $dateString = $toDate->format('Y-m-d H:i:s');

    $result = $this->happensInThePast($dateString);
    $this->assertEquals(TRUE, $result);
  }

  public function testDoesNotHappenInThePast() {
    $date = new \DateTime();
    $toDate = $date->add(new \DateInterval('P10D'));
    $dateString = $toDate->format('Y-m-d H:i:s');

    $result = $this->happensInThePast($dateString);
    $this->assertEquals(FALSE, $result);
  }

  public function testItHappensWithinThreeDays() {
    $date = new \DateTime();
    $dateString = $date->format('Y-m-d H:i:s');

    $result = $this->happensWithinThreeDays($dateString);
    $this->assertEquals(TRUE, $result);
  }

  public function testItDoesNotHappenWithinThreeDays() {
    $date = new \DateTime();
    $toDate = $date->add(new \DateInterval('P10D'));
    $dateString = $toDate->format('Y-m-d H:i:s');

    $result = $this->happensWithinThreeDays($dateString);
    $this->assertEquals(FALSE, $result);
  }

  public function testItHappensTodayOrInThePast() {
    $date = new \DateTime();
    $toDate = $date->sub(new \DateInterval('P10D'));
    $dateString = $toDate->format('Y-m-d H:i:s');

    $result = $this->happensTodayOrInThePast($dateString);
    $this->assertEquals(TRUE, $result);
  }

  public function testItDoesNotHappenTodayOrInThePast() {
    $date = new \DateTime();
    $toDate = $date->add(new \DateInterval('P10D'));
    $dateString = $toDate->format('Y-m-d H:i:s');

    $result = $this->happensTodayOrInThePast($dateString);
    $this->assertEquals(FALSE, $result);
  }

  public function testItHappensTodayOrInTheFuture() {
    $date = new \DateTime();
    $toDate = $date->add(new \DateInterval('P10D'));
    $dateString = $toDate->format('Y-m-d H:i:s');

    $result = $this->happensTodayOrInTheFuture($dateString);
    $this->assertEquals(TRUE, $result);
  }

  public function testItDoesNotHappenTodayOrInTheFuture() {
    $date = new \DateTime();
    $toDate = $date->sub(new \DateInterval('P10D'));
    $dateString = $toDate->format('Y-m-d H:i:s');

    $result = $this->happensTodayOrInTheFuture($dateString);
    $this->assertEquals(FALSE, $result);
  }

  public function testDaysFromNow() {
    $date = new \DateTime();
    $toDate = $date->add(new \DateInterval('P10D'));
    $dateString = $toDate->format('Y-m-d H:i:s');

    $result = $this->daysFromNow($dateString);
    $this->assertEquals(10, $result);
  }

}
