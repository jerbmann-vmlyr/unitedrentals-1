<?php

namespace Drupal\ur_api_dataservice\tests\Integration;

use Drupal\Core\Database\Database;
use Drupal\KernelTests\KernelTestBase;
use Drupal\ur_api_dataservice\Providers\Cache\Database\DatabaseCacheItem;

/**
 * Class DatabaseCacheItemTest
 *
 * @package Drupal\ur_api_dataservice\tests\Integration
 */
class DatabaseCacheItemTest extends KernelTestBase {

  public $connection;

  /**
   * setUp method for the test.
   */
  public function setUp() {
    parent::setUp();

    $this->connection = Database::getConnection();
    $this->connection->query('CREATE TABLE IF NOT EXISTS {api_appliance_v2_cache} ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `itemId` TEXT, `parameters` TEXT, `pluginName` TEXT, `user_id` TEXT, `data` TEXT, `created_at` TEXT, `expires_at` TEXT)');
  }

  /**
   * Test it can cache item to database.
   */
  public function testItCanSaveCacheItemToDatabase() {
    $cacheItem = new DatabaseCacheItem($this->connection, 'CreditCards', 3, '3|3', 'id=3|someotherparam=33923', '{"id": 3235,"name": "Item Name"}', 86400);

    $cacheItem->save();

    $this->assertInstanceOf(DatabaseCacheItem::class, $cacheItem);
  }

}
