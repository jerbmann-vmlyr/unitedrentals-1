<?php

namespace Drupal\ur_api_dataservice\tests\Integration;

use Drupal\KernelTests\KernelTestBase;
use Drupal\ur_api_dataservice\Plugins\CreditCardPlugin;
use Drupal\ur_api_dataservice\Plugins\SSOAuthenticationPlugin;

/**
 * Class CreditCardPluginTest.
 *
 * @package Drupal\ur_api_dataservice\tests\Integration
 */
class CreditCardPluginTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['ur_api_dataservice', 'ur_admin', 'ur_utils'];

  /**
   * setUp method for the test.
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Test the plugin can be instantiated.
   *
   * @throws \Exception
   */
  public function testItCanConstruct() {
    $plugin = new CreditCardPlugin(FALSE);
    $this->assertInstanceOf(CreditCardPlugin::class, $plugin);
  }

  /**
   * Test the plugin can retrieve credit cards.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanRetrieveCreditCards() {
    $authPlugin = new SSOAuthenticationPlugin();
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('CreditCard');
    $this->assertArrayHasKey('id', $plugin->index());
  }

}
