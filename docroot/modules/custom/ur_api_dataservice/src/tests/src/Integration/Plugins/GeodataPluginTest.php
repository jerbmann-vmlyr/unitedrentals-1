<?php

namespace Drupal\ur_api_dataservice\tests\Integration;

use Drupal\KernelTests\KernelTestBase;
use Drupal\ur_frontend_api_v2\Models\Geodata;
use Drupal\ur_api_dataservice\Plugins\GeodataPlugin;

class GeodataPluginTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['ur_api_dataservice', 'ur_admin', 'ur_utils'];

  /**
   * setUp method for this test.
   */
  public function setUp() {
    parent::setUp();
  }

  protected function enable() {
    $authPlugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('SSOAuthentication');
    $authPlugin->authenticate('wayne.anderson@vml.com');

    return \Drupal::service('ur_api_dataservice')
      ->getPlugin('Geodata');
  }

  /**
   * Tests the plugin can be instantiated.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanConstruct() {
    $plugin = new GeodataPlugin(FALSE);
    $this->assertInstanceOf(GeodataPlugin::class, $plugin);
  }

  /**
   * Single piece of equipment with history...
   */
  public function testItCanGetEquipmentHistoryTelemetrics() {
    $plugin = $this->enable();

    /**
     * TODO: These values CANNOT be hard coded like this.
     * We need to come up with a way to get dynamic variables.
     */
    $toDate = date('Ymd'); // Today's date
    $fromDate = date('Ymd', strtotime('-2 week')); // Today minus two weeks

    $sendData = [
      'accountId' => 842859,
      'equipmentId' => '10547337',
      'fromDate' => $fromDate,
      'toDate' => $toDate,
      'includeHistory' => TRUE,
    ];

    $result = $plugin->index('telematics', $sendData);
    $this->assertInstanceOf(Geodata::class, $result);
  }

  /**
   * Single piece of equipment with current data...
   */
  public function testItCanGetEquipmentTelemetrics() {
    $plugin = $this->enable();

    $sendData = [
      'accountId' => 842859,
      'equipmentId' => '10547337',
    ];

    $result = $plugin->index('telematics', $sendData);
    $this->assertInstanceOf(Geodata::class, $result);
  }

  /**
   * Entire account with current data...
   */
  public function testItCanGetAccountTelemetrics() {
    $plugin = $this->enable();

    $sendData = [
      'accountId' => 842859,
    ];

    $result = $plugin->index('telematics', $sendData);
    $this->assertInstanceOf(Geodata::class, $result);
  }

}
