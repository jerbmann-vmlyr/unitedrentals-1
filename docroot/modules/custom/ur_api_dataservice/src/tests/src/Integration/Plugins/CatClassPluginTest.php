<?php

namespace Drupal\ur_api_dataservice\tests\Integration;

use Drupal\KernelTests\KernelTestBase;
use Drupal\ur_frontend_api_v2\Models\CatClass;
use Drupal\ur_api_dataservice\Plugins\CatClassPlugin;

/**
 * Class CreditCardPluginTest.
 *
 * @package Drupal\ur_api_dataservice\tests\Integration
 */
class CatClassPluginTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['ur_api_dataservice', 'ur_admin', 'ur_utils'];

  /**
   * Test the plugin can be instantiated.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanConstruct() {
    $plugin = new CatClassPlugin(FALSE);
    $this->assertInstanceOf(CatClassPlugin::class, $plugin);
  }

  /**
   * Test the plugin can retrieve credit cards.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Exception
   */
  public function testCanRetrieveCatClasses() {
    $authPlugin = \Drupal::service('ur_api_dataservice')->getPlugin('SSOAuthentication');
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('CatClass');

    $this->assertNotEmpty($plugin->index());
  }

}
