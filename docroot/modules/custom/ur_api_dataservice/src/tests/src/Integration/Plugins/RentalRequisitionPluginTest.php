<?php

namespace Drupal\ur_api_dataservice\tests\Integration;

use Drupal\KernelTests\KernelTestBase;
use Drupal\ur_api_dataservice\Plugins\RentalRequisitionPlugin;

/**
 * Class SSOAuthenticationPluginTest.
 *
 * @package Drupal\ur_api_dataservice\tests\Integration
 * @backupGlobals disabled
 * @group integration
 */
class RentalRequisitionPluginTest extends KernelTestBase {

  protected $settings;
  protected $result;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['ur_api_dataservice', 'ur_admin', 'ur_utils'];

  /**
   * setUp method for the test.
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Test to check we can construct.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanConstruct() {
    $plugin = new RentalRequisitionPlugin(FALSE);

    $this->assertInstanceOf(RentalRequisitionPlugin::class, $plugin);
  }

  /**
   * Test the plugin can retrieve requisitions.
   */
  public function testItCanRetrieveRentalRequisitions() {
    $authPlugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('SSOAuthentication');
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RentalRequisition');

    $this->result = $plugin->index('18', 'RR');

    $this->assertArrayHasKey('accountId', $this->result[0]);
  }

  public function testItCanRetrieveOneRequisition() {
    $requestId = 'A001457529';
    $authPlugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('SSOAuthentication');
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RentalRequisition');

    $this->result = (array) $plugin->read($requestId, FALSE);
    $this->assertArrayHasKey('accountId', $this->result);
  }

}
