<?php

namespace Drupal\ur_api_dataservice\tests\Integration;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\Core\Controller\ControllerBaseTest;
use Drupal\Tests\Core\Entity\EntityUnitTest;
use Drupal\Tests\UnitTestCase;
use Drupal\ur_frontend_api_v2\Models\Branch;
use Drupal\ur_api_dataservice\Plugins\BranchPlugin;

use Drupal\ur_api_dataservice\Plugins\SSOAuthenticationPlugin;

/**
 * Class BranchPluginTest.
 *
 * @package Drupal\ur_api_dataservice\tests\Integration
 */
class BranchPluginTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['ur_api_dataservice', 'ur_admin', 'ur_utils', 'system', 'locations', 'user'];

  /**
   * Test the Plugin can be instantiated.
   *
   * @throws \Exception
   */
  public function testItCanConstruct() {
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Branch');

    $this->assertInstanceOf(BranchPlugin::class, $plugin);
  }

  /**
   * Test the plugin can get a branch.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanRetrieveABranch() {
    $authPlugin = new SSOAuthenticationPlugin();
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('Branch');

    $branch = $plugin->read('12E');
    $this->assertInstanceOf('Drupal\ur_frontend_api_v2\Models\Branch', $branch);
  }

  /**
   * Test the plugin can get branches.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanRetrieveBranches() {
    $authPlugin = new SSOAuthenticationPlugin();
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('Branch');

    $branches = $plugin->index();
    print_r($branches);
  }

  /**
   * Test the plugin can get branches by lat / lng.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanRetrieveBranchesByLatLng() {
    $authPlugin = new SSOAuthenticationPlugin();
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('Branch');

    $branches = $plugin->index();
    print_r($branches);
  }

  /**
   * Test the plugin can get branches by address.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanRetrieveBranchesByAddress() {
    $authPlugin = new SSOAuthenticationPlugin();
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('Branch');

    $branches = $plugin->index();
    $branches = (array) $branches;
    $this->assertArrayHasKey('id', $branches);
  }

}
