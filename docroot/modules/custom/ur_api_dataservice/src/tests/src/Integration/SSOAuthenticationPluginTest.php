<?php

namespace Drupal\ur_api_dataservice\tests\Integration;

use Drupal\Tests\UnitTestCase;
use Drupal\ur_api_dataservice\Plugins\SSOAuthenticationPlugin;
use Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException;

/**
 * Class SSOAuthenticationPluginTest.
 *
 * @package Drupal\ur_api_dataservice\tests\Integration
 * @backupGlobals disabled
 * @group integration
 */
class SSOAuthenticationPluginTest extends UnitTestCase {

  /**
   * Test to cehck we can authenticate.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanSSOAuthenticate() {
    $plugin = new SSOAuthenticationPlugin(FALSE);
    $response = $plugin->authenticate('urvml2017.1@gmail.com');
    $this->assertTrue($response);
  }

  /**
   * Test to check it throws an exception when it fails to authenticated.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItThrowsExceptionOnFailToAuthenticate() {
    $this->expectException(SSOAuthenticationException::class);
    $plugin = new SSOAuthenticationPlugin(FALSE);
    $response = $plugin->authenticate('areallyreallybademailaddress@fail.com');
  }

}
