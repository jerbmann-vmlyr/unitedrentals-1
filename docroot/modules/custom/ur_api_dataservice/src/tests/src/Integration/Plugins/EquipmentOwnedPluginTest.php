<?php

namespace Drupal\ur_api_dataservice\tests\Integration;

use Drupal\KernelTests\KernelTestBase;
use Drupal\ur_frontend_api_v2\Models\EquipmentOwned;
use Drupal\ur_api_dataservice\Plugins\EquipmentOwnedPlugin;

use Drupal\ur_api_dataservice\Plugins\SSOAuthenticationPlugin;

/**
 * Class CreditCardPluginTest.
 *
 * @package Drupal\ur_api_dataservice\tests\Integration
 */
class EquipmentOwnedPluginTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['ur_api_dataservice', 'ur_admin', 'ur_utils'];

  /**
   * setUp method for the test.
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Test the plugin can be instantiated.
   *
   * @throws \Exception
   */
  public function testItCanConstruct() {
    $plugin = new EquipmentOwnedPlugin(FALSE);
    $this->assertInstanceOf(EquipmentOwnedPlugin::class, $plugin);
  }

  /**
   * Test the plugin can retrieve available catclasses.
   *
   * @throws \Exception
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanRetrieveAvailableCatClasses() {
    $authPlugin = new SSOAuthenticationPlugin();
    $authPlugin->authenticate('ur.vml.7@gmail.com');
    $plugin = new EquipmentOwnedPlugin();
    $availableCatClasses = $plugin->read();
  }

}
