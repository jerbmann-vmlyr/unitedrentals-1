<?php

namespace Drupal\ur_api_dataservice\tests\Integration;

use Drupal\KernelTests\KernelTestBase;
use Drupal\ur_api_dataservice\Plugins\RentalTransactionPlugin;
use Drupal\ur_api_dataservice\Plugins\SSOAuthenticationPlugin;
use Drupal\ur_frontend_api_v2\Models\RentalTransaction;

/**
 * Class SSOAuthenticationPluginTest.
 *
 * @package Drupal\ur_api_dataservice\tests\Integration
 * @backupGlobals disabled
 * @group integration
 */
class RentalTransactionPluginTest extends KernelTestBase {

  protected $settings;
  protected $result;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['ur_api_dataservice', 'ur_admin', 'ur_utils'];

  /**
   * setUp method for the test.
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Test to check we can construct.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanConstruct() {
    $plugin = new RentalTransactionPlugin(FALSE);

    $this->assertInstanceOf(RentalTransactionPlugin::class, $plugin);
  }

  /**
   * Test the plugin can retrieve transactions for this account
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @group live
   */
  public function testItCanRetrieveRentalTransactions() {
    $authPlugin = new SSOAuthenticationPlugin();
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RentalTransaction');

    $accountId = 1408185;
    $this->result = $plugin->index($accountId, 1);
    $test = (array) $this->result[0];
    $this->assertArrayHasKey('accountId', $test);
  }

  /**
   * Retrieve one transaction
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @group live
   */
  public function testItCanRetrieveOneTransaction() {
    $authPlugin = new SSOAuthenticationPlugin();
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RentalTransaction');

    $transId = 162014261;
    $this->result = (array) $plugin->read($transId, FALSE);
    $this->assertArrayHasKey('accountId', $this->result);

  }

  /**
   * Create a rental transaction
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @group live
   */
  public function testItCanCreateATransaction() {
    $authPlugin = new SSOAuthenticationPlugin();
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RentalTransaction');

    $transTestData = new \stdClass();

    $this->result = (array) $plugin->create($transTestData);
    $this->assertArrayHasKey('accountId', $this->result);
  }

  /**
   * Update a rental transaction
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\SSOAuthenticationException
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @group live
   */
  public function testItCanUpdateATransaction() {
    $authPlugin = new SSOAuthenticationPlugin();
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RentalTransaction');

    $this->testItCanCreateATransaction();

    $transTestData = $this->result;
    $result = $plugin->update($transTestData);
    $this->assertArrayHasKey('accountId', $result);
  }

}
