<?php

namespace Drupal\ur_api_dataservice\tests\Integration;

use Drupal\KernelTests\KernelTestBase;
use Drupal\ur_frontend_api_v2\Models\Account;
use Drupal\ur_api_dataservice\Plugins\AccountPlugin;


/**
 * Class AccountPluginTest
 *
 * @package Drupal\ur_api_dataservice\tests\Integration
 */
class AccountPluginTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['ur_api_dataservice', 'ur_admin', 'ur_utils'];

  /**
   * Test the plugin can be instantiated.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanConstruct() {
    $plugin = new AccountPlugin(FALSE);
    $this->assertInstanceOf(AccountPlugin::class, $plugin);
  }

  /**
   * Test it can retrieve accounts.
   *
   * Note: name, city, state or parentId - Either one of these should be passed to the Plugin
   * Only if customerGroup is passed, it will require to have at least another parameter
   * from keys above in the request.
   */
  public function testItCanRetrieveAccounts() {
    $authPlugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('SSOAuthentication');
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('Account');
    $accounts = $plugin->index([
      'name' => 'CCJV',
//      'city' => 'THE WOODLANDS',
//      'state' => 'TX',
//      'parentId' => '1082042',
//      'customerGroup' => '0',
    ]);

    $accounts = (array) $accounts[0];
    $this->assertArrayHasKey('id', $accounts);
  }

  /**
   * Test it can retrieve one or more accounts with id.
   *
   * Note: This method can retrieve Single Account or more.
   * (id can be comma separated.)
   */
  public function testItCanRetrieveCustomerAccount() {
    $authPlugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('SSOAuthentication');
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('Account');
    $accounts = $plugin->read('180384');

    $this->assertInstanceOf(Account::class, $accounts);
  }

}
