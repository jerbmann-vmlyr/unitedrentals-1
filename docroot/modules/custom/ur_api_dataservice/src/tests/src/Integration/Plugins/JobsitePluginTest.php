<?php

namespace Drupal\ur_api_dataservice\tests\Integration;

use Drupal\KernelTests\KernelTestBase;
use Drupal\ur_frontend_api_v2\Models\Jobsite;
use Drupal\ur_api_dataservice\Plugins\JobsitePlugin;

/**
 * Class JobsitePluginTest
 *
 * @package Drupal\ur_api_dataservice\tests\Integration
 */
class JobsitePluginTest extends KernelTestBase {

  public $jobData;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['ur_api_dataservice', 'ur_admin', 'ur_utils'];

  /**
   * setUp method for this test.
   */
  public function setUp() {
    parent::setUp();

    $this->jobData = '{
      "accountId": "180384",
      "customerJobId": "",
      "location": "E TO E TESTING",
      "branch": "695",
      "name": "E TO E TEST",
      "address1": "NEAR DANIELSON PROV PARK",
      "address2": "#222",
      "city": "ELBOW",
      "state": "SK",
      "zip": "S0H",
      "jobPhone": "503-314-9997",
      "status": "A",
      "statusDesc": "Active",
      "contact": "BOB 250-922-0049",
      "mobilePhone": "360-220-0580",
      "email": "bob@bob.com",
      "preLien": "",
      "cycleBillCode": "",
      "defaultDelivery": true,
      "accessRestrictions": "",
      "branchAccessRestrictions": "",
      "earliestArrivalTime": "2000-01-01T00:00:00",
      "latestArrivalTime": "2020-01-01T00:00:00",
      "rollbackRequired": false,
      "badgingRequired": false,
      "poNumber": "TBD",
      "notes": "",
      "latitude": "51.120314",
      "longitude": "-106.596902",
      "validityCode": "1",
      "fax": null,
      "createdBy": "LKOMADA",
      "createdDate": "2018-11-01T00:00:00",
      "taxDistrict": "700218110",
      "salesRep": "0"
    }';
  }

  /**
   * Tests the plugin can be instantiated.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanConstruct() {
    $plugin = new JobsitePlugin(FALSE);
    $this->assertInstanceOf(JobsitePlugin::class, $plugin);
  }

  /**
   * Tests it can get jobsites.
   */
  public function testItCanRetrieveJobsites() {
    $authPlugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('SSOAuthentication');
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Jobsite');

    $result = $plugin->index('180384');
    $test = (array) $result[0];
    $this->assertArrayHasKey('id', $test);
  }

  /**
   * Tests it can get a single jobsite.
   */
  public function testItCanRetrieveAJobsite() {
    $authPlugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('SSOAuthentication');
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Jobsite');

    $result = $plugin->read('180384', '1');
    $this->assertInstanceOf(Jobsite::class, $result);
  }

  public function testItCanCreateAJobsite() {
    $authPlugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('SSOAuthentication');
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Jobsite');

    $data = json_decode($this->jobData);
    $jobsite = Jobsite::import($data);
    $result = $plugin->create($jobsite);
    $this->assertInstanceOf(Jobsite::class, $result);
  }

  public function testItCanUpdateAJobsite() {
    $authPlugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('SSOAuthentication');
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Jobsite');

    // Need to set ID on the jobsite...
    // That means we need a junk
    // job site to update
    $sendData = json_decode($this->jobData);
    $sendData->id = '214';
    $sendData->address2 = '#333';
    $jobsite = Jobsite::import($sendData);

    $result = $plugin->update($jobsite);
    $this->assertInstanceOf(Jobsite::class, $result);
  }

}
