<?php

namespace Drupal\ur_api_dataservice\tests\Unit;

use Drupal\KernelTests\KernelTestBase;
use Drupal\ur_api_dataservice\Plugins\ContractPlugin;
use Drupal\ur_api_dataservice\Plugins\SSOAuthenticationPlugin;

class ContractPluginTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['ur_api_dataservice', 'ur_admin', 'ur_utils'];

  /**
   * setUp method for this test.
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests the plugin can be instantiated.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function testItCanConstruct() {
    $plugin = new ContractPlugin(FALSE);
    $this->assertInstanceOf(ContractPlugin::class, $plugin);
  }

  /**
   * Tests the plugin can retrieve transactions of type On Contract
   * NOTE:  This plugin can only use the index verb. All other
   *        transaction functions (insert, update) are done
   *        using the base RentalTransactionPlugin class
   *
   * @group live
   */
  public function testItCanGetContracts() {
    $authPlugin = new SSOAuthenticationPlugin();
    $authPlugin->authenticate('ur.vml.7@gmail.com');

    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Contract');

    $result = $plugin->index('1408185');

    $test = (array) $result[0];

    $this->assertArrayHasKey('accountId', $test);
  }

}
