<?php

namespace Drupal\ur_api_dataservice\Exceptions;

/**
 * Class ParameterTypeException.
 *
 * @TODO: Validate we still need this
 *
 * @package Drupal\ur_api_dataservice\Exceptions
 */
class ParameterTypeException extends BaseException {

}
