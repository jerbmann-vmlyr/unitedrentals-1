<?php

namespace Drupal\ur_api_dataservice\Exceptions;

/**
 * Class DataException.
 *
 * The basic exceptions for the DAL Appliance.
 *
 * @package Drupal\ur_api_dataservice\Exceptions
 */
class AdapterException extends BaseException {

  /**
   * AdapterException constructor.
   *
   * @param string $message
   * @param int $code
   * @param \Exception|null $previous
   */
  public function __construct($message = '', $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }

}
