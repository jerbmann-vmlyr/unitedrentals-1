<?php

namespace Drupal\ur_api_dataservice\Exceptions;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class DalException.
 *
 * An exception that occurred while making a request to the DAL.
 *
 * @package Drupal\ur_api_dataservice\Exceptions
 */
class DalException extends BaseException {

  /**
   * The Request object.
   *
   * @var \GuzzleHttp\Psr7\Request
   */
  private $request;

  /**
   * The Response object.
   *
   * @var \GuzzleHttp\Psr7\Response
   */
  private $response;

  /**
   * DalException constructor.
   *
   * @param string $message
   * @param int $code
   * @param \GuzzleHttp\Psr7\Request|null $request
   * @param \GuzzleHttp\Psr7\Response|null $response
   * @param \Exception|null $previous
   */
  public function __construct($message = '', $code = 0, Request $request = NULL, Response $response = NULL, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);

    $this->request = $request;
    $this->response = $response;
  }

  /**
   * Override BaseException buildResponse method.
   *
   * @return array
   *   Array of exception data to return as response.
   */
  public function buildResponse() {
    $data = parent::buildResponse();

    if ($this->request) {
      $request_scheme = $this->request->getUri()->getScheme();
      $request_host = $this->request->getUri()->getHost();
      $request_path = $this->request->getUri()->getPath();
      $request_query = $this->request->getUri()->getQuery();
      $request_url = $request_scheme . '://' . $request_host . $request_path . '?' . $request_query;

      $data['debug']['request'] = [
        'method' => $this->request->getMethod(),
        'attempted' => $request_url,
        'body' => json_decode($this->request->getBody()),
        'headers' => $this->request->getHeaders(),
      ];
    }

    if ($this->response) {
      $data['debug']['response'] = [
        'code' => $this->response->getStatusCode(),
        'response_body' => json_decode($this->response->getBody()),
      ];
    }

    return $data;
  }

}
