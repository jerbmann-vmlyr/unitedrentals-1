<?php

namespace Drupal\ur_api_dataservice\Exceptions;

/**
 * Class ResponseParseException.
 *
 * Exceptions when unable to parse response.
 *
 * @package Drupal\ur_api_dataservice\Exceptions
 */
class ResponseParseException extends BaseException {

  /**
   * ResponseParseException constructor.
   *
   * @param string $message
   * @param int $code
   * @param \Exception|null $previous
   */
  public function __construct($message = '', $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }

}
