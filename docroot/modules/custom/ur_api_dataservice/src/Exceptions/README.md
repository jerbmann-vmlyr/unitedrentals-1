## Exceptions
___

### BaseException


All Exceptions share the `BaseException` class. When creating any new types of exceptions they **MUST** extend BaseException. This exception type is **VITAL** for exception handling. The `BaseException` handles building a error response for the front-end as well as it is the class our exception EventSubscriber is looking for to handle. See `ur_api_dataservice/src/EventSubscriber/UrAPIExcetionSubscriber.php`. 
