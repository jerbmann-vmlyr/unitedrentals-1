<?php

namespace Drupal\ur_api_dataservice\Exceptions;

/**
 * Class MissingRequiredParametersException.
 *
 * @package Drupal\ur_api_dataservice\Exceptions
 */
class MissingRequiredParametersException extends BaseException {

}
