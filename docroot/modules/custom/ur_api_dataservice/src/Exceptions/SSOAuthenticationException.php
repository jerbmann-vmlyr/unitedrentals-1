<?php

namespace Drupal\ur_api_dataservice\Exceptions;

/**
 * Class SSOAuthenticationException.
 *
 * @package Drupal\ur_api_dataservice\Exceptions
 */
class SSOAuthenticationException extends BaseException {

  /**
   * SSOAuthenticationException constructor.
   *
   * @param string $message
   * @param int $code
   * @param \Exception|null $previous
   */
  public function __construct($message = '', $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }

}
