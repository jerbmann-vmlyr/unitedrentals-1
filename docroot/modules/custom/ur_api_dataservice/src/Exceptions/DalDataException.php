<?php

namespace Drupal\ur_api_dataservice\Exceptions;

/**
 * Class DalDataException.
 *
 * The exception to be used when an error occurs when interfacing with the DAL.
 *
 * @package Drupal\ur_api_dataservice\Exceptions
 */
class DalDataException extends BaseException {

  /**
   * DalDataException constructor.
   *
   * @param string $message
   * @param int $code
   * @param \Exception|null $previous
   */
  public function __construct($message = '', $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }

}
