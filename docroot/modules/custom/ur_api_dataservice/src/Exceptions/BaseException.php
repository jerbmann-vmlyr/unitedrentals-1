<?php

namespace Drupal\ur_api_dataservice\Exceptions;

/**
 * Class BaseException.
 *
 * The Base Exception class all API Exceptions will be based from.
 *
 * @package Drupal\ur_api_dataservice\Exceptions
 */
class BaseException extends \Exception {

  /**
   * This handles returning an exception response contract for front-end.
   *
   * This method can and should be overridden to provide a more custom response
   * per Exception type we have.
   *
   * @see \Drupal\ur_api_dataservice\Exceptions\DalException
   *
   * @return array
   *   The error response.
   */
  public function buildResponse() {
    $trace = $this->getTrace();
    $source = $trace[0];

    $data = [
      'error' => TRUE,
      'message' => $this->getMessage(),
      'code' => $this->getCode(),
      'data' => [],
      'debug' => [
        'source' => $source['class'] . '->' . $source['function'] ,
        'time' => time(),
        'line' => $this->getLine(),
      ],
    ];

    return $data;
  }

}
