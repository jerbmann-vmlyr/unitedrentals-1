<?php

namespace Drupal\ur_api_dataservice\Traits;

trait TransformsXML {

  /**
   * Parses an xml response into an associative array
   *
   * @param $response
   *
   * @return mixed
   */
  public function parseXmlResponse($response) {
    $body = simplexml_load_string($response);
    $json = json_encode($body);
    $array = json_decode($json, TRUE);
    return $array;
  }

  /**
   * Takes a SimpleXML response object and processes the attributes, turning
   * them into fields instead. This prevents weirdness when importing the data.
   *
   * This is a recursive function. So beware.
   *
   * @param mixed $entity
   * @return mixed
   */
  public function processXmlAttributes($entity) {
    $entityWasObject = false;

    if (is_object($entity)) {
      $entity = (array) $entity;
      $entityWasObject = true;
    }

    if (is_array($entity)) {
      $hasAttributes = false;
      $attributes = [];

      /**
       * Had to add this because there was some unexpected data bleed over that
       * I was unable to trace to the source. It caused attributes to appear
       * where they should not. This allows me to test before the loop starts
       * and prevent the attributes from being built or tacked on if they
       * somehow bleed back in.
       */
      if (!empty($entity['@attributes'])) {
        $hasAttributes = true;
      }

      foreach ($entity AS $index => $value) {
        $wasObject = false;

        if (is_object($value)) {
          $value = (array) $value;
          $wasObject = true;
        }

        if ($hasAttributes && $index == '@attributes') {
          foreach ($value AS $childIndex => $childValue) {
            $attributes[$childIndex] = $childValue;
          }

          unset($entity[$index], $childIndex, $childValue);
        }
        // Make sure we are not looking at an int or string or other such thing
        else if (is_array($value)) {
          $result = $this->processXmlAttributes($value);

          if ($wasObject) {
            $result = (object) $result;
          }

          // Replace the index value with the processed values.
          $entity[$index] = $result;
        }
      }

      // Make sure we don't add to the array until it is done with the loop
      if ($hasAttributes) {
        $entity = array_merge($entity, $attributes);
      }
    }

    if ($entityWasObject) {
      return (object) $entity;
    }

    return $entity;
  }
}
