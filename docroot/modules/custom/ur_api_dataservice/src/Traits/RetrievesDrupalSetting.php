<?php

namespace Drupal\ur_api_dataservice\Traits;

use Symfony\Component\Yaml\Yaml;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;

/**
 * Trait RetrievesDrupalSetting
 *
 * @package Drupal\ur_api_dataservice\Traits
 *
 * @TODO: REMOVE THIS CLASS after refactoring it's dependents
 */
trait RetrievesDrupalSetting {
  public $parsedDrupalConfig;

  /**
   * Retrieves configuration variable.
   *
   * Attempts retrieving configuration from Drupal first, then by directly
   * parsing the YAML file itself.
   *
   * @param $varName
   * @return mixed
   * @throws \Exception
   */
  public static function getDrupalConfig($varName) {
    return ApiContainer::getParameter($varName);
  }

  /**
   * @return mixed
   */
  public function loadDrupalConfigFile() {
    if (empty($this->parsedDrupalConfig)) {
      // Load the default config.
      $path = __DIR__ . '/../../config/install/ur_api_dataservice.config.yml';

      // @TODO: Drupal config should be able to handle this.  load config.prod.yml on prod.

      // $path = __DIR__ . '/../../config/install/config.prod.yml';.

      $this->parsedDrupalConfig = Yaml::parse(file_get_contents($path));
    }

    return $this->parsedDrupalConfig;
  }

  /**
   * Get setting from the config file.
   *
   * @param $name
   *
   * @return array|mixed|null
   *
   * @throws \Symfony\Component\Yaml\Exception\ParseException
   */
  public function getFromDrupalConfig($name) {
    $config = $this->loadDrupalConfigFile();
    $parts = explode('.', $name);

    if (count($parts) == 1) {
      return isset($config[$name]) ? $config[$name] : NULL;
    }

    $keyExists = NULL;
    $value = $this->getDrupalNestedValue($config, $parts, $keyExists);

    return $keyExists ? $value : NULL;
  }

  /**
   * Digs in for a nested value.
   *
   * @param array $reference
   * @param array $parents
   * @param null|bool $keyExists
   *
   * @return array|mixed|null
   */
  public function getDrupalNestedValue(array &$reference, array $parents, &$keyExists = NULL) {
    foreach ($parents as $parent) {
      if (is_array($reference) && array_key_exists($parent, $reference)) {
        $reference = &$reference[$parent];
      }
      else {
        $keyExists = FALSE;
        return NULL;
      }
    }

    $keyExists = TRUE;
    return $reference;
  }

}
