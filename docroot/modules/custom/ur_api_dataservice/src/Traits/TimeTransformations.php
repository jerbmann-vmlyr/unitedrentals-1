<?php

namespace Drupal\ur_api_dataservice\Traits;

trait TimeTransformations {

  public function happeningNow($fromDateString, $toDateString) {
    $from = strtotime($fromDateString);
    $to = strtotime($toDateString);
    $rounded = NULL;

    if ($from && $to) {
      $diff = $from - $to;
      $rounded = round($diff / (60 * 60 * 24));
    }

    return $rounded;
  }

  public function happensInThePast($dateString) {
    $now = time();
    $d = strtotime($dateString);
    $diff = $d < $now;

    return $diff;
  }

  public function happensWithinThreeDays($dateString) {
    $diff = $this->daysFromNow($dateString);

    return $diff >= 0 && $diff < 3;
  }

  public function happensTodayOrInThePast($dateString) {
    $now = time();
    $d = strtotime($dateString);
    $diff = $now >= $d;

    return $diff;
  }

  public function happensTodayOrInTheFuture($dateString) {
    $now = time();
    $d = strtotime($dateString);
    $diff = $now <= $d;

    return $diff;
  }

  public function daysFromNow($dateString) {
    $now = new \DateTime();
    $d = new \DateTime($dateString);
    $diff = $now->diff($d)->format('%R%a');
    $diff = intval($diff);
    return $diff;
  }

}
