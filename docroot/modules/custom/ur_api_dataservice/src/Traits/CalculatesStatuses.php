<?php

namespace Drupal\ur_api_dataservice\Traits;

/**
 * Trait CalculateStatuses
 *
 * @package Drupal\ur_api_dataservice\Traits
 *
 * A piece of equipment on rent may have multiple statuses
 * assigned to it, depending on its position in the flow.
 * Thus, status is an array containing all assigned
 * statuses, not a single var.
 *
 * Possible Statuses:
 *  AdvancedPickup - Medium - type 6 *
 *  AtJobsite - Medium - n/a *
 *  Exchanged - Medium - n/a *
 *  ExchangeInProgress - Medium - n/a *
 *  ExchangeRequested - Medium - n/a *
 *  Idle - High - n/a *
 *  InLeniency - Medium - type 1 *
 *  OffRent - Low - type 4 *
 *  OnRent - Low - type 1 *
 *  Overdue - High - type 1 *
 *  Overtime - High - n/a *
 *  PendingDue - Medium - type 1 *
 *  PickupCanceled - High - n/a *
 *  PickupRequested - Medium - type 6 *
 *  Quote - Low - type 2 *
 *  ReservedForDelivery - Medium - type 3 *
 *  ReservedForBranchPickup - Medium - type 3 *
 *  ServiceInProgress - Medium - n/a *
 *  ServiceRequested - High - n/a *
 */
trait CalculatesStatuses {

  use TimeTransformations;

  public function setStatus($item) {
    /**
     * Some statuses are only available on specific transaction types.
     * We can exclude looking up some statuses using that criteria.
     */
    $statuses = [];
    switch ($item->type) {
      case '1':
        if ($this->happensInThePast($item->returnDateTime)) {
          $statuses[] = 'Overdue';
        }
        if ($this->happensWithinThreeDays($item->returnDateTime)) {
          $statuses[] = 'PendingDue';
        }
        $statuses[] = 'OnRent';
        if ($this->happeningNow($item->leniencyStartDate, $item->leniencyEndDate)) {
          $statuses[] = 'InLeniency';
        }
        break;
      case '2':
        $statuses[] = 'Quote';
        break;
      case '3':
        if ($item->urDeliver) {
          $statuses[] = 'ReservedForDelivery';
        }
        if (!$item->urDeliver) {
          $statuses[] = 'ReservedForBranchPickup';
        }
        break;
      case '4':
        $statuses[] = 'OffRent';
        break;
      case '6':
        if (strtolower($item->pickupType) == 'scheduled') {
          $statuses[] = 'PickupRequested';
        }
        if (strtolower($item->pickupType) == 'advanced') {
          $statuses[] = 'AdvancedPickup';
        }
        if ($item['pickupType'] != 'scheduled') {
          if ($this->happensInThePast($item->returnDateTime)) {
            $statuses[] = 'Overdue';
          }
          if ($this->happensWithinThreeDays($item->returnDateTime)) {
            $statuses[] = 'PendingDue';
          }
          $statuses[] = 'OnRent';
        }
        if ($this->happeningNow($item->leniencyStartDate, $item->leniencyEndDate)) {
          $statuses[] = 'InLeniency';
        }
        break;
    }
    return $statuses;
  }

}
