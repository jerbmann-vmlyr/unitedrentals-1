<?php

namespace Drupal\ur_api_dataservice\Traits;

/**
 * Class StringUtilities
 * A set of utilities for string manipulation not found in native PHP
 *
 * @package Drupal\ur_api_dataservice\Traits
 */
trait StringUtilities {

  /**
   * Changes a string from camel case to snake case
   *
   * @param $input
   * @return string
   */
  public function to_snake_case($input) {
    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
    $ret = $matches[0];

    foreach ($ret as &$match) {
      $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
    }

    return implode('_', $ret);
  }

  /**
   * Changes a string from snake case to camel case
   *
   * @param $input
   * @return string
   */
  public function to_camel_case($input) {
    $input = str_replace('_', '', ucwords($input, '_'));

    return lcfirst($input);
  }

}
