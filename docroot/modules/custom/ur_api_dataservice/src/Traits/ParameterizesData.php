<?php

namespace Drupal\ur_api_dataservice\Traits;

/**
 * Trait ParameterizesData
 *
 * @package Drupal\ur_api_dataservice\Traits
 */
trait ParameterizesData {

  /**
   * Converts values to acceptable API values.
   *
   * @param array|object $data
   *   The incorrectly formatted data we want to submit to the API.
   *
   * @return array
   *   The correctly formatted data we will submit to the API.
   */
  public function parameterize($data) {
    if (!isset($this->parameters)) {
      return $data;
    }

    if (\is_object($data)) {
      unset($data->dataService);
      $data = (array) $data;
    }

    // Handle field renames.
    foreach ($this->parameters as $key => $params) {
      if (isset($params['renames']) && !empty($params['renames'])) {
        $data[$this->parameters[$key]['renames']] = $data[$key];
        unset($data[$key]);
      }
    }

    foreach ($data as $key => $value) {
      // First check to see whether the item needs to be processed.
      if (!array_key_exists($key, $this->parameters)) {
        continue;
      }

      // Process generic crosswalks.
      if (is_array($this->parameters[$key]['imports'])) {
        $data[$key] = $this->parameterize_generic($data[$key], $this->parameters[$key]['imports']);
        continue;
      }

      switch ($this->parameters[$key]['imports']) {
        case 'TF':
          $data[$key] = $this->parameterize_boolean($data[$key]);
          break;

        case '10':
          $data[$key] = $this->parameterize_01_TF_boolean($data[$key]);
          break;

        case 'YN':
          $data[$key] = $this->parameterize_YN_boolean($data[$key]);
          break;

        default:
          break;
      }
    }

    return $data;
  }

  /**
   * @param $data
   * @param $rules
   * @return false|int|string
   */
  private function parameterize_generic($data, $rules) {
    // Find the import rule that matches.
    if (!in_array($data, $rules)) {
      return $data;
    }

    return array_search($data, $rules, TRUE);
  }

  /**
   * Helper method for converting boolean value to string representation.
   *
   * @param bool $data
   *   The boolean we want converted to string version of boolean.
   *
   * @return string
   *   " TRUE" if data is boolean  TRUE, "FALSE" otherwise.
   */
  private function parameterize_boolean($data) {
    return $data ? ' TRUE' : 'FALSE';
  }

  /**
   * Helper method for converting boolean value to "0"/"1" representation.
   *
   * @param bool $data
   *   The boolean we want to convert to "0"/"1" representation.
   *
   * @return string
   *   "0" if boolean is TRUE, "1" otherwise.
   */
  private function parameterize_01_TF_boolean($data) {
    return $data ? '0' : '1';
  }

  /**
   * Helper method for converting boolean values to "Y"/"N" representation.
   *
   * @param bool $data
   *   The boolean we want converted to "Y"/"N" representation.
   *
   * @return string
   *   "Y" if $data was boolean TRUE, "N" otherwise.
   */
  private function parameterize_YN_boolean($data) {
    return $data ? 'Y' : 'N';
  }

}
