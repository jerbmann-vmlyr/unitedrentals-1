<?php

namespace Drupal\ur_api_dataservice\Traits;

/**
 * Trait ArrayUtilities
 * A collection of added functionality for array manipulation and parsing.
 *
 * @package Drupal\ur_api_dataservice\Traits
 */
trait ArrayUtilities {

  /**
   * Equivalent of javascript's array.some()
   *
   * @param array $array
   * @param callable $fn
   *
   * @return bool
   */
  public function array_some(array $array, callable $fn) {
    foreach ($array as $value) {
      if ($fn($value)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Equivalent of javascript's array.every()
   *
   * @param array $array
   * @param callable $fn
   *
   * @return bool
   */
  public function array_every(array $array, callable $fn) {
    foreach ($array as $value) {
      if (!$fn($value)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * "Shuffle" a member of an array to a new index
   *
   * @param array $array
   * @param $fromIndex
   * @param $toIndex
   *
   * @return array
   */
  public function array_shuffle(array $array, $fromIndex, $toIndex) {
    $p1 = array_splice($array, $fromIndex, 1);
    $p2 = array_splice($array, 0, $toIndex);
    $array = array_merge($p2, $p1, $array);

    return $array;
  }

  /**
   * Determine if an array is an associative array, or indexed array
   *
   * @param array $array
   *
   * @return bool
   */
  public function array_is_assc(array $array) {
    if (array() === $array) {
      return FALSE;
    }

    return array_keys($array) !== range(0, count($array) - 1);
  }

}
