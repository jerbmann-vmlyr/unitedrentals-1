<?php

namespace Drupal\ur_api_dataservice\Traits;

/**
 * Trait ReadsParameters
 *
 * @package Drupal\ur_api_dataservice\Traits
 */
trait ReadsParameters {

  /**
   * Returns list of required parameter keys.
   *
   * @param string $action
   *   The CRUD action we are filtering required parameters for.
   *
   * @return array
   *   Array keys of parameters that are possible for the Plugin.
   */
  public function getRequiredParameters($action) {
    if (empty($this->parameters)) {
      return [];
    }

    $action = strtolower($action);

    $required = array_filter($this->parameters, function ($a) use ($action) {
      return in_array($action, $a['required']);
    });

    return array_keys($required);
  }

  /**
   * Returns list of possible parameter keys.
   *
   * @param string $action
   *   The CRUD action we are filtering possible parameters for.
   *
   * @return array
   *   Array keys of parameters that are possible for the Plugin.
   */
  public function getPossibleParameters($action) {
    if (empty($this->parameters)) {
      return [];
    }

    $action = strtolower($action);

    $possible = array_filter($this->parameters, function ($a) use ($action) {
      return in_array($action, $a['possible']);
    });

    return array_keys($possible);
  }

  /**
   * Builds array of parameter "type" from parameter property of Plugin.
   *
   * @return array
   *   parameter name => parameter type (i.e. ["accountId" => "int", "accountName" => "string"]).
   */
  public function getParameterTypes() {
    if (empty($this->parameters)) {
      return [];
    }

    $types = [];
    $parameters = array_keys($this->parameters);

    foreach ($parameters as $param) {
      $types[$param] = $this->parameters[$param]['type'];
    }

    return $types;
  }

}
