<?php

namespace Drupal\ur_api_dataservice\Traits;

trait GetsParameters {

  /**
   * Utility method to return the static parameters
   *
   * @return array
   */
  protected static function getParams() {
    return static::$parameters;
  }

}
