<?php

namespace Drupal\ur_api_dataservice\Traits;
use Drupal\ur_api_dataservice\Plugins\SSOAuthenticationPlugin;
use Exception;

/**
 * Class BaseAuthenticationTrait
 *
 * @package Drupal\ur_api_dataservice\Traits
 */
trait AuthenticationTrait {

  /**
   * Returns the common auth configured token.
   *
   * @return array|mixed|null
   *   The token from drupal config.
   * @throws Exception
   */
  public function getAuthenticationWORSToken() {
    return SSOAuthenticationPlugin::getAuthenticationWORSToken();
  }

  /**
   * Retrieves user authentication token for API.
   *
   * @param null $email
   *   Optional email to use for retrieving token. Otherwise default to current
   *   logged in user.
   *
   * @return bool|string
   *   The user's authentication token, FALSE if not found.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getAuthenticationUserToken($email = NULL) {
    if (!empty($_SESSION['ur_dal_user_token'])) {
      return 'TOKEN:' . $_SESSION['ur_dal_user_token'];
    }

    if ($email === NULL) {
      $email = $this->getUserEmail();
    }

    if (!empty($email)) {
      try {
        $plugin = new SSOAuthenticationPlugin();
        $plugin->authenticate($email);
        return 'TOKEN:' . $_SESSION['ur_dal_user_token'];
      }
      catch (Exception $e) {
        // TODO: This should return something more meaningful.
        return FALSE;
      }
    }

    return FALSE;
  }

  /**
   * Retrieves the currently logged in Drupal user's email.
   *
   * After retrieving the email this sets the email into session for easy
   * retrieval later.
   *
   * @return null|string
   *   The logged in user's email, or NULL if none found.
   */
  private function getUserEmail() {
    $email = \Drupal::currentUser()->getEmail();

    if ($email) {
      // @TODO: Look to set this using Drupal's private tempstore
      $_SESSION['ur_dal_user_email'] = $email;
    }

    return $email;
  }

}
