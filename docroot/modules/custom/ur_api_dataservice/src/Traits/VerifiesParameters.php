<?php

namespace Drupal\ur_api_dataservice\Traits;

use Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException;
use Drupal\ur_api_dataservice\Exceptions\ParameterTypeException;

/**
 * Trait VerifiesParameters
 *
 * @package Drupal\ur_api_dataservice\Traits
 */
trait VerifiesParameters {

  /**
   * Validates that the Plugin was given all the required parameters.
   *
   * @param array $data
   *   List of parameters that are required for the Plugin method.
   *
   * @param string $action
   *   The CRUD action we are checking parameters against.
   *
   * @return bool
   *   True if all parameters exist in $data.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   */
  public function verifyRequiredParameters($data, $action):bool {
    $required = $this->getRequiredParameters($action);

    foreach ($required as $key) {
      if (!isset($data[$key]) || $data[$key] === '') {
        throw new MissingRequiredParametersException("$key is a required parameter", 400);
      }
    }

    return TRUE;
  }

  /**
   * Attempts to ensure all parameters of the Plugin are of valid types.
   *
   * Runs is_numeric, is_bool, is_string, is_array, or is_float based on Plugin
   * parameter types.
   *
   * @param array $data
   *   Key => value pairs of submitted data, keyed by parameter name.
   *
   * @return bool
   *   True if parameters pass type check.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   */
  public function verifyParameterTypes(array $data):bool {
    $types = $this->getParameterTypes();

    foreach ($data as $param => $value) {
      if (isset($types[$param])) {
        $type = $types[$param];
        $function = 'is_' . $types[$param];

        if (function_exists($function)) {
          $value = $function($value);

          if ($value == FALSE) {
            throw new ParameterTypeException("Unable to validate parameter '$param' of type $type.", 400);
          }
        }
        else {
          throw new ParameterTypeException("Unable to evaluate parameter '$param' of type $type. Function $function does not exist. Are the parameter types set up correctly?", 500);
        }
      }
    }

    return TRUE;
  }

  /**
   * Assembles key/value pair array of parameters to send to the DAL.
   *
   * @param $data
   *
   * @param $action
   *
   * @return array|null
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   */
  public function assembleParameters($data, $action):?array {
    $hasAllRequiredParams = $this->verifyRequiredParameters($data, $action);

    // After ensuring we have our required parameters, assemble all required
    // and possibles into single array
    if ($hasAllRequiredParams) {
      $required = $this->getRequiredParameters($action);
      $possibles = $this->getPossibleParameters($action);
      $params = array_merge($required, $possibles);

      // Verify we have all the correct types for our parameters.
      $verified = $this->verifyParameterTypes($data);

      if ($verified) {
        // Now build key/value array of params.
        $out = [];

        foreach ($params as $key) {
          if (isset($data[$key]) && !empty($data[$key])) {
            $out[$key] = $data[$key];
          }
        }

        return $out;
      }
    }

    return NULL;
  }

}
