<?php

namespace Drupal\ur_api_dataservice\Traits;

/**
 * Class GetsUserId
 *
 * @package Drupal\ur_api_dataservice\Traits
 */
trait GetsUserId {

  /**
   * Gets the user's ID.
   *
   * @return int
   */
  public function getUserId() {
    return \Drupal::currentUser()->id();
  }

  /**
   * Gets the user's GUID.
   *
   * @return bool|string
   */
  public function getGuid() {
    if (!empty($_SESSION['ur_guid'])) {
      return $_SESSION['ur_guid'];
    }

    // Try {
    //      // @todo convert this to a plugin ie AdAccountPlugin
    //      $adAccountObj = new AdAccountsEndpoint($this);
    //      $guid = $adAccountObj->readGuid();
    //      $_SESSION['ur_guid'] = $guid;
    //
    //      return $guid;
    //    }
    //    catch (DalException $e) {
    //      // Do nothing
    //    }.

    return FALSE;
  }

}
