<?php

namespace Drupal\ur_api_dataservice\Traits;

/**
 * Class NormalizesData
 *
 * @package Drupal\ur_api_dataservice\Traits
 */
trait NormalizesData {

  /**
   * Converts data going to and returning from API into sensible values.
   *
   * API doesn't return the most friendly values at times. This helper method is
   * for turning odd data points into more useful, developer friendly data
   * points.
   *
   * @param object $input
   *   The array of data to normalize
   *
   * @return array|mixed
   */
  public function normalize($input) {
    if (!isset($this->parameters)) {
      return $input;
    }

    if (is_object($input)) {
      $data = (array) $input;
    }

    // Handle field renames.
    foreach ($this->parameters as $key => $params) {
      if (isset($params['renames'])) {
        $data[$key] = $data[$this->parameters[$key]['renames']];
      }
    }

    // Do the conversion stuff.
    foreach ($data as $key => $value) {
      // First check to see whether the item needs to be processed.
      if (!array_key_exists($key, $this->parameters)) {
        continue;
      }

      // Process generic crosswalks.
      if (is_array($this->parameters[$key]['imports']) && !empty($this->parameters[$key]['imports'])) {
        $data[$key] = $this->normalize_generic($data[$key], $this->parameters[$key]['imports']);
        continue;
      }

      // Run through default normalizers.

      switch ($this->parameters[$key]['imports']) {
        case 'TF':
          $data[$key] = $this->normalize_boolean($data[$key]);
          break;

        case '01':
          $data[$key] = $this->normalize_01_TF_boolean($data[$key]);
          break;

        case '10':
          $data[$key] = $this->normalize_10_TF_boolean($data[$key]);
          break;

        case 'YN':
          $data[$key] = $this->normalize_YN_boolean($data[$key]);
          break;

        default:
          break;
      }

    }

    foreach ($data as $key => $val) {
      $input->{$key} = $val;
    }

    return $input;
  }

  /**
   * @param $data
   * @param $rules
   * @return mixed
   */
  private function normalize_generic($data, $rules) {
    return isset($rules[$data]) ? $rules[$data] : $data;
  }

  /**
   * Helper method to convert "truth-y" values to actual boolean value.
   *
   * @param $data
   *   The data to check for "truth-y-ness"
   * @return bool
   *   True if the $data equates to a "truth-y" value, FALSE otherwise.
   */
  private function normalize_boolean($data) {
    $truthyValues = [
      ' TRUE',
      'True',
      'TRUE',
      TRUE,
    ];

    return in_array($data, $truthyValues, TRUE);
  }

  /**
   * Helper method to convert 0/1 to  TRUE/FALSE values.
   *
   * @param mixed $data
   *   Data to check if it's  TRUE.
   *
   * @return bool
   *   True if $data was 0 or "0", FALSE otherwise
   */
  private function normalize_01_TF_boolean($data) {
    // Yes - this seems backwards but it is not ¯\_(ツ)_/¯.
    return ($data === '0' || $data === 0);
  }

  /**
   * Helper method to convert 1/0 to  TRUE/FALSE values.
   *
   * @param mixed $data
   *   Data to check if it's  TRUE.
   *
   * @return bool
   *   True if $data was 1 or "1",
   */
  private function normalize_10_TF_boolean($data) {
    return ($data === '1' || $data === 1);
  }

  /**
   * Helper method to convert "Y" to  TRUE/FALSE value.
   *
   * @param string $data
   *   The string to check if was meant to be  TRUE.
   *
   * @return bool
   *   True if "y" or "Y", FALSE otherwise.
   */
  private function normalize_YN_boolean($data) {
    return (strtoupper($data) === 'Y');
  }

}
