<?php

namespace Drupal\ur_api_dataservice;

use Drupal\ur_frontend_api_v2\Service\FEDataService;

/**
 * Class DataService.
 */
class DataService extends FEDataService {

  /**
   * Get a string description of the dataservice.
   */
  public function describe() {
    return 'Production Data';
  }

}
