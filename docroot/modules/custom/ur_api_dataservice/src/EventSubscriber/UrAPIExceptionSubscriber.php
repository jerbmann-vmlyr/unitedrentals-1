<?php

namespace Drupal\ur_api_dataservice\EventSubscriber;

use Drupal\ur_admin\InsightsLogger;
use Drupal\ur_api_dataservice\Exceptions\BaseException;
use Drupal\ur_api_dataservice\Exceptions\DalException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class UrAPIExceptionSubscriber.
 *
 * The event subscriber that is responsible for responding to exceptions.
 *
 * @package Drupal\ur_api_dataservice\EventSubscriber
 */
class UrAPIExceptionSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\ur_admin\InsightsLogger
   */
  protected $insightsLogger;

  /**
   * UrAPIExceptionSubscriber constructor.
   *
   * @param \Drupal\ur_admin\InsightsLogger $insights_logger
   */
  public function __construct(InsightsLogger $insights_logger) {
    $this->insightsLogger = $insights_logger;
  }

  /**
   * Handler responsible for returning JsonResponse from exception.
   *
   * This is the method that is called when any Kernel Exceptions occur. We will
   * not process anything if the exception is not of type BaseException.
   *
   * @see BaseException
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
   *   The exception event that was fired.
   */
  public function onException(GetResponseForExceptionEvent $event) {
    $exception = $event->getException();

    if ($exception instanceof BaseException) {
      // @TODO: Discuss letting the Plugin decide what to log
      // If it's a DAL Exception we want to log the message to Insights.
      // if ($exception instanceof DalException) {
        // $this->insightsLogger->logInsights(['message' => $exception->getMessage()]);
      // }

      $response = new JsonResponse($exception->buildResponse(), $exception->getCode());
      $event->setResponse($response);
    }
  }

  /**
   * {@inheritdoc} Here we subscribe to any KernalEvent Exceptions.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::EXCEPTION][] = ['onException', 60];
    return $events;
  }

}
