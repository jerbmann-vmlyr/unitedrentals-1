<?php

namespace Drupal\ur_api_dataservice\Providers\Cache\Database;

use Drupal\ur_api_dataservice\Interfaces\CacheCollectionInterface;
use Drupal\ur_api_dataservice\Providers\Cache\Database\DatabaseCacheItem;

use Symfony\Component\Serializer;

/**
 * Class DatabaseCacheCollection
 *
 * @package Drupal\ur_api_dataservice\Providers\Cache\Database
 */
class DatabaseCacheCollection implements CacheCollectionInterface {

  public $collectionKey;

  /**
   * Stores the data to the database
   *
   * @param $dataSet
   * @param $parameters
   * @param $cacheSettings
   *
   * @return mixed|void
   */
  public static function store($dataSet, $parameters, $cacheSettings) {
    foreach ($dataSet as $data) {
      DatabaseCacheItem::store($data, $parameters, $cacheSettings);
    }
  }

  /**
   * Queries the cache given parameters
   *
   * @param $parameters
   * @param $cacheSettings
   *
   * @return array|bool|mixed
   */
  public static function find($parameters, $cacheSettings) {
    $connection = \Drupal::database();
    $query = $connection->query('select * from {api_appliance_v2_cache} where user_id = :userid and cacheKey = :cacheKey and parameters = :parameters', [
      ':userid' => $cacheSettings['userId'],
      ':cacheKey' => $cacheSettings['cacheKey'],
      ':parameters' => $parameters,
    ]);

    $results = $query->fetchAll();

    if (count($results) <= 0) {
      return FALSE;
    }

    $output = [];

    foreach ($results as $result) {
      $data = (string) $result->data;
      $output[] = unserialize($data);
    }

    return $output;
  }

  /**
   * Invalidates cache
   *
   * @param $collectionKey
   * @param $cacheSettings
   */
  public static function invalidate($collectionKey, $cacheSettings) {
    $connection = \Drupal::database();
    $connection->delete('api_appliance_v2_cache', [
      'user_id' => $cacheSettings['userId'],
      'collectionKey' => $collectionKey,
    ]);
  }

}
