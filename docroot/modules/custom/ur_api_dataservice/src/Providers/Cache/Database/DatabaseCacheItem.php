<?php

namespace Drupal\ur_api_dataservice\Providers\Cache\Database;

use Drupal\Core\Database\Connection;
use Drupal\ur_api_dataservice\Traits\GetsUserId;
use Drupal\ur_api_dataservice\Interfaces\CacheItemInterface;
use Drupal\ur_api_dataservice\Providers\Cache\Database\DatabaseCacheCollection;

/**
 * Class DatabaseCacheItem
 *
 * @package Drupal\ur_api_dataservice\Providers\Cache\Database
 */
class DatabaseCacheItem implements CacheItemInterface {

  public $parameters;

  public $itemId;

  public $data;

  public $cacheSettings;

  /**
   * DatabaseCacheItem constructor.
   *
   * @param $itemId
   * @param $parameters
   * @param $data
   * @param $cacheSettings
   */
  public function __construct($itemId, $parameters, $data, $cacheSettings) {
    $this->parameters = $parameters;
    $this->itemId = $itemId;
    $this->data = $data;
  }

  /**
   * Stores the cache to the database.
   *
   * @param $data
   * @param $parameters
   * @param $cacheSettings
   *
   * @return mixed|void
   *
   * @throws \Exception
   */
  public static function store($data, $parameters, $cacheSettings) {
    $connection = \Drupal::database();
    $created = time();
    $itemid = self::getKeyFromFields($data, $cacheSettings['idFields']);

    $query = $connection->insert('api_appliance_v2_cache')->fields([
      'cacheKey' => $cacheSettings['cacheKey'],
      'user_id' => $cacheSettings['userId'],
      'itemId' => $itemid,
      'data' => serialize($data),
      'parameters' => $parameters,
      'created_at' => $created,
      'expires_at' => $created + $cacheSettings['ttl'],
    ])->execute();
  }

  /**
   * Retrieves a cached item from the cache.
   *
   * @param $itemId
   * @param $cacheSettings
   *
   * @return bool|mixed
   */
  public static function get($itemId, $cacheSettings) {
    $connection = \Drupal::database();

    $query = $connection->query("SELECT * FROM {api_appliance_v2_cache} WHERE user_id = :userid AND cacheKey = :cacheKey AND itemId = :itemId", [
      ':userid' => $cacheSettings['userId'],
      ':cacheKey' => $cacheSettings['cacheKey'],
      ':itemId' => $itemId,
    ]);

    $results = $query->fetchAll();

    if (empty($results)) {
      return FALSE;
    }

    $cacheItem = array_shift($results);
    return unserialize($cacheItem->data);
  }

  /**
   * Updates cached items to reflect new data.
   *
   * @param $data
   * @param $cacheSettings
   * @throws \Exception
   */
  public static function patchAll($data, $cacheSettings) {
    $connection = \Drupal::database();

    // Get the item id.
    $itemId = self::getKeyFromFields($data, $cacheSettings['idFields']);

    // Find all of the items in the cache with the same item id.
    $params = [
      ':cacheKey' => $cacheSettings['cacheKey'],
      ':itemId' => $itemId,
    ];

    $query = $connection->query('SELECT * FROM {api_appliance_v2_cache} WHERE cacheKey = :cacheKey AND itemId = :itemId', $params);
    $results = $query->fetchAll();

    // If we don't have any results, there's no patching that needs to be done.
    if (count($results) <= 0) {
      return;
    }

    $ids = [];

    foreach ($results as $result) {
      $ids[] = (int) $result->id;
    }

    // Recreate all of the records with the updated data.
    foreach ($results as $result) {
      $tempCacheSettings = $cacheSettings;
      $tempCacheSettings['userId'] = $result->user_id;

      self::store($data, $result->parameters, $cacheSettings);
    }

    // Invalidate all of the old records.
    $connection->delete('api_appliance_v2_cache')
      ->condition('id', $ids, 'IN')
      ->execute();
  }

  /**
   * Invalidates a single item in cache.
   *
   * @param $data
   * @param $cacheSettings
   */
  public static function invalidate($data, $cacheSettings) {
    $connection = \Drupal::database();
    $itemId = self::getKeyFromFields($data, $cacheSettings['idFields']);

    $connection->delete('api_appliance_v2_cache')
      ->condition('user_id', $cacheSettings['userId'])
      ->condition('cacheKey', $cacheSettings['cacheKey'])
      ->condition('itemId', $itemId)
      ->execute();
  }

  /**
   * Invalidates all items of a cache collection.
   *
   * @param $data
   * @param $cacheSettings
   */
  public static function invalidateAll($data, $cacheSettings) {
    $connection = \Drupal::database();
    $itemId = self::getKeyFromFields($data, $cacheSettings['idFields']);

    $connection->delete('api_appliance_v2_cache')
      ->condition('cacheKey', $cacheSettings['cacheKey'])
      ->condition('itemId', $itemId)
      ->execute();
  }

  /**
   * Retrieves key values from fields in cache.
   *
   * @param $data
   * @param $fields
   * @return string
   */
  public static function getKeyFromFields($data, $fields) {
    $keyValues = [];

    foreach ($fields as $field) {
      if (is_array($data)) {
        $keyValues[] = $data[$field];
      }
      else {
        $keyValues[] = $data->$field;
      }
    }

    return implode('|', $keyValues);
  }

}
