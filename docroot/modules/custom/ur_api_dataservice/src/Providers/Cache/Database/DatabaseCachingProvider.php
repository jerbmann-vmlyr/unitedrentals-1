<?php

namespace Drupal\ur_api_dataservice\Providers\Cache\Database;

use Drupal\ur_api_dataservice\Providers\Cache\Database\DatabaseCacheItem;
use Drupal\ur_api_dataservice\Providers\Cache\Database\DatabaseCacheCollection;

/**
 * Class DatabaseCachingProvider.
 *
 * @package Drupal\ur_api_dataservice\Providers\Cache\Database
 */
class DatabaseCachingProvider {

  /**
   * Caches a single item.
   *
   * @param $data
   * @param $parameters
   * @param $cacheSettings
   *
   * @throws \Exception
   */
  public function cacheItem($data, $parameters, $cacheSettings) {
    DatabaseCacheItem::store($data, $parameters, $cacheSettings);
  }

  /**
   * Caches a collection of data.
   *
   * @param $data
   * @param $parameters
   * @param $cacheSettings
   */
  public function cacheCollection($data, $parameters, $cacheSettings) {
    DatabaseCacheCollection::store($data, $parameters, $cacheSettings);
  }

  /**
   * Retrieves a single item from cache.
   *
   * @param $itemId
   * @param $cacheSettings
   * @return bool|mixed
   */
  public function retrieveItem($itemId, $cacheSettings) {
    return DatabaseCacheItem::get($itemId, $cacheSettings);
  }

  /**
   * Retrieves a collection from cache.
   *
   * @param $parameters
   * @param $cacheSettings
   * @return array|bool|mixed
   */
  public function retrieveCollection($parameters, $cacheSettings) {
    return DatabaseCacheCollection::find($parameters, $cacheSettings);
  }

  /**
   * Invalidates a collection's cache.
   *
   * @param $parameters
   * @param $cacheSettings
   */
  public function invalidateCollection($parameters, $cacheSettings) {
    DatabaseCacheCollection::invalidate($parameters, $cacheSettings);
  }

  /**
   * Invalidates a items cache.
   *
   * @param $item
   * @param $cacheSettings
   */
  public function invalidateItem($item, $cacheSettings) {
    DatabaseCacheItem::invalidate($item, $cacheSettings);
  }

  /**
   * Invalidates all of a particular cache.
   *
   * @param $item
   * @param $cacheSettings
   */
  public function invalidateAll($item, $cacheSettings) {
    DatabaseCacheItem::invalidateAll($item, $cacheSettings);
  }

  /**
   * Updates all items of a particular cache.
   * 
   * @param $item
   * @param $cacheSettings
   * @throws \Exception
   */
  public function patchAll($item, $cacheSettings) {
    DatabaseCacheItem::patchAll($item, $cacheSettings);
  }

}
