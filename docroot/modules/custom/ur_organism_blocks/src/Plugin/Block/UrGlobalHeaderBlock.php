<?php

namespace Drupal\ur_organism_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'UrGlobalHeaderBlock' block.
 *
 * @Block(
 *  id = "ur_block_global_header",
 *  admin_label = @Translation("UR global header block"),
 * )
 */
class UrGlobalHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'ur_global_header',
      'customer_service_number' => \Drupal::config('ur_admin.settings')->get('customer_service_number'),
    ];

  }

}
