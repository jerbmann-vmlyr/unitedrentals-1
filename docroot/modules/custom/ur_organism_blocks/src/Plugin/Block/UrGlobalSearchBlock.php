<?php

namespace Drupal\ur_organism_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'UrGlobalSearchBlock' block.
 *
 * @Block(
 *  id = "ur_block_global_search",
 *  admin_label = @Translation("UR global search block"),
 * )
 */
class UrGlobalSearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'ur_global_search',
    ];
  }

}
