<?php

namespace Drupal\ur_organism_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'UrGlobalFooterBlock' block.
 *
 * @Block(
 *  id = "ur_block_global_footer",
 *  admin_label = @Translation("UR global footer block"),
 * )
 */
class UrGlobalFooterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
      return array(
          '#theme' => 'ur_global_footer',
      );
  }

}
