<?php

namespace Drupal\ur_organism_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'UrMobileMenuBlock' block.
 *
 * @Block(
 *  id = "ur_block_mobile_menu",
 *  admin_label = @Translation("UR mobile menu block"),
 * )
 */
class UrMobileMenuBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'ur_mobile_menu',
      'customer_service_number' => \Drupal::config('ur_admin.settings')->get('customer_service_number'),
      'french_translation_url' => \Drupal::config('ur_admin.settings')->get('french_translation_url'),
      'spanish_translation_url' => \Drupal::config('ur_admin.settings')->get('spanish_translation_url'),
    ];
  }

}
