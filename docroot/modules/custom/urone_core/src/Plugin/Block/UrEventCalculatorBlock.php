<?php

namespace Drupal\urone_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a 'UrEventCalculator' block.
 *
 * @Block(
 *  id = "ur_event_calculator",
 *  admin_label = @Translation("UR Event Calculator Block"),
 * )
 */
class UrEventCalculatorBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $url = Url::fromUri($this->configuration['lead_form']);

    return [
      '#theme' => 'ur_event_calculator',
      '#lead_form_link' => $url,
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Creates a generic configuration form for all block types. Individual
   * block plugins can add elements to this form by overriding
   * BlockBase::blockForm(). Most block plugins should not override this
   * method unless they need to alter the generic form elements.
   *
   * @see \Drupal\Core\Block\BlockBase::blockForm()
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['lead_form'] = [
      '#type' => 'url',
      '#title' => $this->t('CTA Link'),
      '#url' => !empty($this->configuration['lead_form'])
        ? Url::fromUri($this->configuration['lead_form'])
        : '',
      '#default_value' => !empty($this->configuration['lead_form'])
        ? $this->configuration['lead_form']
        : ''
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * Most block plugins should not override this method. To add submission
   * handling for a specific block type, override BlockBase::blockSubmit().
   *
   * @see \Drupal\Core\Block\BlockBase::blockSubmit()
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['lead_form'] = $form_state->getValue('lead_form');
    parent::submitConfigurationForm($form, $form_state);
  }
}
