<?php

namespace Drupal\global_search\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Item\Item;
use Drupal\search_api\Query\ResultSetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\ur_appliance\Provider\Placeable;
use Drupal\ur_frontend_api\Controller\UrCache;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Class GlobalSearchController.
 *
 * @package Drupal\global_search\Controller
 */
class GlobalSearchController extends ControllerBase {
  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  public $request;

  /**
   * Declaration of Drupal EntityTypeManager for dependency injection.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  public $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request stack.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Drupal EntityTypeManager.
   */
  public function __construct(RequestStack $request, EntityTypeManager $entity_type_manager) {
    $this->request = $request;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Helper method to return a cached search result or request a new result.
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse
   *   Search result formatted as Cacheable Json.
   */
  public function getGlobalSearch() {
    $query = $this->request->getCurrentRequest()->get('search');
    $safe_keyword = Xss::filter($query);
    $response = new CacheableJsonResponse();

    if (empty($safe_keyword)) {
      \Drupal::logger('global_search')->notice($query . ' caused the XSS filter to return a null value');

      // Set cache to 0 so empty response doesn't get cached.
      $data = [
        'result' => '',
        '#cache' => [
          'max-age' => 0,
          'contexts' => [
            'url',
          ],
        ],
      ];

      $response->addCacheableDependency(CacheableMetadata::createFromRenderArray($data));
      $response->setData($data);
      $response->headers->set('Cache-Control', 'max-age=0 public');

      return $response;
    }

    // Removing Drupal caching for now, letting Varnish handle it.
    $result = $this->performSearch($safe_keyword);
    $data = [
      'result' => $result,
    ];

    // Cache response for a week based on the url.
    $data['#cache'] = [
      'max-age' => UrCache::WEEK,
      'contexts' => [
        'url',
      ],
    ];

    $response->addCacheableDependency(CacheableMetadata::createFromRenderArray($data));

    // Make sure we don't encode our html tags.
    $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    $response->setData($data);

    return $response;
  }

  /**
   * Handles front-end queries for global search content.
   *
   * @param string $safe_keyword
   *   Sanitized search input string.
   *
   * @return array
   *   Array of search results.
   */
  public function performSearch($safe_keyword) {
    /** @var \Drupal\search_api\Entity\Index $index */
    $index = Index::load($this->config('ur_admin.settings')->get('global_search_index'));
    $search_destination = '/search?search=' . urlencode($safe_keyword);
    // $taxnomy_search_destination = '/taxonomy-search?tid=';.
    $search_results = [];

    // Perform each search query per result group set.
    $item_results = $this->getItemResults($safe_keyword, $index);
    $term_results = $this->getTermResults($safe_keyword, $index);
    $type_results = $this->getTypeResults($index, $term_results, $item_results);
    $category_results = $this->getCategoryResults($index, $term_results, $type_results);
    // $type_string = $this->getResultTids($type_results);
    // $category_string = $this->getResultTids($category_results);
    $other_results = $this->getOtherResults($safe_keyword, $index);

    if ($item_results->getResultCount() > 0) {
      $item_data = [
        'id' => 1,
        "display_name" => "Equipment",
        "total" => $item_results->getResultCount(),
        "group_results_page" => $search_destination,
        // "group_results_page" => $search_destination . '&type=item',.
        "results" => $this->formatSolrResult($item_results, $safe_keyword),
      ];

      $search_results[] = $item_data;
    }

    if ($type_results->getResultCount() > 0) {
      $type_data = [
        'id' => 2,
        "display_name" => "Equipment Type",
        "total" => count($type_results->getResultItems()),
        "group_results_page" => $search_destination,
        // "group_results_page" => $taxnomy_search_destination . $type_string,.
        "results" => $this->formatSolrResult($type_results, $safe_keyword),
      ];

      $search_results[] = $type_data;
    }

    if ($category_results->getResultCount() > 0) {
      $category_data = [
        'id' => 3,
        "display_name" => "Equipment Category",
        "total" => count($category_results->getResultItems()),
        "group_results_page" => $search_destination,
        // "group_results_page" => $taxnomy_search_destination . $category_string,.
        "results" => $this->formatSolrResult($category_results, $safe_keyword),
      ];

      $search_results[] = $category_data;
    }

    if ($other_results->getResultCount() > 0) {
      $article_data = [
        'id' => 5,
        "display_name" => "Found on unitedrentals.com",
        "total" => $other_results->getResultCount(),
        "group_results_page" => $search_destination,
        // "group_results_page" => $search_destination . '&type=article+events+news+page+project_uptime_pages+service_sub_pages+solution_sub_pages+press_releases+faq+landing_page',.
        "results" => $this->formatSolrResult($other_results, $safe_keyword),
      ];

      $search_results[] = $article_data;
    }

    return $search_results;
  }

  /**
   * Parses a Search API ResultSet and returns a simplified array for the front end.
   *
   * @param \Drupal\search_api\Query\ResultSetInterface $results
   *   Results set returned from Solr search.
   * @param string $safe_keyword
   *   Sanitized search input string.
   *
   * @return array
   *   Returns array of normalized Solr results.
   */
  protected function formatSolrResult(ResultSetInterface $results, $safe_keyword) {
    $max_group_results = $this->config('ur_admin.settings')->get('global_search_max_results_group');
    $formatted_results = [];

    if ($results->getResultCount() > 0) {
      /** @var \Drupal\search_api\Item\ItemInterface[] $items */
      $items = array_slice($results->getResultItems(), 0, $max_group_results);

      foreach ($items as $id => $item) {
        // $score = $item->getScore();
        $original_item = $item->getOriginalObject();

        if ($item->getDatasourceId() != 'entity:taxonomy_term') {
          $itemType = $item->getFields(true)['type']->getValues();
        }

        // Custom link creation for NEWS type for Global Search
        if ($itemType[0] == 'news') {
          $title = $original_item->get('title')->getValue();
          $highlighted_title = $this->applyHighlight($title[0]['value'], $safe_keyword);
          $alias = $item->getOriginalObject(true)->getValue()->get('field_news_external_link')->uri;
        }

        // Handle taxonomy term entities differently than content entities.
        elseif ($item->getDatasourceId() == 'entity:taxonomy_term') {
          $title = $original_item->get('name')->getValue();
          $highlighted_title = $this->applyHighlight($title[0]['value'], $safe_keyword);
          $tid = $original_item->get('tid')->getValue();

          if ($original_item->get('vid')->target_id == 'categories') {
            $path = \Drupal::service('path.alias_manager')->getAliasByPath('/taxonomy/term/' . $tid[0]['value']);
            $alias = preg_replace('/_catalog$/', '', $path);
          }
          else {
            $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/taxonomy/term/' . $tid[0]['value']);
          }
        }

        else {
          $title = $original_item->get('title')->getValue();
          $highlighted_title = $this->applyHighlight($title[0]['value'], $safe_keyword);
          $nid = $original_item->get('nid')->getValue();
          $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $nid[0]['value']);
        }

        $result = [
          'name' => $highlighted_title,
          'url' => $alias,
        ];

        $formatted_results[] = $result;
      }
    }

    return $formatted_results;
  }

  /**
   * Applies custom highlighting so that each searched keyword is wrapped with an HTML tag configured in the ur_admin global search settings.
   *
   * @param string $input_string
   *   String to add highlighting to.
   * @param string $match_string
   *   String to match for highlighting.
   *
   * @return string
   *   Input string with any matched phrases wrapped in configured highlight wrapper.
   */
  protected function applyHighlight($input_string, $match_string) {
    $highlight_prefix = $this->config('ur_admin.settings')->get('global_search_highlight_prefix');
    $highlight_suffix = $this->config('ur_admin.settings')->get('global_search_highlight_suffix');

    return preg_replace('/' . str_replace("+", "|", $match_string) . '/i', $highlight_prefix . '$0' . $highlight_suffix, $input_string);
  }

  /**
   * Makes programmatic searches against the Solr database.
   *
   * @param array $conditions
   *   The conditions for which to conduct our search.
   * @param string $safe_keyword
   *   Sanitized search input string.
   * @param \Drupal\search_api\Entity\Index $index
   *   The index we're searching against.
   * @param array $fulltext_fields
   *   An array of field machine names from Search API Index.
   *
   * @return \Drupal\search_api\Query\ResultSetInterface
   *   Result set returned from Solr search.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Drupal Search API exceptions.
   */
  protected function querySolrIndex(array $conditions, $safe_keyword, Index $index, array $fulltext_fields) {
    $max_results = $this->config('ur_admin.settings')->get('global_search_max_results_query');
    // Set up query limit and offset.
    $query = $index->query([
      'limit' => $max_results,
      'offset' => 0,
    ]);

    // Leaving the other parse mode's commented out for testing purposes.
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')
      ->createInstance('phrase');
    $parse_mode->setConjunction('OR');
    $query->setParseMode($parse_mode);

    // Set fulltext search keywords and fields.
    $query->keys($safe_keyword);
    $query->setFulltextFields($fulltext_fields);

    // Make sure we're sorting with highest relevance "score" first.
    $query->sort('search_api_relevance', 'DESC');

    foreach ($conditions as $key => $value) {
      // Create OR condition groups for multi-value conditions.
      if (is_array($value)) {
        $group = $query->createConditionGroup('OR');
        foreach ($value as $val) {
          $group->addCondition($key, $val);
        }

        $query->addConditionGroup($group);
      }
      else {
        $query->addCondition($key, $value);
      }
    }

    // Execute query.
    $results = $query->execute();

    return $results;
  }

  /**
   * The "Equipment" group in the search results.
   *
   * @param string $safe_keyword
   *   Sanitized search input string.
   * @param \Drupal\search_api\Entity\Index $index
   *   Search index to perform the search on.
   *
   * @return \Drupal\search_api\Query\ResultSetInterface
   *   Results are displayed as Product Name. (title)
   *   Hard matches based on Product Name. (title)
   *   Soft matches based on Cat Class. (field_item_cat_class_code)
   */
  protected function getItemResults($safe_keyword, Index $index) {
    $conditions = ['status' => 1, 'type' => 'item'];
    // @TODO: Determine rest of fields to index for items
    // These are the fields set up in Search API Index for Content
    $fulltext_fields = ['title', 'field_item_cat_class_code', 'field_search_synonyms'];
    // $fulltext_fields = ['title', 'field_item_cat_class_code', 'name_1'];.
    $item_results = $this->querySolrIndex($conditions, $safe_keyword, $index, $fulltext_fields);

    return $item_results;
  }

  /**
   * Returns any hits on the Categories taxonomy term. Covers Type and Category result groups.
   *
   * @param string $safe_keyword
   *   Sanitized search input string.
   * @param \Drupal\search_api\Entity\Index $index
   *   Search index to perform the search on.
   *
   * @return \Drupal\search_api\Item\ItemInterface[]|ResultSetInterface
   *   Result set containing any taxonomy term matches.
   */
  protected function getTermResults($safe_keyword, Index $index) {
    $conditions = ['vid' => 'categories'];
    $fulltext_fields = ['name'];

    // Returns all hard matches for category terms.
    $term_results = $this->querySolrIndex($conditions, $safe_keyword, $index, $fulltext_fields);

    return $term_results;
  }

  /**
   * Iterates through Category term results and builds a ResultSet of only second level (Type) terms. Takes several ResultSetInterface objects as arguments to make processing more efficient.
   *
   * @param \Drupal\search_api\Entity\Index $index
   *   Search index to perform the search on.
   * @param \Drupal\search_api\Query\ResultSetInterface $term_results
   *   Result set object containing any first tier taxonomy term results.
   * @param \Drupal\search_api\Query\ResultSetInterface $item_results
   *   Result set object containing any Equipment Item results.
   *
   * @return \Drupal\search_api\Item\ItemInterface[]|ResultSetInterface
   *   Result set containing any second tier taxonomy term matches.
   */
  protected function getTypeResults(Index $index, ResultSetInterface $term_results, ResultSetInterface $item_results) {
    $category_terms = $this->retrieveCachedCategoryTerms();
    $type_results = clone($term_results);
    $result_set = $type_results->getResultItems();
    $soft_results = [];

    foreach ($result_set as $idx => $type) {
      $tid = $type->getOriginalObject()->get('tid')->value;
      if ($category_terms[$tid]->depth > 1) {
        // Add term matches to soft results where depth > 1.
        $soft_results[] = $tid;
        unset($result_set[$idx]);
      }
      elseif ($category_terms[$tid]->depth == 0) {
        unset($result_set[$idx]);
      }
    }

    // Add Item term ids to soft results array.
    foreach ($item_results as $idx => $item) {
      $original = $item->getOriginalObject();
      $tid = $original->get('field_item_category')->target_id;
      if (empty($soft_results[$tid])) {
        $soft_results[] = $tid;
      }
    }

    // Add depth 1 parents of soft results to result list.
    foreach ($soft_results as $tid) {
      $parents = $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadAllParents($tid);
      $parents = array_keys($parents);

      foreach ($parents as $parent_tid) {
        if ($category_terms[$parent_tid]->depth == 1) {
          $custom_item = new Item($index, 'entity:taxonomy_term/' . $parent_tid . ':en', NULL);
          $result_set['entity:taxonomy_term/' . $parent_tid . ':en'] = $custom_item;
        }
      }
    }

    $type_results->setResultItems($result_set);
    $type_results->setResultCount(count($result_set));

    return $type_results;
  }

  /**
   * The "Equipment Category" group in the search results. These are 1st level taxonomy terms from the "Categories" vocabulary.
   *
   * @param \Drupal\search_api\Entity\Index $index
   *   Search index to perform the search on.
   * @param \Drupal\search_api\Query\ResultSetInterface $term_results
   *   Result set object containing any first tier taxonomy term results.
   * @param \Drupal\search_api\Query\ResultSetInterface $type_results
   *   Result set object containing any second tier taxonomy term, or "type" results.
   *
   * @return \Drupal\search_api\Query\ResultSetInterface
   *   Results are displayed as Equipment Category.
   *   Hard matches based on Equipment Category.
   *   Soft matches based on Equipment Type, Product Name, or Cat Class.
   */
  protected function getCategoryResults(Index $index, ResultSetInterface $term_results, ResultSetInterface $type_results) {
    $category_terms = $this->retrieveCachedCategoryTerms();
    $category_results = clone($term_results);
    $result_set = $category_results->getResultItems();

    foreach ($result_set as $idx => $type) {
      $tid = $type->getOriginalObject()->get('tid')->value;
      if ($category_terms[$tid]->depth > 0) {
        unset($result_set[$idx]);
      }
    }

    // Loop through our Type Results for soft matches. Item results have already been merged.
    foreach ($type_results as $type) {
      $tid = $type->getOriginalObject()->get('tid')->value;
      $parents = $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadAllParents($tid);
      $parents = array_keys($parents);

      $custom_item = new Item($index, 'entity:taxonomy_term/' . $parents[1] . ':en', NULL);
      $result_set['entity:taxonomy_term/' . $parents[1] . ':en'] = $custom_item;
    }

    $category_results->setResultItems($result_set);
    $category_results->setResultCount(count($result_set));

    return $category_results;
  }

  /**
   * The "other" group of the content that should appear in the search, listed as "Found on UnitedRentals.com".
   *
   * @param string $safe_keyword
   *   Sanitized search input string.
   * @param \Drupal\search_api\Entity\Index $index
   *   Search index to perform the search on.
   *
   * @return \Drupal\search_api\Query\ResultSetInterface
   *   Results are displayed as Page Title.
   *   Hard matches based on Page Title.
   *   Soft matches based on Page description (i.e. body fields).
   */
  protected function getOtherResults($safe_keyword, Index $index) {
    $conditions = [
      'status' => 1,
      'type' => [
        'events',
        'news',
        'page',
        'project_uptime_pages',
        'service_sub_pages',
        'solution_sub_pages',
        'press_releases',
        'faq',
        'landing_page',
      ],
    ];
    $fulltext_fields = ['title', 'body', 'field_summary', 'field_search_synonyms'];
    $other_results = $this->querySolrIndex($conditions, $safe_keyword, $index, $fulltext_fields);

    return $other_results;
  }

  /**
   * Helper method that will cache the first and second levels of the Categories vocabulary.
   *
   * @return array|mixed
   *   Taxonomy term tree array.
   */
  protected function retrieveCachedCategoryTerms() {
    $term_data = &drupal_static(__FUNCTION__);

    if (!isset($term_data)) {
      if ($cache = \Drupal::cache()->get('global_search_category_data')) {
        $term_data = $cache->data;
      }
      else {
        $terms = $this->entityTypeManager->getStorage('taxonomy_term')
          ->loadTree('categories', 0, 4, TRUE);

        $term_data = [];
        foreach ($terms as $term) {
          $term_data[$term->get('tid')->value] = $term;
        }

        \Drupal::cache()->set('global_search_category_data', $term_data);
      }
    }

    return $term_data;
  }

  /**
   * Helper method that will gather the tid of each taxonomy term and concatenate string to use as query in search url.
   *
   * @param object $type_results
   *  Result set containing taxonomy term objects
   *
   * @return string
   *  String of taxonomy tids to be added to query in search url
   */
  /*protected function getResultTids($type_results) {
  $tids = '';

  foreach($type_results as $type) {
  $tid = $type->getOriginalObject()->get('tid')->value;
  $tids .= $tid . '+';
  }

  $trim_tids = substr($tids, 0, -1);

  return $trim_tids;

  }*/

}
