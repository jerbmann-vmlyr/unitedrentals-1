<?php

namespace Drupal\ur_migrations\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Transform id hash of user to UID for saving content.
 *
 * @MigrateProcessPlugin(
 *   id = "get_author"
 * )
 *
 * To do get the User ID from the hash do the following:
 *
 * @code
 * field_text:
 *   plugin: get_author
 *   source: uid/data/id
 * @endcode
 */
class GetAuthor extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $query = \Drupal::entityTypeManager()->getStorage('user')->getQuery()->condition('uuid', $value)->execute();

    if (!empty($query)) {
      return reset($query);
    }

    return NULL;
  }

}
