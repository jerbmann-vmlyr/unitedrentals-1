<?php

namespace Drupal\ur_migrations\Plugin\migrate\process;

use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\node\Entity\Node;

/**
 * Skips row import if uuid and nid already exist.
 *
 * @MigrateProcessPlugin(
 *   id = "skip_on_duplicate"
 * )
 *
 * To iplement do the following
 *
 * @code
 * field_text:
 *   plugin: skip_on_duplicate
 *   source: uuid
 * @endcode
 */
class SkipOnDuplicate extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $type = $row->getSourceProperty('json_entity_type');
    if ($type) {
      if (substr($type, 0, strlen('node')) === 'node') {
        $type = 'node';
      }
      elseif (substr($type, 0, strlen('paragraph')) === 'paragraph') {
        $type = 'paragraph';
      }
      elseif (substr($type, 0, strlen('taxonomy_term')) === 'taxonomy_term') {
        $type = 'taxonomy_term';
      }
    }

    if (!$type) {
      $type = 'node';
    }

    $query = \Drupal::entityTypeManager()->getStorage($type)->getQuery()->condition('uuid', $value)->execute();

    // The entity exists, skip this row.
    if (!empty($query)) {
      throw new MigrateSkipRowException();
    }

    return $value;
  }

}
