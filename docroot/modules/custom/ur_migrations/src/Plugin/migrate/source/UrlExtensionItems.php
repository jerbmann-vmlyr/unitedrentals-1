<?php

namespace Drupal\ur_migrations\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\SourcePluginExtension;
use Drupal\migrate\Row;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Source plugin to provide a list of source URLs.
 *
 * This plugin allows the user to specify file or stream-based content (where a
 * URL, including potentially a local filepath, points to a file containing data
 * to be migrated). The source plugin itself simply manages the (potentially
 * multiple) source URLs, and works with Http and File fetcher plugins for
 * retrieving the data.
 *
 * Available configuration keys:
 * - urls: A single url, a list of urls, or a callback defined as a nested item
 *   keyed by callback (required)
 * - data_fetcher_plugin: id of valid DataFetcherPluginInterface - eg http, file
 * - data_parser_plugin: id of valid DataParserPluginInterface - eg json, xml
 *
 * Examples:
 *
 * This will import articles from a single URL endpoint.
 * @code
 * source:
 *   plugin: url
 *   urls: https://example.com/jsonapi/node/article
 *   data_fetcher_plugin: http
 *   data_parser_plugin: json
 *   item_selector: data
 * @endcode
 *
 * This will call the function my_module_get_urls_to_import() which should
 * return an array of URLs or files corresponding to all data sources to import.
 * @code
 * source:
 *   plugin: url_extension_items
 *   urls:
 *      callback: my_module_get_urls_to_import
 *   data_fetcher_plugin: http
 *   data_parser_plugin: json
 *   item_selector: data
 * @endcode
 *
 * The 'callback' would normally live in a .module file, and is passed the
 * current migration context.  For mode detail of use:
 * @see https://www.drupal.org/node/3040427
 *
 * @MigrateSource(
 *   id = "url_extension_items",
 *   source_module = "ur_migrations",
 * )
 */
class UrlExtensionItems extends SourcePluginExtension {

  /**
   * The source URLs to retrieve.
   *
   * @var array
   */
  protected $sourceUrls = [];

  /**
   * The data parser plugin.
   *
   * @var \Drupal\migrate_plus\DataParserPluginInterface
   */
  protected $dataParserPlugin;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    if (!is_array($configuration['urls'])) {
      $configuration['urls'] = [$configuration['urls']];
    }
    // Support a callback to return arrays of URLs.
    elseif (!empty($configuration['urls']['callback']) && is_callable($configuration['urls']['callback'])) {
      $configuration['urls'] = $configuration['urls']['callback']($migration);
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $this->sourceUrls = $configuration['urls'];
  }

  /**
   * Return a string representing the source URLs.
   *
   * @return string
   *   Comma-separated list of URLs being imported.
   */
  public function __toString() {
    // This could cause a problem when using a lot of urls, may need to hash.
    $urls = implode(', ', $this->sourceUrls);
    return $urls;
  }

  /**
   * Returns the initialized data parser plugin.
   *
   * @return \Drupal\migrate_plus\DataParserPluginInterface
   *   The data parser plugin.
   */
  public function getDataParserPlugin() {
    if (!isset($this->dataParserPlugin)) {
      $this->dataParserPlugin = \Drupal::service('plugin.manager.migrate_plus.data_parser')->createInstance($this->configuration['data_parser_plugin'], $this->configuration);
    }
    return $this->dataParserPlugin;
  }

  /**
   * Creates and returns a filtered Iterator over the documents.
   *
   * @return \Iterator
   *   An iterator over the documents providing source rows that match the
   *   configured item_selector.
   */
  protected function initializeIterator() {
    return $this->getDataParserPlugin();
  }

  /**
   * Returns source URLs.
   *
   * @return array
   *   The list of source Urls.
   */
  public function getSourceUrls() {
    return $this->sourceUrls;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    if ($row->getSourceProperty('json_entity_type') !== 'node--item') {
      return FALSE;
    }

    // Set the base url for all the API calls we need to make.
    $json_url = "https://stage.unitedrentals.com/jsonapi/";

    // Get all the data from our inital call we will use to prepare our row.
    $author_data = $row->getSourceProperty('field_author');
    $categories_data = $row->getSourceProperty('field_item_category');
    $equipment_data = $row->getSourceProperty('field_item_equipment');
    $images_data = $row->getSourceProperty('field_images_unlimited');
    $categories_tids = [];
    $equipment_ids = [];
    $author_id = '1';
    $image_rows = [];

    if (!empty($categories_data['data'])) {
      $load_bundle = 'categories';
      $load_properties = [
        'vid' => $load_bundle,
      ];
      $categories_tids = self::makeCallGetEntities(
        'taxonomy_term',
        $load_bundle,
        $load_properties,
        $categories_data
      );
    }

    if (!empty($equipment_data['data'])) {
      $load_bundle = 'equipment';
      $load_properties = [
        'type' => $load_bundle,
      ];
      $equipment_ids = self::makeCallGetEntities(
        'node',
        $load_bundle,
        $load_properties,
        $equipment_data
      );
    }

    // The lookup for user is done differently than above, so we handle it here.
    if (!empty($author_data['data'])) {
      $url = $json_url . "/user/user?filter[id][condition][path]=id&filter[id][condition][operator]=IN";
      $url .= '&filter[id][condition][value][]=' . $author_data['data']['id'];
      $json = file_get_contents($url);
      $obj = json_decode($json);
      if (isset($obj->data) && isset($obj->data->attributes)) {
        $mail = $obj->data->attributes->mail;
        $users = \Drupal::entityTypeManager()->getStorage('user')
          ->loadByProperties(['mail' => $mail]);
        $user = reset($users);
        if ($user) {
          $author_id = $user->id();
        }
      }
    }

    $publish_state_change = $row->getSourceProperty('field_publish_state_change');
    if (!empty($publish_state_change)) {
      $publish_state_change = strtotime($publish_state_change);
      $row->setSourceProperty('field_publish_state_change', $publish_state_change);
    }
    $row->setSourceProperty('categories_tids', $categories_tids);
    $row->setSourceProperty('equipment_ids', $equipment_ids);
    $row->setSourceProperty('author_id', intval($author_id));

    // Set images.
    if (!empty($images_data['data'])) {
      foreach ($images_data['data'] as $image_row) {
        $load_properties = [
          'uuid' => $image_row['id'],
        ];
        $entities = \Drupal::entityManager()
          ->getStorage('file')
          ->loadByProperties($load_properties);
        if (!empty($entities)) {
          $file = array_pop($entities);
          if (!empty($file)) {
            $image_rows[] = [
              'target_id' => $file->id(),
              'alt' => $image_row['meta']['alt'],
              'title' => $image_row['meta']['title'],
              'width' => $image_row['meta']['width'],
              'height' => $image_row['meta']['height'],
            ];
          }
        }
      }
    }
    $row->setSourceProperty('image_rows', $image_rows);

    return parent::prepareRow($row);
  }

  /**
   * Utility: find entity by properties.
   */
  public static function getIdByProperties($entity_type, array $load_properties) {
    // If properties include alias, convert to id.
    if (isset($load_properties['alias']) && !empty($load_properties['alias'])) {
      $alias_properties = ['alias' => $load_properties['alias']];
      $alias = \Drupal::service('path.alias_storage')->load($alias_properties);
      if (!empty($alias)) {
        if ($entity_type == 'node') {
          $load_properties['nid'] = str_replace('/node/', '', $alias['source']);
        }
        elseif ($entity_type == 'taxonomy_term') {
          $load_properties['tid'] = str_replace('/taxonomy/term/', '', $alias['source']);
        }
      }
      unset($load_properties['alias']);
    }

    // Now try to load the entity.
    $entity = NULL;
    $entities = \Drupal::entityManager()
      ->getStorage($entity_type)
      ->loadByProperties($load_properties);
    if (!empty($entities)) {
      $entity = reset($entities);
    }

    return !empty($entity) ? $entity->id() : 0;
  }

  /**
   * Utility: make jsonapi call and get entity ids.
   */
  public static function makeCallGetEntities($entity_type, $bundle, array $load_properties, array $data) {
    // Set the base url for all the API calls we need to make.
    $url = 'https://stage.unitedrentals.com/jsonapi/'
      . $entity_type . '/' . $bundle
      . '?filter[id][condition][path]=id&filter[id][condition][operator]=IN';
    
    $ids = [];
    foreach ($data['data'] as $datum) {
      $url .= '&filter[id][condition][value][]=' . $datum['id'];
    }
    $json = file_get_contents($url);
    $obj = json_decode($json);
    if (!empty($obj->data)) {
      foreach ($obj->data as $d) {
        if (isset($d->attributes->path->alias)) {
          $load_properties['alias'] = $d->attributes->path->alias;
          $entity_id = self::getIdByProperties($entity_type, $load_properties);
          if ($entity_id) {
            array_push($ids, $entity_id);
          }
        }
      }
    }

    return $ids;
  }

}
