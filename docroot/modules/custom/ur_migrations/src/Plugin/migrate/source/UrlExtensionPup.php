<?php

namespace Drupal\ur_migrations\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\SourcePluginExtension;
use Drupal\migrate\Row;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Source plugin to provide a list of source URLs.
 *
 * This plugin allows the user to specify file or stream-based content (where a
 * URL, including potentially a local filepath, points to a file containing data
 * to be migrated). The source plugin itself simply manages the (potentially
 * multiple) source URLs, and works with Http and File fetcher plugins for
 * retrieving the data.
 *
 * Available configuration keys:
 * - urls: A single url, a list of urls, or a callback defined as a nested item
 *   keyed by callback (required)
 * - data_fetcher_plugin: id of valid DataFetcherPluginInterface - eg http, file
 * - data_parser_plugin: id of valid DataParserPluginInterface - eg json, xml
 *
 * Examples:
 *
 * This will import articles from a single URL endpoint.
 * @code
 * source:
 *   plugin: url
 *   urls: https://example.com/jsonapi/node/article
 *   data_fetcher_plugin: http
 *   data_parser_plugin: json
 *   item_selector: data
 * @endcode
 *
 * This will call the function my_module_get_urls_to_import() which should
 * return an array of URLs or files corresponding to all data sources to import.
 * @code
 * source:
 *   plugin: url
 *   urls:
 *      callback: my_module_get_urls_to_import
 *   data_fetcher_plugin: http
 *   data_parser_plugin: json
 *   item_selector: data
 * @endcode
 *
 * The 'callback' would normally live in a .module file, and is passed the
 * current migration context.  For mode detail of use:
 * @see https://www.drupal.org/node/3040427
 *
 * @MigrateSource(
 *   id = "url_extension_pup",
 *   source_module = "ur_migrations",
 * )
 */
class UrlExtensionPup extends SourcePluginExtension {

  /**
   * The source URLs to retrieve.
   *
   * @var array
   */
  protected $sourceUrls = [];

  /**
   * The data parser plugin.
   *
   * @var \Drupal\migrate_plus\DataParserPluginInterface
   */
  protected $dataParserPlugin;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    if (!is_array($configuration['urls'])) {
      $configuration['urls'] = [$configuration['urls']];
    }
    // Support a callback to return arrays of URLs.
    elseif (!empty($configuration['urls']['callback']) && is_callable($configuration['urls']['callback'])) {
      $configuration['urls'] = $configuration['urls']['callback']($migration);
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $this->sourceUrls = $configuration['urls'];
  }

  /**
   * Return a string representing the source URLs.
   *
   * @return string
   *   Comma-separated list of URLs being imported.
   */
  public function __toString() {
    // This could cause a problem when using a lot of urls, may need to hash.
    $urls = implode(', ', $this->sourceUrls);
    return $urls;
  }

  /**
   * Returns the initialized data parser plugin.
   *
   * @return \Drupal\migrate_plus\DataParserPluginInterface
   *   The data parser plugin.
   */
  public function getDataParserPlugin() {
    if (!isset($this->dataParserPlugin)) {
      $this->dataParserPlugin = \Drupal::service('plugin.manager.migrate_plus.data_parser')->createInstance($this->configuration['data_parser_plugin'], $this->configuration);
    }
    return $this->dataParserPlugin;
  }

  /**
   * Creates and returns a filtered Iterator over the documents.
   *
   * @return \Iterator
   *   An iterator over the documents providing source rows that match the
   *   configured item_selector.
   */
  protected function initializeIterator() {
    return $this->getDataParserPlugin();
  }

  /**
   * Returns source URLs.
   *
   * @return array
   *   The list of source Urls.
   */
  public function getSourceUrls() {
    return $this->sourceUrls;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Set the base url for all the API calls we need to make.
    $json_url = "https://stage.unitedrentals.com/jsonapi/";

    // Get all the data from our inital call we will use to prepare our row.
    $author_data = $row->getSourceProperty('field_author');
    $business_units_data = $row->getSourceProperty('field_business_units');
    $categories_data = $row->getSourceProperty('field_categories');
    $pillars_data = $row->getSourceProperty('field_pillars');
    $tags_data = $row->getSourceProperty('field_tags');
    $sub_headline = $row->getSourceProperty('field_sub_headline');
    $body = $row->getSourceProperty('body');
    $paragraphs = $row->getSourceProperty('field_paragraph');

    // Multi value references set to empty.
    $tags_tids = [];
    $business_units_tids = [];
    $categories_tids = [];
    $pillars_tids = [];
    $paragraph_references = 0;

    // Single value references set to default.
    $author_id = '1';
    $sub_headline_value = '';
    $sub_headline_format = 'basic_html';
    $body_value = '';
    $body_format = 'basic_html';

    // For each term reference, if there is a reference, call a function to get the tid(s).
    if (!empty($tags_data['data'])) {
      $url = $json_url . 'taxonomy_term/tags?filter[id][condition][path]=id&filter[id][condition][operator]=IN';
      $tags_tids = self::makeCallGetTerms($url, $tags_data, 'tags');
    }

    if (!empty($business_units_data['data'])) {
      $url = $json_url . 'taxonomy_term/project_uptime_business_unit?filter[id][condition][path]=id&filter[id][condition][operator]=IN';
      $business_units_tids = self::makeCallGetTerms($url, $business_units_data, 'project_uptime_business_unit');
    }

    if (!empty($categories_data['data'])) {
      $url = $json_url . 'taxonomy_term/categories?filter[id][condition][path]=id&filter[id][condition][operator]=IN';
      $categories_tids = self::makeCallGetTerms($url, $categories_data, 'categories');
    }

    if (!empty($pillars_data['data'])) {
      $url = $json_url . 'taxonomy_term/project_uptime_pillars?filter[id][condition][path]=id&filter[id][condition][operator]=IN';
      $pillars_tids = self::makeCallGetTerms($url, $pillars_data, 'project_uptime_pillars');
    }

    // The lookup for user is done differently than above, so we handle it here.
    if (!empty($author_data['data'])) {
      $url = "https://stage.unitedrentals.com/jsonapi/user/user?filter[id][condition][path]=id&filter[id][condition][operator]=IN";
      $url .= '&filter[id][condition][value][]=' . $author_data['data']['id'];
      $json = file_get_contents($url);
      $obj = json_decode($json);
      if (isset($obj->data) && isset($obj->data->attributes)) {
        $mail = $obj->data->attributes->mail;
        $users = \Drupal::entityTypeManager()->getStorage('user')
          ->loadByProperties(['mail' => $mail]);
        $user = reset($users);
        if ($user) {
          $author_id = $user->id();
        }
      }
    }

    // Sub Headline, because Extract Plugin is terrible.
    if ($sub_headline) {
      $sub_headline_value = $sub_headline['value'];
      $sub_headline_format = $sub_headline['format'];
    }

    // Body, because Extract Plugin is terrible.
    if ($body) {
      $body_value = $body['value'];
      $body_format = $body['format'];
    }

    // Paragraphs.
    if ($paragraphs['data']) {
      $paragraph_references = [];
      foreach ($paragraphs['data'] as $d) {
        $query = \Drupal::entityTypeManager()
          ->getStorage('paragraph')
          ->getQuery()
          ->condition('uuid', $d['id'])
          ->execute();

        if (!empty($query)) {
          $paragraph = Paragraph::load(reset($query));
          $paragraph_references[] = [
            'target_id' => reset($query),
            'target_revision_id' => $paragraph->getRevisionId(),
          ];
        }
      }
    }

    // Just in case the queries were empty and the field was set to an empty array.
    if (is_array($paragraph_references) && empty($paragraph_references)) {
      $paragraph_references = 0;
    }

    // Set the source properties for all the references, empty if null.
    $row->setSourceProperty('tags_tids', $tags_tids);
    $row->setSourceProperty('business_units_tids', $business_units_tids);
    $row->setSourceProperty('categories_tids', $categories_tids);
    $row->setSourceProperty('pillars_tids', $pillars_tids);
    $row->setSourceProperty('author_id', intval($author_id));
    $row->setSourceProperty('sub_headline_value', $sub_headline_value);
    $row->setSourceProperty('sub_headline_format', $sub_headline_format);
    $row->setSourceProperty('body_value', $body_value);
    $row->setSourceProperty('body_format', $body_format);
    $row->setSourceProperty('paragraph_references', $paragraph_references);

    return parent::prepareRow($row);
  }

  /**
   * Utility: find term by name and vid.
   */
  public static function getTidByName($name = NULL, $vid = NULL) {
    $properties = [];
    if (!empty($name)) {
      $properties['name'] = $name;
    }
    if (!empty($vid)) {
      $properties['vid'] = $vid;
    }
    $terms = \Drupal::entityManager()->getStorage('taxonomy_term')->loadByProperties($properties);
    $term = reset($terms);

    return !empty($term) ? $term->id() : 0;
  }

  /**
   * Utility: make jsonapi call and get term ids.
   */
  public static function makeCallGetTerms($url, $data, $vid) {
    $ids = [];
    foreach ($data['data'] as $datum) {
      $url .= '&filter[id][condition][value][]=' . $datum['id'];
    }
    $json = file_get_contents($url);
    $obj = json_decode($json);
    if (!empty($obj->data)) {
      foreach ($obj->data as $d) {
        $term_id = self::getTidByName($d->attributes->name, $vid);
        if ($term_id) {
          array_push($ids, $term_id);
        }
      }
    }

    return $ids;
  }

}
