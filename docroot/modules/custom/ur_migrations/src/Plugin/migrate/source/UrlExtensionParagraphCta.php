<?php

namespace Drupal\ur_migrations\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\SourcePluginExtension;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
 * Source plugin to provide a list of source URLs.
 *
 * This plugin allows the user to specify file or stream-based content (where a
 * URL, including potentially a local filepath, points to a file containing data
 * to be migrated). The source plugin itself simply manages the (potentially
 * multiple) source URLs, and works with Http and File fetcher plugins for
 * retrieving the data.
 *
 * Available configuration keys:
 * - urls: A single url, a list of urls, or a callback defined as a nested item
 *   keyed by callback (required)
 * - data_fetcher_plugin: id of valid DataFetcherPluginInterface - eg http, file
 * - data_parser_plugin: id of valid DataParserPluginInterface - eg json, xml
 *
 * Examples:
 *
 * This will import articles from a single URL endpoint.
 * @code
 * source:
 *   plugin: url
 *   urls: https://example.com/jsonapi/node/article
 *   data_fetcher_plugin: http
 *   data_parser_plugin: json
 *   item_selector: data
 * @endcode
 *
 * This will call the function my_module_get_urls_to_import() which should
 * return an array of URLs or files corresponding to all data sources to import.
 * @code
 * source:
 *   plugin: url
 *   urls:
 *      callback: my_module_get_urls_to_import
 *   data_fetcher_plugin: http
 *   data_parser_plugin: json
 *   item_selector: data
 * @endcode
 *
 * The 'callback' would normally live in a .module file, and is passed the
 * current migration context.  For mode detail of use:
 * @see https://www.drupal.org/node/3040427
 *
 * @MigrateSource(
 *   id = "url_extension_paragraph_cta",
 *   source_module = "ur_migrations",
 * )
 */
class UrlExtensionParagraphCta extends SourcePluginExtension {

  /**
   * The source URLs to retrieve.
   *
   * @var array
   */
  protected $sourceUrls = [];

  /**
   * The data parser plugin.
   *
   * @var \Drupal\migrate_plus\DataParserPluginInterface
   */
  protected $dataParserPlugin;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    if (!is_array($configuration['urls'])) {
      $configuration['urls'] = [$configuration['urls']];
    }
    // Support a callback to return arrays of URLs.
    elseif (!empty($configuration['urls']['callback']) && is_callable($configuration['urls']['callback'])) {
      $configuration['urls'] = $configuration['urls']['callback']($migration);
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $this->sourceUrls = $configuration['urls'];
  }

  /**
   * Return a string representing the source URLs.
   *
   * @return string
   *   Comma-separated list of URLs being imported.
   */
  public function __toString() {
    // This could cause a problem when using a lot of urls, may need to hash.
    $urls = implode(', ', $this->sourceUrls);
    return $urls;
  }

  /**
   * Returns the initialized data parser plugin.
   *
   * @return \Drupal\migrate_plus\DataParserPluginInterface
   *   The data parser plugin.
   */
  public function getDataParserPlugin() {
    if (!isset($this->dataParserPlugin)) {
      $this->dataParserPlugin = \Drupal::service('plugin.manager.migrate_plus.data_parser')->createInstance($this->configuration['data_parser_plugin'], $this->configuration);
    }
    return $this->dataParserPlugin;
  }

  /**
   * Creates and returns a filtered Iterator over the documents.
   *
   * @return \Iterator
   *   An iterator over the documents providing source rows that match the
   *   configured item_selector.
   */
  protected function initializeIterator() {
    return $this->getDataParserPlugin();
  }

  /**
   * Returns source URLs.
   *
   * @return array
   *   The list of source Urls.
   */
  public function getSourceUrls() {
    return $this->sourceUrls;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    if ($row->getSourceProperty('type') !== 'paragraph--cta') {
      return FALSE;
    }

    $link = $row->getSourceProperty('field_link');

    $link_uri = $link ? $link['uri'] : NULL;
    $link_title = $link ? $link['title'] : NULL;

    $row->setSourceProperty('link_uri', $link_uri);
    $row->setSourceProperty('link_title', $link_title);

    return parent::prepareRow($row);
  }
}
