<?php

namespace Drupal\ur_schema_metatag_organization_areaserved\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;
use Drupal\schema_metatag\SchemaMetatagManager;

/**
 * Schema.org Organization Area Served Extension
 */
class SchemaOrganizationAreaServedBase extends SchemaNameBase {

  public static function geoFormKeys() {
    return [
      '@type',
      'latitude',
      'longitude',
    ];
  }

  public static function organizationAreaServedFormKeys() {
    return [
      '@type',
      'geoMidpoint',
      'geoRadius',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $element = [])
  {

    $value = SchemaMetatagManager::unserialize($this->value());

    $input_values = [
      'title' => $this->label(),
      'description' => $this->description(),
      'value' => $value,
      '#required' => isset($element['#required']) ? $element['#required'] : FALSE,
      'visibility_selector' => $this->visibilitySelector(),
    ];
    $input_values += SchemaMetatagManager::defaultInputValues();

    $visibility_selector = $input_values['visibility_selector'];
    $selector = ':input[name="' . $visibility_selector . '[@type]"]';
    $visibility = ['invisible' => [$selector => ['value' => '']]];
    $selector2 = SchemaMetatagManager::altSelector($selector);
    $visibility2 = ['invisible' => [$selector2 => ['value' => '']]];
    $visibility['invisible'] = [$visibility['invisible'], $visibility2['invisible']];

    $form['#type'] = 'fieldset';
    $form['#title'] = $input_values['title'];
    $form['#description'] = $input_values['description'];
    $form['#tree'] = TRUE;

    $form['@type'] = [
      '#type' => 'select',
      '#title' => $this->t('@type'),
      '#default_value' => !empty($value['@type']) ? $value['@type'] : '',
      '#empty_option' => t('- None -'),
      '#empty_value' => '',
      '#options' => [
        'GeoCircle' => $this->t('GeoCircle'),
      ],
      '#required' => FALSE,
      '#weight' => -10,
    ];

    $form['geoMidpoint'] = $this->geoForm([
      'title' => $this->t('geomidpoint'),
      'description' => 'The geographical area served by the business, defined by a GeoCircle.',
      'value' => !empty($value['geoMidpoint']) ? $value['geoMidpoint'] : [],
      '#required' => $input_values['#required'],
      'visibility_selector' => $visibility_selector . '[geoCircle]',
    ]);

    $form['geoMidpoint']['#states'] = $visibility;

    $form['geoRadius'] = [
      '#type' => 'textfield',
      '#title' => $this->t('geoRadius'),
      '#default_value' => !empty($value['geoRadius']) ? $value['geoRadius'] : '80467.2',
      '#maxlength' => 20,
      '#required' => FALSE,
      '#weight' => 20,
      '#description' => $this->t("The radius of the GeoCircle in Meters."),
    ];

    $keys = static::organizationAreaServedFormKeys();
    foreach ($keys as $key) {
      if ($key != '@type') {
        $form[$key]['#states'] = $visibility;
      }
    }

    return $form;
  }

  private function geoMidpoint($input_values) {

    $input_values += SchemaMetatagManager::defaultInputValues();
    $value = $input_values['value'];

    // Get the id for the nested @type element.
    $visibility_selector = $input_values['visibility_selector'];
    $selector = ':input[name="' . $visibility_selector . '[@type]"]';
    $visibility = ['invisible' => [$selector => ['value' => '']]];
    $selector2 = SchemaMetatagManager::altSelector($selector);
    $visibility2 = ['invisible' => [$selector2 => ['value' => '']]];
    $visibility['invisible'] = [$visibility['invisible'], $visibility2['invisible']];

    $form['#type'] = 'fieldset';
    $form['#title'] = $input_values['title'];
    $form['#description'] = $input_values['description'];
    $form['#tree'] = TRUE;

    $form['@type'] = [
      '#type' => 'select',
      '#title' => $this->t('@type'),
      '#default_value' => !empty($value['@type']) ? $value['@type'] : '',
      '#empty_option' => t('- None -'),
      '#empty_value' => '',
      '#options' => [
        'Place' => $this->t('Place'),
        'AdministrativeArea' => $this->t('AdministrativeArea'),
      ],
      '#required' => $input_values['#required'],
      '#weight' => -10,
    ];

    $form['geoMidpoint'] = $this->geoForm($input_values);
    $form['geoMidpoint']['#states'] = $visibility;


    return $form;
  }

  /**
   * The form element.
   */
  public function geoForm($input_values) {

    $input_values += SchemaMetatagManager::defaultInputValues();
    $value = $input_values['value'];

    // Get the id for the nested @type element.
    $selector = ':input[name="' . $input_values['visibility_selector'] . '[@type]"]';
    $visibility = ['invisible' => [$selector => ['value' => '']]];
    $selector2 = SchemaMetatagManager::altSelector($selector);
    $visibility2 = ['invisible' => [$selector2 => ['value' => '']]];
    $visibility['invisible'] = [$visibility['invisible'], $visibility2['invisible']];

    $form['#type'] = 'fieldset';
    $form['#title'] = $input_values['title'];
    $form['#description'] = $input_values['description'];
    $form['#tree'] = TRUE;

    $form['@type'] = [
      '#type' => 'select',
      '#title' => $this->t('@type'),
      '#default_value' => !empty($value['@type']) ? $value['@type'] : '',
      '#empty_option' => t('- None -'),
      '#empty_value' => '',
      '#options' => [
        'GeoCoordinates' => $this->t('GeoCoordinates'),
      ],
      '#required' => $input_values['#required'],
      '#weight' => -10,
    ];

    $form['latitude'] = [
      '#type' => 'textfield',
      '#title' => $this->t('latitude'),
      '#default_value' => !empty($value['latitude']) ? $value['latitude'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
      '#description' => $this->t("The latitude of a location. For example 37.42242 (WGS 84)."),
    ];

    $form['longitude'] = [
      '#type' => 'textfield',
      '#title' => $this->t('longitude'),
      '#default_value' => !empty($value['longitude']) ? $value['longitude'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
      '#description' => $this->t("The longitude of a location. For example -122.08585 (WGS 84)."),
    ];

    $keys = static::geoFormKeys();
    foreach ($keys as $key) {
      if ($key != '@type') {
        $form[$key]['#states'] = $visibility;
      }
    }
    return $form;
  }

}
