<?php

namespace Drupal\ur_schema_metatag_organization_areaserved\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_organization_name' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_organization_area_served",
 *   label = @Translation("areaserved"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. This defines the area around the location that is served."),
 *   name = "areaServed",
 *   group = "schema_organization",
 *   weight = 1,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaOrganizationAreaServed extends SchemaOrganizationAreaServedBase {

}
