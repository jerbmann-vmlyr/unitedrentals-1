<?php

namespace Drupal\ur_api_mock_dataservice\Worker;

use Drupal\ur_api_mock_dataservice\Data\JSONDataSource;


class TestSaver {

  private $datasource;

  /**
   * TestSaver constructor.
   */
  public function __construct() {
    $this->datasource = new JSONDataSource('testdata');
  }

  /**
   * @param $data
   */
  public function execute($data) {
    $this->datasource->append($data);
  }

}
