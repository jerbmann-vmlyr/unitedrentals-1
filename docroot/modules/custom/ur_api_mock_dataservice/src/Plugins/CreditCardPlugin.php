<?php

namespace Drupal\ur_api_mock_dataservice\Plugins;

use Drupal\ur_api_mock_dataservice\Data\JSONDataSource;

class CreditCardPlugin extends BasePlugin {

  private $datasource;

  private $userId;

  /**
   * CreditCardPlugin constructor.
   */
  public function __construct() {
    $this->userId = \Drupal::currentUser()->id();
    $this->datasource = new JSONDataSource('creditcard');
  }

  /**
   * @param null $id
   * @return array|mixed
   */
  public function get($id = NULL) {
    if ($id === NULL) {
      return $this->datasource->read();
    }

    return $this->datasource->retrieveByKey('id', $id);
  }

  /**
   * @TODO: Probably doesn't work right.
   *
   * @param $card
   * @return mixed
   */
  public function save($card) {
    $existingCards = $this->datasource->read();

    if (count($existingCards) <= 0) {
      $card['id'] = 'Z' . str_pad(rand(10000, 999999), 9, '0', STR_PAD_LEFT);
    }
    else {
      $ids = array_map(function ($a) {
        return $a->id;
      }, $existingCards);

      sort($ids);
      $maxId = array_pop($ids);
      $card['id'] = substr($maxId, 0, 1) . str_pad((int) substr($maxId, 1) + 1, 9, 0, STR_PAD_LEFT);
    }

    $card['ccNum4'] = substr($card['cardNumber,'] - 4);

    switch (substr($card['cardNumber'], 0, 1)) {
      case '3':
        $card['cardType'] = 'A';
        break;

      case '4':
        $card['cardType'] = 'V';
        break;

      case '5':
        $card['cardType'] = 'M';
        break;

      case '6':
        $card['cardType'] = 'D';
        break;

      default:
        break;
    }

    $card['status'] = 'A';
    unset($card['cardNumber']);
    $this->datasource->append((object) $card);

    return $card;
  }

  /**
   * @param $card
   * @return array
   */
  public function update($card) {
    $existingCard = $this->datasource->retrieveByKey('id', $card['id']);
    $this->datasource->removeItemByKey('id', $card['id']);

    foreach ($card as $key => $value) {
      $existingCard->{$key} = $value;
    }

    $this->datasource->append((object) $existingCard);

    return $existingCard;
  }

  /**
   * @param $id
   */
  public function delete($id) {
    $this->datasource->removeItemByKey('id', $id);
  }

}
