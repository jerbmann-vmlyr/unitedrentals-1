<?php

namespace Drupal\ur_api_mock_dataservice\Worker;

use Drupal\ur_api_mock_dataservice\Data\JSONDataSource;

class TestGetter {

  private $datasource;

  /**
   * TestGetter constructor.
   */
  public function __construct() {
    $this->datasource = new JSONDataSource('testdata');
  }

  /**
   * @param null $id
   * @return array|mixed
   */
  public function execute($id = NULL) {
    if ($id === NULL) {
      return $this->datasource->read();
    }

    return $this->datasource->retrieveByKey('id', $id);
  }

}
