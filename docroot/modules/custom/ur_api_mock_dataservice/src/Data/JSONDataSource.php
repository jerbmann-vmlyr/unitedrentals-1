<?php

namespace Drupal\ur_api_mock_dataservice\Data;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Exception\FileLocatorFileNotFoundException;

/**
 * Class JSONDataSource
 *
 * @package Drupal\ur_api_mock_dataservice\Data
 */
class JSONDataSource {

  /**
   * @var string
   */
  public $file;

  /**
   * JSONDataSource constructor.
   *
   * @param $datatype
   */
  public function __construct($datatype) {
    $fileLocator = new FileLocator($this->getDrupalPath('ur_api_mock_dataservice') . '/data');
    $filename = "$datatype.json";

    try {
      $this->file = $fileLocator->locate($filename);
    }
    catch (FileLocatorFileNotFoundException $e) {
      $filepath = $this->getDrupalPath('ur_api_mock_dataservice') . '/data/';
      $filename = "$filepath$filename";

      $fhandle = fopen($filename, 'w');
      fwrite($fhandle, '[]');

      $this->file = $filename;
    }
  }

  /**
   * Save the data to the json file.
   *
   * @param $data
   */
  public function save($data) {
    $file = fopen($this->file, 'w+');
    fwrite($file, json_encode($data));
    fclose($file);
  }

  /**
   * Read all of the data from the JSON File.
   *
   * @return mixed
   */
  public function read() {
    $data = file_get_contents($this->file);

    return json_decode($data);
  }

  /**
   * Append to the JSON file.
   * @param $data
   */
  public function append($data) {
    $fileData = $this->read();
    $fileData[] = $data;

    $this->save($fileData);
  }

  /**
   * Retrieve an item from the JSON File by Key.
   *
   * @param $key
   *   The key to search.
   * @param $value
   *   The value to find in the key.
   *
   * @return array
   */
  public function retrieveByKey($key, $value) {
    $fileData = $this->read();

    $item = array_filter($fileData, function ($a) use ($key, $value) {
      return $a->$key == $value;
    });

    if (count($item) > 0) {
      return reset($item);
    }

    return [];
  }

  /**
   * Remove an item from the JSON File by Key.
   *
   * @param $key
   *   The key to search.
   * @param $value
   *   The value to find in the key.
   */
  public function removeItemByKey($key, $value) {
    $fileData = $this->read();

    foreach ($fileData as $id=>$item) {
      if ($item->$key === $value) {
        unset($fileData[$id]);
      }
    }

    $this->save(array_values($fileData));
  }

  /**
   * Update an item in the JSON File by key.
   *
   * @param $key
   * @param $value
   * @param $data
   */
  public function updateItemByKey($key, $value, $data) {
    $this->removeItemByKey($key, $value);
    $this->append($data);
  }

  /**
   * Get the Path of a module.
   *
   * @param $moduleName
   *
   * @return array|string
   */
  public function getDrupalPath($moduleName) {
    if (function_exists('drupal_get_path')) {
      return drupal_get_path('module', $moduleName);
    }

    $filepath = explode($moduleName, __DIR__);
    $filepath = $filepath[0] . "/$moduleName";

    return $filepath;
  }
}