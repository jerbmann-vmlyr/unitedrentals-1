<?php

namespace Drupal\ur_api_mock_dataservice\tests\Unit;

use Drupal\Tests\UnitTestCase;

use Drupal\ur_api_mock_dataservice\Data\JSONDataSource;

class JSONDataSourceTest extends UnitTestCase {

  public $filepath;

  /**
   * JSONDataSourceTest constructor.
   */
  public function __construct() {
    $filepath = explode('ur_api_mock_dataservice', __DIR__);
    $filepath = $filepath[0] . "/ur_api_mock_dataservice/data/testdata.json";
    $this->filepath = $filepath;
  }

  public function testItCreatesAJsonFileIfOneDoesntExist() {
    // clean up testing file to ensure that the module will recreate it.
    if (file_exists($this->filepath)) {
      unlink($this->filepath);
    }

    $source = new JSONDataSource('testdata');
    $this->assertFileExists($this->filepath);

    // clean up the testing file
    unlink($this->filepath);
  }

  public function testItCanAppendASingleObject() {
    // clean up testing file to ensure that the module will recreate it.
    if (file_exists($this->filepath)) {
      unlink($this->filepath);
    }

    $source = new JSONDataSource('testdata');
    $item = (object) ['id' => 'a', 'data' => '1'];
    $source->append($item);
    $expectedResults = '[{"id":"a","data":"1"}]';
    $testContents = file_get_contents($this->filepath);
    $this->assertTrue($expectedResults === $testContents);

    unlink($this->filepath);
  }

  public function testItCanAppendAnObject() {
    // clean up testing file to ensure that the module will recreate it.
    if (file_exists($this->filepath)) {
      unlink($this->filepath);
    }

    $source = new JSONDataSource('testdata');
    $item = (object) ['id' => 'a', 'data' => '1'];
    $source->append($item);

    $item = (object) ['id' => 'b', 'data' => '2'];
    $source->append($item);

    $expectedResults = '[{"id":"a","data":"1"},{"id":"b","data":"2"}]';
    $testContents = file_get_contents($this->filepath);
    $this->assertEquals($expectedResults, $testContents);

    unlink($this->filepath);
  }

  public function testItCanRetrieveAnObjectByKey() {
    // clean up testing file to ensure that the module will recreate it.
    if (file_exists($this->filepath)) {
      unlink($this->filepath);
    }

    //setup the data
    $source = new JSONDataSource('testdata');
    $item = (object) ['id' => 'a', 'data' => '1'];
    $source->append($item);

    $item = (object) ['id' => 'b', 'data' => '2'];
    $source->append($item);

    //the actual test
    $data = $source->retrieveByKey('id', 'b');
    $this->assertEquals($data->data, '2');

    // clean up the testing file
    unlink($this->filepath);
  }

  public function testItCanUpdateAnObjectByKey() {
    // clean up testing file to ensure that the module will recreate it.
    if (file_exists($this->filepath)) {
      unlink($this->filepath);
    }

    //setup the data
    $source = new JSONDataSource('testdata');
    $item = (object) ['id' => 'a', 'data' => '1'];
    $source->append($item);

    $item = (object) ['id' => 'b', 'data' => '2'];
    $source->append($item);

    //here's the test
    $updatedItem = (object) ['id' => 'a', 'data' => '3'];

    $source->updateItemByKey('id', 'a', $updatedItem);

    $expectedResults = '[{"id":"b","data":"2"},{"id":"a","data":"3"}]';
    $testContents = file_get_contents($this->filepath);
    $this->assertEquals($expectedResults, $testContents);

    // clean up the testing file
    unlink($this->filepath);
  }

}
