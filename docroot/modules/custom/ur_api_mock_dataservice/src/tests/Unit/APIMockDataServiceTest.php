<?php

namespace Drupal\ur_api_mock_dataservice\tests\Unit;

use PHPUnit\Framework\TestCase;
use Drupal\ur_api_mock_dataservice\DataService;
use Drupal\ur_api_mock_dataservice\Worker\TestGetter;

class APIMockDataServiceTest extends TestCase {

  public function testItCanInstantiateItself() {
    $service = new DataService();
    $this->assertInstanceOf(DataService::class, $service);
  }

  public function testItCanGetAWorker() {
    $service = new DataService();
    $getter = $service->get('TestGetter');
    $this->assertInstanceOf(TestGetter::class, $getter);
  }

}
