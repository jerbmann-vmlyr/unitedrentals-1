<?php

namespace Drupal\ur_api_mock_dataservice;

use Drupal\ur_frontend_api_v2\Service\FEDataService;

class DataService extends FEDataService {

  public function describe() {
    return 'Mock Data';
  }

}
