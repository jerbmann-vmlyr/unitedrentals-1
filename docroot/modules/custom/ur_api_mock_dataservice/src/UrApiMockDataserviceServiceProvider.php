<?php

namespace Drupal\ur_api_mock_dataservice;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\ur_api_mock_dataservice\DataService;

class UrApiMockDataserviceServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('ur_api_dataservice');
    $definition->setClass(DataService::class);
  }

}
