<?php

/**
 * @file
 * Hooks for urone_categories module.
 */

use \Drupal\Core\Entity\EntityInterface;
use \Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function urone_categories_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the ur_sso module.
    case 'help.page.urone_categories':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides helper functions related to the categories taxonomy') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_extra_field_info().
 */
function urone_categories_entity_extra_field_info() {
  $extra = array();

  $extra['taxonomy_term']['categories']['display']['category_parent'] = [
    'label' => t('Category Parent'),
    'description' => t('Parent category for sub categories'),
    'weight' => 100,
    'visible' => TRUE,
  ];

  return $extra;
}

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function urone_categories_taxonomy_term_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if ($display->getComponent('category_parent')) {
    $ancestors = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadAllParents($entity->id());
    $parent = end($ancestors);
    $aliasManager = \Drupal::service('path.alias_manager');
    $alias = $aliasManager->getAliasByPath('/taxonomy/term/' . $parent->id());

    $build['category_parent'] = [
      '#type' => 'markup',
      '#markup' => '<a href="' . $alias . '">' . $parent->label() . '</a>',
    ];
  }
}
