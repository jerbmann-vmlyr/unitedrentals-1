<?php

namespace Drupal\urone_block_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SearchBlock' block.
 *
 * @Block(
 *  id = "search_block",
 *  admin_label = @Translation("Search"),
 *  category = @Translation("urone"),
 * )
 */
class SearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'urone_block_search_block';
    $build['#attached']['library'][] = 'urone_block_search/block-search';
    return $build;
  }

}
