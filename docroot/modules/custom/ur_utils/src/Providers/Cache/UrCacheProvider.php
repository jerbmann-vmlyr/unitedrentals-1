<?php

namespace Drupal\ur_utils\Providers\Cache;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;


/**
 * Class UrCache
 *
 * @package Drupal\ur_utils\Providers
 */
class UrCacheProvider {

  // General settings
  public const PERMANENT = Cache::PERMANENT;

  // Cache Bins
  public const BIN_DB_STANDARD = 'ur_db_standard';
  public const BIN_DB_API = 'ur_db_api';

  public const BIN_MEMCACHE_STANDARD = 'ur_memcache_standard';
  public const BIN_MEMCACHE_API = 'ur_memcache_api';

  public const BIN_FILESYSTEM_STANDARD = 'ur_filesystem_standard';
  public const BIN_FILESYSTEM_API = 'ur_filesystem_api';

  public const BIN_DEFAULT = self::BIN_DB_STANDARD;

  // Active bits
  private static $classSettingsList = [];

  /**
   * @var UrCacheProvider
   *    Hold an instance of the class
   */
  private static $instance;

  /**
   * The singleton method
   *
   * @return UrCacheProvider
   */
  public static function getInstance():UrCacheProvider {
    if (!isset(static::$instance)) {
      static::$instance = new UrCacheProvider();
    }

    return static::$instance;
  }

  /**
   * Handle dynamic, static calls to the object.
   *
   * @param string $method
   * @param array $args
   * @return mixed
   *
   * @throws \RuntimeException
   */
  public static function __callStatic($method, $args) {
    $instance = static::getInstance();

    if (!$instance) {
      throw new \RuntimeException('Unable to load the class instance.');
    }

    return $instance->$method(...$args);
  }

  /**
   * UrCacheProvider constructor.
   */
  public function __construct() {
    // Nothing to see here

  }

  /**
   * Just a way to always load the default cache bin and such.
   *
   * @param string $bin
   * @return \Drupal\Core\Cache\CacheBackendInterface
   */
  public static function getCache($bin = self::BIN_DEFAULT):CacheBackendInterface {
    $cacheBackend = \Drupal::cache($bin);
    return $cacheBackend;
  }

  /**
   * Builds a basic expiration time.
   *
   * @param int $seconds
   *   - The amount of time, in seconds, to expire the cache.
   *   One of the following values:
   *   - CacheBackendInterface::CACHE_PERMANENT: Indicates that the item should
   *     not be removed unless it is deleted explicitly.
   *   - Number of seconds: Indicates that the item will be considered invalid
   *     after this amount of time, i.e. it will not be returned by get() unless
   *     $allow_invalid has been set to TRUE. When the item has expired, it may
   *     be permanently deleted by the garbage collector at any time.
   *
   * @return int
   */
  public static function expire($seconds):int {
    if ($seconds == static::PERMANENT) {
      return static::PERMANENT;
    }

    return time() + $seconds;
  }

  /**
   * Generate a unique ID to cache this to.
   *
   * @param array $requestParams
   * @param array $settings
   * @param int $traceLevel
   *
   * @return bool|mixed|null|string
   */
  public static function generateId($requestParams = [], $settings = [], $traceLevel = 3) {
    if (empty($settings)) {
      $settings = static::getInfoAndSettings($traceLevel);
    }

    $uuid = [];

    if (!empty($settings) && isset($settings['key'])) {
      if (!empty($settings['idFields'])) {
        $keyString = static::getKeyFromFields($requestParams, $settings['idFields']);

        if (!empty($keyString)) {
          $uuid[] = $keyString;
        }
      }

      if (!empty($settings['uidKey'])) {
        $uuid[] = $settings['uidKey'];
      }

      if (empty($settings['key'])) {
        $uuid[] = $settings['baseName'] . $uuid;
      }
      else {
        $uuid[] = $settings['key'];
      }
    }

    /*
     * If no UUID has been built yet, then make one from the caller class and
     * method name.
     */
    if (empty($uuid)) {
      $uuid[] = $settings['baseName'];
    }

    return implode('|', $uuid);
  }

  /**
   * Get the caller info and class settings.
   *
   * @param null $traceLevel
   * @param array $dynamicTags
   *   - Dynamically generated tags
   *
   * @return array
   */
  public static function getInfoAndSettings($traceLevel = NULL, $dynamicTags = []):array {
    $info = static::getCallerInfo($traceLevel);
    $settings = static::getClassCachingSettings($info['classFull'], $dynamicTags);

    return array_merge($info, $settings);
  }

  /**
   * Gets the cache settings for a given class.
   *
   * @see: NOTE: Consider duplicating Drupal's token system to generate dynamic
   *    tags that we use to find and invalidate. My concern here is duplicating
   *    functionality reliably. This may be better to do within the calling
   *    class. But then that might create a lot of one off solutions that are
   *    inconsistent. The challenge of doing it here is that we need to always
   *    pair the dynamic data with the pattern. Data which is not coming from
   *    the DB, but instead from external data sources.
   *
   * @param string|\Drupal\ur_utils\Traits\UrCacheSettings $class
   *   - A string containing the name of any class or a class that uses the
   *     UrCacheSettings trait.
   *
   * @param array $dynamicTags
   *   - Dynamically generated tags that are passed in. Adds flexibility.
   *
   * @return array
   */
  public static function getClassCachingSettings($class, $dynamicTags = []):array {
    $settings = [];

    // Retrieve statically store calculated settings.
    if (isset(static::$classSettingsList[$class])) {
      return static::$classSettingsList[$class];
    }

    $classMethods = get_class_methods($class);

    // Make sure that the calling class has the cache settings we are looking for
    if (!empty($classMethods) && \in_array('getCacheSettings', $classMethods, FALSE)) {
      /**
       * @see \Drupal\ur_utils\Traits\UrCacheSettings::getCacheSettings
       * @var array $settings
       */
      $settings = $class::getCacheSettings();
      $cacheToUser = $settings['cacheToUser'];

      // Tie this to the user, unless anonymous.
      if ($cacheToUser && !\Drupal::currentUser()->isAnonymous()) {
        $userId = \Drupal::currentUser()->id();
        $settings['uidKey'] = "uid:$userId";

        if (!empty($settings['tags'])) {
          $tagList = $settings['tags'];

          /*
           * Generate uid versions of current tags
           *
           * NOTE: IF we cache to the user then we run a risk in clearing cache
           * for all users if we are not careful. So we can either remove generic
           * tags and only associate to the user. OR we have to be EXTRA careful
           * when we go to clear them.
           *
           * To start, I am going to use the passed in tags to build user-specific
           * versions of those tags. Then I will remove the completely generic
           * tags so that we only invalidate the user's cache and not everyone's.
           */
          $newTagList = [];

          foreach ($tagList as $tag) {
            $newTag = $settings['uidKey'] . '|' . $tag;
            static::addTag($newTag, $newTagList);
          }

          $settings['tags'] = $newTagList; // To ensure only user-specific tags
        }

        $settings['tags'][] = $settings['uidKey'];
      }
    }

    // Make sure that the calling class has the method we are looking for
    if (!empty($dynamicTags)
        && \in_array('matchTagPattern', get_class_methods($class), FALSE)) {
      // Process dynamic tag patterns
      $dynamicTags = $class::matchTagPattern($dynamicTags);
    }

    if (!empty($settings['tags'])) {
      // Merge in any dynamically generated tags
      $settings['tags'] = array_merge($settings['tags'], $dynamicTags);

      // If we have set a cacheKey, then let's add it as a tag.
      if (!empty($settings['key']) && empty($settings['cacheToUser'])) {
        static::addTag($settings['key'], $settings['tags']);
      }
    }

    // Statically store calculated settings for a given class to reduce processing load.
    static::$classSettingsList[$class] = $settings;

    return $settings;
  }

  /**
   * Adds a tag to a tag list while preventing duplicates.
   *
   * @param $tag
   * @param $tagList
   */
  public static function addTag($tag, &$tagList):void {
    // Prevent duplicate tags (probably a rare occasion)
    if (!\in_array($tag, $tagList, FALSE)) {
      $tagList[] = $tag;
    }
  }

  /**
   * Calling method info.
   *
   * @param int $traceLevel
   * @return array|bool
   */
  public static function getCallerInfo($traceLevel = NULL):?array {
    // @TODO: This might be wrong.
    if ($traceLevel !== NULL) {
      $traceLevel++; // Increase level to catch up to this
    }
    else {
      $traceLevel = 3;
    }

    // Detect calling function and class.
    $trace = debug_backtrace();

    // Use the trace to build the name.
    if (!empty($trace[$traceLevel])) {
      $caller = $trace[$traceLevel];
      $caller_array = explode('\\', $caller['class']);
      $class = array_pop($caller_array);
      $methodName = $caller['function'];
      $baseName = "$class|$methodName";

      return [
        'class' => $class,
        'classFull' => $caller['class'],
        'method' => $methodName,
        'baseName' => $baseName,
      ];
    }

    return FALSE;
  }

  /**
   * Retrieves key values from fields in cache.
   *
   * @param $data
   * @param $fields
   *
   * @return string|bool - Either the ID string or false
   */
  public static function getKeyFromFields($data, $fields):?string {
    if (empty($fields)) {
      return FALSE;
    }

    $keyValues = [];

    // Only add the data from the fields we have enabled
    foreach ($data as $fieldName => $fieldValue) {
      if (\in_array($fieldName, $fields)) {
        $keyValues[] = $fieldName . ':' . $fieldValue;
      }
    }

    return implode('|', $keyValues);
  }

  /**
   * Attempts to retrieve the cache and builds the cache name dynamically from
   * a list of parameters and the name of the calling class/method.
   *
   * @param array $requestParams
   *   - The parameters were used to load the data.
   *
   * @param bool $allowInvalid
   * @param int $traceLevel
   *
   * @return false|object
   */
  public static function get($requestParams = [], $allowInvalid = FALSE, $traceLevel = 2) {
    $settings = static::getInfoAndSettings($traceLevel);
    $cid = static::generateId($requestParams, $settings);

    $result = static::getCache($settings['bin'])->get($cid, $allowInvalid);

    if (isset($result->data)) {
      return $result->data;
    }

    return $result;
  }

  /**
   * Attempts to retrieve the cache for a specified key and bin
   *
   * @param string $cid
   * @param string $bin
   * @param bool $allowInvalid
   *
   * @return false|object
   */
  public static function getByCacheId($cid, $bin, $allowInvalid = FALSE) {
    $result = static::getCache($bin)->get($cid, $allowInvalid);

    if (isset($result->data)) {
      return $result->data;
    }

    return $result;
  }

  /**
   * Attempts to retrieve the cache and builds the cache name dynamically from
   * a list of parameters and the name of the calling class/method. It, finally,
   * pulls the data from the data element and returns that.
   *
   * @param array $requestParams
   *   - The parameters were used to load the data.
   * @param bool $allowInvalid
   *
   * @return bool
   */
  public static function getData($requestParams = [], $allowInvalid = FALSE):?bool {
    /** @var \stdClass $cache */
    $cache = static::get($requestParams, $allowInvalid, 3); // Only use traceLevel because this is an intermediary method

    if (isset($cache->data)) {
      return $cache->data;
    }

    return FALSE;
  }

  /**
   * @TODO: Debating as to whether or not I want to build this out. If I do,
   *        then I will also need to create a new checksum tool to inject into
   *        the cache bins.
   *
   * @param $tag
   * @param bool $allowInvalid
   * @param int $traceLevel
   */
  public static function getByTag($tag, $allowInvalid = FALSE, $traceLevel = 2) {
    // @TODO: Build out a way to load by tag. This is key to collections and data plucking
  }

  /**
   * Sets some data in a cache for a pecified cid and bin. It also converts
   * an expiration of seconds into a timestamp.
   *
   * @param mixed $data
   *   - The data to store in the cache.
   *   Some storage engines only allow objects up to a maximum of 1MB in size to
   *   be stored by default. When caching large arrays or similar, take care to
   *   ensure $data does not exceed this size.
   *
   * @param string $cid
   *   - The cache id (key)
   *
   * @param string $bin
   *   - The cache bin to use
   *
   * @param string $expireSeconds
   *   - The number of seconds until the cache should expire
   *
   * @param array $dynamicTags
   *   - Dynamically generated tags
   */
  public static function setByCacheId($data, $cid, $bin, $expireSeconds, $dynamicTags = []) {
    $expire = static::expire($expireSeconds);

    static::getCache($bin)->set($cid, $data, $expire, $dynamicTags);
  }

  /**
   * Sets some data in a cache. Builds the cache ID itself. It also converts
   * an expiration of seconds into a timestamp.
   *
   * @param mixed $data
   *   - The data to store in the cache.
   *   Some storage engines only allow objects up to a maximum of 1MB in size to
   *   be stored by default. When caching large arrays or similar, take care to
   *   ensure $data does not exceed this size.
   *
   * @param array $requestParams
   *   - The parameters were used to load the data.
   *
   * @param array $dynamicTags
   *   - Dynamically generated tags
   */
  public static function set($data, $requestParams = [], $dynamicTags = []) {
    $settings = static::getInfoAndSettings(2, $dynamicTags);
    $cid = static::generateId($requestParams, $settings);

    // Simplifies how we do the expiration.
    $expire = static::expire($settings['ttl']);

    static::getCache($settings['bin'])->set($cid, $data, $expire, $settings['tags']);
  }

  public static function getCollection() {
    // @TODO: Build out this stub for collections
  }

  public static function setCollection() {
    // @TODO: Build out this stub for collections
  }

  /**
   * Deletes a bit of cache. Builds the name for the cache.
   *
   * @param array $requestParams
   *   - The parameters were used to load the data.
   */
  public static function delete($requestParams = []):void {
    $settings = static::getInfoAndSettings(2);
    $cid = static::generateId($requestParams);

    static::getCache($settings['bin'])->delete($cid);
  }

  /**
   * Invalidates a bit of cache. Builds the name for the cache.
   *
   * @param string $cid
   *   - Cache ID.
   */
  public static function invalidate($cid):void {
    $settings = static::getInfoAndSettings(2);
    static::getCache($settings['bin'])->invalidate($cid);
  }

  /**
   * Invalidates a bit of cache. Builds the name for the cache.
   *
   * @param string $cid
   *   - Cache ID.
   *
   * @param string $bin
   *   - The cache bin to use
   */
  public static function invalidateByCid($cid, $bin):void {
    static::getCache($bin)->invalidate($cid);
  }

  /**
   * Invalidates a bit of cache. Builds the name for the cache.
   *
   * @param array $requestParams
   *   - The parameters were used to load the data.
   */
  public static function invalidateByAutoId($requestParams = []):void {
    $settings = static::getInfoAndSettings(2);
    $cid = static::generateId($requestParams);

    static::getCache($settings['bin'])->invalidate($cid);
  }

  /**
   * Marks cache items from all bins with any of the specified tags as invalid.
   *
   * Implemented in the Memcache and Cache utilities, but not the DB cache utility.
   * We can work around this by replicating how Memcache invalidates tags and
   * extending the class to our own to add that functionality.
   *
   * @param string[] $tags
   *   The list of tags to invalidate cache items for.
   */
  public static function invalidateTags(array $tags):void {
    Cache::invalidateTags($tags);
  }

  /**
   * Invalidates a single tag by wrapping the string in an array and passing it
   * to the invalidateTags() method.
   *
   * @param string $tag
   * @return mixed
   */
  public static function invalidateTag($tag):void {
    static::invalidateTags([$tag]);
  }

  /**
   * Invalidates all tags for the calling class.
   */
  public static function invalidateClassTags():void {
    $settings = static::getInfoAndSettings(2);
    static::invalidateTags($settings['tags']);
  }

}
