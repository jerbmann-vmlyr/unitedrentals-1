<?php

namespace Drupal\ur_utils\Traits;

/**
 * Class GetsUserId
 *
 * @package Drupal\ur_api_dataservice\Traits
 */
trait GetsUserId {

  /**
   * Gets the user's ID.
   *
   * @return int
   */
  public function getUserId() {
    return \Drupal::currentUser()->id();
  }

  /**
   * Gets the user's GUID.
   *
   * @return bool|string
   */
  public function getGuid() {
    if (!empty($_SESSION['ur_guid'])) {
      return $_SESSION['ur_guid'];
    }

    // @TODO: Call a static method to get a GUID from the API Data Service

    return FALSE;
  }

}
