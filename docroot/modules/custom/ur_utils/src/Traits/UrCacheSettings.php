<?php

namespace Drupal\ur_utils\Traits;

use Drupal\ur_utils\Helpers\TimeHelper;
use Drupal\ur_utils\Providers\Cache\UrCacheProvider;

/**
 * Trait UrCacheSettings
 *
 * Provides cache settings and simplified methods for caching within the class
 * that uses this.
 *
 * @package Drupal\ur_utils\Traits
 */
trait UrCacheSettings {

  /**
   * The unique "key" for this class
   *
   * @var string
   */
  protected static $cacheKey;

  /**
   * Select the cache bin the current class will use.
   *
   * Cache Bins from this module
   * =============================
   *
   * Using the DB:
   *  - ur_db_standard
   *  - ur_db_api
   *
   * Using Memcache:
   *  - ur_memcache_standard
   *  - ur_memcache_api
   *
   * @var string
   */
  protected static $cacheBin = UrCacheProvider::BIN_DB_STANDARD;

  /**
   * Placeholder for the id fields to merge into a key.
   *
   * These are a list of the fields from a data set to use to generate the cache
   * ID for storing the data.
   */
  protected static $cacheIdFields = [];

  /**
   * Default value for the cache time to live
   *
   * int $ttl
   *   - The expiration time, in seconds.
   *   One of the following values:
   *   - CacheBackendInterface::CACHE_PERMANENT: Indicates that the item should
   *     not be removed unless it is deleted explicitly.
   *   - Number of seconds: Indicates that the item will be considered invalid
   *     after this amount of time, i.e. it will not be returned by get() unless
   *     $allow_invalid has been set to TRUE. When the item has expired, it may
   *     be permanently deleted by the garbage collector at any time.
   */
  protected static $cacheTtl = TimeHelper::DAY;

  /**
   * Should this be tied to the user?
   */
  protected static $cacheToUser = false;

  /**
   * Array of tags to use for this data source.
   *
   * Should include all levels of generic and specific we want to be able to
   * invalidate easily.
   *
   * Example....
   * --------------------
   * Tags for transactions: transactions, uid-[user's id]-transactions, quotes,
   *    rentals, account-[account id]-transactions, pending-transactions
   *
   * In this example, we give tags that let us clear ALL transactions, just the
   * current user's transactions, all quotes, all pending transactions, account
   * specific transactions, and all rentals. We can do a variety of combinations
   * for this, depending on what we think we will need.
   *
   * array $tags
   *   An array of tags to be stored with the cache item. These should normally
   *   identify objects used to build the cache item, which should trigger
   *   cache invalidation when updated. For example if a cached item represents
   *   a node, both the node ID and the author's user ID might be passed in as
   *   tags. For example ['node:123', 'node:456', 'user:789'].
   *
   * @var array
   */
  protected static $cacheTags = [];

  /**
   * Define patterns for matching values with string combos. Always use "%s" to
   * set the replace location for the dynamic data.
   *
   * Examples:
   * [id:account-item-%s] - This will match with "[id:42]" from the dynamic tags
   *    to create a tag of "account-item-42" that will end up in the list of tags.
   *
   * @var array
   */
  protected static $cachePatterns = [];

  /**
   * Returns the cache settings for the class as an array.
   *
   * @return array
   *   The array of cache settings for the class.
   */
  public static function getCacheSettings():array {
    return [
      'key' => static::$cacheKey,
      'bin' => static::$cacheBin,
      'uidKey' => '',
      'idFields' => static::$cacheIdFields,
      'ttl' => static::$cacheTtl,
      'cacheToUser' => static::$cacheToUser,
      'tags' => static::$cacheTags,
      'patterns' => static::$cachePatterns,
    ];
  }

  /**
   * Runs through dynamic tags looking for preset patterns to fill.
   *
   * Pattern List
   * ----------------------------
   * [id:%number] - Pass through an item id and it will be paired with the cache
   *    key to create a repeated tagging pattern. Example: [id:3] for accounts
   *    becomes accounts-item-id-3 in the tag list.
   *
   * @param array $dynamicTags
   *  - The list of dynamic tags, including those with set patterns.
   *
   * @return array
   */
  public static function matchTagPattern(array $dynamicTags):array {
    // Scan for patterns
    $dataMatches = static::patternListScan($dynamicTags);
    $patternMatches = static::patternListScan(static::$cachePatterns);
    $matchedFields = []; // Array of fields that were matched to cache patterns

    // Pair data pattern matches with defined cache patterns
    foreach ($dataMatches as $field => $dataMatch) {
      $data = array_keys($dataMatch)[0];
      $tagToReplace = $dataMatch[$data];

      // Replace the found tag pattern with the updated tag.
      if (isset($patternMatches[$field])) {
        $subject = array_keys($patternMatches[$field])[0];
        $newTag = str_replace('%s', $data, $subject);

        $dynamicTags = str_replace($tagToReplace, $newTag, $dynamicTags);
        $matchedFields[] = $field;
      }
    }

    // Do a standard pattern match for ID if one is passed but not specified in patterns
    if (!empty($dataMatches['id']) && !\in_array('id', $matchedFields)) {
      $data = array_keys($dataMatches['id'])[0];
      $dynamicTags[] = static::$cacheKey . '-item-' . $data;
    }

    // Return all tags with embedded matched data
    return $dynamicTags;
  }

  /**
   * Loops through a list of strings looking for patterns.
   *
   * @param array $list
   * @return array
   */
  public static function patternListScan(array $list):array {
    $foundPatterns = [];

    foreach ($list as $item) {
      $itemPatterns = static::patternScan($item);
      $foundPatterns = array_merge($foundPatterns, $itemPatterns);
    }

    return $foundPatterns;
  }

  /**
   * Builds a list of all token-like patterns that appear in the text.
   *
   * @param string $text
   *   The text to be scanned for possible tokens.
   *
   * @return array
   *   An associative array of discovered tokens, grouped by type.
   */
  public static function patternScan($text):array {
    // Matches tokens with the following pattern: [$type:$name]
    // $type and $name may not contain [ ] characters.
    // $type may not contain : or whitespace characters, but $name may.
    preg_match_all('/
      \[             # [ - pattern start
      ([^\s\[\]:]+)  # match $type not containing whitespace : [ or ]
      :              # : - separator
      ([^\[\]]+)     # match $name not containing [ or ]
      \]             # ] - pattern end
      /x', $text, $matches);

    $types = $matches[1];
    $tokens = $matches[2];

    // Iterate through the matches, building an associative array containing
    // $tokens grouped by $types, pointing to the version of the token found in
    // the source text. For example, $results['id']['42'] from '[id:42]'
    $tokenCnt = count($tokens);
    $results = [];

    for ($i = 0; $i < $tokenCnt; $i++) {
      $results[$types[$i]][$tokens[$i]] = $matches[0][$i];
    }

    return $results;
  }

}
