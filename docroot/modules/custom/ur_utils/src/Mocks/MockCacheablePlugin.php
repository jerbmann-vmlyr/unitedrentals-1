<?php

namespace Drupal\ur_utils\Mocks;

use Drupal\ur_utils\Providers\Cache\UrCacheProvider;

class MockCacheablePlugin extends MockBase {

  /**
   * TEST Cache overrides -----------------------------------------.
   */
  protected static $cacheKey = 'mock-test';
  protected static $cacheIdFields = ['id'];
  protected static $cacheBin = UrCacheProvider::BIN_DB_API;
  protected static $cacheTags = ['mocks'];
  protected static $cachePatterns = ['[id:mock-item-%s]', '[name:mocker-%s]'];

  public function __construct() {
    parent::__construct();

    // @TODO: Do other things
  }

}
