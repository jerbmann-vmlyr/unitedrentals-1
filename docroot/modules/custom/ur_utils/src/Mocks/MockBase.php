<?php

namespace Drupal\ur_utils\Mocks;

use Drupal\ur_utils\Traits\UrCacheSettings;

/**
 * Class BasePlugin
 *
 * This is the base class used for all Plugins.
 *
 * @TODO: Make an interface for this OR simply use it as the base for a class of
 *        Plugins that are extended, then turn this BasePlugin into
 *        "BaseDalPlugin" or extend it in "BaseDalPlugin".
 *
 * @package Drupal\ur_api_dataservice\Plugins
 */
abstract class MockBase {

  use UrCacheSettings;

  public function __construct() {
    // @TODO: Fill this out if needed
  }

}
