<?php

namespace Drupal\ur_utils\Helpers;

use Closure;
use Exception;
use Countable;
use Throwable;

class Utils {

  /**
   * Create a collection from the given value.
   *
   * @param mixed $value
   *
   * @return Collection
   */
  public static function collect($value = NULL) {
    return new Collection($value);
  }

  /**
   * @param array $attributes
   * @return Fluent
   */
  public static function fluent($attributes = []) {
    return new Fluent($attributes);
  }

  /**
   * Throw the given exception if the given condition is true.
   *
   * @param mixed $condition
   * @param \Throwable|string $exception
   * @param $parameters
   *
   * @return mixed
   *
   * @throws Throwable
   */
  public static function throw_if($condition, $exception, ...$parameters) {
    if ($condition) {
      throw (is_string($exception) ? new $exception(...$parameters) : $exception);
    }
    return $condition;
  }

  /**
   * Throw the given exception unless the given condition is true.
   *
   * @param mixed $condition
   * @param \Throwable|string $exception
   * @param $parameters
   *
   * @return mixed
   *
   * @throws Throwable
   */
  public static function throw_unless($condition, $exception, ...$parameters) {
    if (!$condition) {
      throw (is_string($exception) ? new $exception(...$parameters) : $exception);
    }
    return $condition;
  }

  /**
   * Returns all traits used by a trait and its traits.
   *
   * @param string $trait
   *
   * @return array
   */
  public static function trait_uses_recursive($trait) {
    $traits = class_uses($trait);
    foreach ($traits as $trait) {
      $traits += Utils::trait_uses_recursive($trait);
    }
    return $traits;
  }

  /**
   * Transform the given value if it is present.
   *
   * @param mixed $value
   * @param callable $callback
   * @param mixed $default
   *
   * @return mixed|null
   */
  public static function transform($value, callable $callback, $default = NULL) {
    if (Utils::filled($value)) {
      return $callback($value);
    }
    if (is_callable($default)) {
      return $default($value);
    }
    return $default;
  }

  /**
   * Determine if the given value is "blank".
   *
   * @param mixed $value
   *
   * @return bool
   */
  public static function blank($value) {
    if (is_null($value)) {
      return TRUE;
    }
    if (is_string($value)) {
      return trim($value) === '';
    }
    if (is_numeric($value) || is_bool($value)) {
      return FALSE;
    }
    if ($value instanceof Countable) {
      return count($value) === 0;
    }
    return empty($value);
  }

  /**
   * Get the class "basename" of the given object / class.
   *
   * @param string|object $class
   *
   * @return string
   */
  public static function class_basename($class) {
    $class = is_object($class) ? get_class($class) : $class;
    return basename(str_replace('\\', '/', $class));
  }

  /**
   * Determine if a value is "filled".
   *
   * @param mixed $value
   *
   * @return bool
   */
  public static function filled($value) {
    return !Utils::blank($value);
  }

  /**
   * Set an item on an array or object using dot notation.
   *
   * @param mixed $target
   * @param string|array $key
   * @param mixed $value
   * @param bool $overwrite
   *
   * @return mixed
   */
  public static function data_set(&$target, $key, $value, $overwrite = TRUE) {
    $segments = is_array($key) ? $key : explode('.', $key);
    if (($segment = array_shift($segments)) === '*') {
      if (!Arr::accessible($target)) {
        $target = [];
      }
      if ($segments) {
        foreach ($target as &$inner) {
          Utils::data_set($inner, $segments, $value, $overwrite);
        }
      }
      elseif ($overwrite) {
        foreach ($target as &$inner) {
          $inner = $value;
        }
      }
    }
    elseif (Arr::accessible($target)) {
      if ($segments) {
        if (!Arr::exists($target, $segment)) {
          $target[$segment] = [];
        }
        Utils::data_set($target[$segment], $segments, $value, $overwrite);
      }
      elseif ($overwrite || !Arr::exists($target, $segment)) {
        $target[$segment] = $value;
      }
    }
    elseif (is_object($target)) {
      if ($segments) {
        if (!isset($target->{$segment})) {
          $target->{$segment} = [];
        }
        Utils::data_set($target->{$segment}, $segments, $value, $overwrite);
      }
      elseif ($overwrite || !isset($target->{$segment})) {
        $target->{$segment} = $value;
      }
    }
    else {
      $target = [];
      if ($segments) {
        Utils::data_set($target[$segment], $segments, $value, $overwrite);
      }
      elseif ($overwrite) {
        $target[$segment] = $value;
      }
    }
    return $target;
  }

  /**
   * Get an item from an object using "dot" notation.
   *
   * @param object $object
   * @param string $key
   * @param mixed $default
   *
   * @return mixed
   */
  public static function object_get($object, $key, $default = NULL) {
    if (is_null($key) || trim($key) == '') {
      return $object;
    }
    foreach (explode('.', $key) as $segment) {
      if (!is_object($object) || !isset($object->{$segment})) {
        return Utils::value($default);
      }
      $object = $object->{$segment};
    }
    return $object;
  }

  /**
   * Get an item from an array or object using "dot" notation.
   *
   * @param mixed $target
   * @param string|array|int $key
   * @param mixed $default
   *
   * @return mixed
   */
  public static function data_get($target, $key, $default = NULL) {
    if (is_null($key)) {
      return $target;
    }
    $key = is_array($key) ? $key : explode('.', $key);
    while (!is_null($segment = array_shift($key))) {
      if ($segment === '*') {
        if ($target instanceof Collection) {
          $target = $target->all();
        }
        elseif (!is_array($target)) {
          return Utils::value($default);
        }
        $result = [];
        foreach ($target as $item) {
          $result[] = Utils::data_get($item, $key);
        }
        return in_array('*', $key) ? Arr::collapse($result) : $result;
      }
      if (Arr::accessible($target) && Arr::exists($target, $segment)) {
        $target = $target[$segment];
      }
      elseif (is_object($target) && isset($target->{$segment})) {
        $target = $target->{$segment};
      }
      else {
        return Utils::value($default);
      }
    }
    return $target;
  }

  /**
   * Retry an operation a given number of times.
   *
   * @param int $times
   * @param callable $callback
   * @param int $sleep
   * @param callable $when
   *
   * @return mixed
   *
   * @throws Exception
   */
  public static function retry($times, callable $callback, $sleep = 0, $when = NULL) {
    $attempts = 0;
    $times--;
    beginning:
    $attempts++;
    try {
      return $callback($attempts);
    }
    catch (Exception $e) {
      if (!$times || ($when && !$when($e))) {
        throw $e;
      }
      $times--;
      if ($sleep) {
        usleep($sleep * 1000);
      }
      goto beginning;
    }
  }

  /**
   * Return the given value, optionally passed through the given callback.
   *
   * @param mixed $value
   * @param callable|null $callback
   *
   * @return mixed
   */
  public static function with($value, callable $callback = NULL) {
    return is_null($callback) ? $value : $callback($value);
  }

  /**
   * Return the default value of the given value.
   * If callback is passed in, then it will be executed ~ otherwise value is returned.
   *
   * @param mixed $value
   *
   * @return mixed
   */
  public static function value($value) {
    return $value instanceof Closure ? $value() : $value;
  }

}
