<?php

namespace Drupal\ur_utils\Helpers;


class TimeHelper {

  /*
   * Time settings
   * ----------------------
   * Time increments in seconds.
   */
  public const YEAR = 31449600;
  public const MONTH = 2419200;
  public const WEEK = 604800;
  public const DAY = 86400;
  public const HOUR = 3600;
  public const MINUTE = 60;
  public const FIVE_MINUTES = 300;
  public const TEN_MINUTES = 600;
  public const ZERO = 0;
  public const EIGHT_HOURS = 28800;

}
