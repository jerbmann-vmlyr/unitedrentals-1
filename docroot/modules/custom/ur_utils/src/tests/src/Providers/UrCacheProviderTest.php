<?php

namespace Drupal\ur_utils\tests\Providers;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\KernelTests\Core\Cache\GenericCacheBackendUnitTestBase;
use Drupal\ur_utils\Providers\Cache\UrCacheProvider;
use Drupal\ur_utils\Mocks\MockCacheablePlugin;

class UrCacheProviderTest extends GenericCacheBackendUnitTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['system', 'memcache', 'ur_utils'];

  /**
   * Creates a new instance of MemcacheBackend.
   *
   * @param string $bin
   * @return CacheBackendInterface
   *   A new MemcacheBackend object.
   *
   * @throws \Exception
   */
  protected function createCacheBackend($bin):CacheBackendInterface {
    return UrCacheProvider::getCache();
  }

  public function testBins() {
    $cache = UrCacheProvider::getCache();
    $default = UrCacheProvider::getCache(UrCacheProvider::BIN_DEFAULT);
    $dbStandard = UrCacheProvider::getCache(UrCacheProvider::BIN_DB_STANDARD);
    $dbApi = UrCacheProvider::getCache(UrCacheProvider::BIN_DB_API);
    $memcacheStandard = UrCacheProvider::getCache(UrCacheProvider::BIN_MEMCACHE_STANDARD);
    $memcacheApi = UrCacheProvider::getCache(UrCacheProvider::BIN_MEMCACHE_API);

    self::assertEquals('cache.ur_db_standard', $cache->_serviceId);
    self::assertEquals('cache.ur_db_standard', $default->_serviceId);
    self::assertEquals('cache.ur_db_standard', $dbStandard->_serviceId);
    self::assertEquals('cache.ur_db_api', $dbApi->_serviceId);
    self::assertEquals('cache.ur_memcache_standard', $memcacheStandard->_serviceId);
    self::assertEquals('cache.ur_memcache_api', $memcacheApi->_serviceId);
  }

  public function testExpire() {
    $seconds = 20;
    self::assertEquals(time() + $seconds, UrCacheProvider::expire($seconds));

    $seconds = Cache::PERMANENT;
    self::assertEquals($seconds, UrCacheProvider::expire($seconds));
  }

  public function testGenerateId() {
    $settings = [
      'key' => 'GenTest',
      'uidKey' => 'uid:1',
      'idFields' => [],
      'cacheToUser' => FALSE,
      'tags' => ['test', 'generate', 'special-id'],
      'patterns' => [],
    ];

    $cid = UrCacheProvider::generateId([], $settings);
    self::assertEquals('uid:1|GenTest', $cid);

    $settings['idFields'] = ['id'];
    $request = ['id' => 42];
    $cid = UrCacheProvider::generateId($request, $settings);
    self::assertEquals('id:42|uid:1|GenTest', $cid);

    $cid = UrCacheProvider::generateId([], [], 2);
    self::assertEquals('UrCacheProviderTest|testGenerateId', $cid);
  }

  public function testGetClassCachingSettings() {
    $className = MockCacheablePlugin::class;
    $settings = UrCacheProvider::getClassCachingSettings($className);

    self::assertArrayHasKey('key', $settings);
    self::assertArrayHasKey('patterns', $settings);
    self::assertContains('[id:mock-item-%s]', $settings['patterns']);
  }

  public function testAddTag() {
    $tag = 'Bueller';
    $tagList = ['Ferris', 'Day Off'];

    UrCacheProvider::addTag($tag, $tagList);
    self::assertContains($tag, $tagList);
  }

  public function testGetCallerInfo() {
    $info = UrCacheProvider::getCallerInfo(0);

    self::assertEquals('UrCacheProviderTest', $info['class']);
    self::assertEquals('testGetCallerInfo', $info['method']);
  }

  public function testGetKeyFromFields() {
    $data = [
      'id' => 42,
      'boogers' => 'areGross',
      'name' => 'Goku',
    ];

    $fields = ['id', 'name'];

    $keyString = UrCacheProvider::getKeyFromFields($data, $fields);
    self::assertEquals('id:42|name:Goku', $keyString);
  }

}
