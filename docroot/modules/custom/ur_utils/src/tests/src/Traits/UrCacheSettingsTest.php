<?php

namespace Drupal\ur_utils\tests\Traits;

use Drupal\ur_utils\Helpers\TimeHelper;
use Drupal\ur_utils\Mocks\MockCacheablePlugin;
use Drupal\KernelTests\KernelTestBase;

class UrCacheSettingsTest extends KernelTestBase {

  protected $sut; // System under test

  /**
   * @return MockCacheablePlugin
   */
  public function getTestSystem() {
    if (empty($this->sut)) {
      /** @var MockCacheablePlugin */
      $this->sut = new MockCacheablePlugin();
    }

    return $this->sut;
  }

  public function testGetCacheSettings() {
    /** @var MockCacheablePlugin */
    $testSystem = $this->getTestSystem();
    $settings = $testSystem::getCacheSettings();

    self::assertEquals(TimeHelper::DAY, $settings['ttl']);
    self::assertEquals('mock-test', $settings['key']);
  }

  public function testLoadMockClass() {
    $mock = new MockCacheablePlugin();
    $this->assertInstanceOf(MockCacheablePlugin::class, $mock);
  }

  public function testPatternScan() {
    /** @var MockCacheablePlugin */
    $testSystem = $this->getTestSystem();
    $pattern = '[id:42]';

    $foundPatterns = $testSystem::patternScan($pattern);
    $this->assertArrayHasKey('id', $foundPatterns);
    $this->assertArrayHasKey(42, $foundPatterns['id']);
  }

  public function testPatternListScan() {
    /** @var MockCacheablePlugin */
    $testSystem = $this->getTestSystem();

    $patternList = ['bizarro', 'accounts-zip', '[name:Deadpool]', 'DeadPoolRulez', '[id:42]'];
    $foundPatterns = $testSystem::patternListScan($patternList);

    $this->assertArrayHasKey('name', $foundPatterns);
    $this->assertArrayHasKey('id', $foundPatterns);
    $this->assertArrayHasKey('Deadpool', $foundPatterns['name']);
    $this->assertArrayHasKey('42', $foundPatterns['id']);
  }

  public function testMatchTagPattern() {
    /** @var MockCacheablePlugin */
    $testSystem = $this->getTestSystem();

    $dynamicTags = ['bizarro', 'accounts-zip', '[name:Deadpool]', 'DeadPoolRulez', '[id:42]'];
    $resultingTags = $testSystem::matchTagPattern($dynamicTags);

    $this->assertContains('mocker-Deadpool', $resultingTags);
    $this->assertContains('mock-item-42', $resultingTags);
  }

}
