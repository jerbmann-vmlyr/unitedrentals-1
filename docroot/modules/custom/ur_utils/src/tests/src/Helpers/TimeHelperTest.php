<?php

namespace Drupal\ur_utils\tests\Helpers;

use Drupal\KernelTests\KernelTestBase;
use Drupal\ur_utils\Helpers\TimeHelper;

class TimeHelperTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['system', 'memcache', 'ur_utils'];

  public function testTimeSettings() {
    self::assertEquals(31449600, TimeHelper::YEAR);
    self::assertEquals(2419200, TimeHelper::MONTH);
    self::assertEquals(604800, TimeHelper::WEEK);
    self::assertEquals(86400, TimeHelper::DAY);
    self::assertEquals(3600, TimeHelper::HOUR);
    self::assertEquals(60, TimeHelper::MINUTE);
    self::assertEquals(300, TimeHelper::FIVE_MINUTES);
    self::assertEquals(600, TimeHelper::TEN_MINUTES);
    self::assertEquals(0, TimeHelper::ZERO);
    self::assertEquals(28800, TimeHelper::EIGHT_HOURS);
  }

}
