<?php

namespace Drupal\schema_branch\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_product_name' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_organization_hours",
 *   label = @Translation("Hours"),
 *   description = @Translation("The Hours of operation of the branch."),
 *   name = "openingHours",
 *   group = "schema_organization",
 *   weight = 5,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 * )
 */
class SchemaBranchOrganizationOpeningHours extends SchemaNameBase {
}
