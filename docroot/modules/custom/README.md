# Kelex Custom Modules Overview

_More to come_

* * *

### Custom REST endpoints in Kelex

All internal API endpoints are kept within the `ur_fronted_api` module. This module's only purpose is to be the repository for all the internal API routes we use. When creating a new endpoint define the route in the `ur_frontend_api.routing.yml` file. The namespace used in the `_controller:` option of the route will make sure that your controller's method is the one called. This will allow us to separate our API calls into whatever module we need, but still have 1 central location to see all the internal API calls rather than having to grep or search for where a call comes from.

**Note:** Any module that defines a Controller in the `ur_frontend_api` module will need to add a dependency to `ur_frontend_api` in the `{your_module}.info.yml` file to prevent errors in case for whatever reason the `ur_frontend_api` module is not enabled.  

* * * 

### Custom REST endpoints
This segment aims to show how to create a custom REST endpoint for use within Kelex. A lot of this documentation comes directly from D.O. [RESTful Web Services](https://www.drupal.org/docs/8/core/modules/rest) API as well as from related blogs. 

Although Drupal 8 released with an answer for Web Services, namely the REST modules provided in core, there were a lot of features that either didn't work as intended or were completely missing. With the release of Drupal 8.3, web services got a pretty big update making it more stable and providing more out-of-the-box features. Here's a quote from D.O on the [8.3 release](https://www.drupal.org/blog/drupal-8-3-0):

> Drupal 8.3 continues to expand Drupal's support for web services that benefit decoupled sites and applications, with bug fixes, improved responses, and new features. It is now possible to register users from the REST API, 403 responses now return a reason why access was denied, for greatly improved developer experience, and anonymous REST API performance has been increased by 60% when utilizing the internal page cache. The REST API also got a massive overhaul of its test coverage.

All of this to say that Drupal's answer to Web Services has come a long way and will continue to get better and better as time goes on. The code examples in this segment may become out of date over time but we will make our best effort to keep this document up to date with any changes that are made with future releases. 
 
 
##### Resource Links
- [An Introduction to RESTful Web Services in Drupal 8](https://drupalize.me/blog/201401/introduction-restful-web-services-drupal-8)
- [Custom REST Resources](https://www.drupal.org/docs/8/api/restful-web-services-api/custom-rest-resources)
- [Drupal 8 REST/JSON - Integration with simple javascript application](https://www.droptica.com/blog/drupal-8-restjson-integration-simple-javascript-application/)

##### Basic Steps

Within Core, you can create a REST endpoint using the core modules: RESTful Web Services, REST UI, Serialization, and Views. However, this segment will go over how to create a custom endpoint using a custom module and only a custom module, no extra modules required.

_This will heavily use drupal console for it's scaffolding ability_

1. Create custom module: `drupal generate:module` (skip this if you already have a custom module)
2. Create a new routing file `my_module.routing.yml` within the root of your custom module (if you already have one just add a new route)

```yaml 
my_module_endpoint:
  path: '/v1/my-endpoint'
  methods: [GET]
  defaults:
    _controller: 'Drupal\my_module\Controller\DemoController::get'
  requirements:
    _permission: 'access content'
``` 
Explanation: By requesting GET at `{mylocalsite.dev}/v1/my-endpoint` we are going to receive the data returned by the DemoController executing the get() method

3. Now create the Controller `drupal generate:controller`. Be sure to name the controller the same as what you called it in the routing.yml otherwise you will have to edit/update the routing.yml with whatever you called your Controller.
4. Create our get() method we specified in our routing.yml in the newly created controller at `my_module/src/Controller/DemoController.php`. So your controller should look a little something like this.
```php
<?php

namespace Drupal\my_module\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * An example controller.
 */
class DemoController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function get() {
    // Do your logic to retrieve data here...
    // In this instance we are retrieving some simple site config data
    $response = new JsonResponse();
    $config = \Drupal::config('system.site');
    $data = [
      'date' => time(),
      'site_name' => $config->get('name'),
      'site_email' => $config->get('mail')
    ];

    $response->setData($data);

    return $response;
  }
}
```
5. Enable your module
6. Clear drupal cache, `drush cr` or `drupal cache:rebuild all`
7. Navigate to the path we specified, `/v1/my-endpoint` and you should see some JSON output like this:
```json
{
  "date": 1492179151,
  "site_name": "My Drupal Website",
  "site_email": "carson.oldson@vml.com"
}

```

It's as simple as that! Create a module and/or create a new route. Create a new controller with the specified method and return a JsonResponse object.
  
This is a very basic implementation of Drupal's rest API, but shows the fundamentals of how deliver a RESTful response within Drupal 8. 