<?php

namespace Drupal\ur_project_uptime_pages\EventSubscriber;

use Drupal\Core\Template\Attribute;
use Drupal\node\Entity\Node;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProjectUptimeSocialMediaBlockSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events['social_media.pre_render'][] = ['preRenderSocialMedia', 39];
    return $events;
  }

  /**
   * Event subscriber callback for social media block pre-render event.
   *
   * @param \Drupal\social_media\Event\SocialMediaEvent $event
   */
  public function preRenderSocialMedia($event) {
    // Only perform these alterations for project uptime pages type nodes.
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof Node) {
      if ($node->bundle() == 'project_uptime_pages') {
        $elements = $event->getElement();

        // Create extra elements for social share block for project uptime.
        $elements['print'] = [
          'text' => 'Print',
          'api' => '',
          'attr' => ['class' => new Attribute(['class' => ['print-link']])],
          'img' => '../../../../../themes/custom/urone/icons/print.svg',
        ];

        $elements['copy_link'] = [
          'text' => 'Copy Link',
          'api' => '',
          'attr' => ['class' => new Attribute(['class' => ['copy-link']])],
          'img' => '../../../../../themes/custom/urone/icons/link.svg',
        ];

        $host = \Drupal::request()->getSchemeAndHttpHost();
        $rss_url = $host . '/feeds/latest-project-uptime.xml';
        $elements['rss'] = [
          'text' => 'RSS Feed',
          'api' => new Attribute(['href' => ['value' => $rss_url]]),
          'attr' => ['class' => new Attribute(['class' => ['rss-feed']])],
          'img' => '../../../../../themes/custom/urone/icons/share.svg',
        ];

        $event->setElement($elements);
      }
    }
  }

}
