<?php

namespace Drupal\ur_project_uptime_pages\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ProjectUptimeSubscribeForm.
 *
 * @package Drupal\ur_project_uptime_pages\Form
 */
class ProjectUptimeSubscribeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'project_uptime_subscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = [
      '#type' => 'markup',
      '#markup' => '<a class="button button--style-2 button--filled" href="https://wvw.unitedrentals.com/project-uptime/sign-up?_ga=2.160249021.1032650427.1499706134-2069295564.1485198207" target="_blank">' . $this->t("Subscribe") . '</a>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitForm() method.
  }

}
