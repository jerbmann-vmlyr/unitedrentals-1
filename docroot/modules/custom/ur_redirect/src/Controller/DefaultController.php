<?php

namespace Drupal\ur_redirect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class DefaultController.
 *
 * @package Drupal\ur_redirect\Controller
 */
class DefaultController extends ControllerBase {

  /**
   * Content route call back. This redirects the user to the front-page as ORS did.
   */
  public function content() {

    $user = \Drupal::currentUser();

    if ($user->isAuthenticated()) {
      return new RedirectResponse('/marketplace/equipment');
    }
    else {
      $host = \Drupal::request()->getHost();
      $current_path = \Drupal::service('path.current')->getPath();
      setrawcookie('simplesamlphp_auth_returnto', 'http://' . $host . $current_path, time() + 60 * 60, NULL, NULL, NULL, TRUE);
      return new RedirectResponse('/saml_login');
    }

  }

  // TODO: May need to add urRedirectionCheckAndRedirect() back.
}
