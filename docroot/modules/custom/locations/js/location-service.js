var LocationService = function LocationService() {
  this.inputElement = '';
  this.clearElement = '';
  this.holdPlace = '';
  this.currentTypeFilter = 'All Branches';
  this.placeHasFocus = false;
  this.suggestions = [];
  this.suggestion = {};
  this.busy = false;
  this.originalPlace = '';
  this.selected = null;
  this.service = false;
  this.place = false;
  this.countryRegex = /, United States| United States|, USA| USA| Canada|, Canada/;
  // Regardless of what the linter may tell you, the square brackets [] are very much required
  // to be escaped in this next line, so the linter is disabled for that line.
  this.symbolRegex = /[!$%^&*()_+|~=`{}\[\]":â€œ;â€˜<>?\/@#]/g; // eslint-disable-line
  this.watchingScroll = false;
};

LocationService.prototype.clear = function clear () {
  this.holdPlace = '';
  this.suggestions = [];
  this.selected = null;
  this.inputHasFocus = true;
  this.inputElement.focus();
  this.clearElement.hide();
  this.locationsList.hide();
};

LocationService.prototype.doFocus = function doFocus (state) {
    var this$1 = this;

  setTimeout(function () {
    this$1.placeHasFocus = state;
    if (!state) {
      this$1.suggestions = [];
      if (this$1.holdPlace !== this$1.originalPlace && this$1.holdPlace !== '') {
        this$1.holdPlace = this$1.originalPlace;
      }
    }
  }, 250);
};

LocationService.prototype.handleEnterKey = function handleEnterKey () {
    var this$1 = this;

  if (this.suggestions.length > 0 && !this.busy) {
    if (this.selected === null) {
      this.selected = 1;
    }
    this.setSuggestion(this.suggestions[this.selected - 1]).then(function () {
      var text = jQuery('#main-content .location-finder-container .location-autocomplete ul li:nth-child(' + this$1.selected + ')').text();
      jQuery('#main-content .location-finder-container .location-autocomplete input').val(text);
      this$1.suggestions = [];
      this$1.clear();
    });
  }
};

LocationService.prototype.handleEscapeKey = function handleEscapeKey (text) {
  this.suggestions = [];
  this.holdPlace = text;
  this.clear();
};

LocationService.prototype.handleDownKey = function handleDownKey () {
  if (this.selected === null || this.selected === this.suggestions.length) {
    this.selected = 1;
  }
  else {
    this.selected++;
  }

  jQuery('#main-content .location-finder-container .location-autocomplete ul li').removeClass('selected');
  jQuery('#main-content .location-finder-container .location-autocomplete ul li:nth-child(' + this.selected + ')').addClass('selected');
};

LocationService.prototype.handleUpKey = function handleUpKey () {
  if (this.selected === null || this.selected === 1) {
    this.selected = this.suggestions.length;
  }
  else {
    this.selected--;
  }

  jQuery('#main-content .location-finder-container .location-autocomplete ul li').removeClass('selected');
  jQuery('#main-content .location-finder-container .location-autocomplete ul li:nth-child(' + this.selected + ')').addClass('selected');
};

LocationService.prototype.handleWindowScroll = function handleWindowScroll () {
  this.suggestions = [];
  jQuery('#main-content .location-finder-container .location-autocomplete ul').hide();
  this.watchingScroll = false;
  document.removeEventListener('scroll', this.handleWindowScroll, true);
};

LocationService.prototype.prepareText = function prepareText (text) {
  return text.replace(this.countryRegex, '')
    .replace(RegExp(("(" + (this.holdPlace) + ")"), 'ig'), '<span class="searchText">$1</span>');
};

LocationService.prototype.setSuggestion = function setSuggestion (suggestion) {
    var this$1 = this;

  this.loadService();
  return this.service.getPlaceFromPrediction(suggestion).then(function (togo) {
    this$1.holdPlace = togo.formatted_address.slice(0).replace(this$1.countryRegex, '');
    this$1.originalPlace = this$1.holdPlace;
    this$1.suggestion = suggestion;
    this$1.setPlace(suggestion);
  });
};

LocationService.prototype.showSuggestions = function showSuggestions () {
    var this$1 = this;

  var arrayLength = this.suggestions.length;
  for (var i = 0; i < arrayLength; i++) {
    this$1.locationsList.append('<li>' + this$1.prepareText(this$1.suggestions[i].description) + '</li>');
    this$1.locationsList.show();
    this$1.clearElement.show();
  }
};

LocationService.prototype.getSuggestions = function getSuggestions (text) {
    var this$1 = this;

  return new Promise(function (resolve) {
    this$1.holdPlace = text.replace(this$1.symbolRegex, '');
    if (!this$1.busy && this$1.holdPlace.length > 2) {
      this$1.busy = true;
      this$1.loadService();
      try {
        this$1.service.getPredictionsFromAddress(this$1.holdPlace)
          .then(function (response) {
            this$1.suggestions = response;
            if (!this$1.watchingScroll) {
              this$1.watchingScroll = true;
              document.addEventListener('scroll', this$1.handleWindowScroll, true);
            }
            this$1.busy = false;
            resolve();
          })
          .catch(function (error) {
            this$1.suggestions = [];
            this$1.watchingScroll = false;
            this$1.busy = false;
            resolve();
          });
      }
      catch (error) {
        this$1.suggestions = [];
        this$1.watchingScroll = false;
        this$1.busy = false;
        resolve();
      }
    }
    else {
      this$1.clear();
      resolve();
    }
  });
};

LocationService.prototype.setPlace = function setPlace (prediction) {
    var this$1 = this;

  this.busy = true;
  return new Promise(function (resolve) {
    this$1.service.getPlaceFromPrediction(prediction)
        .then(function (response) {
          this$1.place = response;
          this$1.busy = false;
        })
        .then(resolve);
  })
};

LocationService.prototype.setTypeFilter = function setTypeFilter (type) {
  this.currentTypeFilter = type;
};

LocationService.prototype.loadService = function loadService (inputElement, locationsList, clearElement) {
    if ( inputElement === void 0 ) inputElement = false;
    if ( locationsList === void 0 ) locationsList = false;
    if ( clearElement === void 0 ) clearElement = false;

  if (!this.service && google) {
    this.service = new GooglePlaces();
    this.service.autoCompleteOptions = {
      types: ['(regions)'],
      componentRestrictions: { country: ['us', 'ca', 'pr'] },
    };
  }

  if (locationsList) {
    this.locationsList = locationsList;
  }

  if (inputElement) {
    this.inputElement = locationsList;
  }

  if (clearElement) {
    this.clearElement = clearElement;
  }
};

LocationService.prototype.modifyService = function modifyService (options) {
  this.service.autoCompleteOptions = options;
};

LocationService.prototype.getPlaceFromAddress = function getPlaceFromAddress (address, opts) {
  return this.service.getPlaceFromAddress(address, opts);
};
