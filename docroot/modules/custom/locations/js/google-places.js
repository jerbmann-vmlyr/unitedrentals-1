var GooglePlaces = function GooglePlaces() {
  this.autocompleteService = new google.maps.places.AutocompleteService();
  this.placesService = new google.maps.places.PlacesService(document.createElement('div'));
  this.geo = new google.maps.Geocoder();
  this.geolocateOptions = {
    timeout: 30000,
    enableHighAccuracy: true,
  };
  this.addressKeys = {
    street_number: 'number',
    route: 'street',
    locality: 'city',
    administrative_area_level_1: 'state',
    administrative_area_level_2: 'county',
    administrative_area_level_3: 'township',
    country: 'country',
    postal_code: 'zip',
    postal_code_suffix: 'zipSuffix',
  };
  // this.branchApi = api.branches;
  this.autoCompleteOptions = {};
};

/**
 * Gets a location predictions based on an address string.
 *
 * @param {string} address - Address string to use for lookup
 * @param {obj} opts - autoComplete options
 * @return {promise<array>} - An array of autocomplete predictions
 */
GooglePlaces.prototype.getPredictionsFromAddress = function getPredictionsFromAddress (address, opts) {
    var this$1 = this;

  return new Promise(function (resolve, reject) {
    var options = (opts !== undefined) ? opts : this$1.autoCompleteOptions;
    options.input = GooglePlaces.sanitizeInput(address);
    this$1.autocompleteService.getPlacePredictions(options, function (predictions, status) {
      if (status !== 'OK' || !predictions) {
        reject(new Error(("Failed to get place for: " + address)));
        return;
      }
      resolve(predictions);
    });
  });
};

/**
 * Takes a response object from google places api and returns an address.
 *
 * @param {string} place - A google place object
 * @return {promise<string>} - An address string for search inputs
 */
GooglePlaces.prototype.getAddressFromPlace = function getAddressFromPlace (place) {
    var this$1 = this;

  return new Promise(function (resolve) {
    this$1.getAddressComponentsFromPlace(place)
      .then(function (components) { return resolve(("" + (components.city) + (components.state
      || components.zip ? ', ' : '') + " " + (components.state
      || '') + " " + (components.zip || ''))); });
  });
};

/**
 * Gets a google place object from an address String
 *
 * @param {string} address - Address string to use for lookup
 * @param {array} opts - autoComplete options
 * @return {promise<object>} - A google place object
 */
GooglePlaces.prototype.getPlaceFromAddress = function getPlaceFromAddress (address, opts) {
    var this$1 = this;

  return new Promise(function (resolve, reject) {
    this$1.getPredictionsFromAddress(address, opts)
      .then(function (predictions) {
        // Added most basic fields from https://developers.google.com/places/web-service/details
        var fields = ['address_component', 'adr_address', 'formatted_address', 'geometry', 'icon', 'name',
          'place_id', 'plus_code', 'type', 'url', 'utc_offset', 'vicinity'];
        var placeId = predictions[0].place_id;
        this$1.placesService.getDetails({ placeId: placeId, fields: fields }, function (place, status) {
          if (status !== 'OK') {
            reject(new Error(("Failed to get place for: " + address)));
            return;
          }
          resolve(place);
        });
      })
      .catch(function (e) {
        reject(e);
      });
  });
};

/**
 * Gets a google place from a google prediction
 *
 * @param {object} prediction - a google prediction object
 * @return {promise<object>} - a google place object
 */
GooglePlaces.prototype.getPlaceFromPrediction = function getPlaceFromPrediction (prediction) {
    var this$1 = this;

  return new Promise(function (resolve) {
    var fields = ['address_component', 'adr_address', 'formatted_address', 'geometry', 'icon', 'name',
      'place_id', 'plus_code', 'type', 'url', 'utc_offset', 'vicinity'];
    var placeId = prediction.place_id;
    this$1.placesService.getDetails({ placeId: placeId, fields: fields }, function (place) {
      resolve(place);
    });
  });
};

/**
 * Accepts a set of lat-long coordinates and returns an address string for use
 * in search boxes.
 *
 * @param {object} coords - { latitude, longitude }
 * @return {promise<object>} - A google place object
 */
GooglePlaces.prototype.getPlaceFromLatLong = function getPlaceFromLatLong (coords) {
    var this$1 = this;

  return new Promise(function (resolve) {
    var latLng = new google.maps.LatLng(parseFloat(coords.latitude), parseFloat(coords.longitude));
    this$1.geo.geocode({ latLng: latLng }, function (results, status) {
      if (status !== google.maps.GeocoderStatus.OK) {
        resolve('');
      }

      try {
        // Return the first item in the list
        resolve(results[0]);
      }
      catch (error) {
        resolve('');
      }
    });
  });
};

/**
 * Gets list of nearby branches sorted by distance from the api for a given place
 *
 * @param {object} place - A google place object
 * @return {promise<object>} - A branch object
 */
GooglePlaces.prototype.getNearbyBranchesFromPlace = function getNearbyBranchesFromPlace (place) {
    var this$1 = this;

  return new Promise(function (resolve) {
    var ref = place.geometry.location;
      var lat = ref.lat;
      var lng = ref.lng;
    this$1.branchApi.getNearby({ lat: lat, lng: lng })
      .then(function (ref) {
          var data = ref.data;

        resolve(data.results.results);
      });
  });
};

/**
 * Makes the api call for getting a branch using the lat lng from a place object
 *
 * @param {object} place - A google place object
 * @return {promise<object>} - A branch object
 */
GooglePlaces.prototype.getBranchFromPlace = function getBranchFromPlace (place) {
    var this$1 = this;

  return new Promise(function (resolve) {
    this$1.getNearbyBranchesFromPlace(place)
      .then(function (branches) {
        if (branches.length > 0) {
          this$1.branchApi.getBranchData(branches[0].branchId)
            .then(function (ref) {
                var data = ref.data;

                return resolve(data[0]);
            });
        }
        else {
          resolve(("No branch found for " + place));
        }
      });
  });
};

/**
 * Gets a branch from the api based on an address String
 *
 * @param {string} address - Address string to use for lookup
 * @return {promise<object>} - A branch object
 */
GooglePlaces.prototype.getBranchFromAddress = function getBranchFromAddress (address) {
    var this$1 = this;

  return new Promise(function (resolve) {
    this$1.getPlaceFromAddress(address)
      .then(function (place) {
        resolve(this$1.getBranchFromPlace(place));
      });
  });
};

/**
 * Takes a place object and returns and object with usable address components.
 *
 * @param {array} place - A google place object
 * @return {promise<object>} - An object containing user-friendly address attributes
 */
GooglePlaces.prototype.getAddressComponentsFromPlace = function getAddressComponentsFromPlace (place) {
    var this$1 = this;

  return new Promise(function (resolve) {
    var addressParts = {};

    Promise.all(place.address_components.map(function (part) { return new Promise(function (resolve) {
      if (this$1.addressKeys[part.types[0]] !== undefined) {
        var ap = this$1.addressKeys[part.types[0]];
        addressParts[ap] = part.short_name;
      }
      resolve(true);
    }); }))
      .then(resolve(addressParts));
  });
};

/**
 * Strips extra characters from the zip code for use in google api
 *
 * @param {string} input - The address string to be checked
 * @return {string} - The altered address string
 */
GooglePlaces.sanitizeInput = function sanitizeInput (input) {
  var hasUsZipPlusFour = /(\d{5}-\d{4})/.test(input);

  if (hasUsZipPlusFour) {
    input = input.replace(/(\d{5}-\d{4})/, function (match) { return match.substring(0, 5); });
  }

  // strip out "undefined"
  input = input.replace(/undefined/g, '').trim();

  return input;
};

/**
 * Calculate the distance, in miles, between two LatLng Objects
 *
 * @param {Object} pointA - a latlng object
 * @param {Object} pointB - a latlng object
 * @return {Number} distance in miles between pointA and pointB
 */
GooglePlaces.distanceBetween = function distanceBetween (pointA, pointB) {
  var R = 6378137; // Earth’s mean radius in meter
  var dLat = this.rad(pointB.lat() - pointA.lat());
  var dLong = this.rad(pointB.lng() - pointA.lng());
  var a = (Math.sin(dLat / 2) * Math.sin(dLat / 2))
    + (Math.cos(this.rad(pointA.lat())) * Math.cos(this.rad(pointB.lat()))
    * Math.sin(dLong / 2) * Math.sin(dLong / 2));
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d * 0.000621371; // returns the distance in miles
};

/**
 * Helper for distanceBetween
 * @param {Number} x - a number to convert to radians
 * @return {Number} - return the radians
 */
GooglePlaces.rad = function rad (x) {
  return (x * Math.PI) / 180;
};
