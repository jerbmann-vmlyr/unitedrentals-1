(function($, Drupal) {

  var BranchMarker = window.L.Marker.extend({
    options: {
      branchId: '',
      branchIndex: 0,
    },
  });

  var icons = {
    leafletCluster: {
      iconSize: [40, 40],
      iconAnchor: [20, 40],
    },
    leafletGeneral: {
      iconSize: [20, 30],
      iconAnchor: [10, 25],
    },
    searchLocation: {
      iconUrl: '/themes/custom/urone/images/search-marker.png',
      iconSize: [20, 40],
      iconAnchor: [10, 35],
    },
  };

  /**
   * Returns a function, that, as long as it continues to be invoked, will not
   * be triggered. The function will be called after it stops being called for
   * N milliseconds. If `immediate` is passed, trigger the function on the
   * leading edge, instead of the trailing. Credit David Walsh
   * (https://davidwalsh.name/javascript-debounce-function).
   */
  function debounce(func, wait, immediate) {
    var timeout;

    return function executedFunction() {
      var context = this;
      var args = arguments;

      var later = function() {
        timeout = null;
        if (!immediate) { func.apply(context, args); }
      };

      var callNow = immediate && !timeout;

      clearTimeout(timeout);

      timeout = setTimeout(later, wait);

      if (callNow) { func.apply(context, args); }
    };
  }

  function createCookie(name, value, days) {
    var expires;
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = "; expires=" + date.toGMTString();
    }
    else {
      expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
  }

  function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1,c.length);
      }
      if (c.indexOf(nameEQ) === 0) {
        return c.substring(nameEQ.length,c.length);
      }
    }
    return null;
  }

  function eraseCookie(name) {
    createCookie(name,"",-1);
  }

  var isIE11 = function(start, end, useKm) {
    return !!navigator.userAgent.match(/Trident\/7\./);
  };

  var isMSEdge = function(start, end, useKm) {
    return !!navigator.userAgent.match(/Edge/);
  };

  var isIOS = function(start, end, useKm) {
    return (/iPad|iPhone|iPod/).test(navigator.userAgent) && !window.MSStream;
  };

  var isMobile = function(start, end, useKm) {
    return (/iphone|ipod|android|ie|blackberry|fennec/i).test(navigator.userAgent);
  };

  var isIOS11Pre113 = function(start, end, useKm) {
    return (/ OS 11_0| OS 11_1| OS 11_2/i).test(navigator.userAgent);
  };

  var getState = function(abbr) {
    var array = {
      "AL": "Alabama",
      "AK": "Alaska",
      "AZ": "Arizona",
      "AR": "Arkansas",
      "CA": "California",
      "CO": "Colorado",
      "CT": "Connecticut",
      "DE": "Delaware",
      "DC": "District Of Columbia",
      "FL": "Florida",
      "GA": "Georgia",
      "HI": "Hawaii",
      "ID": "Idaho",
      "IL": "Illinois",
      "IN": "Indiana",
      "IA": "Iowa",
      "KS": "Kansas",
      "KY": "Kentucky",
      "LA": "Louisiana",
      "ME": "Maine",
      "MD": "Maryland",
      "MA": "Massachusetts",
      "MI": "Michigan",
      "MN": "Minnesota",
      "MS": "Mississippi",
      "MO": "Missouri",
      "MT": "Montana",
      "NE": "Nebraska",
      "NV": "Nevada",
      "NH": "New Hampshire",
      "NJ": "New Jersey",
      "NM": "New Mexico",
      "NY": "New York",
      "NC": "North Carolina",
      "ND": "North Dakota",
      "OH": "Ohio",
      "OK": "Oklahoma",
      "OR": "Oregon",
      "PA": "Pennsylvania",
      "PR": "Puerto Rico",
      "RI": "Rhode Island",
      "SC": "South Carolina",
      "SD": "South Dakota",
      "TN": "Tennessee",
      "TX": "Texas",
      "UT": "Utah",
      "VT": "Vermont",
      "VA": "Virginia",
      "WA": "Washington",
      "WV": "West Virginia",
      "WI": "Wisconsin",
      "WY": "Wyoming",
      "AB": "Alberta",
      "BC": "British Columbia",
      "MB": "Manitoba",
      "NB": "New Brunswick",
      "NL": "Newfoundland and Labrador",
      "NS": "Nova Scotia",
      "ON": "Ontario",
      "PE": "Prince Edward Island",
      "QC": "Quebec",
      "SK": "Saskatchewan"
    };

    return array[abbr];
  };

  function capitalize_Words(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  }

  /**
   * Distance function
   *
   * Returns the distance between two coords in miles
   *
   * @param {object} start | starting point in form {lat, lng}
   * @param {object} end | end point in form {lat, lng}
   * @param {bool} useKm | Boolean to use KM instead of Miles
   * @return {number} the distance between the points in miles
   */
  var getDistance = function(start, end, useKm) {

    if (typeof useKm === 'undefined' || useKm === null) {
      useKm = false;
    }
    var R = 6371e3; // earth's radius, in meters
    var lat1 = start.lat * (Math.PI / 180);
    var lat2 = end.lat * (Math.PI / 180);
    var difLat = (start.lat - end.lat) * (Math.PI / 180);
    var difLng = (start.lng - end.lng) * (Math.PI / 180);

    var a = (Math.sin(difLat / 2) * Math.sin(difLat / 2)) + (Math.cos(lat1) * Math.cos(lat2) * Math.sin(difLng / 2) * Math.sin(difLng / 2));
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    var distance = (R * c) * 0.000621371; // meter -> mile

    distance = Number(Math.round(distance * 100) / 100).toString();

    // Convert to km?
    if (useKm) {
      distance *= 1.60934;
    }

    return distance;
  };

  var getSearchPosition = function() {
    var allcookies = document.cookie;
    var cookiearray = allcookies.split(';');
    for(var i = 0; i < cookiearray.length; i++) {
      var name = cookiearray[i].split('=')[0].trim();
      if (name == 'searchPosition') {
        return JSON.parse(cookiearray[i].split('=')[1].trim());
      }
    }
    return {"lat":0,"lng":0};
  };

  var markerClicked = function(map, branches, index) {
    var ref = branches[index];
    var state = ref.state;
    var city = ref.city;
    var branchId = ref.branchId;
    var latitude = ref.latitude;
    var longitude = ref.longitude;
    var primaryBusinessType = ref.primaryBusinessType;
    var bounds = map.getBounds();

    // recenter/zoom map
    if (window.screen.availWidth > 640) {
      map.flyTo(window.L.latLng(latitude, longitude), 14, { animate: true, duration: 0.5 });
    }
    else {
      var paddingTop = 40;
      var paddingBottom = 200;
      var newCenter = new window.L.LatLng(latitude, longitude);
      map.panToOffset(newCenter, { paddingTop: paddingTop, paddingBottom: paddingBottom });
    }

    var stateParam = encodeURI(state.replace(/[^a-zA-Z0-9\-]/g, '-').replace(/[\-]{2,}/g, '-').toLowerCase());
    var cityParam = encodeURI(city.replace(/[^a-zA-Z0-9\-]/g, '-').replace(/[\-]{2,}/g, '-').toLowerCase());
    var businessTypeParam = encodeURI(primaryBusinessType.url.replace(/[^a-zA-Z0-9\-]/g, '-').replace(/[\-]{2,}/g, '-').toLowerCase());
    var branchParam = encodeURI(branchId.replace(/[^a-zA-Z0-9\-]/g, '-').replace(/[\-]{2,}/g, '-').toLowerCase());
    location.href = '/locations/'
      + stateParam + '/'
      + cityParam + '/'
      + businessTypeParam + '/'
      + branchParam;
  };

  var makeBranchCard = function(map, branch) {
    var state = branch.state;
    var city = branch.city;
    var branchId = branch.branchId;
    var latitude = branch.latitude;
    var longitude = branch.longitude;
    var primaryBusinessType = branch.primaryBusinessType;
    var phoneLink = 'tel://' + branch.phone.replace(/(\d\d\d)-(\d\d\d)-(\d\d\d\d)/, '$1.$2.$3');
    var phoneFormatted = branch.phone.replace(/(\d\d\d)-(\d\d\d)-(\d\d\d\d)/, '($1) $2-$3');
    var directionsLink = 'https://www.google.com/maps/dir//United+Rentals,'
      + branch.address1 + ',' + branch.city + ',' + branch.state;
    var dom = '<div class="branch-popup" data-state="'
      + state + '" data-city="' + city
      + '" data-primary-business-type-url="' + primaryBusinessType.url + '" data-branch-id="' + branchId + '">';
    dom += '  <h6 class="small-grey__heading-6">' + Drupal.t('United Rentals') + '</h6>';
    dom += '  <div class="results-content-blocks_title-mile">';
    dom += '    <h4 class="heading-4 branch-name">' + branch.name + '</h4>';
    dom += '    <p class="paragraph small-grey__paragraph">';
    dom += '      <span class="branch-distance"></span>';
    dom += '    </p>';
    dom += '  </div>';
    dom += '  <p class="phone-paragraph">';
    dom += '    <span><a href="' + phoneLink + '">' + phoneFormatted + '</a></span>';
    dom += '  </p>';
    dom += '  <div>';
    dom += '    <span>' + branch.address1 + '</span>';
    dom += '    <span>' + branch.city + ', ' + branch.state + ' ' + branch.zip + '</span>';
    dom += '  </div>';
    dom += '  <h6 class="heading-6 blue__heading-6 directions">';
    dom += '    <a rel="noopener noreferrer" target="_blank" href="' + directionsLink + '"'
      + '         onclick="(function getDirectionsNewWindow(evt) {'
      + '           evt.preventDefault();'
      + '           window.open(\'' + directionsLink + '\', \'_blank\');'
      + '         })(event)">'
      +           Drupal.t('Get Directions')
      +        '</a>';
    dom += '  </h6>';
    dom += '</div>';

    return dom;
  };

  var markerHovered = function(maps, branches, marker) {
    var branch = branches[marker.layer.options.branchIndex];
    // Calculate distance and add to card.
    var branchPosition = {'lat': branch.latitude, 'lng': branch.longitude};
    var branchCard = makeBranchCard(map, branch);
    var popupOptions = {
      offset: [10, 30],
      minWidth: 330,
      maxWidth: 330,
      autoPan: false,
      closeButton: false,
      className: 'hover-card',
      autoPanPadding: [20, 20],
    };

    // Set popup content and open.
    var popup = window.L.responsivePopup(popupOptions).setContent(branchCard);
    popup.on('click', popupClicked);
    marker.layer.bindPopup(popup).openPopup();
    var branchDistance = jQuery('[data-branchid="' + branch.branchId + '"]').data('distance');

    if (!branchDistance) {
      branchDistance = getBranchDistance(branchPosition, branch.countryCode)
    }

    var oldSpan = '<span class="branch-distance"></span>';
    var newSpan = '<span class="branch-distance">' + branchDistance + '</span>';
    var newContent = popup.getContent().replace(oldSpan, newSpan);
    popup.setContent(newContent);

  };

  var popupClicked = function(popup) {
    var state = jQuery(popup).data('state').toString();
    var city = jQuery(popup).data('city').toString();
    var primaryBusinessTypeUrl = jQuery(popup).data('primary-business-type-url').toString();
    var branchId = jQuery(popup).data('branch-id').toString();
    var stateParam = encodeURI(state.replace(/[^a-zA-Z0-9\-]/g, '-').replace(/[\-]{2,}/g, '-').toLowerCase());
    var cityParam = encodeURI(city.replace(/[^a-zA-Z0-9\-]/g, '-').replace(/[\-]{2,}/g, '-').toLowerCase());
    var businessTypeParam = encodeURI(primaryBusinessTypeUrl.replace(/[^a-zA-Z0-9\-]/g, '-').replace(/[\-]{2,}/g, '-').toLowerCase());
    var branchParam = encodeURI(branchId.replace(/[^a-zA-Z0-9\-]/g, '-').replace(/[\-]{2,}/g, '-').toLowerCase());

    location.href = '/locations/'
      + stateParam + '/'
      + cityParam + '/'
      + businessTypeParam + '/'
      + branchParam;

  };

  var updateMarkers = function(branches, filter) {
    var newBranches = [];
    branches.forEach(function (arrayItem) {
      if (arrayItem['businessType']) {
        if (arrayItem['businessType'] === filter) {
          newBranches.push(arrayItem);
        }
      }
    });

    return newBranches;
  };

  var markerCluster;
  var addMarkersToMap = function(map, branches) {
    var markerGroups = [];
    var stateMarkerGroups = [];

    // Create marker cluster.
    markerCluster = window.L.markerClusterGroup({
      showCoverageOnHover: false,
      maxClusterRadius: 60,
      iconCreateFunction: function (cluster) {
        var clusterIcon = window.L.divIcon($.extend({}, icons.leafletCluster, {
          html: '<div class="cluster_icon">' + cluster.getChildCount() + '</div>',
        }));
        return clusterIcon;
      },
    });
    markerCluster.on('mouseover', function(marker) {
      markerHovered(map, branches, marker);
    });
    markerCluster.on('click', function(marker) {
      markerClicked(map, branches, marker.layer.options.branchIndex);
    });
    map.addLayer(markerCluster);

    // Now add the markers.
    if (typeof branches !== 'undefined' && branches.length > 0) {
      for (var i in branches) {
        var branch = branches[i];
        if (branch.latitude === '0' || branch.longitude === '0') {
          return;
        }

        // Create div con.
        var icon = window.L.divIcon($.extend({}, icons.leafletGeneral, {
          html: '<div class="marker_icon marker_'
            + (branch.businessType === 'GR' ? 'general' : 'specialty')
            + '"></div>',
        }));

        // Create branch marker.
        var marker = new BranchMarker(new window.L.LatLng(branch.latitude, branch.longitude), {
          icon: icon,
          branchId: branch.branchId,
          branchIndex: i,
        });

        if (!markerGroups[branch.businessType]) {
          markerGroups[branch.businessType] = [];
        }
        markerGroups[branch.businessType].push(marker);

        if (!stateMarkerGroups[branch.state]) {
          stateMarkerGroups[branch.state] = [];
        }
        stateMarkerGroups[branch.state].push(marker);
      }

      // Creat branch groups.
      var branchGroups = {};
      for(var key in markerGroups) {
        var typeGroup = markerGroups[key];
        branchGroups[key] = new window.L.FeatureGroup.SubGroup(markerCluster, typeGroup);
        branchGroups[key].addTo(map);
      }

      // Create state groups.
      var stateGroups = {};
      for(var key in stateMarkerGroups) {
        var stateGroup = stateMarkerGroups[key];
        stateGroups[key] = new window.L.FeatureGroup.SubGroup(markerCluster, stateGroup);
        stateGroups[key].addTo(map);
      }
    }

    jQuery('.location-map .spinner-overlay').removeClass('active');
  };


  var initMap = function(lat, lng) {
    var zoom = 4;
    var promiseQueue = [];

    if ( $('#map').length > 0 ) {
      var myLatLng = {lat: 39.12330, lng: -94.5923};
      var defaultLatLng = false;
      if (typeof lat !== 'undefined' && typeof lng !== 'undefined') {
        myLatLng = {lat: parseFloat(lat), lng: parseFloat(lng)};
        zoom = 7;
        defaultLatLng = true;
      }
      // Grab branches for lat / lng.
      var branchesEndpoint = '/api/v2/branches';
      var boundsArray = false;

      var pathArray = window.location.pathname.split('/');
      if (pathArray.length < 3) {
        defaultLatLng = true;
      }

      if (!defaultLatLng) {
        var boundsEndpoint = '/api/v2/custom/get-branches';

        if (2 in pathArray) {
          var state = pathArray[2].toUpperCase();
          boundsEndpoint += '?state=' + state;
          zoom = 7;

          if (3 in pathArray) {
            boundsEndpoint += '&city=' + pathArray[3];
            zoom = 11;
          }

          if (5 in pathArray) {
            //boundsEndpoint += '&branch=' + pathArray[5];
            zoom = 14;
          }
        }

        promiseQueue.push($.ajax({
          method: "GET",
          url: boundsEndpoint,
        }).done(function(data) {
          boundsArray = data;
        }));
      }

      return Promise.all(promiseQueue).then(function () {
        // Initialize map.
        var center = {
          lat: parseFloat(myLatLng['lat']),
          lng: parseFloat(myLatLng['lng']),
        };

        var boundsCoords = [];
        if (boundsArray) {
          boundsArray.forEach(function (element, index, array) {
            boundsCoords.push([element.latitude, element.longitude]);
          });
          center = window.L.latLngBounds(boundsCoords).getCenter();
        }

        map = window.L.map('map', {
          center: center,
          zoom: zoom,
          maxZoom: 18,
          zoomControl: false,
          attributionControl: false,
          preferCanvas: false,
        });

        window.L.control.zoom({
          position: 'bottomright',
        }).addTo(map);

        var gridLayer = window.L.gridLayer.googleMutant({
          type: 'roadmap',
          styles: [
            { featureType: 'poi', stylers: [{ visibility: 'off' }] } ],
        });
        gridLayer.addTo(map);

        // Now add the locations.
        $.ajax({
          method: "GET",
          url: branchesEndpoint,
        }).done(function(data) {
          addMarkersToMap(map, data.data);
          var searchIcon = window.L.icon($.extend({}, icons.searchLocation));
          if (typeof $(".location-finder-wrapper").data('lat') !== 'undefined') {
            map.searchMarker = window.L.marker([$(".location-finder-wrapper").data('lat'), $(".location-finder-wrapper").data('lng')], {icon: searchIcon}).addTo(map);
          }
          else if (window._app && window._app.$store) {
            var geolocation = Promise.resolve(window._app.$store.state.user.geolocation);
            geolocation.then(function (result) {
              if (result.coords && result.coords.latitude && result.coords.longitude) {
                map.searchMarker = window.L.marker([result.coords.latitude, result.coords.longitude], {icon: searchIcon}).addTo(map);
              }
              else if (boundsCoords && boundsCoords[0]) {
                map.searchMarker = window.L.marker(boundsCoords[0], {icon: searchIcon}).addTo(map);
              }
              else {
              }
            });
          }
          else {
            map.searchMarker = window.L.marker([myLatLng['lat'], myLatLng['lng']], {icon: searchIcon}).addTo(map);
          }
        });
      });
    }
  };

  
  var getBranchDistance = function(branchPositionLatLng, lang, callback) {

    var currentPositionLatLng = {'lat': map.options.center.lat, 'lng': map.options.center.lng };
    var useKm = false;
    var kmMi = 'mi';
    if (typeof lang !== 'undefined' && lang != 'US') {
      useKm = true;
      kmMi = 'km';
    }
    var distance = getDistance(branchPositionLatLng, currentPositionLatLng, useKm);
    distance = parseFloat(distance).toLocaleString('en') + ' ' + kmMi;
    return distance;
  };

  Drupal.behaviors.locations = {
    attach: function (context, settings) {
      // Element Variables.
      var locationsList = $('.location-autocomplete ul');
      var clearSpan = $('.clearspan');
      var inputEl = $('.location-autocomplete input');
      var spinner = $('.location-map .spinner-overlay');

      window.locationListing = {
        totalResults: $('.view-location-listing .views-row').length,
        resultsPerPage: 20,
        currentPage: 1,
        pagerPrev: function pagerPrev() {
          locationListing.currentPage = Math.max(locationListing.currentPage - 1, 1);
          locationListing.updateResults();
          locationListing.refreshPagerHtml();
        },
        pagerNext: function pagerNext() {
          locationListing.currentPage = Math.min(locationListing.currentPage + 1, locationListing.numberOfPages());
          locationListing.updateResults();
          locationListing.refreshPagerHtml();
        },
        numberOfPages: function numberOfPages() {
          return Math.ceil(locationListing.totalResults / locationListing.resultsPerPage);
        },
        showingResults: function showingResults() {
          var startResult = ((locationListing.currentPage - 1) * locationListing.resultsPerPage) + 1;
          var endResult = Math.min(startResult + (locationListing.resultsPerPage - 1), locationListing.totalResults);

          return startResult + ' - ' + endResult;
        },
        refreshPagerHtml: function refreshPagerHtml() {
          $('.js-pagination').remove();
          var paginationHtml = ''
            + '<div class="js-pagination">'
            + '  <nav role="navigation" aria-labelledby="pagination-heading">'
            + '    <h4 class="visually-hidden">Pagination</h4>'
            + '    <div class="pager__summary">'
            + (( locationListing.totalResults > 0 )
              ? '      <h6 class="heading-6">Showing Results <strong>' + locationListing.showingResults() + '</strong> of ' + locationListing.totalResults + '</h6>'
              : '')
            + '    </div>'
            + (( locationListing.totalResults > 0 )
              ? '    <div class="js-pager__items"></div>'
              : '')
            + '  </nav>';
            + '</div>';
          $('.content-panel_pagination').after(paginationHtml);
          var iconPrev = '<i class="icon icon-before-arrow-left active" onclick="locationListing.pagerPrev()"></i>';
          var iconNext = '<i class="icon icon-before-arrow-right active" onclick="locationListing.pagerNext()"></i>';
          var pageControls = ''
            + '<nav class="page-controls">'
            + ' <div rel="prev">'
            +     (locationListing.currentPage === 1 ? '' : iconPrev)
            + ' </div>'
            + ' <div rel="next">'
            +     (locationListing.currentPage === locationListing.numberOfPages() ? '' : iconNext)
            + ' </div>';
          $(pageControls).appendTo('.js-pagination .js-pager__items');
          var millisToScroll = 400;
          $('.content-panel__content').animate({ scrollTop: (0) }, millisToScroll);
          $('html, body').animate({ scrollTop: $('#block-urone-content').position().top - 50 }, millisToScroll);
        },
        updateResults: function updateResults() {
          var startResult = ((locationListing.currentPage - 1) * locationListing.resultsPerPage);
          var endResult = Math.min(startResult + (locationListing.resultsPerPage - 1), locationListing.totalResults);
          $('.view-location-listing .views-row').prop('hidden', true);
          $('.view-location-listing .views-row').slice(startResult, endResult).prop('hidden', false);
        }
      };

      $('.content-panel_pagination').prop('hidden', true);
      locationListing.refreshPagerHtml();
      locationListing.updateResults();

      // Move pagination to correct area.
      $('[data-entity-id="location_listing"] [rel="prev"]').html('<i class="icon icon-before-arrow-left active"></i>');
      $('[data-entity-id="location_listing"] [rel="next"]').html('<i class="icon icon-before-arrow-right active"></i>');
      $('[data-entity-id="location_listing"] nav').appendTo('.content-panel_pagination');
      // Initialize LocationService.
      var autoComplete = new LocationService();
      autoComplete.loadService(inputEl, locationsList, clearSpan);

      return initMap().then(function () {
        var urlParams = new URLSearchParams(window.location.search);
        var promiseQueue = [];
        if (urlParams.has('type')) {
          $('.results-content-blocks_container .views-exposed-form input').val(urlParams.get('type'));
          $('.results-content-blocks_container .views-exposed-form .form-submit').click();
          spinner.addClass('active');
          var type = $('.ur-menu-group').find('[data-type=' + urlParams.get('type') + ']').data('name');
          $('.ur-menu-group').find('[data-type=' + urlParams.get('type') + ']').parent().append('<i class="icon icon-check"></i>');
          $('.branch-filter-menu-label').text(type);
          autoComplete.setTypeFilter(type);

          var branchesEndpoint = '/api/v2/branches';
          promiseQueue.push($.ajax({
            method: "GET",
            url: branchesEndpoint,
          }).done(function(data) {
            var updatedBranches = data.data;
            if (urlParams.get('type') !== "All Branches") {
              updatedBranches = updateMarkers(data.data, urlParams.get('type'));
            }
            map.removeLayer(markerCluster);
            addMarkersToMap(map, updatedBranches);
          }));
        }

        return Promise.all(promiseQueue).then(function () {

          $(document).ready(function() {
            if ($('.content-panel_pagination').children().length > 1) {
              $('.content-panel_pagination').children(":first").remove();
            }

            $('.pager__summary h6:contains("of 0")').parent().addClass('visually-hidden');
          });

          // Attach click for items in search filter menu.
          $('.ur-menu-list-item, .ur-menu-group-item').once('ur-menu-list-item-click').on('click', function(event) {
            event.stopImmediatePropagation();
            // Remove all icons first.
            $(this).parents('.ur-menu-group').first().find('i.icon-check').remove();
            if ($(this).hasClass('ur-menu-group-item')) {
              $(this).append('<i class="icon icon-check"></i>');
            } else {
              $(this).parent().append('<i class="icon icon-check"></i>');
            }
            $('.branch-filter-menu-label').text($(this).text());
            $('.ur-menu-button').removeClass('active');
            $('.ur-menu-container').removeClass('active');
            $('.ur-menu-wrapper').trigger('focusout');
            $('button.location-search-button').trigger('click');
          });

          // Updated to blur due to IE errors. Would not allow IE selection. UR19-2777
          // Attach click and focus events to search filter menu.
          $('.ur-menu-wrapper').once('ur-menu-wrapper-blur').on('blur', function() {
            $(this).find('.ur-menu-button').removeClass('active');
            $(this).find('.ur-menu-container').removeClass('active');
          });
          
          $('.ur-menu-wrapper').once('ur-menu-button-click').on('click', function() {
            var $button = $(this).find('.ur-menu-button');
            $button.toggleClass('active');
            $button.siblings('.ur-menu-container').toggleClass('active');
          });

          // Hide/Show State List.
          $('.state_result_country_title').once('toggleList').click(function(e) {
            var $element = $(this);
            $(this).toggleClass('closed');
            $(this).parent().find('.results_section').toggleClass('hide');
          });

          // Close all menus.
          $(document).once('document-click').click(function(e) {
            if (!$(e.target).hasClass('ur-menu-button') && !$(e.target).hasClass('branch-filter-menu-label')
              && !$(e.target).hasClass('ur-menu-list-item')
            ) {
              $('.ur-menu-wrapper').trigger('focusout');
            }

            // Hide Suggestions if click outside of input while ul is visible.
            if (!$(e.target).closest('.location-autocomplete').length && $('.location-autocomplete ul').is(":visible")) {
              autoComplete.handleEscapeKey($('.location-autocomplete input').val());
            }
          });

          // Clear Location if clear button clicked.
          $(document).on("click", '.clearspan', function(event) {
            autoComplete.clear();
            inputEl.val('');
          });

          // Clear Location if clear button clicked.
          $(document).on("click", '.rent-online-link', function(event) {
            var this$1 = this;

            window._app.$emit('set-rent-location', $(this).data('address'), function () {
              window.location = $(this$1).data('link');
            });
          });

          // Listen for input on Location autocomplete.
          $('.location-autocomplete input').once('autocomplete').on('input', function() {
            var this$1 = this;

            setTimeout(function () {
              $(autoComplete.locationsList).empty();
              autoComplete.getSuggestions($(this$1).val()).then(function () {
                if (autoComplete.suggestions.length) {
                  autoComplete.showSuggestions();
                }
              });
            }, 250);
          });

          // Set location to clicked item, clear the list of suggestions.
          $(document).on("click", '.location-autocomplete ul li', function(event) {
            inputEl.val($(this).text());
            autoComplete.setSuggestion(autoComplete.suggestions[$(this).index()]);
            autoComplete.clear();
          });

          $(document).ajaxSend(function(event, xhr, settings) {
            if (settings.data !== undefined) {
              if (settings.data.indexOf("view_name=location_listing") >= 0) {
                $('.location-finder-content-panel .spinner-overlay').addClass('active');
              }
            }
          });

          $(document).ajaxSuccess(function(event, xhr, settings) {
            if (settings.data !== undefined) {
              if (settings.data.indexOf("view_name=location_listing") >= 0) {
                $('.content-panel_pagination').children(":first").remove();
                $('.location-finder-content-panel .spinner-overlay').removeClass('active');

                $('.pager__summary h6:contains("of 0")').parent().addClass('visually-hidden');
              }
            }
          });

          // Handle specific keys when pressed in the locations suggestions.
          $(document).once('keyup').on("keyup", '.location-autocomplete', function(event) {
            switch (event.keyCode) {
              case 38:
                autoComplete.handleUpKey();
                break;
              case 40:
                autoComplete.handleDownKey();
                break;
              case 27:
                autoComplete.handleEscapeKey($('.location-autocomplete input').val());
                break;
              case 13:
                autoComplete.handleEnterKey();
                break;
              default:
                break;
            }
          });

          // Handle click of branch pop up card.
          $(document).once('branchclick').on('click', '.branch-popup', function(event) {
            var path = window.location.pathname;
            if (path.split('/').length <= 3) {
              createCookie('locationlist', path);
            }
            popupClicked($(this));
          });

          $('.branch-wrapper .branch-name a').once('branchlistclick').click(function() {
            var path = window.location.pathname;
            if (path.split('/').length <= 3) {
              createCookie('locationlist', path);
            }
          });

          $('.return__heading-6 a').once('returntoresults').click(function(e) {
            e.preventDefault();
            if (readCookie('locationlist') !== null ) {
              var goto = readCookie('locationlist');
              window.location.href = window.location.protocol + "//" + window.location.host + decodeURIComponent(goto);
            }
            else {
              window.history.back();
            }
          });

          $('.branch-wrapper').once('branch-card').click(function(e) {
            if (e.target.tagName.toLowerCase() === 'a') {
              return;
            }
            e.preventDefault();
            var href = $(this).find('.branch-name a').attr("href");
            window.location = href;
          });

          // Handle search for location/branch type changes.
          $(document).once('locations-search').on("click", '.location-search-button', function(event) {
            var path = '';
            var checkEndpoint = '/api/v2/custom/check-endpoint';
            var type = $('.branch-filter-menu-label').text();
            var abbr = $('.ur-menu-group .icon-check').prev().data('type');
            var promiseQueue = [];
            if (type !== autoComplete.currentTypeFilter) {
              spinner.addClass('active');
              if (autoComplete.place) {
                var lat = autoComplete.place['geometry']['location'].lat();
                var lng = autoComplete.place['geometry']['location'].lng();
                var s = false;
                var c = false;
                var cc = false;
                if (autoComplete.place['address_components'].length > 2) {
                  $.each(autoComplete.place['address_components'], function(key, value) {
                    if (value['types'][0] === 'locality') {
                      c = decodeURIComponent(value['long_name'].toLowerCase()).replace(/ /g, '-').replace(/[^a-zA-Z-]/gi, "");
                    }
                    else if (value['types'][0] === 'administrative_area_level_3' && !c) {
                      c = decodeURIComponent(value['long_name'].toLowerCase()).replace(/ /g, '-').replace(/[^a-zA-Z-]/gi, "");
                    }
                    else if (value['types'][0] === 'administrative_area_level_1') {
                      s = value['short_name'];
                    }
                    else if (value['types'][0] === 'country') {
                      cc = value['short_name'];
                    }
                  });

                  path = "/locations/" + s.toLowerCase() + '/' + c;
                  if (abbr !== undefined && abbr !== 'All Branches') {
                    path += '?type=' + abbr;
                  }
                  checkEndpoint += '?state=' + s;
                  if (c) {
                    checkEndpoint += '&city=' + c;
                  }
                  if (cc) {
                    checkEndpoint += '&countrycode=' + cc;
                  }
                }
                else {
                  var s$1 = autoComplete.place['address_components'][0]['short_name'].toLowerCase();
                  var cc$1 = autoComplete.place['address_components'][1]['short_name'].toLowerCase();
                  path = "/locations/" + s$1;
                  if (abbr !== undefined && abbr !== 'All Branches') {
                    path += '?type=' + abbr;
                  }
                  checkEndpoint += '?state=' + s$1 + '&countrycode=' + cc$1;
                }
                checkEndpoint += '&lat=' + lat + '&lng=' + lng;

                promiseQueue.push($.ajax({
                  method: "GET",
                  url: checkEndpoint,
                }).done(function(data) {
                  window.location.href = path;
                }));
              }
              else {
                autoComplete.setTypeFilter(type);
                var branchesEndpoint = '/api/v2/branches';
                if (type !== "All Branches") {
                  $('.results-content-blocks_container .views-exposed-form input').val(abbr);
                }
                else {
                  $('.results-content-blocks_container .views-exposed-form input').val('');
                }
                $('.results-content-blocks_container .views-exposed-form .form-submit').click();

                promiseQueue.push($.ajax({
                  method: "GET",
                  url: branchesEndpoint,
                }).done(function (data) {
                  var updatedBranches = data.data;
                  if (type !== "All Branches") {
                    updatedBranches = updateMarkers(data.data, abbr);
                  }
                  map.removeLayer(markerCluster);
                  addMarkersToMap(map, updatedBranches);
                }));
              }
            }
            else {
              if (autoComplete.place) {
                spinner.addClass('active');
                var abbr$1 = $('.ur-menu-group .icon-check').prev().data('type');
                var lat$1 = autoComplete.place['geometry']['location'].lat();
                var lng$1 = autoComplete.place['geometry']['location'].lng();
                var checkEndpoint$1 = '/api/v2/custom/check-endpoint';
                var s$2 = false;
                var c$1 = false;
                var cc$2 = false;

                $.each(autoComplete.place['address_components'], function(key, value) {
                  if (value['types'][0] === 'locality') {
                    c$1 = decodeURIComponent(value['long_name'].toLowerCase()).replace(/ /g, '-').replace(/[^a-zA-Z-]/gi, "");
                  }
                  else if (value['types'][0] === 'administrative_area_level_3' && !c$1) {
                    c$1 = decodeURIComponent(value['long_name'].toLowerCase()).replace(/ /g, '-').replace(/[^a-zA-Z-]/gi, "");
                  }
                  else if (value['types'][0] === 'administrative_area_level_1') {
                    s$2 = value['short_name'];
                  }
                  else if (value['types'][0] === 'country') {
                    cc$2 = value['short_name'];
                  }
                });

                if (s$2) {
                  path = "/locations/" + s$2.toLowerCase();
                  checkEndpoint$1 += '?state=' + s$2;

                  if (c$1) {
                    path += '/' + c$1;
                    checkEndpoint$1 += '&city=' + c$1;
                  }

                  if (cc$2) {
                    checkEndpoint$1 += '&countrycode=' + cc$2;
                  }

                  checkEndpoint$1 += '&lat=' + lat$1 + '&lng=' + lng$1;

                  if (abbr$1 !== 'All Branches' && abbr$1 !== undefined) {
                    path += '?type=' + abbr$1;
                  }
                }

                promiseQueue.push($.ajax({
                  method: "GET",
                  url: checkEndpoint$1,
                }).done(function(data) {
                  window.location.href = path;
                }));
              }
            }

            return Promise.all(promiseQueue);
          });
        });
      });
    }
  };

})(jQuery, Drupal);
