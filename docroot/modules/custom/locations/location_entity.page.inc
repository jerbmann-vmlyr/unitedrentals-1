<?php

/**
 * @file
 * Contains location_entity.page.inc.
 *
 * Page callback for Location entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\ur_api_dataservice\Plugins\BranchPlugin;

/**
 * Prepares variables for Location templates.
 *
 * Default template: location_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_location_entity(array &$variables) {
  // Fetch LocationEntity Entity Object.
  $location_entity = $variables['elements']['#location_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  $library = isset($_ENV['AH_SITE_ENVIRONMENT']) ? 'locations/main' : 'locations/local';

  // Attach library.
  $variables['#attached']['library'][] = $library;
  $variables['#attached']['library'][] = 'urone/location-entity--full';

  // Make sure it's a branch first.
  $variables['locationEntityType'] = $location_entity->get('type')->target_id;
  $func = 'location_entity_preprocess__' . $location_entity->get('type')->target_id;
  if (function_exists($func)) {
    $func($variables, $location_entity);
  }
}

/**
 * Preprocess branch.
 */
function location_entity_preprocess__branch(array &$variables, $location_entity) {
  // Set lat / lng.
  $variables['branchLatitude'] = $location_entity->get('field_latitude')->value;
  $variables['branchLongitude'] = $location_entity->get('field_longitude')->value;

  // Extra helper variables.
  $variables['ur'] = t('United Rentals');

  $branchTypes = [
    'AR' => 'Aerial Equipment',
    'FS' => 'Fluid Solutions',
    'GR' => 'General Equipment & Tools',
    'IR' => 'Industrial',
    'PR' => 'Power & HVAC',
    'SS' => 'Reliable Onsite Services',
    'OS' => 'Reliable Onsite Services',
    'TR' => 'Trench Safety',
    'TS' => 'Trench Safety',
  ];

  $variables['branchName'] = $location_entity->getName();
  $variables['branchType'] = $location_entity->get('field_business_type')->value;
  $variables['branchTypeLong'] = $branchTypes[$variables['branchType']];
  $variables['branchId'] = $location_entity->get('field_branch_id')->value;

  $address1 = $location_entity->get('field_address')->address_line1;
  $variables['address1'] = $address1;
  $address2 = $location_entity->get('field_address')->address_line2;
  $variables['address2'] = $address2;

  $city = $location_entity->get('field_address')->locality;
  $variables['city'] = $city;

  $state = $location_entity->get('field_address')->administrative_area;
  $variables['state'] = $state;

  $zipcode = $location_entity->get('field_address')->postal_code;
  $variables['zip'] = $zipcode;

  if (!$location_entity->get('field_address_override')->isEmpty()) {
    $address1 = $location_entity->get('field_address_override')->address_line1;
    $variables['address1'] = $address1;
    $address2 = $location_entity->get('field_address_override')->address_line2;
    $variables['address2'] = $address2;

    $city = $location_entity->get('field_address_override')->locality;
    $variables['city'] = $city;

    $state = $location_entity->get('field_address_override')->administrative_area;
    $variables['state'] = $state;

    $zipcode = $location_entity->get('field_address_override')->postal_code;
    $variables['zip'] = $zipcode;
  }

  // Set country code.
  $variables['countryCode'] = $location_entity->get('field_address')->country_code;

  $variables['directionsLink'] = "https://www.google.com/maps/dir//United+Rentals,{$address1},{$city},{$state}";

  $variables['managerEmail'] = $location_entity->get('field_manager_email')->value;
  $variables['managerName'] = $location_entity->get('field_manager_name')->value;

  $variables['phone'] = $location_entity->get('field_phone')->value;
  $variables['phoneLink'] = 'tel://' . str_replace('-', '.', $variables['phone']);
  $variables['fax'] = $location_entity->get('field_fax')->value;

  // Grab video url.
  $variables['videoUrl'] = $location_entity->get('field_video_url')->value;

  // Grab web description and web content block.
  $variables['webDescription'] = $location_entity->get('field_web_description')->value;
  $variables['webContentBlock'] = $location_entity->get('field_web_content_block')->value;

  // Set branch level content blocks.
  $variables['branchContentBlocks'] = [];
  if (!empty($location_entity->get('field_content_blocks'))) {
    foreach ($location_entity->get('field_content_blocks')->getValue() as $row) {
      $id = current($row);
      $entity = \Drupal::entityTypeManager()->getStorage('paragraph')->load($id);
      if (!empty($entity)) {
        $view = entity_view($entity, 'branch');
        $render = render($view);
        $variables['branchContentBlocks'][] = $render;
      }
    }
  }

  // Grab nearby branches.
  $variables['nearbyBranches'] = [];
  $branch_geo = [
    'latitude' => $location_entity->get('field_latitude')->value,
    'longitude' => $location_entity->get('field_longitude')->value,
  ];
  $nearby_branches = BranchPlugin::getNearbyBranches($branch_geo);
  if (!empty($nearby_branches)) {
    // Limit to 4 branches.
    for ($cnt = 0; count($variables['nearbyBranches']) < 4 && $cnt < count($nearby_branches); $cnt++) {
      $nearby_branch = $nearby_branches[$cnt];
      // Skip if it's the current branch.
      if ($variables['branchId'] == $nearby_branch->id) {
        continue;
      }
      $branch_entity = \Drupal::entityTypeManager()->getStorage('location_entity')->loadByProperties([
        'type' => 'branch',
        'field_branch_id' => $nearby_branch->id,
      ]);
      if (!empty($branch_entity)) {
        $branch_entity = array_pop($branch_entity);
        // Make branch object.
        if ($branch_entity->get('field_web_display')->value) {
          $lat = $branch_entity->get('field_geolocation')->getValue()[0]['lat'];
          $lng = $branch_entity->get('field_geolocation')->getValue()[0]['lng'];

          if ($branch_entity->get('field_address')->country_code === 'US') {
            $distance = round(_distance($lat, $lng, $branch_geo['latitude'], $branch_geo['longitude'], 'M'), 2) . " mi";
          }
          else {
            $distance = round(_distance($lat, $lng, $branch_geo['latitude'], $branch_geo['longitude'], 'K'), 2) . ' km';
          }
          $branch = new \stdClass();
          $branch->distance = $distance;
          $branch->branchId = $branch_entity->get('field_branch_id')->value;
          $branch->businessType = $branch_entity->get('field_business_type')->value;
          $branch->name = $branch_entity->getName();
          $branch->primaryBusinessType = $branch_entity->get('field_business_type')->value;

          $branch->address1 = $branch_entity->get('field_address')->address_line1;
          $branch->address2 = $branch_entity->get('field_address')->address_line2;
          $branch->city = $branch_entity->get('field_address')->locality;
          $branch->state = $branch_entity->get('field_address')->administrative_area;
          $branch->zip = $branch_entity->get('field_address')->postal_code;

          if (!$branch_entity->get('field_address_override')->isEmpty()) {
            $branch->address1 = $branch_entity->get('field_address_override')->address_line1;
            $branch->address2 = $branch_entity->get('field_address_override')->address_line2;
            $branch->city = $branch_entity->get('field_address_override')->locality;
            $branch->state = $branch_entity->get('field_address_override')->administrative_area;
            $branch->zip = $branch_entity->get('field_address_override')->postal_code;
          }

          $branch->phone = $branch_entity->get('field_phone')->value;
          $branch->phoneLink = 'tel://' . str_replace('-', '.', $branch->phone);
          $branch->directionsLink = "https://www.google.com/maps/dir//United+Rentals,{$branch->address1},{$branch->city},{$branch->state}";
          $branch->branchLink = \Drupal::service('path.alias_manager')->getAliasByPath('/location_entity/' . $branch_entity->id());
          $variables['nearbyBranches'][] = $branch;
        }
      }
    }
  }

  // Set global and branch type content blocks.
  $location_entity->branchId = $location_entity->get('field_branch_id')->value;
  $location_entity->businessType = $location_entity->get('field_business_type')->value;
  BranchPlugin::setGlobalContentBlocks($location_entity);
  if (isset($location_entity->globalContentBlocks)) {
    $variables['globalContentBlocks'] = $location_entity->globalContentBlocks;
  }
  BranchPlugin::setBranchTypeFields($location_entity);
  if (isset($location_entity->branchTypeContentBlocks)) {
    $variables['branchTypeContentBlocks'] = $location_entity->branchTypeContentBlocks;
  }
  if (isset($location_entity->branchTypeEquipmentCategoryLinks)) {
    $variables['branchTypeEquipmentCategoryLinks'] = $location_entity->branchTypeEquipmentCategoryLinks;
  }

  // Set location branches taxonomy in drupalSettings.
  $location_branches = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'location_branches']);
  foreach ($location_branches as $location_branch) {
    $branch_code = $location_branch->get('field_branch_code')->value;
    $contact_us_link = $location_branch->get('field_branch_contact_us_link')[0];
    $rent_online_link = $location_branch->get('field_branch_rent_online_link')[0];

    $variables['location_branches'][$branch_code]['branch_name'] = $location_branch->get('name')->value;
    if ($contact_us_link) {
      $variables['location_branches'][$branch_code]['contact_us_link'] = Url::fromUri($contact_us_link->uri)->toString();
    }
    else {
      $variables['location_branches'][$branch_code]['contact_us_link'] = '/customer-care/contact-us';
    }
    if ($rent_online_link) {
      $variables['location_branches'][$branch_code]['rent_online_link'] = Url::fromUri($rent_online_link->uri)->toString();
    }
    else {
      $variables['location_branches'][$branch_code]['rent_online_link'] = '/marketplace/equipment';
    }
    $variables['location_branches'][$branch_code]['contact_us_link_show'] = $location_branch->get('field_branch_contact_us_showhide')->value;
    $variables['location_branches'][$branch_code]['rent_online_link_show'] = $location_branch->get('field_branch_rentonline_showhide')->value;
  }

  // Show buttons?
  $variables['contactUsLinkShow'] = 0;
  if (isset($variables['location_branches'][$variables['branchType']]['contact_us_link_show'])) {
    $variables['contactUsLinkShow'] = $variables['location_branches'][$variables['branchType']]['contact_us_link_show'];
    $variables['contactUsLink'] = $variables['location_branches'][$variables['branchType']]['contact_us_link'];
  }

  $variables['rentOnlineLinkShow'] = 0;
  if (isset($variables['location_branches'][$variables['branchType']]['rent_online_link_show'])) {
    $variables['rentOnlineLinkShow'] = $variables['location_branches'][$variables['branchType']]['rent_online_link_show'];
    $variables['rentOnlineLink'] = $variables['location_branches'][$variables['branchType']]['rent_online_link'];
  }

  // Build hours.
  $count = count($location_entity->get('field_branch_office_hours')->getValue());
  $variables['todayHours'] = FALSE;

  for ($x = 0; $x < $count; $x++) {

    $day = $location_entity->get('field_branch_office_hours')->getValue()[$x]['day'];

    switch ($day) {
      case 0:
        $variables['sundayHours'] = FALSE;
        $sundayHoursOpen = $location_entity->get('field_branch_office_hours')->getValue()[$x]['starthours'];
        $sundayHoursClose = $location_entity->get('field_branch_office_hours')->getValue()[$x]['endhours'];
        if ($sundayHoursOpen != '') {
          $variables['sundayHours'] = TRUE;

          // Now determine if open.
          if (date('N') == 7 && date('H') >= ($sundayHoursOpen / 100) && date('H') < ($sundayHoursClose / 100)) {
            $variables['isOpen'] = TRUE;
          }

          if ($sundayHoursOpen < 1200) {
            if (strlen($sundayHoursOpen) == 3) {
              $sundayHoursOpen = substr_replace($sundayHoursOpen, ':', 1, 0);
            }
            else {
              $sundayHoursOpen = substr_replace($sundayHoursOpen, ':', 2, 0);
            }
            $sundayHoursOpen = $sundayHoursOpen . "AM";
          }
          else {
            if ($sundayHoursOpen == 1200 || $sundayHoursOpen == 1230) {
              $sundayHoursOpen = substr_replace($sundayHoursOpen, ':', 2, 0);
            }
            else {
              $sundayHoursOpen = $sundayHoursOpen - 1200;
              $sundayHoursOpen = substr_replace($sundayHoursOpen, ':', 1, 0);
            }
            $sundayHoursOpen = $sundayHoursOpen . "PM";
          }

          $variables['sundayHoursOpen'] = date('g:ia', strtotime($sundayHoursOpen));

          if ($sundayHoursClose < 1200) {
            if (strlen($sundayHoursClose) == 3) {
              $sundayHoursClose = substr_replace($sundayHoursClose, ':', 1, 0);
            }
            else {
              $sundayHoursClose = substr_replace($sundayHoursClose, ':', 2, 0);
            }
            $sundayHoursClose = $sundayHoursClose . "AM";
          }
          else {
            if ($sundayHoursClose == 1200 || $sundayHoursClose == 1230) {
              $sundayHoursClose = substr_replace($sundayHoursClose, ':', 2, 0);
            }
            else {
              $sundayHoursClose = $sundayHoursClose - 1200;
              $sundayHoursClose = substr_replace($sundayHoursClose, ':', 1, 0);
            }
            $sundayHoursClose = $sundayHoursClose . "PM";
          }

          $variables['sundayHoursClose'] = date('g:ia', strtotime($sundayHoursClose));

          if (date('N') == 7) {
            $variables['today'] = "Sunday";
            $variables['todayHours'] = TRUE;
            $variables['todayOpen'] = $variables['sundayHoursOpen'];
            $variables['todayClose'] = $variables['sundayHoursClose'];
          }
        }
        break;

      case 1:
        $variables['mondayHours'] = FALSE;
        $mondayHoursOpen = $location_entity->get('field_branch_office_hours')->getValue()[$x]['starthours'];
        $mondayHoursClose = $location_entity->get('field_branch_office_hours')->getValue()[$x]['endhours'];
        if ($mondayHoursOpen != '') {
          $variables['mondayHours'] = TRUE;

          // Now determine if open.
          if (date('N') == 1 && date('H') >= ($mondayHoursOpen / 100) && date('H') < ($mondayHoursClose / 100)) {
            $variables['isOpen'] = TRUE;
          }

          if ($mondayHoursOpen < 1200) {
            if (strlen($mondayHoursOpen) == 3) {
              $mondayHoursOpen = substr_replace($mondayHoursOpen, ':', 1, 0);
            }
            else {
              $mondayHoursOpen = substr_replace($mondayHoursOpen, ':', 2, 0);
            }
            $mondayHoursOpen = $mondayHoursOpen . "AM";
          }
          else {
            if ($mondayHoursOpen == 1200 || $mondayHoursOpen == 1230) {
              $mondayHoursOpen = substr_replace($mondayHoursOpen, ':', 2, 0);
            }
            else {
              $mondayHoursOpen = $mondayHoursOpen - 1200;
              $mondayHoursOpen = substr_replace($mondayHoursOpen, ':', 1, 0);
            }
            $mondayHoursOpen = $mondayHoursOpen . "PM";
          }

          $variables['mondayHoursOpen'] = date('g:ia', strtotime($mondayHoursOpen));

          if ($mondayHoursClose < 1200) {
            if (strlen($mondayHoursClose) == 3) {
              $mondayHoursClose = substr_replace($mondayHoursClose, ':', 1, 0);
            }
            else {
              $mondayHoursClose = substr_replace($mondayHoursClose, ':', 2, 0);
            }
            $mondayHoursClose = $mondayHoursClose . "AM";
          }
          else {
            if ($mondayHoursClose == 1200 || $mondayHoursClose == 1230) {
              $mondayHoursClose = substr_replace($mondayHoursClose, ':', 2, 0);
            }
            else {
              $mondayHoursClose = $mondayHoursClose - 1200;
              $mondayHoursClose = substr_replace($mondayHoursClose, ':', 1, 0);
            }
            $mondayHoursClose = $mondayHoursClose . "PM";
          }

          $variables['mondayHoursClose'] = date('g:ia', strtotime($mondayHoursClose));

          if (date('N') == 1) {
            $variables['today'] = "Monday";
            $variables['todayHours'] = TRUE;
            $variables['todayOpen'] = $variables['mondayHoursOpen'];
            $variables['todayClose'] = $variables['mondayHoursClose'];
          }
        }
        break;

      case 2:
        $variables['tuesdayHours'] = FALSE;
        $tuesdayHoursOpen = $location_entity->get('field_branch_office_hours')->getValue()[$x]['starthours'];
        $tuesdayHoursClose = $location_entity->get('field_branch_office_hours')->getValue()[$x]['endhours'];
        if ($tuesdayHoursOpen != '') {
          $variables['tuesdayHours'] = TRUE;

          // Now determine if open.
          if (date('N') == 2 && date('H') >= ($tuesdayHoursOpen / 100) && date('H') < ($tuesdayHoursClose / 100)) {
            $variables['isOpen'] = TRUE;
          }

          if ($tuesdayHoursOpen < 1200) {
            if (strlen($tuesdayHoursOpen) == 3) {
              $tuesdayHoursOpen = substr_replace($tuesdayHoursOpen, ':', 1, 0);
            }
            else {
              $tuesdayHoursOpen = substr_replace($tuesdayHoursOpen, ':', 2, 0);
            }
            $tuesdayHoursOpen = $tuesdayHoursOpen . "AM";
          }
          else {
            if ($tuesdayHoursOpen == 1200 || $tuesdayHoursOpen == 1230) {
              $tuesdayHoursOpen = substr_replace($tuesdayHoursOpen, ':', 2, 0);
            }
            else {
              $tuesdayHoursOpen = $tuesdayHoursOpen - 1200;
              $tuesdayHoursOpen = substr_replace($tuesdayHoursOpen, ':', 1, 0);
            }
            $tuesdayHoursOpen = $tuesdayHoursOpen . "PM";
          }

          $variables['tuesdayHoursOpen'] = date('g:ia', strtotime($tuesdayHoursOpen));

          if ($tuesdayHoursClose < 1200) {
            if (strlen($tuesdayHoursClose) == 3) {
              $tuesdayHoursClose = substr_replace($tuesdayHoursClose, ':', 1, 0);
            }
            else {
              $tuesdayHoursClose = substr_replace($tuesdayHoursClose, ':', 2, 0);
            }
            $tuesdayHoursClose = $tuesdayHoursClose . "AM";
          }
          else {
            if ($tuesdayHoursClose == 1200 || $tuesdayHoursClose == 1230) {
              $tuesdayHoursClose = substr_replace($tuesdayHoursClose, ':', 2, 0);
            }
            else {
              $tuesdayHoursClose = $tuesdayHoursClose - 1200;
              $tuesdayHoursClose = substr_replace($tuesdayHoursClose, ':', 1, 0);
            }
            $tuesdayHoursClose = $tuesdayHoursClose . "PM";
          }

          $variables['tuesdayHoursClose'] = date('g:ia', strtotime($tuesdayHoursClose));

          if (date('N') == 2) {
            $variables['today'] = "Tuesday";
            $variables['todayHours'] = TRUE;
            $variables['todayOpen'] = $variables['tuesdayHoursOpen'];
            $variables['todayClose'] = $variables['tuesdayHoursClose'];
          }
        }
        break;

      case 3:
        $variables['wednesdayHours'] = FALSE;
        $wednesdayHoursOpen = $location_entity->get('field_branch_office_hours')->getValue()[$x]['starthours'];
        $wednesdayHoursClose = $location_entity->get('field_branch_office_hours')->getValue()[$x]['endhours'];
        if ($wednesdayHoursOpen != '') {
          $variables['wednesdayHours'] = TRUE;

          // Now determine if open.
          if (date('N') == 3 && date('H') >= ($wednesdayHoursOpen / 100) && date('H') < ($wednesdayHoursClose / 100)) {
            $variables['isOpen'] = TRUE;
          }

          if ($wednesdayHoursOpen < 1200) {
            if (strlen($wednesdayHoursOpen) == 3) {
              $wednesdayHoursOpen = substr_replace($wednesdayHoursOpen, ':', 1, 0);
            }
            else {
              $wednesdayHoursOpen = substr_replace($wednesdayHoursOpen, ':', 2, 0);
            }
            $wednesdayHoursOpen = $wednesdayHoursOpen . "AM";
          }
          else {
            if ($wednesdayHoursOpen == 1200 || $wednesdayHoursOpen == 1230) {
              $wednesdayHoursOpen = substr_replace($wednesdayHoursOpen, ':', 2, 0);
            }
            else {
              $wednesdayHoursOpen = $wednesdayHoursOpen - 1200;
              $wednesdayHoursOpen = substr_replace($wednesdayHoursOpen, ':', 1, 0);
            }
            $wednesdayHoursOpen = $wednesdayHoursOpen . "PM";
          }

          $variables['wednesdayHoursOpen'] = date('g:ia', strtotime($wednesdayHoursOpen));

          if ($wednesdayHoursClose < 1200) {
            if (strlen($wednesdayHoursClose) == 3) {
              $wednesdayHoursClose = substr_replace($wednesdayHoursClose, ':', 1, 0);
            }
            else {
              $wednesdayHoursClose = substr_replace($wednesdayHoursClose, ':', 2, 0);
            }
            $wednesdayHoursClose = $wednesdayHoursClose . "AM";
          }
          else {
            if ($wednesdayHoursClose == 1200 || $wednesdayHoursClose == 1230) {
              $wednesdayHoursClose = substr_replace($wednesdayHoursClose, ':', 2, 0);
            }
            else {
              $wednesdayHoursClose = $wednesdayHoursClose - 1200;
              $wednesdayHoursClose = substr_replace($wednesdayHoursClose, ':', 1, 0);
            }
            $wednesdayHoursClose = $wednesdayHoursClose . "PM";
          }

          $variables['wednesdayHoursClose'] = date('g:ia', strtotime($wednesdayHoursClose));

          if (date('N') == 3) {
            $variables['today'] = "Wednesday";
            $variables['todayHours'] = TRUE;
            $variables['todayOpen'] = $variables['wednesdayHoursOpen'];
            $variables['todayClose'] = $variables['wednesdayHoursClose'];
          }
        }
        break;

      case 4:
        $variables['thursdayHours'] = FALSE;
        $thursdayHoursOpen = $location_entity->get('field_branch_office_hours')->getValue()[$x]['starthours'];
        $thursdayHoursClose = $location_entity->get('field_branch_office_hours')->getValue()[$x]['endhours'];
        if ($thursdayHoursOpen != '') {
          $variables['thursdayHours'] = TRUE;

          // Now determine if open.
          if (date('N') == 4 && date('H') >= ($thursdayHoursOpen / 100) && date('H') < ($thursdayHoursClose / 100)) {
            $variables['isOpen'] = TRUE;
          }

          if ($thursdayHoursOpen < 1200) {
            if (strlen($thursdayHoursOpen) == 3) {
              $thursdayHoursOpen = substr_replace($thursdayHoursOpen, ':', 1, 0);
            }
            else {
              $thursdayHoursOpen = substr_replace($thursdayHoursOpen, ':', 2, 0);
            }
            $thursdayHoursOpen = $thursdayHoursOpen . "AM";
          }
          else {
            if ($thursdayHoursOpen == 1200 || $thursdayHoursOpen == 1230) {
              $thursdayHoursOpen = substr_replace($thursdayHoursOpen, ':', 2, 0);
            }
            else {
              $thursdayHoursOpen = $thursdayHoursOpen - 1200;
              $thursdayHoursOpen = substr_replace($thursdayHoursOpen, ':', 1, 0);
            }
            $thursdayHoursOpen = $thursdayHoursOpen . "PM";
          }

          $variables['thursdayHoursOpen'] = date('g:ia', strtotime($thursdayHoursOpen));

          if ($thursdayHoursClose < 1200) {
            if (strlen($thursdayHoursClose) == 3) {
              $thursdayHoursClose = substr_replace($thursdayHoursClose, ':', 1, 0);
            }
            else {
              $thursdayHoursClose = substr_replace($thursdayHoursClose, ':', 2, 0);
            }
            $thursdayHoursClose = $thursdayHoursClose . "AM";
          }
          else {
            if ($thursdayHoursClose == 1200 || $thursdayHoursClose == 1230) {
              $thursdayHoursClose = substr_replace($thursdayHoursClose, ':', 2, 0);
            }
            else {
              $thursdayHoursClose = $thursdayHoursClose - 1200;
              $thursdayHoursClose = substr_replace($thursdayHoursClose, ':', 1, 0);
            }
            $thursdayHoursClose = $thursdayHoursClose . "PM";
          }

          $variables['thursdayHoursClose'] = date('g:ia', strtotime($thursdayHoursClose));

          if (date('N') == 4) {
            $variables['today'] = "Thursday";
            $variables['todayHours'] = TRUE;
            $variables['todayOpen'] = $variables['thursdayHoursOpen'];
            $variables['todayClose'] = $variables['thursdayHoursClose'];
          }

        }
        break;

      case 5:
        $variables['fridayHours'] = FALSE;
        $fridayHoursOpen = $location_entity->get('field_branch_office_hours')->getValue()[$x]['starthours'];
        $fridayHoursClose = $location_entity->get('field_branch_office_hours')->getValue()[$x]['endhours'];
        if ($fridayHoursOpen != '') {
          $variables['fridayHours'] = TRUE;

          // Now determine if open.
          if (date('N') == 5 && date('H') >= ($fridayHoursOpen / 100) && date('H') < ($fridayHoursClose / 100)) {
            $variables['isOpen'] = TRUE;
          }

          if ($fridayHoursOpen < 1200) {
            if (strlen($fridayHoursOpen) == 3) {
              $fridayHoursOpen = substr_replace($fridayHoursOpen, ':', 1, 0);
            }
            else {
              $fridayHoursOpen = substr_replace($fridayHoursOpen, ':', 2, 0);
            }
            $fridayHoursOpen = $fridayHoursOpen . "AM";
          }
          else {
            if ($fridayHoursOpen == 1200 || $fridayHoursOpen == 1230) {
              $fridayHoursOpen = substr_replace($fridayHoursOpen, ':', 2, 0);
            }
            else {
              $fridayHoursOpen = $fridayHoursOpen - 1200;
              $fridayHoursOpen = substr_replace($fridayHoursOpen, ':', 1, 0);
            }
            $fridayHoursOpen = $fridayHoursOpen . "PM";
          }

          $variables['fridayHoursOpen'] = date('g:ia', strtotime($fridayHoursOpen));

          if ($fridayHoursClose < 1200) {
            if (strlen($fridayHoursClose) == 3) {
              $fridayHoursClose = substr_replace($fridayHoursClose, ':', 1, 0);
            }
            else {
              $fridayHoursClose = substr_replace($fridayHoursClose, ':', 2, 0);
            }
            $fridayHoursClose = $fridayHoursClose . "AM";
          }
          else {
            if ($fridayHoursClose == 1200 || $fridayHoursClose == 1230) {
              $fridayHoursClose = substr_replace($fridayHoursClose, ':', 2, 0);
            }
            else {
              $fridayHoursClose = $fridayHoursClose - 1200;
              $fridayHoursClose = substr_replace($fridayHoursClose, ':', 1, 0);
            }
            $fridayHoursClose = $fridayHoursClose . "PM";
          }

          $variables['fridayHoursClose'] = date('g:ia', strtotime($fridayHoursClose));

          if (date('N') == 5) {
            $variables['today'] = "Friday";
            $variables['todayHours'] = TRUE;
            $variables['todayOpen'] = $variables['fridayHoursOpen'];
            $variables['todayClose'] = $variables['fridayHoursClose'];
          }
        }
        break;

      case 6:
        $variables['saturdayHours'] = FALSE;
        $saturdayHoursOpen = $location_entity->get('field_branch_office_hours')->getValue()[$x]['starthours'];
        $saturdayHoursClose = $location_entity->get('field_branch_office_hours')->getValue()[$x]['endhours'];
        if ($saturdayHoursOpen != '') {
          $variables['saturdayHours'] = TRUE;

          // Now determine if open.
          if (date('N') == 6 && date('H') >= ($saturdayHoursOpen / 100) && date('H') < ($saturdayHoursClose / 100)) {
            $variables['isOpen'] = TRUE;
          }

          if ($saturdayHoursOpen < 1200) {
            if (strlen($saturdayHoursOpen) == 3) {
              $saturdayHoursOpen = substr_replace($saturdayHoursOpen, ':', 1, 0);
            }
            else {
              $saturdayHoursOpen = substr_replace($saturdayHoursOpen, ':', 2, 0);
            }
            $saturdayHoursOpen = $saturdayHoursOpen . "AM";
          }
          else {
            if ($saturdayHoursOpen == 1200 || $saturdayHoursOpen == 1230) {
              $saturdayHoursOpen = substr_replace($saturdayHoursOpen, ':', 2, 0);
            }
            else {
              $saturdayHoursOpen = $saturdayHoursOpen - 1200;
              $saturdayHoursOpen = substr_replace($saturdayHoursOpen, ':', 1, 0);
            }
            $saturdayHoursOpen = $saturdayHoursOpen . "PM";
          }

          $variables['saturdayHoursOpen'] = date('g:ia', strtotime($saturdayHoursOpen));

          if ($saturdayHoursClose < 1200) {
            if (strlen($saturdayHoursClose) == 3) {
              $saturdayHoursClose = substr_replace($saturdayHoursClose, ':', 1, 0);
            }
            else {
              $saturdayHoursClose = substr_replace($saturdayHoursClose, ':', 2, 0);
            }
            $saturdayHoursClose = $saturdayHoursClose . "AM";
          }
          else {
            if ($saturdayHoursClose == 1200 || $saturdayHoursClose == 1230) {
              $saturdayHoursClose = substr_replace($saturdayHoursClose, ':', 2, 0);
            }
            else {
              $saturdayHoursClose = $saturdayHoursClose - 1200;
              $saturdayHoursClose = substr_replace($saturdayHoursClose, ':', 1, 0);
            }
            $saturdayHoursClose = $saturdayHoursClose . "PM";
          }

          $variables['saturdayHoursClose'] = date('g:ia', strtotime($saturdayHoursClose));

          if (date('N') == 6) {
            $variables['today'] = "Saturday";
            $variables['todayHours'] = TRUE;
            $variables['todayOpen'] = $variables['saturdayHoursOpen'];
            $variables['todayClose'] = $variables['saturdayHoursClose'];
          }
        }
        break;
    }
  }
}

/**
 * Calculates distance between two points.
 */
function _distance($lat1, $lon1, $lat2, $lon2, $unit = 'M') {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  }
  elseif ($unit == "N") {
    return ($miles * 0.8684);
  }
  else {
    return $miles;
  }
}
