<?php

namespace Drupal\locations\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\locations\Batch\LocationsBatchProcessor;

/**
 * Class LocationsSyncForm.
 *
 * @package Drupal\locations\Form
 */
class BranchesImportForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'branches_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['help'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Submitting this form will start the import of Branch information.')
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Import Branches',

    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batchProcessor = new LocationsBatchProcessor();
    $batch = $batchProcessor->createBatch();
    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_admin.settings'];
  }

}
