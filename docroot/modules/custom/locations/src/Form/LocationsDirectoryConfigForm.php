<?php

namespace Drupal\locations\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LocationsDirectoryConfigForm.
 *
 * @package Drupal\locations\Form
 */
class LocationsDirectoryConfigForm extends FormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const LOCATIONSDIRECTORYTITLE = 'ur.locationsdirectory.title';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'locations_directory_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $pageTitle = \Drupal::state()->get(static::LOCATIONSDIRECTORYTITLE);

    if ($pageTitle === NULL) {
      $pageTitle = $this->t("Branch Locations Directory");
    }

    $form['directory_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page Title'),
      '#default_value' => $pageTitle,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $pageTitle = $form_state->getValue('directory_page_title');
    if (!empty($pageTitle))
      \Drupal::state()->set(static::LOCATIONSDIRECTORYTITLE, $pageTitle);
  }
}
