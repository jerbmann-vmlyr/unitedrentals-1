<?php

namespace Drupal\locations\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Location entities.
 *
 * @ingroup locations
 */
class LocationEntityDeleteForm extends ContentEntityDeleteForm {


}
