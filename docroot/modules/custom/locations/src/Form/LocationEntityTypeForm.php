<?php

namespace Drupal\locations\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LocationEntityTypeForm.
 *
 * @package Drupal\locations\Form
 */
class LocationEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $location_entity_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $location_entity_type->label(),
      '#description' => $this->t("Label for the Location type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $location_entity_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\locations\Entity\LocationEntityType::load',
      ],
      '#disabled' => !$location_entity_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $location_entity_type = $this->entity;
    $status = $location_entity_type->save();

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addMessage($this->t('Created the %label Location type.', [
          '%label' => $location_entity_type->label(),
        ]));
        break;

      default:
        \Drupal::messenger()->addMessage($this->t('Saved the %label Location type.', [
          '%label' => $location_entity_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($location_entity_type->toUrl('collection'));
  }

}
