<?php

namespace Drupal\locations\Batch;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerManager;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\ur_api_dataservice\Exceptions\NoResultsException;
use Drupal\ur_appliance\Exception\DataException;
use Drupal\ur_appliance\Provider\DAL;

define("NUMBER_OF_BATCHES", 10);

/**
 * Batch Processor for Locations.
 */
class LocationsBatchProcessor {

  /**
   * @return array The definition of the batch job
   */
  public function createBatch() {
    $batch = [
      'title' => t('Importing Branches...'),
      'operations' => [],
      'finished' => 'Drupal\locations\Batch\LocationsBatchProcessor::batchFinished',
    ];

    $batch['operations'][] = [
      'Drupal\locations\Batch\LocationsBatchProcessor::preImport',
      [],
    ];

    $batch['operations'][] = [
      'Drupal\locations\Batch\LocationsBatchProcessor::getAndQueueBranchData',
      [],
    ];

    for ($i = 0; $i < NUMBER_OF_BATCHES; $i++) {
      $batch['operations'][] = [
        'Drupal\locations\Batch\LocationsBatchProcessor::batchProcess',
        [],
      ];
    }

    return $batch;
  }

  /**
   * @param $context
   *   array Access to message     *
   *   The first step in the batch process.
   *   Because the next step can take a while, this step ensures the user sees
   *   a message about that before the next step starts.
   */
  public function preImport(&$context) {
    $message = 'Getting branch data, this could take a while...';
    \Drupal::logger('locations_sync')->info($message);
    $context['message'] = t($message);
  }

  /**
   * @param $context
   *   array Access to message
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * The second step in the batch process.  It gets the branch information from
   *   the DAL and queues it for processing in the batch.
   */
  public function getAndQueueBranchData(&$context) {
    \Drupal::logger('locations_sync')->info('Getting branches from the DAL');
    set_time_limit(240);
    $branches = DAL::Service(TRUE)->BranchDetails()->readAll();

    if (empty($branches)) {
      $message = 'No branch data returned. Batch import failed.';
      \Drupal::logger('locations_sync')->error($message);
      throw new NoResultsException($message);
    }

    /** @var \Drupal\Core\Queue\QueueFactory $queueFactory */
    $queueFactory = \Drupal::service('queue');

    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = $queueFactory->get('locations_queue');

    // In case old items are not all processed from previous run, empty queue.
    $queue->deleteQueue();

    \Drupal::logger('locations_sync')->info('Queueing branch data for import');

    foreach ($branches as $branch) {
      $item = new \stdClass();
      $item->branch = $branch;
      $queue->createItem($item);
    }

    $message = 'Finished getting branch data. Beginning import';
    \Drupal::logger('locations_sync')->info($message);
    $context['message'] = t($message);
    $maxBatchSize = ceil($queue->numberOfItems() / NUMBER_OF_BATCHES);
    $context['results']['maxBatchSize'] = $maxBatchSize;
  }

  /**
   * @param $context
   *   array Provides access to the number of locations to import
   *   between calls to batchProcess Process and import a batch of locations
   *   from the queue, runs NUMBER_OF_BATCHES times
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public static function batchProcess(&$context) {
    \Drupal::logger('locations_sync')
      ->info('Branch import batch process step starting');

    /** @var \Drupal\Core\Queue\QueueFactory $queueFactory */
    $queueFactory = \Drupal::service('queue');

    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = $queueFactory->get('locations_queue');

    /** @var \Drupal\Core\Queue\QueueWorkerManager $queueManager */
    $queueManager = \Drupal::service('plugin.manager.queue_worker');
    $queueWorker = $queueManager->createInstance('locations_queue');
    $currentBatchSize = min($queue->numberOfItems(), $context['results']['maxBatchSize']);

    for ($i = 0; $i < $currentBatchSize; $i++) {
      if ($item = $queue->claimItem()) {
        try {
          $queueWorker->processItem($item);
          $queue->deleteItem($item);
        }
        catch (SuspendQueueException $e) {
          $queue->releaseItem($item);
          break;
        }
        catch (\Exception $e) {
          $message = $e->getMessage();
          \Drupal::logger('locations_sync')
            ->error('Error during location sync %message', ['%message' => $message]);
          $queue->releaseItem($item);
        }
      }
    }

    \Drupal::logger('locations_sync')
      ->info('Branch import batch process step finished');
    $context['message'] = t('Branches remaining %numberInQueue', ['%numberInQueue' => $queue->numberOfItems()]);
  }

  /**
   * Batch finished callback.
   *
   * @param $success
   * @param $results
   * @param $operations
   */
  public static function batchFinished($success, $results, $operations) {
    if ($success) {
      $message = 'Batch Process Finished. The Branches were imported successfully.';
      \Drupal::logger('locations_sync')->info($message);
    }
    else {
      $error_operation = reset($operations);
      $message = t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]);
      \Drupal::logger('locations_sync')->error($message);
    }

    \Drupal::messenger()->addMessage(t($message));
  }

}
