<?php

namespace Drupal\locations;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for location_entity.
 */
class LocationEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
