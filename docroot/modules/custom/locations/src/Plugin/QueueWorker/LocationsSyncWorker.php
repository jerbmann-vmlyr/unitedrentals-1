<?php

namespace Drupal\locations\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\locations\Entity\LocationEntity;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;
use Drupal\Core\Url;

/**
 *
 * @inheritdoc
 */
class LocationsSyncWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  protected $videoEmbedUrls = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    \Drupal::logger('locations_sync')->info('Locations Sync Worker created');
    return new static([], $plugin_id, $plugin_definition);
  }

  /**
   * Processes a single item of Queue.
   *
   * @param $item
   *   mixed The branch data to be processed
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function processItem($item) {
    // This can be initiated manually via Configuration->United Rentals Settings->Branch Import or drush branch-import.
    if ($branch = $item->data->branch) {
      $this->importBranch($branch);
    }
  }

  /**
   * ConvertTextStatus changes the text based status returned by the DAL into a
   * boolean for display purposes as the LocationEntity status field is boolean.
   *
   * @param $textStatus
   *   string The string "A" or "I" representing
   *   Active/Inactive
   *
   * @return bool
   */
  protected function convertTextStatus($textStatus) {
    $status = $textStatus === 'A';
    return $status;
  }

  /**
   * FormatPhone adds formatting to the input phone number ex: 999-999-9999, it
   * assumes a 10 digit number.
   *
   * @param $phoneNumber
   *   string The phone number for the branch, without
   *   formatting ex: 9999999999
   *
   * @return string The formatted phone number
   */
  protected function formatPhone($phoneNumber) {
    // Guard against changes from the source data, break the formatting but keep the data.
    if (strlen($phoneNumber) !== 10) {
      return $phoneNumber;
    }

    return preg_replace('/(\d{3})(\d{3})(\d{4})/', '$1-$2-$3', $phoneNumber);
  }

  /**
   * Import a branch.
   *
   * @param $branch
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function importBranch($branch) {
    $fieldMap = [
      'id' => 'field_branch_id',
      'latitude' => 'field_latitude',
      'longitude' => 'field_longitude',
      'managerEmail' => 'field_manager_email',
      'managerName' => 'field_manager_name',
      'onlineOrders' => 'field_order_online',
      'region' => 'field_region_code',
      'saturdayhours' => 'field_saturday_hours',
      'sundayhours' => 'field_sunday_hours',
      'weekdayhours' => 'field_weekday_hours',
      'branchofficehours' => 'field_branch_office_hours',
      'timeOffset' => 'field_time_offset',
      'daylightSavings' => 'field_daylight_savings',
      'district' => 'field_district',
      'name' => 'name',
      'description' => 'field_web_description',
      'contentBlock' => 'field_web_content_block',
      'webProductType' => 'field_web_product_type',
    ];

    $addressFieldMap = [
      'address1' => 'address_line1',
      'address2' => 'address_line2',
      'city' => 'locality',
      'state' => 'administrative_area',
      'zip' => 'postal_code',
      'countryCode' => 'country_code',
    ];

    $geoLocationMap = [
      'latitude' => 'lat',
      'longitude' => 'lng',
    ];

    \Drupal::logger('locations_sync')
      ->debug('Locations Sync Worker processing branch id: %branch_id', ['%branch_id' => $branch['id']]);

    $query = \Drupal::entityQuery('location_entity');
    $query->condition('field_branch_id', $branch['id']);
    $query->condition('type', 'branch');
    $entity_ids = $query->execute();

    if (!empty($entity_ids)) {
      $entity_id = reset($entity_ids);
      /** @var \Drupal\locations\Entity\LocationEntity $location_entity */
      $location_entity = LocationEntity::load($entity_id);
    }
    else {
      $location_entity = LocationEntity::create(['type' => 'branch']);
      $this->importCityAndState($branch);
    }

    /*
     * If the entity is empty or we are missing the latitude or longitude then
     * we do not want to save this at all.
     */
    if (empty($location_entity) || empty($branch['latitude']) || empty($branch['longitude'])) {
      return;
    }

    $location_entity->set('field_status', $this->convertTextStatus($branch['status']));
    $location_entity->set('field_phone', $this->formatPhone($branch['phone']));
    $location_entity->set('field_fax', $this->formatPhone($branch['fax']));
    $location_entity->set('field_web_display', $branch['webDisplay'] === 'Y');

    foreach ($fieldMap as $branchKey => $fieldKey) {
      $location_entity->set($fieldKey, $branch[$branchKey]);
    }

    $address = [];

    foreach ($addressFieldMap as $branchKey => $fieldKey) {
      $address[$fieldKey] = $branch[$branchKey];
    }

    $geo = [];
    foreach ($geoLocationMap as $branchKey => $fieldKey) {
      $geo[$fieldKey] = $branch[$branchKey];
    }

    $businessTypes = $this->getBusinessTypes();

    $primaryBusinessTypeKey = array_search($branch['businessType'], array_column($businessTypes, 'typeMap'));
    if ($primaryBusinessTypeKey !== FALSE) {
      $primaryBusinessType = $businessTypes[$primaryBusinessTypeKey]->tid;
      $location_entity->set('field_primary_business_type', $primaryBusinessType);
    }

    if (branch['status'] === 'A' && !empty($branch['businessType']) && $primaryBusinessTypeKey === FALSE) {
      $term = Term::create([
        'vid' => 'location_branches',
        'name' => empty($branch['businessTypeDesc']) ? $branch['businessType'] : $branch['businessTypeDesc'],
        'field_branch_code' => $branch['businessType'],
        'field_hide_from_type_lists' => TRUE,
        'field_branch_contact_us_showhide' => FALSE,
        'field_branch_rentonline_showhide' => FALSE,
        'field_branch_contact_us_link' => Url::fromUri('internal:/customer-care/contact-us')->toString(),
        'field_branch_rent_online_link' => Url::fromUri('internal:/marketplace/equipment/')->toString(),
      ]);
      $term->enforceIsNew();
      $term->save();
      $location_entity->set('field_primary_business_type', $term->tid->getValue()[0]['value']);
    }

    $location_entity->set('field_business_type', [$branch['businessType']]);
    $location_entity->set('field_address', [$address]);
    $location_entity->set('field_geolocation', [$geo]);
    $location_entity->set('type', 'branch');
    $location_entity->set('field_video_url', $this->getEmbedLink($branch['videoUrl']));
    $location_entity->save();
  }

  private function getBusinessTypes() {
    $businessTypesTaxonomy = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree("location_branches");
    $businessTypes = array();
    foreach ($businessTypesTaxonomy as $item) {
      $taxonomyItem = Term::load($item->tid);
      $code = $taxonomyItem->get('field_branch_code')->getValue()[0]['value'];

      $businessTypes[] = (object) [
        'tid' => $item->tid,
        'name' => $item->name,
        'typeMap' => $code,
      ];
    }
    return $businessTypes;
  }

  /**
   * Import city and state.
   *
   * @param $branch
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function importCityAndState($branch) {
    // Query for existing city, import if not found.
    $query = \Drupal::entityQuery('location_entity');
    $query->condition('field_address.locality', $branch['city']);
    $query->condition('field_address.administrative_area', $branch['state']);
    $query->condition('type', 'city');
    $entity_ids = $query->execute();

    // A new city not imported before.
    if (empty($entity_ids)) {
      $location_entity = LocationEntity::create(['type' => 'city']);

      $address = [
        'locality' => $branch['city'],
        'administrative_area' => $branch['state'],
        'country_code' => $branch['countryCode'],
      ];

      $location_entity->set('field_address', $address);
      $location_entity->set('type', 'city');
      $location_entity->save();
    }

    // Query for existing state/province, import if not found.
    $query = \Drupal::entityQuery('location_entity');
    $query->condition('field_address.administrative_area', $branch['state']);
    $query->condition('field_address.country_code', $branch['countryCode']);
    $query->condition('type', 'state_province');
    $entity_ids = $query->execute();

    // A new state not imported before.
    if (empty($entity_ids)) {
      $location_entity = LocationEntity::create(['type' => 'state_province']);
      $address = [
        'administrative_area' => $branch['state'],
        'country_code' => $branch['countryCode'],
      ];

      $location_entity->set('field_address', $address);
      $location_entity->set('type', 'state_province');
      $location_entity->save();
    }
  }

  /**
   * Convert a vimeo video link into an embeddable format.
   * Other video links (youtube) are returned unchanged.
   * This is needed because of vimeo security.
   * * https://developer.vimeo.com/apis/oembed.
   *
   * @param $videoUrl
   *   string The link to the branch's video
   *
   * @return string The embeddable video link.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getEmbedLink($videoUrl) {
    \Drupal::logger('locations_sync')->debug('In getEmbedLink');

    try {
      // Only vimeo requires special handling of embeded videos.
      if (!$videoUrl || !strpos($videoUrl, 'vimeo')) {
        return $videoUrl;
      }

      // For performance, check if this has been converted before.
      if (isset($this->videoEmbedUrls[$videoUrl])) {
        \Drupal::logger('locations_sync')
          ->debug('Video converted previously: ' . $videoUrl . ' embed link: ' . $this->videoEmbedUrls[$videoUrl]);
        return $this->videoEmbedUrls[$videoUrl];
      }

      \Drupal::logger('locations_sync')
        ->debug('Getting embed link from vimeo');

      $client = new Client();
      $response = $client->request('GET', 'https://vimeo.com/api/oembed.xml', [
        'query' => [
          'url' => $videoUrl,
          'xhtml' => TRUE,
        ],
      ]);

      \Drupal::logger('locations_sync')
        ->debug('Response Status: ' . $response->getStatusCode());
      \Drupal::logger('locations_sync')
        ->debug('Response Reason: ' . $response->getReasonPhrase());

      $xmlResp = (string) $response->getBody()->getContents();

      \Drupal::logger('locations_sync')
        ->debug('Response Body: ' . $xmlResp);

      if ($response->getStatusCode() != 200 || !$xmlResp) {
        \Drupal::logger('locations_sync')
          ->error('Failed to get embed link from vimeo. Status: ' . $response->getStatusCode() . ' Response reason: ' . $response->getReasonPhrase() . 'Response body: ' . $xmlResp);
      }

      $xmlResp = html_entity_decode($xmlResp);

      if (preg_match('/iframe src="([^"]*)/', $xmlResp, $matches)) {
        $this->videoEmbedUrls[$videoUrl] = $matches[1];
        return $this->videoEmbedUrls[$videoUrl];
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('locations_sync')
        ->error('Failed to create embed link for branch video url: ' . $videoUrl . ' details: ' . $e->getMessage());
    }
  }

}
