<?php

namespace Drupal\locations\Plugin\QueueWorker;

/**
 *
 * @QueueWorker(
 * id = "locations_queue",
 * title = "Cron Locations Sync Worker",
 * cron = {"time" = 10}
 * )
 */
class CronLocationsSyncWorker extends LocationsSyncWorker {

}
