<?php

namespace Drupal\locations\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to calculate distance from current routes geolocation.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("distance_calculation")
 */
class DistanceCalculation extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // No query on this field.
  }

  /**
   * Define the available options
   * @return array
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $current_location = \Drupal::routeMatch()->getParameter('location_entity');
    $location = $values->_entity;

    $lat1 = $current_location->get('field_geolocation')->getValue()[0]['lat'];
    $lng1 = $current_location->get('field_geolocation')->getValue()[0]['lng'];
    $lat2 = $location->get('field_geolocation')->getValue()[0]['lat'];
    $lng2 = $location->get('field_geolocation')->getValue()[0]['lng'];
    $unit = 'M';

    return self::distance($lat1, $lng1, $lat2, $lng2, $unit);
  }

  public static function distance($lat1, $lon1, $lat2, $lon2, $unit) {

    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
      return number_format((float) ($miles * 1.609344), 2, '.', '');
    }
    elseif ($unit == "N") {
      return ($miles * 0.8684);
    }
    else {
      return number_format((float) $miles, 2, '.', '');
    }
  }

}
