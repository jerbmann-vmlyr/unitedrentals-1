<?php

namespace Drupal\locations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\locations\Entity\LocationEntity;

/**
 * Class DefaultController.
 *
 * @package Drupal\locations\Controller
 */
class MigrateGeoLocations extends ControllerBase {

  /**
   * Handler for process request.
   */
  public function process() {

    $query = \Drupal::entityQuery('location_entity');
    $query->condition('type', 'branch');
    $entityIds = $query->execute();
    $chunks = array_chunk($entityIds, 10);

    $batch = [
      'title' => t('Migrating GeoLocation Field Data.'),
      'operations' => [],
      'init_message' => t('Migrating GeoLocation Field Data now..'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('GeoLocation Migrate has encountered an error.'),
    ];
    foreach ($chunks as $chunk) {
      $batch['operations'][] = ['Drupal\locations\Controller\MigrateGeoLocations::migrate', [$chunk]];
    }

    batch_set($batch);
    return batch_process('/admin/config/locations/migrate-geolocations');
  }

  /**
   * Migrates the lat/long fields into a geofield.
   */
  public static function migrate($items, &$context) {
    foreach ($items as $id) {
      if ($id) {
        /** @var \Drupal\locations\Entity\LocationEntity $location_entity */
        $branch = LocationEntity::load($id);
        $lat = $branch->get('field_latitude')->value;
        $lng = $branch->get('field_longitude')->value;

        if ($lat && $lng) {
          $branch->set('field_geolocation', ['lat' => (float) $lat, 'lng' => (float) $lng]);
          $branch->save();
        }
      }
    }
  }

}
