<?php

namespace Drupal\locations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\locations\Entity\LocationEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Tinify\Exception;

/**
 * Controller for Branch Finder.
 */
class BranchController extends ControllerBase {
  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  public $request;

  /**
   * Class constructor.
   *   Request stack.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   */
  public function __construct(RequestStack $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack')
    );
  }

  /**
   * Default Branches method.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   the default branches
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function defaultBranches($json = TRUE) {
    // Use a cached version if available.
    $cid = 'apiDefaultBranches';
    $results = [];
    if ($cache = \Drupal::cache()->get($cid)) {
      if (!empty($cache->data)) {
        $results = $cache->data;
      }
      else {
        \Drupal::logger('Branch List')
          ->error('Branches cache returned an empty object.');
      }
    }
    if (empty($results)) {
      /** @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage $branchStorage */
      $branchStorage = \Drupal::entityTypeManager()
        ->getStorage('location_entity');

      // Status == 1 because we only want published branches.
      $branches = $branchStorage->loadByProperties([
        'type' => 'branch',
        'status' => 1
      ]);

      foreach ($branches as $branch) {
        $results[] = $this->mapBranchFields($branch);
      }

      if (empty($results)) {
        \Drupal::logger('Branch List')
          ->error('Branches database call resulted in an empty result set.');
      }
    }

    if ($json) {
      $response = new JsonResponse();
      $response->setData($results);
    }
    else {
      $response = $results;
    }

    // Cache the response for faster subsequent queries.
    $expireIn24Hours = time() + (24 * 60 * 60);
    \Drupal::cache()->set($cid, $results, $expireIn24Hours);

    return $response;
  }

  /**
   * Creates an array of values from a branch Location Entity
   * The Entity field names are mapped/translated to the field names expected by the frontend.
   *
   * @param \Drupal\locations\Entity\LocationEntityInterface $branch
   *
   * @return array The values for the Branch/Location
   */
  protected function mapBranchFields(LocationEntityInterface $branch) {
    $fieldMap = [
      'branchId' => 'field_branch_id',
      'fax' => 'field_fax',
      'latitude' => 'field_latitude',
      'longitude' => 'field_longitude',
      'managerEmail' => 'field_manager_email',
      'managerName' => 'field_manager_name',
      'onlineOrders' => 'field_order_online',
      'phone' => 'field_phone',
      'region' => 'field_region_code',
      'saturdayHours' => 'field_saturday_hours',
      'sundayHours' => 'field_sunday_hours',
      'webDisplay' => 'field_web_display',
      'weekdayHours' => 'field_weekday_hours',
      'name' => 'name',
      'timeOffset' => 'field_time_offset',
      'daylightSavings' => 'field_daylight_savings',
      'district' => 'field_district',
      'businessType' => 'field_business_type',
      'businessTypes' => 'field_business_types',
      'primaryBusinessType' => 'field_primary_business_type',
      'status' => 'field_status',
      'videoUrl' => 'field_video_url',
      'webContentBlock' => 'field_web_content_block',
      'webDescription' => 'field_web_description',
      'webProductType' => 'field_web_product_type',
    ];

    $addressFieldMap = [
      'address1' => 'address_line1',
      'address2' => 'address_line2',
      'city' => 'locality',
      'state' => 'administrative_area',
      'zip' => 'postal_code',
      'countryCode' => 'country_code',
    ];

    $geoLocationMap = [
      'latitude' => 'lat',
      'longitude' => 'lng',
    ];

    $mappedBranch = [];

    foreach ($fieldMap as $mappedKey => $branchKey) {
      $mappedBranch[$mappedKey] = $this->replaceNullWithEmpty($branch->get($branchKey)->value);
    }

    $address = $branch->get('field_address')[0];

    foreach ($addressFieldMap as $mappedKey => $branchKey) {
      $mappedBranch[$mappedKey] = $this->replaceNullWithEmpty($address->getValue()[$branchKey]);
    }


    $geo = $branch->get('field_geolocation')[0];
    foreach ($geoLocationMap as $mappedKey => $branchKey) {
      $mappedBranch[$mappedKey] = $this->replaceNullWithEmpty($geo->getValue()[$branchKey]);
    }

    return $mappedBranch;
  }

  /**
   * Replaces Null with an empty string.
   *
   * @param $value
   *
   * @return string
   */
  public function replaceNullWithEmpty($value) {
    return $value === NULL ? '' : $value;
  }

  /**
   * Return an array with values for the branch/location with the provided branchId.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getBranchById() {
    $branchId = $this->request->getCurrentRequest()->query->get('branchId');
    $response = new JsonResponse();

    if (!isset($branchId) || empty($branchId)) {
      $response->setData(['message' => 'Missing branchId parameter.']);
      return $response;
    }

    try {
      $branchStorage = \Drupal::entityTypeManager()
        ->getStorage('location_entity');

      $arrResult = $branchStorage->loadByProperties([
        'type' => 'branch',
        'field_branch_id' => $branchId
      ]);

      $branch = reset($arrResult);
      $results = $this->mapBranchFields($branch);
    }
    catch (\Exception $e) {
      $results = $e->getMessage();
    }

    $response->setData([$results]);
    return $response;
  }

}
