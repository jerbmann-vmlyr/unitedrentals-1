<?php

namespace Drupal\locations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\geolocation\Annotation\Location;
use Drupal\locations\EventSubscriber\LocationsChecker;
use Drupal\taxonomy\Entity\Term;
use Drupal\ur_frontend_api_v2\Models\BusinessType;

/**
 * Class DefaultController.
 *
 * @package Drupal\locations\Controller
 */
class DefaultController extends ControllerBase {

  /**
   * Locations.
   *
   * @return array
   *   Return markup needed for Vue component.
   */
  public function listBranchesByState() {
    $location_description = 'United Rentals has rental locations all across the United States and Canada. Find a tool and equipment rental location near you.';
    $location_title = 'Equipment Rental Locations';

    $meta_description = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'description',
        'content' => $location_description,
      ],
    ];

    $twitter_description = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'twitter.description',
        'content' => $location_description,
      ],
    ];

    $og_description = [
      '#tag' => 'meta',
      '#attributes' => [
        'property' => 'og:description',
        'content' => $location_description,
      ],
    ];

    $twitter_title = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'twitter.title',
        'content' => $location_title,
      ],
    ];

    $og_title = [
      '#tag' => 'meta',
      '#attributes' => [
        'property' => 'og:title',
        'content' => $location_title,
      ],
    ];

    $library = isset($_ENV['AH_SITE_ENVIRONMENT']) ? 'locations/main' : 'locations/local';

    return [
      '#type' => 'markup',
      '#theme' => 'locations_state_list',
      '#states' => $this->getStates(),
      '#provinces' => $this->getProvinces(),
      '#businessTypes' => $this->getBranchTypes(),
      '#attached' => [
        'library' => [$library],
        'html_head' => [
          [$meta_description, 'description'],
          [$twitter_description, 'twitter.description'],
          [$og_description, 'og:description'],
          [$twitter_title, 'twitter.title'],
          [$og_title, 'og:title'],
        ],
      ],
    ];
  }

  /**
   ** Locations Directory.
   *
   * @return array
   *   Return markup needed for component.
   */
  public function directory() {

    $pageTitle = \Drupal::state()->get('ur.locationsdirectory.title');
    if (empty($pageTitle))
      $pageTitle = $this->t("Branch Locations Directory");

    $location_description = 'United Rentals has rental locations all across the United States and Canada. Find tool & equipment rental locations near you.';

    $meta_description = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'description',
        'content' => $location_description,
      ],
    ];

    $twitter_description = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'twitter.description',
        'content' => $location_description,
      ],
    ];

    $og_description = [
      '#tag' => 'meta',
      '#attributes' => [
        'property' => 'og:description',
        'content' => $location_description,
      ],
    ];

    $twitter_title = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'twitter.title',
        'content' => $pageTitle,
//        'content' => $location_title,
      ],
    ];

    $og_title = [
      '#tag' => 'meta',
      '#attributes' => [
        'property' => 'og:title',
        'content' => $pageTitle,
//        'content' => $location_title,
      ],
    ];

    $directory = [];
    $query = \Drupal::entityQuery('location_entity');
    $query->condition('status', 1);
    $query->condition('type', 'branch');
    $query->condition('field_web_display', '1');
    $response = $query->execute();

    /** @var \Drupal\locations\Entity\LocationEntity $branch */
    $result = \Drupal::entityTypeManager()
      ->getStorage('location_entity')->loadMultiple(array_keys($response));

    // All active branches loaded, loop through.
    foreach ($result as $node) {

      // If we don't have an address and a business type, skip.
      if (!empty($node->get('field_address')->getValue()) && !empty($node->get('field_primary_business_type')->getValue())) {
        $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/location_entity/' . $node->id());
        $address = $node->get('field_address')->getValue()[0];
        $types = [];
        $type_id = $node->get('field_primary_business_type')->getValue()[0]['target_id'];

        if ($type_id) {

          // Create Business Type array so we don't have to load the term if not necessary.
          if (!array_key_exists($type_id, $types)) {
            $term = Term::load($type_id);
            $types[$type_id] = $term->getName();
          }

          // Only US and Canada, all others considered Europe for now.
          $country_codes = [
            'US' => 'United States',
            'CA' => 'Canada',
          ];

          if (array_key_exists($address['country_code'], $country_codes)) {
            $country = $country_codes[$address['country_code']];
            $admin_area = LocationsChecker::getFullState($address['administrative_area']);
            $state_path = '/locations/' . strtolower($address['administrative_area']);
          }
          else {
            $country = 'Europe';
            $eu_codes = [
              'FR' => "France",
              'DE' => "Denmark",
              'UK' => "United Kingdom",
              'NL' => "Netherlands"
            ];

            if (array_key_exists($address['country_code'], $eu_codes)) {
              $admin_area = $eu_codes[$address['country_code']];
            }
            else {
              $admin_area = $address['country_code'];
            }

            $state_path = NULL;
          }

          if ($admin_area) {
            $directory[$country][$admin_area]['path'] = $state_path;
            $directory[$country][$admin_area]['values'][$types[$type_id]][$node->id()] = [
              'path' => $alias,
              'address' => $address,
            ];
          }
        }
      }
    }

    foreach ($directory['United States'] as $k => $v) {
      $count = 0;
      foreach ($v['values'] as $group) {
        $count += count($group);
      }
      unset($directory['United States'][$k]);
      $k .= ' (' . $count . ')';
      $directory['United States'][$k] = $v;
      ksort($directory['United States'][$k]['values']);
    }

    foreach ($directory['Canada'] as $k => $v) {
      $count = 0;
      foreach ($v['values'] as $group) {
        $count += count($group);
      }
      unset($directory['Canada'][$k]);
      $k .= ' (' . $count . ')';
      $directory['Canada'][$k] = $v;
      ksort($directory['Canada'][$k]['values']);
    }

    if (array_key_exists('Europe', $directory)) {
      foreach ($directory['Europe'] as $k => $v) {
        $count = 0;
        foreach ($v['values'] as $group) {
          $count += count($group);
        }
        unset($directory['Europe'][$k]);
        $k .= ' (' . $count . ')';
        $directory['Europe'][$k] = $v;
        ksort($directory['Europe'][$k]['values']);
      }
    }

    ksort($directory['United States']);
    ksort($directory['Canada']);
    if (array_key_exists('Europe', $directory)) {
      ksort($directory['Europe']);
    }

    return [
      '#type' => 'markup',
      '#theme' => 'locations_directory',
      '#directory_listing' => $directory,
      '#page_title' => $pageTitle,
      '#attached' => [
        'library' => ['locations/locations-directory'],
        'html_head' => [
          [$meta_description, 'description'],
          [$twitter_description, 'twitter.description'],
          [$og_description, 'og:description'],
          [$twitter_title, 'twitter.title'],
          [$og_title, 'og:title'],
        ],
      ],
    ];
  }


  /**
   * Locations.
   *
   * @return array
   *   Return markup needed for Vue component.
   */
  public function stateList() {
    $location_description = 'United Rentals has rental locations all across the United States and Canada. Find a tool and equipment rental location near you.';
    $location_title = 'Equipment Rental Locations';

    $meta_description = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'description',
        'content' => $location_description,
      ],
    ];

    $twitter_description = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'twitter.description',
        'content' => $location_description,
      ],
    ];

    $og_description = [
      '#tag' => 'meta',
      '#attributes' => [
        'property' => 'og:description',
        'content' => $location_description,
      ],
    ];

    $twitter_title = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'twitter.title',
        'content' => $location_title,
      ],
    ];

    $og_title = [
      '#tag' => 'meta',
      '#attributes' => [
        'property' => 'og:title',
        'content' => $location_title,
      ],
    ];

    $branchTypes = [
      'AR' => 'Aerial Equipment',
      'FS' => 'Fluid Solutions',
      'GR' => 'General Equipment & Tools',
      'IR' => 'Industrial',
      'PR' => 'Power & HVAC',
      'SS' => 'Reliable Onsite Services',
      'TR' => 'Trench Safety',
    ];

    $library = isset($_ENV['AH_SITE_ENVIRONMENT']) ? 'locations/main' : 'locations/local';

    return [
      '#type' => 'markup',
      '#theme' => 'locations_state_list',
      '#states' => $this->getStates(),
      '#provinces' => $this->getProvinces(),
      '#businessTypes' => $branchTypes,
      '#attached' => [
        'library' => [
          $library,
          'urone/location-entity--full',
        ],
        'html_head' => [
          [$meta_description, 'description'],
          [$twitter_description, 'twitter.description'],
          [$og_description, 'og:description'],
          [$twitter_title, 'twitter.title'],
          [$og_title, 'og:title'],
        ],
      ],
    ];
  }

  /**
   * Returns a list of states and their state codes.
   */
  protected function getStates() {
    $states = [];
    $jsonPath = drupal_realpath($_SERVER['DOCUMENT_ROOT']) . '/themes/custom/urone/data/states-hash.json';
    if (file_exists($jsonPath)) {
      $json = file_get_contents($jsonPath);
      $states_tmp = json_decode($json, TRUE);

      // Add counts to states.
      foreach ($states_tmp as $abbr => $state) {
        $stateObj = new \stdClass();
        $stateObj->abbr = $abbr;
        $stateObj->name = $state;
        $stateObj->total = count($this->getBranchesByState($abbr));
        if ($stateObj->total) {
          $states[$abbr] = $stateObj;
        }
      }
    }
    return $states;
  }

  /**
   * Returns a list of states and their state codes.
   */
  protected function getProvinces() {
    $provinces = [];
    $jsonPath = drupal_realpath($_SERVER['DOCUMENT_ROOT']) . '/themes/custom/urone/data/provinces-hash.json';
    if (file_exists($jsonPath)) {
      $json = file_get_contents($jsonPath);
      $provinces_tmp = json_decode($json, TRUE);

      // Add counts to provinces.
      foreach ($provinces_tmp as $abbr => $province) {
        $provinceObj = new \stdClass();
        $provinceObj->abbr = $abbr;
        $provinceObj->name = $province;
        $provinceObj->total = count($this->getBranchesByState($abbr));
        $provinces[$abbr] = $provinceObj;
      }
    }
    return $provinces;
  }

  /**
   * Returns Branches in a state.
   */
  protected function getBranchesByState($abbr = NULL) {
    $branchController = BranchController::create(\Drupal::getContainer());
    $branches = $branchController->defaultBranches(FALSE);
    $response = [];

    // Skip if empty.
    if (empty($branches)) {
      return [];
    }

    // Now loop through branches and split up by state.
    foreach ($branches as $idx => $branch) {
      // If the state key does not exist.
      if (!(isset($response[$branch['state']]))) {
        $response[$branch['state']] = [];
        $response[$branch['state']][$branch['branchId']] = $idx;
      }
      else {
        $response[$branch['state']][$branch['branchId']] = $idx;
      }
    };

    // Return response for specific state?
    if ($abbr != NULL && isset($response[$abbr])) {
      return $response[$abbr];
    }
    elseif ($abbr != NULL && !isset($response[$abbr])) {
      return [];
    }
    return $response;
  }

  /**
   * Returns the branch types.
   */
  protected function getBranchTypes() {
    /** @var \Drupal\ur_api_dataservice\Plugins\BranchPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Branch');
    return $plugin->getBusinessTypes();
  }

}
