<?php

namespace Drupal\locations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\TypedData\Plugin\DataType\Map;
use Drupal\locations\Entity\LocationEntity;
use Drupal\ur_appliance\Controller\Google\MapsController;
use Exception;

/**
 * Class SetCityStateGeo.
 *
 * @package Drupal\locations\Controller
 */
class SetCityStateGeo extends ControllerBase {

  /**
   * Handler for process request.
   */
  public function process() {

    $query = \Drupal::entityQuery('location_entity');
    $query->condition('type', 'branch', '!=');
    $entityIds = $query->execute();
    $chunks = array_chunk($entityIds, 10);

    $batch = [
      'title' => t('Calculating GeoLocation Field Data.'),
      'operations' => [],
      'init_message' => t('Calculating GeoLocation Field Data now..'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('GeoLocation Calculating has encountered an error.'),
    ];
    foreach ($chunks as $chunk) {
      $batch['operations'][] = ['Drupal\locations\Controller\SetCityStateGeo::calculate', [$chunk]];
    }

    batch_set($batch);
    return batch_process('/admin/config/locations/calculate-geolocations');
  }

  /**
   * Migrates the lat/long fields into a geofield.
   */
  public static function calculate($items, &$context) {
    foreach ($items as $id) {
      $location = LocationEntity::load($id);

      $query = \Drupal::entityQuery('location_entity')
        ->condition('type', 'branch')
        ->condition('status', 1);

      if ($location->bundle() === 'city') {
        $query->condition('field_address.administrative_area', $location->get('field_address')->getValue()[0]['administrative_area']);
        $query->condition('field_address.locality', $location->get('field_address')->getValue()[0]['locality']);
      }
      else {
        $query->condition('field_address.administrative_area', $location->get('field_address')->getValue()[0]['administrative_area']);
      }

      $ids = $query->execute();
      $nodes = \Drupal::entityTypeManager()->getStorage('location_entity')->loadMultiple($ids);

      $points = [];
      if (!empty($nodes)) {
        foreach ($nodes as $node) {
          if ($node->get('field_latitude')->value && $node->get('field_longitude')->value) {
            $points[] = [
              'lat' => $node->get('field_latitude')->value,
              'lng' => $node->get('field_longitude')->value,
            ];
          }
        }
      }
      else {
        if ($location->get('field_address')->getValue()[0]['country_code'] !== 'CA') {
          $state = $location->get('field_address')->getValue()[0]['administrative_area'];
          $city = $location->get('field_address')->getValue()[0]['locality'];
          $address = $city . ', ' . $state;
          $controller = new MapsController();
          try {
            $place = $controller->getPlace(NULL, $city . ', ' . $state);
            if ($place) {
              $lat = $place->getGeometry()->location->lat;
              $lng = $place->getGeometry()->location->lng;
              $points[] = [
                'lat' => $lat,
                'lng' => $lng,
              ];
            }
          }
          catch (Exception $ex) {
            continue;
          }
        }
      }

      if (!empty($points)) {
        $center = self::get_center($points);
        $location->set('field_geolocation', ['lat' => $center[0], 'lng' => $center[1]]);
        $location->save();
      }
    }
  }

  /**
   * Gets center of multiple points.
   */
  public static function get_center($coords) {
    $count_coords = count($coords);
    $xcos = 0.0;
    $ycos = 0.0;
    $zsin = 0.0;

    foreach ($coords as $lnglat) {
      $lat = $lnglat['lat'] * pi() / 180;
      $lon = $lnglat['lng'] * pi() / 180;

      $acos = cos($lat) * cos($lon);
      $bcos = cos($lat) * sin($lon);
      $csin = sin($lat);
      $xcos += $acos;
      $ycos += $bcos;
      $zsin += $csin;
    }

    $xcos /= $count_coords;
    $ycos /= $count_coords;
    $zsin /= $count_coords;
    $lon = atan2($ycos, $xcos);
    $sqrt = sqrt($xcos * $xcos + $ycos * $ycos);
    $lat = atan2($zsin, $sqrt);

    return [$lat * 180 / pi(), $lon * 180 / pi()];
  }

}
