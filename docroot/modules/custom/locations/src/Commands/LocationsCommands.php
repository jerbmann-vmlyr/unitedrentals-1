<?php

namespace Drupal\locations\Commands;

use Drupal\locations\Batch\LocationsBatchProcessor;
use Drush\Commands\DrushCommands;

/**
 * Class LocationCommands for Drush 9+
 */
class LocationsCommands extends DrushCommands {

  /**
   * Import location data
   *
   * @command locations:importLocations
   * @aliases branch-import
   * @validate-module-enabled locations
   */
  public function locationsImport() {
    $message = 'Branch Import started via Drush';
    $this->output()->writeln(dt($message));
    $this->logger()->info(dt($message));

    $locationsBatchProcessor = new LocationsBatchProcessor();
    $batch = $locationsBatchProcessor->createBatch();
    batch_set($batch);
    drush_backend_batch_process();

    $this->output()->writeln(dt('Batch Process Finished'));
  }

}
