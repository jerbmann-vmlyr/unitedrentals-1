<?php

namespace Drupal\locations\EventSubscriber;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Url;
use Drupal\ur_frontend_api_v2\Controller\GetBranches;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\ur_appliance\Provider\Google;
use Drupal\ur_appliance\Data\Google\Place;


/**
 * Event Subscriber LocationsChecker.
 */
class LocationsChecker implements EventSubscriberInterface {

  /**
   * Code to be triggered on REQUEST event.
   */
  public function onRequest(GetResponseEvent $event) {
    $fall_to_state = FALSE;
    $route_name_other = \Drupal::routeMatch()->getRouteName();
    if ($route_name_other === 'system.404') {
      $path = explode('/', $_SERVER['REQUEST_URI']);
      if ($path[1] === 'locations' && count($path) > 2 && count($path) <= 4) {
        $state = strtoupper($path[2]);
        if (isset($path[3])) {
          $city = ucwords(str_replace('-', ' ', $path[3]));
          try {
            $place = Google::Maps()->getPlace(NULL, $city . ', ' . $state);
          }
          catch (\Exception $e) {
            $fall_to_state = TRUE;
          }
          if ($place instanceof Place) {
            $hcity = str_replace(' ', '-', $city);
            if ($place->getName() !== $city ||
              (strcasecmp($hcity, $place->getName()) == 0 && strstr($hcity, '-'))) {
              $query = \Drupal::entityQuery('location_entity');
              $query->condition('field_address.administrative_area', $state);
              $query->condition('type', 'city');

              if ($place->getName() !== $city && strcasecmp($hcity, $place->getName()) == 0) {
                $query->condition('field_address.locality', $city);
              }
              else {
                $query->condition('field_address.locality', $place->getName());
              }
              $entity_ids = $query->execute();

              // City Google returned exists, redirect to it.
              if (!empty($entity_ids)) {
                $id = reset($entity_ids);
                $url = Url::fromUserInput('/location_entity/' . $id);
                $response = new RedirectResponse($url->toString());
                $response->send();
              }
              else {
                $fall_to_state = TRUE;
              }
            }
            elseif ($place->getName() == $city) {
              $lat = $place->getGeometry()->location->lat;
              $lng = $place->getGeometry()->location->lng;
              $id = GetBranches::createEntity($lat, $lng, $state, $city);
            }
            else {
              $fall_to_state = TRUE;
            }
          }
        }
        else {
          $map_state = self::getFullState($state);
          if ($map_state) {
            $place = Google::Maps()->getPlace(NULL, $map_state);
            if ($place instanceof Place) {
              $lat = $place->getGeometry()->location->lat;
              $lng = $place->getGeometry()->location->lng;
              $id = GetBranches::createEntity($lat, $lng, $state);
            }
          }
          else {
            $fall_to_state = TRUE;
          }
        }

        if ($id) {
          $url = Url::fromUserInput('/location_entity/' . $id);
          $response = new RedirectResponse($url->toString());
          $response->send();
        }
      }
    }

    if ($fall_to_state) {
      $url = Url::fromUserInput('/locations/' . $path[2]);
      $response = new RedirectResponse($url->toString());
      $response->send();
    }

    return;
  }

  public static function getFullState($abbr) {
    $states = [
      "AL" => "Alabama",
      "AK" => "Alaska",
      "AZ" => "Arizona",
      "AR" => "Arkansas",
      "CA" => "California",
      "CO" => "Colorado",
      "CT" => "Connecticut",
      "DE" => "Delaware",
      "DC" => "District Of Columbia",
      "FL" => "Florida",
      "GA" => "Georgia",
      "HI" => "Hawaii",
      "ID" => "Idaho",
      "IL" => "Illinois",
      "IN" => "Indiana",
      "IA" => "Iowa",
      "KS" => "Kansas",
      "KY" => "Kentucky",
      "LA" => "Louisiana",
      "ME" => "Maine",
      "MD" => "Maryland",
      "MA" => "Massachusetts",
      "MI" => "Michigan",
      "MN" => "Minnesota",
      "MS" => "Mississippi",
      "MO" => "Missouri",
      "MT" => "Montana",
      "NE" => "Nebraska",
      "NV" => "Nevada",
      "NH" => "New Hampshire",
      "NJ" => "New Jersey",
      "NM" => "New Mexico",
      "NY" => "New York",
      "NC" => "North Carolina",
      "ND" => "North Dakota",
      "OH" => "Ohio",
      "OK" => "Oklahoma",
      "OR" => "Oregon",
      "PA" => "Pennsylvania",
      "PR" => "Puerto Rico",
      "RI" => "Rhode Island",
      "SC" => "South Carolina",
      "SD" => "South Dakota",
      "TN" => "Tennessee",
      "TX" => "Texas",
      "UT" => "Utah",
      "VT" => "Vermont",
      "VA" => "Virginia",
      "WA" => "Washington",
      "WV" => "West Virginia",
      "WI" => "Wisconsin",
      "WY" => "Wyoming",
      "AB" => "Alberta",
      "BC" => "British Columbia",
      "MB" => "Manitoba",
      "NB" => "New Brunswick",
      "NL" => "Newfoundland and Labrador",
      "NS" => "Nova Scotia",
      "ON" => "Ontario",
      "PE" => "Prince Edward Island",
      "QC" => "Quebec",
      "SK" => "Saskatchewan",
    ];

    if (array_key_exists($abbr, $states)) {
      return $states[$abbr];
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onRequest', 31];
    return $events;
  }

}
