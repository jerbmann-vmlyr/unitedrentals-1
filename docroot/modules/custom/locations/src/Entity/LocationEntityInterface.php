<?php

namespace Drupal\locations\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Location entities.
 *
 * @ingroup locations
 */
interface LocationEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Location name.
   *
   * @return string
   *   Name of the Location.
   */
  public function getName();

  /**
   * Sets the Location name.
   *
   * @param string $name
   *   The Location name.
   *
   * @return \Drupal\locations\Entity\LocationEntityInterface
   *   The called Location entity.
   */
  public function setName($name);

  /**
   * Gets the Location creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Location.
   */
  public function getCreatedTime();

  /**
   * Sets the Location creation timestamp.
   *
   * @param int $timestamp
   *   The Location creation timestamp.
   *
   * @return \Drupal\locations\Entity\LocationEntityInterface
   *   The called Location entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Location published status indicator.
   *
   * Unpublished Location are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Location is published.
   */
  public function isPublished();

}
