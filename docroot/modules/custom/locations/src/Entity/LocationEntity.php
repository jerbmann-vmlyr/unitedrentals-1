<?php

namespace Drupal\locations\Entity;

use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Location entity.
 *
 * @ingroup locations
 *
 * @ContentEntityType(
 *   id = "location_entity",
 *   label = @Translation("Location"),
 *   bundle_label = @Translation("Location type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\locations\LocationEntityListBuilder",
 *     "views_data" = "Drupal\locations\Entity\LocationEntityViewsData",
 *     "translation" = "Drupal\locations\LocationEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\locations\Form\LocationEntityForm",
 *       "add" = "Drupal\locations\Form\LocationEntityForm",
 *       "edit" = "Drupal\locations\Form\LocationEntityForm",
 *       "delete" = "Drupal\locations\Form\LocationEntityDeleteForm",
 *     },
 *     "access" = "Drupal\locations\LocationEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\locations\LocationEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "location_entity",
 *   data_table = "location_entity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer location entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/location_entity/{location_entity}",
 *     "add-page" = "/admin/structure/location_entity/add",
 *     "add-form" = "/admin/structure/location_entity/add/{location_entity_type}",
 *     "edit-form" = "/admin/structure/location_entity/{location_entity}/edit",
 *     "delete-form" = "/admin/structure/location_entity/{location_entity}/delete",
 *     "collection" = "/admin/structure/location_entity",
 *   },
 *   bundle_entity_type = "location_entity_type",
 *   field_ui_base_route = "entity.location_entity_type.edit_form"
 * )
 */
class LocationEntity extends ContentEntityBase implements LocationEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Business rules state if webDisplay AND status are TRUE then to display the
   * branch in location finder, otherwise it should never display in location
   * finder. There are some instances where branches will never display online,
   * but might accept online orders. The same can be said for the inverse; some
   * branches may display online, but never accept online orders, i.e. corporate
   * office, distribution hubs.
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // We only want to do this for branch type locations.
    if ($this->bundle() == 'branch') {
      $web_display = $this->get('field_web_display')->value;
      $status = $this->get('field_status')->value;

      if ($web_display == TRUE && $status == TRUE) {
        $this->setPublished(TRUE);
      }
      else {
        $this->setUnpublished();
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * Most of this is copied out of what core node module does regarding publish
   * status.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Location entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Location entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE);;

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
