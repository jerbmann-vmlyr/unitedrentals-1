<?php

namespace Drupal\locations\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Location type entity.
 *
 * @ConfigEntityType(
 *   id = "location_entity_type",
 *   label = @Translation("Location type"),
 *   handlers = {
 *     "list_builder" = "Drupal\locations\LocationEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\locations\Form\LocationEntityTypeForm",
 *       "edit" = "Drupal\locations\Form\LocationEntityTypeForm",
 *       "delete" = "Drupal\locations\Form\LocationEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\locations\LocationEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "location_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "location_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/location_entity_type/{location_entity_type}",
 *     "add-form" = "/admin/structure/location_entity_type/add",
 *     "edit-form" = "/admin/structure/location_entity_type/{location_entity_type}/edit",
 *     "delete-form" = "/admin/structure/location_entity_type/{location_entity_type}/delete",
 *     "collection" = "/admin/structure/location_entity_type"
 *   }
 * )
 */
class LocationEntityType extends ConfigEntityBundleBase implements LocationEntityTypeInterface {

  /**
   * The Location type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Location type label.
   *
   * @var string
   */
  protected $label;

}
