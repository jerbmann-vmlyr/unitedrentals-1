<?php

namespace Drupal\locations\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Location type entities.
 */
interface LocationEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
