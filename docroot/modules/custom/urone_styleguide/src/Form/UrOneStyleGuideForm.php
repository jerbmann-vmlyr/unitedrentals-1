<?php

namespace Drupal\urone_styleguide\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UrOneStyleGuideForm.
 */
class UrOneStyleGuideForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'urone_styleguide.uronestyleguide',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ur_one_style_guide_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('urone_styleguide.uronestyleguide');
    $form['hide_style_guide_content_type'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Style Guide content type?'),
      '#description' => $this->t('Toggle the visibility of the style guide content type.'),
      '#default_value' => $config->get('hide_style_guide_content_type'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('urone_styleguide.uronestyleguide')
      ->set('hide_style_guide_content_type', $form_state->getValue('hide_style_guide_content_type'))
      ->save();
  }

}
