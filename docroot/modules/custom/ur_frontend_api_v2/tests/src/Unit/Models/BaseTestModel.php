<?php

namespace Drupal\Tests\ur_frontend_api_v2\Unit\Models;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\ur_api_dataservice\DataService;

class BaseTestModel extends UnitTestCase {

  public function setUp() {
    parent::setUp();

    /*
     * All our models require this dataservice upon construction. This sets up
     * our a mock container and service.
     */
    $data_service = new DataService();
    $container = new ContainerBuilder();

    \Drupal::setContainer($container);
    $container->set('ur_api_dataservice', $data_service);
  }

}
