<?php

namespace Drupal\Tests\ur_frontend_api_v2\Unit\Models;

use Drupal\ur_frontend_api_v2\Models\Contract;

/**
 * Class ContractModelTest
 * @package Drupal\Tests\ur_frontend_api_v2\Unit\Models
 */
class ContractModelTest extends BaseTestModel {

  /**
   * @var \Drupal\ur_frontend_api_v2\Models\ChargeEstimate
   */
  protected $contractModel;

  public function setUp() {
    parent::setUp();

    $this->contractModel = $this->prophesize(Contract::class);
  }

  public function testImport() {
    $rawData = file_get_contents('../Data/ContractTestData.json');
    $mock_data = json_decode($rawData);

    $contract = Contract::import($mock_data);
    $this->assertInstanceOf(Contract::class, $contract);
  }

}
