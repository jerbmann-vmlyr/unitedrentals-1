<?php

namespace Drupal\Tests\ur_frontend_api_v2\Unit\Models;

use Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException;
use Drupal\ur_frontend_api_v2\Models\ChargeEstimate;
use Drupal\ur_frontend_api_v2\Models\ChargeEstimateItem;

/**
 * Class ChargeEstimateModelTest
 * @package Drupal\Tests\ur_frontend_api_v2\Unit\Models
 */
class ChargeEstimateModelTest extends BaseTestModel {

  /**
   * @var \Drupal\ur_frontend_api_v2\Models\ChargeEstimate
   */
  protected $chargeEstimateModel;

  public function setUp() {
    parent::setUp();

    $this->chargeEstimateModel = $this->prophesize(ChargeEstimate::class);
  }

  /**
   * Test
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   */
  public function testImport() {
    // Need to json encode, then decode this mimic the data this receives
    $mock_array = [
      'branchId' => '1',
      'accountId' => '123',
      'jobsiteId' => '11',
      'startDate' => '2018-10-24T16:00:00',
      'returnDate' => '2018-10-30T16:00:00',
      'delivery' => TRUE,
      'pickup' => TRUE,
      'items' => [
        '300-100' => [
          'quantity' => 1,
        ],
      ],
    ];

    $mock_data = json_decode(json_encode($mock_array));

    /** @var \Drupal\ur_frontend_api_v2\Models\ChargeEstimate $estimate */
    $estimate = ChargeEstimate::import($mock_data);
    $this->assertInstanceOf(ChargeEstimate::class, $estimate);

    foreach ($estimate->items as $charge_item) {
      $this->assertInstanceOf(ChargeEstimateItem::class, $charge_item);
    }
  }

  /**
   * Test we get the correct exception when "items" data is missing.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   */
  public function testRequiredItems() {
    $this->setExpectedException(MissingRequiredParametersException::class, 'Items are required.', 400);

    $mock_array = [
      'branchId' => '1',
      'accountId' => '123',
      'jobsiteId' => '11',
      'startDate' => '2018-10-24T16:00:00',
      'returnDate' => '2018-10-30T16:00:00',
      'delivery' => TRUE,
      'pickup' => TRUE,
    ];

    $mock_data = json_decode(json_encode($mock_array));
    $estimate = ChargeEstimate::import($mock_data);
  }

}
