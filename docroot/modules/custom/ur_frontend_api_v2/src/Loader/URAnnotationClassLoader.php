<?php

namespace Drupal\ur_frontend_api_v2\Loader;

use OpenApi\Analyser;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\Routing\Loader\AnnotationClassLoader;

/**
 * Class URAnnotationClassLoader.
 *
 * @package Drupal\ur_frontend_api_v2\Loader
 */
class URAnnotationClassLoader extends AnnotationClassLoader {

  /**
   * Annotation Class to use.
   *
   * @var string
   */
  protected $routeAnnotationClass = 'Swagger\\Annotations\\Swagger';

  /**
   * Namespace of the route annotation.
   *
   * @var string
   */
  protected $routeAnnotationNameSpace = 'Swagger\\Annotations\\Swagger';

  /**
   * Configure a route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to configure.
   * @param \ReflectionClass $class
   *   The class name to use.
   * @param \ReflectionMethod $method
   *   The method to use.
   * @param string $annot
   *   The annotation to use.
   */
  protected function configureRoute(Route $route, \ReflectionClass $class, \ReflectionMethod $method, $annot) {
    // TODO: Implement configureRoute() method if necessary.
    // This method is required to be implemented by the AnnotationClassLoader abstract class.
  }

  /**
   * Loads routes from annotations from a class.
   *
   * @param string $class
   *   A class name.
   * @param string|null $type
   *   The resource type.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   *   A RouteCollection instance.
   *
   * @throws \ReflectionException
   *   When route can't be parsed.
   */
  public function load($class, $type = NULL) {
    if (!class_exists($class)) {
      throw new \InvalidArgumentException(sprintf('Class "%s" does not exist.', $class));
    }

    $class = new \ReflectionClass($class);

    if ($class->isAbstract()) {
      throw new \InvalidArgumentException(sprintf('Annotations from class "%s" cannot be read as it is abstract.', $class->getName()));
    }

    $globals = $this->getGlobals($class);

    $collection = new RouteCollection();
    $collection->addResource(new FileResource($class->getFileName()));
    $analyzer = new Analyser();

    foreach ($class->getMethods() as $method) {
      $this->defaultRouteIndex = 0;
      $annotations = $analyzer->fromComment($method->getDocComment());

      foreach ($annotations as $annotation) {
        if (property_exists($annotation, 'path')) {
          $this->addRoute($collection, $annotation, $globals, $class, $method);
        }
      }
    }

    return $collection;
  }

  /**
   * Loads from annotations from a class.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   A collection of routes.
   * @param object $annot
   *   The annotation defining the route.
   * @param array $globals
   *   Global settings that are applied across all routes unless overridden.
   * @param \ReflectionClass $class
   *   The class to which the Annotation was attached.
   * @param \ReflectionMethod $method
   *   The method to which the Annotation was attached.
   */
  protected function addRoute(RouteCollection $collection, $annot, $globals, \ReflectionClass $class, \ReflectionMethod $method) {
    $name = $annot->operationId;

    if (NULL === $name) {
      $name = $this->getDefaultRouteName($class, $method);
    }

    $defaults = array_replace($globals['defaults'], ['_controller' => $class->getName() . '::' . $method->getName()]);
    $requirements = array_replace($globals['requirements'], ['_access' => 'TRUE']);
    $options = $globals['options'];
    $schemes = $globals['schemes'];
    $methods = $annot->method;

    $host = $globals['host'];
    $condition = $globals['condition'];

    $route = $this->createRoute($globals['path'] . $annot->path, $defaults, $requirements, $options, $host, $schemes, $methods, $condition);
    $this->configureRoute($route, $class, $method, $annot);

    $collection->add($name, $route);
  }

}
