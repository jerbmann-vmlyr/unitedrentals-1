<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Drupal\ur_frontend_api_v2\Models\ApiResponse;
use Drupal\ur_frontend_api_v2\Traits\ApiTrait;

/**
 * Class BaseController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class BaseController {
  use ApiTrait;

  /**
   * Build and return a response.
   *
   * @param $data
   *   The data to send back to the front end.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A formatted response.
   */
  public function respond($data) {
    $code = (count($data) > 0) ? 200 : 204;
    $service = \Drupal::service('ur_api_dataservice');
    $returnData = [
      'error' => FALSE,
      'code' => $code,
      'data' => $data,
      'dataSource' => get_class($service),
    ];

    if (array_key_exists('debug', $data)) {
      $returnData['debug'] = $data['debug'];
      unset($data['debug']);
    }

    $response = new ApiResponse($returnData);
    $response->setStatusCode($code);

    return $response;
  }

}
