<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Drupal\ur_api_dataservice\Exceptions\DalException;
use Drupal\ur_frontend_api_v2\Models\RentalTransaction;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RentalTransactionController
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class RentalTransactionController extends BaseController {

  /**
   * @OA\Tag(
   *   name="rentaltransactions",
   *   description="Rental transaction information per account.",
   *  @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get all rental transactions for an account.
   *
   * @OA\Get(
   *   path="/rentaltransactions",
   *   summary="Get a list of all rental transactions",
   *   tags={"rentaltransactions"},
   *   description="Get all rental transactions for a specified account, by their type (1,2,3,4,6)",
   *   operationId="getRentalTransactionsAction",
   *   @OA\Parameter(
   *     name="accountId",
   *     description="The id for the user account",
   *     in="query",
   *     required=true,
   *     @OA\Schema(type="number")
   *   ),
   *   @OA\Parameter(
   *     name="type",
   *     description="The type of transaction to retrieve (1,2,3,4,6)",
   *     in="query",
   *     required=true,
   *     @OA\Schema(type="number")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="accountId required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   security={{"basicAuth":{}}}
   * )
   *
   * @param Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function get(Request $request) {
    $accountId = $request->get('accountId');
    $type = $request->get('type');

    if (!$accountId && $type) {
      throw new DalException('Account ID and transaction type are required');
    }

    $data = RentalTransaction::index($accountId, $type);

    return $this->respond($data);
  }

  /**
   * Get a single rental transaction.
   *
   * @OA\Get(
   *   path="/rentaltransaction/{transId}",
   *   summary="Get a single rental transaction",
   *   tags={"rentaltransactions"},
   *   description="Get a rental transaction",
   *   operationId="getRentalTransactionAction",
   *   @OA\Parameter(
   *     name="transId",
   *     in="path",
   *     description="Id of the rental transaction to retrieve",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="trans id required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response="204",
   *     description="Transaction Not Found",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param $transId
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Exception
   */
  public function show($transId) {
    $data = RentalTransaction::find($transId);

    return $this->respond($data);
  }

  /**
   * Create a new Rental Transaction.
   *
   * @OA\Post(
   *   path="/rentaltransaction",
   *   summary="Add A New Rental Transaction via JSON Object",
   *   tags={"rentaltransactions"},
   *   description="Add A New Rental Transaction via JSON Object",
   *   operationId="postRentalTransactionAction",
   *   @OA\RequestBody(
   *     required=true,
   *     @OA\JsonContent(ref="#/components/schemas/RentalTransaction")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="successful operation",
   *     @OA\Schema(ref="#/components/schemas/RentalTransaction")
   *   ),
   * )
   *
   * @param Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function post(Request $request) {
    $data = json_decode($request->getContent());
    $trans = RentalTransaction::import($data);
    $response = $trans->save();

    return $this->respond($response);
  }

  /**
   * Update an identified transaction.
   *
   * @OA\Patch(
   *   path="/rentaltransaction/{transId}",
   *   summary="Update an existing rental transaction",
   *   tags={"rentaltransactions"},
   *   description="Update an existing rental transaction.  Updates any field
   *   provided",
   *   operationId="patchRentalTransactionAction",
   *   @OA\Parameter(
   *     name="transId",
   *     in="path",
   *     description="Id of the transaction to update",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="invalid data",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param $transId
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function patch(Request $request, $transId) {
    $newValues = $request->getContent();
    $trans = RentalTransaction::find($transId);
    $trans->updateValues($newValues);
    $response = $trans->save();

    return $this->respond($response);
  }

}
