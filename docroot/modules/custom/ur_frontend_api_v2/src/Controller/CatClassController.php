<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\ur_frontend_api_v2\Models\CatClass;


/**
 * Class CatClassController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class CatClassController extends BaseController {

  /**
   * @OA\Tag(
   *   name="catclasses",
   *   description="Cat Classes",
   *   @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get all cat classes.
   *
   * @OA\Get(
   *   path="/catclasses",
   *   summary="Get a list of all cat classes",
   *   tags={"catclasses"},
   *   description="Get all cat classes for the website",
   *   operationId="getCatClassesAction",
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="Something went very wrong",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Parameter(
   *     name="catclass",
   *     in="query",
   *     description="The catclass code.",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="maplevel",
   *     in="query",
   *     description="The map level.",
   *     required=false,
   *     @OA\Schema(
   *       type="number"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="status",
   *     in="query",
   *     description="Status of the catclass(es).",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="limit",
   *     in="query",
   *     description="Restrict the number of results.",
   *     required=false,
   *     @OA\Schema(
   *       type="number"
   *     )
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   */
  public function get(Request $request):JsonResponse {
    $params = $request->query->all();
    $data = CatClass::index($params);

    return $this->respond($data);
  }

}
