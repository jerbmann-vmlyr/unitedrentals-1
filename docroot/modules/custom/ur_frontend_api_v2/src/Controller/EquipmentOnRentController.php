<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Drupal\ur_frontend_api_v2\Models\EquipmentOnRent;


/**
 * Class EquipmentOnRentController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class EquipmentOnRentController extends BaseController {

  /**
   * Gets list of rented equpment for specified account.
   *
   * @OA\Get(
   *   path="/equipment/on-rent/{accountId}",
   *   summary="Get a list of rented equipment for specified account",
   *   tags={"equipmentOnRent"},
   *   description="Get a list of rented equipment for specified account",
   *   operationId="getEquipmentOnRentAction",
   *   @OA\Parameter(
   *     name="accountId",
   *     in="path",
   *     description="Account ID to retrieve rented equipment",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="accountId required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response="204",
   *     description="Account Not Found",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param $accountId
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   */
  public function get($accountId) {

    if (!$accountId) {
      throw new DalException('Account ID is required');
    }

    $params = $this->getRequest()->query->all();
    $useCache = !$params['useCache'] || strtolower($params['useCache']) === 'true';

    $equipmentOnRent = EquipmentOnRent::index($accountId, $params['type'], $params['transId'], $useCache);

    return $this->respond($equipmentOnRent);
  }

}
