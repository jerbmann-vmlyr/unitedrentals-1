<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Drupal\ur_frontend_api_v2\Models\PurchaseOrder;
use Drupal\ur_frontend_api_v2\Models\PurchaseOrderFormat;


/**
 * Class PurchaseOrderFormatsController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class PurchaseOrderController extends BaseController {

  /**
   * @OA\Tag(
   *   name="purchase-orders",
   *   description="Purchase Orders information per account.",
   *  @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Gets list of PO numbers pre-defined on a specific account.
   *
   *  @OA\Get(
   *   path="/purchase-orders/{accountId}",
   *   summary="Get list of Purchase Order Numbers for a specific account.",
   *   tags={"purchase-orders"},
   *   description="Returns list of pre-defined PO Numbers that the user will choose from to submit with their transaction/quote. </br><b>NOTE: This only applies to accounts with requirePo='V'</b>",
   *   operationId="getPurchaseOrdersAction",
   *   @OA\Parameter(
   *     name="accountId",
   *     in="path",
   *     description="Account Id",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="Account ID required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param string $accountId
   *   The accountId we are retrieving PO numbers for.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function get($accountId) {
    $data = PurchaseOrder::index($accountId);

    return $this->respond($data);
  }

  /**
   * Gets PO Formats for a specific account.
   *
   * @OA\Get(
   *   path="/purchase-orders/{accountId}/formats",
   *   summary="Get Purchase Order Formats for a specific account",
   *   tags={"purchase-orders"},
   *   description="Rather than choosing from a list, the user types the PO number and is validated against the formats returned from this endpoint.</br><b>NOTE: This only applies to accounts with requirePo='Y'</b>",
   *   operationId="getPurchaseOrderFormatsAction",
   *   @OA\Parameter(
   *     name="accountId",
   *     in="path",
   *     description="Account Id",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="Account ID required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param string $accountId
   *   The accountId we are retrieving PO Formats for.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getFormats($accountId) {
    $data = PurchaseOrderFormat::index($accountId);

    return $this->respond($data);
  }

}
