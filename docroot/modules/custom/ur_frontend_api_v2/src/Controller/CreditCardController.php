<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Symfony\Component\HttpFoundation\Request;
use Drupal\ur_frontend_api_v2\Models\CreditCard;


/**
 * Class CreditCardController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class CreditCardController extends BaseController {

  /**
   * @OA\Tag(
   *   name="creditcards",
   *   description="Credit Cards",
   *   @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get all credit cards.
   *
   * @OA\Get(
   *   path="/creditcards",
   *   summary="Get a list of all credit cards",
   *   tags={"creditcards"},
   *   description="Get all credit cards for authenticated user",
   *   operationId="getCreditCardsAction",
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="Token is required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function get() {
    $data = CreditCard::index();

    return $this->respond($data);
  }

  /**
   * Get a single card.
   *
   * @OA\Get(
   *   path="/creditcards/{cardId}",
   *   summary="Get a single credit card",
   *   tags={"creditcards"},
   *   description="Get a credit card",
   *   operationId="getSingleCreditCardAction",
   *   @OA\Parameter(
   *     name="cardId",
   *     in="path",
   *     description="Card id of the card to retrieve",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="card id required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response="204",
   *     description="Card Not Found",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param $cardId
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function show($cardId) {
    $card = CreditCard::find($cardId);

    return $this->respond($card->toArray());
  }

  /**
   * Create a new Card.
   *
   * @OA\Post(
   *   path="/creditcards",
   *   summary="Add A New Credit Card via JSON Object",
   *   tags={"creditcards"},
   *   description="Add A New Credit Card via JSON Object",
   *   operationId="postCreditCardAction",
   *   @OA\RequestBody(
   *     required=true,
   *     @OA\JsonContent(ref="#/components/schemas/CreditCard")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="successful operation",
   *     @OA\Schema(ref="#/components/schemas/CreditCard")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\DataException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function post(Request $request) {
    $data = json_decode($request->getContent());
    $card = CreditCard::import($data);
    $response = $card->save();

    return $this->respond($response);
  }

  /**
   * Update an identified card.
   *
   * @OA\Patch(
   *   path="/creditcards/{cardId}",
   *   summary="Update an existing credit card",
   *   tags={"creditcards"},
   *   description="Update an existing credit card.  Updates any field
   *   provided",
   *   operationId="patchCreditCardAction",
   *   @OA\Parameter(
   *     name="cardId",
   *     in="path",
   *     description="Card id of the card to update",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="invalid data",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param $cardId
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\DataException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function patch(Request $request, $cardId) {
    $newValues = $request->getContent();
    $card = CreditCard::find($cardId);
    $card->updateValues($newValues);
    $response = $card->save();

    return $this->respond($response);
  }

  /**
   * Delete the given card.
   *
   * @OA\Delete(
   *   path="/creditcards/{cardId}",
   *   summary="delete the specified credit card",
   *   tags={"creditcards"},
   *   description="",
   *   operationId="deleteCreditCardAction",
   *   @OA\Parameter(
   *     name="cardId",
   *     in="path",
   *     description="Card id of the card to delete",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Success",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="card id required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=204,
   *     description="card not found",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param string $cardId
   *   The card ID to delete.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function delete($cardId) {
    $card = CreditCard::find($cardId);
    $card->delete();

    return $this->respond([$cardId]);
  }

}
