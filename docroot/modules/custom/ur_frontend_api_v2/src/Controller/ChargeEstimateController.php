<?php

namespace Drupal\ur_frontend_api_v2\Controller;
use Drupal\ur_frontend_api_v2\Models\ChargeEstimateItem;
use Drupal\ur_frontend_api_v2\Models\ChargeEstimate;
use Drupal\ur_frontend_api_v2\Models\EquipmentOwned;
use Drupal\ur_frontend_api_v2\Models\Voucher;
use Symfony\Component\HttpFoundation\Request;
use Voucherify\ClientException;

/**
 * Class ChargeEstimateController
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class ChargeEstimateController extends BaseController {

  /**
   * @OA\Tag(
   *   name="charge-estimates",
   *   description="This API retrieves charge estimates for a given reservation. A reservation consists of the equipment codes (cat-class),quantities, and the rental rates.",
   * @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get charge estimates.
   *
   * @OA\Post(
   *   path="/charge-estimates",
   *   summary="Get a charge estimate for a reservation. This requires the Start & Return dates, the Branch ID for the transaction, and the equipment (cat-class) codes along with their associated quantities for the reservation.",
   *   tags={"charge-estimates"},
   *   description="Retrieve charge estimates for a given reservation.",
   *   operationId="postChargeEstimateAction",
   *   @OA\RequestBody(
   *     required=true,
   *     @OA\JsonContent(ref="#/components/schemas/ChargeEstimate")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\JsonContent(
   *       allOf={
   *         @OA\Schema(ref="#/components/schemas/ApiResponse"),
   *         @OA\Schema(
   *           required={"data"},
   *           @OA\Property(property="data", ref="#/components/schemas/ChargeEstimate")
   *         )
   *       }
   *     )
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="Missing required parameters",
   *     @OA\JsonContent(
   *       allOf={
   *         @OA\Schema(ref="#/components/schemas/ApiResponse"),
   *         @OA\Schema(
   *           required={"code"},
   *           @OA\Property(
   *             type="integer",
   *             format="int32",
   *             property="code",
   *             default=400,
   *             example=400
   *           )
   *         ),
   *         @OA\Schema(
   *           required={"error"},
   *           @OA\Property(
   *             type="boolean",
   *             property="error",
   *             default=true
   *           )
   *         ),
   *         @OA\Schema(
   *           required={"message"},
   *           @OA\Property(
   *             type="string",
   *             property="message",
   *             example="Error message here."
   *           )
   *         ),
   *         @OA\Schema(
   *           required={"data"},
   *           @OA\Property(
   *             type="array",
   *             property="data",
   *             @OA\Items({}),
   *             example={}
   *           )
   *         )
   *       }
   *     )
   *   )
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\DataException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function post(Request $request) {
    $this->setRequest($request);
    $data = (object) $this->getRequestParams();

    $useCache = isset($data->useCache) ? (bool) $data->useCache : TRUE;
    unset($data->useCache);

    $branchId = $this->getRequestValue('branchId');
    $items = $this->getRequestValue('items');

    // Making a comma separated string from catclasses for Equipment Owned CatClass API
    $catClasses = array_keys($items);
    $catClassList = implode(',', $catClasses);

    $branchItems = EquipmentOwned::read($branchId, $catClassList);
    $availableItems = $unavailableItems = [];

    foreach ($branchItems as $item) {
      $availableItems[] = $item->catClass;
    }

    foreach ($catClasses as $idx => $catClass) {
      // Set the flag as TRUE if the item is available in the current branchID
      $items[$catClass]['availableEquip'] = TRUE;
      $items[$catClass]['catClass'] = $catClass;

      if (!\in_array($catClass, $availableItems, FALSE)) {
        // Step 1: Making mock charge-estimates data for unavailable items in current branch
        $unavailableItem = ChargeEstimateItem::import($items[$catClass]);

        // Step 2: Set all rate values to ZERO($0), if item is unavailable
        $unavailableItem->setTotalsToZero();

        // Step 3: Keep the original catClass and quantity in unavailable array.
        // Should be pushed back to the result after getting charge estimates for available items.
        $unavailableItems[] = $unavailableItem;

        // Step 4: Unset the item from data if catClass is not available in the current branchID
        unset($items[$catClass]);
      }
    }

    $data->items = (object) $items;

    /** @var \Drupal\ur_frontend_api_v2\Models\ChargeEstimate $estimate */
    $estimate = ChargeEstimate::import($data);

    if (!empty($items)) {
      $estimate = $estimate->create(FALSE);
    }

    // Add unavailable items back to estimate object.
    $availableItemsWithEstimate = $estimate->items;

    // Merging available and unavailable items to return all items, with rates.
    $allItems = array_merge($availableItemsWithEstimate, $unavailableItems);
    $estimate->updateValue('items', $allItems);

    // retrieve Voucherify voucher and update item rates, if applicable
    if ($data->promotionCode && \Drupal::service('module_handler')->moduleExists('ur_voucher')) {
      $voucher = Voucher::retrieve($data->promotionCode);
      if ($voucher->status) {
        $voucherEstimate = Voucher::apply($estimate, $voucher);
        $estimate->updateValue('items', $voucherEstimate->items);
        // NOTE: webRates must be false, else DAL ignores our discounted rates
        $estimate->webRates = FALSE;
        $estimate = $estimate->create(FALSE);
      }
      $response = $estimate->toArray();
      $response['voucher'] = $voucher;
    }
    else {
      $response = $estimate->toArray();
    }

    return $this->respond($response);
  }

}
