<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Drupal\ur_frontend_api_v2\Models\EquipmentOwned;
use Symfony\Component\HttpFoundation\Request;

use Drupal\ur_api_dataservice\Exceptions\NoResultsException;

/**
 * Class EquipmentOwnedController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class EquipmentOwnedController extends BaseController {

  /**
   * @OA\Tag(
   *   name="equipmentowned",
   *   description="Equipment CatClass Availability",
   *   @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get all available catClasses in branch and its count.
   *
   * @OA\Get(
   *   path="/catclassesowned",
   *   summary="Get a list of all available catclasses and the count in the branch",
   *   tags={"equipmentowned"},
   *   description="Get a list of all available catclasses and the count in the branch",
   *   operationId="getAvailableCatClasses",
   *   @OA\Parameter(
   *     name="branchId",
   *     in="query",
   *     description="Retrieve all available catclasses which branchId is ...",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="catClass",
   *     in="query",
   *     description="Retrieve branchId's available catclasses from this comma separated value",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="catClass and branchId are required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\NoResultsException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function get(Request $request) {
    $data = EquipmentOwned::read($request->get('branchId'), $request->get('catClass'));

    if ($data) {
      return $this->respond($data);
    }

    // Means we didn't get any available items in this branch data and that is bad.
    throw new NoResultsException('No Available Equipment', 204);
  }

}
