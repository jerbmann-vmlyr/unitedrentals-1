<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Drupal\ur_frontend_api_v2\Models\Geodata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GeodataController
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class GeodataController extends BaseController {

  /**
   * @OA\Tag(
   *   name="geodata",
   *   description="Access to all geospacial data required by the presentation layer.",
   *   @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * @OA\Get(
   *   path="/geodata/{type}",
   *   summary="Get a list all geo-data for the specified type.",
   *   tags={"geodata"},
   *   description="Get geodata for a specified type.",
   *   operationId="getGeodataAction",
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="Something went very wrong",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Parameter(
   *     name="accountId",
   *     in="query",
   *     description="Used to limit geodata to one account.",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="equipmentId",
   *     in="query",
   *     description="Used to limit geodata to one piece of equipment.",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="dataType",
   *     in="path",
   *     description="Retrieve all geodata for this type.",
   *     required=true,
   *     @OA\Schema(
   *       type="string",
   *       example="telematics"
   *     )
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param $dataType
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   */
  public function get(Request $request, $dataType) {
    $vars = NULL;

    if ($request->query->count()) {
      $vars = $request->query->all();
    }

    $data = Geodata::index($dataType, $vars);

    return $this->respond($data);
  }

}
