<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Symfony\Component\HttpFoundation\Request;
use Drupal\ur_frontend_api_v2\Models\Branch;
use Drupal\ur_user\Controller\UserController;

/**
 * Class BranchController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class BranchController extends BaseController {

  /**
   * Get a collection of branches.
   *
   * @OA\Tag(
   *   name="branches",
   *   description="Branches",
   * @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   *
   * Get a collection of branches.
   *
   * @OA\Get(
   *   path="/branches",
   *   summary="Get a list of branches",
   *   tags={"branches"},
   *   description="Get branches for authenticated user.",
   *   operationId="getBranchesAction",
   *   @OA\Parameter(
   *     name="city",
   *     in="query",
   *     description="The city",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="state",
   *     in="query",
   *     description="The state",
   *     required=false,
   *     @OA\Schema(
   *       type="boolean"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="zip",
   *     in="query",
   *     description="The zip",
   *     required=false,
   *     @OA\Schema(
   *       type="boolean"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="latitude",
   *     in="query",
   *     description="the location latitude",
   *     required=false,
   *     @OA\Schema(
   *       type="boolean"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="longitude",
   *     in="query",
   *     description="the location longitude",
   *     required=false,
   *     @OA\Schema(
   *       type="boolean"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="unit",
   *     in="query",
   *     description="Units for distance",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="limit",
   *     in="query",
   *     description="Limit for number of branch results",
   *     required=false,
   *     @OA\Schema(
   *       type="number"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Branch location parameters.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns collection of branches
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function get(Request $request) {
    $params = $request->query->all();
    if ($params) {
      $data = Branch::getBranchesByLocation($params);

      // Set closest branch in SESSION.
      if (!empty($data) && isset($data[0]) && !empty($data[0])) {
        $closestBranch = new \stdClass();
        $closestBranch->branchId = $data[0]->branchId;
        $closestBranch->region = $data[0]->region;
        $_SESSION['closest_branch'] = $closestBranch;
        $userController = new UserController();
        $userController->update('ratesPlaceBranch', $closestBranch->branchId);
      }
    }
    else {
      $data = Branch::index();
    }

    return $this->respond($data);
  }

  /**
   * Get a single branch.
   *
   * @OA\Get(
   *   path="/branch/{id}",
   *   summary="Get a single branch",
   *   tags={"branches"},
   *   description="Get branch by its id (branchId).",
   *   operationId="getBranchesAction",
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Retrieve the branch with this id",
   *     required=false,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   )
   * )
   *
   * @param string $id
   *   Branch ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns JSON response of the branch location entity
   */
  public function show($id) {
    $branch = Branch::find($id);

    return $this->respond($branch->toArray());
  }

  /**
   * Get a collection of default branches.
   *
   * @OA\Tag(
   *   name="branches-default",
   *   description="Default Branches",
   * @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get a collection of default branches.
   *
   * @OA\Get(
   *   path="/branches/default",
   *   summary="Get a list of default branches",
   *   tags={"branches-default"},
   *   description="Get default branches",
   *   operationId="getBranchesAction",
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns JSON response of the branches array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getBranches() {
    $data = Branch::getBranchesDefault();
    return $this->respond($data);
  }

}
