<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Drupal\ur_api_dataservice\Exceptions\DalException;
use Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException;
use Drupal\ur_api_dataservice\Exceptions\NoResultsException;
use Drupal\ur_api_dataservice\Exceptions\ParameterTypeException;
use Symfony\Component\HttpFoundation\Request;
use Drupal\ur_frontend_api_v2\Models\Rates;

/**
 * Class RatesController
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class RatesController extends BaseController {

  /**
   * @OA\Tag(
   *   name="rates",
   *   description="rates",
   * @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get a collection of rates.
   *
   * @OA\Get(
   *   path="/rates",
   *   summary="Get a list of job sites for specified account.",
   *   tags={"rates"},
   *   description="Get rates for specified account.",
   *   operationId="get",
   *   @OA\Parameter(
   *     name="branchId",
   *     in="query",
   *     description="Retrieve all rental rates for this branchId.",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Parameter(
   *     name="catClass",
   *     description="Cat/Class",
   *     in="query",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Parameter(
   *     name="accountId",
   *     description="Customer Account Number",
   *     in="query",
   *     required=false,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Parameter(
   *     name="deliveryDistance",
   *     description="Delivery Distance",
   *     in="query",
   *     required=false,
   *     @OA\Schema(type="integer")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param $branchId
   * @param $catClass
   * @param $accountId
   * @param $deliveryDistance
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function get() {
    $params = \Drupal::request()->query->all();
    $data = Rates::index(
      $params['branchId'], $params['catClass'], $params['accountId'], $params['deliveryDistance']
    );
    return $this->respond($data);
  }

}
