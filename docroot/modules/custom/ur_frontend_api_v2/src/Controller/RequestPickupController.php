<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\ur_frontend_api_v2\Models\RequestPickup;

/**
 * Class RequestPickupController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class RequestPickupController extends BaseController {
  /**
   * @OA\Tag(
   *   name="requestpickup",
   *   description="Request Pickup",
   *   @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get the pickup request data.
   *
   * @OA\Get(
   *   path="/requestpickup",
   *   summary="Get all the details needed to submit a pickup request",
   *   tags={"requestpickup"},
   *   description="Get the required pickup request data from the DAL",
   *   operationId="getPickupRequestAction",
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="Something went very wrong",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function get(Request $request):JsonResponse {
    $params = $request->query->all();
    $results = RequestPickup::index($params[transId], $params[equipmentId], $params[phone], $params[email], $params[accountId]);
    return $this->respond($results);
  }

}
