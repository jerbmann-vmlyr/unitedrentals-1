<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Drupal\ur_api_dataservice\Exceptions\DalException;
use Drupal\ur_frontend_api_v2\Models\RentalRequisition;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RentalRequisitionController
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class RentalRequisitionController extends BaseController {

  /**
   * @OA\Tag(
   *   name="rentalrequisitions",
   *   description="Rental requisition information per account.",
   *  @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get all rental requisitions for an account and statuses.
   *
   * @OA\Get(
   *   path="/rentalrequisitions/{id}/{status}",
   *   summary="Get a list of all rental requisitions",
   *   tags={"rentalrequisitions"},
   *   description="Get all rental requisitions for a specified account and one/more status(es) (comma separated)",
   *   operationId="getRentalRequisitionsAction",
   *   @OA\Parameter(
   *     name="id",
   *     description="The id for the user account",
   *     in="path",
   *     required=true,
   *     @OA\Schema(type="number")
   *   ),
   *   @OA\Parameter(
   *     name="status",
   *     description="The status of requisition to retrieve (QR,OR,OA,OC,OD,QT,QC,QD,RR,RS,RC,RD)",
   *     in="path",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="accountId and status are required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   security={{"basicAuth":{}}}
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function get(Request $request) {
    $id = $request->get('id');
    $status = $request->get('status');

    if (!$id && $status) {
      throw new DalException('Account ID and Status Code are required');
    }

    $data = RentalRequisition::index($id, $status);

    return $this->respond($data);
  }

  /**
   * Get a single rental requisition.
   *
   * @OA\Get(
   *   path="/rentalrequisition/{requestId}",
   *   summary="Get a single rental requisition",
   *   tags={"rentalrequisitions"},
   *   description="Get a rental requisition",
   *   operationId="getRentalRequisitionAction",
   *   @OA\Parameter(
   *     name="requestId",
   *     in="path",
   *     description="Request id of the requisition to retrieve",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="Account id and requestId are required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response="204",
   *     description="Transaction Not Found",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param $requestId
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Exception
   */
  public function show($requestId) {
    $data = RentalRequisition::find($requestId);

    return $this->respond($data);
  }

  /**
   * Create a new Rental Requisition.
   *
   * @OA\Post(
   *   path="/rentalrequisition",
   *   summary="Add A New Rental Requisition via JSON Object",
   *   tags={"rentalrequisition"},
   *   description="Add A New Rental Requisition via JSON Object",
   *   operationId="postRentalRequisitionAction",
   *   @OA\RequestBody(
   *     required=true,
   *     @OA\JsonContent(ref="#/components/schemas/RentalRequisition")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="successful operation",
   *     @OA\Schema(ref="#/components/schemas/RentalRequisition")
   *   ),
   * )
   *
   * @param Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Exception
   */
  public function post(Request $request) {
    $data = json_decode($request->getContent());
    $requisition = RentalRequisition::import($data);
    $response = $requisition->save();

    return $this->respond($response);
  }

  /**
   * Update an identified requisition.
   *
   * @OA\Patch(
   *   path="/rentalrequisition/{requestId}",
   *   summary="Update an existing rental requisition",
   *   tags={"rentalrequisitions"},
   *   description="Update an existing rental requisition.  Updates any field
   *   provided",
   *   operationId="patchRentalRequisitionAction",
   *   @OA\RequestBody(
   *     @OA\JsonContent(ref="#/components/schemas/RentalRequisition")
   *   ),
   *   @OA\Parameter(
   *     name="requestId",
   *     in="path",
   *     description="Id of the requisition to update",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="Invalid data",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param $requestId
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Exception
   */
  public function patch(Request $request, $requestId) {
    $newValues = $request->getContent();
    $requisition = RentalRequisition::find($requestId);
    $requisition->updateValues($newValues);
    $response = $requisition->save();

    return $this->respond($response);
  }

}
