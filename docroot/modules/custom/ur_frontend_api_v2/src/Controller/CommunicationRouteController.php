<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Symfony\Component\HttpFoundation\Request;
use Drupal\ur_frontend_api_v2\Models\CommunicationRoute;

/**
 * Class EquipmentController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class CommunicationRouteController extends BaseController {

  /**
   * Get the landing page based on given url parameters
   *
   * @OA\Get(
   *   path="/messaging/router",
   *   summary="Map a message url to a workplace url",
   *   tags={"communication"},
   *   description="Get a new location based on accountId, requisitionId, and
   *   catClass",
   *   operationId="getCommunicationRouteAction",
   *   @OA\Parameter(
   *   name="messageType",
   *     in="query",
   *     description="Type of message",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="accountId",
   *     in="query",
   *     description="Account ID",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="requisitionId",
   *     in="query",
   *     description="Account ID",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="catClass",
   *     in="query",
   *     description="Account ID",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="missing required parameters",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response="204",
   *     description="Route Not Found",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function show(Request $request) {
    $params = $request->query->all();

    $route = CommunicationRoute::find(
      $params['messageType'], $params['accountId'],
      $params['requisitionId'], $params['catClass'],
      $params['transLineId']
    );

    return $this->respond($route->toArray());
  }

}
