<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\views\Views;
use Drupal\locations\Plugin\views\field\DistanceCalculation;
use Drupal\Core\Url;
use Drupal\locations\Entity\LocationEntity;

/**
 * Class GetBranches.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class GetBranches extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function returnBranches() {
    $points = [];

    $query = \Drupal::entityQuery('location_entity')
      ->condition('type', 'branch')
      ->condition('status', 1);

    if (isset($_GET['state'])) {
      $query->condition('field_address.administrative_area', $_GET['state']);

      if (isset($_GET['city'])) {
        $city = strtoupper(str_replace('-', ' ', $_GET['city']));
        $query->condition('field_address.locality', $city);
      }

      if (isset($_GET['branch'])) {
        $branch = $_GET['branch'];
        $query->condition('field_branch_id', $branch);
      }

      if (isset($_GET['type'])) {
        $terms = Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'location_branches', 'field_branch_code' => $_GET['type']]);
        $term = reset($terms);
        if ($term) {
          $query->condition('field_primary_business_type.target_id', $term->id());
        }
      }

      $ids = $query->execute();
      $nodes = \Drupal::entityTypeManager()->getStorage('location_entity')->loadMultiple($ids);

      if (!empty($nodes)) {
        foreach ($nodes as $node) {
          $points[] = [
            'latitude' => $node->get('field_latitude')->value,
            'longitude' => $node->get('field_longitude')->value,
          ];
        }
      }
      else {
        $continue = FALSE;

        if ($_GET['state'] && !$_GET['branch']) {
          $continue = TRUE;
          $locality = FALSE;
          $type = 'state_province';
          $state = 'field_address.administrative_area';
          $state_value = $_GET['state'];

          if ($_GET['city']) {
            $type = 'city';
            $locality = 'field_address.locality';
            $locality_value = strtoupper(str_replace('-', ' ', $_GET['city']));
          }
        }

        if ($continue) {
          $query = \Drupal::entityQuery('location_entity')
            ->condition('type', $type)
            ->condition('status', 1)
            ->condition($state, $state_value);

          if ($locality) {
            $query->condition($locality, $locality_value);
          }

          $ids = $query->execute();
          $nodes = \Drupal::entityTypeManager()->getStorage('location_entity')->loadMultiple($ids);

          if (!empty($nodes)) {
            foreach ($nodes as $node) {
              $points[] = [
                'latitude' => $node->get('field_geolocation')->getValue()[0]['lat'],
                'longitude' => $node->get('field_geolocation')->getValue()[0]['lng'],
              ];
            }
          }
          else {
            $alias = \Drupal::service('path.alias_manager')->getPathByAlias('/locations/' . $_GET['state'] . '/' . $_GET['city']);
            $params = Url::fromUri("internal:" . $alias)->getRouteParameters();
            $entity_type = key($params);
            $node = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);

            if ($node) {
              $points[] = [
                'latitude' => $node->get('field_geolocation')->getValue()[0]['lat'],
                'longitude' => $node->get('field_geolocation')->getValue()[0]['lng'],
              ];
            }
          }
        }
      }
    }

    $response = JsonResponse::create($points);
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function checkEntity() {
    $type = NULL;
    $query = \Drupal::entityQuery('location_entity')
      ->condition('status', 1);

    if ($_GET['city']) {
      $type = 'city';
      $city = strtoupper(str_replace('-', ' ', $_GET['city']));
      $query->condition('type', 'city');
      $query->condition('field_address.administrative_area', $_GET['state']);
      $query->condition('field_address.locality', $city);
    }
    elseif ($_GET['state']) {
      $type = 'state_province';
      $query->condition('type', 'state_province');
      $query->condition('field_address.administrative_area', $_GET['state']);
    }

    $data = ['success' => FALSE];
    if ($type) {
      $ids = $query->execute();
      $nodes = \Drupal::entityTypeManager()->getStorage('location_entity')->loadMultiple($ids);

      if (!empty($nodes)) {
        $data['success'] = TRUE;
      }
      else {
        $address = [
          'administrative_area' => $_GET['state'],
          'country_code' => $_GET['countrycode'],
        ];

        if ($type === 'city') {
          $address['locality'] = $city;
        }

        $lat = $_GET['lat'];
        $lng = $_GET['lng'];

        $location_entity = LocationEntity::create(['type' => $type]);
        $location_entity->set('field_address', $address);
        $location_entity->set('field_geolocation', ['lat' => $lat, 'lng' => $lng]);
        $location_entity->set('type', $type);
        $location_entity->save();
        $data['success'] = TRUE;
      }
    }

    $response = JsonResponse::create($data);
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public static function createEntity($lat = FALSE, $lng = FALSE, $state = FALSE, $city = FALSE) {
    $nid = 0;
    if ($lat && $lng) {
      $type = NULL;
      $query = \Drupal::entityQuery('location_entity')
        ->condition('status', 1);

      if ($city) {
        $type = 'city';
        $city = strtoupper(str_replace('-', ' ', $city));
        $query->condition('type', 'city');
        $query->condition('field_address.administrative_area', $state);
        $query->condition('field_address.locality', $city);
      }
      elseif ($state) {
        $type = 'state_province';
        $query->condition('type', 'state_province');
        $query->condition('field_address.administrative_area', $state);
      }

      if ($type) {
        $ids = $query->execute();

        if (!empty($ids)) {
          $nid = reset($ids);
        }
        else {
          $address = [
            'administrative_area' => $state,
            'country_code' => 'US',
          ];

          if ($type === 'city') {
            $address['locality'] = $city;
          }

          $location_entity = LocationEntity::create(['type' => $type]);
          $location_entity->set('field_address', $address);
          $location_entity->set('field_geolocation', ['lat' => $lat, 'lng' => $lng]);
          $location_entity->set('type', $type);
          $location_entity->save();

          $nid = $location_entity->id();
        }
      }
    }

    return $nid;
  }

  /**
   * {@inheritdoc}
   */
  public function returnListing() {
    $args = [];
    $response = [
      'success' => FALSE,
    ];
    if (!isset($_GET['city'])) {
      $args = [$_GET['state']];

      $view = Views::getView('location_listing');
      if (is_object($view)) {
        $view->setArguments($args);
        $view->setDisplay('state_branch_listing');
        $view->preExecute();
        $view->execute();
        $renderable = $view->buildRenderable('state_branch_listing', $args);
        $response = render(self::sort_by_distance($renderable));
      }
    }
    else {
      $view = Views::getView('location_listing');
      if (is_object($view)) {
        $view->setArguments($args);
        $view->setDisplay('branch_listing');
        $view->preExecute();
        $view->execute();
        $renderable = $view->buildRenderable('branch_listing', $args);
        $response = render(self::sort_by_distance($renderable));
      }
    }

    return new Response($response);
  }

  /**
   * Sort renderable array based on distance.
   */
  public static function sort_by_distance(&$renderable) {
    $results = [];
    $current_location = \Drupal::routeMatch()->getParameter('location_entity');
    if (!$current_location) {
      $route = $_SERVER['HTTP_REFERER'];
      $parts = explode('/', str_replace('https://', '', $route));
      unset($parts[0]);
      if ($parts[1] === 'locations') {
        $path = '';
        foreach ($parts as $part) {
          $pieces = explode('?', $part);
          $path .= '/' . $pieces[0];
        }

        $alias = \Drupal::service('path.alias_manager')->getPathByAlias($path);
        $params = Url::fromUri("internal:" . $alias)->getRouteParameters();
        $entity_type = key($params);
        $current_location = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);
      }
    }
    $unit = 'M';
    $i = 0;

    foreach ($renderable['#view']->result as $value) {
      $location = $value->_entity;
      $lat1 = $current_location->get('field_geolocation')->getValue()[0]['lat'];
      $lng1 = $current_location->get('field_geolocation')->getValue()[0]['lng'];
      $lat2 = $location->get('field_geolocation')->getValue()[0]['lat'];
      $lng2 = $location->get('field_geolocation')->getValue()[0]['lng'];
      $results[$i] = DistanceCalculation::distance($lat1, $lng1, $lat2, $lng2, $unit);
      $i++;
    }

    asort($results);
    $sorted = [];
    foreach ($results as $key => $value) {
      $sorted[] = $renderable['#view']->result[$key];
    }

    $renderable['#view']->result = $sorted;

    return $renderable;
  }

}
