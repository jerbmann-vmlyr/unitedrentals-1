<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Symfony\Component\HttpFoundation\Request;
use Drupal\ur_frontend_api_v2\Models\Jobsite;

/**
 * Class AccountController
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class JobsiteController extends BaseController {

  /**
   * @OA\Tag(
   *   name="jobsites",
   *   description="Jobsites",
   * @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get a collection of jobsites.
   *
   * @OA\Get(
   *   path="/jobsites/{accountId}",
   *   summary="Get a list of job sites for specified account.",
   *   tags={"jobsites"},
   *   description="Get job sites for specified account.",
   *   operationId="getJobsitesAction",
   *   @OA\Parameter(
   *     name="accountId",
   *     in="path",
   *     description="Retrieve all job sites for this accountId.",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param $accountId
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function get($accountId) {
    $data = Jobsite::index($accountId);

    return $this->respond($data);
  }

  /**
   * Get a single jobsite.
   *
   * @OA\Get(
   *   path="/jobsite/{accountId}/{id}",
   *   summary="Get a single job site",
   *   tags={"jobsites"},
   *   description="Get a job site",
   *   operationId="getSingleJobsiteAction",
   *   @OA\Parameter(
   *     name="accountId",
   *     in="path",
   *     description="Id of the account for this job site",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id of the job site to retrieve",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="job site id required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response="204",
   *     description="Job site Not Found",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param $accountId
   * @param $id
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function show($accountId, $id) {
    $jobsite = Jobsite::find($accountId, $id);

    return $this->respond($jobsite->toArray());
  }


  /**
   * Create a new Jobsite.
   *
   * @OA\Post(
   *   path="/jobsite",
   *   summary="Add A New Jobsite via JSON Object",
   *   tags={"jobsites"},
   *   description="Add A New Credit Card via JSON Object",
   *   operationId="postJobsiteAction",
   *   @OA\RequestBody(
   *     required=true,
   *     @OA\JsonContent(ref="#/components/schemas/Jobsite")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/components/schemas/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function post(Request $request) {
    $data = json_decode($request->getContent());
    $jobsite = Jobsite::import($data);
    $response = $jobsite->save();

    return $this->respond($response);
  }

  /**
   * Update an identified jobsite.
   *
   * @OA\Patch(
   *   path="/jobsite/{id}",
   *   summary="Update an existing jobsite",
   *   tags={"jobsites"},
   *   description="Update an existing jobsite.  Updates any field
   *   provided",
   *   operationId="patchJobsiteAction",
   *   @OA\RequestBody(
   *     @OA\JsonContent(ref="#/components/schemas/Jobsite")
   *   ),
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id of the jobsite to update",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/components/schemas/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="invalid data",
   *     @OA\Schema(ref="#/components/schemas/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param $id
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function patch(Request $request, $id) {
    $jobsite = Jobsite::find($id);
    $jobsite->updateValues($request->getContent());
    $response = $jobsite->save();

    return $this->respond($response);
  }

}
