<?php

namespace Drupal\ur_frontend_api_v2\Controller;


use Drupal\ur_frontend_api_v2\Models\EquipmentItem;

/**
 * Class EquipmentController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class EquipmentController extends BaseController {

  /**
   * Get details for a single piece of equipment.
   *
   * @OA\Get(
   *   path="/equipment/{equipId}",
   *   summary="Get a single piece of equipment",
   *   tags={"equipment"},
   *   description="Get a single piece of equipment",
   *   operationId="getSingleEquipmentAction",
   *   @OA\Parameter(
   *     name="equipId",
   *     in="path",
   *     description="Equipment id of the equipment to retrieve",
   *     required=true,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="equipment id required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response="204",
   *     description="Equipment Not Found",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param $equipId
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function show($equipId) {
    $equipment = EquipmentItem::find($equipId);

    return $this->respond($equipment->toArray());
  }

}
