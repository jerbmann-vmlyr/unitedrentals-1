<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\ur_frontend_api_v2\Models\Account;

/**
 * Class AccountController
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class AccountController extends BaseController {

  /**
   * @OA\Tag(
   *   name="accounts",
   *   description="Accounts",
   *   @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get Accounts
   *
   * @OA\Schema(
   *   schema="AccountsResponse",
   *   description="A Accounts Response",
   *   type="object",
   *   allOf={
   *     @OA\Schema(ref="#/components/schemas/ApiResponse"),
   *     @OA\Schema(
   *       required={"data"},
   *       @OA\Property(
   *         type="array",
   *         property="data",
   *         @OA\Items(ref="#/components/schemas/Account")
   *       )
   *     )
   *   }
   * )
   */

   /**
   * @OA\Get(
   *   path="/accounts",
   *   summary="Get a list of all accounts",
   *   tags={"accounts"},
   *   description="Get all accounts for authenticated user",
   *   operationId="getAccountsAction",
   *   @OA\Parameter(
   *     name="name",
   *     in="query",
   *     description="Retrieve all accounts with this customer name",
   *     required=false,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Parameter(
   *     name="name_filter",
   *     in="query",
   *     description="Retrieve all accounts who's customer name contains ...",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="city",
   *     in="query",
   *     description="Retrieve all accounts who's city is ...",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="state",
   *     in="query",
   *     description="Retrieve all accounts who's state is ...",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="zip",
   *     in="query",
   *     description="Retrieve all accounts who's zip is ...",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="parentId",
   *     in="query",
   *     description="Retrieve all accounts who's parentId is ...",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="parentId_filter",
   *     in="query",
   *     description="Additional parameter where parentId is equal to (eq), less than (lt) greather than (gt), ...",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="status",
   *     in="query",
   *     description="Retrieve all accounts who's status is ...",
   *     required=false,
   *     @OA\Schema(
   *       type="string"
   *     )
   *   ),
   *   @OA\Parameter(
   *     name="limit",
   *     in="query",
   *     description="Record retrieval limit",
   *     required=false,
   *     @OA\Schema(
   *       type="integer"
   *     )
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\JsonContent(ref="#/components/schemas/AccountsResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="account id required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function get(Request $request) {
    $params = $request->query->all();
    $data = Account::index($params);

    return $this->respond($data);
  }

  /**
   * Get a single account.
   *
   * @OA\Get(
   *   path="/accounts/{accountId}",
   *   summary="Get a single account",
   *   tags={"accounts"},
   *   description="Get an account",
   *   operationId="getSingleAccountAction",
   *   @OA\Parameter(
   *     name="accountId",
   *     in="path",
   *     description="The account ID to retrieve information for.",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Parameter(
   *     name="includeLinked",
   *     in="query",
   *     description="Boolean flag to include linked (child) account data",
   *     @OA\Schema(type="boolean")
   *   ),
   *   @OA\Parameter(
   *     name="includeCollectorInfo",
   *     in="query",
   *     description="Boolean flag to include collector info",
   *     @OA\Schema(type="boolean")
   *   ),
   *   @OA\Parameter(
   *     name="includePunchoutInfo",
   *     in="query",
   *     description="Boolean flag to include punchout info",
   *     @OA\Schema(type="boolean")
   *   ),
   *   @OA\Parameter(
   *     name="customerGroup",
   *     in="query",
   *     description="The customer group",
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="account id required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response="204",
   *     description="Account Not Found",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param $accountId
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function show($accountId) {
    $account = Account::find($accountId);

    return $this->respond($account->toArray());
  }

  /**
   * Create a new Account.
   *
   * @OA\Post(
   *   path="/accounts",
   *   summary="Add A New Account via JSON Object",
   *   tags={"accounts"},
   *   description="Add A New Account via JSON Object",
   *   operationId="postAccountAction",
   *   @OA\RequestBody(
   *     required=true,
   *     description="Request body information",
   *     @OA\JsonContent(ref="#/components/schemas/Account")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function post(Request $request) {
    $data = json_decode($request->getContent());
    $account = Account::import($data);
    $response = $account->save();

    return $this->respond($response);
  }

  /**
   * Update an identified account.
   *
   * @OA\Patch(
   *   path="/accounts/{accountId}",
   *   summary="Update an existing account",
   *   tags={"accounts"},
   *   description="Update an existing account.  Updates any field provided",
   *   operationId="patchAccountAction",
   *   @OA\RequestBody(
   *     description="Request body information",
   *     required=true,
   *     @OA\JsonContent(ref="#/components/schemas/Account")
   *   ),
   *   @OA\Parameter(
   *     name="accountId",
   *     in="path",
   *     description="id of the account to update",
   *     required=true,
   *     @OA\Schema(type="string")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/components/schemas/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="invalid data",
   *     @OA\Schema(ref="#/components/schemas/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param $accountId
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function patch(Request $request, $accountId) {
    $params = $request->request->all();
    $userPreferences = Account::updateFavorites($accountId, $params);
    $account = Account::find($accountId);
    return $this->respond($account);
  }

}
