<?php


namespace Drupal\ur_frontend_api_v2\Controller;

use Drupal;
use Drupal\ur_api_dataservice\Exceptions\DalException;
use Drupal\ur_frontend_api_v2\Models\Order;
use Drupal\ur_utils\Helpers\Fluent;
use Drupal\ur_utils\Helpers\Collection;

/**
 * Class OrderController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class OrderController extends BaseController {
  /**
   * @OA\Tag(
   *   name="order",
   *   description="Order",
   *   @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get all credit cards.
   *
   * @OA\Get(
   *   path="/orders/{accountId}",
   *   summary="Get a list of all orders",
   *   tags={"orders"},
   *   description="Get all orders for authenticated user",
   *   operationId="getOrderssAction",
   *   @OA\Parameter(
   *     name="accountId",
   *     description="The id for the user account",
   *     in="path",
   *     required=true,
   *     @OA\Schema(type="number")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="accountId required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response="204",
   *     description="Account Not Found",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param $accountId
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws DalException 'Account Id is required'
   */
  public function get($accountId)
  {
    if (!$accountId) throw new DalException('Account ID is required');

    $types = $this->getRequest()->query->all()['type'];

    // 1. collect equipment items and add properties needed for orders
    $items = Collection::from(
      Drupal::service('ur_api_dataservice')
        ->getPlugin('Order')
        ->getOrders($accountId, $types)
    )
    ->mapInto(Fluent::class)
    ->map(function($item) use ($accountId)
    {
      return $item->with([
        'accountId' => $accountId,
        Order::$key => $item->transId,
        'key' => "{$item->catClass}-{$item->equipmentId}"
      ]);
    });

    // 2. create orders collection
    Order::$collection = Collection::from([]);
    $items->each(function ($item)
    {
      Order::$collection->when(
        Order::$collection->contains(Order::$key, $item->{Order::$key}),

        // order exists
        function($collection) use ($item) {
          return $collection
            ->firstWhere(Order::$key, $item->{Order::$key})
            ->addEquipment($item);
        },

        // order does not exist
        function ($collection) use ($item) {
          return $collection->push(
              Order::create($item)->addEquipment($item)
          );
        }
      );
    });

    // return orders api response
    return $this->respond(Order::$collection->toArray());
  }
}
