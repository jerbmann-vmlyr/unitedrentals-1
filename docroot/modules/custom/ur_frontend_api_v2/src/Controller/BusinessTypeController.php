<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\ur_frontend_api_v2\Models\BusinessType;


/**
 * Class BusinessTypeController.
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class BusinessTypeController extends BaseController {

  /**
   * @OA\Tag(
   *   name="businesstype",
   *   description="Business Type",
   *   @OA\ExternalDocumentation(
   *     description="Find out more",
   *     url="https://testmobiledal.ur.com/dashboard/docs"
   *   )
   * )
   */

  /**
   * Get all business types.
   *
   * @OA\Get(
   *   path="/branches/business-type",
   *   summary="Get a list of all business types",
   *   tags={"businesstype"},
   *   description="Get all business types from drupal taxonamy",
   *   operationId="getBusinessTypeAction",
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="Something went very wrong",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   * )
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function get():JsonResponse {
    // Use a cached version if available.
    $cid = 'apiBusinessTypes';
    $results = [];

    if ($cache = \Drupal::cache()->get($cid)) {
      if (!empty($cache->data)) {
        $results = $cache->data;
      }
      else {
        \Drupal::logger('Business Types')
          ->error('Business Types cache returned an empty object.');
      }
    }

    if (empty($results)) {
      $results = BusinessType::index();
    }

    // Cache the response for faster subsequent queries. (168 hours = 7 days)
    $expireTime = time() + (168 * 60 * 60);
    \Drupal::cache()->set($cid, $results, $expireTime);
    return $this->respond($results);
  }
}
