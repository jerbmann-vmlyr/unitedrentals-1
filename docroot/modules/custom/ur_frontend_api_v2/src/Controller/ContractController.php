<?php

namespace Drupal\ur_frontend_api_v2\Controller;

use Drupal\ur_frontend_api_v2\Models\Contract;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ContractController
 *
 * @package Drupal\ur_frontend_api_v2\Controller
 */
class ContractController extends BaseController {

  /**
   * Get all contracted rental transactions for an account.
   *
   * @OA\Get(
   *   path="/rentaltransactions/{accountId}/contracts",
   *   summary="Get a list of all contracted rental transactions",
   *   tags={"rentaltransactions"},
   *   description="Get all contracted rental transactions for a specified account type 1",
   *   operationId="getContractsAction",
   *   @OA\Parameter(
   *     name="accountId",
   *     description="The id for the user account",
   *     in="path",
   *     required=true,
   *     @OA\Schema(type="number")
   *   ),
   *   @OA\Response(
   *     response=200,
   *     description="Operation Successful",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   @OA\Response(
   *     response=400,
   *     description="accountId required",
   *     @OA\Schema(ref="#/definitions/ApiResponse")
   *   ),
   *   security={{"basicAuth":{}}}
   * )
   *
   * @param $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function get(Request $request) {
    $accountId = $request->get('accountId');

    if (!$accountId) {
      throw new DalException('Account ID is required');
    }

    $data = Contract::index($accountId, 1);

    return $this->respond($data);
  }

}
