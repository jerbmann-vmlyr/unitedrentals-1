<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class Branch.
 *
 * @OA\Schema(
 *    type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class Branch extends BaseModel {

  /**
   * The street address of the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $address1;

  /**
   * The location address of the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $address2;

  /**
   * The id of the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $branchId;

  /**
   * The business type code for the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $businessType;

  /**
   * The business type codes for the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $businessTypes;

  /**
   * The city location of the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $city;

  /**
   * The country code for the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $countryCode;

  /**
   * The branch observes daylight savings.
   *
   * @var bool
   * @OA\Property()
   */
  public $daylightSavings;

  /**
   * The branch district.
   *
   * @var string
   * @OA\Property()
   */
  public $district;

  /**
   * The branch fax number.
   *
   * @var string
   * @OA\Property()
   */
  public $fax;

  /**
   * The branch holiday hours.
   *
   * @var array
   *
   * @OA\Property(
   *   description="Array of holiday hours.",
   *   type="object"
   * )
   */
  public $holidayHours = [];

  /**
   * The branch record id : maps to branchId.
   *
   * @var string
   * @OA\Property()
   */
  public $id;

  /**
   * The branch default language code.
   *
   * @var string
   * @OA\Property()
   */
  public $defaultLangcode;

  /**
   * The branch latitude geoposition.
   *
   * @var float
   * @OA\Property()
   */
  public $latitude;

  /**
   * The branch longitude geoposition.
   *
   * @var float
   * @OA\Property()
   */
  public $longitude;

  /**
   * The branch manager's email address.
   *
   * @var string
   * @OA\Property()
   */
  public $managerEmail;

  /**
   * The name of the branch manager.
   *
   * @var string
   * @OA\Property()
   */
  public $managerName;

  /**
   * The branch name.
   *
   * @var string
   * @OA\Property()
   */
  public $name;

  /**
   * A flag for online orders available.
   *
   * @var string
   * @OA\Property()
   */
  public $onlineOrders;

  /**
   * The branch phone number.
   *
   * @var string
   * @OA\Property()
   */
  public $phone;

  /**
   * The primary business type code for the branch. Replaces previous businessType property.
   *
   * @var \Drupal\ur_frontend_api_v2\Models\BusinessType
   * @OA\Property()
   */
  public $primaryBusinessType;

  /**
   * The branch region code.
   *
   * @var string
   * @OA\Property()
   */
  public $region;

  /**
   * The branch saturday hours of operation.
   *
   * @var array
   *
   * @OA\Property(
   *   description="Saturday hours of operation.",
   *   type="object"
   * )
   */
  public $saturdayHours = [];

  /**
   * The state location of the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $state;

  /**
   * The branch status code.
   *
   * @var string
   * @OA\Property()
   */
  public $status;

  /**
   * The branch sunday hours of operation.
   *
   * @var array
   *
   * @OA\Property(
   *   description="Sunday hours of operation.",
   *   type="object"
   * )
   */
  public $sundayHours;

  /**
   * The hours offset from the system time.
   *
   * @var int
   * @OA\Property()
   */
  public $timeOffset;

  /**
   * The branch promotional video.
   *
   * @var string
   * @OA\Property()
   */
  public $videoUrl;

  /**
   * The marketing tag line for the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $webContentBlock;

  /**
   * The marketing description for the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $webDescription;

  /**
   * Flag for determining whether the branch should be displayed on the web.
   *
   * @var bool
   * @OA\Property()
   */
  public $webDisplay;

  /**
   * The types of products offered by the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $webProductType;

  /**
   * The branch weekday hours of operation.
   *
   * @var array
   * @OA\Property(
   *   description="The branch weekday hours of operation.",
   *   type="object"
   * )
   */
  public $weekdayHours;

  /**
   * The zip code location of the branch.
   *
   * @var string
   * @OA\Property()
   */
  public $zip;

  /**
   * The global branch content blocks.
   *
   * @var array
   *
   * @OA\Property(
   *   description="The global branch content blocks.",
   *   type="object"
   * )
   */
  public $globalContentBlocks = [];

  /**
   * The branch type specific content blocks.
   *
   * @var array
   *
   * @OA\Property(
   *   description="The branch type specific content blocks.",
   *   type="object"
   * )
   */
  public $branchTypeContentBlocks = [];

  /**
   * The branch type specific content blocks.
   *
   * @var array
   *
   * @OA\Property(
   *   description="The branch type specific content blocks.",
   *   type="object"
   * )
   */
  public $branchTypeEquipmentCategoryLinks = [];

  /**
   * The branch specific content blocks.
   *
   * @var array
   *
   * @OA\Property(
   *   description="The branch specific content blocks.",
   *   type="object"
   * )
   */
  public $contentBlocks = [];

  /**
   * Unused variables - available at DAL but not used by Kelex .
   */
  // Public $acquiredCompanyCode    String    1    Acquired Company Code    R - RSC, N - NES
  // public $businessTypes ***    Array         Business Types
  // public $capacityPct    Integer    3    Capacity % at which fast delivery is considered limited    75
  // public $districtMgr    String    20    District Manager Name    MICHAEL ROBERTS
  // public $districtMgrEm    String    70    District Manager Email    MROBERTS@UR.COM
  // public $districtMgrPh    String    12    District Manager Phone#    555-555-5555
  // public $districtName    String    36    District Name    813 - MA-DELAWARE/BALTIMORE
  // public $formerRSC    Boolean         Former RSC Branch    True
  // public $hub    String    3    Hub    12H
  // public $hubDesc    String    30    Hub Description    BOSTON METRO (700/801/308)
  // public $onSite    String    1         N
  // public $reportHeading    String    30    Report Heading    PENSACOLA, FL (G53)
  // public $serviceContact1    String    70    Service Contact #1 Name    Tom Jones
  // public $serviceContact1Em    String    70    Service Contact #1 Email    tjones@ur.com
  // public $serviceContact1Ph    String    12    Service Contact #1 Phone#    555-555-5555
  // public $serviceContact2    String    70    Service Contact #2 Name    Bob Smith
  // public $serviceContact2Em    String    70    Service Contact #2 Email    bsmith@ur.com
  // public $serviceContact2Ph    String    12    Service Contact #2 Phone#    555-555-5555
  // public $serviceHub    String    3    Service Hub    05S.

  /**
   * Get an array of all Accounts as defined by the getter.
   *
   * This will pull the drupal data.
   *
   * @return array
   *   An array of Account Objects.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function index() {
    /** @var \Drupal\ur_api_dataservice\Plugins\BranchPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Branch');

    return $plugin->index();
  }

  /**
   * Get an array of all Accounts as defined by the getter.
   *
   * This will pull the data from the DAL.
   *
   * @param array $params
   *   Location parameters.
   *
   * @return array
   *   An array of Account Objects.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getBranchesByLocation(array $params) {
    /** @var \Drupal\ur_api_dataservice\Plugins\BranchPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Branch');

    return $plugin->getBranchesByLocation($params);
  }

  /**
   * Retrieve a single branch from the data source.
   *
   * @param string $id
   *   The Branch Id.
   *
   * @return mixed
   *   Returns single branch details
   */
  public static function find($id) {
    /** @var \Drupal\ur_api_dataservice\Plugins\Branch $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Branch');

    $jobsite = $plugin->read($id);

    return $jobsite;
  }

  /**
   * Retrieve default branches from the data source.
   *
   * @return mixed
   *   Returns all default branches details
   */
  public static function getBranchesDefault() {
    /** @var \Drupal\ur_api_dataservice\Plugins\BranchPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Branch');

    $data = $plugin->retrieve();

    return $data;
  }

}
