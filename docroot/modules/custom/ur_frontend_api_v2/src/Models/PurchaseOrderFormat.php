<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class PurchaseOrderFormat - A PO Format object.
 *
 * @OA\Schema(required={"pattern"}, type="object")
 *
 * @package Drupal\ur_frontend_api_v2\Model
 */
class PurchaseOrderFormat extends BaseModel {

  /**
   * The regular expression string for the PO Format pattern.
   *
   * @var string
   * @OA\Property()
   * @required
   */
  public $pattern;

  /**
   * The original PO pattern string from the DAL.
   *
   * @var string
   * @OA\Property()
   */
  public $poFormat;

  /**
   * The date at which the PO Format is effective from.
   *
   * @var \DateTime
   * @OA\Property()
   */
  public $effectiveDate;

  /**
   * The expiration date of the PO format.
   *
   * @var \DateTime
   * @OA\Property()
   */
  public $expirationDate;

  /**
   * Retrieves PO Formats for a specific account.
   *
   * @param string $accountId
   *   The Account ID.
   *
   * @return array|bool
   *   A collection of PO Format objects.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function index($accountId) {
    /** @var \Drupal\ur_api_dataservice\Plugins\PurchaseOrderFormatPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('PurchaseOrderFormat');

    $poFormats = $plugin->index($accountId);

    return $poFormats;
  }

}
