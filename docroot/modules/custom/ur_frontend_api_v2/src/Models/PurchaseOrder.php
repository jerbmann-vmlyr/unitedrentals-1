<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class PurchaseOrder - A PurchaseOrder object.
 *
 * @OA\Schema(required={"poNumber"}, type="object")
 *
 * @package Drupal\ur_frontend_api_v2\Model
 */
class PurchaseOrder extends BaseModel {

  // Example Response object from DAL.
  // @TODO: Get understanding of what each of these properties mean to determine which ones we need
  /**
   * {
   * "accountId": "842859",
   * "poNumber": "ABC-123",
   * "statusCode": "O",
   * "statusCodeDesc": "Open",
   * "attention": "",
   * "orderedBy": "JOHN DOE",
   * "phoneNum": "816-314-1234",
   * "type": "O",
   * "poDate": "2018-08-01T00:00:00",
   * "deliveryDate": "0",
   * "poAmount": 80000,
   * "currency": "USD",
   * "createDate": "2018-08-01T00:00:00",
   * "updateDate": "2018-08-01T00:00:00",
   * "comments": "ASDFGHJK",
   * "poType": "REG",
   * "expirationDate": "2018-10-31T00:00:00",
   * "invoicedAmt": "0",
   * "balanceAmt": "80000.00",
   * "lastMaintainedDateTime": "2018-08-01T14:57:50",
   * "lastMaintainedBy": "TC-WANDERS"
   * }.
   */

  /**
   * The account ID this PO Number is for.
   *
   * @var string
   * @OA\Property()
   */
  public $accountId;

  /**
   * The PO Number.
   *
   * @var string
   * @OA\Property()
   * @required
   */
  public $poNumber;

  /**
   * The Status Code for this PO Number.
   *
   * @var string
   * @OA\Property()
   */
  public $statusCode;

  /**
   * The friendly label for the Status Code.
   *
   * @var string
   * @OA\Property()
   */
  public $statusCodeDesc;

  /**
   * The amount the PO Number was originally set up for.
   *
   * @var string
   * @OA\Property()
   */
  public $poAmount;

  /**
   * The 3 letter abbreviation for which country the PO Amount is for.
   *
   * @var string
   * @OA\Property()
   */
  public $currencyCode;

  /**
   * The remaining balance that the PO Number has left.
   *
   * @var float
   * @OA\Property()
   */
  public $balanceAmt;

  /**
   * Retrieves Purchase Order Numbers for a specific account.
   *
   * @param string $accountId
   *   The Account ID.
   *
   * @return array|bool
   *   A collection of PO Format objects.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function index($accountId) {
    /** @var \Drupal\ur_api_dataservice\Plugins\PurchaseOrderPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('PurchaseOrder');

    $poFormats = $plugin->index($accountId);

    return $poFormats;
  }

}
