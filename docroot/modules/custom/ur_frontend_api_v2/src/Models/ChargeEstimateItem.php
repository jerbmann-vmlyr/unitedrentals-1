<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class ChargeEstimateItem.
 *
 * @package Drupal\ur_frontend_api_v2\Models
 *
 * @OA\Schema(
 *   title="Charge Estimate Item",
 *   required={
 *     "catClass",
 *     "quantity",
 *   },
 *   type="object",
 * @OA\Discriminator(propertyName="catClass")
 * )
 */
class ChargeEstimateItem extends BaseModel {

  /**
   * The cat-class identification number we're getting the estimate for.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The cat-class code we're estimating for",
   *   title="Cat-Class code",
   *   pattern="^[0-9]{3}-[0-9]{4}$",
   *   readOnly=true,
   *   example="310-9100"
   * )
   */
  public $catClass;

  /**
   * The amount of the cat-class in the charge estimate.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The amount of the cat-class in the charge estimate.",
   *   title="Quantity",
   *   example=2
   * )
   */
  public $quantity;

  /**
   * The minimum rate of the charge estimate.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The minimum rate of the charge estimate.",
   *   title="Minimum Rate",
   *   example=124
   * )
   */
  public $minRate;

  /**
   * The day rate of the charge estimate.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The day rate of the charge estimate.",
   *   title="Day Rate",
   *   example=124
   * )
   */
  public $dayRate;

  /**
   * The week rate of the charge estimate.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The week rate of the charge estimate.",
   *   title="Week Rate",
   *   example=248
   * )
   */
  public $weekRate;

  /**
   * The month rate of the charge estimate.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The month rate of the charge estimate.",
   *   title="Month Rate",
   *   example=505
   * )
   */
  public $monthRate;

  /**
   * The shift differential. Multiple shifts affect price.
   *
   * @var string
   *
   * @OA\Property(
   *   title="Quantity",
   *   enum={"S","D","T"},
   *   writeOnly=true,
   *   description="Sort order:
   *       'S' - Single time
   *       'D' - Double time
   *       'T' - Triple time"
   *   )
   * )
   */
  public $shiftRate;

  /**
   * The total for the item(s).
   *
   * @var float
   *
   * @OA\Property(
   *   description="The total for the item(s). Price * Quantity",
   *   readOnly=true,
   *   example=496
   * )
   */
  public $total;

  /**
   * Calculated total for single item. Total / quantity.
   *
   * @var float
   *
   * @OA\Property(
   *   description="Calculated total for single item. Total / quantity.",
   *   readOnly=true,
   *   example=248
   * )
   */
  public $individualTotal;

  /**
   * Is this item available at the currently set branch?
   *
   * @var bool
   *
   * @OA\Property(
   *   description="Item is or is not available at the current branch.",
   *   readOnly=true,
   *   example=true
   * )
   */
  public $availableEquip;

  /**
   * The rate type for this item
   *
   * @var string
   *
   * @OA\Property(
   *   description="The item's rate type.",
   *   readOnly=true,
   *   example=Core
   * )
   */
  public $rateType;

  /**
   * The rate type code for this item
   *
   * @var string
   *
   * @OA\Property(
   *   description="The item's rate type code.",
   *   readOnly=true,
   *   example=C
   * )
   */
  public $rateTypeCode;

  /**
   * If the item has contract pricing
   *
   * @var string
   *
   * @OA\Property(
   *   description="If the item has contact pricing",
   *   readOnly=true,
   *   example=false
   * )
   */
  public $hasContractPricing;

  /**
   * Override parent BaseModel toArray method.
   *
   * We don't want the catClass, shiftRate and dataService properties returned
   * in our response.
   *
   * @return array
   *   The ChargeEstimateItem object as an array.
   */
  public function toArray() {
    $array = parent::toArray();

    unset($array['catClass']);
    unset($array['shiftRate']);
    unset($array['dataService']);

    return $array;
  }

  /**
   * Set total values of charge-estimates to ZERO ($0) for UNAVAILABLE equipments.
   */
  public function setTotalsToZero() {
    $this->availableEquip = FALSE;
    $this->dayRate = 0;
    $this->minRate = 0;
    $this->weekRate = 0;
    $this->monthRate = 0;
    $this->individualTotal = 0;
    $this->total = 0;
    $this->rateType = '';

    return $this;
  }

  public function hasContractPricing() {
    // These rate types determine if contract pricing is enabled currently.
    $contractPricingRateTypes = ['fixed', 'not to exceed'];

    return in_array(strtolower($this->rateType), $contractPricingRateTypes);
  }

}
