<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class Contract - A Rental Transaction Object: On Rent.
 *
 * @OA\Schema(
 *   title="Contract",
 *   required={
 *    "accountId"
 *   },
 *   type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class Contract extends RentalTransaction {

  /**
   * Get an array of all RentalTransaction as defined by the getter.
   *
   * @param string $accountId
   * @param null $type
   *
   * @return array
   *   An array of RentalTransaction Objects.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function index($accountId, $type = NULL) {
    /** @var \Drupal\ur_api_dataservice\Plugins\RentalTransactionPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Contract');

    return $plugin->index($accountId);
  }

}
