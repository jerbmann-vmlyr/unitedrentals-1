<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class Geodata - A geodata object.
 *
 * @OA\Schema(
 *   title="Geodata",
 *   required={
 *     "type"
 *   },
 *   type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class Geodata extends BaseModel {

  /**
   * The actual geo-data records
   *
   * @var object
   * @OA\Property()
   */
  public $geodata;

  /**
   * Get an array of all telematic data as defined by the getter.
   *
   * @param string $type
   *
   * @param array|null $vars
   *
   * @return array|mixed
   *   An array of geodata.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   */
  public static function index($type, $vars = NULL) {
    /** @var \Drupal\ur_api_dataservice\Plugins\GeodataPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Geodata');

    $geodata = $plugin->index($type, $vars);

    return $geodata;
  }

}
