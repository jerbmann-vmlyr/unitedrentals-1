<?php


namespace Drupal\ur_frontend_api_v2\Models;

use Drupal;
use DateTime;
use Drupal\ur_utils\Helpers\Fluent;
use Drupal\ur_utils\Helpers\Collection;

class Order extends BaseModel
{
  public static $collection;
  public static $key = 'orderNumber';

  /**
   * Order constructor.
   * @param $item
   * @throws \Exception
   */
  public function __construct($item) {
    return $this->populateWith($item);
  }

  /**
   * @param $item
   * @return Order
   * @throws \Exception
   */
  public static function create($item) {
    return new Order($item);
  }

  /**
   * @param Fluent $item
   * @return $this
   * @throws \Exception
   */
  public function populateWith(Fluent $item) {
    $map = [
      'id' => 'accountId',
      'accountId' => 'accountId',
      'orderNumber' => 'transId',
      'billThru' => 'lastBilledDate',
      'approver' => 'approverName',
      'orderDeliveryMethod' => 'urDeliver',
      'orderPickupMethod' => 'pickup',
      'orderEstimatedEndDate' => 'returnDateTime',
      'orderStartDate' => 'startDateTime',
      'orderStatus' => 'status',
      'orderType' => 'transType',
      'purchaseOrder' => 'po',
      'requester' => 'requesterName',
      'requisitionNumber' => 'requisitionId',
      'approverGuid' => 'approverGuid',
      'orderedBy' => 'orderedBy',
      'branchId' => 'branchId',
      // orderRentalDuration
      // totalItems
      // totalDailyEstimate @todo totalEstimate
      // equipment
      // jobsite
    ];

    foreach ($map as $orderProperty => $itemProperty)
      $this->$orderProperty = $item->$itemProperty;

    $this->orderRentalDuration =
      (new DateTime($this->orderEstimatedEndDate))
        ->diff((new DateTime($this->orderStartDate)))
        ->format('%a');

    return $this;
  }

  /**
   * @param Fluent $item
   * @return Order
   * @throws \Exception
   */
  public function addEquipment(Fluent $item) {

    $equipment = &$this->equipment[$item->key];
    $equipment['orderId'] = $this->orderNumber;
    $equipment = $item->toArray();

    /* Hook to aggregate equipment data on the order level */
    $this->addedEquipmentHook($item);

    return $this->populateWith($item);
  }

  /**
   * Life cycle hook after new piece of equipment is added to order
   * to aggregate equipment data into order level properties
   * @param $item
   */
  public function addedEquipmentHook($item) {
    $this->totalItems++;
    $this->jobsite = $item->jobsite;
    $this->totalDailyEstimate = $this->totalDailyEstimate + $item->totalDailyEstimate;
  }

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="number"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $id;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="number"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $totalItems = 0;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $approver;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $billThru;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $orderDeliveryMethod;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $orderEstimatedEndDate;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $orderNumber;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $orderPickupMethod;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $orderRentalDuration;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $orderStartDate;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $orderStatus;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $purchaseOrder;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $requester;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $requisitionNumber;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="number"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $totalDailyEstimate;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $accountCodes;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $accountId;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $active;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $approverGuid;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $currency;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $branchId;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $customerJobId;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $customerName;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $equipment;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $leniencyEndDate;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $leniencyStartDate;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $orderedBy;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $pickup;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $requesterGuid;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $rentalDuration;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $orderType;

  /**
   * Class Order.
   *
   * @OA\Schema(
   *    type="string"
   * )
   *
   * @package Drupal\ur_frontend_api_v2\Models
   */
  public $jobsite;
}
