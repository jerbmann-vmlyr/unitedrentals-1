<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class CommunicationRoute - An Communication reroute object.
 *
 * @OA\Schema(type="object")
 *
 * @package Drupal\ur_frontend_api_v2\Model
 */
class CommunicationRoute extends BaseModel {

  /**
   * The route to which the user should be redirected.
   *
   * @var string
   * @OA\Property()
   */
  public $newRoute;

  /**
   * @param $messageType
   * @param $accountId
   * @param $requisitionId
   * @param $catClass
   * @param $transLineId
   *
   * @return \Drupal\ur_frontend_api_v2\Models\CommunicationRoute
   */
  public static function find($messageType, $accountId, $requisitionId, $catClass, $transLineId) {
    $result = new static();

    switch (strtolower($messageType)) {
      case 'reservation':
        try {
          $requisitionHistory = RequisitionHistory::find($requisitionId);
        }
        catch (\Exception $e) {
          $result->newRoute = "manage#/manage/$accountId/equipment";
          return $result;
        }

        if (!$requisitionHistory || !$requisitionHistory->requestId) {
          $result->newRoute = "manage#/manage/$accountId/equipment";
          return $result;
        }

        $transId = array_shift($requisitionHistory->orderDetails)->transactionId;

        if ($transId === '') {
          $result->newRoute = "manage#/manage/$accountId/equipment";
          return $result;
        }

        try {
          $transactionLines = RentalTransaction::find($transId);
        }
        catch (\Exception $e) {
          $result->newRoute = "manage#/manage/$accountId/equipment";
          return $result;
        }

        if (!$transactionLines) {
          $result->newRoute = "manage#/manage/$accountId/equipment";
          return $result;
        }

        $transaction = array_shift(array_filter($transactionLines, function ($a) use ($catClass, $transLineId) {
          if ($a->catClass == $catClass && $a->transLineId == $transLineId) {
            return $a;
          }

          return FALSE;
        }));

        /*
         * If we can't find the line item with specified cat-class & transLineId
         * then return to equipment.
         */
        if ($transaction == NULL) {
          $result->newRoute = "/manage#/manage/$accountId/equipment";
          return $result;
        }

        $result->newRoute = "/manage#/manage/$accountId/equipment/$transaction->equipmentId/$transaction->transId/$transaction->transLineId";
        return $result;

      default:
        break;
    }
  }

}
