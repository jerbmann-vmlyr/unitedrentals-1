<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class Totals.
 *
 * @package Drupal\ur_frontend_api_v2\Models
 *
 * @OA\Schema(
 *   title="Totals",
 *   required={
 *     "totalRentalAmount",
 *     "environmentFee",
 *     "tax",
 *     "damageWaiverFee",
 *     "deliveryFee",
 *     "pickupFee",
 *     "miscellaneousFees",
 *     "authAmount"
 *   },
 *   readOnly=true,
 *   type="object"
 * )
 */
class ChargeEstimateTotals extends BaseModel {

  /**
   * Example totals object response from DAL.
   *
   * "totals": {
   *   "totrentamt": 3953.2, -- $totalRentalAmount
   *   "totenvamt": 0,  -- $environmentFee
   *   "tottaxamt": 0,  -- combined into $tax
   *   "totgstamt": 0, -- combined into $tax
   *   "totdwamt": 0, -- $damageWaiverFee
   *   "totdelamt": 0, -- $deliveryFee
   *   "totpuamt": 355.17, -- $pickupFee
   *   "miscCharges": 0, -- $miscellaneousFees
   *   "authAmount": 5385.46, -- $authAmount
   *   "authPercent": 125, -- @TODO: Determine if we need?
   *   "rateZone": "275", -- @TODO: Determine if we need?
   *   "tspAgrmt": "N", -- @TODO: Determine if we need?
   *   "tspAgrmtRate": 0 -- @TODO: Determine if we need?
   *   "totalsOverwritten": TRUE -- flag to determine if totals have been overwritten
   * }
   */

  /**
   * The total rental amount not including taxes or fees.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The total rental amount not including taxes or fees.",
   *   example=955
   * )
   */
  public $totalRentalAmount;

  /**
   * The  environment fee amount.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The environmental fee amount.",
   *   example=9.12
   * )
   */
  public $environmentFee;

  /**
   * The total tax amount.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The total tax amount.",
   *   example=62.67
   * )
   */
  public $tax;

  /**
   * The damage waiver fee amount.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The damage waiver fee amount.",
   *   example=12.51
   * )
   */
  public $damageWaiverFee;

  /**
   * The total delivery fee amount.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The total delivery fee amount.",
   *   example=125
   * )
   */
  public $deliveryFee;

  /**
   * The total pickup fee amount.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The total pickup fee amount.",
   *   example=125
   * )
   */
  public $pickupFee;

  /**
   * The miscellaneous charges fee amount.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The miscellaneous charges fee amount.",
   *   example=0
   * )
   */
  public $miscellaneousFees;

  /**
   * The authorized amount for credit.
   *
   * @var float
   *
   * @OA\Property(
   *   description="The authorized amount for credit.",
   *   example=1595.99
   * )
   */
  public $authAmount;

  /**
   * Override parent BaseModel import method.
   *
   * @param object $rawData
   *   The rawData returned for the totals from the API.
   *
   * @return \Drupal\ur_frontend_api_v2\Models\BaseModel|\Drupal\ur_frontend_api_v2\Models\ChargeEstimateTotals
   */
  public static function import($rawData) {
    $total = new static();
    $total->totalRentalAmount = $rawData->totrentamt;
    $total->environmentFee = $rawData->totenvamt;
    $total->damageWaiverFee = $rawData->totdwamt;
    $total->deliveryFee = $rawData->totdelamt;
    $total->pickupFee = $rawData->totpuamt;
    $total->miscellaneousFees = $rawData->miscCharges;
    $total->authAmount = $rawData->authAmount;
    $total->totalsOverwritten = FALSE;

    /*
     * API sends us both US and Canada tax values, but one will always be 0.
     * If "tottaxamt" (US tax) is empty, then we assume the "totgstamt"
     * (Canada Tax) will have a value.
     */
    $total->tax = !empty($rawData->tottaxamt) ? $rawData->tottaxamt : $rawData->totgstamt;

    return $total;
  }

  /**
   * Override BaseModel toArray method.
   *
   * @return array
   *   The ChargeEstimateTotals object in array form.
   */
  public function toArray() {
    $array = parent::toArray();
    unset($array['dataService']);

    return $array;
  }
  
}
