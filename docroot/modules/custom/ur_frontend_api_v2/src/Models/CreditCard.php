<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class CreditCard - A credit card object.
 *
 * @OA\Schema(
 *   required={
 *    "cardHolder",
 *    "cardName",
 *    "cardNumber",
 *    "expireDate"
 *   },
 *   type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Model
 */
class CreditCard extends BaseModel {

  /**
   * The identifier of the credit card - as provided by the data source.
   *
   * @var string
   * @OA\Property()
   */
  public $id;

  /**
   * The provided identifier of the credit card - will be translated to id field for data source.
   *
   * @var string
   * @OA\Property()
   */
  public $cardId;

  /**
   * The cardholder's name.
   *
   * @var string
   * @OA\Property()
   * @required
   */
  public $cardHolder;

  /**
   * The User Provided alternate name for the card.
   *
   * @var string
   * @OA\Property()
   */
  public $cardName;

  /**
   * The card number.
   *
   * @var string
   * @OA\Property()
   */
  public $ccNumber;

  /**
   * The type of the card.
   *
   * @var string
   * @OA\Property()
   */
  public $cardType;

  /**
   * The status of the card.
   *
   * @var string
   * @OA\Property()
   */
  public $status;

  /**
   * The last 4 digits of the card number.
   *
   * @var string
   * @OA\Property()
   */
  public $ccNum4;

  /**
   * The Card's Expiration Date.
   *
   * @var string
   * @OA\Property()
   */
  public $expireDate;

  /**
   * Indicator of whether the card is the default payment method.
   *
   * @var bool
   * @OA\Property()
   */
  public $defaultCard;

  /**
   * Retrieve a single card from the data source.
   *
   * @param string $id
   *   The Credit Card Id.
   *
   * @return array|\Drupal\ur_frontend_api_v2\Models\CreditCard
   *   A single Credit Card Object as identified by $id.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function find($id) {
    /** @var \Drupal\ur_api_dataservice\Plugins\CreditCardPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('CreditCard');

    $card = $plugin->read($id);

    return $card;
  }

  /**
   * Delete the credit card object identified by the instance's id.
   *
   * @return mixed
   *   The results of the execute method of the deleter worker.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function delete() {
    /** @var \Drupal\ur_api_dataservice\Plugins\CreditCardPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('CreditCard');

    return $plugin->delete($this);
  }

  /**
   * Save the current instance to the data source.
   *
   * @return mixed
   *   The results of the execute method of the saver worker.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\DataException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function save() {
    /** @var \Drupal\ur_api_dataservice\Plugins\CreditCardPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('CreditCard');

    if ($this->id === NULL) {
      return $plugin->create($this);
    }

    return $plugin->update($this);
  }

  /**
   * Get an array of all CreditCards as defined by the getter.
   *
   * @return array
   *   An array of CreditCard Objects.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function index() {
    /** @var \Drupal\ur_api_dataservice\Plugins\CreditCardPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('CreditCard');

    return $plugin->index();
  }

}
