<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class RentalRequisition - A Rental Requisition Object.
 *
 * @OA\Schema(
 *   title="Rental Requisition",
 *   required={
 *    "requestId"
 *   },
 *   type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class RentalRequisition extends BaseModel {

  public $id; // The id of the associated account
  public $requestId; // The id of the requisition
//  public $returnSeq; // The specific return sequence if needed
  public $status; // Status Code - Single code or comma-separated list
  public $createdguid; // Created by GUID
  public $approverguid; // Approved by GUID
  public $reqGuid; // Requested by GUID
//  public $apprinclude; // Status Code and Approver GUID to include
//  public $apprexclude; // Status Code and Approver GUID to exclude
  public $catClass; // Rental item cat class
  public $jobId; // The job site id
  public $alias; // Alias for bulk items
  public $lineSeq; // Sequence Number
  public $includecustcodes; // Customer codes to include in request --> CC,CD,CF,CI,CP,CR
  public $excludecustcodes; // Customer codes to exlcude in request --> CC,CD,CF,CI,CP,CR
  public $branchId; // The rental branch
  public $accountId; // The id of the associated account --> in result object
  public $statusCode; // The status of requisition to retrieve (QR,OR,OA,OC,OD,QT,QC,QD,RR,RS,RC,RD)
  public $statusCodeDesc; // Status Code Description
  public $poNumber; // Purchase Order Number
  public $reqContact; // Request Contact
  public $reqPhone; // Request Contact Phone #
  public $reqEmail; // Request Contact Email
  public $jobContact; // Job Contact
  public $jobPhone; // Job Contact Phone #
  public $jobEmail; // Job Contact Email
  public $rpp; // Rental Protection Plan (Y/N)
  public $reqDateTime; // Request Date/Time --> timestamp
  public $jobsiteName; // The UR job site name
  public $jobsiteAddress1; // The job site address 1
  public $jobsiteCity; // The job site city
  public $jobsiteState; // The job site state
  public $customerJobId;  // Customer jobsite id
  public $startDateTime; // The rental start date/time
  public $endDateTime; // End Rental Date/Time
  public $delivery; // Delivery (Y/N)
  public $pickup; // Pickup (Y/N)
  public $approverGuid; // Approver GUID
  public $approverName; // Approver Full Name
  public $createdGuid; // Id of the user who created the record
  public $createdName; // Name of the user who created the record
  public $timeStamp; // Record TimeStamp --> needs to be pulled before updating another Requisition
  public $equipmentId; // The id of the equipment
  public $make; // The make of the equipment item
  public $model; // The model of the equipment item
  public $serial; // The equipment item serial number
  public $catClassDesc; // CatClass description
  public $qty; // The total quantity for this record
//  public $reservationQuoteId; // Reservation/Quote Number
  public $quoteId; // Quote Number
//  public $custOwn; // Equpment is customer owned (boolean)
  public $coeStatusCode; // Status code for customer owned requisition
//  public $bulk; // Bulk item flag (boolean)
  public $remainingQty; // Number of items not returned and not scheduled for pickup
  public $scheduledPickupQty; // Number of items scheduled for pickup
//  public $outsourced; // Outsourced (Y/N)
  public $vendorName; // Vendor Name
  public $catClassList; // Cat/Class
  public $qtyList; // Quantity
  public $requestSeq; // Request Sequence
//  public $transChg; // Transportation Charge
//  public $miscChg; // Miscellaneous Charge
  public $updateType; // Type Customer Owned update --> 1 = Put In Use || 3 = Returned
  public $details; // Equipment Data
  public $quantity; // The total quantity for this record
  public $equipNum; // Equipment Number
  public $custOwn; // Customer Owned
  public $jobNum; // Jobsite Number
//  public $meter;
//  public $dayRate;
//  public $totalAmount;

  /**
   * Retrieve a single requisition from the data source.
   *
   * @param string $requestId
   *   The Rental Transaction Id.
   *
   * @return \Drupal\ur_frontend_api_v2\Models\RentalRequisition
   *   A single Rental Requisition Object as identified by $requestId.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function find($requestId) {
    /** @var \Drupal\ur_api_dataservice\Plugins\RentalRequisitionPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RentalRequisition');

    return $plugin->read($requestId);
  }

  /**
   * Save the current instance to the data source. -- ONLY UPDATE REQUISITION
   * DAL will create the requisition record on transaction submission
   *
   * @return mixed
   *   The results of the execute method of the saver worker.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function save() {
    /** @var \Drupal\ur_api_dataservice\Plugins\RentalRequisitionPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('RentalRequisition');

    return $plugin->update($this);
  }

  /**
   * Get an array of all RentalRequisition as defined by the getter.
   *
   * @param string $id
   * @param int $status
   *
   * @return array
   *   An array of RentalRequisition Objects.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function index($id, $status) {
    /** @var \Drupal\ur_api_dataservice\Plugins\RentalRequisitionPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RentalRequisition');

    return $plugin->index($id, $status);
  }

}
