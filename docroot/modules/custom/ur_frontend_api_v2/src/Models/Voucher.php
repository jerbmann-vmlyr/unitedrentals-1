<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class Voucher - A voucher redeemable from checkout
 *
 * @OA\Schema( type="object")
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class Voucher extends BaseModel {

  /**
   * The voucher code
   *
   * @var array
   * @OA\Property()
   */
  public $code;

  /**
   * The voucher type
   *
   * @var array
   * @OA\Property()
   */
  public $type;

  /**
   * The voucher status
   *
   * @var array
   * @OA\Property()
   */
  public $status;

  /**
   * The voucher discount
   *
   * @var array
   * @OA\Property()
   */
  public $discount;

  /**
   * The voucher metadata (optional)
   *
   * @var array
   * @OA\Property()
   */
  public $metadata;

  /**
   * The voucher errorMessage, if redemption fails
   *
   * @var array
   * @OA\Property()
   */
  public $errorMessage;

  /**
   * @param $voucherCode
   * @return \Drupal\ur_frontend_api_v2\Models\Voucher
   */
  public static function retrieve($voucherCode): Voucher {
    $voucherPlugin = \Drupal::service('ur_api_dataservice')->getPlugin('Voucher');
    return $voucherPlugin->retrieveVoucher($voucherCode);
  }

  /**
   * @param \Drupal\ur_frontend_api_v2\Models\ChargeEstimate $chargeEstimate
   * @param \Drupal\ur_frontend_api_v2\Models\Voucher $voucher
   * @return \Drupal\ur_frontend_api_v2\Models\ChargeEstimate
   */
  public static function apply(ChargeEstimate $chargeEstimate, Voucher $voucher): ChargeEstimate {
    $voucherPlugin = \Drupal::service('ur_api_dataservice')->getPlugin('Voucher');
    return $voucherPlugin->applyVoucher($chargeEstimate, $voucher);
  }

}
