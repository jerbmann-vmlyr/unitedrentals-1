<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class Jobsite.
 *
 * @OA\Schema(required={"accountId"}, type="object")
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class Jobsite extends BaseModel {

  /**
   * Jobsite access restrictions?
   *
   * @var bool
   */
  public $accessRestrictions;

  /**
   * The account this jobsite is associated with.
   *
   * @var string
   */
  public $accountId;

  /**
   * The street address of the jobsite.
   *
   * @var string
   */
  public $address1;

  /**
   * The location address of the jobsite.
   *
   * @var string
   */
  public $address2;

  /**
   * Driver badging required?
   *
   * @var bool
   */
  public $badgingRequired;

  /**
   * The branch that opened the job.
   *
   * @var string
   */
  public $branch;

  /**
   * Branch access restrictions?
   *
   * @var bool
   */
  public $branchAccessRestrictions;

  /**
   * The city location of the jobsite.
   *
   * @var string
   */
  public $city;

  /**
   * The contact name for the jobsite.
   *
   * @var string
   */
  public $contact;

  /**
   * Customer jobsite id.
   *
   * @var string
   */
  public $customerJobId;

  /**
   * Default to delivery?
   *
   * @var bool
   */
  public $defaultDelivery;

  /**
   * The earliest arrival time for the jobsite.
   *
   * @var string
   */
  public $earliestArrivalTime;

  /**
   * The email address for the contact at this jobsite.
   *
   * @var string
   */
  public $email;

  /**
   * The fax number for the jobsite.
   *
   * @var string
   */
  public $fax;

  /**
   * The jobsite record id.
   *
   * @var string
   */
  public $id;

  /**
   * The job id.
   *
   * @var string
   */
  public $jobId;

  /**
   * The job phone.
   *
   * @var string
   */
  public $jobPhone;

  /**
   * The latest arrival time for the jobsite.
   *
   * @var string
   */
  public $latestArrivalTime;

  /**
   * The jobsite latitude geoposition.
   *
   * @var int
   */
  public $latitude;

  /**
   * This field is auto-generated from the address1 and city fields upon jobsite creation.
   *
   * @var string
   */
  public $location;

  /**
   * The jobsite longitude geoposition.
   *
   * @var int
   */
  public $longitude;

  /**
   * The mobile phone number for the contact at the jobsite.
   *
   * @var string
   */
  public $mobilePhone;

  /**
   * The name of the jobsite.
   *
   * @var string
   */
  public $name;

  /**
   * Notes associated with the jobsite.
   *
   * @var string
   */
  public $notes;

  /**
   * The PO number associated with the jobsite.
   *
   * @var string
   */
  public $poNumber;

  /**
   * Rollback required?
   *
   * @var bool
   */
  public $rollbackRequired;

  /**
   * The state location for the jobsite.
   *
   * @var string
   */
  public $state;

  /**
   * The status of the jobsite: A = active, D = deactivated.
   *
   * @var string
   */
  public $status;

  /**
   * The decoded status description from the Reason Codes file.
   *
   * @var string
   */
  public $statusDesc;

  /**
   * The jobsite tax district.
   *
   * @var string
   */
  public $taxDistrict;

  /**
   * The type of jobsite: U = United, C = Customer only.
   *
   * @var string
   */
  public $type;

  /**
   * The jobsite validity code.
   *
   * @var string
   */
  public $validityCode;

  /**
   * The zip code location of the jobsite.
   *
   * @var string
   */
  public $zip;

  /**
   * Retrieve a single jobsite from the data source.
   *
   * @param string $accountId
   *   The Account Id.
   *
   * @param string $id
   *   The Jobsite Id.
   *
   * @return array|\Drupal\ur_frontend_api_v2\Models\Jobsite
   *   A single Jobsite Object as identified by $id.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function find($accountId, $id) {
    /** @var \Drupal\ur_api_dataservice\Plugins\JobsitePlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Jobsite');

    $jobsite = $plugin->read($accountId, $id);

    return $jobsite;
  }

  /**
   * Save the current instance to the data source.
   *
   * @return mixed
   *   The results of the execute method of the saver worker.
   */
  public function save() {
    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('Jobsite');

    if ($this->id === NULL) {
      return $plugin->create($this);
    }

    return $plugin->update($this);
  }

  /**
   * Get an array of all Jobsites as defined by the getter.
   *
   * @param $accountId
   * @return array|bool|mixed
   *   An array of Account Objects.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function index($accountId) {
    /** @var \Drupal\ur_api_dataservice\Plugins\JobsitePlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Jobsite');

    return $plugin->index($accountId);
  }

}
