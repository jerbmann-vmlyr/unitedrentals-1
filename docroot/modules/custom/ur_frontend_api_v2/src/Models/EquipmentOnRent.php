<?php

namespace Drupal\ur_frontend_api_v2\Models;


/**
 * Class EquipmentOnRent - A piece of rented equipment.
 *
 * @OA\Schema( type="object")
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class EquipmentOnRent extends BaseModel {

  /**
   * The id of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $id;

  /**
   * The catClass of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $catClass;

  /**
   * The equipmentId of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $equipmentId;

  /**
   * The urEquipmentId of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $urEquipmentId;

  /**
   * The title of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $title;

  /**
   * The description of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $description;

  /**
   * The dalDescription of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $dalDescription;

  /**
   * The quantity of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantity;

  /**
   * The quantityOnRent of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantityOnRent;

  /**
   * The quantityReturned of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantityReturned;

  /**
   * The quantityPickedUp of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantityPickedUp;

  /**
   * The quantityPendingPickup of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantityPendingPickup;

  /**
   * The quantityNextPickup of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantityNextPickup;

  /**
   * The usageToday of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $usageToday;

  /**
   * The lastMeterChange of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $lastMeterChange;

  /**
   * The remainingQuantity of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $remainingQty;

  /**
   * The scheduledPickupQty of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $scheduledPickupQty;

  /**
   * The gps of the equipment on rent.
   *
   * @var array
   * @OA\Property()
   */
  public $gps;

  /**
   * The latitude of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $latitude;

  /**
   * The longitude of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $longitude;

  /**
   * The reportedDateTime of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $reportedDateTime;

  /**
   * The lastBilledDate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $lastBilledDate;

  /**
   * The orderedBy of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $orderedBy;

  /**
   * The make of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $make;

  /**
   * The model of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $model;

  /**
   * The year of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $year;

  /**
   * The serial of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $serial;

  /**
   * The eqpType of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $eqpType;

  /**
   * The transType of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $transType;

  /**
   * The status of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $status;

  /**
   * The po of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $po;

  /**
   * The rate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $rate;

  /**
   * The minRate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $minRate;

  /**
   * The dayRate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $dayRate;

  /**
   * The weekRate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $weekRate;

  /**
   * The monthRate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $monthRate;

  /**
   * The rentalAmount of the equipment on rent.
   *
   * @var float
   * @OA\Property()
   */
  public $rentalAmount;

  /**
   * The custOwn of the equipment on rent.
   *
   * @var boolean
   * @OA\Property()
   */
  public $custOwn;

  /**
   * The comment of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $comment;

  /**
   * The jobsite of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $jobsite;

  /**
   * The customerJobsite of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $customerJobId;

  /**
   * The jobsiteId of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  // public $jobsiteId;

  /**
   * The active of the equipment on rent.
   *
   * @var boolean
   * @OA\Property()
   */
  public $active;

  /**
   * The approverGuid of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $approverGuid;

  /**
   * The approverName of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $approverName;

  /**
   * The requesterGuid of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $requesterGuid;

  /**
   * The requesterName of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $requesterName;

  /**
   * The startDateTime of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $startDateTime;

  /**
   * The returnDateTime of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $returnDateTime;

  /**
   * The pickupDateTime of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $pickupDateTime;

  /**
   * The nextPickupDateTime of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $nextPickupDateTime;

  /**
   * The accountCodes of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $accountCodes;

  /**
   * The returnSeq of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $returnSeq;

  /**
   * The urDeliver of the equipment on rent.
   *
   * @var boolean
   * @OA\Property()
   */
  public $urDeliver;

  /**
   * The pickup of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $pickup;

  /**
   * The pickupId of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $pickupId;

  /**
   * The pickupType of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $pickupType;

  /**
   * The pendingPickup of the equipment on rent.
   *
   * @var boolean
   * @OA\Property()
   */
  public $pendingPickup;

  /**
   * The transId of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $transId;

  /**
   * The transLineId of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $transLineId;

  /**
   * The requisitionId of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $requisitionId;

  /**
   * The gpsUtilization of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $gpsUtilization;

  /**
   * The branchid of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $branchId;

  /**
   * The currency of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $currency;

  /**
   * The leniencyStartDate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $leniencyStartDate;

  /**
   * The leniencyEndDate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $leniencyEndDate;

  /**
   * The customerName of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $customerName;

  /**
   * The simActive of the equipment on rent.
   *
   * @var boolean
   * @OA\Property()
   */
  public $simActive;

  /**
   * The type of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $type;

  /**
   * The rpp of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $rpp;

  /**
   * The $subcategory of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $subcategory;

  /**
  * The createdDateTime of the transaction.
  *
  * @var string
  * @OA\Property()
  */
  public $createdDateTime;

    /**
     * The returnType of the transaction.
     *
     * @var string
     * @OA\Property()
     */
  public $returnType;

  /**
   * @param $accountId
   * @param null $type
   * @return array|false|mixed|object
   */
  public static function index($accountId, $type, $transId, $useCache) {
    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('EquipmentOnRent');

    return $plugin->getEquipmentOnRent($accountId, $type, $transId, $useCache);
  }

}
