<?php

namespace Drupal\ur_frontend_api_v2\Models;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * An ApiResponse object that is sent to the frontend.
 *
 * @OA\Schema(type="object")
 */
class ApiResponse extends JsonResponse {

  /**
   * The response HTTP status code.
   *
   * @var int
   * @OA\Property(format="int32", default=200, example=200)
   */
  protected $code;

  /**
   * The response message.
   *
   * @var string
   * @OA\Property(type="string", example="Success")
   */
  protected $message;

  /**
   * The response data.
   *
   * @var mixed
   * @OA\Property({})
   */
  protected $data;

  /**
   * ApiResponse constructor.
   *
   * @param array $data
   *   The data to include in the response.
   * @param string $code
   *   The response code.
   * @param string $message
   *   The error or response message.
   */
  public function __construct(array $data, $code = '200', $message = 'Success') {
    parent::__construct($data, $code, [], FALSE);

    $this->data = [
      'code' => $code,
      'message' => $message,
      'data' => $data,
      'type' => '',
    ];
  }

  /**
   * Set the error code for the response.
   *
   * @param int $code
   *   The code to use for the response.
   *
   * @return $this
   */
  public function code($code) {
    $this->setStatusCode($code);
    $this->data['code'] = $code;

    return $this;
  }

  /**
   * Set the type for the response.
   *
   * @param string $type
   *   The type of the response.
   *
   * @return $this
   */
  public function type($type) {
    $this->type = $type;
    $this->data['type'] = $type;

    return $this;
  }

  /**
   * Set the message for the response.
   *
   * @param string $message
   *   The message to use for the response.
   *
   * @return $this
   */
  public function statusText($message) {
    $this->statusText = $message;
    $this->data['message'] = $message;

    return $this;
  }

}
