<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class RentalTransaction - A Rental Transaction Object.
 *
 * @OA\Schema(
 *   title="Rental Transaction",
 *   required={
 *    "accountId",
 *    "type",
 *   },
 *   type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class RentalTransaction extends BaseModel {

  public $accountId; // The id of the associated account
//  public $acctCode1;
//  public $acctCode2;
//  public $acctCode3;
//  public $acctCode4;
//  public $acctCode5;
//  public $acctCode6;
  public $approver; // The approver (person)
//  public $approverGuid;
//  public $approverName;
  public $branchId; // The rental branch
  public $bulk; // Bulk item flag (boolean)
  public $catClass; // Rental item cat class
  public $catClassDesc; // Cat class description
  public $createdDateTime; // Date the record was created
  public $createdGuid; // Id of the user who created the record
  public $createdName; // Name of the user who created the record
  public $creditCardTokenId; // The id (token) of the card used
  public $crtGuid; // The GUID of the user who created the record
  public $currency; // The national currency to use
//  public $customerAddress1; // Customer account address 1
//  public $customerCity; // Customer account city
  public $customerJobId;  // Customer jobsite id
  public $customerName; // Customer account name
//  public $customerPhone; // Customer account phone
//  public $customerState; // Customer account state
//  public $customerZip; // Customer account zip
//  public $custOwn; // Equpment is customer owned (boolean)
//  public $cycleBillCode; // Billing cycle code
//  public $dateCanceled; // Date used with types 8 and 9
  public $dateExpired; // Date used with types 2 and 3
  public $dayRate; // Daily rental rate
//  public $daysOffset;
//  public $delComments;
  public $delivery; // Schedule for UR delivery (boolean)
  public $email; // Email address for this transaction
  public $emailPDF; // Send the transaction as a PDF ? (boolean)
  public $eqpDetails; // An array of details: SEE the notes in the Data definition
//  public $eqpValue; // The value of the equipment
  public $equipmentId; // The id of the equipment
//  public $genComments; // Transaction generated comments
//  public $glCode; // G/L Acccount code
//  public $hasFASTPdf; // Has FAST PDF ? (boolean)
  public $id; // The transaction id
  public $includeAllocations; // Include Allocation Codes (boolean)
  public $includeClosed; // Include Closed Contract Details
  public $includeLinked; // Flag to include Linked (child) account data (boolean)
  public $jobContactName; // The job site contact name
  public $jobContactPhone; // The job site contact phone
  public $jobId; // The job site id
  public $jobsiteAddress1; // The job site address 1
  public $jobsiteCity; // The job site city
  public $jobsiteId; // The job site id
  public $jobsiteLocation; // The job site name
  public $jobsiteName; // The UR job site name
  public $jobsitePhone; // The job site phone
  public $jobsiteState; // The job site state
  public $jobsiteZip; // The job site zip
  public $lastBilledDate; // Last date of billing
  public $latitude; // The gps latitude of the job site
  public $leniencyDate; // ?
  public $leniencyEndDate; // The date leniency ends
  public $leniencyStartDate; // The date leniency starts
  public $longitude; // The gps longitude of the job site
  public $make; // The make of the equipment item
//  public $maxRecords; // Maximum number of records to return (index)
//  public $mgrRateReasonCode;
  public $minRate; // The minimum rate for the equipment item
  public $model; // The model of the equipment item
  public $monthRate; // The monthly rate for the equipment item
  public $nextPickupDateTime; // Next pickup for partial pickup
  public $openTrans; // Open transaction flag (boolean)
  public $orderedBy; // The name of the person who ordered the equipment
  public $pendingPickup; // Pickup has been requested (boolean)
  public $pickupDateTime; // Pickup date requested
  public $pickupFirm; // Firm pickup time (boolean)
  public $pickupId; // The id of the pickup record
  public $pickupType; // The type of pickup (advanced / scheduled)
  public $po; // The purchase order number
  public $poNumber; // The purchase order number
//  public $poType;
  public $quantity; // The total quantity for this record
  public $quantityOnRent; // The quantity on rent
  public $quantityPickedUp; // The quantity picked up
  public $quantityReturned; // The quantity returned
  public $rentalAmount; // The total rental amount
  public $reportedDateTime; // The GPS reporting date/time
//  public $reqCode1;
//  public $reqCode2;
//  public $reqCode3;
//  public $reqCode4;
//  public $reqGuid;
//  public $requestContactName;
  public $requester; // The email of the requester
  public $requesterGuid; // The GUID of the requester
  public $requesterName; // The name of the requester
  public $requesterPhone; // The phone of the requester
  public $requisitionId; // The requisition ID
  public $returnDateTime; // The estimated rental return date / time
  public $rpp; // Rental protection plan flag (boolean)
//  public $search; // Term to search for (index)
  public $serial; // The equipment item serial number
  public $simActive; // Flag for equipment item sim active (boolean)
  public $simId; // The id of the sim for the equipment item
//  public $sortBy;
//  public $sortOrder;
  public $startDateTime; // The rental start date / time
//  public $srvComments;
//  public $startRecord;
  public $totalDelivery; // Delivery total
  public $totalPickup; // Pickup total
  public $transId; // The transaction id
  public $transLineId; // The "line id" of the equipment item
  public $transLineType; // The "type" of the line for this line id
  public $transSeqId; // The sequence number for the equipment item
  public $transSource; // THe source of the transaction
  public $transType; // The type of transaction (R, O, C, Q)
  public $type; // The rental transaction type
  public $updateType; // The type of put request (update, reserve, cancel, transaction)
  public $updateUser; // Update the user name
  public $urDeliver; // UR will deliver (boolean)
  public $urPickup; // UR will pick up (boolean)
  public $urWillPickup; // UR will pick up (boolean)
//  public $usageToday;
  public $weekRate; // The weekly rental rate for the equipment item
  public $year; // The year of manufacture for the equipment item

  /**
   * Retrieve a single transaction from the data source.
   *
   * @param string $transId
   *   The Rental Transaction Id.
   *
   * @return \Drupal\ur_frontend_api_v2\Models\RentalTransaction
   *   A single Rental Transaction Object as identified by $transId.
   */
  public static function find($transId) {
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RentalTransaction');

    return $plugin->read($transId);
  }

  /**
   * Save the current instance to the data source.
   *
   * @return mixed
   *   The results of the execute method of the saver worker.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function save() {
    /** @var \Drupal\ur_api_dataservice\Plugins\RentalTransactionPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')->getPlugin('RentalTransaction');

    if ($this->transId === NULL) {
      return $plugin->create($this);
    }

    return $plugin->update($this);
  }

  /**
   * Get an array of all RentalTransaction as defined by the getter.
   *
   * @param string $accountId
   * @param int $type
   *
   * @return array
   *   An array of RentalTransaction Objects.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function index($accountId, $type) {
    /** @var \Drupal\ur_api_dataservice\Plugins\RentalTransactionPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RentalTransaction');

    return $plugin->index($accountId, $type);
  }

}
