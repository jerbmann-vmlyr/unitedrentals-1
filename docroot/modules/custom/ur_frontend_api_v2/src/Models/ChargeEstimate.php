<?php

namespace Drupal\ur_frontend_api_v2\Models;

use Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException;

/**
 * Class ChargeEstimate - A Charge Estimate Object.
 *
 * @OA\Schema(
 *   title="Charge Estimate",
 *   required={
 *    "branchId",
 *    "startDate",
 *    "returnDate",
 *    "delivery",
 *    "pickup",
 *    "items"
 *   },
 *   type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class ChargeEstimate extends BaseModel {

  /**
   * The Branch Id for the charge estimate.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The Branch Id the charge estimate will be based from.",
   *   title="Branch ID",
   *   pattern="^[A-Za-z0-9]{3}$",
   *   example="L09"
   * )
   */
  public $branchId;

  /**
   * The Account Id for the charge estimate (optional)
   *
   * @var string
   * @OA\Property(
   *   description="The Account ID for the charge estimate. Note: this field is required if submitting a jobsiteId. It is not required if supplying address, city, state, & postalCode",
   *   title="Account ID",
   *   example="842859"
   * )
   */
  public $accountId;

  /**
   * The Jobsite ID for the charge estimate.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The Jobsite ID for the charge estimate. Note: this field is required if submitting an accountId. It is not required if supplying address, city, state, & postalCode",
   *   title="Jobsite ID",
   *   example="18"
   * )
   */
  public $jobsiteId;

  /**
   * The starting date for the charge estimate.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The Starting Date - Time for the charge estimate.",
   *   title="Start Date",
   *    example="2018-10-06T06:00:00"
   * )
   */
  public $startDate;

  /**
   * The return date for the charge estimate.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The Returning Date - Time for charge estimate.",
   *   title="Return Date",
   *   example="2018-10-10T06:00:00"
   * )
   */
  public $returnDate;


  /**
   * Will UR be delivering for this charge estimate.
   *
   * @var bool
   * @OA\Property(
   *   description="Flag for if United Rentals will be delivering the equipment up when this order starts.",
   *   title="Delivery",
   *   default=false
   * )
   */
  public $delivery = FALSE;

  /**
   * Will UR be picking up the equipment in this charge estimate.
   *
   * @var bool
   * @OA\Property(
   *   description="Flag for if United Rentals will be picking the equipment up when this order is complete.",
   *   title="Pickup",
   *   default=false
   * )
   */
  public $pickup = FALSE;

  /**
   * Flag to include Rental Protect Plan in the charge estimate.
   *
   * @var bool
   * @OA\Property(
   *   description="Flag to include Rental Protect Plan cost in the charge estimate.",
   *   title="Rental Protection Plan",
   *   default=false
   * )
   */
  public $rpp = FALSE;

  /**
   * The street address to get charge estimate from.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The street address to get charge estimate from. Note: If including accountId & jobsiteId you don't need to supply this.",
   *   title="Address",
   *   example="250 Richards Rd."
   * )
   */
  public $address;

  /**
   * The city to get charge estimate from.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The city to get charge estimate from. Note: If including accountId & jobsiteId you don't need to supply this.",
   *   title="City",
   *   example="Kansas City"
   * )
   */
  public $city;

  /**
   * The 2 character state abbreviation to get charge estimate from.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The US State / Canadian Province abbreviation to get charge estimate from. Note: If including accountId & jobsiteId you don't need to supply this.",
   *   title="State",
   *   pattern="^[A-Z]{2}$",
   *   example="MO"
   * )
   */
  public $state;

  /**
   * The postal code to get charge estimate from.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The postal code to get charge estimate from. Note: If including accountId & jobsiteId you don't need to supply this.",
   *   title="Postal Code",
   *   example="64116"
   * )
   */
  public $postalCode;

  /**
   * Flag to use web rates for the charge estimate.
   *
   * @var bool
   * @OA\Property(
   *   description="Flag to use web rates for the charge estimate.",
   *   title="Web Rates",
   *   default=true
   * )
   */
  public $webRates = TRUE;

  /**
   * The Jobsite ID for the jobsite (optional)
   *
   * @TODO: Provide property description
   * @var bool
   * @OA\Property()
   */
  public $omitNonDspRates = TRUE;

  /**
   * Flag to override the delivery and pickup costs.
   *
   * @var bool
   *
   * @OA\Property(
   *   description="Flag to override the delivery and pickup costs.",
   *   title="Cost Override",
   *   default=false
   * )
   */
  public $costOverride = FALSE;

  /**
   * If costOverride == TRUE we can override the delivery cost with this amount.
   *
   * @var float
   *
   * @OA\Property(
   *   description="If costOverride == TRUE we can override the delivery cost with this amount.",
   *   title="Delivery Cost",
   *   default=null
   * )
   */
  public $deliveryCost = NULL;

  /**
   * If costOverride == TRUE we can override the pickup cost with this amount.
   *
   * @var float
   *
   * @OA\Property(
   *   description="If costOverride == TRUE we can override the pickup cost with this amount.",
   *   title="Pickup Cost",
   *   default=null
   * )
   */
  public $pickupCost = NULL;


  /**
   * The cat classes and quantities of them we're getting estimates for.
   *
   * @var array
   *
   * @OA\Property(
   *   description="Array of items keyed by the cat-class.",
   *   type="object",
   * @OA\AdditionalProperties(
   *     ref="#/components/schemas/ChargeEstimateItem",
   *     example="300-1000"
   *   )
   * )
   */
  public $items = [];

  /**
   * String of cat-class codes and their quantities.
   *
   * @var \Drupal\ur_frontend_api_v2\Models\ChargeEstimateTotals
   *
   * @OA\Property(
   *   description="The totals that have come back from the API for the charge estimate. This is a read-only property only available in the response of the charge estimate.",
   *   ref="#/components/schemas/ChargeEstimateTotals"
   * )
   */
  public $totals;

  /**
   * @return \Drupal\ur_frontend_api_v2\Models\ChargeEstimate
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\DataException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function create($useCache = TRUE) {
    /** @var \Drupal\ur_api_dataservice\Plugins\ChargeEstimatePlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('ChargeEstimate');

    return $plugin->create($this, $useCache);
  }

  /**
   * Override BaseModel import method.
   *
   * @param object $rawData
   *   The data received in the request body for a charge estimate.
   *
   * @return \Drupal\ur_frontend_api_v2\Models\BaseModel
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   */
  public static function import($rawData) {
    $estimate = parent::import($rawData);

    // Format items data into ChargeEstimateItem objects.
    if (!empty($rawData->items)) {
      $items = [];

      // Items data is sent as an associative array keyed by cat-class.
      foreach ($rawData->items as $catclass => $data) {
        $data = (object) $data;

        // Manually set cat-class property into the data for easy import.
        $data->catClass = $catclass;

        // Now create new ChargeEstimateItem object with given data.
        $charge_item = ChargeEstimateItem::import($data);

        $items[] = $charge_item;
      }

      // After building the ChargeEstimateItem objects set them to the estimate.
      $estimate->updateValue('items', $items);
    }
    else {
      // Means we didn't get any items data and that is bad.
      throw new MissingRequiredParametersException('Items are required.', 400);
    }

    return $estimate;
  }

  /**
   * Lookup a ChargeEstimateItem object given a catClass.
   *
   * @param $catClass
   *   The cat-class to search for.
   *
   * @return bool|\Drupal\ur_frontend_api_v2\Models\ChargeEstimateItem
   */
  public function lookupItemByCatClass($catClass) {
    $items = $this->items;

    // Filter to any items in the array of items with the given catclass.
    $found = array_keys(array_filter($items, function ($value) use ($catClass) {
      return $value->catClass == $catClass;
    }));

    // If we found one, return it.
    if (isset($found[0])) {
      return $items[$found[0]];
    }

    // Didn't find one.
    return FALSE;
  }

  /**
   * Override parent BaseModel toArray method.
   *
   * @return array
   *   The array of a ChargeEstimate object.
   */
  public function toArray() {
    $array = parent::toArray();

    unset($array['dataService']);

    // Any property that was null needs to be removed.
    foreach ($array as $key => $value) {
      if ($value === NULL) {
        unset($array[$key]);
      }
    }

    /*
     * Rather than a flat array of ChargeEstimateItem objects, we're serving an
     * associative array of items keyed by cat-class.
     *
     * Need to loop over the items, build the associative array, then unset the
     * ChargeEstimateItem object from the array.
     */
    if (isset($array['items']) && !empty($array['items'])) {
      $items_copy = $array['items'];

      /**
       * @var integer $key
       * @var ChargeEstimateItem $item
       */
      foreach ($items_copy as $key => $item) {
        $array['items'][$item->catClass] = $item->toArray();
        unset($array['items'][$key]);
      }
    }

    // Get the ChargeEstimateTotals object into array.
    if (isset($array['totals']) && !empty($array['totals']) && $array['totals'] instanceof ChargeEstimateTotals) {
      $array['totals'] = $this->totals->toArray();
    }

    return $array;
  }

}
