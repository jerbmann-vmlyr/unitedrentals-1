<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class RequisitionHistory.
 *
 * @package Drupal\ur_frontend_api_v2\Models
 *
 * @OA\Schema(
 *   description="Requisition History Model",
 *   title="Requisition History Model",
 *   required={"requestId","transactionId"}
 *
 * )
 */
class RequisitionHistory extends BaseModel {

  /**
   * The identifier of the requisition - as provided by the data source.
   *
   * @var string
   * @OA\Property()
   */
  public $requestId;

  /**
   * The transaction Id.
   *
   * @var string
   * @OA\Property()
   */
  public $transactionId;

  /**
   * The long form of the status code.
   *
   * @var string
   * @OA\Property()
   */
  public $statusCodeDesc;

  /**
   * The order Status of the requisition.
   *
   * @var string
   * @OA\Property()
   */
  public $orderStatus;

  /**
   * The Human readable order Status.
   *
   * @var string
   * @OA\Property()
   */
  public $orderStatusDesc;

  /**
   * The Order Details.
   *
   * @var string
   * @OA\Property()
   */
  public $orderDetails;

  /**
   * The Status code of the requisition.
   *
   * @var string
   * @OA\Property
   */
  public $statusCode;

  /**
   * Returns a requsition given a requisition ID.
   *
   * @param $requisitionId
   * @return mixed
   */
  public static function find($requisitionId) {
    $rhplugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RequisitionHistory');

    return $rhplugin->read($requisitionId);
  }

}
