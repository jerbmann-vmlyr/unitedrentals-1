<?php


namespace Drupal\ur_frontend_api_v2\Models;


class OrderEquipmentData
{
  /**
   * The transLineId of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $transLineId;

  /**
   * The equipmentidd of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $equipmentId;

  /**
   * The title of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $title;

  /**
   * The urEquipmentId of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $urEquipmentId;

  /**
   * The description of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $description;

  /**
   * The dalDescription of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $dalDescription;

  /**
   * The quantity of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantity;

  /**
   * The quantityOnRent of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantityOnRent;

  /**
   * The quantityReturned of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantityReturned;

  /**
   * The quantityPickedUp of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantityPickedUp;

  /**
   * The quantityPendingPickup of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantityPendingPickup;

  /**
   * The quantityNextPickup of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $quantityNextPickup;

  /**
   * The usageToday of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $usageToday;

  /**
   * The lastMeterChange of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $lastMeterChange;

  /**
   * The remainingQuantity of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $remainingQuantity;

  /**
   * The scheduledPickupQty of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $scheduledPickupQty;

  /**
   * The gps of the equipment on rent.
   *
   * @var array
   * @OA\Property()
   */
  public $gps;

  /**
   * The reportedDateTime of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $reportedDateTime;

  /**
   * The lastBilledDate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $lastBilledDate;

  /**
   * The make of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $make;

  /**
   * The model of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $model;

  /**
   * The year of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $year;

  /**
   * The serial of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $serial;

  /**
   * The eqpType of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $eqpType;

  /**
   * The rate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $rate;

  /**
   * The minRate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $minRate;

  /**
   * The dayRate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $dayRate;

  /**
   * The weekRate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $weekRate;

  /**
   * The monthRate of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $monthRate;

  /**
   * The rentalAmount of the equipment on rent.
   *
   * @var float
   * @OA\Property()
   */
  public $rentalAmount;

  /**
   * The custOwn of the equipment on rent.
   *
   * @var boolean
   * @OA\Property()
   */
  public $custOwn;

  /**
   * The comment of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $comment;

  /**
   * The gpsUtilization of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $gpsUtilization;

  /**
   * The pickupId of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $pickupId;

  /**
   * The pickupType of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $pickupType;

  /**
   * The pendingPickup of the equipment on rent.
   *
   * @var boolean
   * @OA\Property()
   */
  public $pendingPickup;

  /**
 * The pickupDateTime of the equipment on rent.
 *
 * @var string
 * @OA\Property()
 */
  public $pickupDateTime;

  /**
   * The nextPickupDateTime of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $nextPickupDateTime;

  /**
   * The returnSeq of the equipment on rent.
   *
   * @var int
   * @OA\Property()
   */
  public $returnSeq;

  /**
   * The simActive of the equipment on rent.
   *
   * @var boolean
   * @OA\Property()
   */
  public $simActive;

  /**
   * The type of the equipment on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $type;

  /**
   * OrderEquipmentData constructor.
   *
   * @param $incomingData
   */
  public function __construct($incomingData)
  {
    $this->urEquipmentId = $incomingData['urEquipmentId'];
    $this->simActive = $incomingData['simActive'];
    $this->urEquipmentId = $incomingData['urEquipmentId'];
    $this->description = $incomingData['description'];
    $this->dalDescription = $incomingData['dalDescription'];
    $this->quantity = $incomingData['quantity'];
    $this->quantityOnRent = $incomingData['quantityOnRent'];
    $this->quantityReturned = $incomingData['quantityReturned'];
    $this->quantityPickedUp = $incomingData['quantityPickedUp'];
    $this->quantityPendingPickup = $incomingData['quantityPendingPickup'];
    $this->quantityNextPickup = $incomingData['quantityNextPickup'];
    $this->usageToday = $incomingData['usageToday'];
    $this->lastMeterChange = $incomingData['lastMeterChange'];
    $this->remainingQuantity = $incomingData['remainingQuantity'];
    $this->scheduledPickupQty = $incomingData['scheduledPickupQty'];
    $this->gps = $incomingData['gps'];
    $this->reportedDateTime = $incomingData['reportedDateTime'];
    $this->lastBilledDate = $incomingData['lastBilledDate'];
    $this->make = $incomingData['make'];
    $this->model = $incomingData['model'];
    $this->year = $incomingData['year'];
    $this->serial = $incomingData['serial'];
    $this->eqpType = $incomingData['eqpType'];
    $this->rate = $incomingData['rate'];
    $this->minRate = $incomingData['minRate'];
    $this->dayRate = $incomingData['dayRate'];
    $this->weekRate = $incomingData['weekRate'];
    $this->monthRate = $incomingData['monthRate'];
    $this->rentalAmount = $incomingData['rentalAmount'];
    $this->custOwn = $incomingData['custOwn]'];
    $this->comment = $incomingData['comment]'];
    $this->gpsUtilization = $incomingData['gpsUtilization'];
    $this->returnSeq = $incomingData['returnSeq'];
    $this->pickupId = $incomingData['pickupId'];
    $this->pickupDateTime = $incomingData['pickupDateTime'];
    $this->nextPickupDateTime = $incomingData['nextPickupDateTime'];
    $this->pickupType = $incomingData['pickupType'];
    $this->pendingPickup = $incomingData['pendingPickup'];
    $this->transLineId = $incomingData['transLineId'];
    $this->title = $incomingData['title'];
    $this->type = $incomingData['type'];
  }


}