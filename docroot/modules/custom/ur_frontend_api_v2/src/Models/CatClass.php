<?php

namespace Drupal\ur_frontend_api_v2\Models;


/**
 * Class CatClass - A cat class object.
 *
 * @OA\Schema(
 *   title="Features",
 *   type="array",
 *   @OA\Items({}),
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Model
 */
class CatClass extends BaseModel {

  /**
   * The CatClass code.
   *
   * @var string
   * @OA\Property()
   * @required
   */
  public $id;

  /**
   * The cat class description.
   *
   * @var string
   * @OA\Property()
   * @required
   */
  public $description;

  /**
   * The User Provided alternate name for the card.
   *
   * @var string
   * @OA\Property()
   */
  public $imageURL;

  /**
   * The status of the cat class. Active (true or false).
   *
   * @var bool
   * @OA\Property()
   */
  public $active;

  /**
   * The map level to map to the primary category.
   *
   * @var int
   * @OA\Property()
   */
  public $mapLevel;

  /**
   * Indicator of whether to display this cat class in the catalog.
   *
   * @var bool
   * @OA\Property()
   */
  public $displayInCatalog;

  /**
   * Is this cat class a bulk item?
   *
   * @var bool
   * @OA\Property()
   */
  public $bulkItem;

  /**
   * How many are owned.
   *
   * @var string
   * @OA\Property()
   */
  public $qtyOwned;

  /**
   * How many are on rent.
   *
   * @var string
   * @OA\Property()
   */
  public $qtyOnRent;

  /**
   * The "features" or bullet list items about the CatClass.
   *
   * Combines "feature1", "feature2", "feature3" values from API into array.
   *
   * @var array
   *
   * @OA\Property(
   *   @OA\Items(
   *     type="string"
   *   )
   * )
   */
  public $features = [];

  /**
   * Get an array of all CatClasses as defined by the getter.
   *
   * @param array $parameters
   *
   * @return CatClass|array
   *   An array of CatClass Objects.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   */
  public static function index($parameters = []) {
    /** @var \Drupal\ur_api_dataservice\Plugins\CatClassPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('CatClass');

    $catClasses = $plugin->index($parameters);

    return $catClasses;
  }

}
