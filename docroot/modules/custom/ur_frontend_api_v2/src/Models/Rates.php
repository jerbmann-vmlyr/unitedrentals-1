<?php


namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class Rates - A Rental Rates Object.
 *
 * @OA\Schema(
 *   title="Rates",
 *   required={
 *    "catClass",
 *    "branchId",
 *   },
 *   type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class Rates extends BaseModel
{

  /**
   * The account this rate is associated with.
   *
   * @var string
   */
  public $accountId;

  /**
   * The jobsite this rate is associated with.
   *
   * @var string
   */
  public $jobsiteId;

  /**
   * The catClass this rate is associated with.
   *
   * @var string
   */
  public $catClass;

  /**
   * The branch this rate is associated with.
   *
   * @var string
   */
  public $branchId;

  /**
   * The startDate this rate is associated with.
   *
   * @var string
   */
  public $startDate;

  /**
   * The deliveryDistance this rate is associated with.
   *
   * @var int
   */
  public $deliveryDistance;

  /**
   * The includeRateTiers this rate is associated with.
   *
   * @var bool
   */
  public $includeRateTiers;

  /**
   * The webRates this rate is associated with.
   *
   * @var bool
   */
  public $webRates;


  /**
   * Get an array of all Rates filtered by parameters.
   *
   * @param $branchId
   * @param $catClass
   * @param $accountId
   * @param $deliveryDistance
   *
   * @return array|bool|mixed
   *   An array of Rates objects.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \Drupal\ur_api_dataservice\Exceptions\NoResultsException
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   */
  public static function index($branchId, $catClass, $accountId, $deliveryDistance)
  {
    /** @var \Drupal\ur_api_dataservice\Plugins\RatesPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Rates');

    return $plugin->index($branchId, $catClass, $accountId, $deliveryDistance);
  }

}
