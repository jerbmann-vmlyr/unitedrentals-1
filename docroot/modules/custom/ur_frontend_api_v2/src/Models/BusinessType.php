<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class BusinessType - An object mapping to BusinessType taxonomy.
 *
 * @OA\Schema(
 *   title="BusinessType",
 *   required={
 *   },
 *   type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class BusinessType extends BaseModel {
  /**
   * @var string
   * @OA\Property()
   */
  public $tid;

  /**
   * @var string
   * @OA\Property()
   */
  public $name;

  /**
   * @var string
   * @OA\Property()
   */
  public $typeMap;

  /**
   * Get an array of all business types from the drupal taxonomy
   *
   *
   * @return array|mixed
   *   An array of business types.
   */
  public static function index() {
    /** @var \Drupal\ur_api_dataservice\Plugins\BranchPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Branch');
    return $plugin->getBusinessTypes();
  }
}
