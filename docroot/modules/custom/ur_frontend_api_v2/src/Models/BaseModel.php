<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class BaseModel.
 *
 * @package Drupal\ur_frontend_api_v2\Model
 */
class BaseModel {

  /**
   * The dataservice that will be used.
   *
   * @var mixed
   */
  public $dataService;

  /**
   * BaseModel constructor.
   */
  public function __construct() {
    $this->dataService = \Drupal::service('ur_api_dataservice');
  }

  /**
   * Get an array of the current model.
   *
   * @return array
   *   An associative array of all of the properties of this model.
   */
  public function toArray() {
    return get_object_vars($this);
  }

  /**
   * Update the values of the object.
   *
   * @param array $values
   *   An associative array of values to update.
   */
  public function updateValues($values) {
    if (is_string($values)) {
      $values = json_decode($values);
    }

    foreach ($values as $key => $value) {
      $this->updateValue($key, $value);
    }
  }

  /**
   * Update the value of a single property.
   *
   * @param string $key
   *   The key to assign a value.
   * @param mixed $value
   *   The value to assign to the specified key.
   */
  public function updateValue($key, $value) {
    if (property_exists($this, $key)) {
      $this->{$key} = $value;
    }
    else {
      $correctKeyName = $this->findKeyName($key);

      if ($correctKeyName !== NULL) {
        $this->{$correctKeyName} = $value;
      }
    }
  }

  /**
   * Find a property by case-insensitive search.
   *
   * @param string $search
   *   The search string to use.
   *
   * @return mixed
   *   A string representing a property name or null if not found.
   */
  public function findKeyName($search) {
    $propertyNames = array_keys(get_object_vars($this));

    foreach ($propertyNames as $propertyName) {
      if (strcasecmp($search, $propertyName) == 0) {
        return $propertyName;
      }
    }

    return NULL;
  }

  /**
   * Import the given data into a new Object.
   *
   * @param object $rawData
   *   The raw data to load into the object.
   *
   * @return static
   *   A newly hydrated Credit Card Object.
   */
  public static function import($rawData) {
    $item = new static();

    foreach ($rawData as $key => $val) {
      $item->updateValue($key, $val);
    }

    return $item;
  }

}
