<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class EquipmentOwned - An EquipmentOwned object within default radius.
 *
 * @OA\Schema(
 *   title="Equipment Owned",
 *   required={
 *    "catClass",
 *    "branchId",
 *   },
 *   type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Model
 */
class EquipmentOwned extends BaseModel {

  /**
   * Available catClasses.
   *
   * @var string
   * @OA\Property()
   */
  public $catClass;

  /**
   * Branch Id.
   *
   * @var string
   * @OA\Property()
   */
  public $branchId;

  /**
   * Available items count for each catClass.
   *
   * @var string
   * @OA\Property()
   */
  public $ownedQuantity;

  /**
   * Get an array of all EquipmentOwned as defined by the getter.
   *
   * @param $branchId
   * @param $catClass
   *
   * @return array
   *   An array of EquipmentOwned Objects.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function read($branchId, $catClass) {
    /** @var \Drupal\ur_api_dataservice\Plugins\EquipmentOwnedPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('EquipmentOwned');

    return $plugin->read($branchId, $catClass);
  }

}
