<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class RequestPickup - An object with the properties needed to create the pickup request form.
 *
 * @OA\Schema(
 *   title="RequestPickup",
 *   required={
 *   },
 *   type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class RequestPickup {

  public $detailData;
  public $headerData;
  /**
   * Get all the properties needed to create a pickup request
   *
   *
   * @return array|mixed
   *   An object with all the properties needed to create a pickup request.
   */
  public static function index($transId, $equipmentId, $phone, $email, $accountId) {
    /** @var \Drupal\ur_api_dataservice\Plugins\RequestPickupPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('RequestPickup');
    return $plugin->index($transId, $equipmentId, $phone, $email, $accountId);
  }

}
