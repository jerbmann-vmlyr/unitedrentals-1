<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class EquipmentItem - A piece of equipment.
 *
 * @OA\Schema( type="object")
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class EquipmentItem extends BaseModel {

  /**
   * The identifier of the equipment item - as provided by the data source.
   *
   * @var string
   * @OA\Property()
   */
  public $id;

  /**
   * The status of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $status;

  /**
   * The cat class of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $catClass;

  /**
   * The make of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $make;

  /**
   * The model of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $model;

  /**
   * The serialNumber of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $serialNumber;

  /**
   * The gpsId of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $gpsId;

  /**
   * The description of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $description;

  /**
   * The modelYear of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $modelYear;

  /**
   * The lastServiceDate of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $lastServiceDate;

  /**
   * The lastServiceMeter of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $lastServiceMeter;

  /**
   * The imagePath of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $imagePath;

  /**
   * The asgnLoc of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $asgnLoc;

  /**
   * The curLoc of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $curLoc;

  /**
   * The equipStatus of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $equipStatus;

  /**
   * The oec of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $oec;

  /**
   * The miscOptions of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $miscOptions;

  /**
   * The type of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $type;

  /**
   * The catClassVehicle of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $catClassVehicle;

  /**
   * The lastTransDate of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $lastTransDate;

  /**
   * The toolTrailer of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $toolTrailer;

  /**
   * The meter of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $meter;

  /**
   * The maintDate of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $maintDate;

  /**
   * The maintTime of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $maintTime;

  /**
   * The catClassVehicle of the equipment item.
   *
   * @var string
   * @OA\Property()
   */
  public $maintBy;

  /**
   * Retrieve a single card from the data source.
   *
   * @param string $id
   *   The Credit Card Id.
   *
   * @return \Drupal\ur_frontend_api_v2\Models\EquipmentItem
   *   A single Equipment Item Object as identified by $id.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function find($id) {
    /** @var \Drupal\ur_api_dataservice\Plugins\EquipmentItemPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('EquipmentItem');

    $equipment = $plugin->read($id);

    return $equipment;
  }

}
