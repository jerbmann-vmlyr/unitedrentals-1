<?php

namespace Drupal\ur_frontend_api_v2\Models;

/**
 * Class Account - an account object.
 *
 * @OA\Schema(
 *   title="Account object",
 *   description="A single account object.",
 *   required={
 *    "id",
 *    "name",
 *    "address1",
 *    "city",
 *    "state",
 *    "zip",
 *    "phone"
 *   },
 *   type="object"
 * )
 *
 * @package Drupal\ur_frontend_api_v2\Models
 */
class Account extends BaseModel {

  /**
   * The account identifier.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The unique account ID.",
   *   title="Account ID"
   * )
   */
  public $id;

  /**
   * The name of the account.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The name of the account.",
   *   title="Account Name"
   * )
   */
  public $name;


  /**
   * The primary street address.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The street address for the account",
   *   title="Address 1"
   * )
   */
  public $address1;

  /**
   * The secondary location address.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The Suite number, PO Box Number, Floor Number, etc, for the account",
   *   title="Address 2",
   * )
   */
  public $address2;

  /**
   * The city the account resides in.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The City the account is resides in.",
   *   title="City"
   * )
   */
  public $city;

  /**
   * The State the account resides in.
   *
   * @var string
   * @OA\Property()
   */
  public $state;


  /**
   * The Postal Code the account resides in.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The Postal Code the account resides in.",
   *   title="Zip Code"
   * )
   */
  public $zip;

  /**
   * The country code.
   *
   * @var string
   *
   * @OA\Property(
   *   description="The 2 digit country code the account resides in.",
   *   title="Country Code"
   * )
   */
  public $countryCode;

  /**
   * The account credit limit.
   *
   * @var int
   * @OA\Property(
   *
   * )
   */
  public $creditLimit;

  /**
   * Email associated with the account.
   *
   * @var string
   * @OA\Property()
   */
  public $email;

  /**
   * Account Favorite flag.
   *
   * @var bool
   * @OA\Property()
   */
  public $isFavorited = FALSE;


  /**
   * Account job cost flag.
   *
   * @var bool
   * @OA\Property()
   */
  public $jobCost;


  /**
   * The account's parent's id.
   *
   * @var string
   * @OA\Property()
   */
  public $parentId;

  /**
   * The phone number associated with the account.
   *
   * @var string
   * @OA\Property()
   */
  public $phone;

  /**
   * Whether the account requires a PO.
   *
   * @var bool
   * @OA\Property()
   */
  public $requirePo;

  /**
   * The default state of RPP for this account.
   *
   * @var bool
   * @OA\Property()
   */
  public $rppDefault;


  /**
   * The account's status.
   *
   * @var string
   * @OA\Property()
   */
  public $status;

  /**
   * The account's status descriptor.
   *
   * @var string
   * @OA\Property()
   */
  public $statusDesc;

  /**
   * The total control level.
   *
   * @var string
   * @OA\Property()
   */
  public $tcLevel;

  /**
   * The account's terms.
   *
   * @var string
   * @OA\Property()
   */
  public $terms;

  /**
   * Unused variables - available at DAL but not used by Kelex .*/
  // Public $oneTwentyDayAmount;	Decimal	7.2	120+ Day Amount	0
  //  public $thirtyDayAmount;	Decimal	7.2	30 Day Amount	0
  //  public $sixtyDayAmount;	Decimal	7.2	60 Day Amount	0
  //  public $ninetyDayAmount;	Decimal	7.2	90 Day Amount	0
  //  public $averageDays;	Integer	3	Average # of Days to Pay	20
  //  public $consolidatedBill;	String	1	Consolidated Billing Flag	Y, N, or blank
  //  public $consolidatedBillDay;	Integer	2	Consolidated Bill Day of Month	13	0 = No billing day
  //  public $consolidatedBillEmail;	String	50	Consolidated Bill Email	bob@thebuilder.com
  //  public $contactPhone;	String	12	UR Contact Phone	800-877-3687
  //  public $contactPhoneAlpha;	String	12	UR Contact Phone (Alpha)	800-UR-RENTS
  //  public $creditCode;	String	10	Credit Code	Credit	Credit or Cash
  //  public $creditCode2;	String	1	Credit Code	V
  //  public $currentAmount;	Decimal	7.2	Current Amount	925.5
  //  public $customerGroup;	String	7	Customer Group
  //  public $lastConsolidatedBillDate;	Timestamp	 	Last Consolidated Bill Date	2011-11-15T00:00:00:00	May be null
  //  public $lastPaymentAmount;	Decimal	7.2	Last Payment Amount	460.98
  //  public $lastPaymentDate;	Timestamp	 	Last Payment Date	2011-09-05T00:00:00:00
  //  public $lastTransactionDate;	Timestamp	 	Last Transaction Date	2011-10-05T00:00:00:00
  //  public $nextConsolidatedBillDate;	Timestamp	 	Next Consolidated Bill Date	2011-12-15T00:00:00:00	May be null
  //  public $openRentalAmount;	Decimal	7.0	Open Rental Amount	5500
  //  public $rentalLTD;	Decimal	9.2	Rental Lifetime To Date Amount	1540987.42
  //  public $rentalLYR;	Decimal	7.2	Rental Last Year Amount	2379.13
  //  public $rentalYTD;	Decimal	7.2	Rental Year to Date Amount	1566.

  /**
   * Retrieve a single account from the data source.
   *
   * @param string $id
   *   The Account Id.
   *
   * @return \Drupal\ur_frontend_api_v2\Models\Account
   *   A single Account Object as identified by $id.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function find($id) {
    /** @var \Drupal\ur_api_dataservice\Plugins\AccountPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Account');

    $account = $plugin->read($id);

    return $account;
  }

  /**
   * Save the current instance to the data source.
   *
   * @return mixed
   *   The results of the execute method of the saver worker.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function save() {
    /** @var \Drupal\ur_api_dataservice\Plugins\AccountPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Account');
    
    if ($this->id === NULL) {
      return $plugin->create($this);
    }

    return $plugin->update($this);
  }

  /**
   * Get an array of all Accounts filtered by parameters.
   *
   * @param array $params
   *   Key / value pairs sent from the initial request.
   *
   * @return array|bool|mixed
   *   An array of Account objects.
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \Drupal\ur_api_dataservice\Exceptions\NoResultsException
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   */
  public static function index(array $params) {
    /** @var \Drupal\ur_api_dataservice\Plugins\AccountPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Account');

    return $plugin->index($params);
  }

  /**
   * Update the favorite preferences to user preferences.
   *
   * @param array $params
   *   Key / value pairs sent from the initial request.
   *
   * @return bool|mixed
   *
   * @throws \Drupal\ur_api_dataservice\Exceptions\DalException
   * @throws \Drupal\ur_api_dataservice\Exceptions\MissingRequiredParametersException
   * @throws \Drupal\ur_api_dataservice\Exceptions\NoResultsException
   * @throws \Drupal\ur_api_dataservice\Exceptions\ParameterTypeException
   */
  public static function updateFavorites($accountId, array $params) {
    /** @var \Drupal\ur_api_dataservice\Plugins\AccountPlugin $plugin */
    $plugin = \Drupal::service('ur_api_dataservice')
      ->getPlugin('Account');

    return $plugin->updateFavorites($accountId, $params);
  }

}
