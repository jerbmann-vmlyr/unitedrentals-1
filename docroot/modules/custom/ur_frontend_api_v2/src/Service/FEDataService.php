<?php

namespace Drupal\ur_frontend_api_v2\Service;

/**
 * Class FEDataService.
 *
 * @package Drupal\ur_frontend_api_v2\Service
 */
abstract class FEDataService {

  /**
   * The name of the service being used.
   *
   * @var string
   */
  public $serviceName;

  /**
   * Create a new instance - used by Symfony DI.
   *
   * @param \Drupal\ur_frontend_api_v2\Service\ContainerInterface $container
   *   ContainerInterface via DI.
   *
   * @return \Drupal\ur_frontend_api_v2\Service\FEDataService
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('ur_api_dataservice'));
  }

  /**
   * Get an instance of the class specified by the plugin name.
   *
   * @param string $pluginName
   *   The name of the worker plugin to retrieve.
   *
   * @return mixed
   *   An instance of the worker plugin requested.
   */
  public function getPlugin($pluginName) {
    $className = str_replace('\DataService', '', get_class($this));
    $namespacedName = "$className\\Plugins\\" . $pluginName . 'Plugin';
    $namespacedModel = str_replace('\\Service', '', __NAMESPACE__) . "\\Models\\$pluginName";
    $nsModel = FALSE;

    if (class_exists($namespacedModel)) {
      $nsModel = $namespacedModel;
    }

    $plugin = new $namespacedName($nsModel);

    return $plugin;
  }

  /**
   * Returns a string that describes this data source.
   *
   * @return string
   *   Description of the data service implementation.
   */
  abstract public function describe();

}
