<?php

namespace Drupal\ur_frontend_api_v2\Traits;

use Symfony\Component\HttpFoundation\Request;

/**
 * ApiTrait.
 */
trait ApiTrait {

  protected $requestParams;
  protected $request;

  /**
   * Retrieves the currently active request object.
   */
  protected function getRequest() {
    if (empty($this->request)) {
      return \Drupal::request();
    }

    return $this->request;
  }

  protected function setRequest(Request $request) {
    $this->request = $request;
    $this->paramsGetOrPost();
  }

  /**
   * Get URL parameters based on if they are coming in via POST or GET request
   * methods.
   *
   * @throws \LogicException
   */
  public function paramsGetOrPost() {
    // Load from POST.
    if (empty($this->requestParams)) {
      $params = $this->getRequest()->getContent();
      $params = json_decode($params, TRUE);
      $params = (array) $params;

      // If nothing, there we assume it's a GET.
      if (empty($params)) {
        $params = $this->getRequest()->query->all();
      }

      $this->requestParams = $params;
    }

    return $this->requestParams;
  }

  /**
   * @param $key
   * @param null $default
   *
   * @return null
   */
  public function getRequestValue($key, $default = NULL) {
    $this->paramsGetOrPost();

    if (isset($this->requestParams[$key])) {
      return $this->requestParams[$key];
    }

    return $default;
  }

  /**
   * @return mixed
   */
  public function getRequestParams() {
    return $this->requestParams;
  }

}
