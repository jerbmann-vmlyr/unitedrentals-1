<?php

namespace Drupal\ur_frontend_api_v2\Traits;

use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\ur_frontend_api_v2\Models\EquipmentOnRent;
use Drupal\ur_appliance\Exception\DAL\DalDataException;

trait RetrieveDrupalData {

  /**
   * Uses preset data to load the Drupal specific info.
   *
   * Loads the Drupal data for anything that might come from Drupal.
   *
   * @param \Drupal\ur_frontend_api_v2\Models\EquipmentOnRent $equipmentOnRent
   * @return \Drupal\ur_frontend_api_v2\Models\EquipmentOnRent
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function loadDrupalData(EquipmentOnRent $equipmentOnRent) {

    // First try to load from Drupal config; as long as a config.factory exists.
    if (empty(\Drupal::hasService('config.factory'))) {
      throw new DalDataException('Unable to load data because Drupal has not been initiated.');
    }

    if (empty($equipmentOnRent->equipmentId) && empty($equipmentOnRent->catClass)) {
      throw new DalDataException('Unable to load Drupal data without a node ID or cat class code.');
    }

    $node = NULL;

    try {
      // todo: this works w/ 'id' but not equipmentId, is this correct?
      // todo: why bother? we know id is always null from DAL
      if (!empty($equipmentOnRent->id)) {
        $node = Node::load($equipmentOnRent->id);
      }
      elseif (!empty($equipmentOnRent->catClass) && \Drupal::hasService('entity.query')) {
        $catClass = $equipmentOnRent->catClass;

        $query = \Drupal::entityQuery('node')
          ->condition('type', 'item')
          ->condition('field_item_cat_class_code', $catClass);

        $nids = $query->execute();

        if (!empty($nids)) {
          $nodes = Node::loadMultiple($nids);
          /** @var \Drupal\node\Entity\Node $node */
          $node = \array_pop($nodes);
        }
      }

      if (!empty($node) && \is_object($node)) {
        $equipmentOnRent->title = $node->get('title')->getString();
        $equipmentOnRent->description = $node->get('field_plain_long')->getString();
        $equipmentOnRent->subcategory = $this->setCategories($node->get('field_item_category')->getValue());
      }
    }
    catch (\Exception $e) {
      // Don't do anything. Just don't let this kill the call
    }

    return $equipmentOnRent;

  }

  /**
   * Locates and loads the taxonomy term tids in the $categories array.
   *
   * @param array $categories
   *   The field_item_category taxonomy term ids for a given
   *   equipment item.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function setCategories(array $categories) {

    $this->parents = [];

    if (empty($categories)) {
      return;
    }

    foreach ($categories as $category) {
      if (empty($category)) {
        continue;
      }

      // Gotta load this to get the taxonomy vid.
      $term = Term::load($category['target_id']);

      // todo: jump in here and see if we can get string from $term in a
      // todo: less complicated way than below

      if (empty($term) || !\is_object($term)) {
        continue; // We can't act on a non-object.
      }

      // Need all the parents of this term to be sure we find
      // the category we need where depth == 1.
      $parents = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadAllParents($category['target_id']);

      if (empty($parents) || !\is_array($parents)) {
        continue; // We can't act on a non-object.
      }

      // Not my favorite way to do this, but Core assures me
      // this is cached and is not CPU-intensive.
      $taxonomy_tree = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree($term->get('vid')->getString());

      if (empty($taxonomy_tree)) {
        continue; // We can't act on a non-object.
      }

      // Now we loop through the entire tree so we can see the
      // depth of each relevant parent.
      foreach ($taxonomy_tree as $leaf) {
        if (empty($leaf) || !\is_object($leaf) || $leaf->depth != 1) {
          continue; // We can't act on a non-object.
        }

        if (\array_key_exists($leaf->tid, $parents)) {
          $this->parents[$leaf->tid] = $leaf;

          // And here is the one we actually want!
          if ($leaf->depth == 1) {
            // $this->subcategory = $leaf->name;
            return $leaf->name;
            break;
          }
        }
      }
    }
    return '';
  }

}
