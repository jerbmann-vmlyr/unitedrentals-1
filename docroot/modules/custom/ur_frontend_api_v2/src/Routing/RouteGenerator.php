<?php

namespace Drupal\ur_frontend_api_v2\Routing;

use Symfony\Component\Config\FileLocator;
use Doctrine\Common\Annotations\DocParser;
use Doctrine\Common\Annotations\AnnotationReader;
use Drupal\ur_frontend_api_v2\Loader\URAnnotationClassLoader;
use Symfony\Component\Routing\Loader\AnnotationDirectoryLoader;

/**
 * A RouteGenerator class to wrap a method in.
 */
class RouteGenerator {

  /**
   * Generate all the routes 【ツ】.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   * @throws \Doctrine\Common\Annotations\AnnotationException
   */
  public static function generate() {
    $fileLocator = new FileLocator(drupal_get_path('module', 'ur_frontend_api_v2'));

    $parser = new DocParser($fileLocator);
    $reader = new AnnotationReader($parser);

    $classLoader = new URAnnotationClassLoader($reader);
    $routeLoader = new AnnotationDirectoryLoader($fileLocator, $classLoader);

    $routes = $routeLoader->load('src/Controller/');
    $routes->addPrefix('/api/v2');

    return $routes;
  }

}
