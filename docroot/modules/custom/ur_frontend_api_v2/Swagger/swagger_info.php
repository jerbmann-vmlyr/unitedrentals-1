<?php

/**
 * @file
 *
 * @OA\Info(
 *   title="UR Front End API",
 *   description="Integration",
 *   version="2.0-beta",
 * )
 */

/**
 * @OA\Server(
 *   url="https://unitedrentals.docksal/api/v2",
 *   description="Local API",
 * )
 */

/**
 * @OA\SecurityScheme (
 *   securityScheme="WORSToken",
 *   type="apiKey",
 *   in="header",
 *   name="Authorization"
 * )
 * @OA\SecurityScheme (
 *   securityScheme="basicAuth",
 *   type="http",
 *   scheme="basic"
 * )
 */
