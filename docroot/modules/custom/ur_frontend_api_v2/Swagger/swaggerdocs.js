const chokidar = require('chokidar');
const cmd = require('node-cmd');
const suw = require('swagger-ui-watcher');

cmd.run('sh Swagger/updateswaggerdocs.sh');

const watcher = chokidar.watch(".",
  {
    ignored: /node_modules|vendor|swagger.json/,
    persistent: true
  }
);

watcher
  .on('add', (docname) => {
    updateDocs(docname);
  })
  .on('change', (docname) => {
    updateDocs(docname);
  })
  .on('unlink', (docname) => {
    updateDocs(docname);
  })
  .on('unlinkDir', (docname) => {
    updateDocs(docname);
  });

function updateDocs(docname) {
  console.log('File Changed', docname, 'Updating swagger');
  cmd.run('sh Swagger/updateswaggerdocs.sh');
}

suw.start('./Swagger/swagger.json', "./Swagger", 4005, 'localhost');
suw.build('./Swagger/swagger.json', '../../../themes/custom/unitedrentals/dist/swg', '../../../themes/custom/unitedrentals/dist/swg/swagger.json');