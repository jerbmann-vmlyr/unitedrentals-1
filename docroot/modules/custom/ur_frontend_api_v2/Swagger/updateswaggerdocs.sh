#!/bin/bash
#this file re-renders the swagger.json file, which is then pushed to the browsersw
rootprojectfolder=$(pwd | sed 's/\(^.*\)\(docroot\).*/\1/')
$rootprojectfolder/vendor/bin/openapi --output ./Swagger/swagger.json --exclude ./vendor ./
