(function($, Drupal) {
  Drupal.behaviors.ur_employee = {
    attach: function (context, settings) {
      $('#quote_link_button', context).on('click', function(event) {
        event.preventDefault();

        if (document.selection) { // IE
          var range = document.body.createTextRange();
          range.moveToElementText(document.getElementById('copy-quote-link'));
          range.select();
        }
        else if (window.getSelection) {
          var range = document.createRange();
          range.selectNode(document.getElementById('copy-quote-link'));
          window.getSelection().removeAllRanges();
          window.getSelection().addRange(range);
        }

        document.execCommand("Copy", false, null);
      });

      $('#quote_reset_button', context).on('click', function(event) {
        event.preventDefault();
        location.reload();
      });
    }
  };
})(jQuery, Drupal);
