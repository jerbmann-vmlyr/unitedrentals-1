<?php

namespace Drupal\ur_employee\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EmployeeSettingsForm.
 *
 * Provides configuration settings for the UR Employee module.
 *
 * @package Drupal\ur_employee\Form
 */
class EmployeeSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ur_employee_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_employee.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_employee.settings');

    $form['quote_generator_url_parameters'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Quote Generator URL Parameters'),
      '#description' => $this->t(
        'Extra parameters to append to any generated quotes. Do not include initial ? or &.'
      ),
      '#default_value' => $config->get('quote_generator_url_parameters'),
      '#required' => TRUE,
    ];

    $form['quote_generator_sso_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SSO Domain'),
      '#description' => $this->t(
        "Domain for this environment's SSO server, for generating quote links."
      ),
      '#default_value' => $config->get('quote_generator_sso_domain'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_employee.settings');

    $config->set(
      'quote_generator_url_parameters',
      $form_state->getValue('quote_generator_url_parameters')
    )->save();

    $config->set(
      'quote_generator_sso_domain',
      $form_state->getValue('quote_generator_sso_domain')
    )->save();

    parent::submitForm($form, $form_state);
  }

}
