<?php

namespace Drupal\ur_employee\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Exception;
use Drupal\ur_appliance\Provider\DAL;

/**
 * Class QuoteLinkGeneratorForm.
 *
 * @package Drupal\ur_employee\Form
 */
class QuoteLinkGeneratorForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quote_link_generator_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_employee.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state):array {
    $form['#attributes']['novalidate'] = 'novalidate';

    $form['cols'] = [
      '#prefix' => '<div',
      '#suffix' => '</div>',
    ];

    $form['cols']['column-left'] = [
      '#prefix' => '<div class="column left"',
      '#suffix' => '</div>',
    ];

    $form['cols']['column-left']['content'] = [
      '#markup' => '
        <div><h2>Steps to Generate Link</h2>
        <ul>
          <li>Enter customer&#39;s email address, account, and quote number.</li>
          <li>Click &ldquo;Submit&rdquo;.</li>
          <li>One of three things will occur:
            <ul>
              <li>If email is already linked to the account, quote link will be generated.</li>
              <li>If email is not associated with the account, click &ldquo;Link Account&rdquo;. A quote link will then be generated.</li>
              <li>If customer is new, fill in the Customer Information form and click &ldquo;Submit&rdquo;. A link to create an online profile will be generated. After customer creates a password, they can login to see their quote.</li>
            </ul>
          </li>
          <li>Copy link and paste into an email to the customer. Then click Reset Form.</li>
        </ul></div>',
    ];

    $form['cols']['column-right'] = [
      '#prefix' => '<div class="column right"><div',
      '#suffix' => '</div></div>',
    ];

    $form['cols']['column-right']['title'] = [
      '#markup' => '<h2>Quote Generator</h2><div id="message_linked" class="message input-error"></div>'
    ];
    $form['cols']['column-right']['email_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Address'),
      '#size' => 64,
      '#default_value' => '',
      '#suffix' => '<div id="message_email_address" class="message input-error"></div>',
    ];

    $form['cols']['column-right']['account_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account Number'),
      '#maxlength' => 20,
      '#size' => 64,
      '#default_value' => '',
      '#suffix' => '<div id="message_account_number" class="message input-error"></div>',
    ];

    $form['cols']['column-right']['quote_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Quote Number'),
      '#maxlength' => 20,
      '#size' => 64,
      '#default_value' => '',
      '#suffix' => '<div id="message_quote_number" class="message input-error"></div>',
    ];

    $form['cols']['column-right']['customer_information'] = array(
      '#type' => 'fieldset',
      '#attributes' => [
        'class' => [
          'visually-hidden'
        ]
      ]
    );
    $form['cols']['column-right']['customer_information']['title'] = [
      '#markup' => '<h2>Customer Information</h2>'
    ];

    $form['cols']['column-right']['customer_information']['first_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Name'),
      '#maxlength' => 20,
      '#size' => 64,
    ];

    $form['cols']['column-right']['customer_information']['last_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Name'),
      '#maxlength' => 20,
      '#size' => 64,
    ];

    $form['cols']['column-right']['customer_information']['phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone Number'),
      '#maxlength' => 20,
      '#size' => 64,
    ];

    $form['cols']['column-right']['customer_information']['phone_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Phone Type'),
      '#options' => [
        'mobile' => $this->t('Mobile'),
        'landline' => $this->t('Landline'),
      ],
      '#empty_value' => '',
      '#empty_option' => '',
    ];

    $form['cols']['column-right']['customer_information']['account_pin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account Pin'),
      '#maxlength' => 20,
      '#size' => 64,
    ];

    $form['cols']['column-right']['actions']['submit_quote'] = [
      '#type' => 'button',
      '#name' => 'submit_quote',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => '::submitQuote',
        'effect' => 'fade',
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $form['cols']['column-right']['actions']['submit_account_link'] = [
      '#type' => 'button',
      '#name' => 'submit_account_link',
      '#value' => $this->t('Link Account'),
      '#attributes' => [
        'class' => [
          'visually-hidden'
        ]
      ],
      '#ajax' => [
        'callback' => '::submitAccountLink',
        'effect' => 'fade',
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $form['cols']['column-right']['customer_information']['submit_register'] = [
      '#type' => 'button',
      '#name' => 'submit_register',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => [
          'visually-hidden'
        ]
      ],
      '#ajax' => [
        'callback' => '::submitRegister',
        'effect' => 'fade',
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $form['cols']['column-right']['quote_link'] = [
      '#markup' => '
<div class="copy-quote-container">
  <span>Quote Link</span>
  <div id="copy-quote-link-wrapper"></div>
  <a id="quote_link_button" href="#">
    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 448 512"><path d="M320 448v40c0 13.255-10.745 24-24 24H24c-13.255 0-24-10.745-24-24V120c0-13.255 10.745-24 24-24h72v296c0 30.879 25.121 56 56 56h168zm0-344V0H152c-13.255 0-24 10.745-24 24v368c0 13.255 10.745 24 24 24h272c13.255 0 24-10.745 24-24V128H344c-13.2 0-24-10.8-24-24zm120.971-31.029L375.029 7.029A24 24 0 0 0 358.059 0H352v96h96v-6.059a24 24 0 0 0-7.029-16.97z"/></svg>
  </a>
</div>
<div class="quote-reset-wrapper"><a id="quote_reset_button" href="#">Reset Form</a></div>
',
      '#allowed_tags' => ['svg', 'path', 'a', 'div', 'input'],
    ];

    $form['cols']['column-right']['#attached'] = [
      'library' => [
        'core/jquery',
        'ur_employee/ur_employee',
      ]
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return AjaxResponse
   */
  public function submitQuote(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $validated = TRUE;

    $response->addCommand(new HtmlCommand('#message_linked', ''));
    $response->addCommand(new HtmlCommand('#message_email_address', ''));
    $response->addCommand(new HtmlCommand('#message_account_number', ''));
    $response->addCommand(new HtmlCommand('#message_quote_number', ''));

    if (empty($form_state->getValue('email_address'))) {
      $response->addCommand(new HtmlCommand('#message_email_address', 'Email address cannot be empty.'));
      $validated = FALSE;
    }

    if (empty($form_state->getValue('account_number'))) {
      $response->addCommand(new HtmlCommand('#message_account_number', 'Account number cannot be empty.'));
      $validated = FALSE;
    }

    if (empty($form_state->getValue('quote_number'))) {
      $response->addCommand(new HtmlCommand('#message_quote_number', 'Quote number cannot be empty.'));
      $validated = FALSE;
    }
    else {
      // Check Quote ID to see if it's valid
      try {
        $transaction_plugin = \Drupal::service('ur_api_dataservice')
          ->getPlugin('RentalTransaction');
        $quote = $transaction_plugin->read($form_state->getValue('quote_number'), TRUE);
      }
      catch (Exception $e) {
        // General error occurred finding the quote.
        $response->addCommand(new HtmlCommand('#message_quote_number', $this->t('Error retrieving quote')));
        $validated = FALSE;
      }

      if (empty($quote->transId)) {
        // Quote ID does not exist.
        $response->addCommand(new HtmlCommand('#message_quote_number', $this->t('Quote ID not found')));
        $validated = FALSE;
      }
      else {
        if ($quote->accountId != $form_state->getValue('account_number')) {
          $response->addCommand(new HtmlCommand('#message_linked', $this->t("Account and Quote don't match. Verify Account Number and Quote Number are correct")));
          $validated = FALSE;
        }
      }
    }

    if ($validated) {
      // Check if user account exists.
      try {
        $accounts = DAL::Customer(TRUE)->TcUser()->read($form_state->getValue('email_address'));

        // Check if customer needs to be linked to the account.
        if (!array_key_exists($form_state->getValue('account_number'), $accounts)) {
          // User is valid but does not have access to account, show linking button and message.
          $response->addCommand(new HtmlCommand('#message_linked', $this->t('* Email and Account ID are not linked.')));
          $response->addCommand(new InvokeCommand('#edit-submit-account-link', 'removeClass', ['visually-hidden']));
          $response->addCommand(new InvokeCommand('#edit-submit-quote', 'addClass', ['visually-hidden']));
          $response->addCommand(new InvokeCommand('#edit-quote-link', 'val', ['']));

          return $response;
        }
        else {
          $response->addCommand(new InvokeCommand('#edit-submit-account-link', 'addClass', ['visually-hidden']));
        }

        $response->addCommand(new InvokeCommand('#edit-customer-information', 'addClass', ['visually-hidden']));
      }
      catch (Exception $e) {
        // User account does not exist, show registration form.
        $response->addCommand(new InvokeCommand('#edit-customer-information', 'removeClass', ['visually-hidden']));
        $response->addCommand(new InvokeCommand('#edit-submit-register', 'removeClass', ['visually-hidden']));
        $response->addCommand(new InvokeCommand('#edit-submit-quote', 'addClass', ['visually-hidden']));
        $response->addCommand(new InvokeCommand('#edit-quote-link', 'val', ['']));

        return $response;
      }

      // Create quote link.
      $host = \Drupal::request()->getSchemeAndHttpHost();
      $config_parameters = \Drupal::config('ur_employee.settings')->get('quote_generator_url_parameters');
      $quote_link = "<a id=\"copy-quote-link\" href=\"{$host}/manage/quotes#/convert-quote/{$form_state->getValue('quote_number')}/{$form_state->getValue('account_number')}?{$config_parameters}\">Quote # {$form_state->getValue('quote_number')}</a>";
      $response->addCommand(new InvokeCommand('#copy-quote-link-wrapper', 'html', [$quote_link]));
      $response->addCommand(new InvokeCommand('.copy-quote-container', 'addClass', ['generated']));
    }

    return $response;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return AjaxResponse
   */
  public function submitAccountLink(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    try {
      // Link the user to the account.
      $guid = DAL::Customer(TRUE)->TcUser()->readSimple($form_state->getValue('email_address'));

      DAL::Customer()->Account()->linkAccount([
        'accountId' => $form_state->getValue('account_number'),
        'userGuid' => $guid,
      ]);
      $userId = user_load_by_name($form_state->getValue('email_address'))->id();
      $cacheId = "UID:$userId|DalController|accounts|";
      \Drupal::cache()->invalidate($cacheId);

      // Generate a quote link.
      $host = \Drupal::request()->getSchemeAndHttpHost();
      $config_parameters = \Drupal::config('ur_employee.settings')->get('quote_generator_url_parameters');
      $quote_link = "<a id=\"copy-quote-link\" href=\"{$host}/manage/quotes#/convert-quote/{$form_state->getValue('quote_number')}/{$form_state->getValue('account_number')}?{$config_parameters}\">Quote # {$form_state->getValue('quote_number')}</a>";
      $response->addCommand(new InvokeCommand('#copy-quote-link-wrapper', 'html', [$quote_link]));
      $response->addCommand(new InvokeCommand('.copy-quote-container', 'addClass', ['generated']));
      $response->addCommand(new InvokeCommand('#edit-submit-account-link', 'addClass', ['visually-hidden']));
      $response->addCommand(new HtmlCommand('.input-error', ''));
      $response->addCommand(new HtmlCommand('#message_linked', $this->t('Account linked')));
    }
    catch (Exception $e) {
      $response->addCommand(new HtmlCommand('#message_account_number', $this->t('Error linking account')));
    }

    return $response;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return AjaxResponse
   */
  public function submitRegister(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $login_domain = \Drupal::config('ur_employee.settings')->get('quote_generator_sso_domain');
    $config_parameters = \Drupal::config('ur_employee.settings')->get('quote_generator_url_parameters');
    $quote_url = "{$host}/manage/quotes#/convert-quote/{$form_state->getValue('quote_number')}/{$form_state->getValue('account_number')}?{$config_parameters}";

    $url_params = [
      'firstName' => $form_state->getValue('first_name'),
      'lastName' => $form_state->getValue('last_name'),
      'phoneNumber' => $form_state->getValue('phone_number'),
      'phoneSelect' => $form_state->getValue('phone_type'),
      'accountId' => $form_state->getValue('account_number'),
      'accountPIN' => $form_state->getValue('account_pin'),
      'email' => $form_state->getValue('email_address'),
      'ReturnTo' => $quote_url,
    ];

    // Generate user registration link with redirect to quote
    $registration_link = "<a id=\"copy-quote-link\" href=\"https://{$login_domain}/home/index/register?" . http_build_query($url_params) . "\">Quote # {$form_state->getValue('quote_number')}</a>";
    $response->addCommand(new InvokeCommand('#copy-quote-link-wrapper', 'html', [$registration_link]));
    $response->addCommand(new InvokeCommand('.copy-quote-container', 'addClass', ['generated']));
    $response->addCommand(new HtmlCommand('.input-error', ''));

    return $response;
  }

}
