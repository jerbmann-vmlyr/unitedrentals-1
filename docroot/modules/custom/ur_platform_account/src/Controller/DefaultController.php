<?php

namespace Drupal\ur_platform_account\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class DefaultController.
 *
 * @package Drupal\ur_platform_account\Controller
 */
class DefaultController extends ControllerBase {

  /**
   * Reservations.
   *
   * @return array
   *   Return markup needed for Vue component.
   */
  public function reservations() {
    return [
      '#type' => 'markup',
      '#markup' => '<reservations></reservations>',
      '#allowed_tags' => ['reservations'],
      '#attached' => [
        'drupalSettings' => [
        'vueRouter' => TRUE
       ],
     ],
    ];
  }

  /**
   * Quotes.
   *
   * @return array
   *   Return markup needed for Vue component.
   */
  public function quotes() {
    return [
      '#type' => 'markup',
      '#markup' => '<quotes></quotes>',
      '#allowed_tags' => ['quotes'],
      '#attached' => [
        'drupalSettings' => [
          'vueRouter' => TRUE,
        ],
      ],
    ];
  }

}
