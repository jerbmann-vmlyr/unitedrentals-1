User Preferences Settings
-------------------------

To add user specific preferences you will need to POST to the "/api/v1/user/preferences/{PREFERENCE_NAME}" path. You will just post any JSON you want and that will end up in the preference you are saving.

Example POST:
```
jQuery.post(
  '/api/v1/user/preferences/dashboard-panels',
  {
    columnOrder: [
      'cat-class', 'location', 'description', 'actions'
    ],
    enabledPanels: [
      'rented-items', 'cost-ratio', 'daily-activity'
    ],
    theme: 'dark',
    likesFreeThings: true
  }
);
```

To retrieve those values you just need to do a GET against the same path:
```
jQuery.get('/api/v1/user/preferences/dashboard-panels', function(data, status) { console.log("Data: " + data + "\nStatus: " + status); });
```

As you can see, we are changing and loading the values for a preference called "dashboard-panels".
