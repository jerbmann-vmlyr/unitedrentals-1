<?php

namespace Drupal\ur_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\ur_frontend_api\Traits\ApiTrait;
use Drupal\user\UserDataInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DalController.
 *
 * @package Drupal\ur_frontend_api\Controller
 */
class UserController extends ControllerBase {
  use ApiTrait;

  /**
   * @return UserDataInterface
   */
  public static function loadUserData() {
    /** @var UserDataInterface $userData */
    return \Drupal::service('user.data');
  }

  public function read($preferenceName, $json = TRUE) {
    // Load the current user's preferences
    $userData = self::loadUserData();
    $uid = \Drupal::currentUser()->id();

    // Make sure user is logged in.
    $data = NULL;
    if ($uid > 0) {
      $data = $userData->get('ur_user', $uid, $preferenceName);
    }
    // If not logged in, try session.
    elseif (isset($_SESSION['user_data'][$preferenceName])) {
      $data = $_SESSION['user_data'][$preferenceName];
    }

    switch ($preferenceName) {
      case 'ratesPlaceBranch':
        if (is_array($data)) {
          $data = reset($data);
        }
        break;
      default:
        break;
    }

    // Skip json?
    if (!$json) {
      return $data;
    }

    $response = new JsonResponse();
    $response->setData($data);

    return $response;
  }

  public function update($preferenceName, $data) {
    $userData = self::loadUserData();
    $uid = \Drupal::currentUser()->id();

    // Store some prefs in session.
    if (!isset($_SESSION['user_data'])) {
      $_SESSION['user_data'] = [];
    }
    switch ($preferenceName) {
      case 'defaultAccountId':
        // Set default account id in SESSION.
        if (!empty($data) && isset($data[0]) && !empty($data[0])) {
          $_SESSION['user_data'][$preferenceName] = $data[0];
        }
        break;
      case 'ratesPlace':
        // Set default account id in SESSION.
        if (!empty($data) && isset($data['address_components']) && !empty($data['address_components'])) {
          $_SESSION['user_data'][$preferenceName] = $data;
        }
        break;
      case 'ratesPlaceBranch':
        if (!empty($data)) {
          if (is_array($data) && isset($data[0]) && !empty($data[0])) {
            $_SESSION['user_data'][$preferenceName] = $data[0];
          }
          elseif (is_scalar($data)) {
            $_SESSION['user_data'][$preferenceName] = $data;
          }
        }
        break;
      default:
        break;
    }

    try {
      // Update the current user's preferences
      if ($uid > 0) {
        $userData->set('ur_user', $uid, $preferenceName, $data);
      }
    }
    catch (\Exception $e) {
      return FALSE;
    }

    return TRUE;
  }

  public function updateForAPI($preferenceName) {
    $response = new JsonResponse();

    $data = $this->paramsGetOrPost();
    $result = $this->update($preferenceName, $data);

    $response->setData($result);

    return $response;
  }

  public function updateFavoritePreferences($preferenceValue) {
    $preferenceName = 'favoriteAccounts';
    $this->update($preferenceName, $preferenceValue);

    return $this->read($preferenceName);
  }

  public function getLoggedInUsername() {
    $response = new JsonResponse();
    $response->setData(\Drupal::currentUser()->getAccount()->name);

    return $response;
  }

}
