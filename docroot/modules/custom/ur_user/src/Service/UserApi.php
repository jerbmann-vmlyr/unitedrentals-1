<?php

namespace Drupal\ur_user\Service;

use Drupal\user\Entity\User;

/**
 * Class UserApi.
 *
 * @package Drupal\ur_user\Service
 */
class UserApi {
  /**
   *  Returns formatted user display name for current logged in user.
   */
  function getCurrentUserDisplayName() {
    $current_user = User::load(\Drupal::currentUser()->id());

    return $this->getDisplayName($current_user);
  }

  /**
   * Returns formatted user display name.
   *
   * @param $user
   *  A fully-loaded Drupal user entity.
   * @return string
   *  Formatted user display name, falling back to email address.
   */
  function getDisplayName($user) {
    $first_name = $user->get('field_first_name')->value;
    $last_name = $user->get('field_last_name')->value;

    if ($first_name && $last_name) {
      return $first_name . ' ' . $last_name;
    }

    if ($first_name || $last_name) {
      return ($first_name) ? $first_name : $last_name;
    }

    return $user->get('mail')->value;
  }
}