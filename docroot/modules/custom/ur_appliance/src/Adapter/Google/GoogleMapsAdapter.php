<?php
/**
 * Created by PhpStorm.
 * User: patrickthurmond
 * Date: 1/27/17
 * Time: 4:58 PM
 */

namespace Drupal\ur_appliance\Adapter\Google;

use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Drupal\ur_appliance\Adapter\BaseAdapter;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;

/**
 * Class GoogleMapsAdapter
 *
 * @package Drupal\ur_appliance\Adapter\Google
 */
class GoogleMapsAdapter extends BaseAdapter {
  protected $clientId;
  protected $serverKey;
  protected $channel;

  /**
   * @return null|string
   * @throws InvalidArgumentException
   * @throws \Exception
   */
  public function getBaseUrl() {
    if (empty($this->baseUrl)) {
      /**
       * @TODO: Make this contain a code fallback for each environment.
       */
      $this->baseUrl = ApiContainer::getParameter('Google.base_url');
    }

    return $this->baseUrl;
  }

  /**
   * Should we even log the raw data?
   *
   * @return null|mixed
   * @throws InvalidArgumentException
   * @throws \Exception
   */
  public function shouldLogRaw() {
    return ApiContainer::getParameter('Google.log_raw');
  }

  /**
   * Get the App ID
   *
   * @return mixed
   * @throws \Exception
   */
  public function getClientId() {
    if (empty($this->clientId)) {
      $this->clientId = ApiContainer::getParameter('Google.client_id');
    }

    return $this->clientId;
  }

  /**
   * Get the App Key
   *
   * @return mixed
   * @throws \Exception
   */
  public function getServerKey() {
    if (empty($this->serverKey)) {
      $this->serverKey = ApiContainer::getParameter('Google.server_key');
    }

    return $this->serverKey;
  }

  public function getChannel() {
    if (empty($this->channel)) {
      $this->channel = ApiContainer::getParameter('Google.channel');
    }

    return $this->channel;
  }

  /**
   * Make sure that we add the App ID and Key to every call.
   *
   * @return array|mixed|null|\stdClass
   * @throws \Exception|GuzzleException
   */
  public function execute() {
    $this->addParameter('key', $this->getServerKey());

    return parent::execute();
  }

}
