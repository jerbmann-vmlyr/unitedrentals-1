<?php
/**
 * Created by PhpStorm.
 * User: patrickthurmond
 * Date: 1/27/17
 * Time: 4:58 PM
 */

namespace Drupal\ur_appliance\Adapter\DAL;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Drupal\ur_appliance\Adapter\BaseAdapter;
use Drupal\ur_appliance\Controller\DAL\Service\SsoController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Customer\AdAccountsEndpoint;
use Drupal\ur_appliance\Exception\AdapterException;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\EndpointException;
use Psr\Log\LoggerInterface;

/**
 * Class DalAdapter
 *
 * @package Drupal\ur_appliance\Adapter\DAL
 */
class DalAdapter extends BaseAdapter {
  // DAL Specific settings
  static protected $errorStringsToIgnore = [
    'Login failed',
    'No Records Found',
    "No \nRecords\nFound"
  ];

  protected $excludedURIs = [
    'customer/creditcards'
  ];

  // Variables that are changed by inheriting classes
  protected $forceUserToken = false;
  protected $forceWORSToken = false;
  protected $preferUserToken = false;
  protected $guidRequired = false;

  protected $userToken;

  /**
   * The resolved object instances.
   *
   * @var array
   */
  protected static $resolvedInstance;
  protected static $resolvedSsoInstance;

  public function __construct(LoggerInterface $logger, ClientInterface $client) {
    parent::__construct($logger, $client);
    $this->setDefaultSuccessStatus('0'); // In DAL Docs 0 = Success
  }

  /**
   * Resolve the root instance from the container.
   *
   * This should help prevent creation of an infinite loop recursion issue.
   *
   * @param string|\stdClass $name - The fully qualified class name.
   * @param mixed $dep1 - First dependency
   * @param mixed $dep2 - Second dependency
   *
   * @return mixed
   */
  protected static function resolveInstance($name, $dep1 = NULL, $dep2 = NULL) {
    if (is_object($name)) {
      return $name;
    }

    if (isset(static::$resolvedInstance[$name])) {
      return static::$resolvedInstance[$name];
    }

    $obj = NULL;

    // There is probably a better way to dynamically handle dependency injection
    if ($dep2 !== NULL) {
      $obj = new $name($dep1, $dep2);
    }
    else {
      $obj = new $name($dep1);
    }

    return static::$resolvedInstance[$name] = $obj;
  }

  /**
   * Return a separate static instance of this class.
   *
   * @return DalAdapter - New instance of THIS class. Only instantiated once.
   * @throws \Exception
   */
  public function getSeparateDalAdapterInstance() {
    $logger = ApiContainer::getService('logger.channel.php');
    $client = ApiContainer::getService('http_client');

    /** @var DalAdapter $adapter */
    return self::resolveInstance(self::class, $logger, $client);
  }

  /**
   * Return a separate static instance of the SsoController class.
   *
   * @return SsoController - New instance of SsoController class. Only instantiated once.
   * @throws \Exception
   */
  public function getSsoController() {
    /** @var DalAdapter $adapter */
    $adapter = $this->getSeparateDalAdapterInstance();

    /**
     * Grab only one new instance of the Sso Controller
     *
     * @var SsoController $ssoController
     */
    return self::resolveInstance(SsoController::class, false, $adapter);
  }

  /**
   * Gets the user token. Used for various, user-specific calls.
   *
   * @param string|null $email - Option to force it to use an email.
   * @return bool|mixed|null
   */
  public function getUserToken($email = NULL) {
    if (!empty($_SESSION['ur_dal_user_token'])) {
      return $_SESSION['ur_dal_user_token'];
    }

    if ($email === NULL) {
      $email = $this->getUserEmail();
    }

    if (!empty($email)) {
      try {
        $token = $this->getSsoWithSeparateAdapter($email);
      }
      catch (\Exception $e) {
        return false;
      }

      if (!empty($token)) {
        // Always save the token.
        $this->setUserToken($token);

        // Ensure that it always sets this to the last email used.
        $this->setUserEmail($email);

        return $token;
      }
    }

    return false;
  }

  /**
   * We need to avoid weird issues caused by re-using the instance of
   * the Dal Adapter each time. So, to do that, we need to create a new
   * instance, for a moment, of the current class.
   *
   * @param $email
   * @return mixed
   *
   * @throws AdapterException|\Exception|InvalidArgumentException
   * @throws ServiceCircularReferenceException|ServiceNotFoundException
   * @throws DalException|EndpointException
   */
  protected function getSsoWithSeparateAdapter($email) {
    /** @var SsoController $ssoController */
    $ssoController = $this->getSsoController();
    $result = $ssoController->read($email);

    if (is_array($result) && !empty($result['token'])) {
      $token = $result['token'];
    }
    else {
      throw new AdapterException('Unable to get the SSO token.');
    }

    return $token;
  }

  /**
   * Sets the user token. Used in all sorts of calls.
   *
   * @param $token
   */
  public function setUserToken($token) {
    $this->userToken = $token;
    $_SESSION['ur_dal_user_token'] = $token;
  }

  /**
   * Under certain circumstances we need to wipe out the existing token.
   *
   * This will un-set the session variable and the userToken property.
   */
  public function unsetUserToken() {
      unset($_SESSION['ur_dal_user_token']);
      $this->userToken = NULL;
  }

  /**
   * Gets the DAL user email address from the session so that we can try to get
   * the token for the user.
   *
   * @return null
   */
  public function getUserEmail() {
    if (!empty($_SESSION['ur_dal_user_email'])) {
      return $_SESSION['ur_dal_user_email'];
    }

    try {
      if (\Drupal::currentUser()->isAuthenticated()) {
        $email = \Drupal::currentUser()->getEmail();
        $this->setUserEmail($email);

        return $email;
      }
    }
    catch (ServiceNotFoundException $e) {
      // Do nothing for now. We simply do not have an email address available.
    }

    return null;
  }

  /**
   * Sets the DAL user email address in the session so that we can use it to
   * load the token.
   *
   * @param $email
   */
  public function setUserEmail($email) {
    $_SESSION['ur_dal_user_email'] = $email;
  }

  /**
   * Wipes out the session variable storing the user's email and token.
   *
   * In certain circumstances we need to use a different email versus the one on
   * the drupal user. This also calls on unsetUserToken() to remove any tokens
   * that may be set for this email as well.
   */
  public function unsetUserEmail() {
    unset($_SESSION['ur_dal_user_email']);
    $this->unsetUserToken();
  }

  /**
   * Gets the call authorization.
   *
   * @return mixed|null|string
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
   */
  public function getAuth() {
    if (($this->shouldUseUserToken() || $this->prefersUserToken())
        && !$this->shouldUseWORSToken()) {
      $userToken = $this->getUserToken();

      // On the prefers user token, we don't want to throw an exception.
      if (empty($userToken) && !$this->prefersUserToken()) {
        throw new DalException('User token is required, but not available.');
      }

      if (!empty($userToken)) {
        return "TOKEN:$userToken";
      }
    }

    // Prefers is a catchall the prioritizes the user token over the WORS token.
    if ($this->shouldUseWORSToken() || $this->prefersUserToken()) {
      return ApiContainer::getParameter('auth_common');
    }

    // Ok, last ditch effort. If we cannot get a user token then we are hosed
    $userToken = $this->getUserToken();

    if (!empty($userToken)) {
      return "TOKEN:$userToken";
    }

    throw new DalException('Unable to find the appropriate token for this call.');
  }

  /**
   * Reset all of the force token settings. These should be set with the
   * pre-execute method in the endpoints.
   */
  public function postExecuteCleanup() {
    parent::postExecuteCleanup();

    $this->tokenCleanup();
  }

  public function tokenCleanup() {
    $this->shouldNotForceWORSToken();
    $this->shouldNotForceUserToken();
    $this->shouldNotPreferUserToken();
  }

  /**
   * Forces the use of the user token.
   */
  public function shouldForceUserToken() {
    $this->forceUserToken = true;
  }

  /**
   * Makes sure the user token is not forced.
   */
  public function shouldNotForceUserToken() {
    $this->forceUserToken = false;
  }

  /**
   * Should we be forced to use the user token.
   *
   * @return bool
   */
  public function shouldUseUserToken() {
    return $this->forceUserToken;
  }

  /**
   * Set the force WORS token param to true.
   */
  public function shouldForceWORSToken() {
    $this->forceWORSToken = true;
  }

  /**
   * Set the force WORS token param to false.
   */
  public function shouldNotForceWORSToken() {
    $this->forceWORSToken = false;
  }

  /**
   * @return bool
   */
  public function shouldUseWORSToken() {
    return $this->forceWORSToken;
  }

  /**
   * This call would prefer to use the user token, otherwise use WORS token.
   */
  public function shouldPreferUserToken() {
    $this->preferUserToken = true;
  }

  /**
   * We do not prefer the user token. (Only used for resetting this variable)
   */
  public function shouldNotPreferUserToken() {
    $this->preferUserToken = false;
  }

  /**
   * Does this call prefer to use the User Token, but can use the WORS Token?
   *
   * @return bool
   */
  public function prefersUserToken() {
    return $this->preferUserToken;
  }

  /**
   * @return bool
   */
  public function shouldHaveGuid() {
    return $this->guidRequired;
  }

  /**
   * Sets the Guid to be required
   */
  public function requireGuid() {
    $this->guidRequired = true;
  }

  /**
   * Sets the guid to not be required
   */
  public function doNotRequireGuid() {
    $this->guidRequired = false;
  }

  /**
   * Gets the user's GUID.
   *
   * @return bool|string
   */
  public function getGuid() {
    if (!empty($_SESSION['ur_guid'])) {
      return $_SESSION['ur_guid'];
    }

    try {
      $adAccountObj = new AdAccountsEndpoint($this);
      $guid = $adAccountObj->readGuid();
      $_SESSION['ur_guid'] = $guid;

      return $guid;
    }
    catch (DalException $e) {
      // Do nothing
    }

    return false;
  }

  /**
   * @return null|string
   * @throws InvalidArgumentException
   * @throws \Exception
   */
  public function getBaseUrl() {
    if (empty($this->baseUrl)) {
      $this->baseUrl = ApiContainer::getParameter('base_url');
    }

    return $this->baseUrl;
  }

  /**
   * Should we even log the raw data?
   *
   * @return null|mixed
   * @throws InvalidArgumentException
   * @throws \Exception
   */
  public function shouldLogRaw() {
    return ApiContainer::getParameter('log_raw');
  }

  /**
   * Gets the response status.
   *
   * @return null|mixed
   * @throws \RuntimeException
   */
  public function getStatus() {
    if (isset($this->getParsedResponse()->status)) {
      return $this->getParsedResponse()->status;
    }

    return null;
  }

  /**
   * Gets the response message text.
   *
   * @return null
   * @throws \RuntimeException
   */
  public function getMessageText() {
    if (isset($this->getParsedResponse()->messageText)) {
      return $this->getParsedResponse()->messageText;
    }

    return null;
  }

  /**
   * Interfere with parent execute operations as needed.
   *
   * @return array|mixed|null|\stdClass
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \RuntimeException
   * @throws DalException|AdapterException
   */
  public function execute() {
    try {
      return parent::execute();
    }
    catch (ClientException $e) {
      $options = $this->buildRequestOptions();
      $method = $this->getMethod();
      $url = $this->buildUrl();
      $msg = $e->getMessage();

      if (stripos($msg, 'Access not authorized.')) {
        $uri = $this->getUri();
        $rawLog = $this->getRawLog();

        $logMsg = "DAL authorization failure for URI: '$uri'\n";
        $logMsg .= "Full URL: $url\n";
        $logMsg .= "Using method: $method\n";
        $logMsg .= "With options:\n" . print_r($options, true) . "\n";
        $logMsg .= "Details:\n" . print_r($rawLog, true) . "\n";

        // @TODO: Re-enable logging at some point down the road.
        // $this->getLogger()->error($logMsg);

        $this->parsedResponse = (object) [
          'status' => 0,
          'message' => $msg
        ];

        $this->setExecuteError('authorization_error');
        $this->postExecuteCleanup();

        return $this->getParsedResponse();
      }

      throw new AdapterException("Call failed with error: $msg");
    }
  }

  /**
   * Should be overridden in extending class.
   *
   * @return array
   */
  public function getExcludedURIs() {
    return $this->excludedURIs;
  }
}