<?php
/**
 * Created by PhpStorm.
 * User: patrickthurmond
 * Date: 1/27/17
 * Time: 4:58 PM
 */

namespace Drupal\ur_appliance\Adapter;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Drupal\ur_appliance\Exception\AdapterException;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\ResponseParseException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class DalAdapter
 *
 * @package Drupal\ur_appliance\Adapter
 */
class BaseAdapter implements AdapterInterface {
  protected $curlOptions = [];
  protected $baseUrl = '';
  protected $method = 'GET';
  protected static $allowedMethods = ['GET', 'POST', 'PUT'];
  protected $headers = [];
  protected $contentType = 'application/json';

  protected $cacheResult = false;
  protected $cacheToUser = false;
  protected $cacheData = [];
  protected $cacheTTL = 21600;

  // DAL Specific settings
  static protected $errorStringsToIgnore = [];

  // Call and Response specific
  /** @var ResponseInterface - The response object */
  protected $response;

  /** @var bool - Should we process the response as XML */
  protected $shouldProcessAsXML = false;

  /** @var array|\stdClass - The parsed response */
  protected $parsedResponse;

  protected $errorNotification = false;
  protected $shouldLogSuccess = false;
  protected $shouldLogRawData = false;
  protected $defaultSuccessStatus = 1; // What is standard in MOST APIs

  /**
   * Holds one or more endpoints.
   *
   * @var string
   */
  protected $uri = '';

  /** @var string */
  protected $requestBody = '';

  /** @var null|array */
  protected $requestJsonParams;

  /** @var array|null */
  protected $parameters;

  /** @var string */
  protected $executeError;

  /** @var LoggerInterface */
  protected $logger;

  /** @var ClientInterface */
  protected $client;

  /** @var RequestInterface */
  protected $request;

  /**
   * Adapter constructor.
   *
   * @TODO: May instead do what Laravel does and look for the static instances
   *        of the logger and cache tool. Then we would not have to inject them
   *        here to use.
   *
   * @param LoggerInterface $logger
   * @param ClientInterface $client
   */
  public function __construct(LoggerInterface $logger, ClientInterface $client) {
    $this->setLogger($logger);
    $this->setClient($client);
  }

  /**
   * @return LoggerInterface
   */
  public function getLogger() {
    return $this->logger;
  }

  /**
   * @param LoggerInterface $logger
   */
  public function setLogger(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * @return ClientInterface
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * @param \GuzzleHttp\ClientInterface $client
   */
  public function setClient(ClientInterface $client) {
    $this->client = $client;
  }

  /**
   * Gets the call authorization.
   * Default is null until we extend the functionality.
   *
   * @see Extend in child class
   *
   * @return null|string
   * @throws DalException
   */
  public function getAuth() {
    return null;
  }

  /**
   * Enables error notifications.
   */
  public function enableErrorNotifications() {
    $this->errorNotification = true;
  }

  /**
   * Disables error notifications
   */
  public function disableErrorNotifications() {
    $this->errorNotification = false;
  }

  /**
   * Get the error notification status.
   */
  public function areErrorNotificationsEnabled() {
    return $this->errorNotification;
  }

  /**
   * @param bool $val
   */
  public function setLogSuccess($val = false) {
    if (is_bool($val)) {
      $this->shouldLogSuccess = $val;
    }
  }

  /**
   * @return bool
   */
  public function getLogSuccess() {
    return $this->shouldLogSuccess;
  }

  /**
   * Sets the identifier used to indicate successful calls.
   *
   * @param mixed $status
   */
  public function setDefaultSuccessStatus($status) {
    $this->defaultSuccessStatus = $status;
  }

  /**
   * @return int
   */
  public function getDefaultSuccessStatus() {
    return $this->defaultSuccessStatus;
  }

  /**
   * @return string
   */
  public function getRequestBody() {
    return $this->requestBody;
  }

  /**
   * @param $body
   */
  public function setRequestBody($body) {
    $this->requestBody = $body;
  }

  /**
   * @return array|null
   */
  public function getRequestJsonParams() {
    return $this->requestJsonParams;
  }

  /**
   * @param array|null $requestJsonParams
   */
  public function setRequestJsonParams($requestJsonParams) {
    $this->requestJsonParams = $requestJsonParams;
  }

  /**
   * @return string
   */
  public function getContentType() {
    return $this->contentType;
  }

  /**
   * @TODO: Add more checking of the content type later.
   *
   * @param string $type
   */
  public function setContentType($type) {
    $this->contentType = $type;
  }

  /**
   * @param $key
   * @param $value
   */
  public function addHeader($key, $value) {
    if (!in_array($key, $this->headers, false)) {
      $this->headers[$key] = $value;
    }
  }

  /**
   * @return array
   */
  public function getHeaders() {
    return $this->headers;
  }

  /**
   * @return null|string
   */
  public function getBaseUrl() {
    return $this->baseUrl;
  }

  /**
   * @param string $url
   */
  public function setBaseUrl($url) {
    if (is_string($url)) {
      $this->baseUrl = $url;
    }
  }

  /**
   * Gets the specified endpoint. Defaults to index 0.
   *
   * @return mixed
   *
   * @throws AdapterException
   */
  public function getUri() {
    if (!empty($this->uri)) {
      return $this->uri;
    }

    throw new AdapterException('No endpoint defined.');
  }

  /**
   * Allows the URI to be set here.
   *
   * @param $uri
   */
  public function setUri($uri) {
    if (!empty($uri)) {
      $this->uri = $uri;
    }
  }

  /**
   * @return string
   */
  public function getMethod() {
    return $this->method;
  }

  /**
   * @param string $method
   * @throws DalException
   */
  public function setMethod($method = 'GET') {
    $method = strtoupper($method);

    if (!in_array($method, self::$allowedMethods, false)) {
      throw new DalException("Method: $method is not an allowed method.");
    }

    $this->method = $method;
  }

  /**
   * Reset the method value to the normal default.
   */
  public function resetMethod() {
    // Resets the method to GET if it is not in the default method mode
    if ($this->method != 'GET') {
      $this->method = 'GET';
    }
  }

  /**
   * @return array
   */
  public function getParams() {
    return $this->parameters;
  }

  /**
   * Bulk set the parameters.
   *
   * @param array $params
   */
  public function setParams(array $params = []) {
    $this->parameters = $params;
  }

  /**
   * Adds parameters to get sent along.
   *
   * @param $field - The parameter field name.
   * @param $value - The parameter value.
   */
  public function addParameter($field, $value) {
    $this->parameters[$field] = $value;
  }

  /**
   * Gets all the parameters, flattens them, and then returns the result.
   *
   * @return array
   */
  public function getFlattenedParams() {
    return $this->flattenParams( $this->getParams() );
  }

  /**
   * Flattens a given array through recursive actions.
   *
   * @param array $array
   * @param string $prefix
   * @return array
   */
  public function flattenParams($array, $prefix = '') {
    $result = [];

    if (is_array($array)) {
      foreach($array as $key => $value) {
        $key = $prefix ? $prefix . '[' . $key . ']' : $key;

        if (is_array($value)) {
          $result += $this->flattenParams($value, $key);
        }
        else {
          $result[$key] = $value;
        }
      }
    }

    return $result;
  }

  /**
   * @return string
   */
  public function getQueryStrings() {
    if (!empty($this->getParams())) {
      return http_build_query( $this->getFlattenedParams() );
    }

    return '';
  }

  /**
   * Builds the options for a standard PSR-7 request.
   *
   * @return array
   * @throws DalException
   */
  public function buildRequestOptions() {
    $auth = $this->getAuth();
    $requestBody = $this->getRequestBody();
    $requestJsonParams = $this->getRequestJsonParams();

    if (!empty($auth)) {
      $this->addHeader('Authorization', $auth);
    }

    $this->addHeader('Content-Type', $this->getContentType());

    /**
     * Options that are passed into the request object. They are also forwarded
     * to the handler if it is set.
     *
     * Possible options are:
     *  - headers: Any headers we are sending.
     *  - body: The request body.
     *  - version: The Protocol version number (ex. 1.1).
     *  - base_uri - The base part of the URI (probably won't use).
     *  - save_to: ?
     *  - sink: ?
     *  - exceptions: ?
     *  - http_errors: ?
     *  - form_params: ?
     *  - multipart: ?
     *  - _conditional: ?
     *      * Content-Type: Obviously the content type.
     *  - json: Data to json encode and use as the body. Replaces the body option.
     *  - decode_content: Sets the Accept-Encoding header.
     *  - auth: This is an array. Defaults to basic encoding which uses [0]:[1]
     *          index to output. Also known as ['user', 'pass'] for those
     *          positions. Index [2] sets auth type. Second option is "digest".
     *  - query: The query string we are sending. Send as an array of values.
     */
    $options = ['headers' => $this->getHeaders()];

    if (!empty($requestJsonParams)) {
      $options['json'] = $requestJsonParams;
    }
    else {
      $options['body'] = $requestBody;
    }

    return $options;
  }

  /**
   * Builds the URL to contact. Includes the query string.
   *
   * @return string
   * @throws AdapterException
   */
  public function buildUrl() {
    $queryString = $this->getQueryStrings();
    $baseUrl = $this->getBaseUrl();
    $uri = $this->getUri();

    return sprintf('%s/%s?%s', $baseUrl, $uri, $queryString);
  }

  /**
   * Executes the current HTTP request.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \RuntimeException
   * @throws AdapterException
   * @throws DalException
   */
  public function execute() {
    $method = $this->getMethod();
    $url = $this->buildUrl();
    $options = $this->buildRequestOptions();

    /*
     * For UR-468 we're running into a timeout after 30 seconds on some calls.
     * Following Guzzle's docs:
     * http://docs.guzzlephp.org/en/stable/request-options.html#timeout
     *
     * We should be able to set the timeout to whatever we like. Setting to 2
     * mins (120 seconds) for now.
     */
    $options['timeout'] = 120;

    /** @var ResponseInterface $response */
    $response = $this->getClient()->request($method, $url, $options);

    $this->setResponse($response); // Save the response

    // Valid HTTP response codes
    $validStatusCodes = [200, 201, 202, 203, 204, 205, 206, 207, 208];

    if (in_array($response->getStatusCode(), $validStatusCodes, false)) {
      $this->parseResponse();

      if ($this->hasStatusError()) {
        $this->setExecuteError('status_error');
      }

      // @TODO: Re-enable logging at some point in the future
      // $this->logResponse();
    }
    else {
      // @TODO: Re-enable logging at some point in the future
      // $this->logUnknownError();

      // @TODO: Can remove when logging is re-enabled
      $this->setExecuteError('unknown_error');
    }

    if ($response->getStatusCode() >= 500) {
      $this->setExecuteError('server_error');
    }

    $this->postExecuteCleanup();

    return $this->getParsedResponse();
  }

  /**
   * Run through any post-execute cleanup.
   */
  public function postExecuteCleanup() {
    $this->resetMethod();
    $this->resetShouldProcessAsXML();
  }

  /**
   * If we need to do any specific token cleanup per adapter, change them in
   * the inherited class
   */
  public function tokenCleanup() {
    // @TODO: Implement this in child adapters as needed.
  }

  /**
   * Sets the shouldProcessAsXML variable to "true".
   */
  public function processAsXML() {
    $this->shouldProcessAsXML = true;
  }

  /**
   * Resets the shouldProcessAsXML variable to "false".
   */
  public function resetShouldProcessAsXML() {
    $this->shouldProcessAsXML = false;
  }

  /**
   * Parse the response that was recorded.
   *
   * @return array|mixed|\stdClass
   * @throws \RuntimeException
   */
  public function parseResponse() {
    if ($this->shouldProcessAsXML) {
      $this->parsedResponse = $this->parseXML( $this->getResponseBody() );
    }
    else {
      $this->parsedResponse = $this->parseJSON( $this->getResponseBody() );
    }

    return $this->parsedResponse;
  }

  /**
   * Parses an XML response into something usable.
   *
   * @param $xmlString
   * @return mixed
   *
   * @throws ResponseParseException
   */
  public function parseXML($xmlString) {
    $xmlString = str_replace('&', '&amp;', $xmlString);
    $simpleXML = @simplexml_load_string( $xmlString );
    $response = '';

    if ($simpleXML === false) {
      throw new ResponseParseException('XML Parse Failure');
    }

    try {
      // Clean the XML Object and turn it into a standard object
      $convertedToJSON = \GuzzleHttp\json_encode($simpleXML);
      $response = \GuzzleHttp\json_decode($convertedToJSON);
    }
    catch (\InvalidArgumentException $e) {
      throw new ResponseParseException('XML Conversion Failure');
    }

    return $this->processXmlAttributes($response);
  }

  /**
   * Takes a SimpleXML response object and processes the attributes, turning
   * them into fields instead. This prevents weirdness when importing the data.
   *
   * This is a recursive function. So beware.
   *
   * @param mixed $entity
   * @return mixed
   */
  public function processXmlAttributes($entity) {
    $entityWasObject = false;

    if (is_object($entity)) {
      $entity = (array) $entity;
      $entityWasObject = true;
    }

    if (is_array($entity)) {
      $hasAttributes = false;
      $attributes = [];

      /**
       * Had to add this because there was some unexpected data bleed over that
       * I was unable to trace to the source. It caused attributes to appear
       * where they should not. This allows me to test before the loop starts
       * and prevent the attributes from being built or tacked on if they
       * somehow bleed back in.
       */
      if (!empty($entity['@attributes'])) {
        $hasAttributes = true;
      }

      foreach ($entity AS $index => $value) {
        $wasObject = false;

        if (is_object($value)) {
          $value = (array) $value;
          $wasObject = true;
        }

        if ($hasAttributes && $index == '@attributes') {
          foreach ($value AS $childIndex => $childValue) {
            $attributes[$childIndex] = $childValue;
          }

          unset($entity[$index], $childIndex, $childValue);
        }
        // Make sure we are not looking at an int or string or other such thing
        else if (is_array($value)) {
          $result = $this->processXmlAttributes($value);

          if ($wasObject) {
            $result = (object) $result;
          }

          // Replace the index value with the processed values.
          $entity[$index] = $result;
        }
      }

      // Make sure we don't add to the array until it is done with the loop
      if ($hasAttributes) {
        $entity = array_merge($entity, $attributes);
      }
    }

    if ($entityWasObject) {
      return (object) $entity;
    }

    return $entity;
  }

  /**
   * Parse a JSON response into something usable.
   *
   * @param $jsonString
   * @return mixed
   *
   * @throws ResponseParseException
   */
  public function parseJSON($jsonString) {
    try {
      $response = \GuzzleHttp\json_decode( $jsonString );
    }
    catch (\InvalidArgumentException $e) {
      throw new ResponseParseException('JSON Parse Failure');
    }

    return $response;
  }

  /**
   * Should we even log the raw data?
   *
   * @return bool|null
   */
  public function shouldLogRaw() {
    return $this->shouldLogRawData;
  }

  /**
   * Set whether or not we should log the raw data.
   *
   * @param $value
   */
  public function setShouldLogRawData($value) {
    if (is_bool($value)) {
      $this->shouldLogRawData = (bool) $value;
    }
  }

  /**
   * Determines if we should build a raw log and returns either the raw part of
   * the log or an empty array.
   *
   * @return array
   * @throws \RuntimeException
   */
  public function getRawLog() {
    $vars = [];

    if ($this->shouldLogRaw()) {
      $vars = [
        'raw' => $this->getResponseBody(),
        'url' => $this->buildUrl(),
        'method' => $this->getMethod(),
        'options' => $this->buildRequestOptions(),
      ];
    }

    return $vars;
  }

  /**
   * Determines if we should continue with logging or not.
   *
   * @return bool
   * @throws \Drupal\ur_appliance\Exception\AdapterException
   */
  public function shouldSkipLogging() {
    $uri = $this->getUri();

    if (in_array($uri, $this->getExcludedURIs(), false)) {
      return true; // Some things we don't want to log
    }

    return false;
  }

  /**
   * Logs unknown errors.
   *
   * @throws \Drupal\ur_appliance\Exception\AdapterException|\RuntimeException
   */
  public function logUnknownError() {
    $uri = $this->getUri();
    $rawLog = $this->getRawLog();

    /**
     * @TODO: Change this in the future to exclude parameters instead of
     *        returning.
     */
    if ($this->shouldSkipLogging()) {
      return; // Some things we don't want to log
    }

    // @TODO: Re-enable logging at some point down the road.
    /*$this->getLogger()->error(
      "Endpoint $uri failed. Details: " . print_r($rawLog, true)
    );*/

    $this->setExecuteError('unknown_error');
  }

  /**
   * Logs the responses.
   *
   * @throws DalException|\Drupal\ur_appliance\Exception\AdapterException
   */
  public function logResponse() {
    if (( $this->shouldLogSuccess && $this->wasSuccessful() )
        || $this->hasStatusError()) {
      $status = $this->getStatus();
      $message = $this->getMessageText();
      $uri = $this->getUri();

      /**
       * @TODO: Change this in the future to exclude parameters instead of
       *        returning.
       */
      if ($this->shouldSkipLogging()) {
        return; // Some things we don't want to log
      }

      $status = ($status != null) ? $status : '(not set)';
      $message = ($message != null) ? $message : '(not set)';
      $requestBody = serialize($this->getRequestBody());

      $msg = "Endpoint $uri status: $status, message: $message, body: $requestBody";

      // @TODO: Re-enable logging at some point down the road.
      if ($this->hasStatusError()) {
        // $this->getLogger()->error($msg);
      }
      else {
        // $this->getLogger()->info($msg);
      }
    }
  }

  /**
   * Does this have an error message we can ignore?
   *
   * @return bool
   */
  public function hasIgnorableError() {
    return !empty($this->getMessageText())
      && in_array($this->getMessageText(), self::$errorStringsToIgnore, false);
  }

  /**
   * Does this have a status error?
   *
   * @return bool
   */
  public function hasStatusError() {
    return $this->getStatus() == null
      || (!$this->wasSuccessful() && !$this->hasIgnorableError());
  }

  /**
   * Was this call successful?
   *
   * @return bool
   */
  public function wasSuccessful() {
    return (string) $this->getStatus() === (string) $this->defaultSuccessStatus;
  }

  /**
   * Gets the response string, if available.
   *
   * @return ResponseInterface
   */
  public function getResponse() {
    return $this->response;
  }

  /**
   * @param ResponseInterface $response
   */
  public function setResponse(ResponseInterface $response) {
    $this->response = $response;
  }

  /**
   * @return string - The body of the response.
   * @throws \RuntimeException
   */
  public function getResponseBody() {
    if (is_a($this->getResponse(), ResponseInterface::class)) {
      return $this->getResponse()->getBody()->getContents();
    }

    return '';
  }

  /**
   * Loads the fully parsed response if possible.
   *
   * @return array|mixed|null|\stdClass
   * @throws \RuntimeException
   */
  public function getParsedResponse() {
    if ($this->parsedResponse != null) {
      return $this->parsedResponse;
    }

    if (!empty($this->getResponseBody())) {
      return $this->parseResponse();
    }

    return null;
  }

  /**
   * Gets the HTTP Code from the cURL response.
   *
   * @return int|null
   */
  public function getResponseHttpCode() {
    return $this->getResponse()->getStatusCode();
  }

  /**
   * Gets the response status.
   * @see: Must be made specific by the extending class.
   *
   * @return null
   */
  public function getStatus() {
    return null;
  }

  /**
   * Gets the response message text.
   * @see: Must be made specific by the extending class.
   *
   * @return null
   */
  public function getMessageText() {
    return null;
  }

  /**
   * Sets any errors that occur doing call execution.
   *
   * @param $str
   */
  public function setExecuteError($str) {
    $this->executeError = $str;
  }

  /**
   * Returns any call execution errors.
   *
   * @return string
   */
  public function getExecuteError() {
    return $this->executeError;
  }

  /**
   * Send out any notifications that need to happen.
   * Below is the sample wrapper for taking action.
   */
  public function sendNotifications() {
    if ( !empty( $this->getExecuteError() ) ) {
      //@TODO: Override in extending class
    }
  }

  /**
   * Should be overridden in extending class.
   *
   * @return array
   */
  public function getExcludedURIs() {
    return [];
  }
}