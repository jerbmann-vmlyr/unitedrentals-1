<?php
/**
 * Created by PhpStorm.
 * User: patrickthurmond
 * Date: 1/27/17
 * Time: 4:58 PM
 */

namespace Drupal\ur_appliance\Adapter\Placeable;

use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Drupal\ur_appliance\Adapter\BaseAdapter;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Psr\Log\LoggerInterface;

/**
 * Class WorkbenchAdapter
 *
 * @package Drupal\ur_appliance\Adapter\Placeable
 */
class WorkbenchAdapter extends BaseAdapter {
  /**
   * WorkbenchAdapter constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   * @param \GuzzleHttp\ClientInterface $client
   */
  public function __construct(LoggerInterface $logger, ClientInterface $client) {
    parent::__construct($logger, $client);
  }

  /**
   * @return null|string
   * @throws InvalidArgumentException
   * @throws \Exception
   */
  public function getBaseUrl() {
    if (empty($this->baseUrl)) {
      /**
       * @TODO: Make this contain a code fallback for each environment.
       */
      $this->baseUrl = ApiContainer::getParameter('Placeable.workbench_url');
    }

    return $this->baseUrl;
  }

  /**
   * Should we even log the raw data?
   *
   * @return null|mixed
   * @throws InvalidArgumentException
   * @throws \Exception
   */
  public function shouldLogRaw() {
    return ApiContainer::getParameter('Placeable.log_raw');
  }

  /**
   * Get the App ID
   *
   * @return mixed
   * @throws \Exception
   */
  public function getAppId() {
    if (empty($this->appId)) {
      $this->appId = ApiContainer::getParameter(
        'Placeable.workbench_app_id'
      );
    }

    return $this->appId;
  }

  /**
   * Get the App Key
   *
   * @return mixed
   * @throws \Exception
   */
  public function getAppKey() {
    if (empty($this->appKey)) {
      $this->appKey = ApiContainer::getParameter(
        'Placeable.workbench_app_key'
      );
    }

    return $this->appKey;
  }

  /**
   * Had to override this to allow for using the appId and appKey in the query
   * string while doing a POST method.
   *
   * @return string
   */
  public function getQueryStrings() {
    return http_build_query( $this->getFlattenedParams() );
  }

  /**
   * Make sure that we add the X-UserEmail header to every call.
   *
   * @return array|mixed|null|\stdClass
   * @throws \Exception
   */
  public function execute() {
    $this->addHeader('X-UserEmail', 'admin@Drupal\ur_appliance.com');
    $this->addParameter('appId', $this->getAppId());
    $this->addParameter('appKey', $this->getAppKey());


    // Parent will take care of adding the app ID and key
    return parent::execute();
  }
}