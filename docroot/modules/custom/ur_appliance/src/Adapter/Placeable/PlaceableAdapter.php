<?php
/**
 * Created by PhpStorm.
 * User: patrickthurmond
 * Date: 1/27/17
 * Time: 4:58 PM
 */

namespace Drupal\ur_appliance\Adapter\Placeable;

use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Drupal\ur_appliance\Adapter\BaseAdapter;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Psr\Log\LoggerInterface;

/**
 * Class PlaceableAdapter
 *
 * @package Drupal\ur_appliance\Adapter\Placeable
 */
class PlaceableAdapter extends BaseAdapter {
  protected $appId;
  protected $appKey;

  /**
   * PlaceableAdapter constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   * @param \GuzzleHttp\ClientInterface $client
   */
  public function __construct(LoggerInterface $logger, ClientInterface $client) {
    parent::__construct($logger, $client);
  }

  /**
   * @return null|string
   * @throws InvalidArgumentException
   * @throws \Exception
   */
  public function getBaseUrl() {
    if (empty($this->baseUrl)) {
      /**
       * @TODO: Make this contain a code fallback for each environment.
       */
      $this->baseUrl = ApiContainer::getParameter('Placeable.base_url');
    }

    return $this->baseUrl;
  }

  /**
   * Should we even log the raw data?
   *
   * @return null|mixed
   * @throws InvalidArgumentException
   * @throws \Exception
   */
  public function shouldLogRaw() {
    return ApiContainer::getParameter('Placeable.log_raw');
  }

  /**
   * Get the App ID
   *
   * @return mixed
   * @throws \Exception
   */
  public function getAppId() {
    if (empty($this->appId)) {
      $this->appId = ApiContainer::getParameter('Placeable.app_id');
    }

    return $this->appId;
  }

  /**
   * Get the App Key
   *
   * @return mixed
   * @throws \Exception
   */
  public function getAppKey() {
    if (empty($this->appKey)) {
      $this->appKey = ApiContainer::getParameter('Placeable.app_key');
    }

    return $this->appKey;
  }

  /**
   * Had to override this to allow for using the appId and appKey in the query
   * string while doing a POST method.
   *
   * @return string
   */
  public function getQueryStrings() {
    return http_build_query( $this->getFlattenedParams() );
  }

  /**
   * Make sure that we add the App ID and Key to every call.
   *
   * @return array|mixed|null|\stdClass
   * @throws \Exception
   */
  public function execute() {
    $this->addParameter('app_id', $this->getAppId());
    $this->addParameter('app_key', $this->getAppKey());

    return parent::execute();
  }
}