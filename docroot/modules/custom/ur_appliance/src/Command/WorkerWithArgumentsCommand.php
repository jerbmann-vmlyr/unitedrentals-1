<?php

namespace Drupal\ur_appliance\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class WorkerWithArgumentsCommand
 * @package Drupal\ur_appliance\Command
 *
 * @TODO: This is a sample command and not fully fleshed out.
 */
class WorkerWithArgumentsCommand extends Command {
  protected function configure() {
    /**
     * - The name of the command (the part after "app/console").
     * - The short description shown while running "php app/console list".
     * - The full command description shown when running the command with
     *   the "--help" option.
     */
    $this
      ->setName('worker-with-args')
      ->setDescription('Runs a worker with arguments.')
      ->setHelp('This command allows you to initiate a queue worker.')
      ->addArgument('queue', InputArgument::REQUIRED, 'The name of the queue we are digging into.')
      ->addArgument('secondary', InputArgument::REQUIRED, 'Some stuff')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    // outputs multiple lines to the console (adding "\n" at the end of each line)
    $output->writeln([
      'Worker with Argument',
      '====================',
      '',
    ]);

    // outputs a message without adding a "\n" at the end of the line
    $output->writeln('You are about to ');
    $output->writeln('run a worker operation');
    $output->writeln('with arguments passed in.');

    $output->writeln(
      'Queue: ' . $input->getArgument('queue')
    );

    $output->writeln(
      'Secondary: ' . $input->getArgument('secondary')
    );
  }
}