Console Commands
---------------------
The purpose of allowing command-line commands is to allow independent operation from the full Drupal system.

As we embark on making these commands work we need to keep in mind making sure these operations can work with both Drupal and on it's own.

We will need to create message queues and workers for each environment level or set the environment level in the message variables.

I have not yet established any structure or process for these. But here is the first thoughts I have of how this should work.

1. I am thinking that we want a command for each queue name.
2. We would have a parent class to extend to connect to the message queue system. That or a trait. That way we write that part of the code once.
3. The worker will grab the query details from the queue. Then run the call to the DAL. Finally, it will store the result into the storage of choice. The initial storage choice is MongoDB. That may evolve.



Creating a Command
---------------------
How to create new commands.

1. Copy an existing command class in the `src/Command` directory and rename to what you want it to do.
2. The process includes updating the `application.php` file with newly added command.



Running a Command
--------------------
Sample commands...

Sample #1:
`php application.php worker`

Result:
```
Worker
============

You are about to 
run a worker operation.
```


Sample #2:
`php application.php worker-with-args transactions "Some Other Data"`

Result:
```
Worker with Argument
====================

You are about to 
run a worker operation
with arguments passed in.
Queue: transactions
Secondary: Some Other Data

```



