<?php

namespace Drupal\ur_appliance\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class WorkerCommand
 * @package Drupal\ur_appliance\Command
 *
 * @TODO: This is a sample command and not fully fleshed out.
 */
class WorkerCommand extends Command {
  protected function configure() {
    /**
     * - The name of the command (the part after "app/console").
     * - The short description shown while running "php app/console list".
     * - The full command description shown when running the command with
     *   the "--help" option.
     */
    $this
      ->setName('worker')
      ->setDescription('Runs a worker.')
      ->setHelp('This command allows you to initiate a queue worker.')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    // outputs multiple lines to the console (adding "\n" at the end of each line)
    $output->writeln([
      'Worker',
      '============',
      '',
    ]);

    // outputs a message without adding a "\n" at the end of the line
    $output->writeln('You are about to ');
    $output->writeln('run a worker operation.');
  }
}