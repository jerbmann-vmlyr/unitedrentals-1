<?php

namespace Drupal\ur_appliance\Interfaces;

interface DataInterface {
  public function getId();

  /**
   * Suggested function for SOME data objects:
   *    public function loadDrupalData();
   */
}