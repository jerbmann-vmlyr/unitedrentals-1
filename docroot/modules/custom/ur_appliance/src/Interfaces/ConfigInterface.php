<?php
/**
 * Created by PhpStorm.
 * User: patrickthurmond
 * Date: 3/8/17
 * Time: 11:25 PM
 */

namespace Drupal\ur_appliance\Interfaces;


interface ConfigInterface {
  public static function set($index, $value);
  public static function get($index);
  public static function load();
}