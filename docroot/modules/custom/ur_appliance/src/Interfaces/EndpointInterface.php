<?php

namespace Drupal\ur_appliance\Interfaces;

interface EndpointInterface {
  /**
   * Import data from DAL response.
   *
   * @param array|\stdClass - The data structure that comes from United Rentals
   * @return mixed
   */
  public function import($data);

  /**
   * Export data to a DAL request format.
   *
   * @param array|object - The object or array of objects being sent to the UR
   * @return mixed
   */
  public function export($data);
}