<?php

namespace Drupal\ur_appliance\Interfaces;

interface EndpointReaderInterface {
  public function read();
}