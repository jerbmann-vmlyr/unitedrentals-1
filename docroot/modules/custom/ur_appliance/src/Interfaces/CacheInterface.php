<?php

namespace Drupal\ur_appliance\Interfaces;

/**
 * Interface CacheInterface
 * @package Drupal\ur_appliance\Interfaces
 */
interface CacheInterface {
  public function getCacheId($method);
  public function getCache($methodID, $reset = FALSE);
  public function setCache($data, $methodID);
}