<?php

namespace Drupal\ur_appliance\Interfaces;

interface AdapterInterface {
  /** @return null|string */
  public function getRequestBody();

  /** @param string $body */
  public function setRequestBody($body);

  /** @return string */
  public function getContentType();

  /** @param string $type */
  public function setContentType($type);

  /** @return array|null */
  public function getHeaders();

  /**
   * Adds Headers to get sent along with the request.
   *
   * @param $key - The header key name, i.e. "Content-Type".
   * @param $value - The header value, i.e. "application/json".
   */
  public function addHeader($key, $value);

  /** @return array */
  public function buildRequestOptions();

  /** @return string */
  public function getBaseUrl();

  /** @return string|null */
  public function getUri();

  /** @param string */
  public function setUri($uri);

  /** @return string */
  public function getMethod();

  /** @param string $method */
  public function setMethod($method = 'GET');

  public function resetMethod();

  /** @return array */
  public function getParams();

  /** @param array $params */
  public function setParams(array $params = []);

  /**
   * Adds parameters to get sent along.
   *
   * @param $field - The parameter field name.
   * @param $value - The parameter value.
   */
  public function addParameter($field, $value);

  /** @return string */
  public function getQueryStrings();

  /** @return string */
  public function buildUrl();

  /** @return array|mixed|null|\stdClass */
  public function execute();

  /** @return array|mixed|\stdClass */
  public function parseResponse();

  /** @return array|mixed|null|\stdClass */
  public function getParsedResponse();

  /** @return string */
  public function getResponse();

  /** @return bool */
  public function shouldLogRaw();

  /** @return array */
  public function getRawLog();

  /** Logs responses */
  public function logResponse();

  /** @return bool */
  public function hasIgnorableError();

  /** @return bool */
  public function hasStatusError();

  /** @return bool */
  public function wasSuccessful();

  /**
   * Gets the HTTP Code from the cURL response.
   *
   * @return mixed|null
   */
  public function getResponseHttpCode();

  /**
   * Gets the response status.
   *
   * @return null
   */
  public function getStatus();

  /**
   * Sets any errors that occur during call execution.
   *
   * @param $str
   */
  public function setExecuteError($str);

  /**
   * Returns any call execution errors.
   *
   * @return string
   */
  public function getExecuteError();
}