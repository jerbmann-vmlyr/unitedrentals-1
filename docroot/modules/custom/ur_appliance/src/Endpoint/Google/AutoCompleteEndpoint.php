<?php

namespace Drupal\ur_appliance\Endpoint\Google;

use Drupal\ur_appliance\Data\Google\AutoCompleteResponse;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\Google\GoogleMapsException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class AutoComplete
 *
 * @see: Documentation can be found at...
 *   https://developers.google.com/places/web-service/autocomplete
 *
 * @package Drupal\ur_appliance\Endpoint\Google
 */
class AutoCompleteEndpoint extends Endpoint {
  protected $processedResults;

  /**
   * Collection constructor.
   *
   * @see This should use the Google Maps Adapter.
   *
   * @param \Drupal\ur_appliance\Interfaces\AdapterInterface $adapter
   */
  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('autocomplete/json');
  }

  /**
   * Get one specific branch back. This is the Placeable item ID, not the UR
   * branch ID.
   *
   * @param int $query
   * @return array|AutoCompleteResponse
   * @throws GoogleMapsException|\Exception
   */
  public function read($query) {
    if (empty($query)) {
      throw new GoogleMapsException('Unable to get branch without item ID.');
    }

    $query = $this->cleanZipQuery($query);

    $this->getAdapter()->addParameter('input', $query);
    $this->getAdapter()->addParameter('types', '(regions)');

    return $this->import( $this->execute() );
  }

  /**
   * @param $query
   * @return bool|string
   */
  protected function cleanZipQuery($query) {
    // Detect and clean up zip codes.
    if (strlen($query) <= 10) {
      /**
       * Assume it is a zip, now Check for Canadian codes and if that is what it
       * is then leave it alone. Otherwise we need to clean it up and shorten to
       * 5 digits because Google does not play nice with the 9 digit codes.
       */
      $canadianPattern = "/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/";
      $usPattern = '/^\d{5}(-\d{4})?$/';

      if (preg_match($canadianPattern, $query)) {
        return $query; // We don't want to mess with these
      }

      if (preg_match($usPattern, $query)) {
        // Convert to a 5 digit zip code
        $query = substr($query, 0, 5);
      }
    }

    return $query;
  }

  /**
   * Creates Rate data objects from the returned response.
   *
   * @param $data
   *    The response object
   * @return mixed|array<Rate> data objects
   *    Processed data objects
   *
   * @throws \Exception
   */
  public function import($data) {
    $this->processedResults = new AutoCompleteResponse($data);
    return $this->processedResults;
  }

  /**
   * @param $data
   * @return mixed
   */
  public function export($data) {
    //
  }
}