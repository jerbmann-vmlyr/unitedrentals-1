<?php

namespace Drupal\ur_appliance\Endpoint\Google;

use Drupal\ur_appliance\Data\Google\DetailsResponse;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\Google\GoogleMapsException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class Details
 *
 * @see: Documentation can be found at...
 *   https://developers.google.com/places/web-service/details
 *
 * @package Drupal\ur_appliance\Endpoint\Google
 */
class DetailsEndpoint extends Endpoint {
  protected $processedResults;

  /**
   * Collection constructor.
   *
   * @see This should use the Google Maps Adapter.
   *
   * @param \Drupal\ur_appliance\Interfaces\AdapterInterface $adapter
   */
  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('details/json');
  }

  /**
   * Get one specific branch back. This is the Placeable item ID, not the UR
   * branch ID.
   *
   * @param string $placeId
   * @return array|DetailsResponse
   * @throws GoogleMapsException|\Exception
   */
  public function read($placeId) {
    if (empty($placeId)) {
      throw new GoogleMapsException('Unable to get branch without item ID.');
    }

    $this->getAdapter()->addParameter('placeid', $placeId);

    return $this->import( $this->execute() );
  }

  /**
   * Creates Rate data objects from the returned response.
   *
   * @param $data
   *    The response object
   * @return mixed|array<Rate> data objects
   *    Processed data objects
   *
   * @throws \Exception
   */
  public function import($data) {
    $this->processedResults = new DetailsResponse($data);
    return $this->processedResults;
  }

  /**
   * @param $data
   * @return mixed
   */
  public function export($data) {
    //
  }
}