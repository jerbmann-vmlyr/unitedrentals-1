<?php

namespace Drupal\ur_appliance\Endpoint;

use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Adapter\Google\GoogleMapsAdapter;
use Drupal\ur_appliance\Adapter\Placeable\PlaceableAdapter;
use Drupal\ur_appliance\Adapter\Placeable\WorkbenchAdapter;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Interfaces\EndpointInterface;

abstract class Endpoint implements EndpointInterface {
  /** @var AdapterInterface */
  protected $adapter;

  /** @var string - The URI of the current method */
  protected $uri = '';

  /** @var array - The list of allowed fields */
  protected $allowedFields = [];

  protected $defaultValues = [];

  /**
   * Endpoint constructor.
   * @param AdapterInterface $adapter
   */
  public function __construct(AdapterInterface $adapter) {
    $this->setAdapter($adapter);
  }

  /**
   * @return AdapterInterface|DalAdapter|GoogleMapsAdapter|PlaceableAdapter|WorkbenchAdapter
   */
  public function getAdapter() {
    return $this->adapter;
  }

  /**
   * @param AdapterInterface $adapter
   */
  public function setAdapter(AdapterInterface $adapter) {
    $this->adapter = $adapter;
  }

  /**
   * Launch the execution of the current method while avoiding adapter pitfalls.
   *
   * I noticed a pitfall where an adapter can get initialized for one endpoint,
   * then for a second one, then go back to the first endpoint to run again. At
   * this point the adapter's URI has been changed (and I assume the other
   * settings as well). So now you have cross-threaded calls. This aims to fix
   * that problem.
   *
   * @return mixed
   */
  public function execute() {
    $this->getAdapter()->setUri( $this->getUri() );
    $this->getAdapter()->tokenCleanup(); // Do this to guarantee that everything was cleaned up.
    $this->runEndpointSpecificFunctions();

    return $this->getAdapter()->execute();
  }

  /**
   * @return string
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * @param string $uri
   */
  public function setUri($uri) {
    $this->uri = $uri;
  }

  public function runEndpointSpecificFunctions() {
    //@TODO: Override this as needed in the specific endpoint class
    // Example: $this->getAdapter()->shouldForceWORSToken();
  }

  protected function filterParamsAddDefaultValues($data, array $required, $onlyReturnRequired = false) {
    $data = (object) $data;

    /*
     * Set default values if they are not set in the passed in data and we are
     * doing a GET method
     */
    if (!empty($this->defaultValues) && $this->getAdapter()->getMethod() == 'GET') {
      foreach ($this->defaultValues AS $field => $value) {
        if (!isset($data->$field)) {
          $data->$field = $value;
        }
      }
    }

    return $this->filterParams($data, $required, $onlyReturnRequired);
  }

  /**
   * Handles validating parameters prior to being used in the API call.
   *
   * @param $data - An array of parameters to pass to the endpoint
   * @param array $required - Array of all required parameters by the endpoint
   * @param bool $onlyReturnRequired - Only returns the required fields
   *
   * @return array
   * @throws EndpointException
   */
  protected function filterParams($data, array $required, $onlyReturnRequired = false) {
    $data = (object) $data;
    $tmpData = new \stdClass();
    if (property_exists($data, 'branchId')) {
      // fix  branchId padding
      $data->branchId = $this->standardBranchId($data->branchId);

    }
    //Verify required fields are set
    foreach ($required AS $field) {
      if (!isset($data->$field)) {
        throw new EndpointException("Missing required field: $field");
      }

      if ($onlyReturnRequired) {
        $tmpData->$field = $data->$field;
      }
    }

    // Then only return the required fields.
    if ($onlyReturnRequired) {
      return (array) $tmpData;
    }

    return (array) $data;
  }

  /**
   * @param $data
   * @param array $required
   * @param bool $onlyReturnRequired
   *
   * @return array
   * @throws EndpointException
   */
  protected function filterAllowedParams($data, array $required, $onlyReturnRequired = false) {
    $data = (array) $data;

    foreach ($data AS $field => $value) {
      if (is_array($value)) {
        $data[$field] = implode(',', $value);
      }
    }

    $fields = $this->filterParams($data, $required, $onlyReturnRequired);
    $finalFields = [];

    // Filter to only contain allowed fields
    foreach ($fields AS $name => $val) {
      if (in_array($name, $this->allowedFields, false)) {
        $finalFields[$name] = $val;
      }
    }

    return $finalFields;
  }

  protected function standardBranchId($string){
    return str_pad($string, 3, "0", STR_PAD_LEFT);
  }

  protected function validateCatClasses($data) {
    $data['catClass'] = array_filter($data['catClass'], function($catClass){
      return preg_match('/[0-9]*-[0-9]*/', $catClass) === 1;
    });
    return $data;
  }
}