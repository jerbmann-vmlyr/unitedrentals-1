<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Location;

use Drupal\ur_appliance\Data\DAL\Branch;
use Drupal\ur_appliance\Data\Google\Place;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class BranchDistances
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class BranchDistancesEndpoint extends Endpoint {
  protected $processedResults = [];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('location/branchdistances');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceWORSToken();
  }

  /**
   * Get the first branch returned.
   *
   * @param \Drupal\ur_appliance\Data\Google\Place $place
   * @param int $distance
   * @param string $types
   *
   * @return Branch|null
   * @throws \Exception
   */
  public function getSingleByPlace(Place $place, $distance = 0, $types = 'GR,AR') {
    $branches = $this->getByPlace($place, $distance, $types);

    if (!empty($branches)) {
      return $branches[0];
    }

    return null;
  }

  /**
   * Gets branches by Place details object.
   *
   * @param Place $place
   * @param int $distance
   * @param string $types
   *
   * @return array|null
   * @throws \Exception
   */
  public function getByPlace(Place $place, $distance = 0, $types = 'GR,AR') {
    $country = $place->getCountry(true);

    $data = [
      'lat' => $place->getLatitude(),
      'long' => $place->getLongitude(),
      'status' => 'A',
      'distance' => $distance,
      'unit' => ($country == 'US') ? 'M' : 'K',
      'types' => $types,
      'webDisplay' => 'true',
      'onlineOrders' => 'true',
      'countryCode' => $country,
    ];

    return $this->read($data);
  }

  /**
   * @param $data - An array of parameters
   * Allowed values:
   *   address        Street address of target location. Max-length 30.
   *   city           City of target location. Max-length 30.
   *   state          Abbreviated state/providence value. Min/max-length 2.
   *   zip            Min/max-length 3-10, valid for CA or US postal codes.
   *   distance       Distance in miles/kilometers from target location.
   *   status         Allowed values "A" = Active (Default), "I" = Inactive
   *   lat            Latitude coordinate for target location.
   *   long           Longitude coordinate for target location.
   *   min            Minimum # of results to return.
   *   unit           Determines unit of measurement for distance. Allowed values "M" = Miles, "K" = Kilometers
   *   types          Array of Branch Types
   *   webDisplay     Boolean. Include records with this webDisplay. Defaults to true
   *   onlineOrders   Boolean.
   *   onSite         Boolean. Include records with this onSite flag. Defaults to null
   *   countryCode    Allowed values "US" or "CA"
   *   limit          Limit # of returned results
   *
   * @return mixed|array<Branch> data objects
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function read($data) {
    if (empty($data) || (!\is_array($data) && !\is_object($data))) {
      throw new DalException('Unable to query branch distances without data.');
    }

    $data = $this->filterParams($data);
    $this->getAdapter()->setParams($data);

    return $this->import($this->execute());
  }

  /**
   * Filters parameters for querying branch distances.
   *
   * @param $data - An array of parameters to pass to the endpoint
   * @param array $required - Array of all required parameters by the endpoint
   * @param bool $onlyReturnRequired - Only returns the required fields
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  protected function filterParams($data, array $required = [], $onlyReturnRequired = false) {
    $data = (object) parent::filterParams($data, $required);
    $validSearch = FALSE;

    /**
     * Endpoint requires city/state or zip are set. Or both lat and long
     * coordinates are set.
     */
    if (isset($data->city) && isset($data->state)) {
      $validSearch = TRUE;
    }

    if (isset($data->zip)) {
      $validSearch = TRUE;
    }

    if (isset($data->lat) && isset($data->long)) {
      $validSearch = TRUE;
    }

    if ($validSearch == FALSE) {
      throw new DalException('branchdistances endpoint requires City/State or Zip parameters. Or Lat and Long parameters.');
    }

    return (array) $data;
  }

  /**
   * Creates Branch data objects from the data returned back from the API call.
   *
   * @param $data - response object from the API call.
   * @return array<Branch> data objects
   *
   * @throws \Drupal\ur_appliance\Exception\DataException|\Exception
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempBranch = new Branch();

    foreach ($data as $result) {
      $branch = clone $tempBranch;
      $branch->import($result);

      $this->processedResults[] = $branch;
      unset($branch);
    }

    return $this->processedResults;
  }

  public function export($data) {
    // branchdistances does not implement an export method for we can only GET data
  }
}