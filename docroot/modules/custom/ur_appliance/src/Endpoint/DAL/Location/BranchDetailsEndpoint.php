<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Location;

use Drupal\ur_appliance\Data\DAL\Branch;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;


class BranchDetailsEndpoint extends Endpoint {
  protected $processedResults = [];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);

    $this->setUri('intranet/branchdetails');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->addParameter('webDetails', true);
    $this->getAdapter()->shouldForceWORSToken();
  }

  /**
   * Gets details for all branches
   */
  public function readAll() {
    return $this->import( $this->execute() );
  }

  /**
   * Returns an array of Branch data objects.
   *
   * @param $data
   *  The response from the request.
   * @return array<Branch>
   *  Array of branch data objects.
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempBranch = new Branch();

    foreach ($data as $result) {
      $branch = clone $tempBranch;
      $branch->import($result);

      $this->processedResults[] = $branch;
      unset($branch);
    }

    return $this->processedResults;
  }

  /**
   * Gets one branch's details.
   *
   * @param $branchId
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Branch
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function read($branchId) {
    if (empty($branchId)) {
      throw new DalException('You must provide a Branch ID to get branch details.');
    }

    // Putting more or less characters in postman returns error so 3 characters is required if requesting by ID
    if (strlen($branchId) !== 3) {
      throw new DalException('Branch ID parameter can be no more or less than 3 characters.');
    }

    $this->getAdapter()->addParameter('id', $branchId);
    return $this->importSingle( $this->execute(), $branchId );
  }

  /**
   * @param $branchIds
   * @return array
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function readMultiple($branchIds) {
    if (empty($branchIds)) {
      throw new DalException('You must provide a Branch ID to get branch details.');
    }

    $this->getAdapter()->addParameter('id', $branchIds);
    return $this->import( $this->execute() );
  }

  public function export($data) {
    // only to satisfy abstract base class
  }

}