<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Catalog;

use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Data\DAL\Item;

/**
 * Class RecentRentalItems
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class RecentRentalItemsEndpoint extends Endpoint {
  protected $processedResults = [];
  protected static $requiredFields = ['accountId'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('catalog/recentrentalitems');
  }
  
  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
  }

  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }
  
  /**
   * @param array $data
   *    Possible values:
   *      accountId
   *      limit
   *
   * @return array
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function read($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to get recentRentalItems without data.');
    }
  
    $data = $this->filterParams($data, self::$requiredFields);
    $this->getAdapter()->setParams($data);
  
    return $this->import( $this->execute() );
  }
  
  /**
   * Handles validating parameters prior to being used in the API call.
   *
   * @param $data - An array of parameters to pass to the endpoint
   * @param array $required - Array of all required parameters by the endpoint
   * @param bool $onlyReturnRequired - Only returns the required fields
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  protected function filterParams($data, array $required, $onlyReturnRequired = false) {
    $data = parent::filterParams($data, $required, $onlyReturnRequired);
    
    if (empty($data['limit'])) {
      $data['limit'] = 8;
    }
    
    return (array) $data;
  }
  
  /**
   * Imports the data from a call to the API.
   *
   * @param array|\stdClass $data
   * @return array
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempItem = new Item();

    if (empty($data)) {
      throw new DalException('There was no data passed.');
    }

    foreach ($data as $result) {
      $item = clone $tempItem;
      $item->import($result);

      $this->processedResults[] = $item;
      unset($item);
    }

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
    // Probably not needed here.
  }
}