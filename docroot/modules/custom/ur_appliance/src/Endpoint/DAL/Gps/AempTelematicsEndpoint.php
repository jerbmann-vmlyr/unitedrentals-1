<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Gps;

use Drupal\ur_appliance\Data\DAL\Gps;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Data\DAL\Item;
use Drupal\ur_appliance\Exception\NoResultsException;
use Drupal\ur_appliance\Exception\ResponseParseException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Exception\DataException;

/**
 * Class AempTelematics
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class AempTelematicsEndpoint extends Endpoint {
  protected $processedResults = [];
  protected static $requiredFields = ['accountId'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('gps/aemptelematics');
    $this->cacheToUser = true;
    $this->allowedFields = ['accountId', 'equipmentId', 'fromDate', 'toDate', 'includeHistory', 'includeChildren'];

    $this->getAdapter()->processAsXML();
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
    $this->getAdapter()->processAsXML();
  }

  /**
   * Gets one's equipment GPS details.
   *
   * @param array $fields
   *   When sending multiple, you can send as an array or comma-separated string.
   *
   *   Possible fields/parameters:
   *     Field            - Required - Type     - Allow Multiple? - Description
   *     -----------------------------------------------------------------------
   *     accountId        - Yes      - Int      - No
   *     equipmentId      - No       - String   - No
   *     fromDate         - Yes      - Int      - No
   *     toDate           - Yes      - Int      - No
   *     includeHistory   - No       - Bool     - No  - Flag to include history records
   *     includeChildren  - No       - Bool     - No  - Flag to include child accounts
   *
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Account
   *
   * @throws DalException|EndpointException|DalDataException
   * @throws EndpointException|DataException|NoResultsException|\Exception
   */
  public function read(array $fields = []) {
    $fields = $this->filterAllowedParams($fields, self::$requiredFields);
    $this->getAdapter()->setParams($fields);
    $result = null;

    try {
      $result = $this->import( $this->execute() );
    }
    catch (ResponseParseException $e) {
      throw new NoResultsException('No results found.');
    }

    return $result;
  }

  /**
   * Imports all results from a result set.
   *
   * @TODO: Build out this method for this endpoint - NOT DONE YET!!!!
   *
   * @param $data
   * @return array<Item> - Returns an array of item objects.
   *
   * @throws DalException|DalDataException|EndpointException
   * @throws DataException|\Exception|NoResultsException
   */
  public function import($data) {
    if (empty($data)) {
      throw new EndpointException('Call failed. No data returned.');
    }

    if ($data == 'No records found matching search criteria') {
      throw new NoResultsException('No results found.');
    }

    $tempItem = new Item();

    if (isset($data->Equipment) && is_array($data->Equipment)) {
      $data = (array) $data->Equipment;
      $gpsData = [];

      /*
       * First we need to group by EquipmentID. Then we can turn these into
       * GPS responses that can be attached to an item.
       */
      foreach ($data AS $equipment) {
        $equipmentID = $equipment->EquipmentHeader->EquipmentID;

        if (!isset($gpsData[$equipmentID])) {
          $entry = $equipment->EquipmentHeader; // Set the base values
        }
        else {
          $entry = $gpsData[$equipmentID];
        }

        $equipment->Location->OperatingHours = $equipment->CumulativeOperatingHours;
        $entry->Locations[] = $equipment->Location;
        $gpsData[$equipmentID] = $entry;
      }

      /*
       * Get GPS objects and create item objects to return.
       */
      foreach($gpsData AS $gps) {
        $item = $item = clone $tempItem;
        $item->import($gps); // Import the item data

        $this->processedResults[] = $item;
        unset($item);
      }
    }
    else {
      // If there's only 1 item, Equipment is a stdClass with the fields
      // directly on it.
      $item = clone $tempItem;
      $item->import($data->Equipment);
      $this->processedResults[] = $item;
      unset($item);
    }

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}