<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Estimate;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class ChargeEstimates
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class ChargeEstimatesEndpoint extends Endpoint {
  /** @var array - Of Required fields */
  protected static $requiredFields = ['branchId', 'delivery', 'pickup', 'rpp', 'startDate', 'returnDate'];

  /**
   * @var Estimate
   */
  protected $estimate;

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/chargeestimates');
  }

  public function runEndpointSpecificFunctions() {
    // We would rather use the user token, but we can use the WORS token
    $this->getAdapter()->shouldPreferUserToken();
  }

  /**
   * Handles validating parameters prior to being used in the API call.
   *
   * @param $data - An array of parameters to pass to the endpoint
   * @param array $required - Array of all required parameters by the endpoint
   * @param bool $onlyReturnRequired - Only returns the required fields
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  protected function filterParams($data, array $required, $onlyReturnRequired = false) {
    $data = (object) parent::filterParams($data, $required, $onlyReturnRequired);

    if (empty($data->catClassAndQty) && empty($data->catClassQtyRates) && empty($data->items)) {
      throw new DalException('Missing required items information');
    }

    return (array) $data;
  }

  /**
   * Build an Estimate object for use here.
   *
   * @param $data
   * @return \Drupal\ur_appliance\Data\DAL\Estimate
   *
   * @throws \Exception
   */
  public function buildEstimate($data) {
    $estimate = new Estimate();
    $estimate->import($data);

    // We can stop here if we have this
    if (!empty($data->catClassQtyRates)) {
      return $estimate;
    }

    // Build the array of cat class and quantity if needed
    if (empty($this->catClassAndQty) && !empty($this->items)) {
      $estimate->buildCatClassAndQtyFromItems();
    }

    return $estimate;
  }

  /**
   * @param $data
   *    Possible values:
   *      branchId
   *      accountId
   *      jobsiteId
   *      delivery
   *      pickup
   *      startDate
   *      returnDate
   *      webRates (defaults to "Y")
   *      rpp (values: Y or N)
   *
   *      (Use this cart-derived values)
   *      -------------------------------
   *      items (array of Item Data Objects)
   *          - Used to build param "catClassAndQty"
   *
   *      (Use this for non-cart derived values)
   *      -------------------------------
   *      catClassQtyRates (array)
   *        catClass
   *        qty
   *        minRate (expect minRate to equal dayRate)
   *        dayRate
   *        weekRate
   *        monthRate
   *
   * @return Estimate
   * @throws DalException|\Exception
   */
  public function read(array $data) {
    // Build new estimate object from data and return an Estimate
    $data = $this->filterParams($data, self::$requiredFields);
    $estimate = $this->buildEstimate($data);

    $this->estimate = $estimate; // Save the estimate for later
    $this->getAdapter()->setParams( $estimate->export() );

    return $this->import( $this->execute() );
  }

  /**
   * Creates Rate data objects from the returned response.
   *
   * @param $data - The response object
   * @return mixed|array<Rate> - Processed Estimate object
   *
   * @throws \Exception
   */
  public function import($data) {
    $data = (array) $data->result;

    if (!empty($data['totals'])) {
      $this->estimate->setTotals($data['totals']);
    }

    if (!empty($data['details'])) {
      $this->estimate->setItemRates( $data['details'] );
    }

    return $this->estimate;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}