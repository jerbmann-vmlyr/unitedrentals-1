<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Comment;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Exception\DAL\DalException;

/**
 * Class OrderComments
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class CommentsEndpoint extends CommentsBase {
  protected static $requiredFields = ['accountId', 'requestId'];
  protected static $requiredPOSTFields = ['requestId', 'commentType'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/comments');
  }

  /**
   * Post Requisition Comment.
   *
   * @param $data - An array of parameters
   *    Allowed values (* indicate required):
   *      transId          String *
   *      commentType      String *
   *        D - Order Comment (Delivery)
   *        R - Order Comment (Rental)
   *      commentText     String
   *
   * @return boolean
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function post($data) {
    return parent::post($data);
  }

}