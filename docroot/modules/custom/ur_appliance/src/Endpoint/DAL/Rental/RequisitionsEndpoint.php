<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\Data\DAL\Contract\Requisition;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class Requisitions
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class RequisitionsEndpoint extends Endpoint {

  public $processedResults = [];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/requisitions');
  }

  /**
   * @param $data
   * @return array
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function read($data) {
    $requisition = new Requisition($data);
    $body = $requisition->extract('read');

    $this->getAdapter()->setParams($body);

    $result = $this->execute();
    if (count($result->result)) {
      $data = (array) $result->result;
      return $this->import( $data );
    }
    return [];

  }

  /**
   * @param $data
   * @return array<Order>
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function import($data) {

    $tempOrder = new Order();

    foreach ($data as $result) {
      $order = clone $tempOrder;

      if (isset($this->processedResults[$result->requestId])) {
        $order = $this->processedResults[$result->requestId];
      }

      $order->import($result);

      $this->processedResults[$result->requestId] = $order;
      unset($order);
    }

    return $this->processedResults;
  }

  /**
   * @param $data
   * @return mixed
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function update($data) {

    $requisition = new Requisition($data);

    $body = $requisition->extract('update');

    // NOTE:
    // The DAL expects the details element to be an array
    // of object(s), even though on update you're only
    // ever going to have one details object. The
    // api_appliance exports a "flat" data object
    // that is a simple associative array, so
    // we need to put that array in another
    // array to fit with what DAL wants.
    // @TODO: think of a way to put this into the Requisition smart model
    if (isset($body['details'])) {
      $body['details'] = [$body['details']];
    }

    $adapter = $this->getAdapter();
    $adapter->setRequestBody(json_encode($body));
    $adapter->setMethod('PUT');
    $adapter->setRequestJsonParams($body);

    return $this->execute();
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}