<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Item;
use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\DataException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Exception\NoResultsException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class Transactions.
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class TransactionsEndpoint extends Endpoint {
  protected $processedResults = [];
  protected static $requiredFields = ['accountId','type'];

  /**
   * Static Vars.
   *
   * TODO: Add requester, requesterName, requesterPhone as required.
   *
   * @var array
   */
  protected static $requiredPOSTFields = [
    'branchId',
    'accountId',
    'transType',
    'email',
    'jobId',
    'startDateTime',
    'returnDateTime',
    'delivery',
    'pickup',
    'pickupFirm',
    'rpp',
    'emailPDF',
    'transSource',
    'jobContactName',
    'jobContactPhone',
    'eqpDetails',
    'totalPickup',
    'totalDelivery',
  ];

  protected static $requiredPUTFields = [
    'transId',
    'updateType',
  ];

  /**
   * TransactionsEndpoint constructor.
   *
   * @param \Drupal\ur_appliance\Interfaces\AdapterInterface $adapter
   *   Requires an Adapter Interface.
   */
  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/transactions');
    $this->defaultValues = [
      'includeAllocations' => 'N',
      'custOwn' => '1'
    ];
  }

  /**
   * Sets up required functions for this class.
   */
  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
  }

  /**
   * Performs the GET method on Transactions (Search Rental Transactions by Account ID).
   * Gets all Transaction information for a given Account Id.
   *
   *   Possible Parameters of the $data var below.
   *    ===============================
   *    id - Transaction ID
   *    equipmentId - Equipment ID
   *    accountId - Customer Account Number. Single account number or
   *                comma-separated list.
   *    type - Type of request.
   *        Possible Types
   *        -------------------------------
   *        1 = Open Contracts
   *        2 = Open Quotes
   *        3 = Open Reservations
   *        4 = Closed Contracts
   *        6 = Pickup Requests (must be the only type)
   *        7 = Past Due (must be the only type)
   *        8 = Canceled Quotes can only be grouped with type 9)
   *        9 = Canceled Reservations (can only be grouped with type 8)
   *        May be comma-separated list (ex: 1,2,3,4).
   *    includeAllocations - Should Allocation Codes be included? (Y or N)
   *    includeLinked - Flag to include Linked (child) account data.
   *    includeClosed - Include Closed Contract Details.
   *    headerOnly - Header only.
   *    startRecord - Record Number to start data at. Default = 1.
   *    maxRecords - Maximum number of records to return. Defaults to all
   *                 records. If number is null, 0, or >= max records, all
   *                 records will be returned. Negative numbers will return error.
   *    jobId - Job Number: Base64 encoded single item or comma-separated list.
   *    po - Purchase Order: Base64 encoded single item or comma-separated list.
   *    glCode - G/L Account Code: Should be URL Encoded.
   *    catClass - Cat/Class: Single item or comma-separated list.
   *    custOwn - Whether to include customer owned. (Y or N)
   *    crtGuid - Created by GUID.
   *    apvGuid - Approved by GUID.
   *    reqGuid - Requested by GUID.
   *    transId - Transaction Id.
   *    branchId - Branch Number.
   *    category - Equipment Category.
   *    sortBy - Options are startDateTime, returnDateTime, po, branchId,
   *             accountId, transId, jobsiteId, none (default).
   *    sortOrder - Only valid when sortBy is passed. Options: ASC, DESC,
   *                none (default).
   *    search - Searches across jobsiteState, catClass, catClassDesc,
   *             equipmentId, jobsiteCity, and orderedBy.
   *    daysOffset - Number of Days: Number of days that are older than the
   *                 current date (only applies to type 2, 3, 8, and 9).
   *    source - Transaction Source: Specifies to return transactions created
   *             by a specific source.
   *
   * @param array|mixed $data
   *   Request parameters.
   *
   * @return array
   *   Order - An array of Order objects populated from the import() method.
   *
   * @throws DalException
   * @throws DalDataException
   * @throws EndpointException
   */
  public function readAll($data) {
    $data = $this->filterParams($data, self::$requiredFields);

    $this->getAdapter()->setMethod('GET');
    $this->getAdapter()->setParams($data);

    return $this->import( $this->execute() );
  }

  /**
   * List all items for the given account(s) and type(s).
   *
   * Possible Parameters for the $data var below.
   *    ===============================
   *    id - Transaction ID
   *    equipmentId - Equipment ID
   *    accountId - Customer Account Number. Single account number or
   *                comma-separated list.
   *    type - Type of request.
   *        Possible Types
   *        -------------------------------
   *        1 = Open Contracts
   *        2 = Open Quotes
   *        3 = Open Reservations
   *        4 = Closed Contracts
   *        6 = Pickup Requests (must be the only type)
   *        7 = Past Due (must be the only type)
   *        8 = Canceled Quotes can only be grouped with type 9)
   *        9 = Canceled Reservations (can only be grouped with type 8)
   *        May be comma-separated list (ex: 1,2,3,4).
   *    includeAllocations - Should Allocation Codes be included? (Y or N)
   *    includeLinked - Flag to include Linked (child) account data.
   *    includeClosed - Include Closed Contract Details.
   *    headerOnly - Header only.
   *    startRecord - Record Number to start data at. Default = 1.
   *    maxRecords - Maximum number of records to return. Defaults to all
   *                 records. If number is null, 0, or >= max records, all
   *                 records will be returned. Negative numbers will return error.
   *    jobId - Job Number: Base64 encoded single item or comma-separated list.
   *    po - Purchase Order: Base64 encoded single item or comma-separated list.
   *    glCode - G/L Account Code: Should be URL Encoded.
   *    catClass - Cat/Class: Single item or comma-separated list.
   *    custOwn - Whether to include customer owned. (Y or N)
   *    crtGuid - Created by GUID.
   *    apvGuid - Approved by GUID.
   *    reqGuid - Requested by GUID.
   *    transId - Transaction Id.
   *    branchId - Branch Number.
   *    category - Equipment Category.
   *    sortBy - Options are startDateTime, returnDateTime, po, branchId,
   *             accountId, transId, jobsiteId, none (default).
   *    sortOrder - Only valid when sortBy is passed. Options: ASC, DESC,
   *                none (default).
   *    search - Searches across jobsiteState, catClass, catClassDesc,
   *             equipmentId, jobsiteCity, and orderedBy.
   *    daysOffset - Number of Days: Number of days that are older than the
   *                 current date (only applies to type 2, 3, 8, and 9).
   *    source - Transaction Source: Specifies to return transactions created
   *             by a specific source.
   *
   * @param array|mixed $data
   *   Request parameters.
   * @param array $orderFields
   *   Required order fields.
   *
   * @return array
   *   Order - An array of Order objects populated from the import() method.
   *
   * @throws DalException
   * @throws DalDataException
   * @throws EndpointException
   * @throws NoResultsException
   * @throws DataException
   * @throws NoResultsException
   * @throws \Exception
   */
  public function readAllByItem($data, $orderFields = []) {
    $data = $this->filterParams($data, self::$requiredFields);

    $this->getAdapter()->setMethod('GET');
    $this->getAdapter()->setParams($data);

    return $this->importByItem( $this->execute(), $orderFields, false);
  }

  /**
   * Performs the GET method on Transactions (Look up Rental Transactions by ID).
   *
   * Retrieves a single transaction from the DAL
   *
   * Requires id field. Holds the transaction ID.
   *
   * Possible Parameters
   *    ===============================
   *    id - Transaction ID
   *    equipmentId - Equipment ID
   *    accountId - Customer Account Number. Single account number or
   *                comma-separated list.
   *    type - Type of request.
   *        Possible Types
   *        -------------------------------
   *        1 = Open Contracts
   *        2 = Open Quotes
   *        3 = Open Reservations
   *        4 = Closed Contracts
   *        6 = Pickup Requests (must be the only type)
   *        7 = Past Due (must be the only type)
   *        8 = Canceled Quotes can only be grouped with type 9)
   *        9 = Canceled Reservations (can only be grouped with type 8)
   *        May be comma-separated list (ex: 1,2,3,4).
   *    includeAllocations - Should Allocation Codes be included? (Y or N)
   *    includeLinked - Flag to include Linked (child) account data.
   *    includeClosed - Include Closed Contract Details.
   *    headerOnly - Header only.
   *    startRecord - Record Number to start data at. Default = 1.
   *    maxRecords - Maximum number of records to return. Defaults to all
   *                 records. If number is null, 0, or >= max records, all
   *                 records will be returned. Negative numbers will return error.
   *    jobId - Job Number: Base64 encoded single item or comma-separated list.
   *    po - Purchase Order: Base64 encoded single item or comma-separated list.
   *    glCode - G/L Account Code: Should be URL Encoded.
   *    catClass - Cat/Class: Single item or comma-separated list.
   *    custOwn - Whether to include customer owned. (Y or N)
   *    crtGuid - Created by GUID.
   *    apvGuid - Approved by GUID.
   *    reqGuid - Requested by GUID.
   *    transId - Transaction Id.
   *    branchId - Branch Number.
   *    category - Equipment Category.
   *    sortBy - Options are startDateTime, returnDateTime, po, branchId,
   *             accountId, transId, jobsiteId, none (default).
   *    sortOrder - Only valid when sortBy is passed. Options: ASC, DESC,
   *                none (default).
   *    search - Searches across jobsiteState, catClass, catClassDesc,
   *             equipmentId, jobsiteCity, and orderedBy.
   *    daysOffset - Number of Days: Number of days that are older than the
   *                 current date (only applies to type 2, 3, 8, and 9).
   *    source - Transaction Source: Specifies to return transactions created
   *             by a specific source.
   *
   * @param array|mixed $data
   *   Request parameters.
   *
   * @return \Drupal\ur_appliance\Data\DAL\Order
   *   A single Order object if one was found matching the submitted $transactionId.
   *
   * @throws DalException
   * @throws EndpointException
   */
  public function read($data) {
    $data = $this->filterParams($data, ['id']);

    $this->getAdapter()->setMethod('GET');
    $this->getAdapter()->setParams($data);

    return $this->importSingle($this->execute(), $data['id']);
  }

  /**
   * Imports a single transaction.
   *
   * @param array|mixed $data
   *   Request parameters.
   * @param int $transactionId
   *   Requested transaction ID.
   *
   * @return \Drupal\ur_appliance\Data\DAL\Order
   *   Order object.
   *
   * @throws DalException
   */
  public function importSingle($data, int $transactionId) {
    $result = $this->import($data);
  
    /** @var \Drupal\ur_appliance\Data\DAL\Order $order */
    foreach ($result as $order) {
      if ($order->getTransId() == $transactionId) {
        return $order;
      }
    }

    return null;
  }
  
  /**
   * Translates the DAL results to api_appliance results.
   *
   * @param array|mixed $data
   *   Supplied data to be imported.
   *
   * @return array
   *   Order Object.
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempOrder = new Order();

    foreach ($data as $result) {
      $order = clone $tempOrder;

      if (isset($this->processedResults[$result->transId])) {
        $order = $this->processedResults[$result->transId];
      }

      $order->import($result);

      $this->processedResults[$result->transId] = $order;
      unset($order);
    }

    return $this->processedResults;
  }
  
  /**
   * Import the results by item instead of by order.
   *
   * @param array|mixed $data
   *   Request parameters.
   * @param array $orderFields
   *   Required order fields.
   * @param boolean $requireResults
   *   Allows bypassing the 'No data returned' exceptions.
   *
   * @return array
   *   Imported items after individual updating.
   *
   * @throws DalDataException
   * @throws NoResultsException
   * @throws EndpointException
   * @throws DataException
   * @throws \Exception
   */
  public function importByItem($data, $orderFields = [], $requireResults = TRUE) {
    // Sometimes we don't want to fail on 'No data returned'.
    //  I am looking at asynchronous loops in particular.
    if ($requireResults) {
      if (empty($data)) {
        throw new EndpointException('Call failed. No data returned.');
      }

      if ($data->status == 1 && $data->messageText = 'No Records Found') {
        throw new NoResultsException('No data returned.');
      }
    }
    
    $data = (array) $data->result;
    $tempItem = new Item();

    foreach ($data as $result) {
      $item = clone $tempItem;
      $item->import($result, $orderFields);

      $this->processedResults[] = $item;
      unset($item);
    }

    return $this->processedResults;
  }
  
  /**
   * Post data from post() must be prepared for the import() function.
   *
   * We know what we expect when we do a post() so set the data that comes back.
   *
   * @param array|mixed $data
   *   Incoming result set for preparation.
   *
   * @return \Drupal\ur_appliance\Data\DAL\Order
   *   Order object.
   */
  public function postImport($data) {
    $data = (array) $data->result;
    $order = new Order();

    if (isset($data['transID'])) {
      $order->setTransId($data['transID']);
    }

    if (isset($data['reqID'])) {
      $order->setRequisitionId($data['reqID']);
    }

    return $order;
  }
  
  /**
   * Implements the export() method.
   *
   * @param array|mixed $data
   *   Incoming result dataset.
   *
   * @return mixed
   */
  public function export($data) {
    // TODO: Implement export() method.
  }
  
  /**
   * Runs the POST method of the transactions API and submits a new transaction.
   *
   * @param array|mixed $data
   *   The array of data to submit.
   *
   * @return mixed
   *   DAL results
   *
   * @throws DalDataException
   * @throws \EndpointException
   */
  public function post($data) {
    $data = $this->filterPOSTParams($data, self::$requiredPOSTFields, FALSE);

    // Set parameters and proper method
    $this->getAdapter()->setRequestBody(json_encode($data));
    $this->getAdapter()->setMethod('POST');

    return $this->postImport($this->execute());
  }
  
  /**
   * Validate and format the passed in data for submission to the POST.
   *
   * @param array|mixed $data
   *   Submitted data.
   * @param array $required
   *   Required parameters for the endpoint.
   * @param bool $onlyReturnRequired
   *   Return Required flag.
   *
   * @return array
   *   Formatted params.
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  protected function filterPOSTParams($data, array $required, $onlyReturnRequired = FALSE) {
    $order = new Order();
    $order->import($data);
    $postData = $order->postExport();

    $postData = parent::filterParams($postData, $required, $onlyReturnRequired);

    // Validate all values have been supplied for the required fields
    foreach ($required as $field) {
      if (empty($postData[$field]) && $postData[$field] !== 0) {
        throw new DalDataException("$field must have a value to submit a transaction.");
      }
    }

    return $postData;
  }
  
  /**
   * Validate and format the passed in data for submission to the PUT.
   *
   * @param array|mixed $data
   *   Request parameters.
   * @param array $required
   *   Required parameters for verification.
   * @param bool $onlyReturnRequired
   *   Return Required flag.
   *
   * @return array
   *   Filtered and verified parameters.
   *
   * @throws DalDataException
   */
  protected function filterPUTParams($data, array $required, $onlyReturnRequired = FALSE) {
    $order = new Order();
    $order->import($data);
    if (!isset($data['eqpDetails'])) {
      $data = $order->putExport();
    }

    $changedData = parent::filterParams($data, $required, $onlyReturnRequired);

    // Validate all values have been supplied for the required fields
    foreach ($required as $field) {
      if (empty($changedData[$field]) && $changedData[$field] !== 0) {
        throw new DalDataException("$field must have a value to update a transaction.");
      }
    }

    return $changedData;
  }
  
  /**
   * Implements the data update.
   *
   * @param array|mixed $data
   *   Result data to be updated.
   *
   * @return mixed
   *   Success or Failure.
   *
   * @throws DalDataException
   */
  public function update($data) {
    $data = $this->filterPUTParams($data, self::$requiredPUTFields);
    $this->getAdapter()->setRequestBody(json_encode($data));
    $this->getAdapter()->setMethod('PUT');
    $this->getAdapter()->setRequestJsonParams($data);

    return $this->execute();
  }
}