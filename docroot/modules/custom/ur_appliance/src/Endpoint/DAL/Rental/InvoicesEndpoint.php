<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Invoice;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Traits\HasMoreTrait;

/**
 * @Class InvoiceEndpoint
 */
class InvoicesEndpoint extends Endpoint {
  const TYPE_OUTSTANDING_INVOICES = false;
  const TYPE_ALL_INVOICES = true;
  const DATETIME_STRING_FORMAT = \DateTime::ISO8601;

  use HasMoreTrait;

  /**
   * @var array - holds the data entities
   */
  protected $processedResults = [];

  /**
   * Fields required for a single record GET request
   * @var array
   */
  protected $readRequiredFields = ['accountId', 'id'];

  /**
   * Fields required for an index GET method
   * @var array
   */
  protected $readAllRequiredFields = ['accountId'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/invoices');
    $this->defaultValues = [
      'includeLinked' => false,
      'includeZeroDollar' => self::TYPE_OUTSTANDING_INVOICES
    ];
  }

  /**
   * @param $data
   *
   * @return null|Invoice
   * @throws EndpointException
   */
  public function read($data) {

    $data = $this->filterParams($data, $this->readRequiredFields);

    $this->getAdapter()->setMethod('GET');
    $this->getAdapter()->setParams($data);

    return $this->importSingle($this->execute(), $data['id']);
  }

  /**
   * The GET index request -
   *
   * The GET index response -
   *  status 1- success, 2 - error
   *  messageId - null | code - from MSGINDFL in Rentalman
   *  messageText - null | string message
   *  request ** - array - copy of our request
   *  result *** - array - Data Lookup result
   *  recordCount - int
   *  startRecord - int
   *  endRecord - int
   *  moreRecords - boolean - true/false
   *
   * ***
   *  id - string
   *  accountId - string
   *  accountName - string
   *  seqId - int
   *  status - string
   *  type - string
   *  date - Timestamp
   *  jobId - string
   *  jobName - string - jobsite name
   *  jobsiteAddress1 - string - UR jobsite address
   *  jobsiteCity - string UR jobsite city
   *  jobsiteState - string - UR Jobsite state
   *  jobsiteContact - string - contact name
   *  jobsitePhone - string - contact phone
   *  customerJobId - string - customer job site id
   *  po - string - purchase order
   *  amount - decimal - ORIGINAL amount
   *  balance - decimal - Current Balance
   *
   * @param $data
   *
   * @return mixed
   */
  public function readAllByItem($data, $wantHasMore = false) {
    $data = $this->filterParamsAddDefaultValues($data, $this->readAllRequiredFields);

    $this->getAdapter()->setMethod('GET');
    $this->getAdapter()->setParams($data);
    $response = $this->execute();
    if ($wantHasMore) {
      $this->setHasMore($response, 'moreRecords');
    }
    return $this->import($response);
  }

  /**
   * Import data from DAL response.
   *
   * @param array|\stdClass - The data structure that comes from United Rentals
   *
   * @return mixed
   */
  public function import($data) {
    $data = (array)$data->result;
    $tempInvoice = new Invoice();

    foreach ($data as $result) {
      $invoice = clone $tempInvoice;
      $invoice->import($result);
      $this->processedResults[] = $invoice;
    }

    return $this->processedResults;
  }

  /**
   * Export data to a DAL request format.
   *
   * @param array|object - The object or array of objects being sent to the UR
   *
   * @return mixed
   */
  public function export($data) {

  }

  public function importSingle($data, $invoiceId) {
    $result = $this->import($data);

    /** @var Invoice $invoice */
    foreach ($result as $invoice) {
      if ($invoice->getId() == $invoiceId) {
        return $invoice;
      }
    }

    return null;
  }
}