<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class Transactions
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class ContractsEndpoint extends Endpoint {
  protected $processedResults = [];
  // FYI: $requiredFields['type'] !== $requiredPUTFields['type']
  protected static $requiredFields = ['type'];
  protected static $requiredPUTFields = ['transId', 'extensionDateTime', 'requester', 'type'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/contracts');
    $this->defaultValues = [
      'type' => '1',
    ];
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
  }

  /**
   * Performs the GET method on Contracts (Search Rental Contracts by Type)
   * Gets all Contract information for a given type.
   *
   * @param $data
   *
   * @return array Contracts - An array of Contract objects populated from the import() method.
   *
   * @throws DalException|DalDataException|EndpointException
   */
  public function readAll($data) {
    $data = $this->filterParamsAddDefaultValues($data, self::$requiredFields);

    $this->getAdapter()->setMethod('GET');
    $this->getAdapter()->setParams($data);

    return $this->import($this->execute());
  }

  /**
   * Performs the GET method on Contracts (Look up Rental Transactions by ID)
   * Retrieves a single transaction from the DAL
   *
   * Requires id field. Holds the transaction ID.
   *
   * @param $data
   *
   * @return Order - A single Order object if one was found matching the submitted $transactionId
   *
   * @throws DalException|EndpointException
   */
  public function read($data) {
    $data = $this->filterParams($data, ['id']);

    $this->getAdapter()->setMethod('GET');
    $this->getAdapter()->setParams($data);

    return $this->importSingle($this->execute(), $data['id']);
  }

  /**
   * Performs the PUT (Extend Rental) method on a Contract (Looks up the contract by transId)
   *
   * @param $data
   * @return string Message - Indicates success or failure
   */
  public function update($data) {
    $data = $this->filterPUTParams($data, self::$requiredPUTFields);

    $this->getAdapter()->setRequestBody(json_encode($data));
    $this->getAdapter()->setMethod('PUT');
    $this->getAdapter()->setRequestJsonParams($data);
    return $this->execute();
  }

  protected function filterPUTParams($data, array $required, $onlyReturnRequired = false) {
    $order = new Order();
    $order->import($data);
    $changedData = $order->putContractExtensionExport($data);

    $changedData = parent::filterParams($changedData, $required, $onlyReturnRequired);

    // Validate all values have been supplied for the required fields
    foreach ($required as $field) {
      if (empty($changedData[$field]) && $changedData[$field] !== 0) {
        throw new DalDataException("{$changedData[$field]} must have a value to update a transaction.");
      }
    }

    return $changedData;
  }

  public function export($data) {
    // TODO: Implement export() method.
    return $export;
  }

  public function import($data) {
    // TODO: Implement import() method.
    return $data;
  }

}
