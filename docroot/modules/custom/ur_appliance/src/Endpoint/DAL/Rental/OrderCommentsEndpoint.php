<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Comment;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Exception\DAL\DalException;

/**
 * Class OrderComments
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class OrderCommentsEndpoint extends CommentsBase {
  protected static $requiredFields = ['transId'];
  protected static $requiredPOSTFields = ['transId', 'commentType'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/ordercomments');
  }
  
  /**
   *
   * Post Order Comment.
   *
   * @param $data - An array of parameters
   *    Allowed values (* indicate required):
   *      transId          String *
   *      commentType      String *
   *        D - Order Comment (Delivery)
   *        R - Order Comment (Rental)
   *      commentText     String
   *
   * @return boolean
   */
  public function post($data) {
    return parent::post($data);
  }

  /**
   * @param array $data
   * @return array
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function read($data) {
    $params = $this->filterParams($data, self:: $requiredFields);
    $this->getAdapter()->setMethod('GET');

    //do request for 'R' types
    $params['commentType'] = 'R';
    $this->getAdapter()->setParams($params);
    $this->import($this->execute());

    //do again for 'D' types
    $params['commentType'] = 'D';
    $this->getAdapter()->setParams($params);

    return $this->import($this->execute());
  }
}