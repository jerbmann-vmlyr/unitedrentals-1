<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Rate;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class Rates
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class RatesEndpoint extends Endpoint {
  protected $processedResults;

  protected static $requiredFields = ['catClass', 'branchId'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/rates');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceWORSToken();
  }

  /**
   * @param $data - An array of parameters
   *    Allowed values (* indicate required):
   *      catClass*           Cat/Class (May be comma-separated list)
   *      branchId*           Branch Number
   *      accountId           Customer Account Number
   *      jobsiteId           Customer Job Number
   *      startDate           Rental Start Date (Defaults to today if not passed)
   *      deliveryDistance    One way delivery distance in miles
   *      includeRateTiers    Return rental rate tiers - boolean (true, false).
   *                          Only UR.COM employees can get this data, so if requested,
   *                           and valid user token must be passed.
   *      webRates            Show Web Rates - boolean ("Y"/"N"). We force this
   *                           parameter to "Y".
   *
   * @return mixed|array<Rate> data objects
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   */
  public function read($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to get rates without data.');
    }

    if (array_key_exists('catClass', $data)) {
      $data = $this->validateCatClasses($data);
    }

    if (!isset($data['catClass']) || (count($data['catClass']) === 0)) {
      throw new EndpointException("Missing required field: catClass");
    }

    $data = $this->filterParams($data, self::$requiredFields);
    $data['catClass'] = implode(',', $data['catClass']);
    $this->getAdapter()->setParams($data);

    return $this->import( $this->execute() );
  }

  /**
   * Handles validating parameters prior to being used in the API call.
   *
   * @param $data - An array of parameters to pass to the endpoint
   * @param array $required - Array of all required parameters by the endpoint
   * @param bool $onlyReturnRequired - Only returns the required fields
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  protected function filterParams($data, array $required, $onlyReturnRequired = false) {
    $data = parent::filterParams($data, $required, $onlyReturnRequired);

    // Force setting of webRates to "Y" as we always want the web rates.
    $data['webRates'] = 'Y';

    return (array) $data;
  }

  /**
   * Creates Rate data objects from the returned response.
   *
   * @param $data
   *    The response object
   * @return mixed|array<Rate> data objects
   *    Processed data objects
   *
   * @throws \Exception
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempRate = new Rate();

    foreach ($data as $result) {
      $rate = clone $tempRate;
      $rate->import($result);

      $this->processedResults[] = $rate;
      unset($rate);
    }

    return $this->processedResults;
  }

  /**
   * @param $data
   * @return mixed
   */
  public function export($data) {
    // @TODO: Implement export() method.
  }
}