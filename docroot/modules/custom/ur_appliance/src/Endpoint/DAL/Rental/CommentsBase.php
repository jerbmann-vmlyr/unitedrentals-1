<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Comment;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class CommentsBase - Base class used for Comments endpoints
 * @package Drupal\ur_appliance\Endpoint\DAL\Rental
 */
abstract class CommentsBase extends Endpoint {
  protected static $requiredFields = [];
  protected static $requiredPOSTFields = [];
  protected $processedResults = [];

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
  }

  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }

  /**
   * Runs the GET method for the specified comments endpoint
   *
   * @param array $data - An array of parameters, see Comments or OrderComments
   *                      Endpoints for required fields
   *   Allowed values (* indicate required):
   *    requestId*           Request ID
   *    transId*             Transaction Id
   *    accountId*           Account ID
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function read($data) {
    $data = $this->filterParams($data, self::$requiredFields);
    $this->getAdapter()->setParams($data);

    return $this->import( $this->execute() );
  }

  /**
   * Imports Comment objects for endpoint responses
   *
   * @param $data - The data from the endpoint response
   * @return array - An array of Comment objects
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempComment = new Comment();

    if (empty($data)) {
      throw new DalException("There was no data passed.");
    }

    foreach ($data as $result) {
      $comment = clone $tempComment;
      $comment->import((array) $result);

      $this->processedResults[] = $comment;
      unset($comment);
    }
    return $this->processedResults;
  }

  /**
   * @param $data
   * @return bool
   */
  public function post($data) {
    $data = $this->filterParams($data, self::$requiredPOSTFields);

    $this->getAdapter()->setRequestBody(json_encode($data));
    $this->getAdapter()->setMethod('POST');
    $this->execute(); // Run the call

    // Endpoint doesn't return results in the DAL so just check if it was successful
    return $this->getAdapter()->wasSuccessful();
  }

  /**
   * @param $data
   */
  public function export($data) {
    // TODO: Implement export() method.
  }
}