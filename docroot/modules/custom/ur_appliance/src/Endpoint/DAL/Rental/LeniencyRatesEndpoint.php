<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;


use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Exception\DAL\DalException;

class LeniencyRatesEndpoint extends Endpoint {

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/leniencyrates');
  }

  public function runEndpointSpecificFunctions() {
    // We would should use the user token
    $this->getAdapter()->shouldForceUserToken();
  }

  /**
   * @param $data
   *
   * @return array|mixed
   * @throws DalException
   * @throws \Exception
   */
  public function read($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to get leniency rates without data.');
    }
    $requiredFields = ['contract', 'lineNum', 'startDate'];
    foreach ($requiredFields as $rf) {
      if (!isset($data[$rf]) || empty($data[$rf])) {
        throw new DalException("Cannot get leniency rates without a(n) $rf.");
      }
    }

    $this->getAdapter()->setParams($data);
    return $this->import($this->execute());
  }

  /**
   * Creates Leniency Rates data object from the returned response.
   *
   * @param $data
   *    The response object
   *
   * @return mixed|array<LeniencyRate> data objects
   *    Processed data objects
   *
   * @throws \Exception
   */
  public function import($apiResponse) {
    $startDate = strtotime($apiResponse->request->startDate);
    $data = $apiResponse->result;
    $processedResults = [];
    $previousRate = -1;
    $previousCounter = -1;
    foreach ($data as $counter => $record) {
      if ($record != $previousRate) {

        // Set the end date of the previous rate.
        if ($previousCounter != -1) {
          $processedResults[$previousCounter]['endDate'] = date('Y-m-d', $startDate + (($counter - 1) * 86400));
        }

        // Build the current rate period record.
        $rec = [
          'startDate' => date('Y-m-d', $startDate + ($counter * 86400)),
          'endDate' => '',
          'rate' => $record,
        ];

        $processedResults[$counter] = $rec;

        $previousRate = $record;
        $previousCounter = $counter;
      }
    }

    // Set the end date of the last item.
    if (count($processedResults) >= 1) {
      $processedResults[$previousCounter]['endDate'] = date('Y-m-d', $startDate + ((count($data) - 1) * 86400));
    }
    $filteredResults = array_filter($processedResults, function ($a) {
      return !(strtotime($a['endDate']) < strtotime('today') || $a['rate'] <= 0);
    });
    return $filteredResults;
  }

  /**
   * @param $data
   *
   * @return mixed|void
   */
  public function export($data) {
    // TODO: Implement export() method.
  }

}
