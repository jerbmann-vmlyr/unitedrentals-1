<?php


namespace Drupal\ur_appliance\Endpoint\DAL\Rental;


use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Data\DAL\PickupRequest as Data;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

class PickupRequestsEndpoint extends Endpoint {
  protected $processedResults = [];

  protected static $requiredPOSTFields = ['transId', 'pickupDateTime', 'address1', 'city', 'state', 'zip', 'transLineId', 'quantity'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/pickuprequests');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceWORSToken();
  }

  /**
   * Import data from DAL response.
   *
   * @param array|\stdClass - The data structure that comes from United Rentals
   *
   * @return mixed
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempItem = new Data();

    foreach ($data as $result) {
      $item = clone $tempItem;

      if (isset($this->processedResults[$result->transId])) {
        $item = $this->processedResults[$result->transId];
      }

      $item->import($result);

      $this->processedResults[$result->transId] = $item;
      unset($item);
    }

    return $this->processedResults;
  }

  /**
   * Creates a new account from given data.
   *
   * @param $data
   * @return null
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function create($data) {
     
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to create a new account without data.');
    }

    $data = $this->filterPOSTParams($data, self::$requiredPOSTFields);
    $this->getAdapter()->setMethod('POST');
    $this->getAdapter()->setParams($data);

    $result = $this->execute();

    if($result->status!=0){
      throw new DalDataException($result->messageText);
    }
    /* success - we'll have an id field - either with data or none - let's handle it on the front end
    */
    return ['pickupId' => $result->result->id,
      'transLineId' => $data['transLineId'],
      'transId' => $data['transId']];
  }

  /**
   * Export data to a DAL request format.
   *
   * @param array|object - The object or array of objects being sent to the UR
   *
   * @return mixed
   */
  public function export($data) {
    //TODO
  }

    /**
     * Validate and format the passed in data for submission to the POST
     *
     * @param $data - Submitted data
     * @param array $required - array of required parameters for the endpoint
     * @param bool $onlyReturnRequired
     *
     * @return array - formatted array
     * @throws \Drupal\ur_appliance\Exception\EndpointException
     * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
     */
    protected function filterPOSTParams($data, array $required, $onlyReturnRequired = false) {
        $postData = parent::filterParams($data, $required, $onlyReturnRequired);

        foreach ($postData AS $idx => $val) {
            if (empty($val)) {
                unset($postData[$val]);
            }
            else if ($idx == 'phone') {
                // Clean up the number
                $phone = preg_replace( '/[^0-9]/', '', trim($val));

                // Now add dashes the match required DAL format
                $phone = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '$1-$2-$3', $phone);

                $postData[$idx] = $phone;
            }
        }

        return $postData;
    }
}