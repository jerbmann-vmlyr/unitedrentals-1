<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;


use Drupal\ur_appliance\Data\DAL\Counts;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Exception\DAL\DalException;

class CountsEndpoint extends Endpoint {

  protected $processedResults;

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/counts');
  }

  public function runEndpointSpecificFunctions() {
    // We would should use the user token
    $this->getAdapter()->shouldForceUserToken();
  }

  /**
   * @param $data
   * @return array|mixed
   * @throws DalException
   * @throws \Exception
   */
  public function read($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to get counts without data.');
    }

    if (empty($data['accountId'])){
      throw new DalException("Cannot get counts without an account id.");
    }

    $this->getAdapter()->setParams($data);

    return $this->import( $this->execute() );
  }

  /**
   * Creates Counts data object from the returned response.
   *
   * @param $data
   *    The response object
   * @return mixed|array<Rate> data objects
   *    Processed data objects
   *
   * @throws \Exception
   */
  public function import($data) {
    $data = $data->result[0];
    $tempCounts = new Counts();

    $tempCounts->import($data);
    $this->processedResults = $tempCounts;

    return $this->processedResults;
  }

  /**
   * @param $data
   * @return mixed|void
   */
  public function export($data)
  {
    // TODO: Implement export() method.
  }

}
