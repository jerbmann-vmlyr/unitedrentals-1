<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\CostAllocation;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Exception\DAL\DalException;

class RequisitionCostAllocationsEndpoint extends Endpoint {
  protected $processedResults = [];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('rental/requisitioncostallocations');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
  }

  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }

  /**
   * Gets a Requisition's list of Requisition Cost Allocations
   *
   * @param $data - An array of parameters
   *    Allowed values (* indicate required):
   *      id               String *
   *      codeType         String
   *        A - Allocation Codes
   *        R - Requisition Codes
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\CostAllocation
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function readAll($data) {
    $data = $this->filterParams($data, ['id']);
    $this->getAdapter()->setParams($data);

    return $this->import( $this->execute() );
  }

  /**
   * @param $data
   * @return array
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempAlloc = new CostAllocation();

    foreach ($data as $result) {
      $allocation = clone $tempAlloc;
      $allocation->import($result);

      $this->processedResults[] = $allocation;
      unset($allocation);
    }

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }

  /**
   * Update Requisition Cost Allocation
   *
   * @param $data - An array of parameters
   *    Allowed values (* indicate required):
   *      reqId            String *
   *      codeType         String *
   *        A - Allocation Codes
   *        R - Requisition Codes
   *      values           String *
   *
   * @return boolean
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function update($data) {
    $data = $this->filterParams($data, ['reqId', 'values', 'codeType'], true);
    $this->getAdapter()->setRequestBody(json_encode($data));
    $this->getAdapter()->setMethod('PUT');
    $this->execute(); // Run the call

    return $this->getAdapter()->wasSuccessful();
  }

  /**
   * Filters parameters for getting transaction cost allocations.
   *
   * @param $data - An array of parameters to pass to the endpoint
   * @param array $required - Array of all required parameters by the endpoint
   * @param bool $onlyReturnRequired - Only returns the required fields
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  protected function filterParams($data, array $required, $onlyReturnRequired = false) {
    $data = parent::filterParams($data, $required, $onlyReturnRequired);

    $data = (object) $data;

    if (empty($data->transSeqNum)) {
      $data->transSeqNum = 0;
    }

    return (array) $data;
  }
}