<?php

namespace Drupal\ur_appliance\Endpoint\DAL\File;

use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Data\DAL\File;

/**
 * Class Contracts
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class ContractsEndpoint extends Endpoint {
  protected $processedResults = [];
  protected static $requiredFields = ['id'];
  
  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('file/contracts');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
  }
  
  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }
  
  /**
   * @param array $data - An array of parameters
   *   Allowed values (* indicate required):
   *    id*           Contract ID
   *
   * @return array|mixed
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function read($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to get Contract Files without data.');
    }
    
    $data = $this->filterParams($data, self::$requiredFields);
    $this->getAdapter()->setParams($data);
    
    return $this->import( $this->execute() );
  }
  
  /**
   *  Create Contract Files data objects from the returned response.
   *
   *  @param $data
   *    The response object
   *  @return mixed|array<Contracts> data object
   *    Processed data objects
   *
   * @throws \Exception
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempFile = new File();
    
    if (empty($data)) {
      throw new DalException("There was no data passed.");
    }

    $tempFile->import($data);
    $this->processedResults[] = $tempFile;
    unset($tempFile);

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}