<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Data\DAL\Item;
use Drupal\ur_appliance\Exception\NoResultsException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Exception\DataException;

/**
 * Class Equipment
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class EquipmentEndpoint extends Endpoint {
  protected $processedResults = [];
  protected static $requiredFields = ['accountId'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/equipment');
    $this->cacheToUser = true;

    $this->allowedFields = ['accountId', 'jobId', 'catClass', 'alias', 'equipmentId', 'urEquipmentId', 'catalogGroup', 'description', 'includeDisabled'];
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
  }

  /**
   * Gets one's equipment details.
   *
   * @param array $fields
   *   When sending multiple, you can send as an array or comma-separated string.
   *
   *   Possible fields/parameters:
   *     Field            - Required - Type     - Allow Multiple? - Description
   *     -----------------------------------------------------------------------
   *     accountId        - Yes      - Int      - Yes
   *     jobId            - No       - String   - Yes
   *     catClass         - No       - String   - Yes
   *     alias            - No       - String   - Yes - Customer assigned alias
   *     equipmentId      - No       - String   - Yes
   *     urEquipmentId    - No       - String   - Yes
   *     catalogGroup     - No       - String   - No
   *     description      - No       - String   - No
   *     includeDisabled  - No       - Bool     - No - Flag to include Disabled Equipment
   *
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Account
   *
   * @throws DalException|EndpointException|DalDataException
   */
  public function read(array $fields = []) {
    $fields = $this->filterAllowedParams($fields, self::$requiredFields);
    $this->getAdapter()->setParams($fields);

    return $this->import( $this->execute() );
  }

  /**
   * Imports all results from a result set.
   *
   * @param $data
   * @return array<Item> - Returns an array of item objects.
   *
   * @throws DalException|DalDataException|EndpointException
   * @throws DataException|\Exception|NoResultsException
   */
  public function import($data) {
    if (empty($data)) {
      throw new EndpointException('Call failed. No data returned.');
    }

    if ($data->status == 1 && $data->messageText == 'No Records Found') {
      throw new NoResultsException('No data returned.');
    }

    $data = (array) $data->result;
    $tempItem = new Item();

    if (empty($data)) {
      throw new DalException('There was no data passed.');
    }

    /**
     * @var int $accountId
     * @var array $accountItems
     */
    foreach ($data AS $accountId => $accountItems) {
      foreach ($accountItems as $result) {
        $item = clone $tempItem;
        $item->import($result);

        $this->processedResults[] = $item;
        unset($item);
      }
    }

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}