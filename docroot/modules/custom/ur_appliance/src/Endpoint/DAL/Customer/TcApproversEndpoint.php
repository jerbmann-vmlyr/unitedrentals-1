<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Data\DAL\Approver;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class TcApprovers
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class TcApproversEndpoint extends Endpoint {
  protected $processedResults = [];

  protected static $requiredFields = ['accountId'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/tcapprovers');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
    $this->getAdapter()->shouldNotForceWORSToken();
  }

  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }

  /**
   * Gets approver's details.
   *
   * @param $accountId
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Approver
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function read($accountId) {
    if (empty($accountId)) {
      throw new DalException('You must provide an ID to get an approver.');
    }

    $this->getAdapter()->addParameter('accountId', $accountId);
    return $this->import( $this->execute() );
  }

  /**
   * @param $data
   * @return array|mixed
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempUser = new Approver();

    foreach ($data as $result) {
      $approver = clone $tempUser;
      $approver->import($result);

      $this->processedResults[] = $approver;
      unset($approver);
    }

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}