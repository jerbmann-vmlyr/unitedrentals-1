<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Data\DAL\CreditCard;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\DAL\DalDataException;

/**
 * Class CreditCards
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class CreditCardsEndpoint extends Endpoint {
  protected $processedResults = [];
  protected static $requiredFields = ['cardHolder', 'ccType', 'ccNumber', 'expireDate', 'cardName', 'defaultCard'];
  protected static $types = ['american express' => 'A', 'visa' => 'V', 'discover' => 'D', 'mastercard' => 'M'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/creditcards');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
    $this->getAdapter()->shouldNotForceWORSToken();
  }

  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }

  /**
   * Gets credit card's details.
   *
   * @param $cardId
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\CreditCard
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function read($cardId) {
    if (empty($cardId)) {
      throw new DalException('You must provide a card ID to get credit cards.');
    }

    $this->getAdapter()->addParameter('id', $cardId);
    return $this->importSingle($this->execute(), $cardId);
  }

  /**
   * Gets all credit card details.
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function readAll() {
    return $this->import($this->execute());
  }

  /**
   * Creates a new credit card from given data.
   *
   * @param $data
   * @return boolean
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function create($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to create a credit card without data.');
    }

    $data = $this->filterParams($data, self::$requiredFields, true);
    $this->getAdapter()->setRequestBody(json_encode($data));
    $this->getAdapter()->setMethod('POST');
    $result = $this->execute();

    if (empty($result->result->cardId)) {
      throw new DalException('Adding Credit Card failed. Please check data.');
    }

    return $result->result->cardId;
  }

  /**
   * Updates a credit card from given data.
   *
   * @param $data
   * @return null|mixed
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function update($data) {
    $updateRequiredFields = ['cardId', 'expireDate', 'cardName', 'defaultCard', 'cardHolder'];

    /**
     * Set allowed fields on the endpoint so that when it calls the parent
     * filterAllowedParams method, it actually returns data
     */
    $this->allowedFields = $updateRequiredFields;

    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to update a credit card without data.');
    }

    $data = parent::filterAllowedParams($data, $updateRequiredFields, TRUE);

    $this->getAdapter()->setMethod('PUT');
    $this->getAdapter()->setParams($data);

    $result = $this->execute();

    if (isset($result->status) && $result->status == 0) {
      return $result->request->cardId;
    }

    return $result->messageText;
  }

  /**
   * Deletes an existing card.
   *
   * @param int $cardId
   * @return bool|null
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function delete($cardId) {
    if (empty($cardId)) {
      throw new DalException('Unable to remove credit card without card ID.');
    }

    //Verify the card exists first
    $cards = $this->readAll();
    $found = false;

    /** @var array<CreditCard> $card */
    foreach ($cards AS $card) {
      if ($card->getId() == $cardId) {
        $found = true;
        $updateCard = $card->export();
        break;
      }
    }

    if (!$found) {
      throw new DalException("Card with ID: {$cardId} does not already exist. And cannot be removed.");
    }

    $updateCard['action'] = 'deactivate';
    $updateCard['cardId'] = $updateCard['id'];
    $updateCard['defaultCard'] = 'N';
    $this->getAdapter()->setMethod('PUT');
    $this->getAdapter()->setParams($updateCard);
    $this->execute();

    if ($this->getAdapter()->wasSuccessful()) {
      return true;
    }

    return null;
  }

  /**
   * Filters parameters for creating new credit cards.
   *
   * @param $data - An array of parameters to pass to the endpoint
   * @param array $required - Array of all required parameters by the endpoint
   * @param bool $onlyReturnRequired - Only returns the required fields
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  protected function filterParams($data, array $required, $onlyReturnRequired = false) {
    $data = (array)$data;

    // For POSTing credit cards we need to submit a "ccType",
    if (isset($data['cardType']) && !empty($data['cardType'])) {
      $data['ccType'] = $data['cardType'];
      unset($data['cardType']);
    }

    if (isset($data['defaultCard'])) {
      // DAL only accepts defaultCard boolean as "Y" or "N".
      $data['defaultCard'] = $data['defaultCard'] == TRUE ? "Y" : "N";
    }

    if (isset($data['ccNumber'])) {
      // Cannot include dashes or anything other than the numbers.
      $data['ccNumber'] = preg_replace('/[^\d]/', '', $data['ccNumber']);
    }

    $data = parent::filterParams($data, $required, $onlyReturnRequired);

    return (array)$data;
  }

  /**
   * Imports just one credit card.
   *
   * @param $data - From the execution result
   * @param $cardId
   * @return null|\Drupal\ur_appliance\Data\DAL\CreditCard
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function importSingle($data, $cardId) {
    $result = $this->import($data);

    /** @var CreditCard $card */
    foreach ($result as $card) {
      if ($card->getId() == $cardId) {
        return $card;
      }
    }

    return null;
  }

  /**
   * Imports all results from a result set.
   *
   * @param $data
   * @return array<CreditCard> - Returns an array of credit card objects.
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function import($data) {
    $data = (array)$data->result;
    $tempCard = new CreditCard();

    foreach ($data as $result) {
      $card = clone $tempCard;
      $card->import($result);

      $this->processedResults[] = $card;
      unset($card);
    }

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}