<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Data\DAL\Quote as QuoteStatusData;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class Quotes
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class QuotesEndpoint extends Endpoint {
  protected $processedResults = [];

  protected static $requiredFields = ['guid'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/orderstatus');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceWORSToken();
  }

  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }

  /**
   * Gets Quotes details.
   *
   * @param array $data
   * @return array
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   *
   * Allowed values (* indicate required):
   *  guid *
   *  accountId
   *  startRecord
   *  maxRecords
   *  startDate
   *  createdDate
   *  createdDate_filter
   *  jobId
   *  jobName
   *  jobName_filter
   *  transId
   *  sortBy
   *  sortOrder
   *  status
   *  openQuotes
   *  openRsvReq
   *    openQuotes and openRevReq are not required, but are necessary for
   *    filtering to show only quotes and only reservations. Parameter values
   *    for openQuotes and openRsvReq are "I" to include (which is the default)
   *    and "E" for exclude.
   */
  public function read($data) {
    if (empty($data)) {
      throw new DalException('You must provide GUID to get Orders status detail');
    }

    $data = $this->filterParams($data, self::$requiredFields);
    $this->getAdapter()->setParams($data);

    return $this->import( $this->execute() );
  }

  /**
   * Imports all results from a result set.
   *
   * @param $data
   * @return array <QuoteStatus> - Returns an array of quoteStatus detail.
   * @throws DalException
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempQuoteStatus = new QuoteStatusData();

    if (empty($data)) {
      throw new DalException("There was no data passed.");
    }

    foreach ($data as $result) {
      $quoteStatus = clone $tempQuoteStatus;
      $quoteStatus->import($result);

      $this->processedResults[] = $quoteStatus;
      unset($quoteStatus);
    }

    return $this->processedResults;
  }

  /**
   * Export data to a DAL request format.
   *
   * @param array|object - The object or array of objects being sent to the UR
   * @return mixed
   */
  public function export($data) {
    // TODO: Implement export() method.
  }
}