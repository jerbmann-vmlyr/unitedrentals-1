<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Data\DAL\AllocationCode;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Exception\DAL\DalException;

class AllocationRequisitionCodeTitlesEndpoint extends Endpoint {
  protected $processedResults = [];

  protected static $requiredFields = ['accountId'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/allocationrequisitioncodetitles');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
    $this->getAdapter()->shouldNotForceWORSToken();
  }

  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }

  /**
   * Gets allocation requisition code titles for an account.
   *
   * @param $data - An array of parameters
   *    Allowed values (* indicate required):
   *      accountId        String *
   *      codeType         String
   *        A - Allocation Codes
   *        R - Requisition Codes
   *
   * @return array<\Drupal\ur_appliance\Data\DAL\AllocationRequisitionCodeTitle>
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function readAll($data) {
    $data = $this->filterParams($data, ['accountId']);
    $this->getAdapter()->setParams($data);

    return $this->import( $this->execute() );
  }

  /**
   * Imports all results from a result set.
   *
   * @param $data
   * @return array<\Drupal\ur_appliance\Data\DAL\AllocationRequisitionCodeTitle>
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempCode = new AllocationCode();

    foreach ($data as $result) {
      $code = clone $tempCode;
      $code->import($result);

      $this->processedResults[] = $code;
      unset($code);
    }

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}