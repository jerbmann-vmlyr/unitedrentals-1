<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class ValidatePins
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class ValidatePinsEndpoint extends Endpoint {
  protected $processedResults = [];
  protected static $requiredFields = ['accountId', 'pin'];
  
  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/validatepins');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
  }
  
  /**
   * @param $data - An array of parameters
   *    Allowed values (* indicate required):
   *      accountId          Integer
   *      Pin                String
   *
   * @return string
   * @throws EndpointException|DalException|\Exception
   */
  public function read($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to validate pins without data.');
    }
  
    $data = $this->filterParams($data, self::$requiredFields);
    $this->getAdapter()->setParams($data);

    return $this->import( $this->execute() );
  }

  /**
   * Imports the data from a call to the API.
   *
   * @param $data
   * @return array|mixed
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function import($data) {
    $data = (array) $data->result;
    
    if (!empty($data['code'])) {
      $data = $data['code'];
    }
    else {
      throw new DalException('Invalid response from DAL');
    }
    
    return $data;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}