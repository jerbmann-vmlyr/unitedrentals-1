<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Exception\DAL\DalException;

/**
 * Class TcAccounts
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class TcAccountsEndpoint extends Endpoint {
  protected $processResults = [];
  protected static $requiredFields = ['accountId', 'guid'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/tcaccounts');
  }

  public function runEndpointSpecificFunctions() {
    /**
     * Methods in the DalAdapter
     * @TODO - May need to re-evaluate how we are doing this here as these are
     *         specific to the DAL Adapter.
     */
    $this->getAdapter()->shouldForceWORSToken();
    $this->getAdapter()->requireGuid();
  }

  public function read($data) {
    // TODO: Implement read() method.
  }

  /**
   * Links an account to a user.
   *
   * @param $data - An array of parameters
   *    Allowed values (* indicate required):
   *      accountId          String *
   *      guid               String *
   *
   * @return boolean
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function post($data) {
    if (empty($data['accountId'])) {
      throw new DalException('The account ID is required to link an account.');
    }

    if (empty($data['guid'])) {
      /**
       * @TODO - May need to re-evaluate how we are loading the guid.
       */
      $guidResult = $this->getAdapter()->getGuid();

      if ($guidResult) {
        $data['guid'] = (string) $guidResult;
      }
      else {
        throw new DalException('Unable to load the GUID and none has been provided.');
      }
    }

    $this->getAdapter()->addParameter('accountId', $data['accountId']);
    $this->getAdapter()->addParameter('guid', $data['guid']);
    $this->getAdapter()->setMethod('POST');
    $this->execute(); // Run the call
    return $this->getAdapter()->wasSuccessful();
  }

  /**
   * Links an account to a user upon account creation.
   *
   * @param $accountId
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function link($accountId, $userGuid = NULL) {
    if (empty($accountId)) {
      throw new DalDataException("Cannot link a an account with a TC account without an account ID.");
    }

    if (empty($userGuid)) {
      $userGuid = (string) $this->getAdapter()->getGuid();
    }

    if (empty($userGuid)) {
      throw new DalDataException('Unable to retrieve guid for linking TC account.');
    }

    $data = [
      'accountId' => $accountId,
      'guid' => $userGuid,
    ];

    $this->getAdapter()->setRequestJsonParams($data);
    $this->getAdapter()->setMethod('POST');
    $this->execute();
    return $this->getAdapter()->wasSuccessful();

  }

  /**
   * Deletes 1 to n accounts from a user.
   *
   * @param $data
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function delete($data) {
    if (empty($data['guid'])) {
      /**
       * @TODO - May need to re-evaluate how we are loading the guid.
       */
      $guidResult = $this->getAdapter()->getGuid();

      if ($guidResult) {
        $data['guid'] = (string) $guidResult;
      }
      else {
        throw new DalException('Unable to load the GUID and none has been provided.');
      }
    }

    $this->getAdapter()->setRequestJsonParams($data);
    $this->getAdapter()->setMethod('POST');
    $this->execute();
    return $this->getAdapter()->wasSuccessful();
  }

  public function import($data) {
    // TODO: Implement import() method.
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}