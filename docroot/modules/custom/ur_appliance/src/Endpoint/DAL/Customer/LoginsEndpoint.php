<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class Logins
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class LoginsEndpoint extends Endpoint {
  protected static $requiredFields = ['login', 'password'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/logins');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceWORSToken();
  }

  public function read($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to login without username and password.');
    }

    $data = $this->filterParams($data, self::$requiredFields);
    $this->getAdapter()->setParams($data);

    return $this->import( $this->execute() );
  }

  /**
   * Creates Login Response from the returned response.
   *
   * @param $data
   *    The response object
   * @return mixed|array<Rate> data objects
   *    Processed data objects
   *
   * @throws \Exception
   */
  public function import($data) {
    if (isset($data->status)) {
      return $this->getAdapter()->wasSuccessful();
    }

    throw new DalException('The login call failed.');
  }

  /**
   * @param $data
   * @return mixed
   */
  public function export($data) {
    // @TODO: Implement export() method.
  }
}