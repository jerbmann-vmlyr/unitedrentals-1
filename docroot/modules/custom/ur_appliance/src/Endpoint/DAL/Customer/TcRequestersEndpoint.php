<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Data\DAL\Requester;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class TcRequesters
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class TcRequestersEndpoint extends Endpoint {
  protected $processedResults = [];

  protected static $requiredFields = ['accountId'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/tcrequesters');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
    $this->getAdapter()->shouldNotForceWORSToken();
  }

  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }

  /**
   * Gets requester's details.
   *
   * @param $accountId
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Requester
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function read($accountId) {
    if (empty($accountId)) {
      throw new DalException('You must provide an ID to get an requester.');
    }

    $this->getAdapter()->addParameter('accountId', $accountId);
    return $this->import( $this->execute() );
  }

  /**
   * @param $data
   * @return array|mixed
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempUser = new Requester();

    foreach ($data as $result) {
      $requester = clone $tempUser;
      $requester->import($result);

      $this->processedResults[] = $requester;
      unset($requester);
    }

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}