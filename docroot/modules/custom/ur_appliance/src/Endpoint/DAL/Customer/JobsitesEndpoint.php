<?php
namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Data\DAL\Jobsite;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class Jobsites
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class JobsitesEndpoint extends Endpoint {
  protected $processedResults;

  protected static $requiredFields = ['accountId'];
  protected static $requiredPOSTFields = ['accountId', 'name', 'address1', 'city', 'state', 'zip', 'contact', 'mobilePhone'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/jobsites');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
  }

  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }

  /**
   * Gets jobsite details.
   *
   * @param array $data
   * @return array
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   *
   * Allowed values (* indicate required):
   * Note: Either uid or accountId must be passed
   *  uid*
   *  accountId*            Customer Account Number
   *  id                    Jobsite id
   *  id_filter             Id filter type
   *  customerJobId         Customer Job Site Id
   *  customerJobId_filter  Customer Job Site ID filter type
   *  name                  Name
   *  name_filter           Name filter type
   *  address               Street Address
   *  address_filter        Address filter type
   *  city                  City
   *  city_filter           City filter type
   *  state                 State
   *  zip                   Zip
   *  status                Status
   *  limit                 Row Limit
   *  startRecord           Start Record Number
   *  maxRecords            Max Number of Records
   *  onlyJobsInUse         Only JobSites in Use (on open contracts)
   *  format                Result Format
   */
  public function read($data) {
    if (empty($data)) {
      throw new DalException('You must provide an account ID to get jobsites');
    }

    $data = $this->filterParams($data, self::$requiredFields);
    $this->getAdapter()->setParams($data);

    return $this->import( $this->execute() );
  }

  public function import($data) {
    $data = (array) $data->result;
    $tempJobsite = new Jobsite();

    if (empty($data)) {
      throw new DalException('There was no data passed.');
    }

    foreach ($data as $result) {
      $jobsite = clone $tempJobsite;
      $jobsite->import($result);

      $this->processedResults[] = $jobsite;
      unset($jobsite);
    }

    return $this->processedResults;
  }

  public function importPost($data) {
    $jobsite = new Jobsite();

    if (empty($data)) {
      throw new DalException('There was no data given.');
    }

    $jobsite->import($data->request);
    $this->processedResults = $jobsite;

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }

  public function filterParams($data, array $required, $onlyReturnRequired = FALSE) {
    $data = parent::filterParams($data, $required, $onlyReturnRequired);

    $defaults = [
      'status' => 'A',
      'limit' => 9999,
    ];

    // The actual data overrides any defaults set here.
    $data = array_merge($defaults, $data);

    return $data;
  }

  /**
   * Creates a new jobsite/ Update an existing one from given data.
   * Depends on the id that posts from the form, method will be updated.
   * Data should include id for both methods, PUT and POST
   *
   * @param $data, $method
   * @return null
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function createOrUpdate($data, $method) {
    $data = $this->filterPOSTParams($data, self::$requiredPOSTFields);

    // Set parameters and proper method
    $this->getAdapter()->setRequestBody(json_encode($data));
    $result = NULL;

    if ($method == 'PUT' || $method == 'POST') {
      $this->getAdapter()->setMethod($method);
      $result = $this->execute();
    }
    else {
      throw new DalException('Invalid method passed.');
    }

    if (!empty($result->request->id)) {
      return $result->request->id;
    }

    if (!empty($result->request->jobId)) {
      return $result->request->jobId;
    }

    throw new EndpointException('Unable to retrieve Jobsite ID from response.');
  }

  public function create($data) {
    $data = $this->filterPOSTParams($data, self::$requiredPOSTFields);

    $this->getAdapter()->setMethod('POST');
    $this->getAdapter()->setRequestJsonParams($data);

    return $this->importPost( $this->execute() );
  }
  
  public function update($data) {
    $order = new Jobsite();
    $order->import($data);
    $postData = $order->postExport();
    
    $data = $this->filterPOSTParams($postData, self::$requiredPOSTFields);
    
    $this->getAdapter()->setMethod('PUT');
    $this->getAdapter()->setRequestJsonParams($data);
    
    return $this->importPost( $this->execute() );
  }

  /**
   * Validate and format the passed in data for submission to the POST
   *
   * @param $data - Submitted data
   * @param array $required - array of required parameters for the endpoint
   * @param bool $onlyReturnRequired
   *
   * @return array - formatted array
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  protected function filterPOSTParams($data, array $required, $onlyReturnRequired = false) {
    $postData = parent::filterParams($data, $required, $onlyReturnRequired);

    foreach ($postData AS $idx => $val) {
      if (empty($val)) {
        unset($postData[$val]);
      }
      else if ($idx == 'mobilePhone') {
        // Clean up the number
        $phone = preg_replace( '/[^0-9]/', '', trim($val));

        // Now add dashes since the DAL is retarded
        $phone = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '$1-$2-$3', $phone);

        $postData[$idx] = $phone;
      }
    }

    return $postData;
  }
}