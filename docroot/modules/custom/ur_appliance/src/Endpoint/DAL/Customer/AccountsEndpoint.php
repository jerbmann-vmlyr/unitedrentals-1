<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Data\DAL\Account;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\DAL\DalDataException;

/**
 * Class Accounts
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class AccountsEndpoint extends Endpoint {
  protected $processedResults = [];
  protected static $requiredFields = ['name', 'address1', 'city', 'state', 'zip', 'phone'];
  protected static $requiredPUTFields = ['accountId', 'name', 'address1', 'city', 'state', 'zip', 'phone', 'email'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/accounts');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
  }

  /**
   * Gets one account's details.
   *
   * @param $accountId
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Account
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function read($accountId) {
    if (empty($accountId)) {
      throw new DalException('You must provide an account ID to get accounts.');
    }

    $this->getAdapter()->addParameter('id', $accountId);
    return $this->importSingle( $this->execute(), $accountId );
  }

  /**
   * Gets all account details.
   *
   * @param $accountIds
   * @return array|mixed
   * @throws DalException|DalDataException|\Exception
   */
  public function readAll($accountIds) {
    if (empty($accountIds)) {
      throw new DalException('You must provide an account ID to get accounts.');
    }

    $this->getAdapter()->addParameter('id', $accountIds);
    return $this->import( $this->execute() );
  }

  /**
   * Creates a new account from given data.
   *
   * @param $data
   * @return null
   * @throws DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
   public function create($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to create a new account without data.');
    }

    $data = $this->filterParams($data, self::$requiredFields);
    $this->getAdapter()->setMethod('POST');
    $this->getAdapter()->setParams($data);

    $result = $this->execute();

    if (isset($result->result->accountId)) {
      return $result->result->accountId;
    }

    return null;
  }

  /**
   * Updates an account's info
   *
   * @param $data
   * @return bool
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function update($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to update account without data.');
    }

    $data = $this->filterParams($data, self::$requiredPUTFields);
    $this->getAdapter()->setMethod('PUT');
    $this->getAdapter()->setParams($data);

    $this->execute();

    return $this->getAdapter()->wasSuccessful();
  }

  /**
   * Filters parameters for creating new accounts.
   *
   * @param $data - An array of parameters to pass to the endpoint
   * @param array $required - Array of all required parameters by the endpoint
   * @param bool $onlyReturnRequired - Only returns the required fields
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  protected function filterParams($data, array $required, $onlyReturnRequired = false) {
    $data = (object) parent::filterParams($data, $required, $onlyReturnRequired);
    $data->rpp = 'C'; // Set the RPP default to "C" for companies.

    if (!empty($data->phone)) {
      $data->phone = preg_replace('~.*(\d{3})[^\d]*(\d{3})[^\d]*(\d{4}).*~', '$1-$2-$3', $data->phone);
    }

    if (!empty($data->individual)) {
      $data->rpp = 'N'; // Individuals are forced to N.
    }

    if (!empty($data->zip)) {
      $data->zip = strtoupper($data->zip);
    }

    return (array) $data;
  }

  /**
   * Imports just one account.
   *
   * @param $data - From the execution result
   * @param $accountId
   * @return null|\Drupal\ur_appliance\Data\DAL\Account
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function importSingle($data, $accountId) {
    $result = $this->import($data);

    /** @var Account $account */
    foreach ($result as $account) {
      if ($account->getId() == $accountId) {
        return $account;
      }
    }

    return null;
  }

  /**
   * Imports all results from a result set.
   *
   * @param $data
   * @return array<Account> - Returns an array of account objects.
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempAccount = new Account();

    foreach ($data as $result) {
      $account = clone $tempAccount;
      $account->import($result);

      $this->processedResults[$account->getId()] = $account;
      unset($account);
    }

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}