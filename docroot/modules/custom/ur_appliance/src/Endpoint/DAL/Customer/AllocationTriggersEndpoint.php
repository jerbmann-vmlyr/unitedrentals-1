<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Data\DAL\CodeTrigger;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Exception\DAL\DalException;

class AllocationTriggersEndpoint extends Endpoint {
  protected static $requiredFields = ['accountId', 'codeType'];
  protected $processedResults = [];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/allocationtriggers');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
    $this->getAdapter()->shouldNotForceWORSToken();
  }

  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }

  /**
   * Gets allocation triggers for an account.
   *
   * @param $data - An array of parameters
   *    Allowed values (* indicate required):
   *      accountId        String *
   *      codeType         String *
   *        A - Allocation Codes
   *        R - Requisition Codes
   * @return array
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function readAll($data) {
    $data = $this->filterParams($data, self::$requiredFields);
    $this->getAdapter()->setParams($data);

    return $this->import( $this->execute() );
  }

  /**
   * Imports all results from a result set.
   *
   * @param $data
   * @return array|mixed
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function import($data) {
    $data = (array) $data->result;
    $tempCodeValue = new CodeTrigger();

    foreach ($data as $result) {
      $codeValue = clone $tempCodeValue;
      $codeValue->import($result);

      $this->processedResults[] = $codeValue;
      unset($codeValue);
    }

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}