<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DataException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class AdAccounts
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class AdAccountsEndpoint extends Endpoint {
  protected static $requiredPUTFields = ['id'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/adaccounts');
  }

  /**
   * Returns the user guid.
   * @return mixed
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function read() {
    if (empty(\Drupal::currentUser()->getEmail())) {
      throw new DalException('You must be logged in to load Ad Account Info.');
    }

    $email = \Drupal::currentUser()->getEmail();
    $this->getAdapter()->addParameter('id', $email);

    return $this->import( $this->execute() );
  }

  /**
   * @return mixed
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function readGuid() {
    $data = $this->read();

    if (!isset($data->guid)) {
      throw new DalException('Unable to load the user GUID.');
    }

    return $data->guid;
  }

  /**
   * Updates the customer's AD account
   * @param $data
   */
  public function update($data) {
    $this->filterPUTParams($data, self::$requiredPUTFields);
    $this->getAdapter()->shouldForceUserToken();
    $this->getAdapter()->setMethod('PUT');
    $this->getAdapter()->setRequestBody(json_encode($data));
    $result = $this->execute();

    if ($this->getAdapter()->wasSuccessful()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Need to validate we have the required "id" field and at least one
   * of the following:
   *
   * "email" - string, email
   * "password" - 1 uppercase, 1 number
   * "disabled" - boolean
   * @param $data
   * @param array $required
   * @param bool $onlyReturnRequired
   * @return array
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  function filterPUTParams($data, array $required, $onlyReturnRequired = FALSE) {
    // Validate with parent method first. This ensures we have the "id"
    $data = parent::filterParams($data, $required, $onlyReturnRequired);
    $missing_param = TRUE;

    if (isset($data['email']) && !empty($data['email'])) {
      $missing_param = FALSE;
      if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
        throw new DataException("Provided {$data['email']} is not a valid email address.");
      }
    }

    if (isset($data['password']) && !empty($data['password'])) {
      $missing_param = FALSE;

      if (strlen($data['password']) < 8) {
        throw new DataException("Password must be at least 8 characters.");
      }

      /*
       * Ensure password is:
       * - At least 8 characters
       * - Contains at least 1 uppercase
       * - Contains at least 1 number
       */
      $pattern = '/^\S*(?=\S{8,})(?=\S*[A-Z])(?=\S*[\d])\S*$/';
      if (!preg_match($pattern, $data['password'])) {
        throw new DataException("Password must contain at least 1 uppercase, 1 number, and be at least 8 characters long.");
      }

    }

    if (isset($data['disabled']) && !empty($data['disabled'])) {
      $missing_param = FALSE;
      if (!filter_var($data['disabled'], FILTER_VALIDATE_BOOLEAN)) {
        throw new DataException("The \"disabled\" parameter must be a boolean value.");
      }
    }

    if ($missing_param) {
      throw new DataException('Must provide email, and/or password, and/or disabled parameter.');
    }

    return $data;
  }

  /**
   * @param $data
   * @return mixed
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function import($data) {
    if (is_array($data)) {
      $data = (object) $data;
    }

    if (!isset($data->result)) {
      throw new DalException('Unable to load the result.');
    }

    return $data->result;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }
}