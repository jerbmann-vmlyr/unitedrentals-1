<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Data\DAL\TcUser;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class TcUsers
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class TcUsersEndpoint extends Endpoint {
  protected $processedResults = [];

  protected static $requiredFields = ['id'];
  protected static $requiredPostFields = ['email', 'firstName', 'lastName', 'phone'];
  protected $useWORSToken = FALSE;

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('customer/tcusers');
  }

  public function runEndpointSpecificFunctions() {
    if ($this->useWORSToken) {
      $this->getAdapter()->shouldForceWORSToken();
    }
    else {
      $this->getAdapter()->shouldForceUserToken();
    }
  }

  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }

  /**
   * Gets TC User details.
   *
   * @param $id
   *   The TC User's email address.
   * @param $useAltAuth
   *   Determines whether or not to bypass user authentication.
   *
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\TcUser
   *
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function read($id, $useAltAuth = FALSE) {
    if (empty($id)) {
      throw new DalException('You must provide an ID to get a tcuser.');
    }

    if ($useAltAuth == TRUE) {
      $this->useWORSToken = TRUE;
    }

    $this->getAdapter()->addParameter('id', $id);
    return $this->importSingle( $this->execute(), $id );
  }

  /**
   * Imports just one result.
   *
   * @param $data - From the execution result
   * @param $id - Email address of TcUser
   *
   * @return null|\Drupal\ur_appliance\Data\DAL\TcUser
   *
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function importSingle($data, $id) {
    $result = $this->import($data);

    /** @var TcUser $account */
    foreach ($result as $account) {
      // Need to check lower case as the DAL does not store uppercase
      if ($account->getId() == strtolower($id)) {
        return $account;
      }
    }

    return null;
  }

  /**
   * Post TcUser.
   *
   * @param $data - An array of parameters
   *    Allowed values (* indicates required):
   *      email       String *
   *      firstName   String *
   *      lastName    String *
   *      phone       String *
   *      pswdExpDays Integer
   *      language    String
   *
   * @return string
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function postTcUser($data) {
    if (empty($data['pswdExpDays'])) {
      $data['pswdExpDays'] = '90'; // Default Expiration days = 90 days
    }

    if (empty($data['phone'])) {
      $data['phone'] = '555-555-5555';
    }

    $data = $this->filterPOSTParams($data, self::$requiredPostFields, false);

    // Set parameters and proper method
    $this->getAdapter()->setRequestBody(json_encode($data));
    /*
     * There's no scenario that requires a user to use their own
     * auth token to create a TC User, so force using WORS Token when
     * creating TC Users. See runAdapterSpecificFunctions() above.
     */
    $this->useWORSToken = TRUE;
    $this->getAdapter()->setMethod('POST');
    $result = $this->execute(); // Run the call

    return $result->result->guid;
  }

  /**
   * Updates a TcUser from given data.
   *
   * @param $data
   * @return bool|null|\Drupal\ur_appliance\Data\DAL\TcUser
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function updateTcUser($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to update a TcUser without data.');
    }

    $data = $this->filterParams($data, self::$requiredFields);
    $this->getAdapter()->setMethod('PUT');
    $this->getAdapter()->setRequestBody(json_encode($data));

    return $this->importPutSingle( $this->execute(), $data['id'] );
  }

  /**
   * Imports guid for updating TcUser.
   *
   * @param $data - From the execution result
   * @param $id
   *
   * @return null|bool|\Drupal\ur_appliance\Data\DAL\TcUser
   *
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function importPutSingle($data, $id) {
    $result = $this->import($data);

    /** @var TcUser $account */
    foreach ($result as $account) {
      if ($account->getGuid() == $id) {
        return $account;
      }
    }

    return $this->getAdapter()->wasSuccessful();
  }

  /**
   * @param $data
   * @return array|mixed
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function import($data) {
    if (!$this->getAdapter()->wasSuccessful()) {
      throw new EndpointException($data->messageText);
    }

    $data = (array) $data->result;
    $tempUser = new TcUser();

    foreach ($data as $result) {
      $tcuser = clone $tempUser;
      $tcuser->import($result);

      $this->processedResults[] = $tcuser;
      unset($tcuser);
    }

    return $this->processedResults;
  }

  public function export($data) {
    // TODO: Implement export() method.
  }

  /**
   * Validate and format the passed in data for submission to the POST
   *
   * @param $data - Submitted data
   * @param array $required - array of required parameters for the endpoint
   * @param bool $onlyReturnRequired
   *
   * @return array - formatted array
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   * @throws \Exception
   */
  protected function filterPOSTParams($data, array $required, $onlyReturnRequired = false) {
    $tcuser = new TcUser();
    $tcuser->import($data);

    $postData = $tcuser->postExport();
    $postData = parent::filterParams($postData, $required, $onlyReturnRequired);

    // Validate all values have been supplied for the required fields
    foreach ($required as $field) {
      if (empty($postData[$field]) && $postData[$field] !== 0) {
        throw new DalDataException("{$postData[$field]} must have a value to submit a transaction.");
      }
    }

    return $postData;
  }
}