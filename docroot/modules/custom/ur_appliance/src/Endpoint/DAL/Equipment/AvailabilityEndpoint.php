<?php

namespace Drupal\ur_appliance\Endpoint\DAL\Equipment;

use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Data\DAL\AvailableEquipment;


/**
 * Class Availability
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class AvailabilityEndpoint extends Endpoint {
  protected $processedResults;
  
  protected static $requiredFields = ['catClass', 'branchId'];
  
  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('equipment/availability');
  }
  
  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceUserToken();
  }
  
  /**
   * Returns the processed results.
   *
   * @return array
   */
  public function getResults() {
    return $this->processedResults;
  }
  
  /**
   * @param $data - An array of parameters
   *    Allowed values (* indicate required):
   *      catClass*           catClass (May be comma-separated list)
   *      branchId*           Branch Number
   *      date                AvailabilityDate (default is System Date)
   *      detail              Customer Account Number
   *      multipleCatClass    If true, specifies specific RPG function to call
   *      viewType            View type used in URMAX availability screen
   *
   * @return mixed|array<AvailableEquipment> data objects
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function read($data) {
    if (empty($data) || (!is_array($data) && !is_object($data))) {
      throw new DalException('Unable to get the available equipments without data');
    }
    
    $data = $this->filterParams($data, self::$requiredFields);
    
    $data['catClass'] = implode(',', (array) $data['catClass']);
    $data['branchId'] = implode(',', (array) $data['branchId']);
    
    $this->getAdapter()->setParams($data);
    return $this->import( $this->execute() );
  }

  /**
   *  Create Availability data objects from the returned response.
   *
   *  @param $data
   *    The response object
   *  @return mixed|array<Availability> data object
   *    Processed data objects
   *
   * @throws \Exception
   */
  public function import($data) {
    if (!isset($data->result)) {
      if (empty($data)) {
        throw new EndpointException('The availability response is unexpectedly empty.');
      }

      throw new EndpointException('Unexpected response from availability.');
    }

    $data = (array) $data->result;
    $availableEquipment = new AvailableEquipment();
    
    if (empty($data)) {
      throw new DalException('There was no data passed.');
    }

    foreach ($data as $result) {
      $availability = clone $availableEquipment;
      $availability->import($result);

      $this->processedResults[] = $availability;
      unset($availability);
    }

    return $this->processedResults;
  }
  
  /**
   * @param $data
   * @return mixed
   */
  public function export($data) {
    // TODO: Implement export() method.
  }
}