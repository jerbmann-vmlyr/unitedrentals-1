<?php

namespace Drupal\ur_appliance\Endpoint\DAL;

use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class SsoLogins
 *
 * @package Drupal\ur_appliance\Endpoint\DAL
 */
class SsoLoginsEndpoint extends Endpoint {
  protected static $requiredFields = ['email'];

  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('ssologins');
  }

  public function runEndpointSpecificFunctions() {
    $this->getAdapter()->shouldForceWORSToken();
  }

  /**
   * Uses the ssologins api to log a user in via email. The endpoint is special
   * in that it doesn't return a data object, rather just returns the response to
   * the Service/SSO controller and the logic for returning the response is
   * handled there.
   *
   * @see \Drupal\ur_appliance\Controller\DAL\Service\SsoController::read()
   *
   * @param $email
   *   The email address we are going to SSO login with
   *
   * @return mixed
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function read($email) {
    $this->filterParams(['email' => $email], self::$requiredFields);
    $this->getAdapter()->addParameter('id', $email);
    $this->getAdapter()->setMethod('GET');
    return $this->import( $this->execute() );
  }

  /**
   * Handles validating parameters prior to being used in the API call.
   *
   * @param $data - An array of parameters to pass to the endpoint
   * @param array $required - Array of all required parameters by the endpoint
   * @param bool $onlyReturnRequired - Only returns the required fields
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  protected function filterParams($data, array $required, $onlyReturnRequired = false) {
    $data = parent::filterParams($data, $required, $onlyReturnRequired);

    // Make sure we have a valid email.
    if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
      throw new DalException(
        'Email, "' . $data['email'] . '" is not a valid email address.'
      );
    }

    return (array) $data;
  }

  /**
   * @param $data
   * @return mixed
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function import($data) {
    if (!empty($data->result->token)) {
      $token = $data->result->token;
      $this->getAdapter()->setUserToken($token);

      return $token;
    }

    if (!empty($data->messageText)) {
      throw new DalException('Error calling DAL for a token: ' . $data->messageText);
    }

    throw new DalException('No token returned from DAL.');
  }

  public function export($data) {
    // ssologins doesn't support any method other than GET
  }
}