<?php

namespace Drupal\ur_appliance\Endpoint\Placeable\v2;

use Drupal\ur_appliance\Data\Placeable\Location;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Exception\Placeable\PlaceableException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class Collection
 *
 * @see: Documentation can be found at...
 *   https://github.com/locationinsight/placeable-docs/blob/master/placeable-api-v2.md
 *
 * @package Drupal\ur_appliance\Endpoint\Placeable\v2
 */
class CollectionEndpoint extends Endpoint {
  protected $processedResults;
  protected $baseUri = 'v2/collection/57470c7a0b00002900121396';
  protected static $advancedSearchDefaults = [
    'operator' => 'equals',
    'field' => 'fields.state',
    'filter' => 'and'
  ];

  /**
   * Collection constructor.
   *
   * @see This should use the Workbench Adapter.
   *
   * @param \Drupal\ur_appliance\Interfaces\AdapterInterface $adapter
   */
  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri($this->baseUri);
  }

  /**
   * Get one specific branch back. This is the Placeable item ID, not the UR
   * branch ID.
   *
   * @param int $itemId
   * @return array|mixed
   * @throws PlaceableException|\Exception
   */
  public function read($itemId) {
    if (empty($itemId)) {
      throw new PlaceableException('Unable to get branch without item ID.');
    }

    $uriPath = $this->baseUri . "/item/$itemId"; // For reading one item we refer to "item"
    $this->setUri( $uriPath );

    return $this->importSingle( $this->execute() );
  }

  /**
   * Get back all branches.
   *
   * @return array|mixed
   * @throws PlaceableException|\Exception
   */
  public function readAll() {
    $this->setUri('/items');

    return $this->import($this->getAdapter()->execute());
  }

  /**
   * Perform a basic keyword search of the branches.
   *
   * @param $keywords
   * @param int $limit
   * @param int $offset
   * @return array|mixed
   *
   * @throws PlaceableException|\Exception
   */
  public function basicSearch($keywords, $limit = 25, $offset = 0) {
    $this->getAdapter()->setUri('/items');

    $this->getAdapter()->addParameter('q', $keywords);
    $this->getAdapter()->addParameter('limit', $limit);
    $this->getAdapter()->addParameter('offset', $offset);

    return $this->import($this->execute());
  }

  /**
   * Perform advanced searches against the Placeable database.
   *
   * @param $data
   * @param int $limit
   * @param int $offset
   * @return array|mixed
   *
   * @throws EndpointException|\Exception
   */
  public function advancedSearch($data, $limit = 25, $offset = 0) {
    $filteredData = $this->filterAndSetDefaults($data, ['value'], self::$advancedSearchDefaults);

    $body = [
      'q' => [
        [
          'operator' => $filteredData['operator'],
          'field' => $filteredData['field'],
          'value' => $filteredData['value'],
          'filter' => $filteredData['filter']
        ],
      ],
      'offset' => $offset,
      'limit' => $limit,
    ];

    $body = json_encode($body);

    $this->getAdapter()->setRequestBody($body);
    $this->getAdapter()->setMethod('POST');
    $this->setUri('search');

    try {
      $result = $this->import($this->execute());
    }
    catch (\Exception $e) {
      $result['message'] = $e->getMessage();
      $result['error'] = true;
    }

    return $result;
  }

  /**
   * Perform advanced search passing data into request body.
   *
   * @param $data
   * @return array|mixed
   */
  public function advancedSearchRaw($data) {
    $this->getAdapter()->setRequestBody($data);
    $this->getAdapter()->setMethod('POST');
    $this->setUri('search');

    try {
      $result = $this->import($this->execute());
    }
    catch (\Exception $e) {
      $result['message'] = $e->getMessage();
      $result['error'] = true;
    }

    return $result;
  }

  /**
   * Filters the parameters then sets default values.
   *
   * @param $data - An array of parameters to pass to the endpoint
   * @param array $required - Array of all required parameters by the endpoint
   * @param array $defaults - Array of allowed values and their default values
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function filterAndSetDefaults($data, array $required, array $defaults) {
    $data = (object) $data;
    $tmpData = (object) $this->filterParams($data, $required, true);

    // Build parameter list of allowed values and defaults for those not included
    foreach ($defaults AS $field => $default) {
      $tmpData->$field = $default;

      if (isset($data->$field)) {
        $tmpData->$field = $data->$field;
      }
    }

    return (array) $tmpData;
  }

  /**
   * Creates Rate data objects from the returned response.
   *
   * @param $data
   *    The response object
   * @return mixed|array<Rate> data objects
   *    Processed data objects
   *
   * @throws \Exception
   */
  public function import($data) {
    $data = (array) $data->results;
    $tempLocation = new Location();

    foreach ($data as $result) {
      $location = clone $tempLocation;
      $location->import($result);

      $this->processedResults[] = $location;
      unset($location);
    }

    return $this->processedResults;
  }

  /**
   * @param $data
   * @return array|mixed
   * @throws \Exception
   */
  public function importSingle($data) {
    $data = (array) $data;

    $location = new Location();
    $location->import($data);
    $this->processedResults[] = $location;

    return $this->processedResults;
  }

  /**
   * @param $data
   * @return mixed
   */
  public function export($data) {
    //
  }
}