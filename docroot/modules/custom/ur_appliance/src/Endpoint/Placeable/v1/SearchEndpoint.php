<?php

namespace Drupal\ur_appliance\Endpoint\Placeable\v1;

use Drupal\ur_appliance\Data\Placeable\Location;
use Drupal\ur_appliance\Endpoint\Endpoint;
use Drupal\ur_appliance\Exception\Placeable\PlaceableException;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class Search
 *
 * @TODO: Build this out. None of this is done.
 *
 * @package Drupal\ur_appliance\Endpoint\Placeable\v1
 */
class SearchEndpoint extends Endpoint {
  protected $processedResults;

  protected static $requiredFields = ['q'];

  /**
   * Search constructor.
   *
   * @see This should use the Placeable Adapter.
   *
   * @param \Drupal\ur_appliance\Interfaces\AdapterInterface $adapter
   */
  public function __construct(AdapterInterface $adapter) {
    parent::__construct($adapter);
    $this->setUri('v1/search');
  }

  /**
   * Calling the Placeable Search API.
   *
   * @param $keywords - To search for
   * @param null $sortOrder - Sort By: NameAZ, NameZA, and Distance
   * @param null $limit - Results per page
   * @param null $offset - Which page of results to show
   * @param null $filters - Additional filters to apply to search
   *
   * @return array|mixed
   *
   * @throws PlaceableException|\Exception
   */
  public function read($keywords, $sortOrder = null, $limit = null, $offset = null, $filters = null) {
    if (empty($keywords)) {
      throw new PlaceableException('Unable to get search without data.');
    }

    $allowedSorts = ['NameAZ', 'NameZA', 'Distance'];

    $params = ['q' => $keywords];

    if ($limit !== null) {
      $params['page_size'] = $limit;
    }

    if ($offset !== null) {
      $params['page'] = $offset;
    }

    if ($filters !== null) {
      $params['filters'] = $filters;
    }

    if (in_array($sortOrder, $allowedSorts, false)) {
      $params['sortOrder'] = $sortOrder;
    }

    $this->getAdapter()->setParams($params);

    // @TODO: Clean up response to return data object(s)
//    return $this->import( $this->getAdapter()->execute() );
    return $this->execute();
  }

  /**
   * Creates Location data objects from the returned response.
   *
   * @param $data
   *    The decoded json response
   * @return mixed|array<Location> data objects
   *    Processed data objects
   *
   * @throws \Exception
   */
  public function import($data) {
    $data = (array) $data->results->results;
    $tempLoc = new Location();

    foreach ($data as $result) {
      $location = clone $tempLoc;
      $location->import($result);

      $this->processedResults[] = $location;
      unset($location);
    }

    return $this->processedResults;
  }

  /**
   * Placeable only implements a GET method so no export required
   * @param $data
   * @return mixed
   */
  public function export($data) {
    //
  }
}