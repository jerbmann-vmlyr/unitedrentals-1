<?php

namespace Drupal\ur_appliance\Provider;

use Drupal\ur_appliance\Controller\DAL\EquipmentController;
use Drupal\ur_appliance\Controller\DAL\FileController;
use Drupal\ur_appliance\Controller\DAL\RentalController;
use Drupal\ur_appliance\Controller\DAL\CustomerController;
use Drupal\ur_appliance\Controller\DAL\OrderController;
use Drupal\ur_appliance\Controller\DAL\ServiceController;
use Drupal\ur_appliance\Exception\DAL\DalException;


class DAL {
  /** @var DAL - Hold an instance of the class */
  private static $instance;

  /** @var CustomerController */
  private static $customer;

  /** @var EquipmentController */
  private static $equipment;

  /** @var OrderController */
  private static $order;

  /** @var ServiceController  */
  private static $service;

  /** @var RentalController  */
  private static $rental;

  /** @var FileController */
  private static $file;

  /**
   * The resolved object instances.
   *
   * @var array
   */
  protected static $resolvedInstance;

  protected function __construct() {
    //
  }

  /**
   * -----------------------------------------
   * Beginning of Code Taken from Laravel
   * -----------------------------------------
   *
   * Took this from Laravel and used late-static
   * binding to help with this.
   */

  /**
   * Handle dynamic, static calls to the object.
   *
   * @param  string  $method
   * @param  array   $args
   * @return mixed
   *
   * @throws DalException
   */
  public static function __callStatic($method, $args) {
    $instance = static::getRoot();

    if (! $instance) {
      throw new DalException('A facade root has not been set.');
    }

    return $instance->$method(...$args);
  }

  /**
   * Get the root object behind the facade.
   *
   * @return mixed
   */
  public static function getRoot() {
    return static::resolveInstance(static::getAccessor());
  }

  /**
   * Get the registered name of the component.
   *
   * @return string
   */
  protected static function getAccessor() {
    return DAL::class;
  }

  /**
   * Resolve the facade root instance from the container.
   *
   * @param  string|object  $name
   * @return mixed
   */
  protected static function resolveInstance($name) {
    if (is_object($name)) {
      return $name;
    }

    if (isset(static::$resolvedInstance[$name])) {
      return static::$resolvedInstance[$name];
    }

    return static::$resolvedInstance[$name] = new $name;
  }

  /**
   * -----------------------------------------
   * End of Code Taken from Laravel
   * -----------------------------------------
   *
   * Took this from Laravel and used late-static
   * binding to help with this.
   */


  /**
   * The singleton method
   *
   * @return \Drupal\ur_appliance\Provider\DAL
   */
  public static function getInstance() {
    if (!isset(self::$instance)) {
      self::$instance = new DAL();
    }

    return self::$instance;
  }

  /**
   * Keeps returning one static instance of this so we don't
   * spin up a bunch of these.
   *
   * @param bool $exportResults
   * @return \Drupal\ur_appliance\Controller\DAL\CustomerController
   */
  public static function Customer($exportResults = false) {
    if (empty(self::$customer)) {
      self::$customer = new CustomerController($exportResults);
    }

    return self::$customer;
  }

  /**
   * Keeps returning one static instance of this so we don't
   * spin up a bunch of these.
   *
   * @param bool $exportResults
   * @return \Drupal\ur_appliance\Controller\DAL\EquipmentController
   */
  public static function Equipment($exportResults = false) {
    if (empty(self::$equipment)) {
      self::$equipment = new EquipmentController($exportResults);
    }

    return self::$equipment;
  }

  /**
   * Keeps returning one static instance of this so we don't
   * spin up a bunch of these.
   *
   * @param bool $exportResults
   * @return \Drupal\ur_appliance\Controller\DAL\OrderController
   */
  public static function Order($exportResults = false) {
    if (empty(self::$order)) {
      self::$order = new OrderController($exportResults);
    }

    return self::$order;
  }

  /**
   * Keeps returning one static instance of this so we don't
   * spin up a bunch of these.
   *
   * @param bool $exportResults
   * @return \Drupal\ur_appliance\Controller\DAL\ServiceController
   */
  public static function Service($exportResults = false) {
    if (empty(self::$service)) {
      self::$service = new ServiceController($exportResults);
    }

    return self::$service;
  }

  /**
   * Keeps returning one static instance of this so we don't
   * spin up a bunch of these.
   *
   * @param bool $exportResults
   * @return \Drupal\ur_appliance\Controller\DAL\RentalController
   */
  public static function Rental($exportResults = false) {
    if (empty(self::$rental)) {
      self::$rental = new RentalController($exportResults);
    }

    return self::$rental;
  }

  /**
   * Keeps returning one static instance of this so we don't
   * spin up a bunch of these.
   *
   * @param bool $exportResults
   * @return \Drupal\ur_appliance\Controller\DAL\FileController
   */
  public static function File($exportResults = false) {
    if (empty(self::$file)) {
      self::$file = new FileController($exportResults);
    }

    return self::$file;
  }
}
