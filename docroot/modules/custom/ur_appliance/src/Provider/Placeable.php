<?php

namespace Drupal\ur_appliance\Provider;

use Drupal\ur_appliance\Controller\Placeable\BranchController;
use Drupal\ur_appliance\Controller\Placeable\SearchController;
use Drupal\ur_appliance\Exception\Placeable\PlaceableException;

/**
 * Class Placeable
 *
 * @package Drupal\ur_appliance\Provider
 */
class Placeable {
  /** @var Placeable - Hold an instance of the class */
  private static $instance;

  /** @var SearchController */
  private static $search;

  /** @var BranchController */
  private static $branch;

  /**
   * The resolved object instances.
   *
   * @var array
   */
  protected static $resolvedInstance;

  protected function __construct() {
    //
  }

  /**
   * Handle dynamic, static calls to the object.
   *
   * @param  string  $method
   * @param  array   $args
   * @return mixed
   *
   * @throws PlaceableException
   */
  public static function __callStatic($method, $args) {
    $instance = static::getRoot();

    if (! $instance) {
      throw new PlaceableException('A facade root has not been set.');
    }

    return $instance->$method(...$args);
  }

  /**
   * Get the root object behind the facade.
   *
   * @return mixed
   */
  public static function getRoot() {
    return static::resolveInstance(static::getAccessor());
  }

  /**
   * Get the registered name of the component.
   *
   * @return string
   */
  protected static function getAccessor() {
    return DAL::class;
  }

  /**
   * Resolve the facade root instance from the container.
   *
   * @param  string|object  $name
   * @return mixed
   */
  protected static function resolveInstance($name) {
    if (is_object($name)) {
      return $name;
    }

    if (isset(static::$resolvedInstance[$name])) {
      return static::$resolvedInstance[$name];
    }

    return static::$resolvedInstance[$name] = new $name;
  }

  /**
   * The singleton method
   *
   * @return Placeable
   */
  public static function getInstance() {
    if (!isset(self::$instance)) {
      self::$instance = new Placeable();
    }

    return self::$instance;
  }

  /**
   * Keeps returning one static instance of this so we don't
   * spin up a bunch of these.
   *
   * @param bool $exportResults
   * @return \Drupal\ur_appliance\Controller\Placeable\SearchController
   */
  public static function Search($exportResults = false) {
    if (empty(self::$search)) {
      self::$search = new SearchController($exportResults);
    }

    return self::$search;
  }

  /**
   * Keeps returning one static instance of this so we don't
   * spin up a bunch of these.
   *
   * @param bool $exportResults
   * @return \Drupal\ur_appliance\Controller\Placeable\BranchController
   */
  public static function Branch($exportResults = false) {
    if (empty(self::$branch)) {
      self::$branch = new BranchController($exportResults);
    }

    return self::$branch;
  }
}