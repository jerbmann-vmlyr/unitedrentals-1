<?php

namespace Drupal\ur_appliance\Provider;

use Drupal\ur_appliance\Controller\Google\MapsController;
use Drupal\ur_appliance\Exception\Google\GoogleMapsException;

/**
 * Class Google
 *
 * @package Drupal\ur_appliance\Provider
 */
class Google {
  /** @var Google - Hold an instance of the class */
  private static $instance;

  /** @var MapsController */
  private static $maps;

  /**
   * The resolved object instances.
   *
   * @var array
   */
  protected static $resolvedInstance;

  protected function __construct() {
    //
  }

  /**
   * Handle dynamic, static calls to the object.
   *
   * @param  string  $method
   * @param  array   $args
   * @return mixed
   *
   * @throws GoogleMapsException
   */
  public static function __callStatic($method, $args) {
    $instance = static::getRoot();

    if (! $instance) {
      throw new GoogleMapsException('A facade root has not been set.');
    }

    return $instance->$method(...$args);
  }

  /**
   * Get the root object behind the facade.
   *
   * @return mixed
   */
  public static function getRoot() {
    return static::resolveInstance(static::getAccessor());
  }

  /**
   * Get the registered name of the component.
   *
   * @return string
   */
  protected static function getAccessor() {
    return DAL::class;
  }

  /**
   * Resolve the facade root instance from the container.
   *
   * @param  string|object  $name
   * @return mixed
   */
  protected static function resolveInstance($name) {
    if (is_object($name)) {
      return $name;
    }

    if (isset(static::$resolvedInstance[$name])) {
      return static::$resolvedInstance[$name];
    }

    return static::$resolvedInstance[$name] = new $name;
  }

  /**
   * The singleton method
   *
   * @return Google
   */
  public static function getInstance() {
    if (!isset(self::$instance)) {
      self::$instance = new Google();
    }

    return self::$instance;
  }

  /**
   * Keeps returning one static instance of this so we don't
   * spin up a bunch of these.
   *
   * @param bool $exportResults
   * @return \Drupal\ur_appliance\Controller\Google\MapsController
   */
  public static function Maps($exportResults = false) {
    if (empty(self::$maps)) {
      self::$maps = new MapsController($exportResults);
    }

    return self::$maps;
  }
}