<?php
namespace Drupal\ur_appliance\DependencyInjection;

use Drupal\Component\FileCache\FileCacheFactory;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\YamlFileLoader;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\HttpKernel\Tests\Logger;
use Symfony\Component\Yaml\Yaml;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Endpoint\DAL\Location\BranchesEndpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;

/**
 * Class Drupal\ur_applianceApiConfig
 *
 * @package Drupal\ur_appliance\DependencyInjection
 *
 * @see I wrote this because I got tired of trying to continually do DI to load
 *      the config through the Symfony mechanisms that seem to require you run
 *      your bundle through controllers instead of allowing for usage as a
 *      standalone library. Symfony makes things more complex than need be. So
 *      I solved that problem with the config with this. Will still need to test
 *      this with environment specific overrides. That will be fun.
 *
 * @TODO: Make sure this still allows us to do environment specific overrides
 *        from outside this bundle/library.
 */
class ApiContainer {
  /** @var ApiContainer - Hold an instance of the class */
  private static $instance;

  /** @var  array|\stdClass - Stored parsed config file */
  private static $parsedConfig;

  public static $serviceYamls;
  public static $serviceProviderClasses;
  public static $moduleList;

  /**
   * The singleton method
   *
   * @return \Drupal\ur_appliance\DependencyInjection\ApiContainer
   */
  public static function getInstance() {
    if (!isset(self::$instance)) {
      self::$instance = new ApiContainer();
    }

    return self::$instance;
  }

  /**
   * Handle dynamic, static calls to the object.
   *
   * @param  string  $method
   * @param  array   $args
   * @return mixed
   *
   * @throws DalException
   */
  public static function __callStatic($method, $args) {
    $instance = static::getInstance();

    if (!$instance) {
      throw new DalException('Unable to load the class instance.');
    }

    return $instance->$method(...$args);
  }

  /**
   * Gets config parameters.
   *
   * @param $varName
   * @return mixed
   * @throws InvalidArgumentException
   * @throws \Exception
   */
  public static function getParameter($varName) {
    return self::getConfig($varName);
  }

  /**
   * Gets services.
   *
   * Allows us to take advantage of Symfony service loader (and hopefully auto-
   * wiring of dependencies later).
   *
   * @param $name
   * @return mixed
   *
   * @throws InvalidArgumentException
   * @throws ServiceCircularReferenceException
   * @throws ServiceNotFoundException
   * @throws \Exception
   */
  public static function getService($name) {
    $service = null;

    if (\Drupal::hasContainer()) {
      $service = \Drupal::service($name);
    }
    else {
      // Otherwise we have to do our own DI work
      self::initializeServices();
      $service = \Drupal::service($name);
    }

    return $service;
  }

  public static function initializeServices() {
    self::initializeContainer();
  }

  public static function initializeContainer() {
    $container = new ContainerBuilder();
    FileCacheFactory::setPrefix('file_cache');

    /**
     * Manually register and set some default core definitions.
     * These definitions will already exist in Drupal::service() when we bring
     * this to Drupal.
     */
    $container->register('logger.channel_base', 'Symfony\Component\HttpKernel\Tests\Logger');
    $container->register('logger.channel.php');
    $container->register('http_client', 'GuzzleHttp\ClientInterface');

    $logger_base = new Logger();
    $container->set('logger.channel_base', $logger_base);
    $container->set('logger.channel.php', $logger_base);

    $client_interface = new Client();
    $container->set('http_client', $client_interface);

    /**
     * The YamlFileLoader takes care of registering and setting definitions on
     * the container.
     */
    $yaml_loader = new YamlFileLoader($container);
    $path = __DIR__ . '/../../api_appliance.services.yml';
    $yaml_loader->load($path);

    \Drupal::setContainer($container);
  }

  /**
   * ----------------------------------------------------
   * This next section is VERY hacky. But had to do this
   * for a multi-environment setup because Drupal 8
   * configuration management is convoluted as heck.
   * ----------------------------------------------------
   */

  /**
   * Are we on the Acquia environment?
   *
   * @return bool
   */
  public static function isAcquia() {
    return isset($_ENV['AH_SITE_ENVIRONMENT']);
  }

  /**
   * Is this a production environment?
   *
   * @return bool
   */
  public static function isProd() {
    $prodEnv = ['prod', 'prod2'];

    return in_array($_ENV['AH_SITE_ENVIRONMENT'], $prodEnv, false);
  }

  /**
   * Should we load the prod config?
   *
   * @return bool
   */
  public static function shouldLoadProdConfig() {
    return self::isAcquia() && self::isProd();
  }

  /**
   * Load the configuration from the config files.
   *
   * @return mixed
   * @throws \Symfony\Component\Yaml\Exception\ParseException
   */
  public static function loadConfigFile() {
    if (empty(self::$parsedConfig)) {
      // Load the default config
      $path = __DIR__ . '/../../config/install/config.yml';

      // @TODO: Eventually figure out a better solution for config management
      if (self::shouldLoadProdConfig()) {
        $path = __DIR__ . '/../../config/install/config.prod.yml';
      }

      self::$parsedConfig = Yaml::parse( file_get_contents($path) );
    }

    return self::$parsedConfig;
  }

  /**
   * Get setting from the config file.
   *
   * @param $name
   * @return array|mixed|null
   *
   * @throws \Symfony\Component\Yaml\Exception\ParseException
   */
  public static function getFromConfig($name) {
    $config = self::loadConfigFile();
    $parts = explode('.', $name);

    if (count($parts) == 1) {
      return isset($config[$name]) ? $config[$name] : NULL;
    }

    $keyExists = null;
    $value = self::getNestedValue($config, $parts, $keyExists);

    return $keyExists ? $value : NULL;
  }

  /**
   * Digs in for a nested value.
   *
   * @param array $reference
   * @param array $parents
   * @param null|bool $keyExists
   *
   * @return array|mixed|null
   */
  public static function getNestedValue(array &$reference, array $parents, &$keyExists = NULL) {
    foreach ($parents as $parent) {
      if (is_array($reference) && array_key_exists($parent, $reference)) {
        $reference = &$reference[$parent];
      }
      else {
        $keyExists = FALSE;
        return NULL;
      }
    }

    $keyExists = TRUE;
    return $reference;
  }

  /**
   * Gets configuration variable from Drupal first, then by directly parsing
   * the YAML file itself.
   *
   * @param $varName
   * @return array|mixed|null
   *
   * @throws \Exception
   */
  public static function getConfig($varName) {
    $moduleConfig = 'Drupal\ur_appliance.api';
    $data = null;

    // First try to load from Drupal config; as long as a config.factory exists.
    if (\Drupal::hasService('config.factory')) {
      $data = \Drupal::config($moduleConfig)->get($varName);
    }

    if ($data === null) {
      $data = self::getFromConfig("$moduleConfig.$varName");
    }

    return $data;
  }
}