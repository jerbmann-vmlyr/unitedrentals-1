<?php

namespace Drupal\ur_appliance\Traits;

trait MagicSetterTrait {
  /**
   * Helper method to set property values
   *
   * @param $name - The name of the property
   * @param $value - The value of the property
   * @throws \Exception
   */
  public function setProperties($name, $value) {
    $convertedName = $this->convertToCamelCase($name);

    if (property_exists($this, $name) && empty($this->$name)) {
      $this->$name = $value;
    }
    else if (property_exists($this, $convertedName) && empty($this->$convertedName)) {
      $this->$convertedName = $value;
    }
  }

  /**
   * Converts strings to camelCase.
   *
   * @param $str
   * @return mixed
   */
  public function convertToCamelCase($str) {
    $str = str_replace(array('_', '-'), ' ', $str);
    $str = strtolower($str);
    $str = ucwords($str);
    $str = lcfirst($str);

    return str_replace(' ', '', $str);
  }
}