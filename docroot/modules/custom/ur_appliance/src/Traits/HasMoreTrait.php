<?php
/**
 * Created by PhpStorm.
 * User: mjones2
 * Date: 11/16/17
 * Time: 11:19 AM
 */

namespace Drupal\ur_appliance\Traits;


/**
 * This trait is used to support the flagging of "hasMore" index responses...
 * In the case that the index GET request returns a dataset that still has more, use this to set it as a part of the
 * endpoint, so that it can be returned where necessary
 *
 * Trait HasMoreTrait
 * @package Drupal\ur_appliance\Traits
 */
trait HasMoreTrait {
  /**
   * in case this item still has more and we wanna know...
   * @var bool
   */
  protected $hasMore;

  public function setHasMore($data, $field) {
    $this->hasMore = $data->$field;
  }

  /**
   * @return bool
   */
  public function getHasMore() {
    return $this->hasMore;
  }
}