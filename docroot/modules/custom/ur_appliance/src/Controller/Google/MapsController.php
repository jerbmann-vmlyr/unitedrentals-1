<?php

namespace Drupal\ur_appliance\Controller\Google;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\Google\AutoCompleteEndpoint;
use Drupal\ur_appliance\Endpoint\Google\DetailsEndpoint;
use Drupal\ur_appliance\Exception\Google\GoogleMapsException;

/**
 * Class Maps
 *
 * @package Drupal\ur_appliance\Controller\Google
 */
class MapsController extends BaseController {
  /** @var AutoCompleteEndpoint */
  protected $autoCompleteEndpoint;

  /** @var DetailsEndpoint */
  protected $detailsEndpoint;

  /**
   * Retrieves an AutoComplete endpoint service
   *
   * @return mixed|AutoCompleteEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getAutoCompleteEndpoint() {
    if ($this->autoCompleteEndpoint == null) {
      $this->autoCompleteEndpoint = ApiContainer::getService('endpoint.google.autocomplete');
    }

    return $this->autoCompleteEndpoint;
  }

  /**
   * Retrieves a Details endpoint service
   *
   * @return mixed|DetailsEndpoint
   * @throws \Exception
   */
  public function getDetailsEndpoint() {
    if ($this->detailsEndpoint ==  null) {
      $this->detailsEndpoint = ApiContainer::getService('endpoint.google.details');
    }

    return $this->detailsEndpoint;
  }

  /**
   * Calls the read() method on the Details endpoint
   *
   * @param $placeId
   * @return array|\Drupal\ur_appliance\Data\Google\DetailsResponse
   * @throws \Exception
   */
  public function read($placeId) {
    $result = $this->getDetailsEndpoint()->read($placeId);
    return $this->export($result);
  }

  /**
   * Calls the read() method on the AutoComplete endpoint.
   *
   * @param $keywords
   * @return array|\Drupal\ur_appliance\Data\Google\AutoCompleteResponse
   * @throws \Exception
   */
  public function search($keywords) {
    $result = $this->getAutoCompleteEndpoint()->read($keywords);
    return $this->export($result);
  }

  /**
   * Get a Place object.
   *
   * @param null $placeId
   * @param null $address
   * @return \Drupal\ur_appliance\Data\Google\Place
   *
   * @throws GoogleMapsException|\Exception
   */
  public function getPlace($placeId = null, $address = null) {
    if (empty($placeId) && !empty($address)) {
      /** @var \Drupal\ur_appliance\Data\Google\AutoCompleteResponse $response */
      $response = $this->search($address);
      if ($response->getFirstPrediction()) {
        $placeId = $response->getFirstPrediction()->getPlaceId();
      }
    }

    if (!empty($placeId)) {
      $detailsResponse = $this->read($placeId);
      return $detailsResponse->getResult();
    }

    throw new GoogleMapsException('Unable to load the Place object.');
  }
}
