<?php

namespace Drupal\ur_appliance\Controller\DAL;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Controller\DAL\Service\BranchController;
use Drupal\ur_appliance\Controller\DAL\Service\BranchDetailsController;
use Drupal\ur_appliance\Controller\DAL\Service\SsoController;

/**
 * Class Service
 *
 * @package Drupal\ur_appliance\Controller\DAL
 */
class ServiceController extends BaseController {
  /** @var BranchController */
  private $branchController;

  /** @var BranchDetailsController */
  private $branchDetailsController;

  /** @var SsoController */
  private $ssoController;

  /**
   * Calls a static instance of the Branch sub-controller
   *
   * @return \Drupal\ur_appliance\Controller\DAL\Service\BranchController
   */
  public function BranchDetails() {
    if (empty($this->branchDetailsController)) {
      $this->branchDetailsController = new BranchDetailsController($this->provideExportedObjects);
    }
    return $this->branchDetailsController;
  }

  /**
   * Calls a static instance of the Branch sub-controller
   *
   * @return \Drupal\ur_appliance\Controller\DAL\Service\BranchController
   */
  public function Branch() {
    if (empty($this->branchController)) {
      $this->branchController = new BranchController($this->provideExportedObjects);
    }

    return $this->branchController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Service\SsoController
   */
  public function SSO() {
    if (empty($this->ssoController)) {
      $this->ssoController = new SsoController($this->provideExportedObjects);
    }

    return $this->ssoController;
  }
}