<?php


namespace Drupal\ur_appliance\Controller\DAL\Rental;


use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\EndpointException;

class PickupRequestsController extends BaseController  {

  protected $pickupRequestEndpoint;

  function __construct($exportResults = false) {
    parent::__construct($exportResults);
  }

  /**
   * Retrieves a Rates endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\RatesEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getPickupRequestsEndpoint() {
    if ($this->pickupRequestEndpoint == null) {
      $this->pickupRequestEndpoint = ApiContainer::getService('endpoint.rental.pickup_requests');
    }
    return $this->pickupRequestEndpoint;
  }

  /**
   * Create Rental Transaction
   *
   * @param $data
   * @return mixed
   *
   * @throws DalDataException|EndpointException|\Exception
   */
  public function post($data) {
    return $this->getPickupRequestsEndpoint()->create($data);
  }
}