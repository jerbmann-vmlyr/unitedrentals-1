<?php

namespace Drupal\ur_appliance\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;


class LeniencyRatesController extends BaseController {

  /** Endpoint reference */
  protected $leniencyRatesEndpoint;

  /**
   * Retrieves a Leniency Rates endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\LeniencyRatesEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getLeniencyRatesEndpoint() {
    if ($this->leniencyRatesEndpoint == null) {
      $this->leniencyRatesEndpoint = ApiContainer::getService('endpoint.rental.leniency_rates');
    }
    return $this->leniencyRatesEndpoint;
  }

  /**
   * Calls the read() method on the Leniency Rates endpoint
   *
   * @param $data
   *    Array of key => value parameters to pass to leniency rates endpoint.
   *    @see \Drupal\ur_appliance\Endpoint\DAL\Rental\LeniencyRatesEndpoint::read() for available parameters
   *
   * @return mixed
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   */
  public function read($data) {
    $result = $this->getLeniencyRatesEndpoint()->read($data);
    return $this->export($result);
  }

  public function retrieve($data) {

    return $this->read($data);
  }

}