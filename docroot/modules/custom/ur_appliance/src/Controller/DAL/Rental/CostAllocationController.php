<?php

namespace Drupal\ur_appliance\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;

/**
 * Class CostAllocation
 **
 * @package Drupal\ur_appliance\Controller\DAL\CostAllocation
 */
class CostAllocationController extends BaseController {
  protected $transactionEndpoint;
  protected $requisitionEndpoint;

  /**
   * Retrieves the TransactionCostAllocations endpoint service
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionCostAllocationsEndpoint
   *
   * @throws \Exception
   */
  public function getTransactionEndpoint() {
    if ($this->transactionEndpoint == null) {
      $this->transactionEndpoint = ApiContainer::getService('endpoint.rental.transaction_cost_allocations');
    }

    return $this->transactionEndpoint;
  }

  /**
   * Retrieves the RequisitionCostAllocations endpoint service
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionCostAllocationsEndpoint
   *
   * @throws \Exception
   */
  public function getRequisitionEndpoint() {
    if ($this->requisitionEndpoint == null) {
      $this->requisitionEndpoint = ApiContainer::getService('endpoint.rental.requisition_cost_allocations');
    }

    return $this->requisitionEndpoint;
  }

  /**
   * @param $transId
   * @param $codeType
   * @return array<\Drupal\ur_appliance\Data\DAL\TransactionCostAllocation>|mixed
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Exception
   */
  public function readAllTransactionAllocations($transId, $codeType = '') {
    $result = $this->getTransactionEndpoint()->readAll(['transId' => $transId, 'codeType' => $codeType]);

    return $this->export($result);
  }

  /**
   * @param $orderNum
   * @param $codeType
   * @return array<\Drupal\ur_appliance\Data\DAL\TransactionCostAllocation>|mixed
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Exception
   */
  public function readAllRequisitionAllocations($orderNum, $codeType = '') {
    $result = $this->getRequisitionEndpoint()->readAll(['orderNum' => $orderNum, 'codeType' => $codeType]);

    return $this->export($result);
  }

  /**
   * Responsible for sending both Allocation & Requisition codes from front-end
   * POST to update either a Transaction or Requisition based on the $order object.
   *
   * @param \Drupal\ur_appliance\Data\DAL\Order $order - The completed order object
   * @param $data - Data passed from front-end POST
   *
   * @return array|bool
   * @throws \Exception
   */
  public function sendAllocations(Order $order, $data) {
    // If we don't have any codes bail out.
    if (empty($data['requisitionCodes']) && empty($data['allocationCodes'])) {
      return true;
    }

    return $this->updateCodes($data, $order);
  }

  /**
   * Helper method to handle submitting both requisition codes and allocation codes
   *
   * @param array $data - data from the front-end POST to submit
   * @param Order $order - The completed order object
   *
   * @return array
   * @throws \Exception
   */
  public function updateCodes($data, Order $order) {
    $requisitionUpdate = $allocationUpdate = null;

    // Determine if we're sending allocation and/or requisition codes
    if (isset($data['requisitionCodes'])) {
      // Update the Requisition with the Requisition codes
      $updateData['codeType'] = 'R';
      $updateData['allocationCodes'] = $data['requisitionCodes'];
      $requisitionUpdate = $this->sendCodeUpdate($updateData, $order);
    }

    if (isset($data['allocationCodes'])) {
      // Update the Requisition with the Allocation codes
      $updateData['codeType'] = 'A';
      $updateData['allocationCodes'] = $data['allocationCodes'];
      $allocationUpdate = $this->sendCodeUpdate($updateData, $order);
    }

    return [
      'requisitionResult' => $requisitionUpdate,
      'allocationResult' => $allocationUpdate
    ];
  }

  /**
   * @param $data
   * @param Order $order
   *
   * @return bool
   * @throws \Exception
   */
  public function sendCodeUpdate($data, Order $order) {
    $data['values'] = $this->createValuesString($data['allocationCodes'], $data['codeType']);
    $data['reqId'] = (string) $order->getRequisitionId();
    $data['transId'] = (string) $order->getTransId();

    if ($order->isTransaction()) {
      return $this->getTransactionEndpoint()->update($data);
    }

    return $this->getRequisitionEndpoint()->update($data);
  }

  /**
   * Used to validate "A" codeType AllocationCodes
   * Function assumes we're validating integers and not decimals
   *
   * @param $allocations
   * @return bool
   */
  public function validateAllocationPercentages($allocations) {
    $dateRangeArray = [];
    $validated = true;

    foreach ($allocations as $allocation) {
      $startDate = $this->dateToCodeFormat(reset($allocation));
      $percentage = (int) end($allocation);

      if (!array_key_exists($startDate, $dateRangeArray)) {
        $dateRangeArray[$startDate] = $percentage;
      }
      else {
        $dateRangeArray[$startDate] += $percentage;
      }
    }

    foreach ($dateRangeArray as $percentage) {
      if ($percentage != 100) {
        $validated = false;
      }
    }

    return $validated;
  }

  /**
   * Parses allocation array and builds string used by cost allocation endpoints
   *
   * @param $allocations
   * @param $codeType
   * @return string
   */
  public function createValuesString($allocations, $codeType) {
    $formattedCodes = $dateRangeArray = [];

    foreach ($allocations AS $allocationIndex => $allocation) {
      if ($codeType == 'A') {
        // Then peel off the first and last item in the array
        foreach ($allocation['values'] as $values ) {

          if ($values['id'] == 'splitStart') {
            $startDateValue = $values['value'];
          }

          if ($values['id'] == 'splitPerc') {
            $percentageSplit = $values['value'];
            $percentageDecimal = (int) $percentageSplit / 100;
          }
        }

        $dateRangeArray = $this->getDateRangeArray($allocations);
        $startDate = $this->dateToCodeFormat($startDateValue);

        foreach ($allocation['values'] AS $id => $value) {
          if ($value['id'] != 'splitStart' && $value['id'] != 'splitPerc') {
            $codeArray = [
              $value['id'],
              $allocationIndex + 1,
              $value['value'],
              $percentageDecimal,
              $startDate,
              $dateRangeArray[$startDate]
            ];

            $formattedCodes[] = implode('|', $codeArray);
            unset($codeArray);
          }
        }
      }
      // This is a Requisition code set
      else {
        foreach ($allocation['values'] AS $id => $value) {
          $codeArray = [
            $value['id'],
            $allocationIndex + 1,
            $value['value'],
            '',
            '',
            '',
          ];

          $formattedCodes[] = implode('|', $codeArray);
          unset($codeArray);
        }
      }
    }

    return implode(',', $formattedCodes);
  }

  /**
   * Returns a key/value array of start dates and the corresponding end date
   *
   * @param $allocations
   * @return array
   */
  public function getDateRangeArray($allocations) {
    $dateRangeArray = [];

    // Get an array of distinct start dates.
    foreach ($allocations as $allocation) {
      foreach ($allocation['values'] as $k => $val) {
        if ($val['id'] == 'splitStart') {
          $formattedDate = $this->dateToCodeFormat($val['value']);
          if (!array_key_exists($formattedDate, $dateRangeArray)) {
            $dateRangeArray[$formattedDate] = '';
          }
        }
      }
    }

    // Put the start date values in order.
    ksort($dateRangeArray);
    $dateKeys = array_keys($dateRangeArray);

    foreach (array_keys($dateKeys) as $key) {
      // If this is the last start date in our array, make the end date empty.
      if ($key == count($dateKeys) - 1) {
        $dateRangeArray[$dateKeys[$key]] = '';
      }
      else {
        // Get the next start date in the array, subtract a day, and set as the end date.
        $nextDate = date_create_from_format('Ymd', $dateKeys[$key + 1]);
        $previousDay = date_sub( $nextDate, date_interval_create_from_date_string('1 day'));
        $dateRangeArray[$dateKeys[$key]] = date_format($previousDay, 'Ymd');
      }
    }

    return $dateRangeArray;
  }

  /**
   * Converts date string into preferred Allocation Code date format
   *
   * @param $dateString
   * @return false|string
   */
  protected function dateToCodeFormat($dateString) {
    $dateObj = date_create($dateString);

    return date_format($dateObj, 'Ymd');
  }
}