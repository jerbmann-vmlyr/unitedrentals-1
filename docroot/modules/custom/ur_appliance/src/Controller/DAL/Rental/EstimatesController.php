<?php

namespace Drupal\ur_appliance\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Data\DAL\Estimate;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalException;

class EstimatesController extends BaseController {
  protected $estimatesEndpoint;

  /**
   * Retrieves an Estimates endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\ChargeEstimatesEndpoint
   *  null if service not found
   */
  public function getEstimatesEndpoint() {
    if ($this->estimatesEndpoint == null) {
      $this->estimatesEndpoint = ApiContainer::getService('endpoint.rental.charge_estimates');
    }

    return $this->estimatesEndpoint;
  }

  /**
   * Calls the read() method on the Rates endpoint
   *
   * @param array $data - Basic estimate object that will be returned and
   *                      populated with totals and such.
   *
   * @return mixed|Estimate - Returns a populated estimate object
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception|\RuntimeException
   */
  public function read(array $data) {
    $estimate = $this->getEstimatesEndpoint()->read($data);

    /** @var \Drupal\ur_appliance\Adapter\DAL\DalAdapter $adapter */
    $adapter = $this->getEstimatesEndpoint()->getAdapter();

    if ($adapter->getStatus() != 0) {
      throw new DalException(
        'Error loading estimate with message: ' . $adapter->getMessageText()
      );
    }

    return $this->export($estimate);
  }

  /**
   * @TODO: Once we get Google integration built, we need a function here that
   *        will do most of what we already do in ORS where we call Google and
   *        do all the other things we need to fully orchestrate getting an
   *        estimate together to use on the front-end.
   */
}
