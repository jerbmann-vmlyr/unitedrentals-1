<?php

namespace Drupal\ur_appliance\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Provider\DAL;

class RatesController extends BaseController {
  protected $ratesEndpoint;

  /**
   * Retrieves a Rates endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\RatesEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getRatesEndpoint() {
    if ($this->ratesEndpoint == null) {
      $this->ratesEndpoint = ApiContainer::getService('endpoint.rental.rates');
    }

    return $this->ratesEndpoint;
  }

  /**
   * Calls the read() method on the Rates endpoint
   *
   * @param $data
   *    Array of key => value parameters to pass to branch distances endpoint.
   *    @see \Drupal\ur_appliance\Endpoint\DAL\Rental\RatesEndpoint::read() for available parameters
   *
   * @return mixed|array<Rate> data objects
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   */
  public function read($data) {
    $result = $this->getRatesEndpoint()->read($data);
    return $this->export($result);
  }

  /**
   * Full complex retrieve operation for rates.
   *
   * Parameters:
   *    accountId
   *    placeId
   *    address
   *    deliveryDistance
   *    branchId - (Can be calculated from the Place ID or address)
   *    catClass - The cat classes to be sent. Should be an array.
   *
   * Requires catClasses and either a placeId, an address, or a branchId to
   * be specified to complete a call.
   *
   * @param array|\stdClass $data
   * @return null|array
   * @throws \Exception
   */
  public function retrieve($data) {
    $loadableParams = ['accountId', 'branchId', 'placeId', 'address', 'catClass', 'deliveryDistance'];
    $branchId = $placeId = $address = null;
    $catClass = [];
    $accountId = 0;
    $deliveryDistance = 0;

    $data = (object) $data;

    // Pull in the loadable parameters
    foreach ($loadableParams AS $param) {
      if (isset($data->$param)) {
        $$param = $data->$param;
      }
    }

    if (empty($catClass)) {
      throw new DalException('No cat class codes given. Cannot retrieve rates.');
    }

    if (empty($accountId)) {
      // @TODO: Build this out, current site checks session then tries to load from list
    }

    if (empty($branchId) && (!empty($placeId) || !empty($address))) {
      $branch = DAL::Service()->Branch()->loadSingleFromLocation($placeId, $address);
      $branchId = empty($branch) ? null : $branch->getId();
    }

    if (!empty($branchId) && !empty($catClass)) {
      //@TODO: Do some caching here... probably.
      return $this->load($catClass, $branchId, $accountId, $deliveryDistance);
    }

    throw new DalException('Missing required fields needed to get rates.');
  }

  /**
   * Load from the three base parameters.
   *
   * @param $catClasses
   * @param $branchId
   * @param int $accountId
   * @param int deliveryDistance
   * @return array|mixed
   *
   * @throws \Exception
   */
  public function load($catClasses, $branchId, $accountId = 0, $deliveryDistance) {
    $data = [
      'catClass' => $catClasses,
      'branchId' => $branchId,
      'accountId' => $accountId,
      'deliveryDistance' => $deliveryDistance,
    ];

    return $this->read($data);
  }
}
