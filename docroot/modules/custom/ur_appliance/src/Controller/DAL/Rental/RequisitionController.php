<?php

namespace Drupal\ur_appliance\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;

class RequisitionController extends BaseController {
  protected $requisitionEndpoint;
  
  /**
   * Retrieves a Requisition endpoint service.
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionsEndpoint
   *
   * @throws \Exception
   */
  public function getRequisitionEndpoint() {
    if ($this->requisitionEndpoint == null) {
      $this->requisitionEndpoint = ApiContainer::getService('endpoint.rental.requisitions');
    }
    
    return $this->requisitionEndpoint;
  }
  
  /**
   * Calls the read() method on Requisition endpoint service.
   *
   * @param $data
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Order
   *  null if the API call failed, otherwise a Requisition data object
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   */
  public function read($data) {
    $result = $this->getRequisitionEndpoint()->read($data);
    return $this->export($result);
  }
  
  /*
   * Need to create a new method for quoteJobsiteInfoUpdate here
   * to check the REQ first then check if we need to update the transaction as well as requisition. if transId exists
   * If not only need to update the Requisition
   */
  
  /**
   * Calls the update() method on Requisition endpoint service.
   * The GET Requisition call will made to ensure that no one else has updated the same
   * record while we were editing the record
   *
   * @param $data
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Order
   *  null if the API call failed, otherwise a Requisition data object
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   */
  public function update($data) {
    /** @var Order $result_last_update */
    $result_last_update = $this->getRequisitionEndpoint()->read($data);
    $data['timeStamp'] = $result_last_update[$data['requestId']]->getTimeStamp();
    $data['statusCode'] = $result_last_update[$data['requestId']]->getStatusCode();
    $result = $this->getRequisitionEndpoint()->update($data);
    return $this->export($result);
  }
}