<?php

namespace Drupal\ur_appliance\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Data\DAL\RequisitionCodes;

class RequisitionCodeController extends BaseController {
  protected $allocationRequisitionCodeTitlesEndpoint;
  protected $transactionCostAllocationsEndpoint;
  protected $requistionsCostAllocationsEndpoint;
  
  /**
   * Retrieves a Requisition Code Title endpoint service.
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationRequisitionCodeTitlesEndpoint
   *
   * @throws \Exception
   */
  public function getRequisitionCodesTitlesEndpoint() {
    if ($this->allocationRequisitionCodeTitlesEndpoint == null) {
      $this->allocationRequisitionCodeTitlesEndpoint = ApiContainer::getService('endpoint.customer.allocation_requisition_code_titles');
    }
    
    return $this->allocationRequisitionCodeTitlesEndpoint;
  }
  
  /**
   * Retrieves a Requisition Code endpoint service.
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionCostAllocationsEndpoint
   *
   * @throws \Exception
   */
  public function getTransactionCostAllocationEndpoint() {
    if ($this->transactionCostAllocationsEndpoint == null) {
      $this->transactionCostAllocationsEndpoint = ApiContainer::getService('endpoint.rental.transaction_cost_allocations');
    }
    
    return $this->transactionCostAllocationsEndpoint;
  }

  /**
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionCostAllocationsEndpoint
   */
  public function getRequisitionCostAllocationEndpoint() {
    if ($this->requistionsCostAllocationsEndpoint == null) {
      $this->requistionsCostAllocationsEndpoint = ApiContainer::getService('endpoint.rental.requisition_cost_allocations');
    }

    return $this->requistionsCostAllocationsEndpoint;
  }
  
  /**
   * Calls the read() method on Requisition Codes endpoint service for transactions
   * (Reservations with transId).
   *
   * @param $accountId
   * @param $transId
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\RequisitionCodes
   *  null if the API call failed, otherwise a Requisition data object
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   */
  public function readCodes($accountId, $transId) {

    $isTransaction = TRUE;
    if (substr( $transId, 0, 1 ) === "A") {
      $data = ['id' => $transId];
      $isTransaction = FALSE;
    }
    else {
      $data = ['transId' => $transId];
    }

    $titles = $this->getRequisitionCodesTitlesEndpoint()->readAll(['accountId' => $accountId]);

    if ($isTransaction) {
      $transValues = $this->getTransactionCostAllocationEndpoint()->readAll($data);
    }
    else {
      $transValues = $this->getRequisitionCostAllocationEndpoint()->readAll($data);
    }


    $codes = [];

    /** @var RequisitionCodes $title */
    foreach ($titles as $idx => $title) {
      $code = new RequisitionCodes($title);
      $code->import($transValues);
      $codes[] = $code;
    }

    return $this->export($codes);
  }
  
}