<?php

namespace Drupal\ur_appliance\Controller\DAL\Rental;

use Amp\Loop;
use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Exception\NoResultsException;
use Drupal\ur_appliance\Provider\DAL;

/**
 * Class Transactions.
 * @package Drupal\ur_appliance\Controller\DAL\Rental
 */
class TransactionsController extends BaseController {
  /**
   * Class variables.
   *
   * @var \Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint
   */
  protected $transactionsEndpoint;
  protected $requisitionEndpoint;

  /**
   * Retrieves a Transaction endpoint service.
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint
   *    null if service not found
   *
   * @throws \Exception
   */
  public function getTransactionEndpoint() {
    if ($this->transactionsEndpoint == NULL) {
      $this->transactionsEndpoint = ApiContainer::getService('endpoint.rental.transactions');
    }

    return $this->transactionsEndpoint;
  }

  /**
   * Retrieves a Requisition endpoint service.
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionsEndpoint
   *  A Requisition Endpoint.
   *
   * @throws \Exception
   */
  public function getRequisitionEndpoint() {
    if ($this->requisitionEndpoint == NULL) {
      $this->requisitionEndpoint = ApiContainer::getService('endpoint.rental.requisitions');
    }

    return $this->requisitionEndpoint;
  }

  /**
   * Look up Rental Transactions by ID.
   *
   * @param array|mixed $data
   *    Result set.
   *
   * @throws DalException
   * @throws \Exception|\Drupal\ur_appliance\Exception\EndpointException
   */
  public function read($data) {
    $transaction = $this->getTransactionEndpoint()->read($data);
    $this->export($transaction);
  }

  /**
   * Search Rental Transactions by Account ID (required).
   *
   * * Or any other field offered. See endpoint for further details.
   *
   * @param array|mixed $data
   *   Result set.
   *
   * @return array
   *   Exported result set.
   *
   * @throws \Exception
   */
  public function readAll($data) {
    $transactions = $this->getTransactionEndpoint()->readAll($data);
    return $this->export($transactions);
  }

  /**
   * Search Rental Transactions by Account ID.
   *
   * Collects the On-Rent items asynchronously using AmPHP.
   *
   * @param array $params
   *   Request parameters.
   * @param array $orderFields
   *   Required order fields.
   *
   * @return array|mixed
   *   Array of formatted items on-rent.
   *
   * @throws DalDataException
   * @throws DalException
   * @throws EndpointException
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\NoResultsException
   */
  public function readAllByItem(array $params, array $orderFields = []) {
    // Declare this so we can use it by reference within the Loop's anonymous function.
    $responses = [];

    // The Loop is the heart of Amp's Asynchronous logic.
    // use() feeds all our needed DAL variables to the anon function.
    Loop::run(
      function () use ($params, $orderFields, &$responses) {
        try {
          // Simplified anonymous functions help bundle the calls
          // to take advantage of asynchronous logic.
          // 'yield' keyword is how we run processes concurrently.
          $responses = yield array_map(
            function ($type) use ($params, $orderFields) {
              $params['type'] = $type;

              // Make sure this is empty to avoid duplicates.
              $transactions = $this->getTransactionEndpoint()->readAllByItem($params, $orderFields);
              $result = $this->export($transactions);

              if (!empty($result)) {
                /* @var \Drupal\ur_appliance\Data\DAL\Item $item */
                foreach ($result as $item) {
                  $item->loadDrupalData();

                  // DAL doesn't include the type in the returned results but
                  // we would like to use it so we add it back in.
                  if (empty($item->type)) {
                    $item->type = $type;
                  }
                }
              }

              // Formats our results and converts to a Promise before return!
              return DAL::Customer(TRUE)->Equipment()->asyncExport($result);
            },
            $params['types']
          );
        }
        catch (\Exception $e) {
          $responses = [$e->getMessage()];
        }

        // There are many ways to terminate a Loop.
        // Our existing code base is best served by manual termination.
        Loop::stop();
      }
    );

    // `yield array_map()` returns the batches as separate elements of the $responses array.
    // We want one single array of all the items on-rent.
    $results = array_reduce($responses, 'array_merge', []);

    if (empty($results)) {
      throw new NoResultsException('No data returned.');
    }

    $results = $this->dedupeItems($results);

    return $results;
  }

  /**
   * De-duplicates the items returned from DAL.
   *
   * We pull all applicable types but don't want to repeat the items if they happen to fit
   * more than one type.
   *
   * @param array $data
   *   Item result set.
   *
   * @return mixed
   *   De-duped, flattened numerically index array of items.
   */
  public function dedupeItems(array $data) {
    // Instantiate loop vars.
    $responses = [];

    // Loop through the data.
    foreach ($data as $key => $datum) {
      // Shorten IDs.
      $equipId = $datum['equipmentId'];
      $transId = $datum['transId'];
      $lineId = $datum['transLineId'];
      $type = $datum['type'];

      $id = "$equipId-$transId-$lineId";

      if ($type == 1) {
        // If it is type 1 and the quantity matches the quantity
        $quantitiesSet = isset($datum['quantityOnRent']);
        $pendingSet = isset($datum['quantityPendingPickup']);
        $onRentAndPendingMatch = $quantitiesSet && $pendingSet && ($datum['quantityOnRent'] == $datum['quantityPendingPickup']);

        if ($onRentAndPendingMatch) {
          continue;
        }

      } else if ($type == 6) {

        $id = $id . '-' . $key;

      }

      $responses[$id] = $datum;

    }

    return $responses;
  }

  /**
   * Calls the read() method on Requisition endpoint service.
   *
   * The reservationId existence will be checked here, if does exist,
   * it calls the requisition API otherwise transaction API will be called.
   *
   * @param array|mixed $data
   *    Request parameters.
   *
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Order
   *    null if the API call failed, otherwise a Requisition data object.
   *
   * @throws DalException
   * @throws \Exception|EndpointException
   */
  public function readDetails($data) {
    $requisitionData['requestId'] = $data['requestId'];
    $requisitionData['id']        = $data['id'];

    /* @var \Drupal\ur_appliance\Data\DAL\Order $result_requisition */
    $result_requisition = $this->getRequisitionEndpoint()->read($requisitionData);

    if (!empty($data['reservationId'])) {
      $finalData['id'] = $data['reservationId'];

      /* @var \Drupal\ur_appliance\Data\DAL\Order $result */
      $result = $this->getTransactionEndpoint()->read($finalData);
      $result_requisition[$data['requestId']]->import($result);
    }

    return $this->export($result_requisition);
  }

  /**
   * Create Rental Transaction.
   *
   * @param array|mixed $data
   *    Request Parameters.
   *
   * @return mixed
   *   Success or failure?
   *
   * @throws DalDataException
   * @throws EndpointException
   * @throws \Exception
   */
  public function post($data) {
    /* @var \Drupal\ur_appliance\Data\DAL\Order $order */
    return $this->getTransactionEndpoint()->post($data);
  }

  /**
   * Orchestrates the multiple API calls needed to create a Transaction.
   *
   * Also submit any comments for that and/or submit allocation codes (if any).
   *
   * Order of operations:
   *   1. POST the transaction
   *   2. POST order comments
   *   3. PUT allocation and/or requisition codes (optional)
   *   4. Return a success/fail response back to the front-end.
   *
   * @param array|mixed $data
   *   - Data supplied from the POST request.
   * @return array|Order
   *   The order object.
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function submit($data) {
    if (!isset($data['transaction'])) {
      throw new DalException('Cannot submit a transaction without transaction data.');
    }

    $errors = [];
    $transaction = (array) $data['transaction'];

    /* @var Order $order */
    $order = $this->post($transaction);

    if (!$order instanceof Order) {
      throw new DalDataException('Did not receive order object from transaction post.');
    }

    /*
     * In effort to reconcile discrepancy with missing orders in GA we're
     * sending a custom event here.
     */
    $guid = $this->getAdapter()->getGuid();
    $id = !empty($order->getTransId()) ? $order->getTransId() : $order->getRequisitionId();

    $url = "http://www.google-analytics.com/collect?v=1&t=event&tid=UA-159052-27&cid=$guid&ec=Reservation&ea=$id&el=$guid";
    $request = \Drupal::httpClient()->post($url);

    if ($request->getStatusCode() !== 200) {
      \Drupal::logger('Failed posting google event');
    }

    $commentResults = DAL::Order(TRUE)->Comment()->sendComments($order, $data);

    if ($commentResults['itemResponse'] == FALSE || $commentResults['jobsiteResponse'] == FALSE) {
      // Sending comments failed, return message to front-end.
      $msg = 'There was a problem saving your item notes. Please call to provide any important information.';
      $errors[] = $msg;
    }

    $codesResult = DAL::Rental(TRUE)->CostAllocation()->sendAllocations($order, $data);

    if ($codesResult['requisitionResult'] == FALSE || $codesResult['allocationResult'] == FALSE) {
      // Sending codes failed, return message to front-end.
      // @TODO: Confirm error message
      $msg = 'There was a problem saving your allocation or requisition codes.';
      $errors[] = $msg;
    }

    // @TODO: Handle returning error message(s) if we have any

    return $this->export($order);
  }

  /**
   * Processes any transaction updates requested.
   *
   * @param array|mixed $data
   *   Updated data.
   *
   * @return object
   *   The updated transaction object.
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function update($data) {
    $result = null;

    if (!empty($data['transId']) || !empty($data['quoteId'])) {
      $finalData['id'] = $data['transId'];

      if ($data['quoteId']) {
        $finalData['id'] = $data['quoteId'];
      }

      $finalData['accountId'] = $data['id'];

      /** @var Order $result_transaction */
      $result_transaction = $this->getTransactionEndpoint()->read($finalData);

      if (empty($data['po'])) {
        $data['po'] = $result_transaction->getPo();
      }

      $result = $this->getTransactionEndpoint()->update($data);
    }
    elseif (!$data['jobsiteQuoteEditFlag']) {
      /*
       * Removing the updateType with values of transaction and reserve and update
       * to avoid any failure for updating a requisition.
       * UpdateType for requisition endpoint is numeric value.
       */
      if (isset($data['updateType']) && ($data['updateType'] === 'reserve' || $data['updateType'] === 'transaction' || $data['updateType'] === 'update')) {
        unset($data['updateType']);
      }

      if ($data['updateType'] != 'reserve') {
        $result = $this->getRequisitionEndpoint()->update($data);
      }
    }

    return $this->export($result);
  }

  /**
   * Converts Quote to a Reservation.
   *
   * Needs to check if the user changed the PO number BEFORE converting a Quote
   * Make the update, update rental/transactions and rental/requisition
   * Transaction update should happen to handle
   *    1. Update PO number (if has changed by user)
   *    2. Convert the quote
   * Requisition update should happen to handle
   *    ONLY PO Number update (if has changed by user)
   *
   * @param array|mixed $data
   *   Updated data.
   *
   * - updateType might change depending on PO number existence
   * - po is set in $data, only if user changes it on front-end.
   *
   * @return array|mixed
   *   Updated object.
   *
   * @throws DalDataException
   * @throws DalException
   * @throws EndpointException
   * @throws \Exception
   */
  public function convertQuote($data) {
    $result_convert_process = [];

    if (!empty($data['transId']) || !empty($data['quoteId'])) {
      $finalData['id'] = $data['transId'];

      if ($data['quoteId']) {
        $finalData['id'] = $data['quoteId'];
      }

      $finalData['accountId'] = $data['id'];

      /** @var Order $result_transaction */
      $result_transaction = $this->getTransactionEndpoint()->read($finalData);

      // Steps -- User did change the PO and requested to Convert the quote:
      // 1. Need to change updateType to "update"
      // 2. Update transaction with the new PO
      // 3. Read Requisition info to pull the statusCode and timeStamp
      // 4. Remove updateType from data, to pass data to rental/requisitions
      // 5. Update requisition with the new PO
      // 6. Push updateType back to the array with 'reserve' value to pass it to rental/transaction
      // 7. Start the convert quote process
      if (isset($data['po'])) {
        $data['updateType'] = 'update';
        $data['transId'] = $data['quoteId'];
        $result = $this->getTransactionEndpoint()->update($data);

        if ($result->status == 0) {
          $result_requisition = $this->getRequisitionEndpoint()->read($data);

          $data['timeStamp'] = $result_requisition[$data['requestId']]->getTimeStamp();
          $data['statusCode'] = $result_requisition[$data['requestId']]->getStatusCode();

          unset($data['updateType']);

          // rental/requisition api requires "poNumber" key for PUT method.
          $data['poNumber'] = $data['po'];
          $result_requisition_update = DAL::Rental(TRUE)->Requisitions()->update($data);

          if ($result_requisition_update->status == 0) {
            $data['updateType'] = 'reserve';
            $result_convert_process = $this->getTransactionEndpoint()->update($data);
          }
          else {
            throw new DalDataException('Update requisition failed. Please try again.');
          }
        }
        else {
          throw new DalDataException('Update transaction failed. Please try again.');
        }
      }

      // Steps -- User did not change the PO, just requested to Convert the quote:
      // 1. Pull the current PO number
      // 2. Start the convert quote process
      elseif (!$data['po']) {
        $data['po'] = $result_transaction->getPo();
        $result_convert_process = $this->getTransactionEndpoint()->update($data);
      }
    }
    else {
      // Handles if the quote is a "requisition only" and user wants to convert it
      // Here there is no endpoint to update the transType to something to Convert that QUOTE...
      throw new DalDataException('This Quote can not be converted. Contact Branch location.');
    }

    return $this->export($result_convert_process);
  }

  /**
   * Updates the Transaction Edit Forms.
   *
   * TODO Verify this doc comment is accurate.
   *
   * @param array|mixed $data
   *   Updated data.
   *
   * @return array|mixed
   *   Updated object.
   *
   * @throws DalDataException
   * @throws DalException
   * @throws EndpointException
   * @throws \Exception
   */
  public function updateTransactionEditForms($data) {
    if (!empty($data['transId'])) {
      $finalData['id'] = $data['transId'];
      $finalData['accountId'] = $data['id'];

      /* @var \Drupal\ur_appliance\Data\DAL\Order $result_transaction */
      $result_transaction = $this->getTransactionEndpoint()->read($finalData);

      if ($data['details']) {
        $data['eqpDetails'] = [];
        foreach ($data['details'] as $item => $values) {

          // Let's build the seqId!
          // Glue together any catclassess into a CSV string.
          $catClassString = implode(',', (array) $values->catclass);

          // Grab the array of Items on this result_transaction.
          $resultTransactionItems = $result_transaction->getItems();

          // Add another element to the Transaction Items array with the key of $catClassString
          // And return the TransLineId for the relevant item.
          $seqId = $resultTransactionItems[$catClassString]->getTransLineId();

          $item = (object) [
            'catClass' => implode(',', (array) $values->catclass),
            'quantity' => implode(',', (array) $values->quantity),
            'seqId' => $seqId,
          ];
          $data['eqpDetails'][] = $item;
        }
      }

      $data['po'] = $data['po'] ? $data['po'] : $result_transaction->getPo();

      $result = $this->getTransactionEndpoint()->update($data);
    }
    return $this->export($result);
  }

}