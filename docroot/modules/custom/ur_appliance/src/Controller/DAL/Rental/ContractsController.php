<?php

namespace Drupal\ur_appliance\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\EndpointException;

/**
 * Class Contracts
 * @package Drupal\ur_appliance\Controller\DAL\Rental
 */
class ContractsController extends BaseController
{
  /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\ContractsEndpoint $contractsEndpoint */
  protected $contractsEndpoint;

  /**
   * Retrieves a Contracts endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\ContractsEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getContractEndpoint()
  {
    if ($this->contractsEndpoint == null) {
      $this->contractsEndpoint = ApiContainer::getService('endpoint.rental.contracts');
    }

    return $this->contractsEndpoint;
  }

  /**
   * Look up Rental Contracts by transId
   *
   * @param $data
   *
   * @return Order
   * @throws DalException|\Exception|\Drupal\ur_appliance\Exception\EndpointException
   */
  public function read($data)
  {
    $result = $this->getContractEndpoint()->read($data);
    $this->export($result);
  }

  /**
   * Search Rental Contracts by Account ID (required) and any other field
   * offered. See endpoint for further details.
   *
   * @param $data
   *
   * @return array
   * @throws \Exception
   */
  public function readAll($data)
  {
    $results = $this->getContractEndpoint()->readAll($data);
    return $this->export($results);
  }

  /**
   * Update Rental Contract: Currently only changes the enddate
   *
   * @param $data
   * @return mixed
   *
   * @throws DalDataException|EndpointException|\Exception
   */
  public function update($data)
  {
    if (!empty($data['transId'])) {
      $result = $this->getContractEndpoint()->update($data);
    }
    return $this->export($result);
  }
}