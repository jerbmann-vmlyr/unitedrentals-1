<?php

namespace Drupal\ur_appliance\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;


class CountsController extends BaseController {

  /** Endpoint reference */
  protected $countsEndpoint;

  /**
   * Retrieves a Rates endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Rental\CountsEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getCountsEndpoint() {
    if ($this->countsEndpoint == null) {
      $this->countsEndpoint = ApiContainer::getService('endpoint.rental.counts');
    }

    return $this->countsEndpoint;
  }

  /**
   * Calls the read() method on the Counts endpoint
   *
   * @param $data
   *    Array of key => value parameters to pass to branch distances endpoint.
   *    @see \Drupal\ur_appliance\Endpoint\DAL\Rental\CountsEndpoint::read() for available parameters
   *
   * @return mixed|object<Counts> data objects
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   */
  public function read($data) {
    $result = $this->getCountsEndpoint()->read($data);
    return $this->export($result);
  }

}