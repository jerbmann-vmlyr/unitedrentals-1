<?php

namespace Drupal\ur_appliance\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Data\DAL\Invoice;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Rental\InvoicesEndpoint;
use Drupal\ur_appliance\Interfaces\EndpointInterface;

/**
 * @Class InvoicesController
 */
class InvoicesController extends BaseController {

  /**
   * @var array
   **/
  protected $invoices;

  /**
   * @var EndpointInterface
   **/
  protected $endpoint;

  function __construct($exportResults = false) {
    parent::__construct($exportResults);
  }

  /**
   * @param $params array -
   * Currently, only supports a single accountId request
   *
   * Outstanding param state set in InvoiceEndpoint
   *
   * @return float|int
   */
  public function getInvoices(array $params) {
      //Returns all invoices for accountId for the past year
      $result = $this->getInvoiceEndpoint()->readAllByItem($params);
      return $this->export($result);
  }

  /**
   * @param $params array -
   * Currently, only supports a single accountId request
   *
   * Outstanding param state set in InvoiceEndpoint
   *
   * @return float|int
   */
  public function outstandingInvoices(array $params) {
    //default for only including balances that have yet to be paid
    $this->invoices = $this->getInvoiceEndpoint()->readAllByItem($params, $wantHasMore = true);
    $total = round(Invoice::totalOverdue($this->invoices));
    $more = $this->getInvoiceEndpoint()->getHasMore();
    return compact('total', 'more');
  }

  /**
   * Retrieve the InvoiceEndpoint from our Drupal services container
   * @return InvoicesEndpoint|EndpointInterface
   */
  public function getInvoiceEndpoint() {
    if ($this->endpoint == null) {
      $this->endpoint = ApiContainer::getService('endpoint.rental.invoices');
    }

    return $this->endpoint;
  }
}
