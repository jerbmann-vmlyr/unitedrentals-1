<?php

namespace Drupal\ur_appliance\Controller\DAL;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Controller\DAL\Equipment\AvailabilityController;
use Drupal\ur_appliance\Controller\DAL\Equipment\GpsController;

class EquipmentController extends BaseController {
  private $availabilityController;
  private $gpsController;

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Equipment\AvailabilityController
   */
  public function Availability() {
    if (empty($this->availabilityController)) {
      $this->availabilityController = new AvailabilityController($this->provideExportedObjects);
    }

    return $this->availabilityController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Equipment\GpsController
   */
  public function Gps() {
    if (empty($this->gpsController)) {
      $this->gpsController = new GpsController($this->provideExportedObjects);
    }

    return $this->gpsController;
  }
}