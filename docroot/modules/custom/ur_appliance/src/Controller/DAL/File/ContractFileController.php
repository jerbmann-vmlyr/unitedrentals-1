<?php

namespace Drupal\ur_appliance\Controller\DAL\File;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;

class ContractFileController extends BaseController {
  protected $contractFilesEndpoint;
  
  /**
   * Retrieves a ContractFile endpoint service
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\File\ContractsEndpoint
   *
   * @throws \Exception
   */
  public function getContractFileEndpoint() {
    if ($this->contractFilesEndpoint == null) {
      $this->contractFilesEndpoint = ApiContainer::getService('endpoint.file.contracts');
    }
    
    return $this->contractFilesEndpoint;
  }
  
  /**
   * Calls the read() method on ContractFiles endpoint service
   *
   * @param $contractFile
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\File
   *  null if the API call failed, otherwise a ContractFile data object
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   */
  public function read($data) {
    $result = $this->getContractFileEndpoint()->read($data);
    return $this->export($result);
  }
}