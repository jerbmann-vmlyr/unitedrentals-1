<?php

namespace Drupal\ur_appliance\Controller\DAL\Equipment;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalException;

/**
 * Class Availability
 * @package Drupal\ur_appliance\Controller\DAL\Equipment
 */
class AvailabilityController extends BaseController {
  protected $availabilityEndpoint;

  public function getAvailabilityEndpoint() {
    if ($this->availabilityEndpoint == null) {
      $this->availabilityEndpoint = ApiContainer::getService('endpoint.equipment.availability');
    }

    return $this->availabilityEndpoint;
  }

  /**
   * Call the read method on Availability endpoint service
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   */
  public function read($data) {
    $result = $this->getAvailabilityEndpoint()->read($data);
    return $this->export($result);
  }

}