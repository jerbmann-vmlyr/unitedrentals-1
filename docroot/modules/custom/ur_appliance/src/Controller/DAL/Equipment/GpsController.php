<?php

namespace Drupal\ur_appliance\Controller\DAL\Equipment;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Exception\DataException;
use Drupal\ur_appliance\Exception\NoResultsException;

/**
 * Class Gps
 * @package Drupal\ur_appliance\Controller\DAL\Equipment
 */
class GpsController extends BaseController {
  /** @var \Drupal\ur_appliance\Endpoint\DAL\Gps\AempTelematicsEndpoint */
  protected $aempTelematicsEndpoint;

  /**
   * @return \Drupal\ur_appliance\Endpoint\DAL\Gps\AempTelematicsEndpoint
   */
  protected function getAempTelematicsEndpoint() {
    if ($this->aempTelematicsEndpoint == null) {
      $this->aempTelematicsEndpoint = ApiContainer::getService('endpoint.gps.aemptelematics');
    }

    return $this->aempTelematicsEndpoint;
  }

  /**
   * Gets one's equipment GPS details.
   *
   * @param array $fields
   *   When sending multiple, you can send as an array or comma-separated string.
   *
   *   Possible fields/parameters:
   *     Field            - Required - Type     - Allow Multiple? - Description
   *     -----------------------------------------------------------------------
   *     accountId        - Yes      - Int      - No
   *     equipmentId      - No       - String   - No
   *     fromDate         - Yes      - Int      - No
   *     toDate           - Yes      - Int      - No
   *     includeHistory   - No       - Bool     - No  - Flag to include history records
   *     includeChildren  - No       - Bool     - No  - Flag to include child accounts
   *
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Account
   *
   * @throws DalException|EndpointException|DalDataException
   * @throws EndpointException|DataException|NoResultsException|\Exception
   */
  public function read($fields) {
    $result = $this->getAempTelematicsEndpoint()->read($fields);
    return $this->export($result);
  }
}