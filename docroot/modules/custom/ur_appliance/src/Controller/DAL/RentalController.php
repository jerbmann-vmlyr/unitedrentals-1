<?php

namespace Drupal\ur_appliance\Controller\DAL;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Controller\DAL\Rental\ContractsController;
use Drupal\ur_appliance\Controller\DAL\Rental\CostAllocationController;
use Drupal\ur_appliance\Controller\DAL\Rental\CountsController;
use Drupal\ur_appliance\Controller\DAL\Rental\EstimatesController;
use Drupal\ur_appliance\Controller\DAL\Rental\LeniencyRatesController;
use Drupal\ur_appliance\Controller\DAL\Rental\PickupRequestsController;
use Drupal\ur_appliance\Controller\DAL\Rental\RatesController;
use Drupal\ur_appliance\Controller\DAL\Rental\RequisitionController;
use Drupal\ur_appliance\Controller\DAL\Rental\TransactionsController;
use Drupal\ur_appliance\Controller\DAL\Rental\RequisitionCodeController;
use Drupal\ur_appliance\Controller\DAL\Rental\InvoicesController;

/**
 * Class Service
 * @package Drupal\ur_appliance\Controller\DAL
 */
class RentalController extends BaseController {
  /** @var CostAllocationController */
  private $costAllocationController;

  /** @var CountsController */
  private $countsController;

  /** @var LeniencyRatesController */
  private $leniencyRatesController;

  /** @var RatesController */
  private $ratesController;

  /** @var \Drupal\ur_appliance\Controller\DAL\Rental\EstimatesController */
  private $estimatesController;

  /** @var  \Drupal\ur_appliance\Controller\DAL\Rental\ContractsController */
  private $contractsController;

  /** @var  \Drupal\ur_appliance\Controller\DAL\Rental\TransactionsController */
  private $transactionsController;

  /** @var  \Drupal\ur_appliance\Controller\DAL\Rental\InvoicesController */
  private $invoicesController;

  /** @var  \Drupal\ur_appliance\Controller\DAL\Rental\RequisitionController */
  private $requisitionsController;

  /** @var  \Drupal\ur_appliance\Controller\DAL\Rental\RequisitionCodeController */
  private $requisitionCodesController;

  /** @var  \Drupal\ur_appliance\Controller\DAL\Rental\PickupRequestsController */
  private $pickupRequestsController;

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Rental\CostAllocationController
   */
  public function CostAllocation() {
    if (empty($this->costAllocationController)) {
      $this->costAllocationController = new CostAllocationController($this->provideExportedObjects);
    }

    return $this->costAllocationController;
  }

  /**
   * @return CountsController
   *
   */
  public function Counts() {
    if (empty($this->countsController)) {
      $this->countsController = new CountsController($this->provideExportedObjects);
    }

    return $this->countsController;
  }

  public function LeniencyRates() {
    if (empty($this->leniencyRatesController)) {
      $this->leniencyRatesController = new LeniencyRatesController($this->provideExportedObjects);
    }

    return $this->leniencyRatesController;
  }

  public function Rates() {
    if (empty($this->ratesController)) {
      $this->ratesController = new RatesController($this->provideExportedObjects);
    }

    return $this->ratesController;
  }

  public function Estimates() {
    if (empty($this->estimatesController)) {
      $this->estimatesController = new EstimatesController($this->provideExportedObjects);
    }

    return $this->estimatesController;
  }

  public function Requisitions() {
    if (empty($this->requisitionsController)) {
      $this->requisitionsController = new RequisitionController($this->provideExportedObjects);
    }

    return $this->requisitionsController;
  }

  public function Contracts() {
    if (empty($this->contractsController)) {
      $this->contractsController = new ContractsController($this->provideExportedObjects);
    }

    return $this->contractsController;
  }

  public function Transactions() {
    if (empty($this->transactionsController)) {
      $this->transactionsController = new TransactionsController($this->provideExportedObjects);
    }

    return $this->transactionsController;
  }

  public function RequisitionCodes() {
    if (empty($this->requisitionCodesController)) {
      $this->requisitionCodesController = new RequisitionCodeController($this->provideExportedObjects);
    }

    return $this->requisitionCodesController;
  }

  public function PickupRequests() {
    if ( empty($this->pickupRequestsController)) {
      $this->pickupRequestsController = new PickupRequestsController($this->provideExportedObjects);
    }
    return $this->pickupRequestsController;
  }

  public function Invoices() {
    if (empty($this->invoicesController)) {
      $this->invoicesController = new InvoicesController($this->provideExportedObjects);
    }

    return $this->invoicesController;
  }
}
