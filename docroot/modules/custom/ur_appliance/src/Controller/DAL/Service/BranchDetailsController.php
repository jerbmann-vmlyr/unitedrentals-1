<?php

namespace Drupal\ur_appliance\Controller\DAL\Service;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Data\Google\Place;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Provider\Google;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class BranchDetailsController extends BaseController {

  /** @var \Drupal\ur_appliance\Endpoint\DAL\Location\BranchesDetailsEndpoint  */
  protected $branchDetailsEndpoint;

    /**
   * Retrieves a Branches endpoint service
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Location\BranchesEndpoint
   *
   * @throws \Exception
   */
  public function getBranchDetailsEndpoint() {
    if ($this->branchDetailsEndpoint == null) {
      $this->branchDetailsEndpoint = ApiContainer::getService('endpoint.location.branch_details');
    }

    return $this->branchDetailsEndpoint;
  }

   /**
   * Calls the read() method on BranchDetails endpoint service
   *
   * @param $branchId
   *  The branchId to retrieve data for
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Branch
   *  null if the API call failed, otherwise a Branch data object
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   * @throws InvalidArgumentException
   */
  public function read($branchId) {
    $result = $this->getBranchDetailsEndpoint()->read($branchId);

    return $this->export($result);
  }

  /**
   * Calls the read() method on BranchDetails endpoint service
   *
   * @param $branchIds - The branchId to retrieve data for
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Branch
   *  null if the API call failed, otherwise a Branch data object
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   * @throws InvalidArgumentException
   */
  public function readMultiple($branchIds) {
    $result = $this->getBranchDetailsEndpoint()->readMultiple($branchIds);

    return $this->export($result);
  }

  /**
   * Calls the readAll() method on BranchDetails endpoint service
   *
   * @return array<\Drupal\ur_appliance\Data\DAL\Branch>|mixed
   *  Returns an array of Branch data objects
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Exception
   * @throws InvalidArgumentException
   */
  public function readAll() {
    $result = $this->getBranchDetailsEndpoint()->readAll();

    return $this->export($result);
  }

}