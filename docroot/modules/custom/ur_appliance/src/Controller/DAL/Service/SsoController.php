<?php

namespace Drupal\ur_appliance\Controller\DAL\Service;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\SsoLoginsEndpoint;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\EndpointException;

class SsoController extends BaseController {
  protected $ssoEndpoint;

  /**
   * SSO constructor.
   * @param bool $exportResults
   * @param null $adapter
   */
  public function __construct($exportResults = FALSE, $adapter = null) {
    parent::__construct($exportResults);

    if (!empty($adapter)) {
      $this->setAdapter($adapter);
    }
  }

  /**
   * Retrieves a SSO endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\SsoLoginsEndpoint
   * @throws \Exception
   */
  public function getSSOEndpoint() {
    if ($this->ssoEndpoint == null) {
      if ($this->adapter === null) {
        $this->ssoEndpoint = ApiContainer::getService('endpoint.sso_logins');
      }
      else {
        $this->ssoEndpoint = new SsoLoginsEndpoint( $this->getAdapter() );
      }
    }

    return $this->ssoEndpoint;
  }

  /**
   * Calls the read() method on SSO endpoint service. This endpoint doesn't
   * return any data objects so we need to build an array with the keys 'token'
   * and 'message.'.
   *
   * @see \Drupal\ur_appliance\Endpoint\DAL\SsoLoginsEndpoint::read();
   *
   * If the call is successful the 'token' key will have a value and the
   * 'message' key will be null.
   *
   * If the call was unsuccessful 'token' will be FALSE and
   * the 'message' will have the messageText from the response as it's value.
   *
   * @param $email - The email address we are SSO-ing with
   * @return array - An array with the keys 'token' and 'message' with the
   *                 values from the api call.
   *
   * @throws DalException|EndpointException|\Exception
   */
  public function read($email) {
    $return = ['token' => FALSE, 'message' => ''];
    $token = $this->getSSOEndpoint()->read($email);

    if (!empty($token)) {
      $return['token'] = $token;
    }

    $return['message'] = $this->getSSOEndpoint()->getAdapter()->getMessageText();

    return $return;
  }
}