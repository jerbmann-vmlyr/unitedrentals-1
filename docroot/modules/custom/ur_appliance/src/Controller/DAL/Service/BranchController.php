<?php

namespace Drupal\ur_appliance\Controller\DAL\Service;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Data\Google\Place;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Provider\Google;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class BranchController extends BaseController {
  /** @var \Drupal\ur_appliance\Endpoint\DAL\Location\BranchesEndpoint  */
  protected $branchEndpoint;

  /** @var \Drupal\ur_appliance\Endpoint\DAL\Location\BranchDistancesEndpoint  */
  protected $branchDistEndpoint;

  /**
   * Retrieves a Branches endpoint service
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Location\BranchesEndpoint
   *
   * @throws \Exception
   */
  public function getBranchEndpoint() {
    if ($this->branchEndpoint == null) {
      $this->branchEndpoint = ApiContainer::getService('endpoint.location.branches');
    }

    return $this->branchEndpoint;
  }

  /**
   * Retrieves a BranchDistances endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Location\BranchDistancesEndpoint
   *
   * @throws \Exception
   */
  public function getBranchDistanceEndpoint() {
    if ($this->branchDistEndpoint == null) {
      $this->branchDistEndpoint = ApiContainer::getService('endpoint.location.branch_distances');
    }

    return $this->branchDistEndpoint;
  }

  /**
   * Calls the read() method on Branch endpoint service
   *
   * @param $branchId
   *  The branchId to retrieve data for
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Branch
   *  null if the API call failed, otherwise a Branch data object
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   * @throws InvalidArgumentException
   */
  public function read($branchId) {
    $result = $this->getBranchEndpoint()->read($branchId);

    return $this->export($result);
  }

  /**
   * Calls the read() method on Branch endpoint service
   *
   * @param $branchIds - The branchId to retrieve data for
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Branch
   *  null if the API call failed, otherwise a Branch data object
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   * @throws InvalidArgumentException
   */
  public function readMultiple($branchIds) {
    $result = $this->getBranchEndpoint()->readMultiple($branchIds);

    return $this->export($result);
  }

  /**
   * Calls the readAll() method on Branch endpoint service
   *
   * @return array<\Drupal\ur_appliance\Data\DAL\Branch>|mixed
   *  Returns an array of Branch data objects
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Exception
   * @throws InvalidArgumentException
   */
  public function readAll() {
    $result = $this->getBranchEndpoint()->readAll();

    return $this->export($result);
  }

  /**
   * Calls the BranchDistances read() method
   *
   * @param $data
   *  Array of key => value parameters to pass to branch distances endpoint.
   *  @see \Drupal\ur_appliance\Endpoint\DAL\Location\BranchDistancesEndpoint::read()for
   *  available parameters.
   *
   * @return mixed|array<Branch> data objects
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   * @throws InvalidArgumentException
   */
  public function readDistances($data) {
    $result = $this->getBranchDistanceEndpoint()->read($data);

    return $this->export($result);
  }

  /**
   * @param \Drupal\ur_appliance\Data\Google\Place $place
   * @return array|null
   *
   * @throws \Exception
   */
  public function loadFromPlace(Place $place) {
    $result = $this->getBranchDistanceEndpoint()->getByPlace($place);

    return $this->export($result);
  }

  /**
   * @param \Drupal\ur_appliance\Data\Google\Place $place
   * @return null|\Drupal\ur_appliance\Data\DAL\Branch
   *
   * @throws \Exception
   */
  public function loadSingleFromPlace(Place $place) {
    $result = $this->getBranchDistanceEndpoint()->getSingleByPlace($place);

    return $this->export($result);
  }

  /**
   * @param null $placeId
   * @param null $address
   * @return array|null
   *
   * @throws \Exception
   */
  public function loadFromLocation($placeId = null, $address = null) {
    $place = Google::Maps()->getPlace($placeId, $address);

    return $this->export( $this->loadFromPlace($place) );
  }

  /**
   * @param null $placeId
   * @param null $address
   * @return bool|\Drupal\ur_appliance\Data\DAL\Branch
   *
   * @throws \Exception
   */
  public function loadSingleFromLocation($placeId = null, $address = null) {
    $place = Google::Maps()->getPlace($placeId, $address);

    if ($place instanceof Place) {
      return $this->export( $this->loadSingleFromPlace($place) );
    }

    throw new DalException('Unable to load Google Place to find branch.');
  }
}