<?php

namespace Drupal\ur_appliance\Controller\DAL;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Controller\DAL\Customer\AccountController;
use Drupal\ur_appliance\Controller\DAL\Customer\AllocationCodeController;
use Drupal\ur_appliance\Controller\DAL\Customer\ApproversController;
use Drupal\ur_appliance\Controller\DAL\Customer\CreditCardController;
use Drupal\ur_appliance\Controller\DAL\Customer\JobsiteController;
use Drupal\ur_appliance\Controller\DAL\Customer\OrderController;
use Drupal\ur_appliance\Controller\DAL\Customer\ReservationController;
use Drupal\ur_appliance\Controller\DAL\Customer\QuoteController;
use Drupal\ur_appliance\Controller\DAL\Customer\RequestersController;
use Drupal\ur_appliance\Controller\DAL\Customer\TcUserController;
use Drupal\ur_appliance\Controller\DAL\Customer\UserController;
use Drupal\ur_appliance\Controller\DAL\Customer\EquipmentController;

/**
 * Class Customer
 * @package Drupal\ur_appliance\Controller\DAL
 */
class CustomerController extends BaseController {
  /** @var AccountController */
  private $accountController;

  /** @var ApproversController */
  private $approversController;

  /** @var TcUserController */
  private $tcUserController;


  /** @var CreditCardController */
  private $cardController;

  /** @var JobsiteController */
  private $jobsiteController;

  /** @var OrderController */
  private $orderController;

  /** @var RequestersController */
  private $requestersController;

  /** @var UserController */
  private $userController;

  /** @var AllocationCodeController */
  private $allocationCodeController;

  /** @var ReservationController */
  private $reservationController;

  /** @var QuoteController */
  private $quoteController;

  /** @var EquipmentController */
  private $equipmentController;

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Customer\AccountController
   */
  public function Account() {
    if (empty($this->accountController)) {
      $this->accountController = new AccountController($this->provideExportedObjects);
    }

    return $this->accountController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Customer\AllocationCodeController
   */
  public function AllocationCodes() {
    if (empty($this->allocationCodeController)) {
      $this->allocationCodeController = new AllocationCodeController($this->provideExportedObjects);
    }

    return $this->allocationCodeController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Customer\ApproversController
   */
  public function Approvers() {
    if (empty($this->approversController)) {
      $this->approversController = new ApproversController($this->provideExportedObjects);
    }

    return $this->approversController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Customer\RequestersController
   */
  public function Requesters() {
    if (empty($this->requestersController)) {
      $this->requestersController = new RequestersController($this->provideExportedObjects);
    }

    return $this->requestersController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Customer\TcUserController
   */
  public function TcUser() {
    if (empty($this->tcUserController)) {
      $this->tcUserController = new TcUserController($this->provideExportedObjects);
    }

    return $this->tcUserController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Customer\CreditCardController
   */
  public function CreditCard() {
    if (empty($this->cardController)) {
      $this->cardController = new CreditCardController($this->provideExportedObjects);
    }

    return $this->cardController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Customer\JobsiteController
   */
  public function Jobsite() {
    if (empty($this->jobsiteController)) {
      $this->jobsiteController = new JobsiteController($this->provideExportedObjects);
    }

    return $this->jobsiteController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Customer\OrderController
   */
  public function Order() {
    if (empty($this->orderController)) {
      $this->orderController = new OrderController($this->provideExportedObjects);
    }

    return $this->orderController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Customer\UserController
   */
  public function User() {
    if (empty($this->userController)) {
      $this->userController = new UserController($this->provideExportedObjects);
    }

    return $this->userController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Customer\ReservationController
   */
  public function Reservation() {
    if (empty($this->reservationController)) {
      $this->reservationController = new ReservationController($this->provideExportedObjects);
    }

    return $this->reservationController;
  }

  /**
   * @return QuoteController
   */
  public function Quote() {
    if (empty($this->quoteController)) {
      $this->quoteController = new QuoteController($this->provideExportedObjects);
    }

    return $this->quoteController;
  }

  /**
   * @return EquipmentController
   */
  public function Equipment() {
    if (empty($this->equipmentController)) {
      $this->quoteController = new EquipmentController($this->provideExportedObjects);
    }

    return $this->quoteController;
  }

  /**
   * Use this to set the user's email address before doing operations.
   *
   * @param $email
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   * @throws \Exception
   */
  public function setEmail($email) {
    $this->getAdapter()->setUserEmail($email);
  }
}