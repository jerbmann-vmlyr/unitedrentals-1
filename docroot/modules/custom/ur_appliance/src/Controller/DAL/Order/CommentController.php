<?php

namespace Drupal\ur_appliance\Controller\DAL\Order;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalException;

class CommentController extends BaseController {
  /** @var  \Drupal\ur_appliance\Endpoint\DAL\Rental\OrderCommentsEndpoint */
  protected $orderCommentsEndpoint;

  /** @var  \Drupal\ur_appliance\Endpoint\DAL\Rental\CommentsEndpoint */
  protected $commentsEndpoint;

  private function getOrderCommentsObj() {
    if ($this->orderCommentsEndpoint == null) {
      $this->orderCommentsEndpoint = ApiContainer::getService('endpoint.rental.order_comments');
    }

    return $this->orderCommentsEndpoint;
  }

  private function getCommentsObj() {
    if ($this->commentsEndpoint == null) {
      $this->commentsEndpoint = ApiContainer::getService('endpoint.rental.comments');
    }

    return $this->commentsEndpoint;
  }

  public function postOrderComments($data) {
    return $this->getOrderCommentsObj()->post($data);
  }

  public function postRequisitionComments($data) {
    return $this->getCommentsObj()->post($data);
  }

  /**
   * Responsible for determining what type of comment we're sending so that we
   * can call the correct endpoint's "post" method
   *
   * @param $order - Completed order object
   * @param $data - Data supplied by front-end to submit
   */
  public function sendComments(Order $order, $data) {

    if (empty($order->getTransId()) && empty($order->getRequisitionId())) {
      throw new DalException('transId or ReqId cannot both be empty.');
    }

    $itemCommentResponse = $jobsiteCommentResponse = null;
    $items = isset($data['transaction']['items']) ? $data['transaction']['items'] : [];

    /**
     * The UR DAL requires item comments and jobsite comments to be
     * submitted separately.
     *
     * First we will try the item notes, then the jobsite instructions.
     */
    $notes = [];

    /**
     * First check to see if there are item comments.
     * If so, gather and submit them.
     */
    if (!empty($items)) {
      foreach ($items as $catClass => $item) {
        $item = (object) $item;

        if (!empty($item->notes)) {
          /**
           * @TODO: Do we still need to load the node here? Only thing we get that
           * we don't already have is the node's title.
           */
          //$itemNode = load_item_node_by_cat_class($item->cat_class);
          $notes[] = "{$item->catClass}: {$item->notes}";
        }
      }

      if (!empty($notes)) {
        $notes = implode(' | ', $notes);
        $itemCommentResponse = $this->postComment($order, $notes, 'item');

        if ($itemCommentResponse == false) {
          $msg = t("There was a problem saving your item notes. Please call to provide any important information.");
          // @TODO: Determine how our logging will work
          //watchdog(__FUNCTION__, 'Notes not saved: %s', array('%s' => $notes), WATCHDOG_ERROR);
          //drupal_set_message($msg, 'error');
        }
      }
    }

    /**
     * Then check to see if there are jobsite comments.
     * If so, then submit as well.
     */
    if (isset($data['transaction']['instructions']) && !empty($data['transaction']['instructions'])) {
      $text = $data['transaction']['instructions'];
      $jobsiteCommentResponse = $this->postComment($order, $text);

      if ($jobsiteCommentResponse == false) {
        $msg = t("There was a problem saving your job site instructions. Please call to provide any important information.");
        // @TODO: Determine how our logging will work
        //watchdog(__FUNCTION__, 'Instructions not saved: %s', array('%s' => $notes), WATCHDOG_ERROR);
        //drupal_set_message($msg, 'error');
      }
    }

    return [
      'itemResponse' => $itemCommentResponse,
      'jobsiteResponse' => $jobsiteCommentResponse
    ];

  }


  public function read($params) {
    $result = $this->getOrderCommentsObj()->read($params);
    return $this->export($result);
  }

  /**
   * This method will need to determine if we're sending order comments or requisition
   * comments based on what we received from submitting the transaction
   *
   * @param \Drupal\ur_appliance\Data\DAL\Order $order
   *   The completed Transaction object
   * @param $text
   *  The text to submit as a comment
   * @param string $type
   *  delineator to tell us what commentTypeCode to use for the given comment
   * @return mixed
   */
  public function postComment(Order $order, $text, $type = 'jobsite') {
    /**
     * Arbitrary variable we'll use to determine if we're sending Transaction
     * or Requisition comments
     */
    $transType = 'T';

    // The data we'll submit to the endpoint (both endpoints require similar info)
    $data = [];

    // If we have a reqId and transId = 0...
    if (!empty($order->getRequisitionId()) && empty($order->getTransId())) {
      // ... then we know it's going to submit requisition comments
      $transType = 'R';
      $data['requestId'] = (string) $order->getRequisitionId();
    }
    else {
      // ... otherwise we know it's going to submit transaction comments
      $data['transId'] = (string) $order->getTransId();
    }

    // Now check the "type" of comments we're submitting to determine the "commentTypeCode"
    // used for the endpoint.
    if ($type == 'jobsite') {
      $commentTypeCode = ($transType == 'T') ? 'D' : 'DL';
    }
    else {
      // @TODO: Should "O" be replaced with "R" (docs show "D" and "R" as available types)
      $commentTypeCode = ($transType == 'T') ? 'O' : 'TR';
    }

    $data['commentText'] = $text;
    $data['commentType'] = $commentTypeCode;

    // Now that have all the data, submit comments appropriately
    if ($transType == 'T') {
      return $this->postOrderComments($data);
    }
    else if ($transType == 'R') {
      return $this->postRequisitionComments($data);
    }

    // @TODO: Should never happen, but what should happen if we get here?
    return FALSE;
  }
}