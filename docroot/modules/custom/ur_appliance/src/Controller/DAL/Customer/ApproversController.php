<?php


namespace Drupal\ur_appliance\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;

/**
 * Class Approvers
 **
 * @package Drupal\ur_appliance\Controller\DAL\Customer
 */
class ApproversController extends BaseController {
  protected $approverEndpoint;

  /**
   * Retrieves the Approvers endpoint service
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Customer\TcApproversEndpoint
   *
   * @throws \Exception
   */
  public function getApproversEndpoint() {
    if ($this->approverEndpoint == null) {
      $this->approverEndpoint = ApiContainer::getService('endpoint.customer.tc_approvers');
    }

    return $this->approverEndpoint;
  }

  /**
   * @param $accountId
   * @return array<\Drupal\ur_appliance\Data\DAL\Account>|mixed
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Exception
   */
  public function read($accountId) {
    $result = $this->getApproversEndpoint()->read($accountId);

    return $this->export($result);
  }
}