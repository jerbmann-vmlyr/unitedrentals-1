<?php

namespace Drupal\ur_appliance\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;

class ReservationController extends BaseController {
  protected $reservationEndpoint;
  
  /**
   * Retrieves a Reservation (OrderStatus in the DAL) endpoint service.
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Customer\OrderStatusEndpoint
   *
   * @throws \Exception
   */
  public function getReservationEndpoint() {
    if ($this->reservationEndpoint == null) {
      $this->reservationEndpoint = ApiContainer::getService('endpoint.customer.order_status');
    }
    
    return $this->reservationEndpoint;
  }
  
  /**
   * Calls the read() method on Reservation (OrderStatus in the DAL)
   * endpoint service.
   *
   * @param $data
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Reservation
   *  null if the API call failed, otherwise a OrderStatus data object
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   */
  public function read($data) {
    $result = $this->getReservationEndpoint()->read($data);
    return $this->export($result);
  }
}