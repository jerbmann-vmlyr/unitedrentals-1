<?php

namespace Drupal\ur_appliance\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Customer\EquipmentEndpoint AS EquipmentEndpoint;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Exception\NoResultsException;
use Drupal\ur_appliance\Provider\DAL;

/**
 * Class Equipment
 *
 * @package Drupal\ur_appliance\Controller\DAL\Customer
 */
class EquipmentController extends BaseController {
  /** @var EquipmentEndpoint */
  protected $equipmentEndpoint;

  /**
   * @return EquipmentEndpoint
   * @throws \Exception
   */
  protected function getEquipmentEndpoint() {
    if ($this->equipmentEndpoint == null) {
      $this->equipmentEndpoint = ApiContainer::getService('endpoint.customer.equipment');
    }

    return $this->equipmentEndpoint;
  }

  /**
   * @param array $params
   * @return mixed|null|EquipmentEndpoint
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Exception|EndpointException|DalDataException
   */
  public function read(array $params = []) {
    $result =  $this->getEquipmentEndpoint()->read($params);
    return $this->export($result);
  }

  /**
   * @param array $params
   * @return array
   *
   * @throws \Exception
   */
  public function readCombined(array $params = []) {
    $types = '1,2,3,4';

    if (isset($params['types'])) {
      $types = $params['types'];
      unset($params['types']);
    }

    /**
     * Call the read function and then get the results from transactions for
     * rental data and then we need to combine it together.
     */
    try {
      $customerOwned = $this->read($params);
    }
    catch (NoResultsException $e) {
      // Then don't include the results
      $customerOwned = [];
    }

    // Get the transactions rental data
    try {
      $rentals = DAL::Rental()->Transactions()->readAllByItem($params['accountId'], $types);
    }
    catch (NoResultsException $e) {
      // Then don't include the results
      $rentals = [];
    }

    // Now combine the results
    $result = array_merge($customerOwned, $rentals);

    // Then we need to get equipment utilization information
    // @TODO: Figure out how to add the equipment utilization info

    return $this->export($result);
  }
}