<?php


namespace Drupal\ur_appliance\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DataException;
use Drupal\ur_appliance\Provider\DAL;

/**
 * Class TcUser
 **
 * @package Drupal\ur_appliance\Controller\DAL\Customer
 */
class TcUserController extends BaseController {
  protected $tcUserEndpoint;

  /**
   * Retrieves the TcUser endpoint service
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Customer\TcUsersEndpoint
   *
   * @throws \Exception
   */
  public function getTcUserEndpoint() {
    if ($this->tcUserEndpoint == null) {
      $this->tcUserEndpoint = ApiContainer::getService('endpoint.customer.tc_users');
    }

    return $this->tcUserEndpoint;
  }

  /**
   * Retrieves account data from TcUsers.
   *
   * @param $email
   *   The email to use for retrieving a TcUser
   * @param $noAccounts
   *   Determines if we want accounts or if we want a TcUser
   * @param $useAltAuth
   *    Determines whether to bypass user token authorization.
   *
   * @return \Drupal\ur_appliance\Data\DAL\TcUser|array<\Drupal\ur_appliance\Data\DAL\Account>|mixed
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Exception
   */
  public function read($email, $noAccounts = FALSE, $useAltAuth = FALSE) {
    $details = [];

    try {
      /** @var \Drupal\ur_appliance\Data\DAL\TcUser $tcUser */
      $tcUser = $this->getTcUserEndpoint()->read($email, $useAltAuth);

      if (empty($tcUser)) {
        throw new \Exception('Failed to load account information.');
      }

      if ($noAccounts == TRUE) {
        return $tcUser->export();
      }

      // @TODO: This badly needs to be cached
      $accounts = $tcUser->getAccounts();

      if (empty($accounts)) {
        throw new DataException('No accounts found.');
      }

      $accountIds = array_keys($accounts);
      $accountIds = implode(',', $accountIds);

      // We don't want to call the Customer/Account->read() on each account as
      // it's not as performent as calling them all at once with readAll()
      $details = DAL::Customer(true)->Account()->readAll($accountIds);
    }
    catch (DataException $e) {
      return [];
    }

    return $this->export($details);
  }


  /**
   * @param $email
   * @return string
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Exception
   */
  public function readSimple($email) {
    $result = $this->getTcUserEndpoint()->read($email);
    $guid = null;

    if (is_object($result)) {
      $guid = $result->getGuid();
    }

    return $guid;
  }

  /**
   * Creates a new TcUser.
   *
   * @param array $data
   * @return string
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function create(array $data) {
    return $this->getTcUserEndpoint()->postTcUser($data);
  }

  /**
   * Updates an existing TcUser.
   *
   * @param array $data
   * @return bool|null|\Drupal\ur_appliance\Data\DAL\TcUser
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\DataException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function update(array $data) {
    $tcUser = $this->getTcUserEndpoint()->updateTcUser($data);

    /**
     * After updating the TC User, wipe out the token and email variables stored
     * in session in case the user's email has changed. On subsequent calls following
     * this, the adapter will ensure we generate new tokens and set the proper
     * session variables.
     */
    $this->getAdapter()->unsetUserEmail();

    return $tcUser;
  }
}