<?php

namespace Drupal\ur_appliance\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;

class QuoteController extends BaseController {
  protected $quoteEndpoint;
  
  /**
   * Retrieves an Quote endpoint service
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Customer\Quote
   *
   * @throws \Exception
   */
  public function getQuoteEndpoint() {
    if ($this->quoteEndpoint == null) {
      $this->quoteEndpoint = ApiContainer::getService('endpoint.customer.order_status');
    }
    
    return $this->quoteEndpoint;
  }
  
  /**
   * Calls the read() method on Quote endpoint service
   *
   * @param $data
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Quote
   *  null if the API call failed, otherwise a Quote data object
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException|\Exception
   */
  public function read($data) {
    $result = $this->getQuoteEndpoint()->read($data);
    return $this->export($result);
  }
}