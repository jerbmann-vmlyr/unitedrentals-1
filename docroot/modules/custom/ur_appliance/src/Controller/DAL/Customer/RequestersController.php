<?php


namespace Drupal\ur_appliance\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;

/**
 * Class Requesters
 **
 * @package Drupal\ur_appliance\Controller\DAL\Customer
 */
class RequestersController extends BaseController {
  protected $requesterEndpoint;

  /**
   * Retrieves the Requesters endpoint service
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Customer\TcRequestersEndpoint
   *
   * @throws \Exception
   */
  public function getRequestersEndpoint() {
    if ($this->requesterEndpoint == null) {
      $this->requesterEndpoint = ApiContainer::getService('endpoint.customer.tc_requesters');
    }

    return $this->requesterEndpoint;
  }

  /**
   * @param $accountId
   * @return array<\Drupal\ur_appliance\Data\DAL\Account>|mixed
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Exception
   */
  public function read($accountId) {
    $result = $this->getRequestersEndpoint()->read($accountId);

    return $this->export($result);
  }
}