<?php

namespace Drupal\ur_appliance\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Customer\CreditCardsEndpoint;

/**
 * Class CreditCard
 *
 * @TODO: Build this thing out. None of this is working.
 *
 * @package Drupal\ur_appliance\Controller\DAL\Customer
 */
class CreditCardController extends BaseController {
  /** @var CreditCardsEndpoint */
  protected $creditCardsEndpoint;

  /**
   * @return CreditCardsEndpoint
   */
  protected function getCreditCardsEndpoint() {
    if ($this->creditCardsEndpoint == null) {
      $this->creditCardsEndpoint = ApiContainer::getService('endpoint.customer.credit_cards');
    }

    return $this->creditCardsEndpoint;
  }

  /**
   * Gets back a single credit card.
   *
   * @param $cardId
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\CreditCard
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function read($cardId) {
    $cards = $this->getCreditCardsEndpoint()->read($cardId);
    return $this->export($cards);
  }

  /**
   * Gets a list of all credit cards.
   *
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\CreditCard
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function readAll() {
    $cards = $this->getCreditCardsEndpoint()->readAll();
    return $this->export($cards);
  }

  /**
   * Creates a new card entry.
   *
   * @param array $data
   * @return null
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function create(array $data) {
    return $this->getCreditCardsEndpoint()->create($data);
  }

  /**
   * Update an existing credit card.
   *
   * @param array $data
   * @return null
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function update(array $data) {
    return $this->getCreditCardsEndpoint()->update($data);
  }

  /**
   * Deletes an existing card entry.
   *
   * @param int $cardId
   * @return bool|null
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function delete($cardId) {
    return $this->getCreditCardsEndpoint()->delete($cardId);
  }
}