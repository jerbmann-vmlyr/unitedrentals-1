<?php

namespace Drupal\ur_appliance\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalException;

class AllocationCodeController extends BaseController {
  protected $codeTitlesEndpoint;
  protected $codeValuesEndpoint;
  protected $codeTriggersEndpoint;

  /**
   * Retrieves a AllocationRequisitionCodeTitles endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationRequisitionCodeTitlesEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getTitlesEndpoint() {
    if ($this->codeTitlesEndpoint == null) {
      $this->codeTitlesEndpoint = ApiContainer::getService('endpoint.customer.allocation_requisition_code_titles');
    }

    return $this->codeTitlesEndpoint;
  }

  /**
   * Retrieves a AllocationRequisitionCodeValues endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationRequisitionCodeValuesEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getValuesEndpoint() {
    if ($this->codeValuesEndpoint == null) {
      $this->codeValuesEndpoint = ApiContainer::getService('endpoint.customer.allocation_requisition_code_values');
    }

    return $this->codeValuesEndpoint;
  }

  /**
   * Retrieves a AllocationTriggers endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationTriggersEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getTriggersEndpoint() {
    if ($this->codeTriggersEndpoint == null) {
      $this->codeTriggersEndpoint = ApiContainer::getService('endpoint.customer.allocation_triggers');
    }

    return $this->codeTriggersEndpoint;
  }

  /**
   * Gathers and assembles all allocation or requisition codes, values and triggers
   *
   * @param $accountId - Account Id to retrieve allocation codes for
   * @param $codeType - "A" for Acquisitions, "R" for Requisitions
   * @return array
   *
   */
  public function readAll($accountId, $codeType) {
    /** @var \Drupal\ur_appliance\Data\DAL\AllocationCode[] $codes */
    $codes = $this->getTitlesEndpoint()->readAll(['accountId' => $accountId, 'codeType' => $codeType]);

    // Get validation values for any Allocation codes
    $codeValues = $this->getValuesEndpoint()->readAll(['accountId' => $accountId, 'codeType' => $codeType]);
    // Get triggers  for any Allocation codes
    $codeTriggers = $this->getTriggersEndpoint()->readAll(['accountId' => $accountId, 'codeType' => $codeType]);

    foreach ($codes as $code) {
      $code->setValueList($this->assignCodeValues($code, $codeValues));
      $code->setTriggers($this->assignTriggerValues($code, $codeTriggers));
    }

    return $this->export($codes);
  }

  /**
   * @param \Drupal\ur_appliance\Data\DAL\AllocationCode $code
   * @param \Drupal\ur_appliance\Data\DAl\CodeValue[] $codeValues
   * @return array
   */
  public function assignCodeValues($code, $codeValues) {
    $valueList = [];

    foreach ($codeValues as $codeValue) {
      if ($code->getId() == $codeValue->getIdNum() && $code->getCodeType() == $codeValue->getCodeType()) {
        $valueList[] = $codeValue->getVal();
      }
    }

    return $valueList;
  }

  /**
   * @param \Drupal\ur_appliance\Data\DAL\AllocationCode $code
   * @param \Drupal\ur_appliance\Data\Dal\CodeTrigger[] $codeTriggers
   * @return array
   */
  public function assignTriggerValues($code, $codeTriggers) {
    $triggerList = [];

    foreach ($codeTriggers as $trigger) {
      if ($code->getId() == $trigger->getIdNum() && $code->getCodeType() == $trigger->getCodeType()) {
        $triggerList[] = [
          'triggerValue' => $trigger->getTriggerVal(),
          'updateCodeId' => $trigger->getTriggerId(),
          'updateCodeValue' => $trigger->getVal(),
        ];
      }
    }

    return $triggerList;
  }
}
