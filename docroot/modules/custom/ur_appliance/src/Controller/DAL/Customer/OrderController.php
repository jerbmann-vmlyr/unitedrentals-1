<?php

namespace Drupal\ur_appliance\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Data\DAL\Order as CustomerOrder;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionsEndpoint;
use Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\EndpointException;

/**
 * Class Order
 *
 * @TODO: Build this thing out. NONE of this is working or complete. - PT
 *
 * @package Drupal\ur_appliance\Controller\DAL\Customer
 */
class OrderController extends BaseController {
  /** @var TransactionsEndpoint $transactionsEndpoint */
  protected $transactionsEndpoint;

  /** @var RequisitionsEndpoint $transactionsEndpoint */
  protected $requisitionEndpoint;

  /**
   * Retrieves a Transaction endpoint service
   *
   * @return mixed|TransactionsEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getTransactionsEndpoint() {
    if ($this->transactionsEndpoint == null) {
      $this->transactionsEndpoint = ApiContainer::getService('endpoint.rental.transactions');
    }

    return $this->transactionsEndpoint;
  }

  /**
   * Retrieves a Requisition endpoint service.
   *
   * @return mixed|RequisitionsEndpoint
   *
   * @throws \Exception
   */
  public function getRequisitionsEndpoint() {
    if ($this->requisitionEndpoint == null) {
      $this->requisitionEndpoint = ApiContainer::getService('endpoint.rental.requisitions');
    }

    return $this->requisitionEndpoint;
  }

  /**
   * Gets an individual order from the DAL.
   *
   * @param int $accountId
   * @param string $requisitionId
   * @param string $reservationId
   * @return CustomerOrder
   *
   * @throws DalDataException|EndpointException
   * @throws DalException|\Exception
   */
  public function read($accountId, $requisitionId, $reservationId = null) {
    $data = ['accountId' => $accountId, 'id' => $requisitionId];

    // Then load from the requisitions call
    /** @var CustomerOrder $order */
    $order = $this->getRequisitionsEndpoint()->read($data);

    // Load from the transactions table
    if (!empty($reservationId)) {
      $fields = ['accountId' => $accountId, 'id' => $requisitionId];
      $transactionOrder = $this->getTransactionsEndpoint()->read($fields);
      $order->mergeObjects($transactionOrder);
    }

    $order->loadDrupalData();
    return $order;
  }

  public function create() {
    //@TODO: Not sure we want to put this here. Might make more sense just under orders.
  }

  public function update() {
    //
  }
}