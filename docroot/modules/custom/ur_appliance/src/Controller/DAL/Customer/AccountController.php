<?php

namespace Drupal\ur_appliance\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Customer\AccountsEndpoint;
use Drupal\ur_appliance\Endpoint\DAL\Customer\AdAccountsEndpoint;
use Drupal\ur_appliance\Endpoint\DAL\Customer\TcAccountsEndpoint;
use Drupal\ur_appliance\Endpoint\DAL\Customer\TcUsersEndpoint;
use Drupal\ur_appliance\Endpoint\DAL\Customer\ValidatePinsEndpoint;
use Drupal\ur_appliance\Exception\DAL\DalControllerException;

/**
 * Class Account
 *
 * @TODO: Build this thing out. Some of this is working.
 *
 * @package Drupal\ur_appliance\Controller\DAL\Customer
 */
class AccountController extends BaseController {
  /** @var AccountsEndpoint */
  protected $accountsEndpoint;

  /** @var TcAccountsEndpoint */
  protected $tcAccountsEndpoint;

  /** @var AdAccountsEndpoint */
  protected $adAccountsEndpoint;

  /** @var TcUsersEndpoint */
  protected $tcUserEndpoint;

  /** @var ValidatePinsEndpoint */
  protected $validatePinsEndpoint;

  /**
   * @return AccountsEndpoint
   * @throws \Exception
   */
  protected function getAccountsEndpoint() {
    if ($this->accountsEndpoint == null) {
      $this->accountsEndpoint = ApiContainer::getService('endpoint.customer.accounts');
    }

    return $this->accountsEndpoint;
  }

  /**
   * @return TcAccountsEndpoint
   * @throws \Exception
   */
  protected function getTcAccountsEndpoint() {
    if ($this->tcAccountsEndpoint == null) {
      $this->tcAccountsEndpoint = ApiContainer::getService('endpoint.customer.tc_accounts');
    }

    return $this->tcAccountsEndpoint;
  }

  /**
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Customer\AdAccountsEndpoint
   * @throws \Exception
   */
  protected function getAdAccountsEndpoint() {
    if ($this->adAccountsEndpoint == null) {
      $this->adAccountsEndpoint = ApiContainer::getService('endpoint.customer.ad_accounts');
    }

    return $this->adAccountsEndpoint;
  }

  /**
   * @return TcUsersEndpoint
   * @throws \Exception
   */
  protected function getTcUserEndpoint() {
    if ($this->tcUserEndpoint == null) {
      $this->tcUserEndpoint = ApiContainer::getService('endpoint.customer.tc_users');
    }

    return $this->tcUserEndpoint;
  }

  protected function getValidatePinsEndpoint() {
    if ($this->validatePinsEndpoint == null) {
      $this->validatePinsEndpoint = ApiContainer::getService('endpoint.customer.validate_pins');
    }

    return $this->validatePinsEndpoint;
  }

  /**
   * @param $accountId
   * @return mixed|null|\Drupal\ur_appliance\Data\DAL\Account
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Exception
   */
  public function read($accountId) {
    $result =  $this->getAccountsEndpoint()->read($accountId);
    return $this->export($result);
  }

  /**
   * @param $accountIds
   * @return array
   */
  public function readAll($accountIds) {
    $result = $this->getAccountsEndpoint()->readAll($accountIds);
    return $this->export($result);
  }

  /**
   * Creates a new account and then links it with the user.
   *
   * @param array $data
   * @return null
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\DAL\DalControllerException
   * @throws \Exception
   */
  public function create(array $data) {
    // First create the account
    $accountId = $this->getAccountsEndpoint()->create($data);

    if (empty($accountId)) {
      throw new DalControllerException('Unable to create a new account.');
    }

    // Then link the account
    $status = $this->getTcAccountsEndpoint()->link($accountId);

    if ($status == false) {
      throw new DalControllerException("Failed to link account: $accountId to the user.");
    }

    return $accountId;
  }

  /**
   * Performs the update method on the Accounts endpoint
   *
   * @param $data
   * @return array
   */
  public function update($data) {
    $result = $this->getAccountsEndpoint()->update($data);
    return $this->export($result);
  }

  /**
   * Performs a PUT to the /customer/adaccounts endpoint.
   * @param $data
   * @return array|mixed
   * @throws \Exception
   */
  public function updateAdAccount($data) {
    $result = $this->getAdAccountsEndpoint()->update($data);
    return $this->export($result);
  }

  /**
   * Runs the validate pins endpoint method and upon success links the account
   * to the TcUser.
   *
   * @param $data
   *   A key/value array containing the accountId and pin number
   * @return array
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalControllerException
   */
  public function validateAndLink($data) {
    $result = $this->getValidatePinsEndpoint()->read($data);

    // The above call will return the "code" for validating pins
    if ($result == 'Y') {
      $result = $this->linkAccount($data);
    }
    else {
      throw new DalControllerException('Unable to validate pins, please try again');
    }

    return $this->export($result);
  }

  /**
   * Links a user to an account without a pin.
   *
   * @param $data
   *   A key/value array containing the accountId
   * @return array
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalControllerException
   */
  public function linkAccount($data) {
    $account_id = $data['accountId'];
    $user_guid = $data['userGuid'];

    $attachResponse = $this->getTcAccountsEndpoint()->link($account_id, $user_guid);

    // The above endpoint call will only return if it was successful or not.
    if ($attachResponse == TRUE) {
      $result = TRUE;
    }
    else {
      $result  = FALSE;
    }

    return $this->export($result);
  }

  /**
   * Deletes specified account(s) from user's profile.
   *
   * @param $data
   *   A key/value array containing the accountIds
   * @return array
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalControllerException
   */
  public function deleteAccounts($data) {
    $attachResponse = $this->getTcAccountsEndpoint()->delete($data);

    return $this->export($attachResponse);
  }
}
