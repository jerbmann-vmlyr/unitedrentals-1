<?php

namespace Drupal\ur_appliance\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Exception\DAL\DalException;

/**
 * Class Jobsite
 **
 * @package Drupal\ur_appliance\Controller\DAL\Customer
 */
class JobsiteController extends BaseController {
  protected $jobsiteEndpoint;

  /**
   * Retrieves a Jobsite endpoint service
   * @return mixed|\Drupal\ur_appliance\Endpoint\DAL\Customer\JobsitesEndpoint
   *
   * @throws \Exception
   */
  public function getJobsiteEndpoint() {
    if ($this->jobsiteEndpoint == null) {
      $this->jobsiteEndpoint = ApiContainer::getService('endpoint.customer.jobsites');
    }

    return $this->jobsiteEndpoint;
  }

  /**
   * Calls the read() method on Jobsite endpoint service
   *
   * @param $data
   * @return mixed|JobsiteController - Null if the API call failed, otherwise a Jobsite data object
   *
   * @throws DalException|\Exception
   */
  public function read($data) {
    $result = $this->getJobsiteEndpoint()->read($data);
    return $this->export($result);
  }
  
  /**
   * Calls the create() method on Jobsite endpoint service
   *
   * @param $data
   * @return mixed|JobsiteController - Null if the API call failed, otherwise a Jobsite data object
   *
   * @throws DalException|\Exception
   */
  public function create(array $data) {
    $result = $this->getJobsiteEndpoint()->create($data);
    return $this->export($result);
  }
  
  /**
   * Calls the update() method on Jobsite endpoint service
   *
   * @param $data
   * @return mixed|JobsiteController - Null if the API call failed, otherwise a Jobsite data object
   *
   * @throws DalException|\Exception
   */
  public function update($data) {
    $result = $this->getJobsiteEndpoint()->update($data);
    return $this->export($result);
  }
}