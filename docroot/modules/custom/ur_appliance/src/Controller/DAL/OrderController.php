<?php

namespace Drupal\ur_appliance\Controller\DAL;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Controller\DAL\Order\CommentController;
use Drupal\ur_appliance\Controller\DAL\Order\RatesController;
use Drupal\ur_appliance\Controller\DAL\Order\RentalController as OrderRental;

class OrderController extends BaseController {
  /** @var CommentController */
  private $commentController;

  /** @var OrderRental */
  private $rentalController;

  /** @var RatesController */
  private $ratesController;

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Order\CommentController
   */
  public function Comment() {
    if (empty($this->commentController)) {
      $this->commentController = new CommentController($this->provideExportedObjects);
    }

    return $this->commentController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Order\RentalController
   */
  public function Rental() {
    if (empty($this->rentalController)) {
      $this->rentalController = new OrderRental($this->provideExportedObjects);
    }

    return $this->rentalController;
  }

  /**
   * @return \Drupal\ur_appliance\Controller\DAL\Order\RatesController
   */
  public function Rates() {
    if (empty($this->ratesController)) {
      $this->ratesController = new RatesController($this->provideExportedObjects);
    }

    return $this->ratesController;
  }
}