<?php
namespace Drupal\ur_appliance\Controller\DAL;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\Controller\DAL\File\ContractFileController;

class FileController extends BaseController {
  private $contractsController;

  public function ContractFile() {
    if (empty($this->contractsController)) {
      $this->contractsController = new ContractFileController($this->provideExportedObjects);
    }

    return $this->contractsController;
  }
}
