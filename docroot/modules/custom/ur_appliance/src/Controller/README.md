# Basics and Details about the API Appliance Controllers
This document is mostly focused on integrating Asynchronous PHP (via AmPHP) into api_appliance to improve performance of communicating with DAL.

Please see `../../README.md` for an excellent explanation of the api_appliance module.

Currently we are implementing asynchronous php just for /api/v1/equipment/on-rent as a case-study.

Asynchronous PHP Integration
------------------


Basic Philosophical Thinking So Far
------------------
  -> The endpoint controllers have always been the intended to provide the orchestration and organization of handling DAL calls.
  
  * Therefore: This is the logical place to run the Async Loop and break up the DAL calls into smaller chunks to be run asynchronously.
  
  -> Asynchronous chunks have to return Promises.
  
  * This means we need a certain level of abstraction to convert the DAL response to a Promise for the purposes of Async.
  
  -> Each endpoint controller call will have to be refactored individually to utilize Async processes.
  
  * HOW we break up the logic to take advantage of the Loop will be commonly be different.
  * Leverage DAL's SQL layer optimization by taking advantage of how it breaks up requests.
  
  -> Break up resource intensive sub-tasks within the async Loop as well for further performance wins.
  
  * like Item::loadDrupalDate()

README.md formatting examples
---------------------------
Code Example
------------

Example:
```
  /**
   * @group live
   */
  public function testDataExport() {
    $data = [
      'catClass' => ['310-6001', '240-3131'],
      'branchId' => 'J90'
    ];

    $result = DAL::Rental(true)->Rates()->retrieve($data);
    $this->assertNotEmpty($result);
  }
```

The pipelines tests have been modified to exclude all tests that are marked as part of the "live" group. This ensures higher performance testing while allowing us to still do manual tests of the live segment of tests.



Link examples
-------------------------
1. https://grobmeier.solutions/php-beginners-guide-to-unit-testing.html
2. https://code.tutsplus.com/tutorials/hands-on-unit-testing-with-phpunit--net-27454
3. https://code.tutsplus.com/tutorials/better-workflow-in-php-with-composer-namespacing-and-phpunit--net-29384
4. https://code.tutsplus.com/tutorials/all-about-mocking-with-phpunit--net-27252
5. https://code.tutsplus.com/tutorials/the-theory-of-unit-testing-part-1--wp-26114
6. https://code.tutsplus.com/tutorials/the-theory-of-unit-testing-part-2--wp-26157
7. https://code.tutsplus.com/tutorials/the-theory-of-unit-testing-part-3--wp-26176

Inline Code Examples
--------------------
* Installed with this module is a custom drush command to toggle on & off the dal endpoint logger - writes to a csv found at /sites/default

* ```drush/fin ddrush dal-log-toggle ['on'/'off']``` - defaults to off
