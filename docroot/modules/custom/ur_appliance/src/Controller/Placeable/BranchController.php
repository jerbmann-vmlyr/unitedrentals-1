<?php

namespace Drupal\ur_appliance\Controller\Placeable;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;

class BranchController extends BaseController {
  protected $collectionEndpoint;

  /**
   * Retrieves a Rates endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\Placeable\v2\CollectionEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getCollectionEndpoint() {
    if ($this->collectionEndpoint == null) {
      $this->collectionEndpoint = ApiContainer::getService('endpoint.placeable.collection');
    }

    return $this->collectionEndpoint;
  }

  /**
   * Calls the read() method on the Collection endpoint
   *
   * @param $itemId - The Placeable Item ID (not to be confused with Branch ID)
   *
   * @return mixed|array<Location> data objects
   * @throws \Exception
   */
  public function read($itemId) {
    $result = $this->getCollectionEndpoint()->read($itemId);
    return $this->export($result);
  }

  /**
   * Gets all of the branches from Placeable
   *
   * @return array|mixed
   * @throws \Exception
   */
  public function readAll() {
    $result = $this->getCollectionEndpoint()->readAll();
    return $this->export($result);
  }

  /**
   * Perform a basic search
   *
   * @param $keywords
   * @param int $limit
   * @param int $offset
   * @return array|mixed
   *
   * @throws \Exception
   */
  public function search($keywords, $limit = 25, $offset = 0) {
    $result = $this->getCollectionEndpoint()->basicSearch($keywords, $limit, $offset);
    return $this->export($result);
  }

  /**
   * Perform an advanced search
   *
   * @param $data
   * @param int $limit
   * @param int $offset
   * @return array|mixed
   *
   * @throws \Exception
   */
  public function advancedSearch($data, $limit = 25, $offset = 0) {
    $result = $this->getCollectionEndpoint()->advancedSearch($data, $limit, $offset);

    return $this->export($result);
  }

  /**
   * Perform an advanced search raw allowing front end to pass parameters
   * directly to api call.
   *
   * @param $data
   * @return array|mixed
   *
   * @throws \Exception
   */
  public function advancedSearchRaw($data) {
    $result = $this->getCollectionEndpoint()->advancedSearchRaw($data);

    return $this->export($result);
  }

  /**
   * Searches specifically by branch ID
   *
   * @param $branchId
   * @return array|mixed
   *
   * @throws \Exception
   */
  public function searchByBranchId($branchId) {
    $data = ['value' => $branchId, 'field' => 'field.branchId'];
    $result = $this->advancedSearch($data);

    return $this->export($result);
  }
}
