<?php

namespace Drupal\ur_appliance\Controller\Placeable;

use Drupal\ur_appliance\Controller\BaseController;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;

/**
 * Class Search
 *
 * @package Drupal\ur_appliance\Controller\Placeable
 */
class SearchController extends BaseController {
  /** @var  \Drupal\ur_appliance\Endpoint\Placeable\v1\SearchEndpoint */
  protected $searchEndpoint;

  /**
   * Retrieves a Rates endpoint service
   *
   * @return mixed|\Drupal\ur_appliance\Endpoint\Placeable\v1\SearchEndpoint
   *  null if service not found
   *
   * @throws \Exception
   */
  public function getSearchEndpoint() {
    if ($this->searchEndpoint == null) {
      $this->searchEndpoint = ApiContainer::getService('endpoint.placeable.search');
    }

    return $this->searchEndpoint;
  }

  /**
   * Calls the read() method on the Search endpoint
   *
   * @param $keywords
   * @param null $sortOrder
   * @param null $limit
   * @param null $offset
   *
   * @return array|mixed
   *
   * @throws \Exception
   */
  public function read($keywords, $sortOrder = null, $limit = null, $offset = null) {
    // Keywords is a misnomer,
    $filters = null;

    // Keywords could contain "q" and "filters" potentially,
    if (is_array($keywords)) {
      if (isset($keywords['filters'])) {
        $filters = $keywords['filters'];
        unset($keywords['filters']);
      }

      if (isset($keywords['q'])) {
        $keywords = $keywords['q'];
      }
    }

    $result = $this->getSearchEndpoint()->read($keywords, $sortOrder, $limit, $offset, $filters);

    return $this->export($result);
  }
}
