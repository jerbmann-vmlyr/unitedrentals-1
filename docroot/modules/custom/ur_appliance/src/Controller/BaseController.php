<?php

namespace Drupal\ur_appliance\Controller;

use Amp\Deferred;
use Amp\Promise;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Interfaces\AdapterInterface;

/**
 * Class BaseController.
 *
 * @package Drupal\ur_appliance\Controller
 */
class BaseController {
  protected $provideExportedObjects = FALSE;
  protected $adapter;

  /**
   * BaseController constructor.
   *
   * @param bool $exportResults
   *   Export Results.
   */
  public function __construct($exportResults = FALSE) {
    $this->setProvideExportedObjects($exportResults);
  }

  /**
   * Should we provide Exported Objects?
   *
   * @return bool
   *   TRUE | FALSE.
   */
  public function isProvideExportedObjects() {
    return $this->provideExportedObjects;
  }

  /**
   * Set the provided Exported Objects.
   *
   * @param bool $provideExportedObjects
   *   TRUE | FALSE.
   */
  public function setProvideExportedObjects($provideExportedObjects) {
    $this->provideExportedObjects = $provideExportedObjects;
  }

  /**
   * Goes through any result set and exports.
   *
   * Returns single or arrays of results as
   * readable objects if this is enabled.
   *
   * @param array|mixed $data
   *   The result set to be exported.
   *
   * @return array|mixed
   *   Properly formatted result set.
   */
  public function export($data) {
    if ($this->provideExportedObjects) {
      if (is_array($data)) {
        foreach ($data as $idx => $result) {
          $data[$idx] = $this->exportSingle($result);
        }
      }
      else {
        return $this->exportSingle($data);
      }
    }

    return $data;
  }

  /**
   * Goes through any result set and exports.
   *
   * Returns single or arrays of results as readable o
   * bjects formatted as AmPHP Promise for Asynchronous PHP
   * if this is enabled.
   *
   * @param array|mixed $data
   *   The result set to be exported.
   *
   * @return \Amp\Promise
   *   The result set formatted as an AmPHP Promise.
   */
  public function asyncExport($data) {
    $deferred = new Deferred();

    if ($this->provideExportedObjects) {
      if (is_array($data)) {
        foreach ($data as $idx => $result) {
          $data[$idx] = $this->exportSingle($result);
        }
      }
      else {
        $deferred->resolve($this->exportSingle($data));
        return $deferred->promise();
      }
    }

    $deferred->resolve($data);
    return $deferred->promise();
  }

  /**
   * Runs the export functionality on an individual object.
   *
   * If it actually uses the DataSimple class as a base.
   *
   * @param array|mixed $result
   *   A single item from the result set.
   *
   * @return array
   *   Properly formatted single item from the result set.
   */
  public function exportSingle($result) {
    if (is_object($result) && $result instanceof DataSimple) {
      return $result->export();
    }

    return $result;
  }

  /**
   * Gets the DAL Adapter.
   *
   * @return \Drupal\ur_appliance\Adapter\DAL\DalAdapter
   *   An instance of the DAL API Service Adapter.
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   * @throws \Exception
   */
  public function getAdapter() {
    if ($this->adapter == null) {
      $this->adapter = ApiContainer::getService('dal_adapter');
    }

    return $this->adapter;
  }

  /**
   * Used to set the adapter instance.
   *
   * On the rare occasion that we need to have
   * a separate instance.
   *
   * @param \Drupal\ur_appliance\Interfaces\AdapterInterface $adapter
   *   DAL API Service Adapter.
   */
  protected function setAdapter(AdapterInterface $adapter) {
    $this->adapter = $adapter;
  }

}
