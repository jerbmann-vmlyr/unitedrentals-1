<?php

namespace Drupal\ur_appliance\Exception\DAL;

/**
 * Class DataException
 *
 * The basic exceptions for the DAL Appliance.
 *
 * @package Drupal\ur_appliance\Exception
 */
class DalDataException extends \Exception {
  public function __construct($message = '', $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }
}