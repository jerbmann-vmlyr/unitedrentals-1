<?php

namespace Drupal\ur_appliance\Exception\Google;

/**
 * Class GoogleMapsException
 *
 * @package Drupal\ur_appliance\Exception\Google
 */
class GoogleMapsException extends \Exception {
  public function __construct($message = '', $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }
}