<?php

namespace Drupal\ur_appliance\Exception;

/**
 * Class ResponseParseException
 *
 * Exceptions when unable to parse exceptions.
 *
 * @package Drupal\ur_appliance\Exception
 */
class ResponseParseException extends \Exception {
  public function __construct($message = '', $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }
}