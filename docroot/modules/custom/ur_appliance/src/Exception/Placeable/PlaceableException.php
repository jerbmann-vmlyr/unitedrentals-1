<?php

namespace Drupal\ur_appliance\Exception\Placeable;

/**
 * Class Placeable
 *
 * The basic exceptions for Placeable calls.
 *
 * @package Drupal\ur_appliance\Exception\Placeable
 */
class PlaceableException extends \Exception {
  public function __construct($message = '', $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }
}