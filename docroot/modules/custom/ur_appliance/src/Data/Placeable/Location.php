<?php
namespace Drupal\ur_appliance\Data\Placeable;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Exception\DataException;
use Drupal\ur_appliance\Exception\Placeable\PlaceableDataException;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class Location
 *
 * @package Drupal\ur_appliance\Data\Placeable
 */
class Location extends DataSimple implements DataInterface {
  /** @var  string */
  protected $id;

  /** @var  string */
  protected $branchId;

  /** @var  string */
  protected $name;

  /** @var  string */
  protected $primaryCategory;

  /** @var  string */
  protected $refinementBusType;

  /** @var  string */
  protected $latitude;

  /** @var  string */
  protected $longitude;

  /** @var  string */
  protected $address1;

  /** @var  string */
  protected $address2;

  /** @var  string */
  protected $city;

  /** @var  string */
  protected $state;

  /** @var  string */
  protected $zip;

  /** @var  string */
  protected $countryCode;

  /** @var  string */
  protected $syndicationDescription;

  /** @var  string */
  protected $businessType;

  /** @var  string */
  protected $businessTypes;

  /** @var  string */
  protected $weekdayhours;

  /** @var  string */
  protected $saturdayhours;

  /** @var  string */
  protected $sundayhours;

  /** @var  string */
  protected $phone;

  /** @var  string */
  protected $fax;

  /** @var  string */
  protected $managerName;

  /** @var  string */
  protected $branchDescription;

  /** @var  string */
  protected $subheading;

  /** @var  array */
  protected $businessTypeProducts;

  /** @var  string */
  protected $image;

  /** @var  string */
  protected $video;
  
  /** @var  integer */
  protected $region;

  /** @var  string */
  protected $acquiredCompanyCode;

  /**
   * The below properties come through the workbench API, however, we are not
   * making use of them on the front-end. To make our lives easier we're going
   * to keep them here, commented out. This way if we need to access more data
   * in the future we do not have to add a bunch of code. Just move the property
   * out of the docblock and it will get set properly.

  protected $postal;

  protected $inactive;

  protected $newLocations;

  protected $legacyUrl;

  protected $department;

  protected $businessName;

  protected $location;

  protected $contactFormUrls;

  protected $detailsPageTitle;

  protected $alertMessage;

  protected $url;

  protected $country;

  protected $streetAddress;

  protected $description;

  protected $nationalCampaignAdImage;

  protected $nationalCampaignAdUrl;

  protected $nationalCampaignMobileAdImage;

  protected $orderOnlineURLs;

  protected $hours;

  protected $googleHours;

  protected $infoMessage;

  protected $groupId;

  protected $facebookClosed;

  protected $webDisplay;

  protected $langCode;

  protected $count;

  protected $additionalCategories;

  protected $localPage;

  protected $streetAddress2;

  protected $status;

  protected $distance;
  */

  /**
   * @param $data - Data received from the API call
   *
   * @throws PlaceableDataException|\Drupal\ur_appliance\Exception\DataException
   * @throws \Exception
   */
  public function import($data) {
    $data = (object) $data;

    /**
     * Collection results have a "fields" property whereas v1/Search just returns
     * the fields.
     */
    if (property_exists($data, 'fields')) {
      foreach ($data->fields AS $name => $val) {
        $this->setProperties($name, $val);
      }
    }
    else {
      foreach ($data AS $name => $val) {
        $this->setProperties($name, $val);
      }
    }
  }

  /**
   * @return string
   */
  public function getBranchId() {
    return $this->branchId;
  }

  /**
   * @param string $branchId
   */
  public function setBranchId($branchId) {
    $this->branchId = $branchId;
  }

  /**
   * @return string
   */
  public function getPostal() {
    return $this->postal;
  }

  /**
   * @param string $postal
   */
  public function setPostal($postal) {
    $this->postal = $postal;
  }

  /**
   * @return string
   */
  public function getZip() {
    return $this->zip;
  }

  /**
   * @param string $zip
   */
  public function setZip($zip) {
    $this->zip = $zip;
  }

  /**
   * @return bool
   */
  public function isInactive() {
    return $this->inactive;
  }

  /**
   * @param bool $inactive
   */
  public function setInactive($inactive) {
    $this->inactive = $inactive;
  }

  /**
   * @return string
   */
  public function getNewLocations() {
    return $this->newLocations;
  }

  /**
   * @param string $newLocations
   */
  public function setNewLocations($newLocations) {
    $this->newLocations = $newLocations;
  }

  /**
   * @return string
   */
  public function getLegacyUrl() {
    return $this->legacyUrl;
  }

  /**
   * @param string $legacyUrl
   */
  public function setLegacyUrl($legacyUrl) {
    $this->legacyUrl = $legacyUrl;
  }

  /**
   * @return string
   */
  public function getCity() {
    return $this->city;
  }

  /**
   * @param string $city
   */
  public function setCity($city) {
    $this->city = $city;
  }

  /**
   * @return string
   */
  public function getPrimaryCategory() {
    return $this->primaryCategory;
  }

  /**
   * @param string $primaryCategory
   */
  public function setPrimaryCategory($primaryCategory) {
    $this->primaryCategory = $primaryCategory;
  }

  /**
   * @return string
   */
  public function getDepartment() {
    return $this->department;
  }

  /**
   * @param string $department
   */
  public function setDepartment($department) {
    $this->department = $department;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getRefinementBusType() {
    return $this->refinementBusType;
  }

  /**
   * @param string $refinementBusType
   */
  public function setRefinementBusType($refinementBusType) {
    $this->refinementBusType = $refinementBusType;
  }

  /**
   * @return string
   */
  public function getBusinessName() {
    return $this->businessName;
  }

  /**
   * @param string $businessName
   */
  public function setBusinessName($businessName) {
    $this->businessName = $businessName;
  }

  /**
   * @return string
   */
  public function getLocation() {
    return $this->location;
  }

  /**
   * @param string $location
   */
  public function setLocation($location) {
    $this->location = $location;
  }

  /**
   * @return string
   */
  public function getLatitude() {
    return $this->latitude;
  }

  /**
   * @param string $latitude
   */
  public function setLatitude($latitude) {
    $this->latitude = $latitude;
  }

  /**
   * @return string
   */
  public function getLongitude() {
    return $this->longitude;
  }

  /**
   * @param string $longitude
   */
  public function setLongitude($longitude) {
    $this->longitude = $longitude;
  }

  /**
   * @return string
   */
  public function getContactFormUrls() {
    return $this->contactFormUrls;
  }

  /**
   * @param string $contactFormUrls
   */
  public function setContactFormUrls($contactFormUrls) {
    $this->contactFormUrls = $contactFormUrls;
  }

  /**
   * @return string
   */
  public function getDetailsPageTitle() {
    return $this->detailsPageTitle;
  }

  /**
   * @param string $detailsPageTitle
   */
  public function setDetailsPageTitle($detailsPageTitle) {
    $this->detailsPageTitle = $detailsPageTitle;
  }

  /**
   * @return string
   */
  public function getState() {
    return $this->state;
  }

  /**
   * @param string $state
   */
  public function setState($state) {
    $this->state = $state;
  }

  /**
   * @return string
   */
  public function getAlertMessage() {
    return $this->alertMessage;
  }

  /**
   * @param string $alertMessage
   */
  public function setAlertMessage($alertMessage) {
    $this->alertMessage = $alertMessage;
  }

  /**
   * @return string
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * @param string $url
   */
  public function setUrl($url) {
    $this->url = $url;
  }

  /**
   * @return string
   */
  public function getCountry() {
    return $this->country;
  }

  /**
   * @param string $country
   */
  public function setCountry($country) {
    $this->country = $country;
  }

  /**
   * @return string
   */
  public function getCountryCode() {
    return $this->countryCode;
  }

  /**
   * @param string $countryCode
   */
  public function setCountryCode($countryCode) {
    $this->countryCode = $countryCode;
  }

  /**
   * @return string
   */
  public function getStreetAddress() {
    return $this->streetAddress;
  }

  /**
   * @param string $streetAddress
   */
  public function setStreetAddress($streetAddress) {
    $this->streetAddress = $streetAddress;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return string
   */
  public function getNationalCampaignAdImage() {
    return $this->nationalCampaignAdImage;
  }

  /**
   * @param string $nationalCampaignAdImage
   */
  public function setNationalCampaignAdImage($nationalCampaignAdImage) {
    $this->nationalCampaignAdImage = $nationalCampaignAdImage;
  }

  /**
   * @return string
   */
  public function getNationalCampaignAdUrl() {
    return $this->nationalCampaignAdUrl;
  }

  /**
   * @param string $nationalCampaignAdUrl
   */
  public function setNationalCampaignAdUrl($nationalCampaignAdUrl) {
    $this->nationalCampaignAdUrl = $nationalCampaignAdUrl;
  }

  /**
   * @return string
   */
  public function getNationalCampaignMobileAdImage() {
    return $this->nationalCampaignMobileAdImage;
  }

  /**
   * @param string $nationalCampaignMobileAdImage
   */
  public function setNationalCampaignMobileAdImage($nationalCampaignMobileAdImage) {
    $this->nationalCampaignMobileAdImage = $nationalCampaignMobileAdImage;
  }

  /**
   * @return string
   */
  public function getOrderOnlineURLs() {
    return $this->orderOnlineURLs;
  }

  /**
   * @param string $orderOnlineURLs
   */
  public function setOrderOnlineURLs($orderOnlineURLs) {
    $this->orderOnlineURLs = $orderOnlineURLs;
  }

  /**
   * @return integer
   */
  public function getRegion() {
    return $this->region;
  }

  /**
   * @param integer $region
   */
  public function setRegion($region) {
    $this->region = $region;
  }

  /**
   * @return string
   */
  public function getBusinessType() {
    return $this->businessType;
  }

  /**
   * @param string $businessType
   */
  public function setBusinessType($businessType) {
    $this->businessType = $businessType;
  }

  /**
   * @return string
   */
  public function getBusinessTypes() {
    return $this->businessTypes;
  }

  /**
   * @param string $businessTypes
   */
  public function setBusinessTypes($businessTypes) {
    $this->businessTypes = $businessTypes;
  }

  /**
   * @return string
   */
  public function getHours() {
    return $this->hours;
  }

  /**
   * @param string $hours
   */
  public function setHours($hours) {
    $this->hours = $hours;
  }

  /**
   * @return string
   */
  public function getSaturdayHours() {
    return $this->saturdayHours;
  }

  /**
   * @param string $saturdayHours
   */
  public function setSaturdayHours($saturdayHours) {
    $this->saturdayHours = $saturdayHours;
  }

  /**
   * @return string
   */
  public function getSundayHours() {
    return $this->sundayHours;
  }

  /**
   * @param string $sundayHours
   */
  public function setSundayHours($sundayHours) {
    $this->sundayHours = $sundayHours;
  }

  /**
   * @return string
   */
  public function getWeekdayhours() {
    return $this->weekdayhours;
  }

  /**
   * @param string $weekdayhours
   */
  public function setWeekdayhours($weekdayhours) {
    $this->weekdayhours = $weekdayhours;
  }

  /**
   * @return string
   */
  public function getGoogleHours() {
    return $this->googleHours;
  }

  /**
   * @param string $googleHours
   */
  public function setGoogleHours($googleHours) {
    $this->googleHours = $googleHours;
  }

  /**
   * @return string
   */
  public function getAddress2() {
    return $this->address2;
  }

  /**
   * @param string $address2
   */
  public function setAddress2($address2) {
    $this->address2 = $address2;
  }

  /**
   * @return string
   */
  public function getManagerName() {
    return $this->managerName;
  }

  /**
   * @param string $managerName
   */
  public function setManagerName($managerName) {
    $this->managerName = $managerName;
  }

  /**
   * @return string
   */
  public function getInfoMessage() {
    return $this->infoMessage;
  }

  /**
   * @param string $infoMessage
   */
  public function setInfoMessage($infoMessage) {
    $this->infoMessage = $infoMessage;
  }

  /**
   * @return string
   */
  public function getGroupId() {
    return $this->groupId;
  }

  /**
   * @param string $groupId
   */
  public function setGroupId($groupId) {
    $this->groupId = $groupId;
  }

  /**
   * @return string
   */
  public function getFacebookClosed() {
    return $this->facebookClosed;
  }

  /**
   * @param string $facebookClosed
   */
  public function setFacebookClosed($facebookClosed) {
    $this->facebookClosed = $facebookClosed;
  }

  /**
   * @return string
   */
  public function getWebDisplay() {
    return $this->webDisplay;
  }

  /**
   * @param string $webDisplay
   */
  public function setWebDisplay($webDisplay) {
    $this->webDisplay = $webDisplay;
  }

  /**
   * @return string
   */
  public function getLangCode() {
    return $this->langCode;
  }

  /**
   * @param string $langCode
   */
  public function setLangCode($langCode) {
    $this->langCode = $langCode;
  }

  /**
   * @return string
   */
  public function getBranchDescription() {
    return $this->branchDescription;
  }

  /**
   * @param string $branchDescription
   */
  public function setBranchDescription($branchDescription) {
    $this->branchDescription = $branchDescription;
  }

  /**
   * @return string
   */
  public function getCount() {
    return $this->count;
  }

  /**
   * @param string $count
   */
  public function setCount($count) {
    $this->count = $count;
  }

  /**
   * @return string
   */
  public function getAdditionalCategories() {
    return $this->additionalCategories;
  }

  /**
   * @param string $additionalCategories
   */
  public function setAdditionalCategories($additionalCategories) {
    $this->additionalCategories = $additionalCategories;
  }

  /**
   * @return string
   */
  public function getFax() {
    return $this->fax;
  }

  /**
   * @param string $fax
   */
  public function setFax($fax) {
    $this->fax = $fax;
  }

  /**
   * @return string
   */
  public function getLocalPage() {
    return $this->localPage;
  }

  /**
   * @param string $localPage
   */
  public function setLocalPage($localPage) {
    $this->localPage = $localPage;
  }

  /**
   * @return string
   */
  public function getAddress1() {
    return $this->address1;
  }

  /**
   * @param string $address1
   */
  public function setAddress1($address1) {
    $this->address1 = $address1;
  }

  /**
   * @return string
   */
  public function getStreetAddress2() {
    return $this->streetAddress2;
  }

  /**
   * @param string $streetAddress2
   */
  public function setStreetAddress2($streetAddress2) {
    $this->streetAddress2 = $streetAddress2;
  }

  /**
   * @return string
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param string $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * @return float
   */
  public function getDistance() {
    return $this->distance;
  }

  /**
   * @param float $distance
   */
  public function setDistance($distance) {
    $this->distance = $distance;
  }

  /**
   * @return string
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @param string $phone
   */
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getAcquiredCompanyCode() {
    return $this->acquiredCompanyCode;
  }

  /**
   * @param string $acquiredCompanyCode
   */
  public function setAcquiredCompanyCode($acquiredCompanyCode) {
    $this->acquiredCompanyCode = $acquiredCompanyCode;
  }
}