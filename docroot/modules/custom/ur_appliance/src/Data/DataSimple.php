<?php

namespace Drupal\ur_appliance\Data;

use Drupal\ur_appliance\Exception\DataException;
use Drupal\ur_appliance\Traits\MagicSetterTrait;

class DataSimple {
  use MagicSetterTrait;

  /**
   * DataSimple constructor.
   *
   * @param null $data
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function __construct($data = null) {
    if (!empty($data)) {
      $this->import($data);
    }
  }

  /**
   * Import a data set.
   *
   * @param $data
   * @throws DataException|\Exception
   */
  public function import($data) {
    if (!\is_object($data) && !\is_array($data)) {
      throw new DataException('You must provide a data object or array to import from.');
    }

    $data = (array) $data;

    foreach ($data AS $name => $val) {
      $this->setProperties($name, $val);
    }
  }

  /**
   * Helper method to convert boolean values to "Y" or "N" strings and vice versa.
   *
   * @param $value - The boolean to convert to a string
   * @param bool $reverse - Runs the inverse of this method and returns booleans
   *             from "Y" or "N" strings
   * @return string
   */
  public function convertYesNo($value, $reverse = false) {
    if ($reverse) {
      return $value == 'Y';
    }

    return $value == true ? 'Y' : 'N';
  }

  /**
   * @param $value
   * @return string
   */
  public function boolToYN($value) {
    if (\is_bool($value)) {
      $value = $this->convertYesNo($value);
    }
    
    $value = strtoupper($value);
    return $value;
  }

  /**
   * Helper method to remove a prepended string from a data field key and set property.
   *
   * @param string $prepend - The string to remove from the data field
   * @param array $data - The submitted data
   *
   * @throws \Exception
   */
  public function removePrependAndSet($prepend, $data) {
    foreach ($data as $prop => $value) {
      if (stripos($prop, $prepend) !== false) {
        $name = str_replace($prepend, '', $prop);
        $this->setProperties($name, $value);
      }
    }
  }

  /**
   * @return array - Of property values
   */
  public function export() {
    return get_object_vars($this);
  }
}