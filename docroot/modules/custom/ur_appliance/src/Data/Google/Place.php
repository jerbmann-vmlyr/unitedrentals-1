<?php
namespace Drupal\ur_appliance\Data\Google;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class Place
 *
 * @package Drupal\ur_appliance\Data\Google
 */
class Place extends DataSimple implements DataInterface {
  /** @var string */
  protected $id;

  /** @var string */
  protected $name;

  /** @var string */
  protected $placeId;

  /** @var array */
  protected $addressComponents;

  /** @var string */
  protected $adrAddress;

  /** @var array */
  protected $formattedAddress;

  /** @var array */
  protected $geometry;

  /** @var string */
  protected $icon;

  /** @var string */
  protected $reference;

  /** @var string */
  protected $scope;

  /** @var array */
  protected $types;

  /** @var string */
  protected $url;

  /** @var string */
  protected $utcOffset;

  /** @var string */
  protected $vicinity;

  /**
   * Find an address component.
   *
   * @param $type
   * @param bool $shortName
   * @return null|string
   */
  public function findAddressComponent($type, $shortName = false) {
    $components = $this->getAddressComponents();

    foreach ($components AS $component) {
      if (in_array($type, $component->types, false)) {
        if ($shortName) {
          return $component->short_name;
        }

        return $component->long_name;
      }
    }

    return null;
  }

  /**
   * @param bool $shortName
   * @return null|string
   */
  public function getCountry($shortName = false) {
    return $this->findAddressComponent('country', $shortName);
  }

  /**
   * @param bool $shortName
   * @return null|string
   */
  public function getState($shortName = false) {
    return $this->findAddressComponent('administrative_area_level_1', $shortName);
  }

  /**
   * @param bool $shortName
   * @return null|string
   */
  public function getCounty($shortName = false) {
    return $this->findAddressComponent('administrative_area_level_2', $shortName);
  }

  /**
   * @param bool $shortName
   * @return null|string
   */
  public function getCity($shortName = false) {
    return $this->findAddressComponent('locality', $shortName);
  }

  /**
   * @param bool $shortName
   * @return null|string
   */
  public function getPostalCode($shortName = false) {
    return $this->findAddressComponent('postal_code', $shortName);
  }

  /**
   * @return mixed
   */
  public function getLatitude() {
    return $this->getGeometry()->location->lat;
  }

  /**
   * @return mixed
   */
  public function getLongitude() {
    return $this->getGeometry()->location->lng;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getPlaceId() {
    return $this->placeId;
  }

  /**
   * @param string $placeId
   */
  public function setPlaceId($placeId) {
    $this->placeId = $placeId;
  }

  /**
   * @return array
   */
  public function getAddressComponents() {
    return $this->addressComponents;
  }

  /**
   * @param array $addressComponents
   */
  public function setAddressComponents($addressComponents) {
    $this->addressComponents = $addressComponents;
  }

  /**
   * @return string
   */
  public function getAdrAddress() {
    return $this->adrAddress;
  }

  /**
   * @param string $adrAddress
   */
  public function setAdrAddress($adrAddress) {
    $this->adrAddress = $adrAddress;
  }

  /**
   * @return array
   */
  public function getFormattedAddress() {
    return $this->formattedAddress;
  }

  /**
   * @param array $formattedAddress
   */
  public function setFormattedAddress($formattedAddress) {
    $this->formattedAddress = $formattedAddress;
  }

  /**
   * @return array
   */
  public function getGeometry() {
    return $this->geometry;
  }

  /**
   * @param array $geometry
   */
  public function setGeometry($geometry) {
    $this->geometry = $geometry;
  }

  /**
   * @return string
   */
  public function getIcon() {
    return $this->icon;
  }

  /**
   * @param string $icon
   */
  public function setIcon($icon) {
    $this->icon = $icon;
  }

  /**
   * @return string
   */
  public function getReference() {
    return $this->reference;
  }

  /**
   * @param string $reference
   */
  public function setReference($reference) {
    $this->reference = $reference;
  }

  /**
   * @return string
   */
  public function getScope() {
    return $this->scope;
  }

  /**
   * @param string $scope
   */
  public function setScope($scope) {
    $this->scope = $scope;
  }

  /**
   * @return array
   */
  public function getTypes() {
    return $this->types;
  }

  /**
   * @param array $types
   */
  public function setTypes($types) {
    $this->types = $types;
  }

  /**
   * @return string
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * @param string $url
   */
  public function setUrl($url) {
    $this->url = $url;
  }

  /**
   * @return string
   */
  public function getUtcOffset() {
    return $this->utcOffset;
  }

  /**
   * @param string $utcOffset
   */
  public function setUtcOffset($utcOffset) {
    $this->utcOffset = $utcOffset;
  }

  /**
   * @return string
   */
  public function getVicinity() {
    return $this->vicinity;
  }

  /**
   * @param string $vicinity
   */
  public function setVicinity($vicinity) {
    $this->vicinity = $vicinity;
  }
}