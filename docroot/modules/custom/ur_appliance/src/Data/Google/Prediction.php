<?php
namespace Drupal\ur_appliance\Data\Google;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class Prediction
 *
 * @package Drupal\ur_appliance\Data\Google
 */
class Prediction extends DataSimple implements DataInterface {
  /** @var string */
  protected $id;

  /** @var string */
  protected $description;

  /** @var array */
  protected $matchedSubstrings;

  /** @var string */
  protected $placeId;

  /** @var string */
  protected $reference;

  /** @var array */
  protected $structuredFormatting;

  /** @var array */
  protected $terms;

  /** @var array */
  protected $types;

  /**
   * @return mixed
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param mixed $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return mixed
   */
  public function getMatchedSubstrings() {
    return $this->matchedSubstrings;
  }

  /**
   * @param mixed $matchedSubstrings
   */
  public function setMatchedSubstrings($matchedSubstrings) {
    $this->matchedSubstrings = $matchedSubstrings;
  }

  /**
   * @return mixed
   */
  public function getPlaceId() {
    return $this->placeId;
  }

  /**
   * @param mixed $placeId
   */
  public function setPlaceId($placeId) {
    $this->placeId = $placeId;
  }

  /**
   * @return mixed
   */
  public function getReference() {
    return $this->reference;
  }

  /**
   * @param mixed $reference
   */
  public function setReference($reference) {
    $this->reference = $reference;
  }

  /**
   * @return mixed
   */
  public function getStructuredFormatting() {
    return $this->structuredFormatting;
  }

  /**
   * @param mixed $structuredFormatting
   */
  public function setStructuredFormatting($structuredFormatting) {
    $this->structuredFormatting = $structuredFormatting;
  }

  /**
   * @return mixed
   */
  public function getTerms() {
    return $this->terms;
  }

  /**
   * @param mixed $terms
   */
  public function setTerms($terms) {
    $this->terms = $terms;
  }

  /**
   * @return mixed
   */
  public function getTypes() {
    return $this->types;
  }

  /**
   * @param mixed $types
   */
  public function setTypes($types) {
    $this->types = $types;
  }
}