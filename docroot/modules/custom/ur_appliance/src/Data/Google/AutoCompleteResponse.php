<?php
namespace Drupal\ur_appliance\Data\Google;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Exception\Google\GoogleMapsException;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class AutoCompleteResponse
 *
 * @package Drupal\ur_appliance\Data\Google
 */
class AutoCompleteResponse extends DataSimple implements DataInterface {
  /** @var  string */
  protected $id;

  /** @var string */
  protected $status;

  /** @var array<Prediction> */
  protected $predictions = [];

  /**
   * Import a data set.
   *
   * @param $data
   * @throws GoogleMapsException|\Exception
   */
  public function import($data) {
    if (!is_object($data) && !is_array($data)) {
      throw new GoogleMapsException('You must provide a data object or array to import from.');
    }

    $data = (object) $data;

    if (!empty($data->status)) {
      $this->setStatus($data->status);
    }

    if (!empty($data->predictions)) {
      /** @var array $item */
      foreach ($data->predictions AS $item) {
        $this->addPrediction( new Prediction($item) );
      }
    }
  }

  /**
   * Export the data structures.
   *
   * @return array
   */
  public function export() {
    $export = [
      'status' => $this->getStatus(),
      'predictions' => $this->exportPredictions(),
    ];

    return $export;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param mixed $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * @return array
   */
  public function getPredictions() {
    return $this->predictions;
  }

  /**
   * Get the first Prediction given.
   *
   * @return bool|Prediction
   */
  public function getFirstPrediction() {
    if (!empty($this->predictions[0])) {
      return $this->predictions[0];
    }

    return false;
  }

  /**
   * @param array<Prediction> $predictions
   */
  public function setPredictions(array $predictions) {
    $this->predictions = $predictions;
  }

  /**
   * Adds one prediction object to the list of predictions.
   *
   * @param Prediction $prediction
   */
  public function addPrediction(Prediction $prediction) {
    $this->predictions[] = $prediction;
  }

  /**
   * Export the Predictions array.
   *
   * @return array
   */
  public function exportPredictions() {
    $result = [];

    /** @var Prediction $predictions */
    foreach ($this->predictions AS $prediction) {
      $result[] = $prediction->export();
    }

    return $result;
  }
}