<?php
namespace Drupal\ur_appliance\Data\Google;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Data\Google\Place;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class DetailsResponse
 *
 * @package Drupal\ur_appliance\Data\Google
 */
class DetailsResponse extends DataSimple implements DataInterface {
  /** @var  string */
  protected $id;

  /** @var array */
  protected $htmlAttributions;

  /** @var string */
  protected $status;

  /** @var Place */
  protected $result;

  public function import($data) {
    parent::import($data);
    $data = (object) $data;

    if (!empty($data->result)) {
      $this->setResult( new Place($data->result) );
    }
  }

  /**
   * Export the data structures.
   *
   * @return array
   */
  public function export() {
    $export = [
      'htmlAttributions' => $this->getHtmlAttributions(),
      'status' => $this->getStatus(),
      'result' => $this->getResult()->export(),
    ];

    return $export;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return array
   */
  public function getHtmlAttributions() {
    return $this->htmlAttributions;
  }

  /**
   * @param array $htmlAttributions
   */
  public function setHtmlAttributions($htmlAttributions) {
    $this->htmlAttributions = $htmlAttributions;
  }

  /**
   * @return mixed
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param mixed $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * @return \Drupal\ur_appliance\Data\Google\Place
   */
  public function getResult() {
    return $this->result;
  }

  /**
   * @param \Drupal\ur_appliance\Data\Google\Place $result
   */
  public function setResult(Place $result) {
    $this->result = $result;
  }
}