<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class CostAllocation
 * @package Drupal\ur_appliance\Data\DAL
 */
class CostAllocation extends DataSimple implements DataInterface {
  /** @var int */
  protected $transId;

  /** @var int */
  protected $reqId;

  /** @var int */
  protected $transSeqNum;

  /** @var string */
  protected $codeType;

  /** @var int */
  protected $allocationId;

  /** @var int */
  protected $splitSeqNum;

  /** @var string */
  protected $allocationValue;

  /** @var float */
  protected $splitPerc;

  /** @var int */
  protected $splitStartDate;

  /** @var int */
  protected $splitEndDate;

  public function getId() {
    return $this->allocationId;
  }

  public function setId($allocationId) {
    $this->allocationId = $allocationId;
  }

  public function getTransId() {
    return $this->transId;
  }

  public function setTransId($transId) {
    $this->transId = $transId;
  }

  public function getReqId() {
    return $this->transId;
  }

  public function setReqId($reqId) {
    $this->reqId = $reqId;
  }

  public function getTransSeqNum() {
    return $this->transSeqNum;
  }

  public function setTransSeqNum($transSeqNum) {
    $this->transSeqNum = $transSeqNum;
  }

  public function getCodeType() {
    return $this->codeType;
  }

  public function setCodeType($codeType) {
    $this->codeType = $codeType;
  }

  public function getSplitSeqNum() {
    return $this->splitSeqNum;
  }

  public function setSplitSeqNum($splitSeqNum) {
    $this->splitSeqNum = $splitSeqNum;
  }

  public function getAllocationValue() {
    return $this->allocationValue;
  }

  public function setAllocationValue($allocationValue) {
    $this->allocationValue = $allocationValue;
  }

  public function getSplitPerc() {
    return $this->splitPerc;
  }

  public function setSplitPerc($splitPerc) {
    $this->splitPerc = $splitPerc;
  }

  public function getSplitStartDate() {
    return $this->splitStartDate;
  }

  public function setSplitStartDate($splitStartDate) {
    $this->splitStartDate = $splitStartDate;
  }

  public function getSplitEndDate() {
    return $this->splitEndDate;
  }

  public function setSplitEndDate($splitEndDate) {
    $this->splitEndDate = $splitEndDate;
  }
}