<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class Order
 * @package Drupal\ur_appliance\Data\DAL
 */
class Order extends DataSimple implements DataInterface {
  /** @var int */
  protected $transId;

  /** @TODO: Identify what this is for */
  /** @var string */
  protected $requestId;

  /** @var string */
  protected $requisitionId;

  /** @var string */
  protected $reservationQuoteId;

  /** @var string */
  protected $branchId;

  /** @var int */
  protected $accountId;

  /** @var string */
  protected $transType;

  /** @var bool */
  protected $openTrans;

  /** @var \DateTime - Timestamp 2011-12-15T00:00:00:00 */
  protected $createdDateTime;

  /** @var \DateTime - Timestamp 2011-12-15T00:00:00:00 */
  protected $startDateTime;

  /** @var \DateTime - Timestamp 2011-12-15T00:00:00:00 */
  protected $returnDateTime;

  /** @var \DateTime - Timestamp 2011-12-15T00:00:00:00 */
  protected $pickupDateTime;

  /** @var \DateTime - Timestamp 2011-12-15T00:00:00:00 */
  protected $nextPickupDateTime;

  /** @var \DateTime - Timestamp 2011-12-15T00:00:00:00 */
  protected $endDateTime;
  
  /** @var string */
  protected $po;
  
  /** @var string */
  protected $requesterGuid;

  /** @var bool - 'Y' or 'N' */
  protected $rpp;

  /** @var bool */
  protected $hasFASTPdf;
  protected $currency;

  /** @TODO: Identify what this is for */
  /** @var string */
  protected $customerName;

  /** @var bool - 'Y' or 'N' */
  protected $delivery;

  /** @var bool - 'Y' or 'N'*/
  protected $pickup;

  /** @var  bool - 'Y' or 'N' */
  protected $pickupFirm;
  
  /** @var  bool - 'Y' or 'N' */
  protected $urWillPickup;
  
  /** @var string */
  protected $requesterName;

  /** @var string */
  protected $reqGuid;

  /** @var string */
  protected $reqContact;

  /** @var string */
  protected $requesterPhone;

  /** @var string */
  protected $requester;

  /** @var string */
  protected $email;

  /** @var array */
  protected $statusCode;

  /** @var string */
  protected $statusCodeDesc;

  /** @var string */
  protected $reqDateTime;

  /** @var integer */
  protected $quoteId;

  /** @var string */
  protected $coeStatusCode;

  /** @var string */
  protected $updateType;

  /** @var  \Drupal\ur_appliance\Data\DAL\Approver */
  protected $approver;

  /** @var  string */
  protected $approverName;

  /** @var  string */
  protected $approverGuid;
  
  /** @var \DateTime - Timestamp 2011-12-15T00:00:00:00 */
  protected $timeStamp;

  /** @var \Drupal\ur_appliance\Data\DAL\Jobsite */
  protected $jobsite;

  /** @var \Drupal\ur_appliance\Data\DAL\Item[] */
  protected $items;

  /** @var  \Drupal\ur_appliance\Data\DAL\CreditCard */
  protected $creditCard;

  /** @var  \Drupal\ur_appliance\Data\DAL\genComments */
  protected $genComments;

  // @TODO: Determine if this should be it's own object
  /** @var \stdClass - object of totals from charge estimates */
  protected $totals;

  /**
   * The indexing is preset to match the accounting code numbers.
   *
   * @var array - An array of the accounting code strings.
   */
  protected $accountingCodes = [
    1 => '',
    2 => '',
    3 => '',
    4 => '',
    5 => '',
    6 => '',
  ];

  /**
   * @param array|Order $data
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function import($data) {
    if (gettype($data) == 'object' && get_class($data) == Order::class) {
      // Export and then re-import the data.
      $data = $data->export();
    }
    
    if (is_array($data)) {
      $data = (object) $data;
    }

    if (isset($data->items)) {
      $items = $data->items;
      unset($data->items);
    }
    else {
      $items = [$data];
    }

    parent::import($data);

    if (is_array($this->getTotals())) {
      $totals = (object) $this->getTotals();
      $this->setTotals($totals);
    }

    foreach ($data AS $name => $val) {
      if ($name == 'po' || $name == 'poNumber') {
        $this->setPo($val);
      }
      
      if ($name == 'updateType') {
        $this->setUpdateType($val);
      }
      
      if ($name == 'timeStamp') {
        $this->setTimeStamp($val);
      }
    }

    // Handle the various permutations of transId vs transID
    if (empty($this->getTransId()) && !empty($data->transId)) {
      $this->setTransId($data->transId);
    }

    if (empty($this->getTransId()) && !empty($data->transID)) {
      $this->setTransId($data->transID);
    }

    // Set requisitionId
    if (empty($this->getRequisitionId()) && !empty($data->reqId)) {
      $this->setRequisitionId($data->reqId);
    }

    if (empty($this->getRequestId()) && !empty($data->reqID)) {
      $this->setRequisitionId($data->reqID);
    }

    // Convert 'Y' & 'N' strings to booleans
    if (isset($data->delivery)) {
      $this->setDelivery($data->delivery);
    }
    elseif (isset($data->urDeliver)) {
      $this->setDelivery($data->urDeliver);
    }
    else {
      $this->setDelivery(false);
    }

    if (isset($data->pickup)) {
      $this->setPickup($data->pickup);
    }
    elseif (isset($data->urPickup)) {
      $this->setPickup($data->urPickup);
    }
    else {
      $this->setPickup(false);
    }

    if (isset($data->rpp)) {
      $this->setRpp($data->rpp);
    }
    else {
      $this->setRpp(false);
    }

    if (isset($data->pickupFirm)) {
      $this->setPickupFirm($data->pickupFirm);
    }
    else {
      $this->setPickupFirm(false);
    }

    if (isset($data->genComments)) {
      $this->setGenComments($data->genComments);
    }

    if (isset($data->urWillPickup)) {
      $this->setUrWillPickup($data->urWillPickup);
    }
    else {
      $this->setUrWillPickup(false);
    }
    
    // Import items
    $this->buildItems($items);

    // Import jobsite
    $this->buildJobsite($data);

    // Import the credit card
    $this->buildCreditCard($data);

    // Import accounting codes
    $this->buildAccountingCodes($data);

    if (isset($data->approver)) {
      $this->buildApprover($data->approver);
    }
    else {
      if (!empty($data->approverName)) {
        $this->approverName = $data->approverName;
      }

      if (!empty($data->approverGuid)) {
        $this->approverGuid = $data->approverGuid;
      }
    }
  }

  /**
   * Helper method to set the items property of the Order from various
   * permutations on how the data could be sent.
   *
   * @param $items
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function buildItems($items) {
    if (is_array($items)) {
      $items = (object) $items;
    }

    if (!empty($items)) {
      foreach($items as $item => $values) {
        $item = new Item();
        $item->import($values);
        $this->addItem($item);
      }
    }

    if (!empty($items->catClass) && !isset($this->items[$items->catClass])) {
      $item = new Item();
      $item->import($items);
      $this->addItem($item);
    }
  }

  /**
   * Helper method to set the Order's jobsite property
   *
   * @param $data
   *  The submitted data to build a Jobsite from
   */
  public function buildJobsite($data) {
    if (is_array($data)) {
      $data = (object) $data;
    }

    if (!empty($data->jobsite)) {
      $this->jobsite->import($data->jobsite);
    }
    else {
      $this->jobsite = new Jobsite();
      $this->jobsite->import($data);
    }
  }

  /**
   * Helper method to build the credit card property of the Order
   * @param $data
   */
  public function buildCreditCard($data) {
    if (is_array($data)) {
      $data = (object) $data;
    }

    if ((!empty($data->cardId) || !empty($data->creditCard)) && empty($this->creditCard)) {
      $cc = new CreditCard();

      if (!empty($data->cardId)) {
        $cc->import($data);
      }

      if (!empty($data->card)) {
        $cc->import($data->card);
      }

      $this->setCreditCard($cc);
    }
  }

  public function buildApprover($data) {
    if (is_array($data)) {
      $data = (object) $data;
    }

    if (empty($this->approver)) {
      $this->approver = new Approver();
      $this->approver->import($data);
    }

    if (empty($this->getApprover()) && !empty($data->approver)) {
      $this->approver = new Approver();
      $this->approver->import($data);
    }
  }

  /**
   * @param $data
   */
  public function buildAccountingCodes($data) {
    // Check for all accounting codes.
    for ($index = 1; $index < 7; $index++) {
      $property = "acctCode{$index}";

      if (isset($data->$property)) {
        $this->setAccountingCode($index, $data->$property);
      }
    }
  }

  public function postImport($data) {
    //
  }

  /**
   * Implements the export method for the Order object.
   *
   * @return array
   *  An array of the exported Order object.
   */
  public function export() {
    if ($this->getItems()) {
      /** @var \Drupal\ur_appliance\Data\DAL\Item $item */
      foreach ($this->getItems() as $item) {
        $this->items[$item->getCatClass()] = $item->export();
      }
    }

    if ($this->getCreditCard()) {
      $this->creditCard = $this->getCreditCard()->export();
    }

    if ($this->getJobsite()) {
      $this->jobsite = $this->getJobsite()->export();
    }
    
    return parent::export();
  }
  
  /**
   * Prepares the Order object to be used for a DAL Rental Transactions PUT call
   *
   * @return array
   */
  public function putExport() {
    // reserve updateType is for converting quotes to reservations
    if ($this->getUpdateType() === 'reserve') {
      $putBody = [
        'transId' => $this->getQuoteId(),
        'updateType' => $this->getUpdateType(),
        'po' => $this->getPo(),
      ];
    }
    // po number is required and frontend should pass "po" anyways. if empty, pass "NPOR"
    if ($this->getUpdateType() === 'update') {
      $putBody = [
        'transId' => $this->getTransId(),
        'updateType' => $this->getUpdateType(),
        'po' => $this->getPo(),
        'timeStamp' => $this->getTimeStamp(),
      ];
    }
    // transaction updateType is for updating a transaction which is different with "update" (update PO#)
    if ($this->getUpdateType() === 'transaction') {
      $putBody = [
        'transId' => $this->getTransId(),
        'requestId' => $this->getRequestId(),
        'updateType' => $this->getUpdateType(),
        'po' => $this->getPo(),
        'timeStamp' => $this->getTimeStamp(),
        'transSource' => 'W',
        'custOwn' => 'N',
      ];
      
      if ($this->getStartDateTime()) {
        $putBody['startDateTime'] = $this->getStartDateTime();
      }
      
      if ($this->getReturnDateTime()) {
        $putBody['returnDateTime'] = $this->getReturnDateTime();
      }

      if ($this->getDelivery()) {
        $putBody['delivery'] = $this->getDelivery();
      }

      if ($this->getPickup()) {
        $putBody['pickup'] = $this->getPickup();
      }

      if ($this->getBranchId()) {
        $putBody['branchId'] = $this->getBranchId();
      }
    }
    elseif ($this->getUpdateType() === 'transaction' && $this->getJobsite()->getId()) {
      $putBody = [
        'transId' => $this->getQuoteId(),
        'jobId' => $this->getJobsite()->getId(),
        'updateType' => $this->getUpdateType(),
        'statusCode' => $this->getStatusCode(),
        'po' => $this->getPo(),
      ];
    }

    return $putBody;
  }
  
  /**
   * Prepares the Order object to be used for a DAL Rental Requisition PUT call
   *
   * @return array
   */
  public function putRequisitionExport() {
    if ($this->getStartDateTime() || $this->getEndDateTime()) {
      $putBody = [
        'requestId' => $this->getRequestId(),
        'startDateTime' => $this->getStartDateTime(),
        'endDateTime' => $this->getEndDateTime(),
        'statusCode' => $this->getStatusCode(),
        'timeStamp' => $this->getTimeStamp(),
        'updateType' => '',
      ];
    }
    elseif ($this->getJobsite()->getId()) {
      $jobContactPhone = preg_replace('/\D+/', '', $this->getJobsite()->getPhone());
      
      $putBody = [
        'requestId' => $this->getRequestId(),
        'jobId' => $this->getJobsite()->getId(),
        'jobContact' => $this->getJobsite()->getContact(),
        'jobPhone' => $jobContactPhone,
        'jobEmail' => $this->getJobsite()->getEmail(),
        'timeStamp' => $this->getTimeStamp(),
        'statusCode' => $this->getStatusCode(),
      ];
    }
    
    return $putBody;
  }

  /**
   * Prepares the Order object to be used for a DAL Rental Contract Extension PUT call
   *
   * @return array
   */
  public function putContractExtensionExport($data) {
    $putBody = [
      'transId' => $this->getTransId(),
      'requester' => $data['requester'],
      'type' => $data['type'],
      'extensionDateTime' => $data['extensionDateTime'],
    ];

    return $putBody;
  }

  /**
   * Prepares the Order object to be used for a DAL Rental Transactions POST call
   *
   * @TODO: Implement requester, requesterName, requesterPhone
   * @TODO: Implement discounts for totalDelivery and totalPickup
   *
   * @return array
   *  A DAL formatted POST body for submission to the Transactions endpoint
   */
  public function postExport() {
    $jobContactPhone = preg_replace('/\D+/', '', $this->getJobsite()->getPhone());
    $requesterPhone = preg_replace('/\D+/', '', $this->getReqPhone());

    $postBody = [
      'branchId' => $this->getBranchId(),
      'accountId' => $this->getAccountId(),
      'transType' => $this->getTransType(),
      'email' => $this->getEmail(),
      'jobId' => $this->getJobsite()->getId(),
      'jobContactName' => $this->getJobsite()->getContact(),
      'jobContactPhone' => $jobContactPhone,
      'poNumber' => $this->getPo() ? : 'NPOR',
      'startDateTime' => $this->getStartDateTime(),
      'returnDateTime' => $this->getReturnDateTime(),
      'delivery' => $this->getDelivery(),
      'pickup' => $this->getPickup(),
      'pickupFirm' => $this->getPickupFirm(),
      'rpp' => $this->isRpp(),
      'emailPDF' => 'N',
      'transSource' => 'W',
      'eqpCount' => count($this->getItems()),
      'eqpDetails' => $this->exportItems(),
      'totalDelivery' => $this->getTotalDeliveryAmount(),
      'totalPickup' => $this->getTotalPickupAmount(),
      'requester' => $this->getReqEmail(),
      'requesterName' => $this->getRequesterName(),
      'requesterPhone' => $requesterPhone,
      'genComments' => $this->getGenComments(),
    ];

    // If the approver is empty/null or if there isn't an approver email,
    // Otherwise, skip it.
    if (!empty($this->approver) && !empty($this->approver['approverEmail'])) {
      $postBody['approver'] = $this->approver['approverEmail'];
    }

    if (!empty($this->getCreditCard())) {
      $postBody['creditCardTokenId'] = $this->getCreditCard()->getId();
    }

    return $postBody;
  }

  public function mergeObjects(Order $order) {
    $methods = get_class_methods(Order::class);

    foreach ($methods AS $methodName) {
      $excluded = ['getJobsite', 'getItems'];

      /**
       * See if the method is a getter and if so then determine the setter
       * name and call the getter from the passed in order and use it to
       * populate this object via the setter.
       */
      if (strpos($methodName, 'get') === 0 && !in_array($methodName, $excluded)) {
        $setterName = 'set' . substr($methodName, 3);
        $val = $order->$methodName();

        if (!empty($val)) {
          $this->$setterName();
        }
      }

      // Now deal with the jobsite. Works because objects always passed by reference.
      $jobsite = $order->getJobsite();

      if (!empty($this->jobsite)) {
        $this->getJobsite()->mergeObjects($jobsite);
      }
      else {
        $this->setJobsite($jobsite);
      }

      // Finally deal with the items array
      $items = $order->getItems();

      /** @var Item $item */
      foreach ($items AS $item) {
        if (isset($this->items[ $item->getCatClass() ])) {
          $this->items[ $item->getCatClass() ]->mergeObjects($item);
        }
        else {
          $this->addItem($item);
        }
      }
    }
  }

  /**
   * Helper method to return an array of POST-formatted items.
   *
   * @return array
   */
  public function exportItems() {
    $items = [];
    $itemList = $this->getItems();

    /** @var \Drupal\ur_appliance\Data\DAL\Item $item */
    foreach ($itemList as $item) {
      if ($item instanceof Item) {
        $items[] = $item->postExport();
      }
    }

    return $items;
  }

  /**
   * Loads the Drupal data for anything that might come from Drupal.
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function loadDrupalData() {
    /**
     * Just for items at the moment.
     */

    /** @var Item $item */
    foreach ($this->getItems() AS $item) {
      $item->loadDrupalData();
    }
  }

  /**
   * @return \stdClass
   */
  public function getTotals() {
    if (empty($this->totals)) {
      return new \stdClass();
    }

    return $this->totals;
  }

  public function getTotalDeliveryAmount() {
    $totals = $this->getTotals();

    if (!empty($totals->deliveryFee)) {
      return $totals->deliveryFee;
    }

    return 0;
  }

  public function getTotalPickupAmount() {
    $totals = $this->getTotals();

    if (!empty($totals->pickupFee)) {
      return $totals->pickupFee;
    }

    return 0;
  }

  /**
   * @param \stdClass $totals
   */
  public function setTotals($totals) {
    $this->totals = $totals;
  }

  /**
   * @return int
   */
  public function getTransId() {
    return $this->transId;
  }

  /**
   * @param int $transId
   */
  public function setTransId($transId) {
    $this->transId = $transId;
  }

  /**
   * @return string
   */
  public function getRequestId() {
    return $this->requestId;
  }

  /**
   * @param string $requestId
   */
  public function setRequestId($requestId) {
    $this->requestId = $requestId;
  }

  /**
   * @return string
   */
  public function getRequisitionId() {
    return $this->requisitionId;
  }

  /**
   * @param string $requisitionId
   */
  public function setRequisitionId($requisitionId) {
    $this->requisitionId = $requisitionId;
  }

  /**
   * @return string
   */
  public function getReservationQuoteId() {
    return $this->reservationQuoteId;
  }

  /**
   * @param string $reservationQuoteId
   */
  public function setReservationQuoteId($reservationQuoteId) {
    $this->reservationQuoteId = $reservationQuoteId;
  }

  /**
   * @return string
   */
  public function getBranchId() {
    return $this->branchId;
  }

  /**
   * @param string $branchId
   */
  public function setBranchId($branchId) {
    $this->branchId = $branchId;
  }

  /**
   * @return int
   */
  public function getAccountId() {
    return $this->accountId;
  }

  /**
   * @param int $accountId
   */
  public function setAccountId($accountId) {
    $this->accountId = $accountId;
  }

  /**
   * @return string
   */
  public function getTransType() {
    return $this->transType;
  }

  /**
   * @param string $transType
   */
  public function setTransType($transType) {
    $this->transType = $transType;
  }

  /**
   * @return bool
   */
  public function isOpenTrans() {
    return $this->openTrans;
  }

  /**
   * @param bool $openTrans
   */
  public function setOpenTrans($openTrans) {
    $this->openTrans = $openTrans;
  }

  /**
   * @return \DateTime
   */
  public function getCreatedDateTime() {
    return $this->createdDateTime;
  }

  /**
   * @param \DateTime $createdDateTime
   */
  public function setCreatedDateTime($createdDateTime) {
    $this->createdDateTime = $createdDateTime;
  }

  /**
   * @return \DateTime
   */
  public function getStartDateTime() {
    return $this->startDateTime;
  }

  /**
   * @param \DateTime $startDateTime
   */
  public function setStartDateTime($startDateTime) {
    $this->startDateTime = $startDateTime;
  }

  /**
   * @return \DateTime
   */
  public function getReturnDateTime() {
    return $this->returnDateTime;
  }

  /**
   * @param \DateTime $returnDateTime
   */
  public function setReturnDateTime($returnDateTime) {
    $this->returnDateTime = $returnDateTime;
  }

  /**
   * @return \DateTime
   */
  public function getEndDateTime() {
    return $this->endDateTime;
  }
  
  /**
   * @param \DateTime $endDateTime
   */
  public function setEndDateTime($endDateTime) {
    $this->endDateTime = $endDateTime;
  }
  
  /**
   * @return \DateTime
   */
  public function getPickupDateTime() {
    return $this->pickupDateTime;
  }

  /**
   * @param \DateTime $pickupDateTime
   */
  public function setPickupDateTime($pickupDateTime) {
    $this->pickupDateTime = $pickupDateTime;
  }

  /**
   * @return \DateTime
   */
  public function getNextPickupDateTime() {
    return $this->nextPickupDateTime;
  }

  /**
   * @param \DateTime $nextPickupDateTime
   */
  public function setNextPickupDateTime($nextPickupDateTime) {
    $this->nextPickupDateTime = $nextPickupDateTime;
  }

  /**
   * @return string
   */
  public function getPo() {
    return $this->po;
  }

  /**
   * @param string $po
   */
  public function setPo($po) {
    $this->po = $po;
  }
  
  /**
   * @return string
   */
  public function getUpdateType() {
    return $this->updateType;
  }
  
  /**
   * @param string $updateType
   */
  public function setUpdateType($updateType) {
    $this->updateType = $updateType;
  }

  /**
   * @return bool
   */
  public function isRpp() {
    return $this->rpp;
  }

  /**
   * @param string $rpp
   */
  public function setRpp($rpp) {
    $this->rpp = $this->boolToYN($rpp);
  }

  /**
   * @return bool
   */
  public function isHasFASTPdf() {
    return $this->hasFASTPdf;
  }

  /**
   * @param bool $hasFASTPdf
   */
  public function setHasFASTPdf($hasFASTPdf) {
    $this->hasFASTPdf = $hasFASTPdf;
  }

  /**
   * @return mixed
   */
  public function getCurrency() {
    return $this->currency;
  }

  /**
   * @param mixed $currency
   */
  public function setCurrency($currency) {
    $this->currency = $currency;
  }

  /**
   * @return string
   */
  public function getCustomerName() {
    return $this->customerName;
  }

  /**
   * @param string $customerName
   */
  public function setCustomerName($customerName) {
    $this->customerName = $customerName;
  }

  /**
   * @return bool
   */
  public function getDelivery() {
    return $this->delivery;
  }

  /**
   * @param string $delivery
   */
  public function setDelivery($delivery) {
    $this->delivery = $this->boolToYN($delivery);
  }

  /**
   * @return bool
   */
  public function getPickup() {
    return $this->pickup;
  }

  /**
   * @param bool $pickup
   */
  public function setPickup($pickup) {
    $this->pickup = $this->boolToYN($pickup);
  }
  
  /**
   * @return bool
   */
  public function getUrWillPickup() {
    return $this->urWillPickup;
  }
  
  /**
   * @param bool $urWillPickup
   */
  public function setUrWillPickup($urWillPickup) {
    $this->urWillPickup = $this->boolToYN($urWillPickup);
  }

  /**
   * @return string
   */
  public function getRequesterGuid() {
    return $this->requesterGuid;
  }

  /**
   * @param string $requesterGuid
   */
  public function setRequesterGuid($requesterGuid) {
    $this->requesterGuid = $requesterGuid;
  }

  /**
   * @return string
   */
  public function getRequesterName() {
    return $this->requesterName;
  }

  /**
   * @param string $requesterName
   */
  public function setRequesterName($requesterName) {
    $this->requesterName = $requesterName;
  }

  /**
   * @return string
   */
  public function getReqGuid() {
    return $this->reqGuid;
  }

  /**
   * @param string $reqGuid
   */
  public function setReqGuid($reqGuid) {
    $this->reqGuid = $reqGuid;
  }

  /**
   * @return string
   */
  public function getReqContact() {
    return $this->reqContact;
  }

  /**
   * @param string $reqContact
   */
  public function setReqContact($reqContact) {
    $this->reqContact = $reqContact;
  }

  /**
   * @return string
   */
  public function getReqPhone() {
    return $this->requesterPhone;
  }

  /**
   * @param string $requesterPhone
   */
  public function setReqPhone($requesterPhone) {
    $this->requesterPhone = $requesterPhone;
  }

  /**
   * @return string
   */
  public function getReqEmail() {
    return $this->requester;
  }

  /**
   * @param string $requester
   */
  public function setReqEmail($requester) {
    $this->requester = $requester;
  }

  /**
   * @return \Drupal\ur_appliance\Data\DAL\Jobsite
   */
  public function getJobsite() {
    return $this->jobsite;
  }

  /**
   * @param \Drupal\ur_appliance\Data\DAL\Jobsite $jobsite
   */
  public function setJobsite($jobsite) {
    $this->jobsite = $jobsite;
  }

  /**
   * @return array
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * @param $items
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function setItems($items) {
    if (!is_array($items)) {
      throw new DalDataException('Must be setting an array of items to use the setItems method.');
    }

    $this->items = $items;
  }

  /**
   * @param \Drupal\ur_appliance\Data\DAL\Item $item
   */
  public function addItem(Item $item) {
    $catClass = $item->getCatClass();

    if (!isset($this->items[$catClass])) {
      $this->items[$catClass] = $item;
    }
  }

  /**
   * @param $catClass
   */
  public function removeItem($catClass) {
    unset($this->items[$catClass]);
  }

  /**
   * Since Transactions have 2 IDs (transaction and allocation) then this
   * helper method will determine which to return
   *
   * @return int|string
   */
  public function getId() {
    if ($this->isTransaction()) {
      return $this->getTransId();
    }

    return $this->getRequisitionId();
  }

  /**
   * Helper method to determine if the order is a transaction.
   * @return bool
   */
  public function isTransaction() {
    $isTransaction = true;

    // If the transId is 0 and we have a requisitionId we know
    // we aren't a transaction
    if (empty($this->getTransId()) && !empty($this->getRequisitionId())) {
      $isTransaction = false;
    }

    return $isTransaction;
  }

  /**
   * @return bool
   */
  public function getPickupFirm() {
    return $this->pickupFirm;
  }

  /**
   * @param string $pickupFirm
   */
  public function setPickupFirm($pickupFirm) {
    $this->pickupFirm = $this->boolToYN($pickupFirm);
  }

  public function setGenComments($genComments) {
    $this->genComments = $genComments;
  }

  public function getGenComments() {
    return $this->genComments;
  }

  public function getEmail() {
    return $this->email;
  }

  public function setEmail($email) {
    $this->email = $email;
  }

  public function getCreditCard() {
    return $this->creditCard;
  }

  public function setCreditCard(CreditCard $card) {
    $this->creditCard = $card;
  }

  /**
   * @return \Drupal\ur_appliance\Data\DAL\Approver
   */
  public function getApprover() {
    return $this->approver;
  }

  /**
   * @return \Drupal\ur_appliance\Data\DAL\Approver
   */
  public function getApproverEmail() {
    return $this->approver['approverEmail'];
  }

  /**
   * @param \Drupal\ur_appliance\Data\DAL\Approver $approver
   */
  public function setApprover(Approver $approver) {
    $this->approver = $approver;
  }

  /**
   * @return array
   */
  public function getStatusCode() {
    return $this->statusCode;
  }

  public function setStatusCode($statusCode) {
    $this->statusCode = $statusCode;
  }

  /**
   * @return string
   */
  public function getStatusCodeDesc() {
    return $this->statusCodeDesc;
  }

  public function setStatusCodeDesc($statusCodeDesc) {
    $this->statusCodeDesc = $statusCodeDesc;
  }

  /**
   * @return string
   */
  public function getReqDateTime() {
    return $this->reqDateTime;
  }

  public function setReqDateTime($reqDateTime) {
    $this->reqDateTime = $reqDateTime;
  }

  /**
   * @return integer
   */
  public function getQuoteId() {
    return $this->quoteId;
  }

  public function setQuoteId($quoteId) {
    $this->quoteId = $quoteId;
  }

  /**
   * @return string
   */
  public function getCoeStatusCode() {
    return $this->coeStatusCode;
  }

  public function setCoeStatusCode($coeStatusCode) {
    $this->coeStatusCode = $coeStatusCode;
  }
  
  /**
   * @return \DateTime
   *
   * We do need to pull the timeStamp to Update Requisition while
   * making a PUT method.
   * This ensures no one else has updated the same record while
   * they were editing the record
   *
   */
  public function getTimeStamp() {
    return $this->timeStamp;
  }
  
  /**
   * @param \DateTime $timeStamp
   */
  public function setTimeStamp($timeStamp) {
    $this->timeStamp = $timeStamp;
  }

  /**
   * Set a particular accounting code.
   *
   * @param int $num
   * @param string $val - It will always store this as a string.
   */
  public function setAccountingCode($num, $val) {
    $this->accountingCodes[$num] = (string) $val;
  }

  /**
   * Gets a particular accounting code.
   *
   * @param int $num
   * @return string
   */
  public function getAccountingCode($num) {
    return $this->accountingCodes[$num];
  }
}
