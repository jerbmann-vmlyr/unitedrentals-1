<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

class Rate extends DataSimple implements DataInterface {
  /** @var string */
  protected $branchId;

  /** @var string */
  protected $catClass;

  /** @var float|int */
  protected $dayRate;

  /** @var float|int */
  protected $weekRate;

  /** @var float|int */
  protected $monthRate;

  /** @var float|int */
  protected $minRate;

  /** @var float|int */
  protected $minRateDuration;

  /** @var string */
  protected $rateType;

  /** @var string */
  protected $expirationDate;

  /** @var string */
  protected $currency;

  /** @var string */
  protected $rpp;

  /** @var string */
  protected $dspRateCatalog;

  /** @var float|int */
  protected $deliveryCharge;

  /** @var array */
  protected $rateTiers = [];

  /** @var string */
  protected $rateZone;

  /** @var bool - Dynamically generated value */
  protected $contractPricing = false;

  /**
   * Determines if contract pricing is currently active.
   *
   * @return bool
   */
  public function isContractPricing() {
    // These rate types determine if contract pricing is enabled currently.
    $contractPricingRateTypes = ['fixed', 'not to exceed'];

    return in_array(strtolower($this->rateType), $contractPricingRateTypes);
  }

  /**
   * @return bool
   */
  public function getContractPricing() {
    if (empty($this->contractPricing)) {
      $this->contractPricing = $this->isContractPricing();
    }

    return $this->contractPricing;
  }

  public function getId() {
    return $this->branchId;
  }

  public function getCatClass() {
    return $this->catClass;
  }

  public function getDayRate() {
    return $this->dayRate;
  }

  public function getWeekRate() {
    return $this->weekRate;
  }

  public function getMonthRate() {
    return $this->monthRate;
  }

  public function getMinRate() {
    return $this->minRate;
  }

  public function getMinRateDuration() {
    return $this->minRateDuration;
  }

  public function getRateType() {
    return $this->rateType;
  }

  public function getExpirationDate() {
    return $this->expirationDate;
  }

  public function getCurrency() {
    return $this->currency;
  }

  public function getRpp() {
    return $this->rpp;
  }

  public function getDspRateCatalog() {
    return $this->dspRateCatalog;
  }

  public function getDeliveryCharge() {
    return $this->deliveryCharge;
  }

  public function getRateTiers() {
    return $this->rateTiers;
  }

  public function getRateZone() {
    return $this->rateZone;
  }

  public function import($data) {
    parent::import($data);

    if (is_object($data)) {
      $data = (array) $data;
    }

    if (isset($data['category']) && isset($data['class'])) {
      $this->catClass = $data['category'] . "-" . $data['class'];
    }

    if (isset($data['minrate'])) {
      $this->minRate = $data['minrate'];
    }

    if (isset($data['dayrate'])) {
      $this->dayRate = $data['dayrate'];
    }

    if (isset($data['weekrate'])) {
      $this->weekRate = $data['weekrate'];
    }

    if (isset($data['monthrate'])) {
      $this->monthRate = $data['monthrate'];
    }

    // Makes sure contract pricing gets set
    $this->contractPricing = $this->isContractPricing();
  }
}