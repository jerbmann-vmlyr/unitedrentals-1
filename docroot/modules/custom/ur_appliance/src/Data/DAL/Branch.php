<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

class Branch extends DataSimple implements DataInterface {
  /* @var string */
  protected $id;

  /* @var string */
  protected $name;

  // TODO: Create address object to hold all this. Address object should also have,
  // capability of storing latitude and longitude values.
  /* @var string */
  protected $address1;

  /* @var string */
  protected $address2;

  /* @var string */
  protected $city;

  /* @var string */
  protected $state;

  /* @var string */
  protected $zip;

  /* @var string */
  protected $phone;

  /* @var string */
  protected $fax;

  // Do we want to do any processing on this?
  // returns example "7AM-5:30PM"
  /* @var string */
  protected $weekdayhours;

  // Same as above
  /* @var string */
  protected $saturdayhours;

  // Same as above
  /* @var string */
  protected $sundayhours;

  /* @var string */
  protected $timeOffset;

  /* @var string */
  protected $district;
  
  /* @var string */
  protected $daylightSavings;
  
  /* @var string */
  protected $managerName;

  /* @var string */
  protected $managerEmail;

  /* @var string */
  protected $reportHeading;

  /* @var string */
  protected $status;

  /* @var string */
  protected $langCode;

  /* @var string */
  protected $currency;

  /* @var string */
  protected $countryCode;

  /* @var string */
  protected $businessType;

  /* @var bool */
  protected $webDisplay;

  /* @var string */
  protected $onSite;

  /* @var float */
  protected $latitude;

  /* @var float */
  protected $longitude;

  /* @var bool */
  protected $formerRSC;

  /* @var string */
  protected $acquiredCompanyCode;

  /* @var array */
  protected $businessTypes = [];

  /* @var string */
  protected $serviceHub;

  /* @var string */
  protected $hub;

  /* @var string */
  protected $hubDesc;

  /* @var int */
  protected $webRefreshRate;

  /* @var int */
  protected $capacityPct;

  /* @var string */
  protected $districtName;

  /* @var string */
  protected $districtMgr;

  /* @var string */
  protected $districtMgrEm;

  /* @var string */
  protected $districtMgrPh;

  /* @var string */
  protected $serviceContact1;

  /* @var string */
  protected $serviceContact1Em;

  /* @var string */
  protected $serviceContact1Ph;

  /* @var string */
  protected $serviceContact2;

  /* @var string */
  protected $serviceContact2Em;

  /* @var string */
  protected $serviceContact2Ph;

  /* @var integer */
  protected $distance;

  /* @var integer */
  protected $region;

  /* @var bool */
  protected $onlineOrders;

  /* @var string */
  protected $description;

  /* @var string */
  protected $contentBlock;

  /* @var string */
  protected $videoUrl;

  /* @var string */
  protected $webProductType;

  public function getId() {
    return $this->id;
  }

  public function getName() {
    return $this->name;
  }

  public function getAddress1() {
    return $this->address1;
  }

  public function getAddress2() {
    return $this->address2;
  }

  public function getCity() {
    return $this->city;
  }

  public function getState() {
    return $this->state;
  }

  public function getZip() {
    return $this->zip;
  }

  public function getPhone() {
    return $this->phone;
  }

  public function getFax() {
    return $this->fax;
  }

  public function getWeekdayHours() {
    return $this->weekdayhours;
  }

  public function getSaturdayHours() {
    return $this->saturdayhours;
  }

  public function getSundayHours() {
    return $this->sundayhours;
  }

  public function getTimeOffset() {
    return $this->timeOffset;
  }

  public function getDistrict() {
    return $this->district;
  }
  
  public function getDaylightSavings() {
    return $this->daylightSavings;
  }
  
  public function getManagerName() {
    return $this->managerName;
  }

  public function getManagerEmail() {
    return $this->managerEmail;
  }

  public function getReportHeading() {
    return $this->reportHeading;
  }

  public function getStatus() {
    return $this->status;
  }

  public function getLangCode() {
    return $this->langCode;
  }

  public function getCurrencyType() {
    return $this->currency;
  }

  public function getCountryCode() {
    return $this->countryCode;
  }

  public function getBusinessType() {
    return $this->businessType;
  }

  public function getWebDisplay() {
    return $this->webDisplay;
  }

  public function getOnSite() {
    return $this->onSite;
  }

  public function getLatitude() {
    return $this->latitude;
  }

  public function getLongitude() {
    return $this->longitude;
  }

  public function getFormerRSC() {
    return $this->formerRSC;
  }

  public function getAcquiredCompanyCode() {
    return $this->acquiredCompanyCode;
  }

  public function getBusinessTypes() {
    return $this->businessTypes;
  }

  public function getServiceHub() {
    return $this->serviceHub;
  }

  public function getHub() {
    return $this->hub;
  }

  public function getHubDescription() {
    return $this->hubDesc;
  }

  public function getWebRefreshRate() {
    return $this->webRefreshRate;
  }

  public function getCapacityPercentage() {
    return $this->capacityPct;
  }

  public function getDistrictName() {
    return $this->districtName;
  }

  public function getDistrictManager() {
    return $this->districtMgr;
  }

  public function getDistrictManagerEmail() {
    return $this->districtMgrEm;
  }

  public function getDistrictManagerPhone() {
    return $this->districtMgrPh;
  }

  public function getServiceContact1() {
    return $this->serviceContact1;
  }

  public function getServiceContact1Email() {
    return $this->serviceContact1Em;
  }

  public function getServiceContact1Phone() {
    return $this->serviceContact1Ph;
  }

  public function getServiceContact2() {
    return $this->serviceContact2;
  }

  public function getServiceContact2Email() {
    return $this->serviceContact2Em;
  }

  public function getServiceContact2Phone() {
    return $this->serviceContact2Ph;
  }

  public function getDistance() {
    return $this->distance;
  }

  /**
   * @return integer
   */
  public function getRegion() {
    return $this->region;
  }

  /**
   * @param integer $region
   */
  public function setRegion($region) {
    $this->region = $region;
  }

  /**
   * @return bool
   */
  public function getOnlineOrders()
  {
    return $this->onlineOrders;
  }

  /**
   * @param bool $onlineOrders
   */
  public function setOnlineOrders($onlineOrders)
  {
    $this->onlineOrders = $onlineOrders;
  }

  /**
   * @return mixed
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * @param mixed $description
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * @return string
   */
  public function getContentBlock()
  {
    return $this->contentBlock;
  }

  /**
   * @param string $contentBlock
   */
  public function setContentBlock($contentBlock)
  {
    $this->contentBlock = $contentBlock;
  }

  /**
   * @return string
   */
  public function getVideoUrl()
  {
    return $this->videoUrl;
  }

  /**
   * @param string $videoUrl
   */
  public function setVideoUrl($videoUrl)
  {
    $this->videoUrl = $videoUrl;
  }

  /**
   * @return string
   */
  public function getWebProductType()
  {
    return $this->webProductType;
  }

  /**
   * @param string $webProductType
   */
  public function setWebProductType($webProductType)
  {
    $this->webProductType = $webProductType;
  }

  /**
   * import overrides the parent functionality to fix inconsistency with some field names from the DAL.
   * This fixes a defect where these fields do not get properly set, as it is assumed they are camelCase.
   * @param $data array of branch data
   */
  public function import($data) {
    $fieldMap = [
      'id' => 'rmBranchID',
      'address1' => 'rmAddr1',
      'address2' => 'address2_placeholder',
      'city' => 'rmCity',
      'state' => 'rmStateProv',
      'zip' => 'rmPostalCode',
      'countryCode' => 'rmCountry',
      'phone' => 'rmPhoneNum',
      'fax' => 'rmFaxNum',
      'weekdayhours' => 'rmMFHours',
      'saturdayhours' =>'rmSatHours',
      'sundayhours' => 'rmSunHours',
      'managerName' => 'rmBranchMgr',
      'managerEmail' => 'managerEmail_placeholder',
      'reportHeading' => 'rmBranchName',
      'timeOffset' => 'rmTimeOffset',
      'daylightSavings' => 'rmDaylightSavings',
      'district' => 'rmDistrictID',
      'region' => 'rmRegionID',
      'status' => 'rmStatus',
      'businessType' => 'rmBusinessType',
      'webDisplay' => 'rmWebDisplay',
      'latitude' => 'rmLatitude',
      'longitude' => 'rmLongitude',
      'businessTypes' => 'businessTypes_placeholder',
      'name' => 'webLocName',
      'description' => 'webDescription',
      'contentBlock' => 'webContentBlock',
      'videoUrl' => 'webVideoUrl',
      'acquiredCompanyCode' => 'rmCompanyCode'];

    foreach ($fieldMap as $branchField => $dalField) {
      if (isset($data->$dalField)) {
        $data->$branchField = $data->$dalField;
        unset($data->$dalField);
      }
    }
    if(isset($data->webOrders)){
      $data->onlineOrders = $this->convertYesNo($data->webOrders, true);
    }
    else {
      $data->onlineOrders = (isset($data->onlineOrders) && is_bool($data->onlineOrders)) ? $data->onlineOrders : false;
    }
    parent::import($data);
  }
}