<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class Reservation
 * @package Drupal\ur_appliance\Data\DAL
 */
class Reservation extends DataSimple implements DataInterface {
  /** @var string - Account ID */
  protected $accountId;
  
  /** @var string - Requisition ID */
  protected $requisitionId;
  
  /** @var string - Requisition cancelled */
  protected $requisitionCancelled;
  
  /** @var integer - Reservation ID */
  protected $reservationId;
  
  /** @var integer - Quote ID */
  protected $quoteId;
  
  /** @var string - Start Date */
  protected $startDate;
  
  /** @var  string - Job ID */
  protected $jobId;
  
  /** @var  string - Job Name */
  protected $jobName;
  
  /** @var  string - Order Status */
  protected $orderStatus;
  
  /** @var  string - Order Status Code */
  protected $orderStatusCode;
  
  /** @var  string - Created Date */
  protected $createdDate;
  
  /** @var array \Drupal\ur_appliance\Data\DAL\Order $reservationStat */
  protected $reservationStat = [];
  
  /**
   * @return string
   */
  public function getId() {
    return $this->accountId;
  }
  
  /**
   * @return string
   */
  public function getRequisitionID() {
    return $this->requisitionId;
  }
  
  /**
   * @return string
   */
  public function getRequisitionCancelled() {
    return $this->requisitionCancelled;
  }
  
  /**
   * @return integer
   */
  public function getReservationID() {
    return $this->reservationId;
  }
  
  /**
   * @return string
   */
  public function getQuoteID() {
    return $this->quoteId;
  }
  
  /**
   * @return string
   */
  public function getStartDate() {
    return $this->startDate;
  }
  
  /**
   * @return string
   */
  public function getJobId() {
    return $this->jobId;
  }
  
  /**
   * @return string
   */
  public function getJobName() {
    return $this->jobName;
  }
  
  /**
   * @return string
   */
  public function getReservationStatus() {
    return $this->orderStatus;
  }
  
  /**
   * @return string
   */
  public function getReservationStatusCode() {
    return $this->orderStatusCode;
  }
  
  /**
   * @return string
   */
  public function getReservationCreatedDate() {
    return $this->createdDate;
  }
}