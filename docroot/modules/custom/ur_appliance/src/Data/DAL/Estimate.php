<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;
use Drupal\ur_appliance\Data\DAL\Item;

class Estimate extends DataSimple implements DataInterface {
  /** @var string */
  protected $branchId;

  /** @var int */
  protected $accountId;

  /** @var int */
  protected $jobsiteId;

  /** @var string - Y or N */
  protected $delivery;

  /** @var string - Y or N */
  protected $pickup;

  /** @var string - Format: Y-m-d\TH:i:s */
  protected $startDate;

  /** @var string - Format: Y-m-d\TH:i:s */
  protected $returnDate;

  /** @var string - Y or N, Default, never changes */
  protected $webRates = 'Y';

  /** @var string - Y or N */
  protected $rpp = 'N';

  /** @var string - Default, never changes */
  protected $omitNonDspRates = 'true';

  /** @var int - Delivery cost override */
  protected $deliveryCost;

  /** @var int - Pickup cost override */
  protected $pickupCost;

  /** @var string - General cost override */
  protected $costOverride;

  /** @var array<Item> */
  protected $items = [];

  /** @var array */
  protected $catClassAndQty = [];

  /** @var array */
  protected $catClassQtyRates = [];

  /** @var array - Of estimate totals from the DAL */
  protected $totals = [];

  /**
   * A class to create data objects from.
   */
  public function getId() {
    // No ID for this data object
  }

  /**
   * @return string
   */
  public function getBranchId() {
    return $this->branchId;
  }

  /**
   * @param string $branchId
   */
  public function setBranchId($branchId) {
    $this->branchId = $branchId;
  }

  /**
   * @return int
   */
  public function getAccountId() {
    return $this->accountId;
  }

  /**
   * @param int $accountId
   */
  public function setAccountId($accountId) {
    $this->accountId = $accountId;
  }

  /**
   * @return int
   */
  public function getJobsiteId() {
    return $this->jobsiteId;
  }

  /**
   * @param int $jobsiteId
   */
  public function setJobsiteId($jobsiteId) {
    $this->jobsiteId = $jobsiteId;
  }

  /**
   * @return string
   */
  public function getDelivery() {
    return $this->delivery;
  }

  /**
   * @param string $delivery
   */
  public function setDelivery($delivery) {
    if (strtoupper($delivery) == 'Y' || $delivery === true) {
      $this->delivery = 'Y';
    }
    else if (strtoupper($delivery) == 'N' || $delivery === false) {
      $this->delivery = 'N';
    }
  }

  /**
   * @return string
   */
  public function getPickup() {
    return $this->pickup;
  }

  /**
   * @param string $pickup
   */
  public function setPickup($pickup) {
    if (strtoupper($pickup) == 'Y' || $pickup === true) {
      $this->pickup = 'Y';
    }
    else if (strtoupper($pickup) == 'N' || $pickup === false) {
      $this->pickup = 'N';
    }
  }

  /**
   * @return string - Format: Y-m-d\TH:i:s
   */
  public function getStartDate() {
    return $this->startDate;
  }

  /**
   * @param string $startDate - Format: Y-m-d\TH:i:s
   */
  public function setStartDate($startDate) {
    if (is_numeric($startDate)) {
      $startDate = Date::formatDate($startDate);
    }

    $this->startDate = $startDate;
  }

  /**
   * @return string - Format: Y-m-d\TH:i:s
   */
  public function getReturnDate() {
    return $this->returnDate;
  }

  /**
   * @param string $returnDate - Format: Y-m-d\TH:i:s
   */
  public function setReturnDate($returnDate) {
    if (is_numeric($returnDate)) {
      $returnDate = Date::formatDate($returnDate);
    }

    $this->returnDate = $returnDate;
  }

  /**
   * NOTE: There is no SET function for this parameter because webRates should
   *       ALWAYS return "Y".
   * @return string
   */
  public function getWebRates() {
    return $this->webRates;
  }

  /**
   * @return string
   */
  public function getRpp() {
    return $this->rpp;
  }

  /**
   * @param string $rpp
   */
  public function setRpp($rpp) {
    if (strtoupper($rpp) == 'Y' || $rpp === true) {
      $this->rpp = 'Y';
    }
    else if (strtoupper($rpp) == 'N' || $rpp === false) {
      $this->rpp = 'N';
    }
  }

  public function getOmitNonDspRates() {
    return $this->omitNonDspRates;
  }

  /**
   * @return array
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * @param array $items - Array of Item objects
   */
  public function setItems(array $items) {
    $tmpItems = [];

    /** @var Item $item */
    foreach ($items AS $item) {
      $tmpItems[$item->getCatClass()] = $item;
    }

    $this->items = $tmpItems;

    if (!empty($items)) {
      $this->setCatClassAndQty(
        $this->buildCatClassAndQty($items)
      );
    }
  }

  public function setItemRates($rates) {
    if (!is_array($rates) || empty($rates)) {
      return false;
    }

    // Loop through the array of rates and add them to the respective item
    foreach ($rates AS $rate) {
      $class = str_pad($rate->class, 4, '0', STR_PAD_LEFT);
      $catClass = $rate->category . '-' . $class;

      /** @var Item $item */
      if (!empty($this->items[$catClass])) {
        $item = &$this->items[$catClass];
      }
      else {
        $item = new Item();
        $item->setCatClass($catClass);
        $item->setQuantity($rate->qty);
        $item->setRentalAmount($rate->rentalamt);

        $this->items[$catClass] = $item;
      }

      $item->buildRate($rate);
    }
  }

  /**
   * Builds the "catClassAndQty" parameter.
   *
   * @param $items - Array of Item objects
   * @return array
   */
  public function buildCatClassAndQty(array $items) {
    $mapFunction = function (Item $item) {
      return sprintf(
        '%s=%d',
        $item->getCatClass(),
        $item->getQuantity()
      );
    };

    $this->catClassAndQty = array_map($mapFunction, (array)$items);
    return $this->catClassAndQty;
  }

  /**
   * Builds the Cat Class and Quantity array from the existing items list.
   *
   * @return array
   */
  public function buildCatClassAndQtyFromItems() {
    return $this->buildCatClassAndQty($this->items);
  }

  /**
   * @return array
   */
  public function getCatClassAndQty() {
    return $this->catClassAndQty;
  }

  /**
   * @param array $catClassAndQty
   */
  public function setCatClassAndQty($catClassAndQty) {
    $this->catClassAndQty = $catClassAndQty;
  }

  /**
   * @return array
   */
  public function getCatClassQtyRates() {
    return $this->catClassQtyRates;
  }

  /**
   * @TODO: Should be able to dynamically format this.
   * Ref: mapCatClassQtyRates() method in ../js/api/api.estimates.js
   *
   * @param array $catClassQtyRates
   */
  public function setCatClassQtyRates($catClassQtyRates) {
    $this->catClassQtyRates = $catClassQtyRates;
  }

  /**
   * @return array
   */
  public function getTotals() {
    return $this->totals;
  }

  /**
   * @param array $totals
   */
  public function setTotals($totals) {
    $this->totals = $totals;
  }

  public function import($data) {
    parent::import($data); // TODO: Change the autogenerated stub

    if (isset($data['delivery']) && is_bool($data['delivery'])) {
      $this->delivery = $this->convertYesNo($data['delivery']);
    }

    if (isset($data['pickup']) && is_bool($data['pickup'])) {
      $this->pickup = $this->convertYesNo($data['pickup']);
    }

    if (strtolower($data['rpp']) === 'true') {
      $data['rpp'] = true;
    }
    if (strtolower($data['rpp']) === 'false') {
      $data['rpp'] = false;
    }

    if (isset($data['rpp']) && is_bool($data['rpp'])) {
      $this->rpp = $this->convertYesNo($data['rpp']);
    }

    if (isset($data['webRates']) && is_bool($data['webRates'])) {
      $this->webRates = $this->convertYesNo($data['webRates']);
    }

  }

  public function export() {
    if (!empty($this->items)) {
      /** @var \Drupal\ur_appliance\Data\DAL\Item $item */
      foreach ($this->items as $item) {
        $this->items[$item->getCatClass()] = $item->export();
      }
    }

    $array = parent::export();

    return $array;
  }
}