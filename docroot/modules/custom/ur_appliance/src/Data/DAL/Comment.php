<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

class Comment extends DataSimple implements DataInterface {
  /** @var string - The Request ID */
  protected $requestId;
  
  /** @var string - The Account ID */
  protected $accountId;
  
  /** @var string - The Comment Type */
  protected $commentType;
  
  /** @var string - The Transaction ID */
  protected $transID;
  
  /** @var  Integer - Comment Sequence Number */
  protected $commentSeq;
  
  /** @var String - Comment Text */
  protected $commentText;
  
  /** @var string */
  protected $status;
  
  /**
   * A class to create data objects from.
   */
  public function getId() {
    return $this->requestId;
  }
  
  public function getAccountId() {
    return $this->accountId;
  }
  
  public function getCommentType() {
    return $this->commentType;
  }
  
  public function getCommentSeq() {
    return $this->commentSeq;
  }
  
  public function getCommentText() {
    return $this->commentText;
  }
  
  /**
   * @param int $requestId
   */
  public function setRequestId($requestId) {
    $this->requestId = $requestId;
  }
  
  /**
   * @param string $transId
   */
  public function setTransId($transId) {
    $this->transId = $transId;
  }
  
  /**
   * @param int $commentSeq
   */
  public function setCommentSeq($commentSeq) {
    $this->commentSeq = $commentSeq;
  }
  
  /**
   * @param string $commentText
   */
  public function setCommentType($commentText) {
    $this->commentText = $commentText;
  }
  
  /**
   * @param string $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }
  
}