<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class AllocationCode
 * @package Drupal\ur_appliance\Data\DAL
 */
class AllocationCode extends DataSimple implements DataInterface {
  /** @var int */
  protected $id;

  /** @var string */
  protected $codeType;

  /** @var int */
  protected $sequenceNum;

  /** @var string */
  protected $title;

  /** @var bool */
  protected $required;

  /** @var bool */
  protected $validated;

  /** @var int */
  protected $validatedCnt;

  /** @var array */
  protected $valueList;

  /** @var array */
  protected $triggers;

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getCodeType() {
    return $this->codeType;
  }

  public function setCodeType($codeType) {
    $this->codeType = $codeType;
  }

  public function getSequenceNumber() {
    return $this->sequenceNum;
  }

  public function setSequenceNumber($sequenceNum) {
    $this->sequenceNum = $sequenceNum;
  }

  public function getRequired() {
    return $this->required;
  }

  public function setRequired($required) {
    $this->required = false;

    if ($required == 'Y') {
      $this->required = true;
    }
  }

  public function getValidated() {
    return $this->validated;
  }

  public function setValidated($validated) {
    $this->validated = false;

    if ($validated == 'Y') {
      $this->validated = true;
    }
  }

  public function getValidatedCnt() {
    return $this->validatedCnt;
  }

  public function setValidatedCount($validatedCnt) {
    $this->validatedCnt = $validatedCnt;
  }

  public function getValueList() {
    return $this->valueList;
  }

  public function setValueList($valueList) {
    $this->valueList = $valueList;
  }

  public function getTriggers() {
    return $this->triggers;
  }

  public function setTriggers($triggers) {
    $this->triggers = $triggers;
  }
}