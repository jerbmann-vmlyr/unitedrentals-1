<?php


namespace Drupal\ur_appliance\Data\DAL;


use Drupal\ur_appliance\Data\DataSimple;

class PickupRequest extends DataSimple {

  /**
   * @var null
   */
  private $id;

  /**
   * @return null
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param null $id
   */
  public function setId($id) {
    $this->id = $id;
  }

}