<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

class Counts extends DataSimple implements DataInterface {

  public $quotes;
  public $reservations;
  public $onRent;
  public $scheduledForPickup;
  public $overdue;
  public $allOpen;
  public $closedContracts;
  public $custOwn;

  /**
   * A class to create data objects from.
   */
  public function getId() {
    // required by the interface, not used by the class
  }

  /**
   * @return mixed
   */
  public function getQuotesCount() {
    return $this->quotes;
  }

  /**
   * @return mixed
   */
  public function getReservationsCount() {
    return $this->reservations;
  }

  /**
   * @return mixed
   */
  public function getOnRentCount() {
    return $this->onRent;
  }

  /**
   * @return mixed
   */
  public function getScheduledForPickupCount() {
    return $this->scheduledForPickup;
  }

  /**
   * @return mixed
   */
  public function getOverdueCount() {
    return $this->overdue;
  }

  /**
   * @return mixed
   */
  public function getAllOpenCount() {
    return $this->allOpen;
  }

  /**
   * @return mixed
   */
  public function getClosedContractsCount() {
    return $this->closedContracts;
  }

  /**
   * @return mixed
   */
  public function getCustOwnCount() {
    return $this->custOwn;
  }

}