<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class TcUser
 * @package Drupal\ur_appliance\Data\DAL
 */
class TcUser extends DataSimple implements DataInterface {
  /** @var int - TcUser ID */
  protected $email;
  
  /** @var string - TcUser First Name */
  protected $firstName;
  
  /** @var string - TcUser Last Name */
  protected $lastName;
  
  /** @var string - TcUser Phone */
  protected $phone;

  /** @var string - TcUser Alternate Phone */
  protected $altPhone;

  /** @var string - TcUser Language */
  protected $language;
  
  /** @var string - TcUser pswdExpDays */
  protected $pswdExpDays;
  
  /** @var string - TcUser Guid */
  protected $guid;

  /** @var array \Drupal\ur_appliance\Data\DAL\Account $accounts */
  protected $accounts = [];
  
  /**
   * @return integer
   */
  public function getId() {
    return $this->email;
  }
  
  public function getAccounts() {
    return $this->accounts;
  }
  
  /**
   * @return string
   */
  public function getFirstName() {
    return $this->firstName;
  }
  
  /**
   * @return string
   */
  public function getLastName() {
    return $this->lastName;
  }
  
  /**
   * @return string
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @return string
   */
  public function getAltPhone() {
    return $this->altPhone;
  }

  /**
   * @return string
   */
  public function getLanguage() {
    return $this->language;
  }
  
  /**
   * @return string
   */
  public function getPswdExpDays() {
    return $this->pswdExpDays;
  }
  
  /**
   * @return string
   */
  public function getGuid() {
    return $this->guid;
  }
  
  /**
   * @param string $id
   */
  public function setId($id) {
    $this->email = $id;
  }

  /** @param array $accounts */
  public function setAccounts($accounts) {
    $this->accounts = [];

    foreach ($accounts as $id => $acccountInfo) {
      $this->accounts[$id] = $acccountInfo;
    }
  }
  
  /**
   * @param string $firstName
   */
  public function setFirstName($firstName) {
    $this->firstName = $firstName;
  }
  
  /**
   * @param string $lastName
   */
  public function setLastName($lastName) {
    $this->lastName = $lastName;
  }
  
  /**
   * @param string $phone
   */
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  /**
   * @param string $altPhone
   */
  public function setAltPhone($altPhone) {
    $this->altPhone = $altPhone;
  }
  
  /**
   * @param string $altPhone
   */
  public function setCardHolder($altPhone) {
    $this->altPhone = $altPhone;
  }
  
  /**
   * @param string $language
   */
  public function setLanguage($language) {
    $this->language = $language;
  }
  
  /**
   * @param string $pswdExpDays
   */
  public function setPswdExpDays($pswdExpDays) {
    $this->pswdExpDays = $pswdExpDays;
  }
  
  /**
   * @param string $guid
   */
  public function setGuid($guid) {
    $this->guid = $guid;
  }

  /**
   * Overwrites parent::import() because accounts property gets set improperly.
   *
   * @param $data
   */
  public function import($data) {
    parent::import($data);

    if (!empty($data->accounts)) {
      $this->setAccounts($data->accounts);
    }
  }
  
  /**
   * Prepares the TcUser object to be used for a DAL TcUser DAL POST call
   *
   *
   * @return array
   *  A DAL formatted POST body for submission to the TcUser endpoint
   */
  public function postExport() {
    
    $postBody = [
      'email' => $this->getId(),
      'firstName' => $this->getFirstName(),
      'lastName' => $this->getLastName(),
      'phone' => $this->getPhone(),
      'altPhone' => $this->getAltPhone(),
      'language' => $this->getLanguage(),
      'pswdExpDays' => $this->getPswdExpDays()
    ];
    
    return $postBody;
  }
}