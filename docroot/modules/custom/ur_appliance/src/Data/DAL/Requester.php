<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class Requester
 * @package Drupal\ur_appliance\Data\DAL
 */
class Requester extends DataSimple implements DataInterface {
  /** @var int - TcRequester accountId */
  protected $accountId;

  protected $guid;

  protected $firstName;

  protected $lastName;

  protected $email;

  protected $phone;

  protected $altPhone;

  protected $status;

  protected $name;

  public function getId() {
    return $this->accountId;
  }

  public function import($data) {
    parent::import($data);

    // Import the basic fields
    foreach ($data AS $name => $val) {
      $field = lcfirst(str_ireplace('requester', '', $name));

      if (property_exists($this, $field)) {
        $this->$field = $val;
        continue;
      }

      $field = lcfirst(str_ireplace('contact', '', $name));

      if (property_exists($this, $field)) {
        $this->$field = $val;
        continue;
      }
    }

    $this->name = trim($this->firstName . ' ' . $this->lastName);
  }
}