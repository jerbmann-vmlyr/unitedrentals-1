<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class Jobsite
 * @package Drupal\ur_appliance\Data\DAL
 */
class Jobsite extends DataSimple implements DataInterface {
  /** @var int - Jobsite ID */
  protected $id;

  /** @var string */
  protected $accountId;

  /** @var string */
  protected $name;

  /** @var string */
  protected $customerJobId;

  /** @var string */
  protected $address1;

  /** @var string */
  protected $address2;

  /** @var string */
  protected $city;

  /** @var string */
  protected $state;

  /** @var string */
  protected $zip;

  /** @var string */
  protected $phone;

  /** @var string */
  protected $mobilePhone;

  /** @var string */
  protected $contact;

  /** @var string */
  protected $email;

  /** @var string */
  protected $notes;

  /** @var string */
  protected $status;

  /** @var float */
  protected $latitude;

  /** @var float */
  protected $longitude;

  public function getId() {
    return $this->id;
  }

  public function getAccountId() {
    return $this->accountId;
  }

  public function getName() {
    return $this->name;
  }
  
  public function getCustomerJobId() {
    return $this->customerJobId;
  }

  public function getAddress1() {
    return $this->address1;
  }

  public function getAddress2() {
    return $this->address2;
  }

  public function getCity() {
    return $this->city;
  }

  public function getState() {
    return $this->state;
  }

  public function getZip() {
    return $this->zip;
  }

  public function getContact() {
    return $this->contact;
  }

  public function getPhone() {
    return $this->phone;
  }

  public function getMobilePhone() {
    return $this->mobilePhone;
  }

  public function getEmail() {
    return $this->email;
  }
  
  public function getNotes() {
    return $this->notes;
  }
  
  public function getStatus() {
    return $this->status;
  }

  /**
   * @return float
   */
  public function getLatitude(): float {
    return $this->latitude;
  }

  /**
   * @return float
   */
  public function getLongitude(): float {
    return $this->longitude;
  }
  
  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @param int $accountId
   */
  public function setAccountId($accountId) {
    $this->accountId = $accountId;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @param string $address1
   */
  public function setAddress1($address1) {
    $this->address1 = $address1;
  }

  /**
   * @param string $address2
   */
  public function setAddress2($address2) {
    $this->address2 = $address2;
  }

  /**
   * @param string $city
   */
  public function setCity($city) {
    $this->city = $city;
  }

  /**
   * @param string $state
   */
  public function setState($state) {
    $this->state = $state;
  }

  /**
   * @param string $zip
   */
  public function setZip($zip) {
    $this->zip = $zip;
  }

  /**
   * @param string $phone
   */
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  /**
   * @param string $mobilePhone
   */
  public function setMobilePhone($mobilePhone) {
    $this->mobilePhone = $mobilePhone;
  }

  /**
   * @param string $contact
   */
  public function setContact($contact) {
    $this->contact = $contact;
  }

  /**
   * @param string $email
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * @param string $notes
   */
  public function setNotes($notes) {
    $this->notes = $notes;
  }

  /**
   * @param string $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * @param float|string $latitude
   */
  public function setLatitude($latitude) {
    $this->latitude = (float) $latitude;
  }

  /**
   * @param float|string $longitude
   */
  public function setLongitude($longitude) {
    $this->longitude = (float) $longitude;
  }

  /**
   * @param $data
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function import($data) {
    parent::import($data);

    $this->removePrependAndSet('jobsite', $data);
    $this->removePrependAndSet('job', $data);
    $data = (object) $data;

    foreach ($data AS $name => $val) {
      if (($name == 'contact' || $name == 'jobContact') && empty($this->getContact())) {
        $this->setContact($val);
      }
  
      if (($name == 'phone' || $name == 'jobPhone' || $name == 'jobContactPhone') && empty($this->getPhone())) {
        $this->setPhone($val);
      }
  
      if (($name == 'email' || $name == 'jobEmail' || $name == 'jobContactEmail') && empty($this->getEmail())) {
        $this->setEmail($val);
      }
  
      if (($name == 'notes' || $name == 'jobNotes') && empty($this->getNotes())) {
        $this->setNotes($val);
      }
    }
    
    // DAL sends jobId this way sometimes.
    if (empty($this->getId()) && !empty($data->jobId)) {
      $this->setId($data->jobId);
    }
  }

  /**
   * Prepares the JobSite object to be used for a DAL JobSite DAL POST||PUT call
   *
   *
   * @return array
   *  A DAL formatted POST||PUT body for submission to the Jobsites endpoint
   */
  public function postExport() {
    $postBody = [
      'id' => $this->getId(),
      'jobId' => $this->getId(),
      'accountId' => $this->getAccountId(),
      'name' => $this->getName(),
      'address1' => $this->getAddress1(),
      'address2' => $this->getAddress2(),
      'city' => $this->getCity(),
      'state' => $this->getState(),
      'zip' => $this->getZip(),
      'mobilePhone' => $this->getMobilePhone(),
      'contact' => $this->getContact(),
      'email' => $this->getEmail(),
      'notes' => $this->getNotes(),
      'status' => $this->getStatus(),
    ];
    
    return $postBody;
  }
}