<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class RequisitionCodes
 * @package Drupal\ur_appliance\Data\DAL
 */
class RequisitionCodes extends DataSimple implements DataInterface {
  /** @var int */
  protected $accountId;
  
  /** @var string */
  protected $codeType;
  
  /** @var int */
  protected $id;
  
  /** @var string */
  protected $allocationId;
  
  /** @var string */
  protected $sequenceNum;
  
  /** @var string */
  protected $title;
  
  /** @var string */
  protected $allocationValue;
  
  /** @var string */
  protected $transId;
  
  /** @var string */
  protected $splitPerc;
  
  /** @var string */
  protected $splitStartDate;
  
  /** @var string */
  protected $splitEndDate;
  
  /** @var array */
  protected $valueList;
  
  public function getAccountId() {
    return $this->accountId;
  }
  
  public function setAccountId($accountId) {
    $this->accountId = $accountId;
  }
  
  public function getCodeType() {
    return $this->codeType;
  }
  
  public function setCodeType($codeType) {
    $this->codeType = $codeType;
  }
  
  public function getId() {
    return $this->id;
  }
  
  public function setId($id) {
    $this->id = $id;
  }
  
  public function getAllocationId() {
    return $this->allocationId;
  }
  
  public function setAllocationId($allocationId) {
    $this->allocationId = $allocationId;
  }
  
  public function getSequenceNum() {
    return $this->sequenceNum;
  }
  
  public function setSequenceNum($sequenceNum) {
    $this->sequenceNum = $sequenceNum;
  }
  
  public function getTitle() {
    return $this->title;
  }
  
  public function setTitle($title) {
    $this->title = $title;
  }
  
  public function getAllocationValue() {
    return $this->allocationValue;
  }
  
  public function setAllocationValue($allocationValue) {
    $this->allocationValue = $allocationValue;
  }
  
  public function getTransId() {
    return $this->transId;
  }
  
  public function setTransId($transId) {
    $this->transId = $transId;
  }
  
  public function getSplitPerc() {
    return $this->splitPerc;
  }
  
  public function setSplitPerc($splitPerc) {
    $this->splitPerc = $splitPerc;
  }
  
  public function getSplitStartDate() {
    return $this->splitStartDate;
  }
  
  public function setSplitStartDate($splitStartDate) {
    $this->splitStartDate = $splitStartDate;
  }
  
  public function getSplitEndDate() {
    return $this->splitPerc;
  }
  
  public function setSplitEndDate($splitEndDate) {
    $this->splitEndDate = $splitEndDate;
  }
  
  public function getValueList() {
    return $this->valueList;
  }
  
  public function setValueList($valueList) {
    $this->valueList = $valueList;
  }
  
  /**
   * @param \Drupal\ur_appliance\Data\DAl\CostAllocation[] $codeValues
   */
  public function assignCodeValues($codeValues) {
    if (!is_array($codeValues) && !($codeValues[0] instanceof CostAllocation)) {
      return;
    }
    
    $valueList = [];
    
    foreach ($codeValues as $codeValue) {
      if (($codeValue instanceof CostAllocation) && $this->getId() == $codeValue->getId() && $this->getCodeType() == $codeValue->getCodeType()) {
        $valueList[] = $codeValue->export();
      }
    }
    
    if (!empty($valueList)) {
      $this->setValueList($valueList);
    }
  }
  
  public function import($data) {
    if ($data instanceof AllocationCode) {
      $data = $data->export();
    }
    
    parent::import($data);
    
    $this->assignCodeValues($data);
  }
}