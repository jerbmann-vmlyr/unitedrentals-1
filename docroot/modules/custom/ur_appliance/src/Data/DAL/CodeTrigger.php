<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class CodeTrigger
 * @package Drupal\ur_appliance\Data\DAL
 */
class CodeTrigger extends DataSimple implements DataInterface {
  /** @var int */
  protected $accountId;

  /** @var string */
  protected $codeType;

  /** @var int */
  protected $idNum;

  /** @var string */
  protected $val;

  /** @var int */
  protected $triggerId;

  /** @var string */
  protected $triggerName;

  /** @var string */
  protected $triggerVal;

  public function getId() {
    // No return value for this data type.
  }

  public function getAccountId() {
    return $this->accountId;
  }

  public function setAccountId($accountId) {
    $this->accountId = $accountId;
  }

  public function getCodeType() {
    return $this->codeType;
  }

  public function setCodeType($codeType) {
    $this->codeType = $codeType;
  }

  public function getIdNum() {
    return $this->idNum;
  }

  public function setIdNum($idNum) {
    $this->idNum = $idNum;
  }

  public function getVal() {
    return $this->val;
  }

  public function setVal($val) {
    $this->val = $val;
  }

  public function getTriggerId() {
    return $this->triggerId;
  }

  public function setTriggerId($triggerId) {
    $this->triggerId = $triggerId;
  }

  public function getTriggerName() {
    return $this->triggerName;
  }

  public function setTriggerName($triggerName) {
    $this->triggerName = $triggerName;
  }

  public function getTriggerVal() {
    return $this->triggerVal;
  }

  public function setTriggerVal($triggerVal) {
    $this->triggerVal = $triggerVal;
  }
}