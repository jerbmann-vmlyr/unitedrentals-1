<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class Date
 *
 * This class will handle all of the United Rentaals/DAL-specific
 * date formatting functionality.
 *
 * @package Drupal\ur_appliance\Data\DAL
 */
class Date extends DataSimple implements DataInterface {
  const STANDARD_TIMEZONE = 'UTC';
  const FLAVORED_ISO_8601 = 'Y-m-d\TH:i:s';

  /**
   * A class to create data objects from.
   */
  public function getId() {
    // No ID for this data object
  }

  /**
   * Formats a give Unix Timestamp into the standard format
   * that the DAL accepts.
   *
   * @param $timestamp
   * @return string
   */
  public static function formatDate($timestamp) {
    $timezone = new \DateTimeZone(self::STANDARD_TIMEZONE);
    $date = new \DateTime(null, $timezone);
    $date->setTimestamp($timestamp);

    return $date->format(self::FLAVORED_ISO_8601);
  }
}