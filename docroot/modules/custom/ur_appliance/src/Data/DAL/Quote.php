<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class Quote
 * @package Drupal\ur_appliance\Data\DAL
 */
class Quote extends DataSimple implements DataInterface {
  /** @var string - Account ID */
  protected $accountId;
  
  /** @var string - Requisition ID */
  protected $requisitionId;
  
  /** @var string - Requisition cancelled */
  protected $requisitionCancelled;
  
  /** @var integer - Reservation ID */
  protected $reservationId;
  
  /** @var integer - Quote ID */
  protected $quoteId;
  
  /** @var string - Start Date */
  protected $startDate;
  
  /** @var  string - Job ID */
  protected $jobId;
  
  /** @var  string - Job Name */
  protected $jobName;

  /** @var  string - Quote Status */
  protected $orderStatus;

  /** @var  string - Quote Status Code */
  protected $orderStatusCode;

  /** @var array \United\Data\DAL\Quote $quoteStat */
  protected $quoteStat = [];
  
  /**
   * @return string
   */
  public function getId() {
    return $this->accountId;
  }
  
  /**
   * @return string
   */
  public function getRequisitionID() {
    return $this->requisitionId;
  }
  
  /**
   * @return string
   */
  public function getRequisitionCancelled() {
    return $this->requisitionCancelled;
  }
  
  /**
   * @return integer
   */
  public function getReservationID() {
    return $this->reservationId;
  }
  
  /**
   * @return string
   */
  public function getQuoteID() {
    return $this->quoteId;
  }
  
  /**
   * @return string
   */
  public function getStartDate() {
    return $this->startDate;
  }
  
  /**
   * @return string
   */
  public function getJobId() {
    return $this->jobId;
  }
  
  /**
   * @return string
   */
  public function getJobName() {
    return $this->jobName;
  }

  /**
   * @return mixed
   */
  public function getQuoteStatus() {
    return $this->orderStatus;
  }

  /**
   * @return mixed
   */
  public function getQuoteStatusCode() {
    return $this->orderStatusCode;
  }
}