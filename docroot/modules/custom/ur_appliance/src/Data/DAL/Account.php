<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

class Account extends DataSimple implements DataInterface {
  /** @var int */
  protected $id;

  /** @var int */
  protected $parentId;

  /** @var string */
  protected $status;

  /** @var string */
  protected $statusDesc;

  /** @var string - Account Name */
  protected $name;

  /**
   * @TODO: Create an Address object that can contain this
   *        set of values for multiple data objects.
   */

  /** @var string */
  protected $address1;

  /** @var string */
  protected $address2;

  /** @var string */
  protected $city;

  /** @var string */
  protected $state;

  /** @var string */
  protected $zip;


  /** @var string */
  protected $email;

  /** @var string */
  protected $phone;

  /** @var string */
  protected $requirePo;

  /** @var string */
  protected $countryCode;

  /** @var bool - Job Costing Flag */
  protected $jobCost;

  /** @var string */
  protected $tcLevel;

  /** @var int - Average # of Days to Pay */
  protected $averageDays;

  /** @var string - Term Code */
  protected $terms;

  /** @var string - Default Rental Protection Plan */
  protected $rppDefault;

  /** @var int */
  protected $openRentalAmount;

  /** @var string - Quote Redirect Location */
  protected $quoteRedirectLoc;

  /** @var string - Reservation Redirect Location */
  protected $reservationRedirectLoc;


  /** @var int */
  protected $creditLimit;

  /** @var string */
  protected $creditCode;

  /** @var string|bool - Authorized to use credit card? */
  protected $creditCardAuthorized;


  /** @var \DateTime - Timestamp 2011-12-15T00:00:00:00 */
  protected $lastTransactionDate;

  /** @var \DateTime - Timestamp 2011-12-15T00:00:00:00 */
  protected $lastPaymentDate;

  protected $amounts = [
    'lastPayment' => null,
    'current' => null,
    '30day' => null,
    '60day' => null,
    '90day' => null,
    '120day' => null,
  ];

  protected $rental = [
    'YTD' => null,
    'LTD' => null,
    'LYR' => null,
  ];

  protected $consolidatedBill = [
    'flag' => null,
    'day' => null,
    'email' => null,
    'nextDate' => null, // @var \DateTime - Timestamp 2011-12-15T00:00:00:00
    'lastDate' => null, // @var \DateTime - Timestamp 2011-12-15T00:00:00:00
  ];


  /** @var array[string] */
  protected $collector = [
    'name' => null,
    'phone' => null,
    'email' => null,
  ];

  protected $contact = [
    'phone' => null,
    'phoneAlpha' => null,
  ];

  /**
   * A class to create data objects from.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return int
   */
  public function getParentId() {
    return $this->parentId;
  }

  /**
   * @param int $parentId
   */
  public function setParentId($parentId) {
    $this->parentId = $parentId;
  }

  /**
   * @return string
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param string $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * @return string
   */
  public function getStatusDesc() {
    return $this->statusDesc;
  }

  /**
   * @param string $statusDesc
   */
  public function setStatusDesc($statusDesc) {
    $this->statusDesc = $statusDesc;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getAddress1() {
    return $this->address1;
  }

  /**
   * @param string $address1
   */
  public function setAddress1($address1) {
    $this->address1 = $address1;
  }

  /**
   * @return string
   */
  public function getAddress2() {
    return $this->address2;
  }

  /**
   * @param string $address2
   */
  public function setAddress2($address2) {
    $this->address2 = $address2;
  }

  /**
   * @return string
   */
  public function getCity() {
    return $this->city;
  }

  /**
   * @param string $city
   */
  public function setCity($city) {
    $this->city = $city;
  }

  /**
   * @return string
   */
  public function getState() {
    return $this->state;
  }

  /**
   * @param string $state
   */
  public function setState($state) {
    $this->state = $state;
  }

  /**
   * @return string
   */
  public function getZip() {
    return $this->zip;
  }

  /**
   * @param string $zip
   */
  public function setZip($zip) {
    $this->zip = $zip;
  }

  /**
   * @return string
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @param string $email
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * @return string
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @param string $phone
   */
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  /**
   * @return string
   */
  public function getRequirePo() {
    return $this->requirePo;
  }

  /**
   * @param string $requirePo
   */
  public function setRequirePo($requirePo) {
    $this->requirePo = $requirePo;
  }

  /**
   * @return string
   */
  public function getCountryCode() {
    return $this->countryCode;
  }

  /**
   * @param string $countryCode
   */
  public function setCountryCode($countryCode) {
    $this->countryCode = $countryCode;
  }

  /**
   * @return bool
   */
  public function isJobCost() {
    return $this->jobCost;
  }

  /**
   * @param bool $jobCost
   */
  public function setJobCost($jobCost) {
    $this->jobCost = $jobCost;
  }

  /**
   * @return string
   */
  public function getTcLevel() {
    return $this->tcLevel;
  }

  /**
   * @param string $tcLevel
   */
  public function setTcLevel($tcLevel) {
    $this->tcLevel = $tcLevel;
  }

  /**
   * @return int
   */
  public function getAverageDays() {
    return $this->averageDays;
  }

  /**
   * @param int $averageDays
   */
  public function setAverageDays($averageDays) {
    $this->averageDays = $averageDays;
  }

  /**
   * @return string
   */
  public function getTerms() {
    return $this->terms;
  }

  /**
   * @param string $terms
   */
  public function setTerms($terms) {
    $this->terms = $terms;
  }

  /**
   * @return string
   */
  public function getRppDefault() {
    return $this->rppDefault;
  }

  /**
   * @param string $rppDefault
   */
  public function setRppDefault($rppDefault) {
    $this->rppDefault = $rppDefault;
  }

  /**
   * @return int
   */
  public function getOpenRentalAmount() {
    return $this->openRentalAmount;
  }

  /**
   * @param int $openRentalAmount
   */
  public function setOpenRentalAmount($openRentalAmount) {
    $this->openRentalAmount = $openRentalAmount;
  }

  /**
   * @return string
   */
  public function getQuoteRedirectLoc() {
    return $this->quoteRedirectLoc;
  }

  /**
   * @param string $quoteRedirectLoc
   */
  public function setQuoteRedirectLoc($quoteRedirectLoc) {
    $this->quoteRedirectLoc = $quoteRedirectLoc;
  }

  /**
   * @return string
   */
  public function getReservationRedirectLoc() {
    return $this->reservationRedirectLoc;
  }

  /**
   * @param string $reservationRedirectLoc
   */
  public function setReservationRedirectLoc($reservationRedirectLoc) {
    $this->reservationRedirectLoc = $reservationRedirectLoc;
  }

  /**
   * @return int
   */
  public function getCreditLimit() {
    return $this->creditLimit;
  }

  /**
   * @param int $creditLimit
   */
  public function setCreditLimit($creditLimit) {
    $this->creditLimit = $creditLimit;
  }

  /**
   * @return string
   */
  public function getCreditCode() {
    return $this->creditCode;
  }

  /**
   * @param string $creditCode
   */
  public function setCreditCode($creditCode) {
    $this->creditCode = $creditCode;
  }

  /**
   * @return bool|string
   */
  public function getCreditCardAuthorized() {
    return $this->creditCardAuthorized;
  }

  /**
   * @param bool|string $creditCardAuthorized
   */
  public function setCreditCardAuthorized($creditCardAuthorized) {
    $this->creditCardAuthorized = $creditCardAuthorized;
  }

  /**
   * @return \DateTime
   */
  public function getLastTransactionDate() {
    return $this->lastTransactionDate;
  }

  /**
   * @param \DateTime $lastTransactionDate
   */
  public function setLastTransactionDate($lastTransactionDate) {
    $this->lastTransactionDate = $lastTransactionDate;
  }

  /**
   * @return \DateTime
   */
  public function getLastPaymentDate() {
    return $this->lastPaymentDate;
  }

  /**
   * @param \DateTime $lastPaymentDate
   */
  public function setLastPaymentDate($lastPaymentDate) {
    $this->lastPaymentDate = $lastPaymentDate;
  }

  /**
   * @param null $index
   * @return array|mixed
   */
  public function getAmounts($index = null) {
    if (empty($index) || !isset($this->amounts[$index])) {
      return $this->amounts;
    }

    return $this->amounts[$index];
  }

  /**
   * @param array $data
   */
  public function setAmounts(array $data) {
    if (is_array($data)) {
      foreach ($data AS $field => $val) {
        if (isset($this->amounts[$field])) {
          $this->amounts[$field] = $val;
        }
      }
    }
  }

  /**
   * @param $index
   * @param $val
   */
  public function setAmount($index, $val) {
    if (isset($this->amounts[$index])) {
      $this->amounts[$index] = $val;
    }
  }

  /**
   * @param null $index
   * @return array|mixed
   */
  public function getRentals($index = null) {
    if (empty($index) || !isset($this->rental[$index])) {
      return $this->rental;
    }

    return $this->rental[$index];
  }

  /**
   * @param array $data
   */
  public function setRentals(array $data) {
    if (is_array($data)) {
      foreach ($data AS $field => $val) {
        if (isset($this->rental[$field])) {
          $this->rental[$field] = $val;
        }
      }
    }
  }

  /**
   * @param $index
   * @param $val
   */
  public function setRental($index, $val) {
    if (isset($this->rental[$index])) {
      $this->rental[$index] = $val;
    }
  }

  /**
   * @param null $index
   * @return array|mixed
   */
  public function getConsolidatedBills($index = null) {
    if (empty($index) || !isset($this->consolidatedBill[$index])) {
      return $this->consolidatedBill;
    }

    return $this->consolidatedBill[$index];
  }

  /**
   * @param array $data
   */
  public function setConsolidatedBills(array $data) {
    if (is_array($data)) {
      foreach ($data AS $field => $val) {
        if (isset($this->consolidatedBill[$field])) {
          $this->consolidatedBill[$field] = $val;
        }
      }
    }
  }

  /**
   * @param $index
   * @param $val
   */
  public function setConsolidatedBill($index, $val) {
    if (isset($this->consolidatedBill[$index])) {
      $this->consolidatedBill[$index] = $val;
    }
  }

  /**
   * @param null $index
   * @return array|mixed
   */
  public function getCollector($index = null) {
    if (empty($index) || !isset($this->collector[$index])) {
      return $this->collector;
    }

    return $this->collector[$index];
  }

  /**
   * @param array $data
   */
  public function setCollector(array $data) {
    if (is_array($data)) {
      foreach ($data AS $field => $val) {
        if (isset($this->collector[$field])) {
          $this->collector[$field] = $val;
        }
      }
    }
  }

  /**
   * @param $index
   * @param $val
   */
  public function setCollectorSetting($index, $val) {
    if (isset($this->collector[$index])) {
      $this->collector[$index] = $val;
    }
  }

  /**
   * @param null $index
   * @return array|mixed
   */
  public function getContact($index = null) {
    if (empty($index) || !isset($this->contact[$index])) {
      return $this->contact;
    }

    return $this->contact[$index];
  }

  /**
   * @param array $data
   */
  public function setContact(array $data) {
    if (is_array($data)) {
      foreach ($data AS $field => $val) {
        if (isset($this->contact[$field])) {
          $this->contact[$field] = $val;
        }
      }
    }
  }

  /**
   * @param $index
   * @param $val
   */
  public function setContactSettings($index, $val) {
    if (isset($this->contact[$index])) {
      $this->contact[$index] = $val;
    }
  }

  /**
   * @param \stdClass|array $data - The group of data to import.
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function import($data) {
    if (!is_object($data) && !is_array($data)) {
      throw new DalDataException('You must provide a data object or array to import from.');
    }

    if (is_object($data)) {
      $data = (array) $data;
    }

    foreach ($data AS $name => $val) {
      if ($name != 'consolidatedBill' && property_exists($this, $name)) {
        $this->$name = $val;
        continue;
      }

      // Check the rental array values
      if (strpos($name, 'rental')) {
        $arrayKey = substr($name, 6);

        if (isset($this->rental[$arrayKey])) {
          $this->rental[$arrayKey] = $val;
          continue;
        }
      }

      // Check the collector array
      if (strpos($name, 'collector')) {
        $arrayKey = substr($name, 9);

        if (isset($this->collector[$arrayKey])) {
          $this->collector[$arrayKey] = $val;
          continue;
        }
      }

      // Check the contact array
      if (strpos($name, 'contact')) {
        $arrayKey = substr($name, 7);

        if (isset($this->contact[$arrayKey])) {
          $this->contact[$arrayKey] = $val;
          continue;
        }
      }

      // Check the Amounts property array
      if (substr($name, -6) == 'Amount') {
        $amountPosition = strpos($name, 'Amount') + 1;
        $arrayKey = substr($name, 0, $amountPosition);

        if (isset($this->amounts[$arrayKey])) {
          $this->amounts[$arrayKey] = $val;
          continue;
        }
      }

      // Check the consolidated bill array values
      if (stripos($name, 'consolidatedBill')) {
        $tempName = str_ireplace('consolidatedBill', '', $name);

        // Try the raw version
        if (isset($this->consolidatedBill[$tempName])) {
          $this->consolidatedBill[$tempName] = $val;
          continue;
        }

        $tempName = strtolower($tempName);

        // Try the full lowercase version
        if (isset($this->consolidatedBill[$tempName])) {
          $this->consolidatedBill[$tempName] = $val;
          continue;
        }

        // See if it is the flag
        if ($name == 'consolidatedBill') {
          $this->consolidatedBill['flag'] = $val;
          continue;
        }
      }
    }
  }
}