<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

class AvailableEquipment extends DataSimple implements DataInterface {
  /** @var string - The branch ID */
  protected $branchId;

  /** @var string - The cat class code (Format: 310-9801) */
  protected $catClass;

  /** @var boolean */
  protected $detail;

  /** @var string */
  protected $aDate;

  /** @var string */
  protected $viewType;

  /** @var int */
  protected $locQty;

  /** @var int */
  protected $zoneQty;

  /** @var int */
  protected $day1;

  /** @var int */
  protected $day2;

  /** @var int */
  protected $day3;

  /** @var int */
  protected $day4;

  /** @var int */
  protected $day5;

  /** @var int */
  protected $day6;

  /** @var int */
  protected $day7;

  /**
   * @return string
   */
  public function getId() {
    return $this->branchId;
  }

  /**
   * @param $branchId
   */
  public function setId($branchId) {
    $this->branchId = $branchId;
  }

  /**
   * @return string
   */
  public function getCatClass() {
    return $this->catClass;
  }

  /**
   * @param $catClass
   * @return mixed
   */
  public function setCatClass($catClass) {
    return $this->catClass = $catClass;
  }

  /**
   * @return string
   */
  public function getADate() {
    return $this->aDate;
  }

  /**
   * @param $aDate
   * @return mixed
   */
  public function setADate($aDate) {
    return $this->aDate = $aDate;
  }

  /**
   * @return string
   */
  public function getViewType() {
    return $this->viewType;
  }

  /**
   * @param $viewType
   * @return mixed
   */
  public function setViewType($viewType) {
    return $this->viewType = $viewType;
  }

  /**
   * @return int
   */
  public function getLocQty() {
    return $this->locQty;
  }

  /**
   * @param $locQty
   * @return mixed
   */
  public function setLocQty($locQty) {
    return $this->locQty = $locQty;
  }

  /**
   * @return int
   */
  public function getZoneQty() {
    return $this->zoneQty;
  }

  /**
   * @param $zoneQty
   * @return mixed
   */
  public function setZoneQty($zoneQty) {
    return $this->zoneQty = $zoneQty;
  }

  /**
   * @return int
   */
  public function getDay1() {
    return $this->day1;
  }

  /**
   * @param $day1
   * @return mixed
   */
  public function setDay1($day1) {
    return $this->day1 = $day1;
  }

  /**
   * @return int
   */
  public function getDay2() {
    return $this->day2;
  }

  /**
   * @param $day2
   * @return mixed
   */
  public function setDay2($day2) {
    return $this->day2 = $day2;
  }

  /**
   * @return int
   */
  public function getDay3() {
    return $this->day3;
  }

  /**
   * @param $day3
   * @return mixed
   */
  public function setDay3($day3) {
    return $this->day3 = $day3;
  }

  /**
   * @return int
   */
  public function getDay4() {
    return $this->day4;
  }

  /**
   * @param $day4
   * @return mixed
   */
  public function setDay4($day4) {
    return $this->day4 = $day4;
  }

  /**
   * @return int
   */
  public function getDay5() {
    return $this->day5;
  }

  /**
   * @param $day5
   * @return mixed
   */
  public function setDay5($day5) {
    return $this->day5 = $day5;
  }

  /**
   * @return int
   */
  public function getDay6() {
    return $this->day6;
  }

  /**
   * @param $day6
   * @return mixed
   */
  public function setDay6($day6) {
    return $this->day6 = $day6;
  }

  /**
   * @return int
   */
  public function getDay7() {
    return $this->day7;
  }

  /**
   * @param $day7
   * @return mixed
   */
  public function setDay7($day7) {
    return $this->day7 = $day7;
  }
}