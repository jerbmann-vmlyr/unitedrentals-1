<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class Approver
 * @package Drupal\ur_appliance\Data\DAL
 */
class Approver extends DataSimple implements DataInterface {
  /** @var int - TcApprover accountId */
  protected $accountId;

  protected $guid;

  protected $firstName;

  protected $lastName;

  protected $email;

  protected $phone;

  protected $altPhone;

  protected $status;

  protected $name;

  /**
   * @return int
   */
  public function getAccountId() {
    return $this->accountId;
  }

  /**
   * @param int $accountId
   */
  public function setAccountId($accountId) {
    $this->accountId = $accountId;
  }

  /**
   * @return mixed
   */
  public function getGuid() {
    return $this->guid;
  }

  /**
   * @param mixed $guid
   */
  public function setGuid($guid) {
    $this->guid = $guid;
  }

  /**
   * @return mixed
   */
  public function getFirstName() {
    return $this->firstName;
  }

  /**
   * @param mixed $firstName
   */
  public function setFirstName($firstName) {
    $this->firstName = $firstName;
  }

  /**
   * @return mixed
   */
  public function getLastName() {
    return $this->lastName;
  }

  /**
   * @param mixed $lastName
   */
  public function setLastName($lastName) {
    $this->lastName = $lastName;
  }

  /**
   * @return mixed
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @param mixed $email
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * @return mixed
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @param mixed $phone
   */
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  /**
   * @return mixed
   */
  public function getAltPhone() {
    return $this->altPhone;
  }

  /**
   * @param mixed $altPhone
   */
  public function setAltPhone($altPhone) {
    $this->altPhone = $altPhone;
  }

  /**
   * @return mixed
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param mixed $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  public function getId() {
    return $this->accountId;
  }

  public function import($data) {
    // Import the basic fields
    foreach ($data AS $name => $val) {
      $field = lcfirst(str_ireplace('approver', '', $name));

      if (property_exists($this, $field)) {
        $this->$field = $val;
        continue;
      }

      $field = lcfirst(str_ireplace('contact', '', $name));

      if (property_exists($this, $field)) {
        $this->$field = $val;
        continue;
      }
    }
  }
}