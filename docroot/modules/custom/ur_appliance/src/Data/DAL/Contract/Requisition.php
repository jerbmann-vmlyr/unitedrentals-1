<?php
namespace Drupal\ur_appliance\Data\DAL\Contract;

use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Exception\EndpointException;

/**
 * Requisition "smart object"
 *
 * A Requisition is NOT an order, or a transaction
 *
 * see https://testmobiledal.ur.com  >  /rental/requisitions
 *
 */

class Requisition {

  /** @var array */
  protected $allowedMethods = ['index', 'create', 'read', 'update'];

  protected $catClassRequired = false;

  /** @var string */
  protected $method;

  /** @var array */
  protected $required;

  /** @var array */
  protected $possible;

  /** @var string */
  public $requestId = '';

  /** @var int */
  public $branchId = 0;

  /** @var string */
  public $accountId = '';

  /** @var string */
  public $statusCode = '';
  
  /** @var string */
  public $status = '';

  /** @var string */
  public $poNumber = '';

  /** @var string */
  public $reqGuid = '';

  /** @var string */
  public $reqContact = '';

  /** @var string */
  public $reqPhone = '';

  /** @var string */
  public $reqEmail = '';

  /** @var \stdClass */
  public $jobsite = null;

  /** @var string */
  public $jobContact = '';

  /** @var string */
  public $jobPhone = '';

  /** @var string */
  public $jobEmail = '';

  /** @var boolean */
  public $rpp = false;

  /** @var string */
  public $reqDateTime = '';

  /** @var array */
  public $items = null;

  /** @var string */
  public $jobId = '';

  /** @var string */
  public $startDateTime = '';

  /** @var string */
  public $endDateTime = '';

  /** @var boolean */
  public $delivery = false;

  /** @var boolean */
  public $pickup = false;

  /** @var string */
  public $approverGuid = '';

  /** @var string */
  public $createdGuid = '';

  /** @var string */
  public $timeStamp = '';

  /** @var array */
  public $catClassList = null;

  /** @var array */
  public $qtyList = null;

  /** @var string */
  public $requestSeq = 0;

  /** @var string */
  public $coeStatusCode = '';

  /** @var float */
  public $transChg = 0;

  /** @var float */
  public $miscChg = 0;

  /** @var int */
  public $updateType = 0;

  /** @var array */
  public $details = [
    'catclass' => '', // string
    'quantity' => 0, // int
    'alias' => '', // string
    'equipNum' => '', // string
    'custOwn' => false, // boolean
    'jobNum' => '', // string
    'meter ' => 0, // float
    'dayRate' => 0, // float
    'totalAmount' => 0, // float
  ];

  function __construct($raw) {
    $this->import($raw);

  }

  /**
   * Fill in all data passed to us to get ready
   * to be used by someone, somewhere else
   *
   * @param $raw
   */
  protected function import($raw) {
    foreach ($raw AS $key => $val) {
      // Specific to the Requisition endpoint
      // DAL GET uses id instead of accountId
      // ¯\_(ツ)_/¯
      if ($key == 'id') {
        $key = 'accountId';
      }

      if (array_key_exists($key, $this->details)) {
        $this->details[$key] = $val;
        $this->catClassRequired = true;
      }
      else {
        $this->{$key} = $val;
      }
    }
  }

  /**
   * Normalize and return all data necessary for
   * this specific action of the endpoint
   *
   * @return array
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  protected function filter() {
    $body = [];

    // IF the cat class is required, we MUST have a cat class as
    // a member of the details array sent to the DAL
    if ($this->catClassRequired) {
      if (empty($this->details['catclass'])) {
        throw new EndpointException('Missing required field: catclass');
      }
    }

    $errFields = [];
    foreach ($this->required AS $field) {
      // Is it one of my variables?
      // Is it empty?
      if (property_exists($this, $field) && empty($this->{$field})) {
        $errFields[] = $field;
      }
    }

    if (count($errFields)) {
      throw new EndpointException('Missing required field: ' . implode(', ', $errFields));
    }

    // All of the required fields are here, but we have one other
    // check to make: if cat class is required, we MUST have at
    // lease one other field in the details array to give DAL
    $detailsCount = 0;
    foreach ($this->required AS $field) {
      if (array_key_exists($field, $this->details)) {
        if (!empty($this->details[$field])) {
          $body['details'][$field] = $this->details[$field];
          $detailsCount++;
        }
      }
      else {
        // IF this is an INDEX or GET statusCode
        // needs to be translated into status
        // because ... reasons ¯\_(ツ)_/¯
        $key = $field;
        if ($field == 'statusCode' && ($this->method == 'index' || $this->method == 'read')) {
          $key = 'status';
        }
        $body[$key] = $this->{$field};
      }
    }

    foreach ($this->possible AS $p) {
      if (array_key_exists($p, $this->details)) {
        if (!empty($this->details[$p])) {
          $body['details'][$p] = $this->details[$p];
          $detailsCount++;
        }
      }
      if (property_exists($this, $p) && !empty($this->{$p})) {
        $body[$p] = $this->{$p};
      }
    }

    if ($this->catClassRequired && $detailsCount < 2) {
      throw new EndpointException('Missing required details field pair: catclass and one other');
    }

    return $body;
  }
  
  protected function cleanPhone($phone) {
    // Removing dashes from phone number to normalize the number to 5555555555
    $phone = preg_replace( '/[^0-9]/', '', trim($phone));
    return $phone;
  }

  /**
   * Given the required method (GET, PUT, POST, etc)
   * we need to send a specific set of data to DAL
   *
   * @param $method
   * @return array
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   * @throws \Drupal\ur_appliance\Exception\EndpointException
   */
  public function extract($method) {
    if (in_array($method, $this->allowedMethods)) {
      $this->method = $method;
      $this->setFields();
    }
    else {
      throw new DalException('Method ' . $method . ' is not allowed for this endpoint.');
    }

    return $this->filter();
  }

  /**
   * The actual contract with the DAL
   *
   */
  protected function setFields() {

    // Methods INDEX, GET require status, BUT
    // status is not sent, even though it is
    // expected. So we need to make sure...

    switch ($this->method) {
      case 'index':
        $this->required = [
          'requestId',
        ];
        $this->possible = [
          'lineSeq',
          'includecustcodes',
          'excludecustcodes',
          'returnSeq',
          'catClass',
          'status',
        ];
        break;
      case 'read':
        $this->required = [
          'accountId', // This must be translated to "id" to be received by DAL
        ];
        $this->possible = [
          'requestId',
          'returnSeq',
          'status',
          'createdguid',
          'approverguid',
          'reqguid',
          'apprinclude',
          'apprexclude',
          'catClass',
          'jobId',
          'alias',
          'lineSeq',
          'includecustcodes',
          'excludecustcodes',
        ];
        break;
      case 'create':
        $this->required = [
          'branchId',
          'accountId',
          'statusCode',
          'catclass',
        ];
        $this->possible = [
          'branchId',
          'accountId',
          'poNumber',
          'reqGuid',
          'reqContact',
          'reqPhone',
          'reqEmail',
          'jobContact',
          'jobPhone',
          'jobEmail',
          'rpp',
          'reqDateTime',
          'jobId',
          'startDateTime',
          'endDateTime',
          'delivery',
          'pickup',
          'approverGuid',
          'createdGuid',
          'catClassList',
          'qtyList',
          'requestSeq',
          'coeStatusCode',
          'transChg',
          'miscChg',
          'updateType',
          'quantity',
          'alias',
          'equipNum',
          'custOwn',
          'jobNum',
          'meter',
          'dayRate',
          'totalAmount',
        ];
        break;
      case 'update':
        if (!$this->timeStamp) {
          // 2014-03-05 17:39:52.765100
          $sendStamp = date('Y-m-d H:i:s', time());

          $this->timeStamp = $sendStamp;
        }

        $this->required = [
          'requestId',
          'statusCode',
          'timeStamp',
        ];

        if ($this->catClassRequired) {
          $this->required[] = 'catclass';
        }

        $this->possible = [
          'branchId',
          'accountId',
          'poNumber',
          'reqGuid',
          'reqContact',
          'reqPhone',
          'reqEmail',
          'jobContact',
          'jobPhone',
          'jobEmail',
          'rpp',
          'reqDateTime',
          'jobId',
          'startDateTime',
          'endDateTime',
          'delivery',
          'pickup',
          'approverGuid',
          'createdGuid',
          'catClassList',
          'qtyList',
          'requestSeq',
          'coeStatusCode',
          'transChg',
          'miscChg',
          'updateType',
          'quantity',
          'alias',
          'equipNum',
          'custOwn',
          'jobNum',
          'meter',
          'dayRate',
          'totalAmount',
        ];
        break;
    }
  }
}