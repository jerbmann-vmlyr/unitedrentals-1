<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

class File extends DataSimple implements DataInterface {
  /** @var Integer - The contract ID */
  protected $id;
  
  /** @var string */
  protected $file;
  
  /**
   * A class to create data objects from.
   */
  public function getId() {
    return $this->id;
  }
  
  public function getFile() {
    return $this->file;
  }
}