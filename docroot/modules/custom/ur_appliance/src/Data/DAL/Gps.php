<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

class Gps extends DataSimple implements DataInterface {
  /** @var float */
  protected $latitude;

  /** @var float */
  protected $longitude;

  /**
   * From Standards Documentation...
   * ---------------------------------
   *
   *   The altitude is expressed as a distance above
   *   mean sea level of the vehicle or equipment. It can
   *   be represented either as feet and inches or as
   *   meters, depending on the value of the Unit of
   *   Measure of Altitude.
   *
   *   If the altitude of location is included, and the unit
   *   of measure of altitude is “meters,” then this field
   *   is required and indicates the altitude in meters. It
   *   is a decimal number. If the unit of measure of
   *   altitude is not “meters,” this field must be
   *   omitted.
   *
   * For more information see:
   *   /docs/AEMP telematics data standard Final 2_5_10.pdf
   *
   * @var array
   */
  protected $altitude = ['meters' => null];

  /** @var string - Date time of most recent record */
  protected $datetime;

  /** @var array<String> */
  protected $usageByDay = [];

  public function import($data) {
    parent::import($data);
    $data = (object) $data;

    if (!empty($data->Altitude->Meters)) {
      $this->setAltitude(['meters' => $data->Altitude->Meters]);
    }

    // Multi-location history imports.
    if (!empty($data->Locations)) {
      $latitude = $longitude = null;
      $locationDateTime = null;
      $dayUsage = [];

      foreach ($data->Locations AS $location) {
        $currentDateTime = new \DateTime($location->datetime);
        $currentTimestamp = $currentDateTime->getTimestamp();

        // Track most recent location and calculate operating hours.
        if (empty($latitude) || $locationDateTime < $currentTimestamp) {
          $locationDateTime = $currentTimestamp;
          $latitude = $location->Latitude;
          $longitude = $location->Longitude;
        }

        // Set the day start time.
        $currentDay = $currentDateTime->format('Y-m-d');
        $currentHours = trim($location->OperatingHours->Hour);

        // Skip an empty entry.
        if (!(strlen($currentHours) > 1) || $currentHours == 'P') {
          continue;
        }

        if (!isset($dayUsage[$currentDay])) {
          // Added dates for reference later on
          $dayUsage[$currentDay] = (object) [
            'startHours' => $currentHours,
            'startDate' => $currentDateTime,
            'endHours' => '',
            'endDate' => '',
          ];
        }
        else if (!empty($currentHours)) {
          // This assumes that the last record will be the most recent.
          $dayUsage[$currentDay]->endHours = $currentHours;
          $dayUsage[$currentDay]->endDate = $currentDateTime;
        }
      }

      // Set the most recent location.
      $this->setLatitude($latitude);
      $this->setLongitude($longitude);

      // Now calculate total hours.
      $count = count($dayUsage);
      $currentNum = 0;
      $previousIdx = null;

      foreach ($dayUsage AS $idx => $day) {
        $startHours = $day->startHours;
        $currentNum++;

        // If prior day exists, update it's total hours.
        if ($previousIdx) {
          // Update total hours based on startHours of previous and current days.
          /** @var \DateTime $priorHours */
          $priorHours = $dayUsage[$previousIdx]->startHours;
          $this->calculateAndAddUsage($previousIdx, $startHours, $priorHours);
        }

        /*
         * Only calculate the total for the current day if it is the last
         * in the list. This prevents extra calculations.
         */
        if ($currentNum == $count) {
          if (empty($day->endHours)) {
            // Skip if we can't calculate hours of operation for the day.
            continue;
          }

          $endHours = $day->endHours;

          if (!empty($endHours)) {
            $this->calculateAndAddUsage($idx, $startHours, $endHours);
          }
          else {
            throw new DalDataException('Invalid end date.');
          }
        }

        $previousIdx = $idx; // Set the current index as the prior one.
      }
    }
  }

  /**
   * Get a date object from an interval given.
   *
   * @param string $interval
   * @return \DateTime
   *
   * @throws \Exception
   */
  public function getIntervalDate($interval) {
    if (!(strlen($interval) > 1)) {
      throw new DalDataException(
        'Invalid interval format given: ' . $interval
      );
    }

    $intervalObj = new \DateInterval($interval);
    $dateObj = new \DateTime('00:00');
    $dateObj->add($intervalObj);

    return $dateObj;
  }

  /**
   * Gets the calculated usage and then records it.
   *
   * @param $day
   * @param string $startHours
   * @param string $endHours
   *
   * @throws \Exception
   */
  public function calculateAndAddUsage($day, $startHours, $endHours) {
    $usage = $this->calculateUsage($startHours, $endHours);
    $this->addUsage($day, $usage);
  }

  /**
   * Calculate the usage between two intervals.
   *
   * @param string $startHours
   * @param string $endHours
   *
   * @return array
   * @throws \Exception
   */
  public function calculateUsage($startHours, $endHours) {
    $startDate = $this->getIntervalDate($startHours);
    $endDate = $this->getIntervalDate($endHours);

    return [
      'days' => (int) $endDate->diff($startDate)->format('%d'),
      'hours' => (int) $endDate->diff($startDate)->format('%h'),
      'minutes' => (int) $endDate->diff($startDate)->format('%i'),
    ];
  }

  /**
   * Add a day and total usage to the usage array.
   *
   * @param string $day   - Day in question.
   * @param array  $usage - Calculated usage.
   */
  public function addUsage($day, array $usage) {
    if (empty($usage)) {
      $usage = [
        'days' => 0,
        'hours' => 0,
        'minutes' => 0,
      ];
    }

    $this->usageByDay[$day] = $usage;
  }

  public function getId() {
    // No ID for this data object
  }

  /**
   * @return float
   */
  public function getLatitude() {
    return $this->latitude;
  }

  /**
   * @param float $latitude
   */
  public function setLatitude($latitude) {
    $this->latitude = $latitude;
  }

  /**
   * @return float
   */
  public function getLongitude() {
    return $this->longitude;
  }

  /**
   * @param float $longitude
   */
  public function setLongitude($longitude) {
    $this->longitude = $longitude;
  }

  /**
   * @return array
   */
  public function getAltitude() {
    return $this->altitude;
  }

  /**
   * @param array $altitude
   */
  public function setAltitude($altitude) {
    $this->altitude = $altitude;
  }

  /**
   * @return string
   */
  public function getDatetime() {
    return $this->datetime;
  }

  /**
   * @param string $datetime
   */
  public function setDatetime($datetime) {
    $this->datetime = $datetime;
  }
}