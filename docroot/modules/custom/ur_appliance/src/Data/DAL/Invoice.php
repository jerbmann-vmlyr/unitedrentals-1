<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

/**
 * Class Invoice
 * @package Drupal\ur_appliance\Data\DAL
 */
class Invoice extends DataSimple implements DataInterface {
  const EMPTY_VALUE = 'EMPTY_VALUE';

  /**
   * @var float $balance
   */
  protected $balance;

  /**
   * @var float amount
   */
  protected $amount;

  /**
   * @var string $accountId
   */
  protected $accountId;

  /**
   * @var string $id
   */
  protected $id;

  /**
   * @var null
   */
  protected $seqId;

  /**
   * @var
   */
  protected $status;

  /**
   * @var
   */
  protected $type;

  /**
   * @var
   */
  protected $date;

  /**
   * @var
   */
  protected $jobId;

  /**
   * @var
   */
  protected $jobName;

  /**
   * @var
   */
  protected $jobsiteAddress1;

  /**
   * @var
   */
  protected $jobsiteCity;

  /**
   * @var
   */
  protected $jobsiteState;

  /**
   * @var
   */
  protected $customerJobId;

  /**
   * @var
   */
  protected $po;

  /**
   * @var
   */
  protected $currency;

  /**
   * @var
   */
  protected $datePaid;

  /**
   * @var
   */
  protected $ahType;

  /**
   * @var
   */
  protected $arType;

  /**
   * @var
   */
  protected $hasRacHeader;

  /**
   * @var
   */
  protected $hasFASTPdf;

  /**
   * @var
   */
  protected $paidNotPosted;

  /**
   * @var
   */
  protected $reqId;

  /**
   * @var
   */
  protected $billFromDate;

  /**
   * @var
   */
  protected $billToDate;

  /**
   * This should
   * @return float|int
   */
  public static function totalOverdue(array $collection) {
    $ledger = 0;
    /** @var $invoice Invoice* */
    foreach ($collection as $invoice) {
      //if for whatever reason there's a space at the beginning of the value, it'll give us a 0 instead of the value - trim
      $ledger += floatval(trim($invoice->getBalance()));
    }

    return $ledger;
  }

  public function getBalance() {
    return $this->balance !== self::EMPTY_VALUE ? $this->balance : 0;
  }

  public function getId() {
    // TODO: Implement getId() method.
    return $this->id;
  }

  /**
   * @return string
   */
  public function getAccountId() {
    return $this->accountId;
  }

  /**
   * @return float
   */
  public function getAmount() {
    return $this->amount;
  }

  /**
   * @return null
   */
  public function getSeqId() {
    return $this->seqId;
  }

  /**
   * @return mixed
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @return mixed
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @return mixed
   */
  public function getDate() {
    return $this->date;
  }

  /**
   * @return mixed
   */
  public function getJobId() {
    return $this->jobId;
  }

  /**
   * @return mixed
   */
  public function getJobName() {
    return $this->jobName;
  }

  /**
   * @return mixed
   */
  public function getJobsiteAddress1() {
    return $this->jobsiteAddress1;
  }

  /**
   * @return mixed
   */
  public function getJobsiteCity() {
    return $this->jobsiteCity;
  }

  /**
   * @return mixed
   */
  public function getJobsiteState() {
    return $this->jobsiteState;
  }

  /**
   * @return mixed
   */
  public function getCustomerJobId() {
    return $this->customerJobId;
  }

  /**
   * @return mixed
   */
  public function getPo() {
    return $this->po;
  }

  /**
   * @return mixed
   */
  public function getCurrency() {
    return $this->currency;
  }

  /**
   * @return mixed
   */
  public function getDatePaid() {
    return $this->datePaid;
  }

  /**
   * @return mixed
   */
  public function getAhType() {
    return $this->ahType;
  }

  /**
   * @return mixed
   */
  public function getArType() {
    return $this->arType;
  }

  /**
   * @return mixed
   */
  public function getHasRacHeader() {
    return $this->hasRacHeader;
  }

  /**
   * @return mixed
   */
  public function getHasFASTPdf() {
    return $this->hasFASTPdf;
  }

  /**
   * @return mixed
   */
  public function getPaidNotPosted() {
    return $this->paidNotPosted;
  }

  /**
   * @return mixed
   */
  public function getReqId() {
    return $this->reqId;
  }

}
