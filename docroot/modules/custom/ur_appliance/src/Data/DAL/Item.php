<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermStorage;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Exception\DataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;
use Drupal\node\Entity\Node;

/**
 * Class Item.
 *
 * @package Drupal\ur_appliance\Data\DAL
 */
class Item extends DataSimple implements DataInterface {

  /**
   * The node ID.
   *
   * @var int
   */
  protected $id;

  /**
   * The catClass code Format 3109801.
   *
   * @var string
   */
  protected $catClass;

  /**
   * The Drupal? equipment ID string.
   *
   * @var string
   */
  protected $equipmentId;

  /**
   * United Rentals equipment ID.
   *
   * @var string
   */
  protected $urEquipmentId;

  /**
   * The name title of the item.
   *
   * @var string
   */
  protected $title;

  /**
   * The description of the item.
   *
   * @var string
   */
  protected $description;

  /**
   * The description of the item from the DAL.
   *
   * @var string
   */
  protected $dalDescription;

  /**
   * The equipment quantity.
   *
   * @var int
   */
  protected $quantity;

  /**
   * The equipment quantity on rent.
   *
   * @var int
   */
  protected $quantityOnRent;

  /**
   * The equipment quantity returned.
   *
   * @var int
   */
  protected $quantityReturned;

  /**
   * The equipment quantity picked up.
   *
   * @var int
   */
  protected $quantityPickedUp;

  /**
   * The equipment pending pickup quantity.
   *
   * @var int
   */
  protected $quantityPendingPickup;

  /**
   * The equipment awaiting next pickup quantity.
   *
   * @var int
   */
  protected $quantityNextPickup;

  /**
   * The equipment usage count today.
   *
   * @var int
   */
  protected $usageToday;

  /**
   * The equipment last meter change date.
   *
   * @var string
   */
  protected $lastMeterChange;

  /**
   * The equipment remaining quantity.
   *
   * @var int
   */
  protected $remainingQty;

  /**
   * The equipment scheduled pickup quantity.
   *
   * @var int
   */
  protected $scheduledPickupQty;

  /**
   * Contains most recent location and usage information.
   *
   * @var \Drupal\ur_appliance\Data\DAL\Gps
   */
  protected $gps;

  /**
   * Contains current equipment latitude.
   *
   * @var string
   */
  protected $latitude;

  /**
   * Contains current longitude.
   *
   * @var string
   */
  protected $longitude;

  /**
   * Contains last telemetry update date time.
   *
   * @var string
   */
  protected $reportedDateTime;
  
  /**
   * YYYYMMDD string - last billed date.
   *
   * @var string
   */
  protected $lastBilledDate;
  
  /**
   * Ordered by - NAME
   *
   * @var string
   */
  protected $orderedBy;

  /**
   * The equipment make.
   *
   * @var string
   */
  protected $make;

  /**
   * The equipment model.
   *
   * @var string
   */
  protected $model;

  /**
   * The equipment year.
   *
   * @var int
   */
  protected $year;

  /**
   * The equipment serial number.
   *
   * @var string
   */
  protected $serial;

  /**
   * The equipment type.
   *
   * @var string
   */
  protected $eqpType;

  /**
   * The Transaction type.
   *
   * @var string
   */
  protected $transType;

  /**
   * The equipment status.
   *
   * @var string
   */
  protected $status;

  /**
   * The equipment PO.
   *
   * @var string
   */
  protected $po;

  /**
   * The equipment rate.
   *
   * @var \Drupal\ur_appliance\Data\DAL\Rate
   */
  protected $rate;

  /**
   * The equipment Min Rate.
   *
   * @var string
   */
  protected $minRate;

  /**
   * The equipment Day Rate.
   *
   * @var string
   */
  protected $dayRate;

  /**
   * The equipment Week Rate.
   *
   * @var string
   */
  protected $weekRate;

  /**
   * The equipment Month Rate.
   *
   * @var string
   */
  protected $monthRate;

  /**
   * The equipment rental amount.
   *
   * @var float
   */
  protected $rentalAmount;

  /**
   * The custOwn var.
   *
   * @var int
   */
  protected $custOwn = FALSE;

  /**
   * The equipment comment.
   *
   * @var string
   */
  protected $comment;

  /**
   * The equipment jobsite.
   *
   * @var \Drupal\ur_appliance\Data\DAL\Jobsite
   */
  protected $jobsite;

  /**
   * The customer's jobsite id (freeform).
   *
   * @var string
   */
  protected $customerJobId;

  /**
   * The equipment active var.
   *
   * @var bool
   */
  protected $active;

  /**
   * The equipment approver GUID.
   *
   * @var string
   */
  protected $approverGuid;

  /**
   * The equipment approver name.
   *
   * @var string
   */
  protected $approverName;

  /**
   * The equipment requester GUID.
   *
   * @var string
   */
  protected $requesterGuid;

  /**
   * The equipment requester name.
   *
   * @var string
   */
  protected $requesterName;

  /**
   * The equipment rental start datetime.
   *
   * @var string
   */
  protected $startDateTime;

  /**
   * The equipment rental return datetime.
   *
   * @var string
   */
  protected $returnDateTime;

  /**
   * The equipment pickup datetime.
   *
   * @var string
   */
  protected $pickupDateTime;

  /**
   * The equipment partial-pickup datetime.
   *
   * @var string
   */
  protected $nextPickupDateTime;

  /**
   * The equipment account codes.
   *
   * @var array
   */
  protected $accountCodes;

  /**
   * The equipment return sequence.
   *
   * @var int
   */
  protected $returnSeq;

  /**
   * Previously paired with urPickup.
   *
   * However, UR IT apparently doesn't use that
   * field. It is now paired with pickup.
   *
   * @var bool
   */
  protected $urDeliver;

  /**
   * Holds the results of urPickup and urWillPickup.
   *
   * Because urPickup does not seem to ever change,
   * we are having urWillPickup supersede urPickup.
   *
   * @var bool
   */
  protected $pickup;

  /**
   * The equipment pickup ID.
   *
   * @var string
   */
  protected $pickupId;

  /**
   * The equipment pickup type.
   *
   * @var string
   */
  protected $pickupType;

  /**
   * The equipment pending pickup flag.
   *
   * @var bool
   */
  protected $pendingPickup;

  /**
   * The equipment transaction id.
   *
   * @var string
   */
  protected $transId;

  /**
   * The equipment transaction line id.
   *
   * @var string
   */
  protected $transLineId;

  /**
   * The equipment requisition id.
   *
   * @var string
   */
  protected $requisitionId;

  /**
   * The equipment rental protection plan flag.
   *
   * @var bool
   */
  protected $rpp;

  /**
   * If a discount has been applied to this item
   *
   * @var bool
   */
  protected $discountApplied;

  /**
   * Describes how much an item is being us based on gps information
   *
   * Derived value from "reportedDateTime", "lastMeterChange", and "usageToday"
   * fields returned from the rental/transactions API.
   *
   * By default we will set it to "N/A" and let our setGpsUtilization() method
   * adjust as necessary
   *
   * @see Item::setGpsUtilization()
   *
   * @var string
   */
  protected $gpsUtilization = 'N/A';

  /**
   * Imports extra data and order fields into the item object.
   *
   * @param object|array $data
   *   Extra data from the order process.
   * @param array $orderFields
   *   Various order fields from the updating order process.
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function import($data, array $orderFields = []) {
    parent::import($data);

    if (\is_object($data)) {
      $data = (array) $data;
    }

    $jobsiteData = [];

    // Import the extra fields
    foreach ($data AS $name => $val) {
      if ($name == 'rate') {
        $this->buildRate($val);
      }

      if ($name == 'qty') {
        $this->setQuantity($val);
      }

      if ($name == 'catClassDesc' || $name == 'catclassDesc') {
        $this->setDalDescription($val);
      }

      if ($name == 'rentalamt') {
        $this->setRentalAmount($val);
      }

      if ($name == 'transType') {
        $this->setTransType($val);
        $this->setStatus($val);
      }

      if (stripos($name, 'acctCode')) {
        $codeName = \str_replace('acctCode', '', $name);
        $this->addAccountCode($codeName, $val);
      }

      // Do jobsite import
      if (!empty($val) && (\stripos($name, 'jobsite') !== false || $name == 'accountId')) {
        $jobsiteData[$name] = $val;
      }

      // Import GPS AEMP Telematics Records.
      if ($name == 'EquipmentHeader') {
        $this->setMake($val->Make);
        $this->setModel($val->Model);
        $this->setEquipmentId($val->EquipmentID);
      }

      if ($name == 'Location') {
        $this->setGps($val);
      }

      if ($name == 'Locations') {
        $this->setGps($data);
      }

      if ($name == 'EquipmentID') {
        $this->setEquipmentId($val);
        $this->setId($val);
      }

      if ($name == 'transLineId') {
        $this->setTransLineId($val);
      }

      if ($name == 'SerialNumber') {
        $this->setSerial($val);
      }

      // The urWillPickup field will always supersede the urPickup field.
      if ($name == 'urWillPickup') {
        $this->setPickup($val);
      }

      if ($name == 'discountApplied') {
        $this->setDiscountApplied($val);
      }

      // Add fields that SHOULD be in orders for non-orders endpoints.
      if (\in_array($name, $orderFields, false)) {
        $this->$name = $val;
      }
    }

    // We have to set the customer Owned value after in case we need the ID check.
    if (isset($data['custOwn'])) {
      $this->setCustOwn($data['custOwn']);
    }
    else {
      // Lets the ID check run.
      $this->setCustOwn(FALSE);
    }

    if (!empty($jobsiteData)) {
      $jobsite = new Jobsite();
      $jobsite->import($jobsiteData);
      $this->setJobsite($jobsite);
    }

    // After everything has been added to the Item, attempt to set custom gpsUtilization property.
    $this->setGpsUtilization();
  }

  /**
   * Exports the new item data.
   */
  public function export() {
    // Export rates.
    $rate = $this->getRate();

    if (\is_object($rate) && $rate instanceof Rate) {
      $this->rate = $rate->export();
    }

    // Export jobsite details.
    $jobsite = $this->getJobsite();

    if (\is_object($jobsite) && $jobsite instanceof Jobsite) {
      $this->jobsite = $jobsite->export();
    }

    // Export GPS details.
    $gps = $this->getGps();

    if (\is_object($gps) && $gps instanceof Gps) {
      $this->gps = $gps->export();
    }

    return parent::export();
  }

  /**
   * Helper method to export the Item into DAL accepted format.
   *
   * @return array
   *   An associative array of keys, value pairs to be used for DAL submission
   *   as an "eqpDetails" item for Transactions endpoint
   */
  public function postExport() {
    list($category, $class) = $this->splitCatClass();

    // Ensures all the values are of the proper data type.
    $itemTemplate = [
      'category' => (string) $category,
      'class' => (string) $class,
      'quantity' => (int) $this->getQuantity(),
      'rateOverride' => $this->getDiscountApplied() ? 'Y' : 'N',
      'rateType' => 'WB',
      'minRate' => (float) $this->getRate()->getMinRate(),
      'dayRate' => (float) $this->getRate()->getDayRate(),
      'weekRate' => (float) $this->getRate()->getWeekRate(),
      'monthRate' => (float) $this->getRate()->getMonthRate(),
      'comment' => (string) $this->getComment(),
    ];

    return $itemTemplate;
  }

  /**
   * Merges an object of the same type (this type) with this object.
   *
   * Cleans up duplicate data.
   *
   * @param \Drupal\ur_appliance\Data\DAL\Item $item
   *   Item object.
   */
  public function mergeObjects(Item $item) {
    $methods = \get_class_methods(__CLASS__);

    foreach ($methods as $methodName) {
      /*
       * If the method is a getter?
       * Then determine the setter name.
       * Call the getter from the passed in order.
       * Use it to populate this object via the setter.
       */
      if (\strpos($methodName, 'get') === 0) {
        $setterName = 'set' . \substr($methodName, 3);
        $val = $item->$methodName();

        if (!empty($val)) {
          $this->$setterName();
        }
      }
    }
  }

  /**
   * Returns the item id.
   *
   * @return string
   *   ID string.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Sets the item id.
   *
   * @param $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * Returns the catClass.
   *
   * @return string
   *   catClass string.
   */
  public function getCatClass() {
    return $this->catClass;
  }

  /**
   * Sets the catClass.
   *
   * @param $catClass
   */
  public function setCatClass($catClass) {
    $this->catClass = $catClass;
  }

  /**
   * Returns the equipment ID.
   *
   * @return string
   *   Equipment ID string.
   */
  public function getEquipmentId() {
    return $this->equipmentId;
  }

  /**
   * Sets the equipment ID.
   *
   * @param string $equipmentId
   *   Equipment ID string.
   */
  public function setEquipmentId($equipmentId) {
    $this->equipmentId = $equipmentId;
  }

  /**
   * Returns the equipment title.
   *
   * @return string
   *   Equipment title string.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Sets the equipment title.
   *
   * @param string $title
   *   String title.
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Uses preset data to load the Drupal specific info.
   *
   * Loads the Drupal data for anything that might come from Drupal.
   *
   * @throws \Drupal\ur_appliance\Exception\DAL\DalDataException
   */
  public function loadDrupalData() {
    // First try to load from Drupal config; as long as a config.factory exists.
    if (empty(\Drupal::hasService('config.factory'))) {
      throw new DalDataException('Unable to load data because Drupal has not been initiated.');
    }

    if (empty($this->getId()) && empty($this->getCatClass())) {
      throw new DalDataException('Unable to load Drupal data without a node ID or cat class code.');
    }

    $node = NULL;

    try {
      if (!empty($this->id)) {
        $node = Node::load($this->id);
      }
      elseif (!empty($this->getCatClass()) && \Drupal::hasService('entity.query')) {
        $catClass = $this->getCatClass();

        $query = \Drupal::entityQuery('node')
          ->condition('type', 'item')
          ->condition('field_item_cat_class_code', $catClass);

        $nids = $query->execute();

        if (!empty($nids)) {
          $nodes = Node::loadMultiple($nids);

          /** @var \Drupal\node\Entity\Node $node */
          $node = \array_pop($nodes);
        }
      }

      if (!empty($node) && \is_object($node)) {
        $this->setTitle( $node->get('title')->getString() );
        $this->setDescription( $node->get('field_plain_long')->getString() );
        $this->setCategories( $node->get('field_item_category')->getValue() );
      }
    }
    catch (\Exception $e) {
      // Don't do anything. Just don't let this kill the call
    }
  }

  /**
   * Locates and loads the taxonomy term tids in the $categories array.
   *
   * @param array $categories
   *   The field_item_category taxonomy term ids for a given
   *   equipment item.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function setCategories(array $categories) {
    $this->parents = [];

    if (empty($categories)) {
      return;
    }

    foreach ($categories as $category) {
      if (empty($category)) {
        continue;
      }

      // Gotta load this to get the taxonomy vid.
      $term = Term::load($category['target_id']);

      if (empty($term) || !\is_object($term)) {
        continue; // We can't act on a non-object.
      }

      // Need all the parents of this term to be sure we find
      // the category we need where depth == 1.
      $parents = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadAllParents($category['target_id']);

      if (empty($parents) || !\is_array($parents)) {
        continue; // We can't act on a non-object.
      }

      // Not my favorite way to do this, but Core assures me
      // this is cached and is not CPU-intensive.
      $taxonomy_tree = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree($term->get('vid')->getString());

      if (empty($taxonomy_tree)) {
        continue; // We can't act on a non-object.
      }

      // Now we loop through the entire tree so we can see the
      // depth of each relevant parent.
      foreach ($taxonomy_tree as $leaf) {
        if (empty($leaf) || !\is_object($leaf) || $leaf->depth != 1) {
          continue; // We can't act on a non-object.
        }

        if (\array_key_exists($leaf->tid, $parents)) {
          $this->parents[$leaf->tid] = $leaf;

          // And here is the one we actually want!
          if ($leaf->depth == 1) {
            $this->subcategory = $leaf->name;
            break;
          }
        }
      }
    }
  }

  /**
   * From some derived values on the rental transaction item we can infer
   * the gps utilization label that will be displayed in the front-end
   */
  private function setGpsUtilization() {
    // Check if we have the fields we need. If we don't we will assume it's N/A.
    if (property_exists($this, 'simActive') && property_exists($this, 'reportedDateTime') && property_exists($this, 'lastMeterChange') && property_exists($this, 'usageToday')) {


      if ($this->simActive) {
        /*
         * simActive == TRUE, the item can send gps data.
         *
         * Using PHP's date_diff() gives us the ability to get the day intervals
         * between now and the reportedDateTime/lastMeterChange.
         */

        $now = date_create();
        $reported_date_time = date_create($this->reportedDateTime);
        $last_meter_change = date_create($this->lastMeterChange);

        $reported_interval = date_diff($now, $reported_date_time);
        $last_meter_interval = date_diff($now, $last_meter_change);


        if ($reported_interval->days >= 5) {
          // It hasn't reported in 5 or more days.
          $this->gpsUtilization = 'offline';
        }

        if ($reported_interval->days >= 1 && $reported_interval->days < 5) {
          // It hasn't reported in the last 24 hours, but within the last 5 days.
          $this->gpsUtilization = 'no-signal';
        }

        if ($reported_interval->days < 1 && ($last_meter_interval->days >= 3)) {
          // It's reported within the last 24 hours and hasn't been used in more than 3 days.
          $this->gpsUtilization = 'idle';
        }

        if ($reported_interval->days < 1 && $last_meter_interval->days < 3 && $this->usageToday == 0) {
          // Means it's "idle", but hasn't been used today
          $this->gpsUtilization = 'online';
        }

        if ($reported_interval->days < 1 && $last_meter_interval->days < 3 && ($this->usageToday > 0 && $this->usageToday <= 8.0)) {
          // Means it's "online", and has been used for less than 8 hours today.
          $this->gpsUtilization = 'active';
        }

        if ($reported_interval->days < 1 && $last_meter_interval->days < 3 && $this->usageToday > 8.0) {
          // Means it's "active", but it was used more than 8 hours today.
          $this->gpsUtilization = 'overtime';
        }
      }
    }
  }

  /**
   * Returns the equipment description.
   *
   * @return string
   *   Equipment description string.
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Sets the equipment description.
   *
   * @param string $description
   *   The new equipment description.
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Returns the remaining quantity.
   *
   * @return int
   *   The remaining quantity string.
   */
  public function getRemainingQty() {
    return $this->remainingQty;
  }

  /**
   * Sets the remaining quantity of the equipment.
   *
   * @param string $remainingQty
   *   The new remaining quantity value.
   */
  public function setRemainingQty($remainingQty) {
    $this->remainingQty = $remainingQty;
  }

  /**
   * Returns the DAL description for the item.
   *
   * @return string
   *   The DAL description string.
   */
  public function getDalDescription() {
    return $this->dalDescription;
  }

  /**
   * Updates the DAL description.
   *
   * @param string $dalDescription
   *   The new DAL description string.
   */
  public function setDalDescription($dalDescription) {
    $this->dalDescription = $dalDescription;
  }

  /**
   * Returns the schedule pickup quantity.
   *
   * @return int
   *   The scheduled Pickup Quantity.
   */
  public function getSchedulePickupQty() {
    return $this->scheduledPickupQty;
  }

  /**
   * Updates the scheduled pickup quantity.
   *
   * @param int $scheduledPickupQty
   *   New scheduled pickup quantity.
   */
  public function setScheduledPickupQty($scheduledPickupQty) {
    $this->scheduledPickupQty = $scheduledPickupQty;
  }

  /**
   * Returns item quantity.
   *
   * @return int
   *   returns the quantity string.
   */
  public function getQuantity() {
    return $this->quantity;
  }

  /**
   * Updates the item quantity.
   *
   * @param int $quantity
   *   New item quantity int.
   */
  public function setQuantity($quantity) {
    $this->quantity = $quantity;
  }

  /**
   * Returns the quantity on rent.
   *
   * @return int
   *   Item's quantity on rent
   */
  public function getQuantityOnRent() {
    return $this->quantityOnRent;
  }

  /**
   * Updates the quantity on rent.
   *
   * @param int $quantityOnRent
   *   New quantity on rent value.
   */
  public function setQuantityOnRent($quantityOnRent) {
    $this->quantityOnRent = $quantityOnRent;
  }

  /**
   * Returns the quantity returned.
   *
   * @return int
   *   The item's quantity returned.
   */
  public function getQuantityReturned() {
    return $this->quantityReturned;
  }

  /**
   * Updates the quantity returned value.
   *
   * @param int $quantityReturned
   *   New quantity returned value.
   */
  public function setQuantityReturned($quantityReturned) {
    $this->quantityReturned = $quantityReturned;
  }

  /**
   * Returns the quantity picked up value.
   *
   * @return int
   *   Quantity picked up.
   */
  public function getQuantityPickedUp() {
    return $this->quantityPickedUp;
  }

  /**
   * Updates the quantity picked up value.
   *
   * @param int $quantityPickedUp
   *   New quantity picked up value.
   */
  public function setQuantityPickedUp($quantityPickedUp) {
    $this->quantityPickedUp = $quantityPickedUp;
  }

  /**
   * Returns the quantity picked up value.
   *
   * @return int
   *   Quantity picked up.
   */
  public function getQuantityPendingPickup() {
    return $this->quantityPendingPickup;
  }

  /**
   * Updates the quantity picked up value.
   *
   * @param int $quantityPendingPickup
   *   New quantity picked up value.
   */
  public function setQuantityPendingPickup($quantityPendingPickup) {
    $this->quantityPendingPickup = $quantityPendingPickup;
  }

  /**
   * Returns the next pickup quantity value.
   *
   * @return int
   *   Quantity next pick up.
   */
  public function getQuantityNextPickup() {
    return $this->quantityNextPickup;
  }

  /**
   * Updates the next pickup quantity value.
   *
   * @param int $quantityNextPickup
   *   New quantity next pick up value.
   */
  public function setQuantityNextPickup($quantityNextPickup) {
    $this->quantityNextPickup = $quantityNextPickup;
  }

  /**
   * Returns the last meter change value.
   *
   * @return string
   *   Last meter change.
   */
  public function getLastMeterChange() {
    return $this->lastMeterChange;
  }

  /**
   * Updates the last meter change value.
   *
   * @param string $lastMeterChange
   *   New last meter change value.
   */
  public function setLastMeterChange($lastMeterChange) {
    $this->lastMeterChange = $lastMeterChange;
  }

  /**
   * Returns the usage today value.
   *
   * @return int
   *   Usage today value.
   */
  public function getUsageToday() {
    return $this->usageToday;
  }

  /**
   * Updates the usage today value.
   *
   * @param int $usageToday
   *   New usage today value.
   */
  public function setUsageToday($usageToday) {
    $this->usageToday = $usageToday;
  }

  /**
   * Returns the transaction type.
   *
   * @return string
   *   Transaction type string.
   */
  public function getTransType() {
    return $this->transType;
  }

  /**
   * Updates the transType value.
   *
   * @param string $transType
   *   New transType value.
   */
  public function setTransType($transType) {
    $this->transType = $transType;
  }

  /**
   * Returns item status.
   *
   * @return string
   *   Status value.
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Updates the status value.
   *
   * @param string $status
   *   New status value.
   */
  public function setStatus($status) {
    $statusMap = [
      'R' => 'Reservation',
      'O' => 'Rental',
      'C' => 'Returned',
      'Q' => 'Quote'
    ];

    $this->status = $statusMap[$status];
  }

  /**
   * Returns the return sequence.
   *
   * @return string
   *   returnSeq value.
   */
  public function getReturnSeq() {
    return $this->returnSeq;
  }

  /**
   * Updates the return sequence.
   *
   * @param string $returnSeq
   *   New returnSeq value (not sure if it is str||int).
   */
  public function setReturnSeq($returnSeq) {
    $this->returnSeq = $returnSeq;
  }

  /**
   * Returns GPS data.
   *
   * @return Gps
   *   GPS values.
   */
  public function getGps() {
    return $this->gps;
  }

  /**
   * Updates the GPS data.
   *
   * @param Gps|array $gps
   *   New GPS data.
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function setGps($gps) {
    // If receiving a GPS object then replace the current one.
    if (\is_object($gps) && \get_class($gps) == Gps::class) {
      $this->gps = $gps;
      return;
    }

    if (empty($this->gps)) {
      $this->gps = new Gps();
    }

    $this->gps->import($gps);
  }

  /**
   * Returns make of item.
   *
   * @return string
   *   Make value.
   */
  public function getMake() {
    return $this->make;
  }

  /**
   * Updates item make.
   *
   * @param string $make
   *   New make value.
   */
  public function setMake($make) {
    $this->make = $make;
  }

  /**
   * Returns item model.
   *
   * @return string
   *   Item Model value.
   */
  public function getModel() {
    return $this->model;
  }

  /**
   * Updates item Model.
   *
   * @param string $model
   *   New model value.
   */
  public function setModel($model) {
    $this->model = $model;
  }

  /**
   * Returns serial number.
   *
   * @return string
   *   Serial number value.
   */
  public function getSerial() {
    return $this->serial;
  }

  /**
   * Updates serial number value.
   *
   * @param string $serial
   *   New serial number value.
   */
  public function setSerial($serial) {
    $this->serial = $serial;
  }

  /**
   * Returns item rate.
   *
   * @return \Drupal\ur_appliance\Data\DAL\Rate
   *   Rate value.
   */
  public function getRate() {
    return $this->rate;
  }

  /**
   * Updates Rate value.
   *
   * @param \Drupal\ur_appliance\Data\DAL\Rate $rate
   *   New rate value.
   */
  public function setRate(Rate $rate) {
    $this->rate = $rate;
  }

  /**
   * Builds a rate object from an input array.
   *
   * @param object|array $data
   *   Rate data.
   *
   * @return \Drupal\ur_appliance\Data\DAL\Rate
   *   Rate object.
   *
   * @throws \Exception
   */
  public function buildRate($data) {
    if (!\is_object($data) && !\is_array($data)) {
      return NULL;
    }

    $rateObj = new Rate();
    $rateObj->import($data);

    $this->setRate($rateObj);
    return $rateObj;
  }

  /**
   * Return rental amount.
   *
   * @return float
   *   Rental amount value.
   */
  public function getRentalAmount() {
    return $this->rentalAmount;
  }

  /**
   * Update rental amount.
   *
   * @param float $rentalAmount
   *   New rental amount value.
   */
  public function setRentalAmount($rentalAmount) {
    $this->rentalAmount = $rentalAmount;
  }

  /**
   * Returns CustOwn value.
   *
   * @return bool
   *   CustOwn value.
   */
  public function isCustOwn() {
    if (!empty($this->custOwn)) {
      return $this->custOwn;
    }

    // Another indicator that this is customer-owned equipment.
    if ($this->checkCustOwnById()) {
      // Make sure we set it.
      $this->setCustOwn(TRUE);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Update custOwn value.
   *
   * @param bool $custOwn
   *   New custOwn value.
   */
  public function setCustOwn($custOwn) {
    $this->custOwn = FALSE;

    if ($custOwn === 1 || $custOwn == TRUE || $this->checkCustOwnById()) {
      $this->custOwn = TRUE;
    }
  }

  /**
   * Check the custOwnByID value.
   *
   * @return bool
   *   True|False.
   */
  protected function checkCustOwnById() {
    return strpos($this->urEquipmentId, 'COE') === 0;
  }

  /**
   * Returns item Comment.
   *
   * Return string
   *   Comment value.
   */
  public function getComment() {
    return $this->comment;
  }

  /**
   * Updates item comment value.
   *
   * @param string $comment
   *   New comment value.
   */
  public function setComment($comment) {
    $this->comment = $comment;
  }

  /**
   * Return related jobsite object.
   *
   * @return \Drupal\ur_appliance\Data\DAL\Jobsite
   *   Jobsite Object.
   */
  public function getJobsite() {
    return $this->jobsite;
  }

  /**
   * Update the related jobsite object.
   *
   * @param \Drupal\ur_appliance\Data\DAL\Jobsite $jobsite
   *   New jobsite object.
   */
  public function setJobsite(Jobsite $jobsite) {
    $this->jobsite = $jobsite;
  }

  /**
   * Return account codes.
   *
   * @return mixed|array
   *   Account codes
   */
  public function getAccountCodes() {
    return $this->accountCodes;
  }

  /**
   * Update account codes.
   *
   * @param mixed $accountCodes
   *   New account codes.
   */
  public function setAccountCodes($accountCodes) {
    $this->accountCodes = $accountCodes;
  }

  /**
   * Adds account codes.
   *
   * @param string $index
   *   Account code index.
   * @param string $value
   *   Account code value.
   */
  public function addAccountCode($index, $value) {
    $this->accountCodes[$index] = $value;
  }

  /**
   * Returns Transaction ID.
   *
   * @return string
   *   Transaction ID.
   */
  public function getTransId() {
    return $this->transId;
  }

  /**
   * Update transaction ID.
   *
   * @param string $transId
   *   New transaction ID value.
   */
  public function setTransId($transId) {
    $this->transId = $transId;
  }

  /**
   * Returns transaction Line ID.
   *
   * @return string
   *   Transaction Line ID.
   */
  public function getTransLineId() {
    return $this->transLineId;
  }

  /**
   * Updates the transaction line id.
   *
   * @param string $transLineId
   *   New transaction line id value.
   */
  public function setTransLineId($transLineId) {
    $this->transLineId = $transLineId;
  }

  /**
   * Returns the requisition ID.
   *
   * @return string
   *   Requisition ID.
   */
  public function getRequisitionId() {
    return $this->requisitionId;
  }

  /**
   * Updates the requisition ID.
   *
   * @param string $requisitionId
   *   New requisition ID value.
   */
  public function setRequisitionId($requisitionId) {
    $this->requisitionId = $requisitionId;
  }

  /**
   * Helper method to split a cat class into an array for specific DAL needs.
   *
   * @return array
   *   An array containing the 3 digit category and the 4 digit classification.
   *   Helps to return this in PHP list format, i.e. list($category, $class)
   */
  public function splitCatClass() {
    return \explode('-', $this->getCatClass());
  }

  /**
   * Returns pickup value.
   *
   * @return bool
   *   Pickup value.
   */
  public function getPickup() {
    return $this->pickup;
  }

  /**
   * Updates Pickup value.
   *
   * @param bool $pickup
   *   New pickup value.
   */
  public function setPickup($pickup) {
    $this->pickup = $pickup;
  }

  /**
   * Returns pickup ID.
   *
   * @return string
   *   Pickup ID.
   */
  public function getPickupId() {
    return $this->pickupId;
  }

  /**
   * Updates pickup ID.
   *
   * @param string $pickupId
   *   New pickup ID value.
   */
  public function setPickupId($pickupId) {
    $this->pickupId = $pickupId;
  }

  /**
   * Returns discount applied flag.
   *
   * @return bool
   *   Discount applied flag.
   */
  public function getDiscountApplied() {
    return $this->discountApplied;
  }

  /**
   * Updates the discount applied flag
   *
   * @param bool $discountApplied
   *   Discount Applied value.
   */
  public function setDiscountApplied($discountApplied) {
    $this->discountApplied = $discountApplied;
  }
}