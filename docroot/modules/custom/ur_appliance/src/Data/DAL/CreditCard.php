<?php

namespace Drupal\ur_appliance\Data\DAL;

use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Interfaces\DataInterface;

class CreditCard extends DataSimple implements DataInterface {
  /** @var string - Credit Card Token ID */
  protected $id;

  /** @var string - Credit Card Nick Name */
  protected $cardName;

  /** @var string - Card Holder Name */
  protected $cardHolder;

  /** @var string - Credit Card Type - A/D/M/V */
  protected $cardType;

  /** @var int - Last 4 Digit of Card# */
  protected $ccNum4;

  /** @var int - Card Expiration Date */
  protected $expireDate;

  /** @var string - Default Card? Y/N */
  protected $defaultCard;

  /** @var string - Card Status - A/D */
  protected $status;

  /** @var int - Account Number */
  protected $accountId;

  public function import($data) {
    parent::import($data);

    if (is_object($data)) {
      $data = (array) $data;
    }

    if (isset($data['cardId'])) {
      $this->setId($data['cardId']);
    }

    // Makes sure this gets turned into a boolean.
    if (isset($data['defaultCard'])) {
      $this->setDefaultCard($data['defaultCard']);
    }
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getCardName() {
    return $this->cardName;
  }

  /**
   * @param string $cardName
   */
  public function setCardName($cardName) {
    $this->cardName = $cardName;
  }

  /**
   * @return string
   */
  public function getCardHolder() {
    return $this->cardHolder;
  }

  /**
   * @param string $cardHolder
   */
  public function setCardHolder($cardHolder) {
    $this->cardHolder = $cardHolder;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->cardType;
  }

  /**
   * @param string $ccType
   */
  public function setType($ccType) {
    $this->cardType = $ccType;
  }

  /**
   * @return int
   */
  public function getCcNum4() {
    return $this->ccNum4;
  }

  /**
   * @param int $ccNum4
   */
  public function setCcNum4($ccNum4) {
    $this->ccNum4 = $ccNum4;
  }

  /**
   * @return int
   */
  public function getExpireDate() {
    return $this->expireDate;
  }

  /**
   * @param int $expireDate
   */
  public function setExpireDate($expireDate) {
    $this->expireDate = $expireDate;
  }

  /**
   * @return bool
   */
  public function isDefaultCard() {
    return $this->defaultCard;
  }

  /**
   * @param bool $defaultCard
   */
  public function setDefaultCard($defaultCard) {
    $this->defaultCard = false;

    if ($defaultCard == 'Y') {
      $this->defaultCard = true;
    }
  }

  /**
   * @return string
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param string $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * @return int
   */
  public function getAccountId() {
    return $this->accountId;
  }

  /**
   * @param int $accountId
   */
  public function setAccountId($accountId) {
    $this->accountId = $accountId;
  }
}