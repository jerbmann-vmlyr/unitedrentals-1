<?php
require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Drupal\ur_appliance\Command\WorkerCommand;
use Drupal\ur_appliance\Command\WorkerWithArgumentsCommand;

$application = new Application();


/**
 * Register commands
 * ===========================
 */
$application->add( new WorkerCommand() );
$application->add( new WorkerWithArgumentsCommand() );


// And the App is off to the races...
$application->run();