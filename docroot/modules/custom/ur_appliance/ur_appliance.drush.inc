<?php
/**
 * Ref: https://www.chapterthree.com/blog/how-to-create-custom-drush-commands
* Implements hook_drush_command().
*/
function ur_appliance_drush_command() {
  $commands['dal-log-toggle'] = [
    'description' => 'Turn on/off logging on dal controller',
      'aliases' => ['dlt'],
      'arguments' => [
      'arg1' => 'on/off',
    ],
    'options' => [
      'opt1' => 'My custom option.',
    ],
    'examples' => [
      'drush mec' => 'Print my example command.',
      'drush mec myargument' => 'Print my example command with an argument "myargument".',
      'drush mec myargument --opt1=myoption' => 'Print my example command with an argument "myargument" and an option "myoption".',
    ],
  ];

  return $commands;
}

/**
* Drush command logic.
* drush_[MODULE_NAME]_[COMMAND_NAME]().
*/
function drush_ur_appliance_dal_log_toggle($arg = '') {
  if ($arg !== '') {
    if (strtolower($arg) === 'on' ){
      api_appliance_toggle_on_dal_logger();
    }
    elseif(strtolower($arg) === 'off'){
      api_appliance_toggle_off_dal_logger();
    }
    else {
      drush_print(dt('@arg is not a valid option', compact('arg')));
    }
  } else {
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('api_appliance.config');
    $current = $config->get('dal_log');
    if ($current) {
      api_appliance_toggle_off_dal_logger();
    } else {
      api_appliance_toggle_on_dal_logger();
    }
  }
}

/**
 * Run from the dal-log-toggle method to turn off dal logger
 */
function ur_appliance_toggle_off_dal_logger() {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('api_appliance.config');
  $config->set('dal_log', FALSE);
  $config->save(TRUE);
  drush_print('dal log has been disabled');
}
/**
 * Run from the dal-log-toggle method to turn on dal logger
 */
function ur_appliance_toggle_on_dal_logger() {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('api_appliance.config');
  $config->set('dal_log', TRUE);
  $config->save(TRUE);
  drush_print('dal log has been enabled');
}