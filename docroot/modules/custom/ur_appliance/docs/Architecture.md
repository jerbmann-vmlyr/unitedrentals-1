# Architecture
This document is meant to describe the structure and intent of the various parts of the API Appliance. For a call visualization please refer to the diagram in the "diagrams" folder.


## Dependency Injection
This library leverages dependency injection to allow for flexibility in a number of areas, including HTTP clients and loggers. Future architecture will include a caching tool with data invalidation functionality as well as a way to push data to a search tool so that API call results can be searched by the global search feature.


## Providers
Providers are Gateways or one-stop shops to get to all the controllers and methods for a particular API. It simplifies invocations for each of them.
 

## Controllers
The controllers are classes meant to obfuscate access to one or more endpoints at a time. This includes making calls to multiple endpoints from one controller action or method.

An example of this would be creating new accounts and linking them to the current user through one method. That takes two calls to the DAL API. By using a controller, you can do both actions with one call from the code that uses the appliance.

These controllers **SHOULD NOT** have identical names or paths to the endpoints. They are meant to simplify access. As of the writing of this, this concept has already been violated and will hopefully be corrected in the near future.

The controllers and sub-controllers are meant to be divided up essentially as a category of calls and then sub-category. Organized for the logic of the developer.

Controllers have one final job. That is to export the data response as an array if requested by the calling code. This is accomplished by passing `TRUE` into the first controller as a parameter. Example below...

`$result = DAL::Rental(TRUE)->Transactions()->readAll($params);`


## Endpoints
The purpose of the endpoints is to assemble all the possible and required fields to make a call. They also take care of settings such as which token should be used for a particular call. In an endpoint you specify the URI path to access the endpoint from the particular API domain (which is specified in the Adapter).

The final job of the endpoint is to trigger the importing of the data into data objects. Sometimes this is done by looping through the returned data and passing in specific segments of a response. But most of the data management should be done in the Data Objects themselves.


## Data Objects
Data objects creates a common structure to store and use data returned from the DAL. It is also used to structure some types of data to send back to the API that is being called. For example, when creating orders/transactions within the DAL.

Data Objects can contain other data objects. For example, Items can contain Gps objects.


### Data Importing
The data objects should each know how to import their own data from a data response. At least from a single response array piece (at a minimum). The base object class has a generic import method that can import most basic data properties for an object. As long as the data can be matched up to an existing property then it will be able to import the data automatically.

If your object needs more complex data matching and importing then you must declare your own version of the import() method in your current class and add in what you need. I recommend first passing everything into the parent import method before processing it further down in your own method as we do in many of the Data Objects already.


## Adapters
Adapters are meant to take all the information provided by the endpoint and assemble it into an API-specific call. The base adapter does the heavy lifting for all of this. But each API needs it's own adapter that extends this base adapter. The extended, API-specific adapter will contain the base domain as well as pull in the tokens or API keys to finalize the calls.

The adapters will leverage the http client provided by Drupal, Guzzle HTTP. When running independently of Drupal it falls back to Guzzle on it's own.

