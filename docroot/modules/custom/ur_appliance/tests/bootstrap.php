<?php

/**
 * @file
 * Autoloader for Drupal PHPUnit testing.
 *
 * @see phpunit.xml.dist
 */

use Drupal\Component\Assertion\Handle;


/**
 * Finds all valid extension directories recursively within a given directory.
 *
 * @param string $scan_directory
 *   The directory that should be recursively scanned.
 * @return array
 *   An associative array of extension directories found within the scanned
 *   directory, keyed by extension name.
 */
function ur_appliance_phpunit_find_extension_directories($scan_directory) {
  $extensions = [];
  $dirs = new \RecursiveIteratorIterator(
    new \RecursiveDirectoryIterator($scan_directory,
    \RecursiveDirectoryIterator::FOLLOW_SYMLINKS)
  );

  foreach ($dirs as $dir) {
    $path = $dir->getPathname();

    if (stripos($path, 'vendor') !== false
        || stripos($path, '.git') !== false
        || stripos($path, '.idea') !== false) {
      continue;
    }

    if (strpos($dir->getPathname(), '.info.yml') !== FALSE) {
      /**
       * Cut off ".info.yml" from the filename for use as the extension name. We
       * use getRealPath() so that we can scan extensions represented by
       * directory aliases.
       */
      $extensions[substr($dir->getFilename(), 0, -9)] = $dir->getPathInfo()
        ->getRealPath();
    }
  }

  return $extensions;
}

/**
 * Registers the namespace for each extension directory with the autoloader.
 *
 * @param array $dirs
 *   An associative array of extension directories, keyed by extension name.
 *
 * @return array
 *   An associative array of extension directories, keyed by their namespace.
 */
function ur_appliance_phpunit_get_extension_namespaces($dirs) {
  $suite_names = ['Unit', 'Kernel', 'Functional', 'FunctionalJavascript'];
  $namespaces = [];

  foreach ($dirs as $extension => $dir) {
    if (is_dir($dir . '/src')) {
      // Register the PSR-4 directory for module-provided classes.
      $namespaces['Drupal\\' . $extension . '\\'][] = $dir . '/src';
    }

    $test_dir = $dir . '/tests/src';

    if (is_dir($test_dir)) {
      foreach ($suite_names as $suite_name) {
        $suite_dir = $test_dir . '/' . $suite_name;

        if (is_dir($suite_dir)) {
          // Register the PSR-4 directory for PHPUnit-based suites.
          $namespaces['Drupal\\Tests\\' . $extension . '\\' . $suite_name . '\\'][] = $suite_dir;
        }
      }

      // Extensions can have a \Drupal\extension\Traits namespace for
      // cross-suite trait code.
      $trait_dir = $test_dir . '/Traits';

      if (is_dir($trait_dir)) {
        $namespaces['Drupal\\Tests\\' . $extension . '\\Traits\\'][] = $trait_dir;
      }
    }
  }

  return $namespaces;
}

/**
 * We define the COMPOSER_INSTALL constant, so that PHPUnit knows where to
 * autoload from. This is needed for tests run in isolation mode, because
 * phpunit.xml.dist is located in a non-default directory relative to the
 * PHPUnit executable.
 */
if (!defined('PHPUNIT_COMPOSER_INSTALL')) {
  define('PHPUNIT_COMPOSER_INSTALL', __DIR__ . '/../vendor/autoload.php');
}

if(!defined('MOCK_RESPONSE_ROOT')){
  define('MOCK_RESPONSE_ROOT', __DIR__ . '/data/Endpoint/DAL');
}

/**
 * Populate class loader with additional namespaces for tests.
 *
 * We run this in a function to avoid setting the class loader to a global
 * that can change. This change can cause unpredictable false positives for
 * phpunit's global state change watcher. The class loader can be retrieved from
 * composer at any time by requiring autoload.php.
 */
function ur_appliance_phpunit_populate_class_loader() {
  /** @var \Composer\Autoload\ClassLoader $loader */
  $loader = require __DIR__ . '/../vendor/autoload.php';

  if (!isset($GLOBALS['namespaces'])) {
    // Scan for arbitrary extension namespaces from core and contrib.
    $extension_roots = [ dirname(__DIR__) ];

    $dirs = array_map('ur_appliance_phpunit_find_extension_directories', $extension_roots);
    $dirs = array_reduce($dirs, 'array_merge', []);

    $GLOBALS['namespaces'] = ur_appliance_phpunit_get_extension_namespaces($dirs);
  }

  foreach ($GLOBALS['namespaces'] as $prefix => $paths) {
    $loader->addPsr4($prefix, $paths);
  }

  return $loader;
}

// Do class loader population.
ur_appliance_phpunit_populate_class_loader();

/**
 * Set sane locale settings, to ensure consistent string, dates, times and
 * numbers handling.
 * @see \Drupal\Core\DrupalKernel::bootEnvironment()
 */
setlocale(LC_ALL, 'C');

/**
 * Set the default timezone. While this doesn't cause any tests to fail, PHP
 * complains if 'date.timezone' is not set in php.ini.
 */
date_default_timezone_set('America/Chicago');

/**
 * Runtime assertions. PHPUnit follows the php.ini assert.active setting for
 * runtime assertions. By default this setting is on. Here we make a call to
 * make PHP 5 and 7 handle assertion failures the same way, but this call does
 * not turn runtime assertions on if they weren't on already.
 */
Handle::register();
