<?php

namespace Drupal\ur_appliance\Test\Controller\DAL\Service;

use Drupal\ur_appliance\Data\DAL\Branch;
use Drupal\ur_appliance\Controller\DAL\Service\BranchController as BranchController;
use Drupal\ur_appliance\Endpoint\DAL\Location\BranchDistancesEndpoint;
use Drupal\ur_appliance\Endpoint\DAL\Location\BranchesEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

/**
 * Class BranchTest
 * @package Drupal\ur_appliance\Test\Controller\DAL\Service
 */
class BranchTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->uri = 'location/branches';
    $this->setClassUnderTest(BranchController::class);
    $this->getSystemUnderTest();
  }

  /**
   * Ensures we are returned a Branches endpoint object.
   */
  public function testGetBranchEndpoint() {
    $BranchCtl = $this->getSystemUnderTest();
    $endpoint = $BranchCtl->getBranchEndpoint();

    $this->assertInstanceOf(BranchesEndpoint::class, $endpoint);
  }

  public function testGetBranchDistanceEndpoint() {
    $BranchCtl = $this->getSystemUnderTest();
    $endpoint = $BranchCtl->getBranchDistanceEndpoint();

    $this->assertInstanceOf(BranchDistancesEndpoint::class, $endpoint);
  }

  /**
   * Ensure we are returned an array of Branch data objects.
   *
   * @group live
   */
  public function testReadAll() {
    $BranchCtl = $this->getSystemUnderTest();
    $BranchCtl->getBranchEndpoint()->getAdapter()->setUri($this->uri);
    $BranchCtl->getBranchEndpoint()->getAdapter()->shouldForceWORSToken();

    // Commented this out because it makes a real call.
    // @TODO: Mock the response instead.
    $branches = $BranchCtl->readAll();

    foreach ($branches as $branch) {
      $this->assertInstanceOf(Branch::class, $branch);
    }
  }

  /**
   * Ensure when we call Branch Distances that our Branch objects have
   * a "distance" property
   *
   * @group live
   */
  public function testReadDistances() {
    $data = ['city' => 'Kansas City', 'state' => 'MO', 'limit' => 1];
    $uri = 'location/branchdistances';
    $BranchCtl = $this->getSystemUnderTest();

    $BranchCtl->getBranchDistanceEndpoint()->getAdapter()->setUri($uri);
    $BranchCtl->getBranchDistanceEndpoint()->getAdapter()->shouldForceWORSToken();

    // Commented this out because it makes a real call.
    // @TODO: Mock the response instead.
    $branches = $BranchCtl->readDistances($data);

    foreach ($branches as $branch) {
      $this->assertNotEmpty($branch->getDistance());
    }
  }
}