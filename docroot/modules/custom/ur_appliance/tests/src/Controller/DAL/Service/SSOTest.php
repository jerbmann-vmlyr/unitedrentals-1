<?php

namespace Drupal\ur_appliance\Test\Controller\DAL\Service;

use Drupal\ur_appliance\Controller\DAL\Service\SsoController;
use Drupal\ur_appliance\Test\BaseTest;

/**
 * Class SSOTest
 * @package Drupal\ur_appliance\Test\Controller\DAL\Service
 */
class SSOTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->uri = 'ssologins';
    $this->setClassUnderTest(SsoController::class);
    $this->getSystemUnderTest();
  }

  /**
   * @group live
   */
  public function testSSOLogin() {
    $sso = $this->getSystemUnderTest();
    $email = 'ur.vml.5@gmail.com';
    $sso->getSSOEndpoint()->getAdapter()->shouldForceWORSToken();

    // Commented this out because it makes a real call.
    // @TODO: Mock the response instead.
    $response = $sso->read($email);
    $this->assertArrayHasKey('token', $response);
    $this->assertArrayHasKey('message', $response);
  }
}
