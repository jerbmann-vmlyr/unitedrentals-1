<?php

namespace Drupal\ur_appliance\Test\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\DAL\Customer\JobsiteController as JobsiteController;
use Drupal\ur_appliance\Data\DAL\Jobsite;
use Drupal\ur_appliance\Endpoint\DAL\Customer\JobsitesEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class JobsiteTest extends BaseTest {

  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(JobsiteController::class);
    $this->getSystemUnderTest();
  }

  // @TODO: Fix this test. Getting type conversion error.
//  public function testCreate() {
//    $jobsiteController = $this->getSystemUnderTest();
//    $data = [
//      'accountId' => '1069997'
//    ];
//    $response = $this->$jobsiteController->create($data);
//
//    print_r(json_encode($response));
//  }

}