<?php

namespace Drupal\ur_appliance\Test\Controller\DAL\Rental;

use Drupal\ur_appliance\Test\BaseTest;
use Drupal\ur_appliance\Data\DAL\Rate;
use Drupal\ur_appliance\Endpoint\DAL\Rental\RatesEndpoint;
use Drupal\ur_appliance\Controller\DAL\Rental\RatesController as RatesController;

/**
 * Class RatesTest
 *
 * @package Drupal\ur_appliance\Test\Controller\DAL\Rental
 */
class RatesTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->uri = 'rental/rates';
    $this->setClassUnderTest(RatesController::class);
    $this->getSystemUnderTest();
  }

  /**
   * Ensures we are returned a Rates endpoint object.
   */
  public function testGetRatesEndpoint() {
    /** @var RatesController $ratesController */
    $ratesController = $this->getSystemUnderTest();
    $endpoint = $ratesController->getRatesEndpoint();

    $this->assertInstanceOf(RatesEndpoint::class, $endpoint);
  }

  /**
   * Ensure we are returned a Rates data object.
   */
  public function testRead() {
    //
  }

  /**
   * @group live
   */
  public function testRetrieve() {
    /** @var RatesController $ratesController */
    $ratesController = $this->getSystemUnderTest();
    $data = [
      'catClass' => ['310-6001', '240-3131'],
      'branchId' => 'J90'
    ];

    $result = $ratesController->retrieve($data);
    $this->assertInstanceOf(Rate::class, $result[0]);
    $this->assertInstanceOf(Rate::class, $result[1]);
  }

  /**
   * @group live
   */
  public function testRetrieveWithPlaceId() {
    /** @var RatesController $ratesController */
    $ratesController = $this->getSystemUnderTest();
    $data = [
      'catClass' => ['310-6001', '240-3131'],
      'placeId' => 'ChIJC03wb838wIcR6XgcfYWM4Ec'
    ];

    $result = $ratesController->retrieve($data);
    $this->assertInstanceOf(Rate::class, $result[0]);
    $this->assertInstanceOf(Rate::class, $result[1]);
  }
}
