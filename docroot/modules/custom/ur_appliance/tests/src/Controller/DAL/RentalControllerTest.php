<?php

namespace Drupal\ur_appliance\Test\Controller\DAL;

use Drupal\ur_appliance\Test\BaseTest;
use Drupal\ur_appliance\Controller\DAL\RentalController;
use Drupal\ur_appliance\Controller\DAL\Rental\RatesController;
use Drupal\ur_appliance\Controller\DAL\Rental\EstimatesController;
use Drupal\ur_appliance\Controller\DAL\Rental\TransactionsController;

class RentalControllerTest extends BaseTest {
  protected $systemUnderTest;

  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(RentalController::class);
    $this->getSystemUnderTest();
  }

  public function testRates() {
    /** @var \Drupal\ur_appliance\Controller\DAL\RentalController $rental */
    $rental = $this->getSystemUnderTest();
    $this->assertInstanceOf(RatesController::class, $rental->Rates());
  }

  public function testEstimates() {
    /** @var \Drupal\ur_appliance\Controller\DAL\RentalController $rental */
    $rental = $this->getSystemUnderTest();
    $this->assertInstanceOf(EstimatesController::class, $rental->Estimates());
  }

  public function testTransactions() {
    /** @var \Drupal\ur_appliance\Controller\DAL\RentalController $rental */
    $rental = $this->getSystemUnderTest();
    $this->assertInstanceOf(TransactionsController::class, $rental->Transactions());
  }
}