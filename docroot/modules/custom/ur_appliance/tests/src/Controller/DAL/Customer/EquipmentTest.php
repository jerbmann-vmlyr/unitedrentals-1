<?php

namespace Drupal\ur_appliance\Test\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\DAL\Customer\EquipmentController;
use Drupal\ur_appliance\Provider\DAL;
use Drupal\ur_appliance\Test\BaseTest;

class EquipmentTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(EquipmentController::class);
    $this->getSystemUnderTest();
  }

  /**
   * @group live
   */
  public function testReadCombined() {
    /** @var EquipmentController $controller */
    $controller = $this->getSystemUnderTest();
    DAL::Customer()->setEmail('ur.vml.7@gmail.com');

    $data = [
      'accountId' => '858702',
      'types' => '1,2,3'
    ];

    $response = $controller->readCombined($data);
    $controller->setProvideExportedObjects(true);
    $responseArray = $controller->export($response);

    $this->assertNotEmpty($response);
    $this->assertNotEmpty($responseArray); // Just verify the response
  }
}