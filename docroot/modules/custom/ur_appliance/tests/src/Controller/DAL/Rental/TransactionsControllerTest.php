<?php

namespace Drupal\ur_appliance\Test\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\DAL\Rental\TransactionsController;
use Drupal\ur_appliance\Test\BaseTest;

class TransactionsControllerTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(TransactionsController::class);
    $this->getSystemUnderTest();
  }

  /**
   * @group live
   *
   * @TODO: Setup mocks for testing
   */
  public function testLiveSubmit() {
    $mockJsonData = '{"transaction":{"branchId":"639","accountId":"34","transType":"R","email":"no-email@on-file.com","jobsiteId":"1","jobsiteContact":"PJ","jobsitePhone":"9991234567","startDateTime":"2018-08-24T16:00:00","returnDateTime":"2018-08-31T16:00:00","delivery":true,"pickup":false,"pickupFirm":false,"rpp":false,"instructions":"Use loading dock #8.","items":{"310-9801":{"catClass":"310-9801","quantity":"1","notes":"Bring more.","rate":{"dayrate":"1321","weekrate":"3813","monthrate":"7979","minrate":"1321"}}},"totals":{"totrentamt":3813,"totenvamt":49.56,"tottaxamt":259.75,"totgstamt":0,"totdwamt":0,"totdelamt":0,"totpuamt":0,"miscCharges":0,"authAmount":5152.89,"authPercent":125,"rateZone":"275","tspAgrmt":"N","tspAgrmtRate":0}},"requisitionCodes":[{"values":[{"id":"1","value":"RC1"}]}],"allocationCodes":[{"values":[{"id":"splitStart","value":"20181025"},{"id":"1","value":"AC1 HERE"},{"id":"2","value":"TEST ALLOC CODE 1"},{"id":"3","value":"Some other value"},{"id":"splitPerc","value":"25"}]},{"values":[{"id":"splitStart","value":"20181025"},{"id":"1","value":"GROUP 2 FIELD 1 VALUE"},{"id":"2","value":"GROUP 2 FIELD 2 VALUE"},{"id":"splitPerc","value":"75"}]},{"values":[{"id":"splitStart","value":"20181108"},{"id":"3","value":"GROUP 3 FIELD 3 VALUE"},{"id":"splitPerc","value":"100"}]}]}
';

    $mockData = json_decode($mockJsonData, true);
    $mockData = (array) $mockData;

    /** @var TransactionsController $controller */
    $controller = $this->getSystemUnderTest();
    $controller->getTransactionEndpoint()->getAdapter()->setUserToken(
      '050c1f19c749edb96553b4c38596434149e6fcc1'
    );

    $controller->submit($mockData);
  }
}