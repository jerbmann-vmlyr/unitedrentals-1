<?php

namespace Drupal\ur_appliance\Test\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\DAL\Rental\InvoicesController;
use Drupal\ur_appliance\Test\BaseTest;

class InvoicesControllerTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->uri = 'rental/invoices';
    $this->setClassUnderTest(InvoicesController::class);
    $this->getSystemUnderTest();
  }

  /**
   * @group invoice
   */
  public function testGetTotalOutstandingEndpoint() {

    $params = ['accountId' => '50343'];

    /** @var InvoicesController $invoiceController */
    $invoiceController = $this->getSystemUnderTest();
    $invoiceEndpoint = $invoiceController->getInvoiceEndpoint();
    $invoiceEndpoint->getAdapter()->setUserEmail('wayne.anderson@vml.com');
    $invoiceEndpoint->getAdapter()->setUri($this->uri);
    $totalDue = $invoiceController->outstandingInvoices($params);

  }
}