<?php

namespace Drupal\ur_appliance\Test\Controller\DAL\Rental;

use Drupal\ur_appliance\Controller\DAL\Rental\CostAllocationController as CostAllocationController;
use Drupal\ur_appliance\Data\DAL\CostAllocation;
use Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionCostAllocationsEndpoint;
use Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionCostAllocationsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

/**
 * Class AllocationCodeTest
 *
 * @package Drupal\ur_appliance\Test\Controller\DAL\Rental
 */
class CostAllocationTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(CostAllocationController::class);
    $this->getSystemUnderTest();
  }

  /**
   * Ensures we are returned a TransactionCostAllocations endpoint object.
   */
  public function testGetTransactionEndpoint() {
    $costController = $this->getSystemUnderTest();
    $endPoint = $costController->getTransactionEndpoint();

    $this->assertInstanceOf(TransactionCostAllocationsEndpoint::class, $endPoint);
  }

  /**
   * Ensures we are returned a RequisitionCostAllocations endpoint object.
   */
  public function testGetRequisitionEndpoint() {
    $costController = $this->getSystemUnderTest();
    $endPoint = $costController->getRequisitionEndpoint();

    $this->assertInstanceOf(RequisitionCostAllocationsEndpoint::class, $endPoint);
  }


  // @TODO: Fix this test. Gets a token error.
  public function testReadAll() {
    $transId = '144060476';
    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
    $codeController = $this->getSystemUnderTest();
//    $costAllocations = $codeController->readAllTransactionAllocations(['transId' => $transId]);
//
//    foreach ($costAllocations as $allocation) {
//      $this->assertInstanceOf(CostAllocation::class, $allocation);
//    }
  }

  /**
   * Ensure the buildDateRangeArray is returning a correctly formed array of start date/end date key/value pairs
   */
  public function testBuildDateRangeArray() {
    $arrayToTest = [
      '20170305' => '20171024',
      '20171025' => '20171031',
      '20171101' => '',
    ];

    // $testJson = '{"allocationCodes":[{"values":[{"id":"splitStart","value":"20171025"},{"id":"1","value":"GROUP 1 FIELD 1 VALUE"},{"id":"2","value":"GROUP 1 FIELD 2 VALUE"},{"id":"splitPerc","value":"50"}]},{"values":[{"id":"splitStart","value":"20171101"},{"id":"1","value":"GROUP 2 FIELD 1 VALUE"},{"id":"2","value":"GROUP 2 FIELD 2 VALUE"},{"id":"splitPerc","value":"100"}]},{"values":[{"id":"splitStart","value":"20170305"},{"id":"1","value":"GROUP 3 FIELD 1 VALUE"},{"id":"splitPerc","value":"100"}]},{"values":[{"id":"splitStart","value":"20171025"},{"id":"1","value":"GROUP 4 FIELD 1 VALUE"},{"id":"splitPerc","value":"50"}]}]}';

    $testAllocations[] = [
      'values' => [
        ['id' => 'splitStart', 'value' => '20171025'],
        ['id' => '1', 'value' => 'val1.1'],
        ['id' => '2', 'value' => 'val1.2'],
        ['id' => '3', 'value' => 'val1.3'],
        ['id' => '4', 'value' => 'val1.4'],
        ['id' => 'splitPerc', 'value' => '50'],
      ],
    ];
    $testAllocations[] = [
      'values' => [
        ['id' => 'splitStart', 'value' => '20171101'],
        ['id' => '1', 'value' => 'val2.1'],
        ['id' => '2', 'value' => 'val2.2'],
        ['id' => '3', 'value' => 'val2.3'],
        ['id' => '4', 'value' => 'val2.4'],
        ['id' => 'splitPerc', 'value' => '100'],
      ],
    ];
    $testAllocations[] = [
      'values' => [
        ['id' => 'splitStart', 'value' => '20170305'],
        ['id' => '1', 'value' => 'val3.1'],
        ['id' => '2', 'value' => 'val3.2'],
        ['id' => '3', 'value' => 'val3.3'],
        ['id' => '4', 'value' => 'val3.4'],
        ['id' => 'splitPerc', 'value' => '100'],
      ],
    ];
    $testAllocations[] = [
      'values' => [
        ['id' => 'splitStart', 'value' => '20171025'],
        ['id' => '1', 'value' => 'val4.1'],
        ['id' => '2', 'value' => 'val4.2'],
        ['id' => '3', 'value' => 'val4.3'],
        ['id' => '4', 'value' => 'val4.4'],
        ['id' => 'splitPerc', 'value' => '50'],
      ],
    ];

    $endPoint = $this->getSystemUnderTest();
    $dateArray = $endPoint->getDateRangeArray($testAllocations);

    $this->assertEquals(3, count($dateArray));
    $this->assertEquals($arrayToTest, $dateArray);
  }

  /**
   * Ensure updateTransaction works
   *
   * @TODO: Replace this test since this method no longer exists, yet the test
   *        may still be valuable in other ways.
   *
   * @TODO: Fix or replace.
   */
  public function testTransactionUpdate() {
    $transId = '148189394';
    //$token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
    $allocationJson = '[{"values":[{"id":"splitStart","value":"20170910"},{"id":"1","value":"AC1 HERE"},{"id":"2","value":"TEST ALLOC CODE 1"},{"id":"3","value":"3"},{"id":"splitPerc","value":"100"}]},{"values":[{"id":"splitStart","value":"20170915"},{"id":"1","value":"AC1 HERE 2"},{"id":"2","value":"Anything 2 "},{"id":"3","value":"3 2"},{"id":"splitPerc","value":"100"}]}]';
    $data = [
      'transId' => $transId,
      'codeType' => 'A',
      'allocationCodes' => json_decode($allocationJson, TRUE),
    ];

    /** @var \Drupal\ur_appliance\Controller\DAL\Rental\CostAllocationController $codeController */
//    $codeController = $this->getSystemUnderTest();
//    $codeController->getTransactionEndpoint()->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
//    $response = $codeController->updateTransaction($data);
//
//    // Not sure this is the expected response from a successful call.
//    $this->assertEquals(1, $response);
  }
}