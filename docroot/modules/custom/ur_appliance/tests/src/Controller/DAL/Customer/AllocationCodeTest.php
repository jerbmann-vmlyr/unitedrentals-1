<?php

namespace Drupal\ur_appliance\Test\Controller\DAL\Customer;

use Drupal\ur_appliance\Controller\DAL\Customer\AllocationCodeController as AllocationCodeController;
use Drupal\ur_appliance\Data\DAL\AllocationCode;
use Drupal\ur_appliance\Data\DAL\CodeValue;
use Drupal\ur_appliance\Data\DAL\CodeTrigger;
use Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationRequisitionCodeValuesEndpoint;
use Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationRequisitionCodeTitlesEndpoint;
use Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationTriggersEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

/**
 * Class AllocationCodeTest
 *
 * @package Drupal\ur_appliance\Test\Controller\DAL\Customer
 */
class AllocationCodeTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(AllocationCodeController::class);
    $this->getSystemUnderTest();
  }

  /**
   * Ensures we are returned a AllocationRequisitionCodeValues endpoint object.
   */
  public function testGetValuesEndpoint() {
    $codeController = $this->getSystemUnderTest();
    $endpoint = $codeController->getValuesEndpoint();

    $this->assertInstanceOf(AllocationRequisitionCodeValuesEndpoint::class, $endpoint);
  }

  /**
   * Ensures we are returned a AllocationRequisitionCodeTitles endpoint object.
   */
  public function testGetTitlesEndpoint() {
    $codeController = $this->getSystemUnderTest();
    $endpoint = $codeController->getTitlesEndpoint();

    $this->assertInstanceOf(AllocationRequisitionCodeTitlesEndpoint::class, $endpoint);
  }

  /**
   * Ensures we are returned a AllocationTriggers endpoint object.
   */
  public function testGetTriggersEndpoint() {
    $codeController = $this->getSystemUnderTest();
    $endpoint = $codeController->getTriggersEndpoint();

    $this->assertInstanceOf(AllocationTriggersEndpoint::class, $endpoint);
  }

  /**
   * Ensures value lists are being correctly returned from assignCodeValues function
   */
  public function testAssignCodeValues() {
    $arrayToTest = ['TestVal1', 'TestVal2'];

    $testCode = new AllocationCode();
    $testCode->setId("1");
    $testCode->setCodeType("A");
    $testCode->setRequired(false);

    $codeValue1 = new CodeValue();
    $codeValue1->setAccountId("1278567");
    $codeValue1->setCodeType("A");
    $codeValue1->setIdNum("1");
    $codeValue1->setVal("TestVal1");

    $codeValue2 = new CodeValue();
    $codeValue2->setAccountId("1278567");
    $codeValue2->setCodeType("A");
    $codeValue2->setIdNum("1");
    $codeValue2->setVal("TestVal2");

    $codeValue3 = new CodeValue();
    $codeValue3->setAccountId("1278567");
    $codeValue3->setCodeType("A");
    $codeValue3->setIdNum("3");
    $codeValue3->setVal("TestVal3");

    $codeValue4 = new CodeValue();
    $codeValue4->setAccountId("1278567");
    $codeValue4->setCodeType("R");
    $codeValue4->setIdNum("1");
    $codeValue4->setVal("TestVal4");

    $codeValues = [
      $codeValue1,
      $codeValue2,
      $codeValue3,
      $codeValue4,
    ];

    $codeController = new AllocationCodeController();
    $testCode->setValueList($codeController->assignCodeValues($testCode, $codeValues));

    $this->assertEquals(2, count($testCode->getValueList()));
    $this->assertEquals($arrayToTest, $testCode->getValueList());
  }

  /**
   * Ensures value lists are being correctly returned from assignTriggerValues function
   */
  public function testAssignTriggerValues() {
    $testArray[] = [
      'triggerValue' => 'Field Value 1',
      'updateCodeId' => '4',
      'updateCodeValue' => 'TestVal1',
      ];

    $testArray[] = [
      'triggerValue' => 'Field Value 3',
      'updateCodeId' => '8',
      'updateCodeValue' => 'TestVal3',
    ];

    $testArray[] = [
      'triggerValue' => 'Field Value 5',
      'updateCodeId' => '4',
      'updateCodeValue' => 'TestVal5',
    ];

    $testCode = new AllocationCode();
    $testCode->setId("1");
    $testCode->setCodeType("A");
    $testCode->setRequired(false);

    $codeTrigger1 = new CodeTrigger();
    $codeTrigger1->setAccountId("1278567");
    $codeTrigger1->setCodeType("A");
    $codeTrigger1->setIdNum("1");
    $codeTrigger1->setVal("TestVal1");
    $codeTrigger1->setTriggerId("4");
    $codeTrigger1->setTriggerName("Comments");
    $codeTrigger1->setTriggerVal("Field Value 1");

    $codeTrigger2 = new CodeTrigger();
    $codeTrigger2->setAccountId("1278567");
    $codeTrigger2->setCodeType("A");
    $codeTrigger2->setIdNum("3");
    $codeTrigger2->setVal("TestVal2");
    $codeTrigger2->setTriggerId("4");
    $codeTrigger2->setTriggerName("Comments");
    $codeTrigger2->setTriggerVal("Field Value 2");

    $codeTrigger3 = new CodeTrigger();
    $codeTrigger3->setAccountId("1278567");
    $codeTrigger3->setCodeType("A");
    $codeTrigger3->setIdNum("1");
    $codeTrigger3->setVal("TestVal3");
    $codeTrigger3->setTriggerId("8");
    $codeTrigger3->setTriggerName("Account");
    $codeTrigger3->setTriggerVal("Field Value 3");

    $codeTrigger4 = new CodeTrigger();
    $codeTrigger4->setAccountId("1278567");
    $codeTrigger4->setCodeType("R");
    $codeTrigger4->setIdNum("1");
    $codeTrigger4->setVal("TestVal4");
    $codeTrigger4->setTriggerId("6");
    $codeTrigger4->setTriggerName("PO Number");
    $codeTrigger4->setTriggerVal("Field Value 4");

    $codeTrigger5 = new CodeTrigger();
    $codeTrigger5->setAccountId("1278567");
    $codeTrigger5->setCodeType("A");
    $codeTrigger5->setIdNum("1");
    $codeTrigger5->setVal("TestVal5");
    $codeTrigger5->setTriggerId("4");
    $codeTrigger5->setTriggerName("Comments");
    $codeTrigger5->setTriggerVal("Field Value 5");

    $triggerValues = [
      $codeTrigger1,
      $codeTrigger2,
      $codeTrigger3,
      $codeTrigger4,
      $codeTrigger5,
    ];

    $codeController = new AllocationCodeController();
    $testCode->setTriggers($codeController->assignTriggerValues($testCode, $triggerValues));

    $this->assertEquals(3, count($testCode->getTriggers()));
    $this->assertEquals($testArray, $testCode->getTriggers());
  }

  /*
   * //@TODO Fix this test. Causes a PHP critical error 255.
  public function testReadAll() {
    $accountId = '1278567';
    $codeController = $this->getSystemUnderTest();
    $allocationCodes = $codeController->readAll(['accountId' => $accountId]);

    foreach ($allocationCodes as $code) {
      $this->assertInstanceOf(AllocationCode::class, $code);
    }
  }
  */
}