<?php

namespace Drupal\ur_appliance\Test\Controller\DAL;

use Drupal\ur_appliance\Test\BaseTest;
use Drupal\ur_appliance\Controller\DAL\ServiceController;
use Drupal\ur_appliance\Controller\DAL\Service\BranchController;
use Drupal\ur_appliance\Controller\DAL\Service\SsoController;

class ServiceControllerTest extends BaseTest {
  protected $systemUnderTest;

  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(ServiceController::class);
  }

  public function testBranch() {
    $service = $this->getSystemUnderTest();
    $this->assertInstanceOf(BranchController::class, $service->Branch());
  }

  public function testSSO() {
    $service = $this->getSystemUnderTest();
    $this->assertInstanceOf(SsoController::class, $service->SSO());
  }
}