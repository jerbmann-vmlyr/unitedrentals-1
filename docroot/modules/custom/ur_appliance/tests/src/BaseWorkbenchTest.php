<?php

namespace Drupal\ur_appliance\Test;

use Drupal\ur_appliance\Adapter\Placeable\WorkbenchAdapter;

/**
 * Class BaseWorkbenchTest
 *
 * @package Drupal\ur_appliance\Test
 * @TODO: Build out the Placeable tests
 */
class BaseWorkbenchTest extends BaseTest {
  protected $defaultAdapter = WorkbenchAdapter::class;
  protected $defaultLiveAdapter = 'workbench_adapter';

  protected $baseUrl = 'http://workbench.placeable.com/v2/collection/57470c7a0b00002900121396';
}