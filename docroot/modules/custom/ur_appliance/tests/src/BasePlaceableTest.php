<?php

namespace Drupal\ur_appliance\Test;

use Drupal\ur_appliance\Adapter\Placeable\PlaceableAdapter;

/**
 * Class BasePlaceableTest
 *
 * @package Drupal\ur_appliance\Test
 * @TODO: Build out the Placeable tests
 */
class BasePlaceableTest extends BaseTest {
  protected $defaultAdapter = PlaceableAdapter::class;
  protected $defaultLiveAdapter = 'placeable_adapter';

  protected $baseUrl = 'https://Drupal\ur_appliance.test.placeablepages.com';
}