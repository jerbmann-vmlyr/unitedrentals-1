<?php
namespace Drupal\ur_appliance\Test\Data\DAL;

use Drupal\ur_appliance\Data\DAL\Jobsite;
use Drupal\ur_appliance\Data\DataSimple;
use Drupal\ur_appliance\Test\BaseTest;

class DataSimpleTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(DataSimple::class);
  }

  /**
   * @throws \Exception
   */
  public function testRemovePrependAndSet() {
    /** @var Jobsite $sut */
    $sut = new Jobsite();
    $prepend = 'jobsite';

    $data = [
      'jobsiteId' => '5',
      'jobsiteName' => 'My Test Site',
      'jobsiteAddress1' => '321 Main Work Site Rd',
      'jobsiteCity' => 'Overland Park',
      'jobsiteState' => 'KS',
      'jobsiteZip' => '66213',
      'jobsitePhone' => '913-777-8888',
    ];

    $dataDesired = [
      'id' => '5',
      'name' => 'My Test Site',
      'address1' => '321 Main Work Site Rd',
      'city' => 'Overland Park',
      'state' => 'KS',
      'zip' => '66213',
      'phone' => '913-777-8888',
    ];

    $sut->removePrependAndSet($prepend, $data);
    $finalData = $sut->export();

    foreach ($dataDesired AS $idx => $val) {
      $this->assertArrayHasKey($idx, $finalData);
      $this->assertEquals($val, $finalData[$idx]);
    }
  }
}