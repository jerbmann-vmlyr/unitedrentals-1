<?php

namespace Drupal\ur_appliance\Test\Data\DAL;

use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\Exception\DAL\DalDataException;
use Drupal\ur_appliance\Test\BaseTest;


class OrderTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(Order::class);
  }

  public function testImport() {
    /** @var \Drupal\ur_appliance\Data\DAL\Order $order */
    $order = $this->getSystemUnderTest();
    $data = [
      'branchId' => 'P79',
      'accountId' => 1326960,
      'transType' => 'R',
      'email' => 'ryan.mott@vml.com',
      'jobsiteId' => '7',
      'jobsiteContact' => 'Ryan Mott',
      'jobsitePhone' => '8162182947',
      'startDateTime' => '2017-07-19T14:48:21',
      'returnDateTime' => '2017-08-29T13:17:59',
      'delivery' => true,
      'pickup' => false,
      'rpp' => false, // if not set, default this to "N"
      'items' => [
        "190-2200" => [
          "catClass" => "190-2200",
          "quantity" => "2",
          "rate" => [
            "catClass" => "190-2200",
            "dayRate" => "93",
            "weekRate" => "263",
            "monthRate" => "775",
            "minRate" => "93"
          ],
        ],
      ],
      "cardId" => "231321-jgkljafk-21312",
      'requester' => 'USER10AA@TOTALCONTROL.NET',
      'requesterName' => 'DAVE CASH SMITH',
      'requesterPhone' => '5551112222',
      'approver' => 'USER10AA@TOTALCONTROL.NET',
    ];

    $order->import($data);

    $this->assertNotEmpty($order->getBranchId());
    $this->assertNotEmpty($order->getAccountId());
    $this->assertNotEmpty($order->getItems());
    $this->assertInternalType('array', $order->getItems());
    $this->assertNotEmpty($order->getCreditCard()->getId());
    $this->assertEquals('Y', $order->getDelivery());
    $this->assertEquals('N', $order->getPickup());
    $this->assertEquals('N', $order->isRpp());
    $this->assertEquals('USER10AA@TOTALCONTROL.NET', $order->getReqEmail());
    $this->assertEquals('DAVE CASH SMITH', $order->getRequesterName());
    $this->assertEquals('5551112222', $order->getReqPhone());
    $this->assertEquals('USER10AA@TOTALCONTROL.NET', $order->getApprover());
  }

  /**
   * Ensure that when we call for a postExport of the Order object we are returned
   * an array in a format that we can use to POST to the DAL for a transaction
   */
  public function testPostExport() {
    // @TODO: Add requester, requesterName, requesterPhone
    $requiredFields = [
      'branchId',
      'accountId',
      'transType',
      'email',
      'jobId',
      'poNumber',
      'startDateTime',
      'returnDateTime',
      'delivery',
      'pickup',
      'pickupFirm',
      'rpp',
      'emailPDF',
      'transSource',
      'jobContactName',
      'jobContactPhone',
      'creditCardTokenId',
      'totalDelivery',
      'totalPickup',
      'eqpDetails'
    ];

    $data = [
      'branchId' => 'P79',
      'accountId' => 1326960,
      'transType' => 'R',
      'email' => 'ryan.mott@vml.com',
      'jobsiteId' => '7',
      'jobsiteContact' => 'Ryan Mott',
      'jobsitePhone' => '8162182947',
      'startDateTime' => '2017-07-19T14:48:21',
      'returnDateTime' => '2017-08-29T13:17:59',
      'delivery' => true,
      'pickup' => true,
      'pickupFirm' => false, // if not set, default this to "N"
      'rpp' => false, // if not set, default this to "N"
      'items' => [
        "190-2200" => [
          "catClass" => "190-2200",
          "quantity" => "2",
          "rate" => [
            "catClass" => "190-2200",
            "dayRate" => "93",
            "weekRate" => "263",
            "monthRate" => "775",
            "minRate" => "93"
          ],
        ],
      ],
      "cardId" => "231321-jgkljafk-21312",
      "totals" => [
        "totrentamt" => 9000,
        "totenvamt" => 75,
        "tottaxamt" => 748.69,
        "totgstamt" => 0,
        "totdwamt" => 0,
        "totdelamt" => 0,
        "totpuamt" => 0,
        "miscCharges" => 18.96,
        "authAmount" => 12279.61,
        "authPercent" => 125,
        "rateZone" => "021",
        "tspAgrmt" => "N",
        "tspAgrmtRate" => 0,
      ],
      'requester' => 'USER10AA@TOTALCONTROL.NET',
      'requesterName' => 'DAVE CASH SMITH',
      'requesterPhone' => '5551112222',
      'approver' => ['approverEmail' => 'USER10AA@TOTALCONTROL.NET'],
    ];

    /** @var \Drupal\ur_appliance\Data\DAL\Order $order */
    $order = $this->getSystemUnderTest();

    $order->import($data);
    $body = $order->postExport();

    // Ensure the return is an array
    $this->assertInternalType('array', $body, 'Post Export must be an array');

    // Ensure we have all required fields and they are not null
    foreach ($requiredFields as $field) {
      $this->assertArrayHasKey($field, $body, "$field required");
      $this->assertNotNull($body[$field], "$field cannot be empty");
    }

    // Ensure eqpDetails is an array
    $this->assertInternalType('array', $body['eqpDetails'], 'eqpDetails must be an array');

  }

  public function testBuildAccountingCodes() {
    $data = (object) [
      'acctCode1' => 'Accounting Code 1',
      'acctCode2' => 'Accounting Code 2',
      'acctCode3' => 'Accounting Code 3',
      'acctCode4' => 'Accounting Code 4',
      'acctCode5' => 'Accounting Code 5',
      'acctCode6' => 'Accounting Code 6',
    ];

    /** @var Order $order */
    $order = $this->getSystemUnderTest();
    $order->buildAccountingCodes($data);

    $this->assertEquals('Accounting Code 1', $order->getAccountingCode(1));
    $this->assertEquals('Accounting Code 3', $order->getAccountingCode(3));
    $this->assertEquals('Accounting Code 6', $order->getAccountingCode(6));
  }
}
