<?php
namespace Drupal\ur_appliance\Test\Data\DAL;

use stdClass;
use Drupal\ur_appliance\Data\DAL\Invoice;
use Drupal\ur_appliance\Test\BaseTest;

class InvoiceTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(Invoice::class);
  }

  /**
   * @group invoice
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function testImport() {
    /** @var \Drupal\ur_appliance\Data\DAL\Invoice $invoice */
    $invoice = $this->getSystemUnderTest();
    $data = new stdClass();
    $randIntBounds = random_int(0, 100);

    for ($i = 0; $i < $randIntBounds; $i++) {
      $testInvoice = clone $invoice;

      $data->result = [
        'balance' => random_int(0, 4) ? random_int(1, 10000) : $invoice::EMPTY_VALUE,
        'accountId' => random_int(1, 1000),
        'id' => 10001
      ];

      $testInvoice->import($data->result);

      // Empty asserts
      if ($testInvoice->getBalance() !== 0) {
        $this->assertNotEmpty($testInvoice->getBalance());
      }

      $this->assertNotEmpty($testInvoice->getAccountId());
      $this->assertNotEmpty($testInvoice->getId());

      // Equals asserts
      if ($data->result['balance'] === $invoice::EMPTY_VALUE) {
        //defaults to returning zero in case a value isn't provided
        $this->assertEquals($testInvoice->getBalance(), 0);
      }
      else {
        $this->assertEquals($testInvoice->getBalance(), $data->result['balance']);
      }

      $this->assertEquals($testInvoice->getAccountId(), $data->result['accountId']);
      $this->assertEquals($testInvoice->getId(), $data->result['id']);
    }
  }

  /**
   * @group invoice
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function testOverdue() {
    $totalDue = 0;
    $data = new stdClass();

    /** @var \Drupal\ur_appliance\Data\DAL\Invoice $invoice */
    $invoice = $this->getSystemUnderTest();
    $processed = [];
    $randIntBounds = random_int(0, 100);

    for ($i = 0; $i < $randIntBounds; $i++) {
      $testInvoice = clone $invoice;

      $data->result = [
        'balance' => random_int(0, 4) ? random_int(1, 10000) : $invoice::EMPTY_VALUE,
        'accountId' => random_int(0, 1000),
        'id' => 10001
      ];

      $testInvoice->import($data->result);
      $totalDue += $testInvoice->getBalance();
      $processed[] = $testInvoice;
      $this->assertEquals($testInvoice::totalOverdue($processed), $totalDue);
    }
  }
}