<?php
namespace Drupal\ur_appliance\Test\Data\DAL;

use Drupal\ur_appliance\Data\DAL\Jobsite;
use Drupal\ur_appliance\Test\BaseTest;

class JobsiteTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(Jobsite::class);
  }

  /**
   * @throws \Drupal\ur_appliance\Exception\DataException
   */
  public function testImport() {
    /** @var Jobsite $jobsite */
    $jobsite = $this->getSystemUnderTest();
    $data = ['latitude' => '56.726379', 'longitude' => '-111.380340'];
    $jobsite->import($data);

    $this->assertEquals($data['latitude'], $jobsite->getLatitude());
    $this->assertEquals($data['longitude'], $jobsite->getLongitude());
  }
}