<?php

namespace Drupal\ur_appliance\Test\Provider;

use Drupal\ur_appliance\Provider\DAL;
use Drupal\ur_appliance\Test\BaseTest;
use Drupal\ur_appliance\Data\DAL\Rate;
use Drupal\ur_appliance\Endpoint\DAL\Rental\RatesEndpoint;
use Drupal\ur_appliance\Controller\DAL\Rental\RatesController as RatesController;

/**
 * Class DALTest
 *
 * @package Drupal\ur_appliance\Test\Provider
 */
class DALTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->uri = 'rental/rates';
    $this->setClassUnderTest(DAL::class);
  }

  /**
   * @group live
   */
  public function testDataExport() {
    $data = [
      'catClass' => ['310-6001', '240-3131'],
      'branchId' => 'J90'
    ];

    $result = DAL::Rental(true)->Rates()->retrieve($data);
    $this->assertNotEmpty($result);
  }
}