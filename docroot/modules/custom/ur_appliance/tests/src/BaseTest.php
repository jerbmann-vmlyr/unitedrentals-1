<?php

namespace Drupal\ur_appliance\Test;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\TaggedContainerInterface;
use Symfony\Component\HttpKernel\Tests\Logger;
use Mockery;
use Mockery\MockInterface;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Adapter\Google\GoogleMapsAdapter;
use Drupal\ur_appliance\Adapter\Placeable\PlaceableAdapter;
use Drupal\ur_appliance\Adapter\Placeable\WorkbenchAdapter;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Interfaces\AdapterInterface;
use Drupal\ur_appliance\Interfaces\EndpointInterface;

/**
 * Class BaseTest
 *
 * @package Drupal\ur_appliance\Test
 */
class BaseTest extends TestCase {
  protected $classUnderTest;
  protected $systemUnderTest;
  protected $defaultAdapter = DalAdapter::class;
  protected $defaultLiveAdapter = 'dal_adapter';

  protected $defaultAccountId = "2707"; // A real test Id.
  protected $defaultToken = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
  protected $defaultUserEmail = 'ur.vml.7@gmail.com';
  protected $defaultRequestId = 'A000913370';

  protected $baseUrl = 'https://testmobiledal.ur.com';
  protected $uri = '';

  protected $method = 'GET';
  protected $allowedMethods = ['GET', 'POST', 'PUT'];

  protected $params = [];
  protected $rawResponseToMock;

  // Saved mocks
  protected $mockDalAdapter;
  protected $mockLogger;
  protected $mockStream;
  protected $mockResponse;
  protected $mockClient;
  protected $mockContainer;

  public function setUp() {
    parent::setUp();
  }

  /**
   * @return string
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * @param string $uri
   */
  public function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * @return string
   */
  public function getMethod() {
    return $this->method;
  }

  /**
   * @param string $method
   */
  public function setMethod($method) {
    $this->method = $method;
  }

  /**
   * @return array
   */
  public function getAllowedMethods() {
    return $this->allowedMethods;
  }

  /**
   * @param array $allowedMethods
   */
  public function setAllowedMethods($allowedMethods) {
    $this->allowedMethods = $allowedMethods;
  }

  public function addAllowedMethod($method) {
    $this->allowedMethods[] = $method;
  }

  /**
   * @return array
   */
  public function getParams() {
    return $this->params;
  }

  /**
   * @param array $params
   */
  public function setParams($params) {
    $this->params = $params;
  }

  /**
   * @return string
   */
  public function getRawResponseToMock() {
    return $this->rawResponseToMock;
  }

  /**
   * @param string $rawResponseToMock
   */
  public function setRawResponseToMock($rawResponseToMock) {
    $this->rawResponseToMock = $rawResponseToMock;
  }

  /**
   * PHP7' json_decode returns an error for a NULL or empty JSON input.
   *
   * This simply creates a default mock JSON object to avoid that.
   *
   * @return mixed
   */
  public function defaultJsonExample() {
    $this->rawResponseToMock = '{"Mock JSON Key": "Mock JSON Value"}';
    return json_decode($this->rawResponseToMock);
  }

  /**
   * @return mixed
   */
  public function getClassUnderTest() {
    return $this->classUnderTest;
  }

  /**
   * @param mixed $classUnderTest
   */
  public function setClassUnderTest($classUnderTest) {
    $this->classUnderTest = $classUnderTest;
  }

  public function setDefaultAdapter($adapterClass) {
    $this->defaultAdapter = $adapterClass;
  }

  /**
   * @return string
   */
  public function getDefaultLiveAdapter() {
    return $this->defaultLiveAdapter;
  }

  /**
   * @param string $defaultLiveAdapter
   */
  public function setDefaultLiveAdapter($defaultLiveAdapter) {
    $this->defaultLiveAdapter = $defaultLiveAdapter;
  }

  public function getSystemUnderTest($response = null, $live = false, $reset = false) {
    if (empty($this->systemUnderTest) || !empty($response) || $reset) {
      $class = $this->getClassUnderTest();

      $classImplements = class_implements($class);
      $isAdapter = \in_array(AdapterInterface::class, $classImplements, false);
      $isEndpoint = \in_array(EndpointInterface::class, $classImplements, false);
      $adapter = null;

      if ($isAdapter || $isEndpoint) {
        if ($live) {
          $adapter = ApiContainer::getService( $this->getDefaultLiveAdapter() );
        }
        else {
          $logger = $this->getMockLogger();
          $client = $this->getMockClient($response);

          if ($isAdapter) {
            /** @var \Drupal\ur_appliance\Adapter\BaseAdapter $adapter */
            $adapter = new $class($logger, $client);
          }
          else {
            // Instantiate the default adapter class.
            /** @var \Drupal\ur_appliance\Adapter\BaseAdapter $adapter */
            $adapter = new $this->defaultAdapter($logger, $client);
          }

          $adapter->setResponse( $this->getMockResponse($response) );
        }

        $adapter->setUri($this->uri);
        $adapter->setMethod($this->method);
        $adapter->setBaseUrl($this->baseUrl);
      }

      if ($isEndpoint) {
        $this->systemUnderTest = new $class($adapter);
      }
      else if ($isAdapter) {
        $this->systemUnderTest = $adapter;
      }
      else {
        $this->systemUnderTest = new $class();
      }
    }

    return $this->systemUnderTest;
  }

  /**
   * @param array $shouldReceive
   * @return \Mockery\MockInterface|DalAdapter
   */
  protected function getMockDalAdapter(array $shouldReceive = []) {
    $this->mockDalAdapter = Mockery::mock(DalAdapter::class);

    $defaultShouldReceive = [
      'setUri' => null,
      'setMethod' => null,
      'setBaseUrl' => null,
      'shouldForceWORSToken' => null,
      'setParams' => null,
    ];

    $shouldReceive += $defaultShouldReceive;
    $this->mockDalAdapter->shouldReceive($shouldReceive);

    return $this->mockDalAdapter;
  }

  protected function getMockLogger() {
    $this->mockLogger = Mockery::mock(Logger::class);
    $this->mockLogger->shouldReceive([
      'error' => NULL
    ]);

    return $this->mockLogger;
  }

  /**
   * @param null $response
   * @return \Mockery\MockInterface
   */
  protected function getMockStream($response = null) {
    if ($response === null) {
      $response = $this->rawResponseToMock;
    }

    $this->mockStream = Mockery::mock(Stream::class);
    $this->mockStream->shouldReceive([
      'getContents' => $response
    ]);

    return $this->mockStream;
  }

  /**
   * @param $response
   * @param int $statusCode
   * @return array
   */
  protected function buildResponsePacket($response = null, $statusCode = 200) {
    return [
      'getBody' => $this->getMockStream($response),
      'getStatusCode' => $statusCode
    ];
  }

  /**
   * @param null|mixed $response
   * @return \Mockery\MockInterface
   */
  protected function getMockResponse($response = null) {
    $responsePacket = $this->buildResponsePacket($response);

    $this->mockResponse = Mockery::mock(Response::class);
    $this->mockResponse->shouldReceive($responsePacket);

    return $this->mockResponse;
  }

  /**
   * @param null $response
   * @return \Mockery\MockInterface
   */
  protected function getMockClient($response = null) {
    $this->mockClient = Mockery::mock(Client::class);
    $this->mockClient->shouldReceive([
      'request' => $this->getMockResponse($response)
    ]);

    return $this->mockClient;
  }

  protected function getMockContainer() {
    $this->mockContainer = Mockery::mock(TaggedContainerInterface::class);

    return $this->mockContainer;
  }

  public function testGetMocks() {
    $this->assertInstanceOf('Mockery\MockInterface', $this->getMockContainer());
  }
}