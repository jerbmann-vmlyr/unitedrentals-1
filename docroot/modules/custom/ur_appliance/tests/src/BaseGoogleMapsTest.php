<?php

namespace Drupal\ur_appliance\Test;

use Drupal\ur_appliance\Adapter\Google\GoogleMapsAdapter;

/**
 * Class BaseGoogleMapsTest
 *
 * @package Drupal\ur_appliance\Test
 * @TODO: Build out Google Maps tests
 */
class BaseGoogleMapsTest extends BaseTest {
  protected $defaultAdapter = GoogleMapsAdapter::class;
  protected $defaultLiveAdapter = 'google_adapter';

  protected $baseUrl = 'https://maps.googleapis.com/maps/api/place';
}