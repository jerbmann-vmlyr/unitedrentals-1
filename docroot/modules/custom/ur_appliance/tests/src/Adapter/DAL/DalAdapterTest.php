<?php

namespace Drupal\ur_appliance\Test\Adapter;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Test\BaseTest;

class DalAdapterTest extends BaseTest {
  protected $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"id":"ur.vml.5@gmail.com"},"result":{"token":"fa1992a03b2fb71464e0ae03bbd00266764a050d"},"cached":false}';

  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(DalAdapter::class);
    $this->getSystemUnderTest($this->rawResponseToMock);
  }

  public function testShouldLog() {
    /** @var DalAdapter $dalAdapter */
    $dalAdapter = $this->getSystemUnderTest();
    $dalAdapter->setUri('customer/creditcards');

    $response = $dalAdapter->shouldSkipLogging();
    $this->assertTrue($response);

    $dalAdapter->setUri('equipment/availability');
    $response = $dalAdapter->shouldSkipLogging();
    $this->assertFalse($response);
  }

  public function testSetUserToken() {
    $dalAdapter = $this->getSystemUnderTest();
    $fakeToken = 'test09234jaf3iaja093ujt0';

    $dalAdapter->setUserToken($fakeToken);
    $this->assertEquals($fakeToken, $_SESSION['ur_dal_user_token']);
  }

  /**
   * @group live
   */
  public function testGetUserToken() {
    /** @var DalAdapter $dalAdapter */
    $dalAdapter = $this->getSystemUnderTest(null, true);
    $dalAdapter->setUserEmail('ur.vml.5@gmail.com');

    $this->assertNotEmpty($dalAdapter->getUserToken());
  }

  public function testGetAuth() {
    /** @var DalAdapter $dalAdapter */
    $dalAdapter = $this->getSystemUnderTest();
    $dalAdapter->shouldForceWORSToken();

    $response = $dalAdapter->getAuth();
    $testToken = 'WORS:nLgaSqdiCt6H2aVYSnww8mIH';

    $this->assertEquals($testToken, $response);
  }

  /**
   * Test the grabbing of user tokens
   * @group live
   *
   * @throws \Exception
   * @throws \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function testGetUserAuth() {
    /** @var DalAdapter $dalAdapter */
    $dalAdapter = $this->getSystemUnderTest(null, true);
    $dalAdapter->setUserEmail('ur.vml.5@gmail.com');
    $dalAdapter->shouldForceUserToken();

    $response = $dalAdapter->getAuth();
    $this->assertContains('TOKEN:', $response);
  }

  public function testShouldUseWORSToken() {
    $dalAdapter = $this->getSystemUnderTest();

    $this->assertFalse($dalAdapter->shouldUseWORSToken());

    $dalAdapter->shouldForceWORSToken();
    $this->assertTrue($dalAdapter->shouldUseWORSToken());

    $dalAdapter->shouldNotForceWORSToken();
    $this->assertFalse($dalAdapter->shouldUseWORSToken());
  }

  public function testShouldHaveGuid() {
    $dalAdapter = $this->getSystemUnderTest();

    $this->assertFalse($dalAdapter->shouldHaveGuid());

    $dalAdapter->requireGuid();
    $this->assertTrue($dalAdapter->shouldHaveGuid());

    $dalAdapter->doNotRequireGuid();
    $this->assertFalse($dalAdapter->shouldHaveGuid());
  }

  public function testGetGuid() {
    $dalAdapter = $this->getSystemUnderTest();
    $guid = 'test1231098219038';
    $_SESSION['ur_guid'] = $guid;

    $this->assertEquals($guid, $dalAdapter->getGuid());

    // @TODO: Test extended functionality once available.
  }

  public function testGetBaseUrl() {
    $dalAdapter = $this->getSystemUnderTest();
    $this->assertEquals($this->baseUrl, $dalAdapter->getBaseUrl());
  }

  public function testShouldLogRaw() {
    $dalAdapter = $this->getSystemUnderTest();
    $dalAdapter->shouldLogRaw();

    $this->assertTrue($dalAdapter->shouldLogRaw());
  }

  public function testGetStatus() {
    $dalAdapter = $this->getSystemUnderTest();
    $dalAdapter->setResponse( $this->getMockResponse() );
    $dalAdapter->parseResponse();

    $status = $dalAdapter->getStatus();
    $this->assertEquals('0', $status);
  }

  public function testMessageText() {
    $dalAdapter = $this->getSystemUnderTest();
    $dalAdapter->setResponse($this->getMockResponse());
    $dalAdapter->parseResponse();

    $this->assertNull($dalAdapter->getMessageText());
  }

  /**
   * Verify that calling this method will return the same instance of the class.
   *
   * We do that by assigning a new value to the first object instance and
   * checking the second to see if it appears there.
   *
   * @throws \Exception
   */
  public function testGetSeparateDalAdapterInstance() {
    /** @var DalAdapter $dalAdapter */
    $dalAdapter = $this->getSystemUnderTest();
    $adapter1 = $dalAdapter->getSeparateDalAdapterInstance();

    $testVal = 'Adapter1';
    $adapter1->testVal = $testVal;

    $adapter2 = $dalAdapter->getSeparateDalAdapterInstance();
    $this->assertEquals($testVal, $adapter2->testVal);

    // Make sure the original instance does not have the property
    if (isset($dalAdapter->testVal)) {
      $this->assertNotEquals($testVal, $dalAdapter->testVal);
    }
  }

  /**
   * Verify that calling this method will return the same instance of the class.
   *
   * We do that by assigning a new value to the first object instance and
   * checking the second to see if it appears there.
   *
   * @throws \Exception
   */
  public function testGetSsoController() {
    /** @var DalAdapter $dalAdapter */
    $dalAdapter = $this->getSystemUnderTest();
    $ssoController1 = $dalAdapter->getSsoController();

    $testVal = 'Controller1';
    $ssoController1->testVal = $testVal;

    $ssoController2 = $dalAdapter->getSsoController();
    $this->assertEquals($testVal, $ssoController2->testVal);
  }
}
