<?php

namespace Drupal\ur_appliance\Test\Adapter;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\BaseAdapter;
use Mockery;
use Drupal\ur_appliance\Exception\DAL\DalException;
use Drupal\ur_appliance\Test\BaseTest;

class BaseAdapterTest extends BaseTest {
  protected $params = ['nerds' => 'are cool', 'non-nerds' => 'Drool'];
  protected $uri = 'customer/accounts';

  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(BaseAdapter::class);
    $this->getSystemUnderTest($this->rawResponseToMock);
  }

  public function testGetAuth() {
    $adapter = $this->getSystemUnderTest();
    $this->assertNull($adapter->getAuth());
  }

  public function testEnableErrorNotifications() {
    $adapter = $this->getSystemUnderTest();
    $adapter->enableErrorNotifications();

    $this->assertTrue($adapter->areErrorNotificationsEnabled());
  }

  public function testDisableErrorNotifications() {
    $adapter = $this->getSystemUnderTest();
    $adapter->disableErrorNotifications();

    $this->assertFalse($adapter->areErrorNotificationsEnabled());
  }

  public function testSetLogSuccessTrue() {
    $adapter = $this->getSystemUnderTest();
    $adapter->setLogSuccess(true);

    $this->assertTrue($adapter->getLogSuccess());
  }

  public function testSetLogSuccessFalse() {
    $adapter = $this->getSystemUnderTest();
    $adapter->setLogSuccess(false);

    $this->assertFalse($adapter->getLogSuccess());
  }

  public function testSetLogSuccessNull() {
    $adapter = $this->getSystemUnderTest();
    $adapter->setLogSuccess(null);

    // Should return the default of false
    $this->assertFalse($adapter->getLogSuccess());
  }

  public function testSetDefaultSuccessStatus() {
    $adapter = $this->getSystemUnderTest();
    $setting = '3';
    $adapter->setDefaultSuccessStatus($setting);

    $this->assertEquals($setting, $adapter->getDefaultSuccessStatus());
  }

  public function testRequestBody() {
    $adapter = $this->getSystemUnderTest();
    $requestBody = 'Some kind of test string';

    $adapter->setRequestBody($requestBody);
    $this->assertEquals($requestBody, $adapter->getRequestBody());
  }

  public function testContentType() {
    /**
     * @TODO: Build more of these tests of content type once we get checking
     *        added to it.
     */
    $adapter = $this->getSystemUnderTest();
    $contentType = 'application/json';

    $adapter->setContentType($contentType);
    $this->assertEquals($contentType, $adapter->getContentType());
  }

  public function testHeaders() {
    $adapter = $this->getSystemUnderTest();
    $this->assertEquals([], $adapter->getHeaders());

    $key = "Authorization";
    $value = 'WORS:23904u2ljf093ujo9u903u0o9eu2r3';

    $header = [
      $key => $value,
    ];

    $adapter->addHeader($key, $value);
    $this->assertEquals($header, $adapter->getHeaders());
  }

  public function testGetBaseUrl() {
    $this->assertEquals(
      $this->baseUrl,
      $this->getSystemUnderTest()->getBaseUrl()
    );
  }

  public function testSetBaseUrl() {
    $adapter = $this->getSystemUnderTest();
    $baseUrl = 'http://www.Drupal\ur_appliance.com';

    $adapter->setBaseUrl($baseUrl);
    $this->assertEquals($baseUrl, $adapter->getBaseUrl());
    $adapter->setBaseUrl($this->baseUrl); // Reset value
  }

  public function testGetUri() {
    $this->assertEquals(
      $this->uri,
      $this->getSystemUnderTest()->getUri()
    );
  }

  public function testSetUri() {
    $adapter = $this->getSystemUnderTest();

    $uri = 'rental/transactions';
    $adapter->setUri($uri);
    $this->assertEquals($uri, $adapter->getUri());

    $adapter->setUri(null);
    $this->assertNotNull($adapter->getUri());

    $adapter->setUri($this->uri); // Reset value
  }

  public function testGetMethod() {
    $this->assertEquals(
      $this->method,
      $this->getSystemUnderTest()->getMethod()
    );
  }

  public function testSetMethod() {
    $adapter = $this->getSystemUnderTest();
    $adapter->setMethod('post');
    $this->assertEquals('POST', $adapter->getMethod());

    $adapter->setMethod('GeT');
    $this->assertEquals('GET', $adapter->getMethod());
  }

  public function testSetMethodException() {
    $adapter = $this->getSystemUnderTest();
    $this->setExpectedException(DalException::class);
    $adapter->setMethod('Some invalid MethOD Stringggg');
  }

  public function testResetMethod() {
    $adapter = $this->getSystemUnderTest();

    $method = 'PUT';
    $adapter->setMethod($method);
    $this->assertEquals($method, $adapter->getMethod());

    $adapter->resetMethod();
    $this->assertEquals('GET', $adapter->getMethod());
  }

  public function testGetParams() {
    $this->assertNull( $this->getSystemUnderTest()->getParams() );
  }

  public function testSetParams() {
    $adapter = $this->getSystemUnderTest();
    $adapter->setParams($this->params);

    $this->assertArraySubset($this->params, $adapter->getParams());
    $this->assertArrayHasKey('nerds', $adapter->getParams());
  }

  public function testAddParameter() {
    $adapter = $this->getSystemUnderTest();

    $adapter->setParams([]);
    $this->assertEmpty($adapter->getParams());

    $key = 'Aerial Work Platforms';
    $value = 'Boomlift';
    $adapter->addParameter($key, $value);
    $this->assertArrayHasKey($key, $adapter->getParams());

    $sampleParams = [$key => $value];
    $this->assertArraySubset($sampleParams, $adapter->getParams());
  }

  public function testGetFlattenedParams() {
    $adapter = $this->getSystemUnderTest();
    $expected = [];

    // First an empty array
    $this->assertEquals($expected, $adapter->getFlattenedParams());

    // Then a built up array
    $params = $this->params;
    $newExpectation = $this->params;
    $params['subArray'] = ['Mighty Mouse' => 'Saves the day', 'hero' => true];
    $newExpectation['subArray[Mighty Mouse]'] = 'Saves the day';
    $newExpectation['subArray[hero]'] = true;
    $adapter->setParams($params);

    $this->assertEquals($newExpectation, $adapter->getFlattenedParams());
  }

  public function testFlattenParams() {
    $adapter = $this->getSystemUnderTest();
    $this->assertEquals([], $adapter->flattenParams(null));

    $example1 = ['What in' => 'the Same hill'];
    $this->assertEquals($example1, $adapter->flattenParams($example1));

    $params = $this->params;
    $example2 = $this->params;
    $params['subArray'] = ['Mighty Mouse' => 'Saves the day', 'hero' => true];
    $example2['subArray[Mighty Mouse]'] = 'Saves the day';
    $example2['subArray[hero]'] = true;

    $this->assertEquals($example2, $adapter->flattenParams($params));
  }

  public function testGetQueryStrings() {
    $adapter = $this->getSystemUnderTest();
    $adapter->resetMethod();

    $adapter->setParams($this->params);
    $example1 = 'nerds=are+cool&non-nerds=Drool';

    $this->assertEquals($example1, $adapter->getQueryStrings());

    $params = $this->params;
    $params['subArray'] = ['Mighty Mouse' => 'Saves the day', 'hero' => true];
    $example2 = 'nerds=are+cool&non-nerds=Drool&subArray%5BMighty+Mouse%5D=Saves+the+day&subArray%5Bhero%5D=1';
    $adapter->setParams($params);

    $this->assertEquals($example2, $adapter->getQueryStrings());
  }

  public function testBuildRequestOptions() {
    $adapter = $this->getSystemUnderTest();
    $headers = ["Content-Type" =>  "application/json"];

    $example1 = [
      'headers' => $headers,
      'body' => ''
    ];

    $this->assertEquals($example1, $adapter->buildRequestOptions());
  }

  public function testBuildUrl() {
    $adapter = $this->getSystemUnderTest();
    $expectedUrl = 'https://testmobiledal.ur.com/customer/accounts?';

    $this->assertEquals($expectedUrl, $adapter->buildUrl());

    $key = 'testKey';
    $value = 'TestValue';
    $adapter->addParameter($key, $value);
    $expectedUrl .= "$key=$value";

    $this->assertEquals($expectedUrl, $adapter->buildUrl());
  }

  public function testExecuteSuccess() {
    $responseJsonExample = $this->defaultJsonExample();
    $adapter = $this->getSystemUnderTest($this->rawResponseToMock);
    $response = $adapter->execute();

    $this->assertEquals($responseJsonExample, $response);
  }

  public function testParseResponse() {
    $responseJsonExample = $this->defaultJsonExample();
    $adapter = $this->getSystemUnderTest();
    $adapter->setResponse($this->getMockResponse());

    $this->assertEquals($responseJsonExample, $adapter->parseResponse());
  }

  public function testShouldLogRaw() {
    $adapter = $this->getSystemUnderTest();
    $this->assertFalse($adapter->shouldLogRaw());
  }

  public function testSetShouldLogRawData() {
    $adapter = $this->getSystemUnderTest();
    $this->assertFalse($adapter->shouldLogRaw());

    $adapter->setShouldLogRawData(true);
    $this->assertTrue($adapter->shouldLogRaw());

    $adapter->setShouldLogRawData(false);
    $this->assertFalse($adapter->shouldLogRaw());

    $adapter->setShouldLogRawData('TruE');
    $this->assertFalse($adapter->shouldLogRaw());
  }

  public function testGetRawLog() {
    $adapter = $this->getSystemUnderTest();
    $adapter->setResponse($this->getMockResponse());
    $example = ['raw' => $this->rawResponseToMock, 'url' => $this->baseUrl];

    // Verify default
    $this->assertEmpty($adapter->getRawLog());

    // Verify settings
    $this->assertEquals($this->baseUrl, $adapter->getBaseUrl());

    // Verify full output
    $adapter->setShouldLogRawData(true);
    $rawLog = $adapter->getRawLog();

    $this->assertEquals($example['raw'], $rawLog['raw']);
    $this->assertStringStartsWith($example['url'], $rawLog['url']);
  }

  public function testLogResponse() {
    //@TODO: Build this test. Note: not sure how to test this.
  }

  public function testHasIgnorableError() {
    $this->assertFalse($this->getSystemUnderTest()->hasIgnorableError());
  }

  public function testHasStatusError() {
    $this->assertTrue($this->getSystemUnderTest()->hasStatusError());
  }

  public function testWasSuccessful() {
    $this->assertFalse($this->getSystemUnderTest()->wasSuccessful());
  }

  public function testGetSetResponse() {
    $adapter = $this->getSystemUnderTest();
    $response = $this->getMockResponse();

    $adapter->setResponse($response);
    $this->assertEquals($response, $adapter->getResponse());
  }

  public function testGetParsedResponse() {
    $adapter = $this->getSystemUnderTest();
    $adapter->setResponse($this->getMockResponse());
    $testParsedResponse = json_decode($this->rawResponseToMock);

    $this->assertEquals($testParsedResponse, $adapter->getParsedResponse());
  }

  public function testGetResponseHttpCode() {
    $adapter = $this->getSystemUnderTest();
    $adapter->setResponse($this->getMockResponse());
    $this->assertEquals(200, $adapter->getResponseHttpCode());
  }

  public function testGetStatus() {
    $this->assertNull($this->getSystemUnderTest()->getStatus());
  }

  public function testGetMessageText() {
    $this->assertNull($this->getSystemUnderTest()->getMessageText());
  }

  public function testSetGetExecuteError() {
    $adapter = $this->getSystemUnderTest();
    $testStr = 'server_error';

    $adapter->setExecuteError($testStr);
    $this->assertEquals($testStr, $adapter->getExecuteError());
  }

  /**
   * Test our ability to load the AEMP Telematics XML response.
   */
  public function testAempXmlLoad() {
    // Hopefully won't fail to load from the global PHPUnit call.
    $file = getcwd() . '/../../data/Adapter/aemp.xml';

    if (!file_exists($file)) {
      // Attempt to load from the root directory
      $file = getcwd() . '/../../../../data/Adapter/aemp.xml';
    }

    if (file_exists($file)) {
      $contents = file_get_contents($file);
      $adapter = $this->getSystemUnderTest();
      $response = $adapter->parseXML($contents);

      $this->assertNotEmpty($response);
      $this->assertObjectHasAttribute('Equipment', $response);
    }
  }
}
