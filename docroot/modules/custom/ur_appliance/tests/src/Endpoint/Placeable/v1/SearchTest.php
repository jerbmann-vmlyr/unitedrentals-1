<?php

namespace Drupal\ur_appliance\Test\Endpoint\Placeable\v1;

use Drupal\ur_appliance\Adapter\Placeable\PlaceableAdapter;
use Drupal\ur_appliance\Endpoint\Placeable\v1\SearchEndpoint;
use Drupal\ur_appliance\Data\Placeable\Location;
use Drupal\ur_appliance\Test\BasePlaceableTest;

class SearchTest extends BasePlaceableTest {
  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(SearchEndpoint::class);
    $this->setDefaultAdapter(PlaceableAdapter::class);
  }

  /**
   * @group live
   */
  public function testRead() {
    $keywords = 'Kansas City';
    $limit = 1;

    /** @var SearchEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest(null, true);

    // @TODO: Mock the response instead.
    $locations = $endpoint->read($keywords, null, $limit);
    $this->assertInstanceOf(\stdClass::class, $locations);

    // @TODO: Once clean up the search response we should re-implement this test.
    foreach ($locations as $location) {
      $this->assertInstanceOf(Location::class, $location);
    }

    $this->assertCount(1, $locations);
  }
}