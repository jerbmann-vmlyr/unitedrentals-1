<?php

namespace Drupal\ur_appliance\Test\Endpoint\Placeable\v1;

use Drupal\ur_appliance\Adapter\Placeable\PlaceableAdapter;
use Drupal\ur_appliance\Adapter\Placeable\WorkbenchAdapter;
use Drupal\ur_appliance\Data\Placeable\Location;
use Drupal\ur_appliance\Endpoint\Placeable\v2\CollectionEndpoint;
use Drupal\ur_appliance\Test\BaseWorkbenchTest;

class CollectionTest extends BaseWorkbenchTest {
  protected $rawResponseToMock = '{"results":[{"id":"57470de10b0000035a121593","refId":"57470c7a0b00002900121396","fields":{"Google Hours":"2:7:00:AM:5:00:PM,3:7:00:AM:5:00:PM,4:7:00:AM:5:00:PM,5:7:00:AM:5:00:PM,6:7:00:AM:5:00:PM","legacyUrl":"1110-quebec-street-north-kansas-city-mo-64116","city":"Kansas City","Primary Category":"Equipment Rental Agency","name":"UNITED RENTALS","refinement_bus_type":"General Equipment and Tools","business_name":"United Rentals","Data Mgmt Tracking":"","Branch_ID_PS":"","latitude":"39.1289893","Contact_Form_URLS":"http://www.Drupal\ur_appliance.com/en/customer-care/contact-us?subject=generalquestions","zip":"64116","Details_Page_Title":"General Equipment & Tool Rentals | Kansas City, MO | United Rentals, Inc.","state":"MO","alert_message":"","url":"mo/north-kansas-city/64116/1110-quebec-street","Facebook ID":"639","National_Campaign_Ad_Image":"https://www.Drupal\ur_appliance.com/sites/default/files/desktop_940x120_scissor_online.jpg","Syndication Description":"Visit the world\'s largest equipment & tool rental company, United Rentals, to rent or buy aerial, earthmoving, material handling, power & HVAC, trench safety equipment, industrial tools and more.","Order_Online_URLs":"https://www.Drupal\ur_appliance.com/en/equipment","Region":"South Central","businessType":"GR","sundayhours":"CLOSED","address2":"","managerName":"David Polletta","Info_Message":"","groupId":"793","longitude":"-94.56277759999999","branchId":"639","webDisplay":"1","NES Location":"","langCode":"ENU","Branch_Description":"With the largest rental fleet in the industry, we are your single source to rent or buy heavy equipment. Count on our top-tier equipment & tools, team of industry experts, and 24/7 customer service to keep your operations running smoothly, day or night.","id":"793","saturdayhours":"CLOSED","National_Campaign_Ad_URL":"https://www.Drupal\ur_appliance.com/en/equipment","fax":"816-474-5266","National_Campaign_Mobile_Ad_Image":"https://www.Drupal\ur_appliance.com/sites/default/files/mobile_200x400_scissor_online.jpg","Region #":"240","countryCode":"US","address1":"1110 Quebec St","status":"A","Data Mgmt Notes":"","weekdayhours":"7:00AM-5:00PM","businessTypes":"GR,SM","Subheading":"This location offers general equipment & tool rentals.","Details_Page_H1":"Kansas City, MO Heavy Equipment, Aerial, Earthmoving, Material Handling Equipment & Tool Rentals","currency":"USD","Business_Type_Products":"services.ariel-platforms, services.air-tools, services.compaction, services.concrete, services.earthmoving, services.forklifts, services.light-towers, services.plumbing-pipes, services.power-tools, services.pressure-washers, services.surface-preparation, services.vehicles-control, services.welders","National_Campaign_Ad_Alt_Text":"Online Rental Now Available","Branch_ID_TR":"","formerRSC":"0","Local Page":"https://locations.Drupal\ur_appliance.com/mo/kansas-city/639","sort":"1","Branch_ID_GR":"","timeOffset":"-100","Image":"https://www.Drupal\ur_appliance.com/sites/default/files/Drupal\ur_appliance-logo%20%281%29.jpg","FB Page Id":"1002602443100928","Pilot Market":"Yes","Branch_ID_AR":"","Syndication Duplicate":"","Branch_ID_PR":"","Brands Carried":"Genie, JCB, JLG, Skyjack, Ingersoll Rand, Gradall, SkyTrak, Terex, Doosan, Amida, Multiquip, Magnum,  Wacker Neuson, Gehl, Atlas Copco, Sullair, Komatsu, John Deere, Bobcat, Takeuchi, Kobelco, Toyota,  Bomag, Broce, Broderson, Case Construction, Club Car, Diamond Products, Hamm, Honda, Husqvarna, Lay-Mor, Lincoln Electric, Miller Electric, Mi-T-M, STIHL, Hytorc, Hilti, Vermeer, Honda, DitchWitch, Milwaukee, Barreto, Toro, Bosch, Rice Hydro, Norton, Tennant, Clarke, Gar-Bro","Google Categories":"Construction Equipment Supplier, Material Handling Equipment Supplier, Tool Rental Service","phone":"816-474-5262","Video":"77247947","Status":"Open"},"errors":[],"suggestions":[{"source":"usps","ruleName":"AddressSuggestions","fields":{},"suggestionWasFound":true}],"conflicts":[],"created":"2017-05-04T15:11:20+0000","lastUpdated":"2017-05-04T15:11:21+0000","verified":true,"pins":{"factual":{"lat":39.128869,"long":-94.562736,"street":"1110 Quebec St","city":"Kansas City","state":"MO","postal":"64116","phone":"(816) 474-5262","name":"United Rentals","source":"factual","rating":0,"distance":14,"responseCode":200,"url":"https://factual.com/10d72be8-4d70-490f-b19a-7c91322f2007","stats":{"website":"http://www.Drupal\ur_appliance.com/"},"website":"http://www.Drupal\ur_appliance.com/","differences":["website"],"status":"differ","isPermanentlyClosed":false,"reviews":[],"categories":{"categories":[],"prettyCategories":["Retail","Construction Supplies"]},"lastThirdPartyResponse":200,"hours":"2:7:00:AM:5:00:PM,3:7:00:AM:5:00:PM,4:7:00:AM:5:00:PM,5:7:00:AM:5:00:PM,6:7:00:AM:5:00:PM"},"googlemybusiness":{"lat":0,"long":0,"street":"1110 Quebec St","city":"Kansas City","state":"MO","postal":"64116","phone":"(816) 474-5262","name":"United Rentals","source":"googlemybusiness","rating":0,"distance":10400949,"responseCode":200,"url":"https://business.google.com/manage/#/acc/117331368449244226262/edit/l/7980304568877148096","stats":{"accountId":"117331368449244226262","accountName":"accounts/117331368449244226262/locations/7980304568877148096","isVerified":"true","locationId":"7980304568877148096","placeId":"ChIJVQeqDaDwwIcRMbadK9NF3n8","plusPageId":"116687260912743887739","storeCode":"639","website":"https://locations.Drupal\ur_appliance.com/mo/kansas-city/639?utm_source=google&utm_medium=distrib&utm_campaign=google-distrib"},"website":"https://locations.Drupal\ur_appliance.com/mo/kansas-city/639?utm_source=google&utm_medium=distrib&utm_campaign=google-distrib","differences":[],"status":"match","isPermanentlyClosed":false,"reviews":[],"categories":{"categories":[],"prettyCategories":["Construction Equipment Supplier","Equipment Rental Agency","Material Handling Equipment Supplier","Tool Rental Service"]},"lastThirdPartyResponse":200,"hours":"2:07:00:AM:5:00:PM,3:07:00:AM:5:00:PM,4:07:00:AM:5:00:PM,5:07:00:AM:5:00:PM,6:07:00:AM:5:00:PM"},"google":{"lat":39.1289893,"long":-94.56277759999999,"street":"1110 Quebec St","city":"KCMO","state":"MO","postal":"64116","phone":"(816) 474-5262","name":"United Rentals","source":"google","rating":5,"distance":0,"responseCode":200,"url":"https://maps.google.com/?cid=9213878660918785585","stats":{"reviewCount":"0","website":"https://locations.Drupal\ur_appliance.com/mo/kansas-city/639?utm_source=google&utm_medium=distrib&utm_campaign=google-distrib"},"website":"https://locations.Drupal\ur_appliance.com/mo/kansas-city/639?utm_source=google&utm_medium=distrib&utm_campaign=google-distrib","differences":[],"status":"match","isPermanentlyClosed":false,"reviews":[],"categories":{"categories":[],"prettyCategories":["point_of_interest","establishment"]},"lastThirdPartyResponse":200,"hours":"2:07:00:AM:5:00:PM,3:07:00:AM:5:00:PM,4:07:00:AM:5:00:PM,5:07:00:AM:5:00:PM,6:07:00:AM:5:00:PM"},"yellowpages":{"lat":39.127495,"long":-94.56252,"street":"1110 Quebec St","city":"Kansas City","state":"MO","postal":"64116","phone":"(816) 474-5262","name":"United Rentals","source":"yellowpages","rating":0,"distance":168,"responseCode":200,"url":"http://c.ypcdn.com/2/c/rtd?vrid=4bs018y52c&rid=e8293983-5a3d-45d8-91c3-a254094d2d2c&ptid=4bs018y52c&ypid=10974585&lid=10974585&tl=7&lsrc=MDM&dest=http%3A%2F%2Fwww.yellowpages.com%2Fkansas-city-mo%2Fmip%2Funited-rentals-10974585%3Ffrom%3Dpubapi_4bs018y52c","stats":{"claimed":"false","claimedStatus":"false","ratingCount":"0","website":""},"website":"","differences":[],"status":"match","isPermanentlyClosed":false,"reviews":[],"categories":{"categories":[],"prettyCategories":["Rental Service Stores & Yards","Contractors Equipment Rental","Construction & Building Equipment","Tool Rental"]},"lastThirdPartyResponse":200,"hours":"Mon - Fri 7:00 am - 5:00 pm, Sat - Sun Closed"},"foursquare":{"lat":39.12929811318224,"long":-94.56307980410243,"street":"1110 Quebec St","city":"Kansas City","state":"MO","postal":"64116","phone":"(816) 474-5262","name":"United Rentals","source":"foursquare","rating":0,"distance":43,"responseCode":200,"url":"https://foursquare.com/v/4e563d5a8130f6bba03c1686","stats":{"checkinCount":"194","tipCount":"0","usersCount":"6","verified":"true","visitsCount":"195","website":"https://locations.Drupal\ur_appliance.com/mo/kansas-city/639"},"website":"https://locations.Drupal\ur_appliance.com/mo/kansas-city/639","differences":[],"status":"match","isPermanentlyClosed":false,"reviews":[],"categories":{"categories":[],"prettyCategories":["Construction & Landscaping","Rental Service"]},"lastThirdPartyResponse":200,"hours":"2:7:00:AM:5:00:PM,3:7:00:AM:5:00:PM,4:7:00:AM:5:00:PM,5:7:00:AM:5:00:PM,6:7:00:AM:5:00:PM"},"facebook":{"lat":39.12886867,"long":-94.56273637,"street":"1110 Quebec St","city":"Kansas City","state":"MO","postal":"64116","phone":"(816) 474-5262","name":"United Rentals","source":"facebook","rating":0,"distance":14,"responseCode":200,"url":"https://www.facebook.com/United-Rentals-1002602443100928/","stats":{"checkinCount":"18","isCommunityPage":"false","isPublished":"true","likes":"0","parentPage":"200631115132","talkingAboutCount":"0","website":"https://locations.Drupal\ur_appliance.com/mo/kansas-city/639?utm_source=facebook&utm_medium=distrib&utm_campaign=facebook-distrib","wereHereCount":"18"},"website":"https://locations.Drupal\ur_appliance.com/mo/kansas-city/639?utm_source=facebook&utm_medium=distrib&utm_campaign=facebook-distrib","differences":[],"status":"match","isPermanentlyClosed":false,"reviews":[],"categories":{"categories":[],"prettyCategories":["Local Business","Commercial & Industrial Equipment Supplier"]},"lastThirdPartyResponse":200,"hours":"2:07:00:AM:5:00:PM,3:07:00:AM:5:00:PM,4:07:00:AM:5:00:PM,5:07:00:AM:5:00:PM,6:07:00:AM:5:00:PM"},"yelp":{"street":"","city":"","state":"","postal":"","phone":"","name":"","source":"yelp","rating":0,"distance":0,"responseCode":204,"url":"","stats":{},"website":"","differences":[],"status":"missing","categories":{"categories":[],"prettyCategories":[]},"lastThirdPartyResponse":0,"hours":""},"apple":{"lat":39.12886867,"long":-94.56273637,"street":"1110 Quebec St","city":"Kansas City","state":"MO","postal":"64116","phone":"18164745262","name":"United Rentals","source":"apple","rating":0,"distance":14,"responseCode":200,"url":"","stats":{"categories":"","website":""},"website":"","differences":[],"status":"match","isPermanentlyClosed":false,"reviews":[],"categories":{"categories":[],"prettyCategories":["Machine Rental"]},"lastThirdPartyResponse":200,"hours":"2:07:00:AM:5:00:PM,3:07:00:AM:5:00:PM,4:07:00:AM:5:00:PM,5:07:00:AM:5:00:PM,6:07:00:AM:5:00:PM"}},"compareStatus":[{"source":"factual","status":"differ"},{"source":"googlemybusiness","status":"match"},{"source":"google","status":"match"},{"source":"yellowpages","status":"match"},{"source":"foursquare","status":"match"},{"source":"facebook","status":"match"},{"source":"yelp","status":"missing"},{"source":"apple","status":"match"}],"metaData":{"pinSpreadScore":3,"compareScore":81,"pinSpreadBucket":"close","score":87,"pinPlacementBucket":"confirmed","overallPinScore":71,"pinPlacementScore":9},"loc":[-94.56277759999999,39.1289893],"apiIds":{"factual":"10d72be8-4d70-490f-b19a-7c91322f2007","googlemybusiness":"7980304568877148096","google":"ChIJVQeqDaDwwIcRMbadK9NF3n8","yellowpages":"10974585","foursquare":"4e563d5a8130f6bba03c1686","facebook":"1002602443100928","apple":"united_rental_639"},"changeHash":"590b44951600003cded0ce01"}],"count":1}';

  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(CollectionEndpoint::class);
    $this->setDefaultAdapter(WorkbenchAdapter::class);
    $this->getSystemUnderTest();
  }

  public function testRead() {
    $itemId = '58b70bf70e00004a1f200242';
    $endpoint = $this->getSystemUnderTest();

    // Commented this out because it makes a real call.
    // @TODO: Mock the response instead.
    //$result = $endpoint->read($itemId);

    //$this->assertNotEmpty($result);
    //$this->assertInstanceOf(Location::class, $result[0]);
  }

  public function testReadAll() {
    $endpoint = $this->getSystemUnderTest();

    // Commented this out because it makes a real call. Too big of a call.
    // @TODO: Mock the response instead.
//    $result = $endpoint->readAll();
//
//    $this->assertNotEmpty($result);
//    $this->assertInstanceOf(Location::class, $result[0]);
  }

  public function testBasicSearch() {
    /** @var CollectionEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest();
    $keyword = 'MO';
    $limit = 5;
    $offset = 0;

    // Commented this out because it makes a real call.
    // @TODO: Mock the response instead.
    $result = $endpoint->basicSearch($keyword, $limit, $offset);

    $this->assertNotEmpty($result);
    $this->assertInstanceOf(Location::class, $result[0]);
  }

  /**
   * Ensure we can make a Placeable Advanced Search request and receive an
   * expected Location object.
   */
  public function testAdvancedSearch() {
    /** @var CollectionEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest();
    $limit = 1;
    $offset = 0;

    $data = [
      'operator' => 'contains',
      'field' => 'fields.branchId',
      'value' => '639',
      'filter' => 'and',
    ];

    $result = $endpoint->advancedSearch($data, $limit, $offset);

    $this->assertNotEmpty($result);
    $this->assertInstanceOf(Location::class, $result[0]);
  }
}