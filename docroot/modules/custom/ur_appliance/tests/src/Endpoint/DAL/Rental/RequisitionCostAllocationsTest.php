<?php
namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionCostAllocationsEndpoint;
use Drupal\ur_appliance\Data\DAL\CostAllocation;
use Drupal\ur_appliance\Test\BaseTest;

class RequisitionCostAllocationsTest extends BaseTest {
  protected $uri = 'rental/requisitioncostallocations';
  protected $method = 'GET';
  protected $allowedMethods = ['GET','PUT'];

  public function setUp() {
    parent::setUp();
    $this->uri = 'rental/requisitioncostallocations';
    $this->setClassUnderTest(RequisitionCostAllocationsEndpoint::class);
  }

  /**
   * Ensure we are returned a list of allocation requisition codes with mock data.
   */
  public function testRead() {
    $id = 'A000912143';
    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
    
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"id": "A000912143","codeType": ""},"result": [{"orderNum": "A000912143","codeType": "R","allocationId": "1","splitSeqNum": "1","allocationValue": "TOM CARTER","splitPerc": "0","splitStartDate": "0","splitEndDate": "0"},{"orderNum": "A000912143","codeType": "R","allocationId": "2","splitSeqNum": "1","allocationValue": "2667","splitPerc": "0","splitStartDate": "0","splitEndDate": "0"},{"orderNum": "A000912143","codeType": "R","allocationId": "3","splitSeqNum": "1","allocationValue": "?","splitPerc": "0","splitStartDate": "0","splitEndDate": "0"}],"cached": false}';
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserToken($token);

    $allocations = $endpoint->readAll(['id' => $id]);

    foreach ($allocations as $allocation) {
      $this->assertInstanceOf(CostAllocation::class, $allocation);
    }
  }
}
