<?php
namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationRequisitionCodeValuesEndpoint;
use Drupal\ur_appliance\Data\DAL\CodeValue;
use Drupal\ur_appliance\Test\BaseTest;

class AllocationRequisitionCodeValuesTest extends BaseTest {
  protected $uri = 'customer/allocationrequisitioncodevalues';
  protected $method = 'GET';
  protected $allowedMethods = ['GET'];

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/allocationrequisitioncodevalues';
    $this->setClassUnderTest(AllocationRequisitionCodeValuesEndpoint::class);
  }

  /**
   * Ensure we are returned a list of allocation requisition code values with mock data.
   */
  public function testRead() {
    $accountId = '1278567';
    $codeType = 'R';

    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
    $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"accountId":["1278567"],"codeType":"A","idNum":""},"result":[{"accountId":"1278567","codeType":"A","idNum":"1","val":"2411473086-10 MAK"},{"accountId":"1278567","codeType":"A","idNum":"1","val":"2411479997-10 BLKT, SVC,2015, MDA2-3, UNITED RENTA"},{"accountId":"1278567","codeType":"A","idNum":"4","val":"ABCD"},{"accountId":"1278567","codeType":"A","idNum":"4","val":"2411675803- AR010445, /&AMP;"},{"accountId":"1278567","codeType":"A","idNum":"4","val":"2412109087 - 2016, FIREHOUSE"},{"accountId":"1278567","codeType":"A","idNum":"6","val":"10"},{"accountId":"1278567","codeType":"A","idNum":"6","val":"20"}],"cached":false,"recordCount":7}';
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserToken($token);

    $codeValues = $endpoint->readAll(['accountId' => $accountId, 'codeType' => $codeType]);

    foreach ($codeValues as $codeValue) {
      $this->assertInstanceOf(CodeValue::class, $codeValue);
    }
  }
}
