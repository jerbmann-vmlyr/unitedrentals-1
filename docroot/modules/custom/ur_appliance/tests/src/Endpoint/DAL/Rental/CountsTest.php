<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Counts;
use Drupal\ur_appliance\Test\BaseTest;
use Drupal\ur_appliance\Endpoint\DAL\Rental\CountsEndpoint;

class CountsTest extends BaseTest {

  /**
   * generic setup
   */
  public function setUp() {
    parent::setUp();
    $this->uri = 'rental/counts';

    $this->rawResponseToMock = '{"quotes":7,"reservations":7,"onRent":7,"scheduledForPickup":7,"overdue":7,"allOpen":7,"closedContracts":7,"custOwn": 1}';

    $this->setUri('rental/counts');
    $this->setClassUnderTest(CountsEndpoint::class);
  }

  /**
   * Ensure we are returned a counts object.
   */
  public function testRead() {
    $data = ['accountId' => '2707'];
    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserToken($this->defaultToken);

    $counts = $endpoint->read($data);

    $this->assertInstanceOf(Counts::class, $counts);
  }

}