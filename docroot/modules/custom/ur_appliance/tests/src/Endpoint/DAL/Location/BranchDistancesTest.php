<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Location;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Data\DAL\Branch;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Location\BranchDistancesEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

/**
 * Class BranchDistancesTest
 * @package Drupal\ur_appliance\Test\Endpoint\DAL\Location
 */
class BranchDistancesTest extends BaseTest {
  protected $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"address":null,"city":"KANSAS CITY","state":"MO","zip":null,"distance":"40","status":"A","latitude":null,"longitude":null,"min":null,"unit":null,"types":[],"webDisplay":true,"onSite":false,"countryCode":null,"limit":null},"result":[{"id":"639","name":"UNITED RENTALS","address1":"1110 QUEBEC STREET","address2":"","city":"NORTH KANSAS CITY","state":"MO","zip":"64116","phone":"816-474-5262","fax":"816-474-5266","weekdayhours":"7:00AM-5:00PM","saturdayhours":"CLOSED","sundayhours":"CLOSED","timeOffset":"-100","managerName":"DAVID POLLETTA","reportHeading":"MC-NORTH KANSAS CITY, MO (639)","status":"A","langCode":"ENU","currency":"USD","countryCode":"US","businessType":"GR","webDisplay":true,"latitude":"39.129039","longitude":"-94.562753","formerRSC":false,"acquiredCompanyCode":"","businessTypes":[{"type":"GR"},{"type":"SM"}],"distance":2.1954615161199,"onSite":false},{"id":"G09","name":"UNITED RENTALS","address1":"3505 MANCHESTER TRAFFICWAY","address2":"","city":"KANSAS CITY","state":"MO","zip":"64129","phone":"816-921-4141","fax":"816-921-4242","weekdayhours":"7:00AM-5:00PM","saturdayhours":"CLOSED","sundayhours":"CLOSED","timeOffset":"-100","managerName":"DANIEL HABEL","reportHeading":"TS-KANSAS CITY, MO (G09)","status":"A","langCode":"ENU","currency":"USD","countryCode":"US","businessType":"TR","webDisplay":true,"latitude":"39.060961","longitude":"-94.501619","formerRSC":false,"acquiredCompanyCode":"","businessTypes":[{"type":"TS"}],"distance":4.9197858896857,"onSite":false},{"id":"61A","name":"UNITED RENTALS","address1":"3505 MANCHESTER TRAFFICWAY","address2":"","city":"KANSAS CITY","state":"MO","zip":"64129","phone":"816-921-8051","fax":"816-861-0304","weekdayhours":"7:30AM-5:00PM","saturdayhours":"CLOSED","sundayhours":"CLOSED","timeOffset":"0","managerName":"KRYSTAL BERGSTROM","reportHeading":"PH-KANSAS CITY, MO (61A)","status":"A","langCode":"ENU","currency":"USD","countryCode":"US","businessType":"PR","webDisplay":true,"latitude":"39.060657","longitude":"-94.500668","formerRSC":false,"acquiredCompanyCode":"","businessTypes":[],"distance":4.9740215678442,"onSite":false},{"id":"Q98","name":"UNITED RENTALS","address1":"11615 SOUTH ROGERS ROAD","address2":"","city":"OLATHE","state":"KS","zip":"66062","phone":"913-338-3363","fax":"913-338-3438","weekdayhours":"7:00AM-5:00PM","saturdayhours":"CLOSED","sundayhours":"CLOSED","timeOffset":"-100","managerName":"CARY BARROWS","reportHeading":"MC-OLATHE, KS (Q98)","status":"A","langCode":"ENU","currency":"USD","countryCode":"US","businessType":"GR","webDisplay":true,"latitude":"38.917149","longitude":"-94.764619","formerRSC":true,"acquiredCompanyCode":"R","businessTypes":[{"type":"GR"}],"distance":16.090066547104,"onSite":false},{"id":"735","name":"UNITED RENTALS","address1":"6905 EAST 163RD STREET","address2":"","city":"BELTON","state":"MO","zip":"64012","phone":"816-331-4400","fax":"816-322-4888","weekdayhours":"7:00AM-5:00PM","saturdayhours":"CLOSED","sundayhours":"CLOSED","timeOffset":"-100","managerName":"MICHAEL WILLIAMS","reportHeading":"MC-BELTON, MO (735)","status":"A","langCode":"ENU","currency":"USD","countryCode":"US","businessType":"GR","webDisplay":true,"latitude":"38.827549","longitude":"-94.517569","formerRSC":false,"acquiredCompanyCode":"","businessTypes":[{"type":"GR"},{"type":"AE"},{"type":"SC"},{"type":"SM"}],"distance":19.088110194869,"onSite":false},{"id":"J69","name":"UNITED RENTALS","address1":"930 EAST 30TH STREET","address2":"","city":"LAWRENCE","state":"KS","zip":"66046","phone":"785-838-4110","fax":"785-838-4472","weekdayhours":"7:00AM-5:00PM","saturdayhours":"CLOSED","sundayhours":"CLOSED","timeOffset":"-100","managerName":"GEORGE FIELDS","reportHeading":"MC-LAWRENCE, KS (J69)","status":"A","langCode":"ENU","currency":"USD","countryCode":"US","businessType":"GR","webDisplay":true,"latitude":"38.929675","longitude":"-95.224070","formerRSC":true,"acquiredCompanyCode":"R","businessTypes":[],"distance":36.589292841843,"onSite":false}],"cached":false}';

  public function setUp() {
    parent::setUp();
    $this->uri = 'location/branchdistances';
    $this->setClassUnderTest(BranchDistancesEndpoint::class);
    $this->getSystemUnderTest();
  }

  /**
   * Ensure branch distances endpoint returns Branch data objects
   */
  public function testRead() {
    $data = ['city' => 'Kansas City', 'state' => 'MO', 'distance' => 40, 'webDisplay' => true];
    $endpoint = $this->getSystemUnderTest();

    $branches = $endpoint->read($data);
    $this->assertCount(6, $branches);

    foreach ($branches as $branch) {
      $this->assertInstanceOf(Branch::class, $branch);
    }
  }

  /**
   * Ensure branch distances endpoint returns Branch data objects
   *
   * @group live
   */
  public function testReadTwo() {
    $data = ['lat' => '47.7504469', 'long' => '-90.3342726', 'status' => 'A', 'distance' => '', 'unit' => 'M', 'types' => 'GR,AR', 'countryCode' => 'US', 'limit' => '10', 'webDisplay' => true, 'onlineOrders' => true];
    $endpoint = $this->getSystemUnderTest(null, true, true);

    $branches = $endpoint->read($data);
    $this->assertCount(10, $branches);

    /** @var Branch $branch */
    foreach ($branches as $branch) {
      $this->assertInstanceOf(Branch::class, $branch);
      $this->assertTrue($branch->getOnlineOrders());
    }
  }

  /**
   * Ensure invalid data is caught and error is thrown.
   *
   * @expectedException \Drupal\ur_appliance\Exception\DAL\DalException
   * @expectedExceptionMessage branchdistances endpoint requires City/State or Zip parameters. Or Lat and Long parameters.
   */
  public function testReadInvalidParameters() {
    $invalidData = ['city' => 'Kansas City', 'limit' => 1];
    $endpoint = $this->getSystemUnderTest();
    $branches = $endpoint->read($invalidData);
  }
}