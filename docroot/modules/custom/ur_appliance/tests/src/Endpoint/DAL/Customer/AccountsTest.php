<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Data\DAL\Account;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Customer\AccountsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class AccountsTest extends BaseTest {
  protected $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"id":["2707"],"includeLinked":0,"includeCreditLimit":true,"includeCollectorInfo":false},"result":[{"id":"2707","status":"I","statusDesc":"Inactive","name":"MATRIX LOGISTICS","address1":"%TIBBETT & BRITTEN GROUP","address2":"395 W NORTHWEST PARKWAY #100","city":"SOUTHLAKE","state":"TX","zip":"76092","phone":"817-251-9401","requirePo":"N","countryCode":"","creditLimit":50000,"lastTransactionDate":"2002-06-21T00:00:00","lastPaymentDate":"2002-07-19T00:00:00","lastPaymentAmount":"598.39","amountCurrent":"0","amount30day":"0","amount60day":"0","amount90day":"0","amount120day":"0","rentalYTD":"0","rentalLTD":"19250.00","rentalLYR":"0","parentId":"0","consolidatedBill":"N","consolidatedBillDay":"0","consolidatedBillEmail":"","nextConsolidatedBillDate":null,"lastConsolidatedBillDate":null,"jobCost":false,"contactPhone":"800-877-3687","contactPhoneAlpha":"800-UR-RENTS","email":"","tcLevel":"E","averageDays":"0","terms":"N0","cycleBillCode":"A","billDay":"0","prorateRentals":"N","prorateAfterWeek":"","prorateAfterMonth":"","weekDivisor":"0","monthDivisor":"0","rppDefault":"Y","openRentalAmount":"0","collectorName":"","collectorPhone":"","collectorEmail":"","quoteRedirectLoc":"","reservationRedirectLoc":"","creditCode":"Credit","creditCardAuthorized":"N"}],"cached":false}';

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/accounts';
    $this->setClassUnderTest(AccountsEndpoint::class);
  }

  /**
   * Test retrieving account information
   */
  public function testGetAccount() {

    $accountId = '2707';
    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';

    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserToken($token);

    $account = $endpoint->read($accountId);
    $this->assertInstanceOf(Account::class, $account);
  }

  /**
   * Test retrieving an accountId after creating an account
   */
  public function testCreateAccount() {
    $data = [
      'name' => 'Crazy Patties Biz',
      'address1' => '10895 Lowell Avenue',
      'city' => 'Overland Park',
      'state' => 'KS',
      'zip' => '66210',
      'phone' => '816-555-5555',
      'email' => 'pat-1-23-1@example.com',
      'comment' => '',
      'rpp' => 'C'
    ];

    $this->rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"name":"Crazy+Patties+Biz","address1":"10895+Lowell+Avenue","address2":"","city":"Overland+Park","state":"KS","zip":"66210","phone":"816-555-5555","email":"pat-1-23-1@example.com","comment":"","rpp":"C"},"result":{"accountId":2878748},"cached":false}';


    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';

    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserToken($token);

    $accountId = $endpoint->create($data);
    $this->assertNotNull($accountId);
  }

  /**
   * Test retrieving an accountId
   *
   * @TODO: Review this later and create a method for testing real calls
   * to the DAL and make this one test mock calls
   *
   * @group live
   */
  public function testReadAccount() {
    $id = '2707'; // A real test Id.
    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
    // $adapter = ApiContainer::getService('dal_adapter');
    // $endpoint = new Accounts($adapter);
    $endpoint = $this->getSystemUnderTest(null, true);
    $endpoint->getAdapter()->setUserToken($token);

    $account = $endpoint->read($id);

    $this->assertInstanceOf(Account::class, $account);
    $this->assertEquals($id, $account->getId());
  }

  /**
   * Test updating an account
   */
  public function testUpdateAccount() {
    $data = [
      "accountId" => "3877935",
      "name" => "Better Account Name",
      "address1" => "250 Richards Rd.",
      "address2" => "Suite 130",
      "city" => "Kansas City",
      "state" => "MO",
      "zip" => "64152",
      "phone" => "123-123-1234",
      "email" => "CARSON-9-15-2@EXAMPLE.COM",
    ];

    $response = '{"status":0,"messageId":null,"messageText":null,"request":{"accountId":"3877935","name":"Better Account Name","address1":"250 Richards Rd.","address2":"Suite 130","city":"Kansas City","state":"MO","zip":"64152","phone":"123-123-1234","email":"CARSON-9-15-2@EXAMPLE.COM"},"result":[],"cached":false}';

    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\AccountsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($response);
    $endpoint->getAdapter()->setUserToken($token);

    $wasSuccessful = $endpoint->update($data);

    $this->assertTrue($wasSuccessful);
  }
}