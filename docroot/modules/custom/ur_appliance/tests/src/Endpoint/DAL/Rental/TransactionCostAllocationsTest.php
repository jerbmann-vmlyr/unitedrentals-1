<?php
namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionCostAllocationsEndpoint;
use Drupal\ur_appliance\Data\DAL\CostAllocation;
use Drupal\ur_appliance\Test\BaseTest;

class TransactionCostAllocationsTest extends BaseTest {
  protected $uri = 'rental/transactioncostallocations';
  protected $method = 'GET';
  protected $allowedMethods = ['GET','PUT'];

  public function setUp() {
    parent::setUp();
    $this->uri = 'rental/transactioncostallocations';
    $this->setClassUnderTest(TransactionCostAllocationsEndpoint::class);
  }

  /**
   * Ensure we are returned a list of allocation requisition codes with mock data.
   */
  public function testRead() {
    $transId = '144060476';
    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
    $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"transId":"144060476","transSeqNum":"","codeType":"A"},"result":[{"transId":"144060476","transSeqNum":"0","codeType":"A","allocationId":"1","splitSeqNum":"1","allocationValue":"TEST VALUE 3","splitPerc":".40","splitStartDate":"6","splitEndDate":"1"}],"cached":false}';
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserToken($token);

    $allocations = $endpoint->readAll(['transId' => $transId]);

    foreach ($allocations as $allocation) {
      $this->assertInstanceOf(CostAllocation::class, $allocation);
    }
  }
}
