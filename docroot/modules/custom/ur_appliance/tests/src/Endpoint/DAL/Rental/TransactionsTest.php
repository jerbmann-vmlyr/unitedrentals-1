<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Item;
use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class TransactionsTest extends BaseTest {
  protected $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"branchId":"P79","accountId":"1075311","transType":"R","email":"ryan.mott@vml.com","jobId":"1","poNumber":"NPOR","orderedBy":"","startDateTime":"2017-09-10T09:00:00","returnDateTime":"2017-09-28T09:00:00","delivery":1,"pickup":1,"pickupFirm":0,"rpp":1,"emailPDF":0,"transSource":"W","jobContactName":"Ryan Mott","jobContactPhone":"8162182947","eqpCount":1,"creditCardTokenId":"Z000000632","totalDelivery":0,"totalPickup":0,"mgrRateReasonCode":"","approver":"","requester":"","requesterName":"","requesterPhone":"","debug":"","eqpDetails":[{"category":"375","class":"4894","quantity":1,"rateOverride":"N","rateType":"WB","minRate":1250,"dayRate":1250,"weekRate":2958,"monthRate":7550}]},"result":{"transID":148189202,"reqID":"A000847196"},"cached":false}';

  public function setUp() {
    parent::setUp();

    $this->uri = 'rental/transactions';
    $this->setClassUnderTest(TransactionsEndpoint::class);
  }

  /**
   * Ensure that we can recall info for a single transaction
   */
  public function testRead() {
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"id": ["152085468"],"equipmentId": null,"includeClosed": false,"seqId": "","includeAllocations": true,"custOwn": false},"result": [{"accountId": "35934","catClass": "290-3010","catClassDesc": "AUGER GAS 1 MAN","imagePath": "https://images.ur.com/catclass/290-3010.jpg","transId": "152085468","transType": "R","openTrans": true,"transSeqId": "0","transLineId": "1","transLineType": "VR","equipmentId": "2903010","urEquipmentId": "","vndEquipmentId": "","quantity": 1,"quantityOnRent": 1,"quantityReturned": 0,"quantityPickedUp": 0,"quantityNextPickup": "","quantityPendingPickup": 0,"branchId": "521","startDateTime": "2018-01-11T13:00:00","exchangeDateTime": null,"returnDateTime": "2018-01-12T10:00:00","pickupDateTime": null,"nextPickupDateTime": null,"po": "NPOR","jobsiteId": "168","jobsiteName": "JENSEN DRILLING - LONG BEACH","jobsiteAddress1": "6801 E 2ND ST","jobsiteCity": "LONG BEACH","jobsiteState": "CA","jobsiteZip": "90803-4324","jobsitePhone": "5417267435","jobsiteLocation": "064","customerJobId": "","make": "","model": "","serial": "","year": "2000","eqpValue": "","dayRate": "87.00","weekRate": "313.00","monthRate": "877.00","minRate": "87.00","urPickup": false,"urDeliver": false,"createdDateTime": "2017-11-13T17:48:34","pendingPickup": false,"rentalAmount": "","currency": "USD","pastDue": false,"pickupType": "","pickupId": "","orderedBy": "DAVID VML","bulk": false,"leniencyStartDate": "0","leniencyEndDate": "0","meter": "0","odometer": 0,"lastBilledDate": 0,"customerName": "JENSEN DRILLING CO","customerAddress1": "1775 HENDERSON AVE","customerAddress2": "","customerCity": "EUGENE","customerState": "OR","customerZip": "97403-2371","customerPhone": "5417267435","requisitionId": "A001002570","approverGuid": "","approverName": "","createdGuid": "d5f5ea4e-f6eb-1b49-89d4-fc01adbdf823","createdName": "DAVID VML","requesterGuid": "d5f5ea4e-f6eb-1b49-89d4-fc01adbdf823","requesterName": "DAVID VML","acctCode1": "","acctCode2": "","acctCode3": "","acctCode4": "","acctCode5": "","acctCode6": "","reqCode1": "","reqCode2": "","reqCode3": "","reqCode4": "","hasFASTPdf": false,"outsourcedEqpDesc": "","equipmentSource": "UNITED RENTALS","trackingNumber": "","custOwn": "","statusCodeCoe": "","statusCodeDescCoe": "","simId": "","simActive": false,"altitude": 0,"dateExpired": "20180118","dateCanceled": "20171113","requestContactName": "DAVID VML","reportedDateTime": null,"lastMeterChange": null,"latitude": "","longitude": "","usageToday": 0,"tabDataPdfUrl": "","hasTabData": "","cycleBillCode": "A","urWillPickup": false}],"cached": false,"recordCount": 1}';

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setMethod('GET');
    $endpoint->getAdapter()->setUserEmail('urvml2017.1@gmail.com');

    $data = [
      'id' => '152085468'
    ];

    $results = $endpoint->read($data);

    $this->assertInstanceOf(Order::class, $results);
  }

  public function testReadAll() {
    // This response was massive, took only 1 transaction for brevity rather than all 100 or so
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"startRecord": 1,"maxRecords": 0,"includeLinked": false,"jobId": [],"po": [],"glCode": null,"costCode": null,"catClass": [],"apvGuid": null,"crtGuid": null,"reqGuid": null,"branchId": null,"category": null,"includeAllocations": true,"custOwn": false,"sortBy": null,"sortOrder": null,"search": null,"dayOffset": "0","headerOnly": false,"source": null,"accountId": ["858702"],"type": ["2"]},"result": [{"accountId": "858702","catClass": "241-7662","catClassDesc": "CABLE 4/0 AWG 400 AMP 50\' CAM","imagePath": "https://images.ur.com/catclass/241-7662.jpg","transId": "147027879","transType": "Q","openTrans": true,"transSeqId": "0","transLineId": "12","transLineType": "VR","equipmentId": "2417662","urEquipmentId": "","vndEquipmentId": "","quantity": 375,"quantityOnRent": 375,"quantityReturned": 0,"quantityPickedUp": 0,"quantityNextPickup": "","quantityPendingPickup": 0,"branchId": "S37","startDateTime": "2017-08-01T06:00:00","exchangeDateTime": null,"returnDateTime": "2017-09-01T06:00:00","pickupDateTime": null,"nextPickupDateTime": null,"po": "","jobsiteId": "460013","jobsiteName": "U2 TURNAROUND GROUP","jobsiteAddress1": "U2 TURNAROUND","jobsiteCity": "FORT MCMURRAY","jobsiteState": "AB","jobsiteZip": "T9H","jobsitePhone": "4034784808","jobsiteLocation": "E79","customerJobId": "460013","make": "","model": "","serial": "","year": "2000","eqpValue": "","dayRate": "8.40","weekRate": "21.00","monthRate": "52.50","minRate": "0","urPickup": false,"urDeliver": false,"createdDateTime": "2017-05-28T16:46:57","pendingPickup": false,"rentalAmount": "","currency": "CAD","pastDue": false,"pickupType": "","pickupId": "","orderedBy": "RODNEY REID","bulk": true,"leniencyStartDate": "","leniencyEndDate": "","meter": "0","odometer": 0,"lastBilledDate": 0,"customerName": "CANADIAN NATURAL RESOURCES LTD","customerAddress1": "2500 2 ST SW","customerAddress2": "","customerCity": "CALGARY","customerState": "AB","customerZip": "T2S 0J9","customerPhone": "4035176700","requisitionId": "A000803799","approverGuid": "","approverName": "","createdGuid": "","createdName": "","requesterGuid": "","requesterName": "ANDREW CHAISSON","acctCode1": "TBD","acctCode2": "U2 TURNAROUND","acctCode3": "TBD","acctCode4": "","acctCode5": "","acctCode6": "","reqCode1": "","reqCode2": "","reqCode3": "","reqCode4": "","hasFASTPdf": false,"outsourcedEqpDesc": "","equipmentSource": "UNITED RENTALS","trackingNumber": "","custOwn": "","statusCodeCoe": "","statusCodeDescCoe": "","simId": "","simActive": false,"altitude": 0,"dateExpired": "20170831","dateCanceled": "20170528","requestContactName": "ANDREW CHAISSON","reportedDateTime": null,"lastMeterChange": null,"latitude": "","longitude": "","usageToday": 0,"tabDataPdfUrl": "","hasTabData": ""}],"cached": false,"recordCount": 17,"startRecord": 1,"endRecord": 17,"moreRecords": false
}';

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');

    $data = [
      'accountId' => '858702',
      'type' => '2'
    ];

    $result = $endpoint->readAll($data);
    $this->assertInternalType('array', $result);

    foreach ($result as $transaction) {
      $this->assertInstanceOf(Order::class, $transaction);
    }
  }

  public function testReadAllByItem() {
    // This response was massive, took only 1 transaction for brevity rather than all 100 or so
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"startRecord": 1,"maxRecords": 0,"includeLinked": false,"jobId": [],"po": [],"glCode": null,"costCode": null,"catClass": [],"apvGuid": null,"crtGuid": null,"reqGuid": null,"branchId": null,"category": null,"includeAllocations": true,"custOwn": false,"sortBy": null,"sortOrder": null,"search": null,"dayOffset": "0","headerOnly": false,"source": null,"accountId": ["858702"],"type": ["2"]},"result": [{"accountId": "858702","catClass": "241-7662","catClassDesc": "CABLE 4/0 AWG 400 AMP 50\' CAM","imagePath": "https://images.ur.com/catclass/241-7662.jpg","transId": "147027879","transType": "Q","openTrans": true,"transSeqId": "0","transLineId": "12","transLineType": "VR","equipmentId": "2417662","urEquipmentId": "","vndEquipmentId": "","quantity": 375,"quantityOnRent": 375,"quantityReturned": 0,"quantityPickedUp": 0,"quantityNextPickup": "","quantityPendingPickup": 0,"branchId": "S37","startDateTime": "2017-08-01T06:00:00","exchangeDateTime": null,"returnDateTime": "2017-09-01T06:00:00","pickupDateTime": null,"nextPickupDateTime": null,"po": "","jobsiteId": "460013","jobsiteName": "U2 TURNAROUND GROUP","jobsiteAddress1": "U2 TURNAROUND","jobsiteCity": "FORT MCMURRAY","jobsiteState": "AB","jobsiteZip": "T9H","jobsitePhone": "4034784808","jobsiteLocation": "E79","customerJobId": "460013","make": "","model": "","serial": "","year": "2000","eqpValue": "","dayRate": "8.40","weekRate": "21.00","monthRate": "52.50","minRate": "0","urPickup": false,"urDeliver": false,"createdDateTime": "2017-05-28T16:46:57","pendingPickup": false,"rentalAmount": "","currency": "CAD","pastDue": false,"pickupType": "","pickupId": "","orderedBy": "RODNEY REID","bulk": true,"leniencyStartDate": "","leniencyEndDate": "","meter": "0","odometer": 0,"lastBilledDate": 0,"customerName": "CANADIAN NATURAL RESOURCES LTD","customerAddress1": "2500 2 ST SW","customerAddress2": "","customerCity": "CALGARY","customerState": "AB","customerZip": "T2S 0J9","customerPhone": "4035176700","requisitionId": "A000803799","approverGuid": "","approverName": "","createdGuid": "","createdName": "","requesterGuid": "","requesterName": "ANDREW CHAISSON","acctCode1": "TBD","acctCode2": "U2 TURNAROUND","acctCode3": "TBD","acctCode4": "","acctCode5": "","acctCode6": "","reqCode1": "","reqCode2": "","reqCode3": "","reqCode4": "","hasFASTPdf": false,"outsourcedEqpDesc": "","equipmentSource": "UNITED RENTALS","trackingNumber": "","custOwn": "","statusCodeCoe": "","statusCodeDescCoe": "","simId": "","simActive": false,"altitude": 0,"dateExpired": "20170831","dateCanceled": "20170528","requestContactName": "ANDREW CHAISSON","reportedDateTime": null,"lastMeterChange": null,"latitude": "","longitude": "","usageToday": 0,"tabDataPdfUrl": "","hasTabData": ""}],"cached": false,"recordCount": 17,"startRecord": 1,"endRecord": 17,"moreRecords": false
}';

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');

    $data = [
      'accountId' => '858702',
      'type' => '2'
    ];

    $result = $endpoint->readAllByItem($data);
    $this->assertInternalType('array', $result);

    foreach ($result as $transaction) {
      $this->assertInstanceOf(Item::class, $transaction);
    }
  }

  /**
   * @group live
   */
  public function testReadAllByItemLive() {
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest(null, true, true);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');

    $data = [
      'accountId' => '858702',
      'type' => '1'
    ];

    $result = $endpoint->readAllByItem($data);
    $this->assertInternalType('array', $result);

    foreach ($result as $transaction) {
      $this->assertInstanceOf(Item::class, $transaction);
    }
  }

  /**
   * @group live
   */
  public function testReadAllLive() {
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest(null, true, true);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');

    $data = [
      'accountId' => '858702',
      'type' => '1'
    ];

    $result = $endpoint->readAll($data);
    $this->assertInternalType('array', $result);

    foreach ($result as $transaction) {
      $this->assertInstanceOf(Item::class, $transaction);
    }
  }

  /**
   * Tests the POST method of the transactions endpoint.
   *
   * @TODO: Figure out how to prevent this from making live POST calls.
   */
  public function testPostExport() {
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint $endpoint */
//    $endpoint = $this->getSystemUnderTest();
//    $endpoint->getAdapter()->setMethod('POST');
//    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
//
//    // @TODO: Add requester, requesterName, requesterPhone
//    $data = [
//      'branchId' => 'P79',
//      'accountId' => 1075311,
//      'transType' => 'R',
//      'email' => 'ryan.mott@vml.com',
//      'jobsiteId' => '1',
//      'jobsiteContact' => 'Ryan Mott',
//      'jobsitePhone' => '8162182947',
//      'startDateTime' => '2017-09-10T09:00:00',
//      'returnDateTime' => '2017-09-28T09:00:00',
//      'delivery' => true,
//      'pickup' => true,
//      'rpp' => true,
//      'items' => [
//        '375-4894' => [
//          'catClass' => '375-4894',
//          'quantity' => '1',
//          'rate' => [
//            'dayrate' => '1250',
//            'weekrate' => '2958',
//            'monthrate' => '7550',
//            'minrate' => '1250',
//          ],
//        ],
//      ],
//      'cardId' => 'Z000000632',
//      'totals' => [
//        'totrentamt' => 9000,
//        'totenvamt' => 75,
//        'tottaxamt' => 748.69,
//        'totgstamt' => 0,
//        'totdwamt' => 0,
//        'totdelamt' => 0,
//        'totpuamt' => 0,
//        'miscCharges' => 18.96,
//        'authAmount' => 12279.61,
//        'authPercent' => 125,
//        'rateZone' => '021',
//        'tspAgrmt' => 'N',
//        'tspAgrmtRate' => 0,
//      ],
//    ];
//
//    /** @var \Drupal\ur_appliance\Data\DAL\Order $order */
//    $order = $endpoint->post($data);
//    $this->assertNotNull($order->getTransId()); // transId could be 0
//    $this->assertNotEmpty($order->getRequisitionId());
  }
  
  public function testUpdateTransaction() {
    $data = [
      'transId' => '149869906',
      'updateType' => 'transaction',
      'startDateTime' => '2017-11-21T10:00:00',
      'returnDateTime' => '2017-11-22T10:00:00',
      'pickupFirm' => false,
    ];
    
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"transId": "149869906","transSeqId": "","po": "","includeClosed": false,"poType": "","reqId": "","updateType": "transaction","updateUser": "","cancelType": "X","payloadId": ""},"result": {"userid": "urvml2017.1@gmail.com","transSeqId": "","includeClosed": "","poType": "","po": "","transId": "149869906","reqId": "A000914059","updateType": "transaction","updateUser": "","cancelType": "X","payloadId": "","startDateTime": "2017-11-21T10:00:00","returnDateTime": "2017-11-22T10:00:00","pickupFirm": "N","systemDate": "20171107","delivery": "*NULL","pickup": "*NULL","eqpDetails": {"seq#": "-1","category": 0,"class": 0,"quantity": 0,"rateOverride": "","rateType": "","minRate": 0,"dayRate": 0,"weekRate": 0,"monthRate": 0,"comment": ""}},"cached": false}';
    
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserEmail('urvml2017.1@gmail.com');
    
    $status = $endpoint->update($data);
    $this->assertEquals('N', $status->result->pickupFirm);
    $this->assertEquals('2017-11-21T10:00:00', $status->result->startDateTime);
    $this->assertEquals('2017-11-22T10:00:00', $status->result->returnDateTime);
  }
  
  public function testConvertQuoteToReservation() {
    $data = [
      'transId' => '149870765',
      'updateType' => 'reserve',
      'po' => 'NPOR',
    ];
    
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"transId": "149870765","transSeqId": "","po": "NPOR","includeClosed": false,"poType": "","reqId": "","updateType": "reserve","updateUser": "","cancelType": "X","payloadId": ""},"result": {"userid": "urvml2017.1@gmail.com","transSeqId": "","includeClosed": "","poType": "","po": "NPOR","transId": "149870765","reqId": "A000914489","updateType": "reserve","updateUser": "","cancelType": "X","payloadId": "","systemDate": "20171108"},"cached": false}';
    
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserEmail('urvml2017.1@gmail.com');
    
    $status = $endpoint->update($data);
    $this->assertEquals(0, $status->status);
  }
  
  public function testUpdateItemQuantity() {
    $data = [
      'id' => '5174135',
      'requestId' => 'A001072801',
      'transId' => '154378368',
      'updateType' => 'transaction',
      'transSource' => 'W',
      'eqpDetailsInfo' => [
        'quantity' => '1',
        'catClass' => '310-8001'
      ],
      'eqpDetails' => [
        'seqId' => '1',
        'quantity' => '1',
        'catClass' => '310-8001'
      ],
    ];
    
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": [],"result": {"id": "A001072801"},"cached": false}';
    
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\TransactionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserEmail('urvml2017.1@gmail.com');
    
    $status = $endpoint->update($data);
    $this->assertEquals(0, $status->status);
  }
}