<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Order;
use Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionsEndpoint;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Test\BaseTest;

class RequisitionTest extends BaseTest {

  public function setUp() {
    parent::setUp();
    
    $this->uri = 'rental/requisitions';
    $this->setClassUnderTest(RequisitionsEndpoint::class);
    $this->getSystemUnderTest();
  }

  /**
   * @group live
   */
  public function testReadLive() {
    $mockData = [
      'accountId' => "18",
      'catclass' => "100-3185",
      'quantity' => "2",
      'requestId' => "A001176262",
      'statusCode' => "OR"
    ];

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest(null, true, true);
    $adaptor = $endpoint->getAdapter();
    $adaptor->setUserEmail($this->defaultUserEmail);
    $adaptor->setUserToken($this->defaultToken);

    $requisitions = $endpoint->read($mockData);
    
    foreach ($requisitions as $requisition) {
      $this->assertInstanceOf(Order::class, $requisition);
    }
  }

  /**
   * Test for Exceptions with empty catClass
   */
  public function testRequiredCatClassEmpty() {
    $badMockData = [
      'accountId' => "18",
      'catclass' => "",
      'quantity' => "2",
      'requestId' => "A001176262",
      'statusCode' => "OR"
    ];

    $mockResponse = '{"status":0,"messageId":null,"messageText":null,"request":[],"result":{"id":"A001176262"},"cached":false}';

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($mockResponse);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');

    $this->expectException(\Drupal\ur_appliance\Exception\EndpointException::class);

    $requisitions = $endpoint->update($badMockData);

  }
  /**
   * Test for Exceptions with empty quantity
   */
  public function testRequiredQuantityEmpty() {
    $badMockData = [
      'accountId' => "18",
      'catclass' => "100-3185",
      'quantity' => "",
      'requestId' => "A001176262",
      'statusCode' => "OR"
    ];

    $mockResponse = '{"status":0,"messageId":null,"messageText":null,"request":[],"result":{"id":"A001176262"},"cached":false}';

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($mockResponse);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');

    $this->expectException(\Drupal\ur_appliance\Exception\EndpointException::class);

    $requisitions = $endpoint->update($badMockData);

  }
  /**
   * Test for Exceptions with missing StatusCode
   */
  public function testRequiredStatusCodeMissing() {
    $badMockData = [
      'accountId' => "18",
      'catclass' => "100-3185",
      'quantity' => "2",
      'requestId' => "A001176262",
    ];

    $mockResponse = '{"status":0,"messageId":null,"messageText":null,"request":[],"result":{"id":"A001176262"},"cached":false}';

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($mockResponse);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');

    $this->expectException(\Drupal\ur_appliance\Exception\EndpointException::class);

    $requisitions = $endpoint->update($badMockData);

  }

  /**
   * If $requisitions->details is populated, $requisitions->catClassRequired MUST BE TRUE.
   *
   * This is necessary to match with DAL idiosyncrasies.
   */
  public function testValidCatClassRequired() {
    $data = [
      'accountId' => "18",
      'catclass' => "100-3185",
      'quantity' => "2",
      'requestId' => "A001176262",
      'statusCode' => "OR"
    ];
    $mockResponse = '{"status":0,"messageId":null,"messageText":null,"request":[],"result":{"id":"A001176262"},"cached":false}';

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($mockResponse);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');

    $requisitions = $endpoint->update($data);

    if (isset($requisitions->details)) {
      $this->assertEquals(true, $requisitions->catClassRequired);
    }

  }

  public function testReadMock() {
    $data = [
      'accountId' => '18',
      'requestId' => 'A000913370'
    ];
    
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"requestId": "A000913370","lineSeq": "","status": "","includecustcodes": "","excludecustcodes": "","returnSeq": "","catClass": ""},"result": [{"requestId": "A000913370","branchId": "P79","accountId": "18","statusCode": "RR","statusCodeDesc": "Reservation Requested","statusCodeDescCoe": "","poNumber": "NPOR","reqGuid": "0e35d520-1297-d44f-8bc1-3a9ae944f648","reqContact": "RYAN MOTT","reqPhone": "816-785-9271","reqEmail": "ur.vml.7@gmail.com","jobContact": "Ryan Mott","jobPhone": "816-218-2947","jobEmail": "","rpp": "Y","reqDateTime": "2017-09-11T15:51:00","jobId": "7","jobsiteName": "KING ELECTRIC","jobsiteAddress1": "COURTHOUSE","jobsiteCity": "FAYETTEVILLE","jobsiteState": "NC","customerJobId": "7","startDateTime": "2017-10-12T14:48:00","endDateTime": "2017-10-14T13:17:00","delivery": "N","pickup": "A","approverGuid": "","approverName": "","createdGuid": "0e35d520-1297-d44f-8bc1-3a9ae944f648","createdName": "RYAN MOTT","timeStamp": "2017-09-11 15:51:54.668000","equipmentId": "","miscChg": "0","transChg": "0","make": "","model": "","serial": "","alias": "","catClass": "375-4894","catclassDesc": "GAME FUN CASTLE","qty": "1","outsourced": "","equipmentType": "","remainingQty": "1","scheduledPickupQty": "0","reservationQuoteId": "","quoteId": "","seqNum": "0","lineSeq": "0","lineNum": "1","custOwn": "","coeStatusCode": "","bulk": true,"meter": "0","dayRate": "123.60","weekRate": "370.80","monthRate": "741.60","totalAmount": "0","vendorName": ""},{"requestId": "A000913370","branchId": "P79","accountId": "18","statusCode": "RR","statusCodeDesc": "Reservation Requested","statusCodeDescCoe": "","poNumber": "NPOR","reqGuid": "0e35d520-1297-d44f-8bc1-3a9ae944f648","reqContact": "RYAN MOTT","reqPhone": "816-785-9271","reqEmail": "ur.vml.7@gmail.com","jobContact": "Ryan Mott","jobPhone": "816-218-2947","jobEmail": "","rpp": "Y","reqDateTime": "2017-09-11T15:51:00","jobId": "7","jobsiteName": "KING ELECTRIC","jobsiteAddress1": "COURTHOUSE","jobsiteCity": "FAYETTEVILLE","jobsiteState": "NC","customerJobId": "7","startDateTime": "2017-10-12T14:48:00","endDateTime": "2017-10-14T13:17:00","delivery": "N","pickup": "A","approverGuid": "","approverName": "","createdGuid": "0e35d520-1297-d44f-8bc1-3a9ae944f648","createdName": "RYAN MOTT","timeStamp": "2017-09-11 15:51:54.668000","equipmentId": "","miscChg": "0","transChg": "0","make": "","model": "","serial": "","alias": "","catClass": "310-6026","catclassDesc": "BOOM 60-64\' TELESCOPIC","qty": "1","outsourced": "","equipmentType": "","remainingQty": "1","scheduledPickupQty": "0","reservationQuoteId": "","quoteId": "","seqNum": "0","lineSeq": "0","lineNum": "2","custOwn": "","coeStatusCode": "","bulk": false,"meter": "0","dayRate": "477.00","weekRate": "1249.00","monthRate": "2497.00","totalAmount": "0","vendorName": ""},{"requestId": "A000913370","branchId": "P79","accountId": "18","statusCode": "RR","statusCodeDesc": "Reservation Requested","statusCodeDescCoe": "","poNumber": "NPOR","reqGuid": "0e35d520-1297-d44f-8bc1-3a9ae944f648","reqContact": "RYAN MOTT","reqPhone": "816-785-9271","reqEmail": "ur.vml.7@gmail.com","jobContact": "Ryan Mott","jobPhone": "816-218-2947","jobEmail": "","rpp": "Y","reqDateTime": "2017-09-11T15:51:00","jobId": "7","jobsiteName": "KING ELECTRIC","jobsiteAddress1": "COURTHOUSE","jobsiteCity": "FAYETTEVILLE","jobsiteState": "NC","customerJobId": "7","startDateTime": "2017-10-12T14:48:00","endDateTime": "2017-10-14T13:17:00","delivery": "N","pickup": "A","approverGuid": "","approverName": "","createdGuid": "0e35d520-1297-d44f-8bc1-3a9ae944f648","createdName": "RYAN MOTT","timeStamp": "2017-09-11 15:51:54.668000","equipmentId": "","miscChg": "0","transChg": "0","make": "","model": "","serial": "","alias": "","catClass": "903-0071","catclassDesc": "SKID STEER LOADER 1700-1899#","qty": "1","outsourced": "","equipmentType": "","remainingQty": "1","scheduledPickupQty": "0","reservationQuoteId": "","quoteId": "","seqNum": "0","lineSeq": "0","lineNum": "3","custOwn": "","coeStatusCode": "","bulk": false,"meter": "0","dayRate": "250.00","weekRate": "741.00","monthRate": "466.00","totalAmount": "0","vendorName": ""}],"cached": false,"recordCount": 3}';
    
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $processedResults = $endpoint->read($data);
    
    foreach ($processedResults as $requisition) {
      $this->assertInstanceOf(Order::class, $requisition);
    }
  }
  
  public function testUpdateRequisition() {
    $data = [
      'requestId' => 'A001024374',
      'timeStamp' => '2017-11-27 15:56:42.376450',
      'statusCode' => 'RS',
      'startDateTime' => '2018-01-24T13:00:00',
      'endDateTime' => '2018-01-26T13:00:00',
      'updateType' => '',
    ];
    
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": [],"result": {"id": "A001024374"},"cached": false}';
  
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\RequisitionsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserEmail('urvml2017.1@gmail.com');
    
    $status = $endpoint->update($data);
    $this->assertEquals(0, $status->status);
  }
}