<?php
namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationRequisitionCodeTitlesEndpoint;
use Drupal\ur_appliance\Data\DAL\AllocationCode;
use Drupal\ur_appliance\Test\BaseTest;

class AllocationRequisitionCodeTitlesTest extends BaseTest {
  protected $uri = 'customer/allocationrequisitioncodetitles';
  protected $method = 'GET';
  protected $allowedMethods = ['GET'];

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/allocationrequisitioncodetitles';
    $this->setClassUnderTest(AllocationRequisitionCodeTitlesEndpoint::class);
  }

  /**
   * Ensure we are returned a list of allocation requisition codes with mock data.
   */
  public function testRead() {
    $accountId = '1278567';
    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
    $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"accountId":["1278567"],"codeType":""},"result":[{"accountId":"1278567","codeType":"A","id":"1","sequenceNum":"1","title":"BUSINESS UNIT","required":"N","validated":"N","validatedCnt":"2"},{"accountId":"1278567","codeType":"A","id":"2","sequenceNum":"2","title":"SAP PROJECT #","required":"N","validated":"N","validatedCnt":"0"},{"accountId":"1278567","codeType":"A","id":"3","sequenceNum":"3","title":"MWO #","required":"N","validated":"N","validatedCnt":"0"},{"accountId":"1278567","codeType":"A","id":"4","sequenceNum":"4","title":"PO #","required":"Y","validated":"Y","validatedCnt":"3"},{"accountId":"1278567","codeType":"A","id":"5","sequenceNum":"5","title":"LINE NUMBER","required":"N","validated":"Y","validatedCnt":"0"},{"accountId":"1278567","codeType":"A","id":"6","sequenceNum":"6","title":"COMMENTS","required":"N","validated":"N","validatedCnt":"2"},{"accountId":"1278567","codeType":"R","id":"1","sequenceNum":"1","title":"TEST REQ FIELD","required":"Y","validated":"Y","validatedCnt":"3"},{"accountId":"1278567","codeType":"R","id":"2","sequenceNum":"2","title":"TEST REQ FIELD 2","required":"Y","validated":"N","validatedCnt":"3"},{"accountId":"1278567","codeType":"R","id":"3","sequenceNum":"3","title":"TEST REQ FIELD 3","required":"N","validated":"N","validatedCnt":"0"}],"cached":false,"recordCount":9}';
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserToken($token);

    $codes = $endpoint->readAll(['accountId' => $accountId]);

    foreach ($codes as $code) {
      $this->assertInstanceOf(AllocationCode::class, $code);
    }
  }
}
