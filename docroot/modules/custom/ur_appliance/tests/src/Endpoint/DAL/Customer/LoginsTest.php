<?php
/**
 * Created by PhpStorm.
 * User: patrickthurmond
 * Date: 6/4/17
 * Time: 12:27 AM
 */

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Customer\LoginsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class LoginsTest extends BaseTest {
  protected $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"login":"ur.vml.5@gmail.com","password":"********"},"result":{"guid":"67cd599c-1df7-0348-80bc-04087b296cf3","email":"ur.vml.5@gmail.com"},"cached":false}';

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/logins';
    $this->setClassUnderTest(LoginsEndpoint::class);
  }

  /**
   * Ensure we are returned a rate object.
   */
  public function testRead() {
    $data = [
      'login' => 'ur.vml.5@gmail.com',
      'password' => 'URvml2016'
    ];

    $endpoint = $this->getSystemUnderTest();
    $result = $endpoint->read($data);

    $this->assertTrue($result);
  }

  public function testFailedRead() {
    $data = [
      'login' => 'ur.vml.5@gmail.com',
      'password' => 'URvml2016bad'
    ];

    $rawResponseToMock = '{"status":1,"messageId":"W000005","messageText":"Login failed","request":{"login":"ur.vml.5@gmail.com","password":"********"},"result":{"message":"Failed"},"cached":false}';

    $this->systemUnderTest = null;

    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $result = $endpoint->read($data);

    $this->assertFalse($result);
  }

  /**
   * Test empty call data.
   *
   * @expectedException \Drupal\ur_appliance\Exception\ResponseParseException
   * @expectedExceptionMessage JSON Parse Failure
   */
  public function testReadResponseException() {
    $data = [
      'login' => 'ur.vml.5@gmail.com',
      'password' => 'URvml2016bad'
    ];

    $this->systemUnderTest = null;
    $endpoint = $this->getSystemUnderTest('Bad Response');
    $result = $endpoint->read($data);
  }

  /**
   * Test empty call data.
   *
   * @expectedException \Drupal\ur_appliance\Exception\DAL\DalException
   * @expectedExceptionMessage Unable to login without username and password.
   */
  public function testReadException() {
    $data = [];
    $endpoint = $this->getSystemUnderTest();
    $result = $endpoint->read($data);
  }
}
