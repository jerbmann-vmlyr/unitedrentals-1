<?php
namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Data\DAL\AllocationCode;
use Drupal\ur_appliance\Data\DAL\RequisitionCode;
use Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationRequisitionCodeTitlesEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class RequisitionCodesTest extends BaseTest {
  protected $uri = 'customer/allocationrequisitioncodetitles';
  protected $allowedMethods = ['GET'];

  public function setUp() {
    parent::setUp();
    
    $this->setClassUnderTest(AllocationRequisitionCodeTitlesEndpoint::class);
    $this->getSystemUnderTest();
  }

  /**
   * Ensure we are returned a recentRentalItem object with live request.
   *
   * @group live
   */
  public function testReadLive() {
    /**
     * Recent token changes mean that you can now get the user token to use in
     * our testing. You just have to set their email address.
     * $adapter->setUserEmail('someEmail@example.com');
     */
    $data = ['accountId' => '5174135']; // A valid accountId

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationRequisitionCodeTitlesEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest(null, true, true);
    $endpoint->getAdapter()->setUserEmail('urvml2017.1@gmail.com');
    
    $reqCodes = $endpoint->readAll($data);

    foreach ($reqCodes as $requisitionCodes) {
      print_r($requisitionCodes);
      $this->assertInstanceOf(RequisitionCode::class, $requisitionCodes);
    }
  }
}