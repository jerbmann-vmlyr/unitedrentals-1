<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Data\DAL\Item;
use Drupal\ur_appliance\Endpoint\DAL\Customer\AccountsEndpoint;
use Drupal\ur_appliance\Endpoint\DAL\Customer\EquipmentEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class EquipmentTest extends BaseTest {
  protected $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"accountId":["858702"],"jobId":[],"catClass":[],"alias":[],"equipmentId":[],"catalogGroup":null,"description":"","includeDisabled":false},"result":{"858702":[{"accountId":"858702","equipmentId":"","urEquipmentId":"","jobId":"48","jobName":"CNRL HORIZON","customerJobId":"","jobsiteAddress1":"CNRL","jobsiteCity":"FORT MCMURRAY","jobsiteState":"AB","jobsiteZip":"T9H","jobsitePhone":"403-517-6700","catClass":"100-2130","description":"COMPRESSOR 11-12.9 CFM GAS","catalogGroup":"1010","imageFile":"https:\/\/images.ur.com\/catclass\/100-2130.jpg","alias":"","bulk":true,"quantity":"8","make":"","model":"","year":"0","serial":"","meter":"0","rate":"","wkrt":"","mort":"","eqpType":"","status":"","active":true,"lastMaintainedBy":"TC-NAXLE","lastMaintainedDateTime":"2017-04-25T16:30:55","simId":"","simActive":"","requestId":"","qtyAvailable":"8","qtyInUse":"0","qtyInPickup":"0","approverGuid":"","approverName":"","requesterGuid":"","requesterName":"","createdGuid":"","createdName":"","po":"","startDateTime":null,"returnDateTime":null,"pickupDateTime":null,"acctCode1":"","acctCode2":"","acctCode3":"","acctCode4":"","acctCode5":"","acctCode6":"","reqCode1":"","reqCode2":"","reqCode3":"","reqCode4":""},{"accountId":"858702","equipmentId":"H0498","urEquipmentId":"COE412","jobId":"48","jobName":"CNRL HORIZON","customerJobId":"","jobsiteAddress1":"CNRL","jobsiteCity":"FORT MCMURRAY","jobsiteState":"AB","jobsiteZip":"T9H","jobsitePhone":"403-517-6700","catClass":"975-1400","description":"WELDER ARC 400 AMP DIESEL","catalogGroup":"7030","imageFile":"https:\/\/images.ur.com\/catclass\/975-1400.jpg","alias":"","bulk":false,"quantity":"1","make":"LINCOLN","model":"VANTAGE 4","year":"0","serial":"U1081006754","meter":"0","rate":"0","wkrt":"0","mort":"0","eqpType":"C","status":"I","active":true,"lastMaintainedBy":"TFORBIS","lastMaintainedDateTime":"2016-10-09T15:08:21","simId":"","simActive":"","requestId":"A000429171","qtyAvailable":"0","qtyInUse":"0","qtyInPickup":"0","approverGuid":"51661224-3e8b-824b-a9d5-13f199eaa305","approverName":"","requesterGuid":"cc7ffd5e-575d-234c-9aa4-b4b5e6cbef71","requesterName":"","createdGuid":"cf3a778f-87ce-e848-bd0b-479b82a15f61","createdName":"","po":"1000775","startDateTime":"2016-04-23T00:15:52","returnDateTime":"2018-12-31T00:15:52","pickupDateTime":null,"acctCode1":"692387","acctCode2":"BITUMEN PRODUCTION","acctCode3":"RODNEY BEST","acctCode4":"","acctCode5":"","acctCode6":"","reqCode1":"","reqCode2":"","reqCode3":"","reqCode4":""}]},"cached":false,"recordCount":2}';

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/equipment';
    $this->setClassUnderTest(EquipmentEndpoint::class);
  }

  public function testRead() {
    $accountIds = '858702';
    $data['accountId'] = $accountIds;

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\EquipmentEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest( $this->getRawResponseToMock() );
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $itemList = $endpoint->read($data);

    $this->assertInstanceOf(Item::class, $itemList[0]);
  }

  /**
   * @expectedException \Drupal\ur_appliance\Exception\NoResultsException
   * @expectedExceptionMessage No data returned.
   */
  public function testReadNoResults() {
    $mockResponse = '{"status":1,"messageId":"W000001","messageText":"No Records Found","request":{"accountId":["713"],"jobId":[],"catClass":[],"alias":[],"equipmentId":[],"catalogGroup":null,"description":"","includeDisabled":false},"result":[],"cached":false,"recordCount":0}';
    $accountIds = '713';
    $data['accountId'] = $accountIds;

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\EquipmentEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest( $mockResponse );
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $itemList = $endpoint->read($data);
  }

  /**
   * @group live
   */
  public function testReadLive() {
    /**
     * Sample Mass List of Accounts:
     *    8,34,713,11217,12345,14178,50343,56225,67857,68655,75719,130086,295386,617778,770993,878436,1029707,1093013,1099179,1125636,1154709,1190901,1220341,1262244,1270066,1278567,1278717,1356414,3145609,3196751,3196754,3196755,3196756,3196757,3196758,4032360,4032362,7205489,8020087,8308315
     */
    $accountIds = '858702';
    $data['accountId'] = $accountIds;

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\EquipmentEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest(null, true);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $itemList = $endpoint->read($data);

    $this->assertInstanceOf(Item::class, $itemList[0]);
  }
}