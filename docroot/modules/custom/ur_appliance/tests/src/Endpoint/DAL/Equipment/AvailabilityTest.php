<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Equipment;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Data\DAL\AvailableEquipment;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Endpoint\DAL\Equipment\AvailabilityEndpoint;
use Drupal\ur_appliance\Test\BaseTest;


class AvailabilityTest extends BaseTest {
  protected $uri = 'equipment/availability';
  protected $method = 'GET';
  protected $allowedMethods = ['GET'];

  public function setUp() {
    parent::setUp();
    $this->uri = 'equipment/availability';
    $this->setClassUnderTest(AvailabilityEndpoint::class);
    $this->getSystemUnderTest();
  }

  /**
   * Ensure we are returned a availableEquipments object with live request.
   *
   * @group live
   * @TODO Update to follow newer testing structure standards
   */
  public function testReadLive() {
    $adapter = ApiContainer::getService('dal_adapter');
    $endpoint = new AvailabilityEndpoint($adapter);
    $endpoint->getAdapter()->setUserToken($this->defaultToken);

    $data = ['catClass' => ['231-1205', '031-1000'], 'branchId' => '122'];
    $available_equipments = $endpoint->read($data);
    
    foreach ($available_equipments as $available_equipment) {
      $this->assertInstanceOf(AvailableEquipment::class, $available_equipment);
    }
  }
  
  /**
   * Ensure we are returned a availableEquipments object with live request.
   *
   * @group live
   * @TODO Update to follow newer testing structure standards
   */
  public function testReadLiveWithDetail() {
    $adapter = ApiContainer::getService('dal_adapter');
    $endpoint = new AvailabilityEndpoint($adapter);
    $data = ['catClass' => ['231-1205', '031-1000'], 'branchId' => '122', 'detail' => true];
    $available_equipments = $endpoint->read($data);
    
    foreach ($available_equipments as $available_equipment) {
      $this->assertInstanceOf(AvailableEquipment::class, $available_equipment);
    }
  }
  
  /**
   * Ensure we are returned a availableEquipments object with mock data.
   */
  public function testReadMockNoDetail() {
    $data = ['catClass' => ['231-1205', '031-1000'], 'branchId' => '122'];
    $this->rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"branchId": ["281"],"catClass": ["231-1205","031-1000"],"aDate": "","viewType": ""},"result": [{"branchId": "281","locQty": "0","zoneQty": "8"}],"cached": false,"recordCount": 1}';

    /** @var AvailabilityEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest(null, false, true);
    $endpoint->getAdapter()->setUserToken($this->defaultToken);

    $available_equipments = $endpoint->read($data);
    
    foreach ($available_equipments as $available_equipment) {
      $this->assertInstanceOf(AvailableEquipment::class, $available_equipment);
    }
  }
  
  /**
   * Ensure we are returned a availableEquipments object with mock data.
   */
  public function testReadMockWithDetail() {
    $data = ['catClass' => ['231-1205', '031-1000'], 'branchId' => '279', 'detail' => true];
    $this->rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,
    "request": {"branchId": ["279"],"catClass": ["231-1205"],"aDate": "","viewType": ""},
    "result": [
      {"branchId": "H35","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"},
      {"branchId": "N59","day1": "-2","day2": "-2","day3": "-2","day4": "-2","day5": "-2","day6": "-2","day7": "-2"},
      {"branchId": "N61","day1": "5","day2": "5","day3": "5","day4": "5","day5": "5","day6": "5","day7": "5"},
      {"branchId": "N62","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "1","day7": "1"},
      {"branchId": "N65","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "1","day7": "1"},
      {"branchId": "N75","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"},
      {"branchId": "Z76","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "1","day7": "1"},
      {"branchId": "054","day1": "2","day2": "2","day3": "2","day4": "2","day5": "2","day6": "2","day7": "2"},
      {"branchId": "057","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"},
      {"branchId": "122","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "0","day7": "0"},
      {"branchId": "279","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "1","day7": "1"},
      {"branchId": "280","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"},
      {"branchId": "281","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"},
      {"branchId": "805","day1": "-5","day2": "-5","day3": "-5","day4": "-4","day5": "-4","day6": "-4","day7": "-4"},
      {"branchId": "808","day1": "2","day2": "2","day3": "2","day4": "2","day5": "2","day6": "2","day7": "2"},
      {"branchId": "921","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "1","day7": "1"},
      {"branchId": "992","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"}
      ],"cached": false,"recordCount": 17}';
    $endpoint = $this->getSystemUnderTest(null, false, true);
    $endpoint->getAdapter()->setUserToken($this->defaultToken);

    $available_equipments = $endpoint->read($data);
    
    foreach ($available_equipments as $available_equipment) {
      $this->assertInstanceOf(AvailableEquipment::class, $available_equipment);
    }
  }
  
  
  /**
   * Ensure we are returned an array object.
   */
  public function testImportNoDetail() {
    //Create variable with result data
    $data = json_decode('{"status": 0,"messageId": null,"messageText": null,"request": {"branchId": ["122"],"catClass": ["231-1205","031-1000"],"aDate": "","viewType": ""},"result": [{"branchId": "122","locQty": "1","zoneQty": "8"}],"cached": false,"recordCount": 1}');
    
    //Test function import
    $endpoint = $this->getSystemUnderTest();
    $processedResults = $endpoint->import($data);
    
    //Validate return is an array
    $this->assertInternalType('array', $processedResults);
    
    //Validate foreach rate is an object for the class Rate
    foreach ($processedResults as $availableEquipment) {
      $this->assertInstanceOf(AvailableEquipment::class, $availableEquipment);
    }
    
    //Validate 1 element in the response
    $this->assertCount(1, $processedResults);
  }

  /**
   * Ensure we are returned an array object.
   */
  public function testImportWithDetail() {
    //Create variable with result data
    $data = json_decode('{"status": 0,"messageId": null,"messageText": null,
    "request": {"branchId": ["279"],"catClass": ["231-1205"],"aDate": "","viewType": ""},
    "result": [
      {"branchId": "H35","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"},
      {"branchId": "N59","day1": "-2","day2": "-2","day3": "-2","day4": "-2","day5": "-2","day6": "-2","day7": "-2"},
      {"branchId": "N61","day1": "5","day2": "5","day3": "5","day4": "5","day5": "5","day6": "5","day7": "5"},
      {"branchId": "N62","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "1","day7": "1"},
      {"branchId": "N65","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "1","day7": "1"},
      {"branchId": "N75","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"},
      {"branchId": "Z76","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "1","day7": "1"},
      {"branchId": "054","day1": "2","day2": "2","day3": "2","day4": "2","day5": "2","day6": "2","day7": "2"},
      {"branchId": "057","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"},
      {"branchId": "122","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "0","day7": "0"},
      {"branchId": "279","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "1","day7": "1"},
      {"branchId": "280","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"},
      {"branchId": "281","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"},
      {"branchId": "805","day1": "-5","day2": "-5","day3": "-5","day4": "-4","day5": "-4","day6": "-4","day7": "-4"},
      {"branchId": "808","day1": "2","day2": "2","day3": "2","day4": "2","day5": "2","day6": "2","day7": "2"},
      {"branchId": "921","day1": "1","day2": "1","day3": "1","day4": "1","day5": "1","day6": "1","day7": "1"},
      {"branchId": "992","day1": "0","day2": "0","day3": "0","day4": "0","day5": "0","day6": "0","day7": "0"}
      ],"cached": false,"recordCount": 17}');
  
    //Test function import
    $endpoint = $this->getSystemUnderTest();
    $processedResults = $endpoint->import($data);
  
    //Validate return is an array
    $this->assertInternalType('array', $processedResults);
  
    //Validate foreach rate is an object for the class Rate
    foreach ($processedResults as $availableEquipment) {
      $this->assertInstanceOf(AvailableEquipment::class, $availableEquipment);
    }
  
    //Validate 1 element in the response
    $this->assertCount(17, $processedResults);
  }
  
  /**
   * Ensure not php error without and availableEquipment in response.
   *
   * @expectedException \Drupal\ur_appliance\Exception\EndpointException
   */
  public function testImportUnhappyWithAObjectEmpty() {
    //Create object with result data
    $object = new \stdClass();
    $object->result = json_decode('');
    
    //Test function import
    $endpoint = $this->getSystemUnderTest();
    $processedResults = $endpoint->import($object);
    
    //Validate return is null
    $this->assertNull($processedResults);
  }
}