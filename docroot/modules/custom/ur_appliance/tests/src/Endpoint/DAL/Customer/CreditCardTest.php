<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use ClassesWithParents\D;
use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Data\DAL\CreditCard;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Customer\CreditCardsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class CreditCardTest extends BaseTest {
  protected $uri = 'customer/creditcards';
  protected $method = 'GET';
  protected $allowedMethods = ['GET', 'POST', 'PUT', 'DELETE'];

  public function setUp() {
    parent::setUp();
    
    $this->uri = 'customer/creditcards';
    $this->setClassUnderTest(CreditCardsEndpoint::class);
  }
  
  /**
   * Ensure we are returned a creditCard object with mock data.
   */
  public function testReadMock() {
    $data = 'Z000001005';
    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"id": "Z000001005","guid": null},"result": [{"cardId": "Z000001005","cardHolder": "Theresa Slackimus","cardType": "V","ccNum4": "1111","expireDate": 1220,"defaultCard": "Y","cardName": "Wife edition","status": "A","accountId": 0}],"cached": false}';
    
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserToken($token);
    
    $credit_cards = $endpoint->read($data);
    
    foreach ($credit_cards as $credit_card) {
      $this->assertInstanceOf(CreditCard::class, $credit_card);
    }
  }
  

  /**
   * Ensure we are returned all creditCards in an object with mock data.
   */
  public function testReadAllMock() {
    $data = [];
    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"guid": "","accountId": "","default": ""},"result": [{"cardId": "A000000105","cardHolder": "Maximus Slackimus","cardType": "V","ccNum4": "1118","expireDate": 1126,"defaultCard": "N","cardName": "Magic Infinite Card","status": "A","accountId": 0},{"cardId": "Z000000642","cardHolder": "Eric Brewer","cardType": "V","ccNum4": "1881","expireDate": 118,"defaultCard": "N","cardName": "Brewer Card 2","status": "A","accountId": 0},{"cardId": "Z000001005","cardHolder": "Theresa Slackimus","cardType": "V","ccNum4": "1111","expireDate": 1220,"defaultCard": "Y","cardName": "Wife edition","status": "A","accountId": 0}],"cached": false}';
  
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserToken($token);
    
    $credit_cards = $endpoint->readAll($data);

    foreach ($credit_cards as $credit_card) {
      $this->assertInstanceOf(CreditCard::class, $credit_card);
    }
  }

  /**
   * Test retrieving credit card ID after creating a card
   *
   * @TODO: Do this the systematic way.
   */
  public function testCreateCreditCard() {
    $data = [
      'guid' => '',
      'cardHolder' => 'Theresa Slackimus',
      'cardType' => 'V',
      'ccNumber' => '4111111111111111',
      'expireDate' => '1220',
      'cardName' => 'Unit Test Data',
      'defaultCard' => 'y',
      'status' => '',
      'accountId' => '',
      'cvv' => ''
    ];
    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
  
    $this->rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"guid": "","cardHolder": "Theresa Slackimus","ccType": "V","ccNumber": "****************","expireDate": "****","cardName": "Wife edition","defaultCard": "Y","status": "","accountId": "","cvv": "****"},"result": {"cardId": "A000000111"},"cached": false}';
  
    $logger = $this->getMockLogger();
    $client = $this->getMockClient();
    $adapter = new DalAdapter($logger, $client);
    $adapter->setResponse($this->getMockResponse());
    $endpoint = new CreditCardsEndpoint($adapter);
    $endpoint->getAdapter()->setUserToken($token);
    $returnedCardId = $endpoint->create($data);
    $this->assertEquals('A000000111', $returnedCardId);
  }
  
  /**
   * Test retrieving credit card ID after updating a card
   *
   * @TODO: Do this the systematic way.
   */
  public function testUpdateCreditCard() {
    $data = [
      'guid' => '',
      'cardId' => 'A000000111',
      'cardHolder' => 'Hammond B Armstrong',
      'cardType' => 'V',
      'ccNumber' => '4111111111111111',
      'expireDate' => '0323',
      'cardName' => 'Zensure Bill',
      'defaultCard' => 'y',
      'status' => '',
      'accountId' => '',
      'cvv' => ''
    ];
    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
    
    $this->rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"cardId": "A000000111","guid": "","cardHolder": "Hammond B Armstrong","expireDate": "****","cardName": "Zensure Bill","defaultCard": "Y","cvv": "****"},"result": [],"cached": false}';
    
    $logger = $this->getMockLogger();
    $client = $this->getMockClient();
    $adapter = new DalAdapter($logger, $client);
    $adapter->setResponse($this->getMockResponse());
    $endpoint = new CreditCardsEndpoint($adapter);
    $endpoint->getAdapter()->setUserToken($token);
    $updatedCardId = $endpoint->update($data);
    $this->assertEquals('A000000111', $updatedCardId);
  }
}