<?php
namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Data\DAL\Jobsite;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Customer\JobsitesEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class JobsitesTest extends BaseTest {
  protected $uri = 'customer/jobsites';
  protected $allowedMethods = ['GET', 'POST'];

  public function setUp() {
    parent::setUp();
    
    $this->uri = 'customer/jobsites';
    $this->setClassUnderTest(JobsitesEndpoint::class);
    $this->getSystemUnderTest();
  }

  /**
   * Ensure we are returned a recentRentalItem object with live request.
   *
   * @group live
   */
  public function testReadLive() {
    /**
     * Recent token changes mean that you can now get the user token to use in
     * our testing. You just have to set their email address.
     * $adapter->setUserEmail('someEmail@example.com');
     */
    $data = ['accountId' => '18']; // A valid accountId

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\JobsitesEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest(null, true, true);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    
    $jobsites = $endpoint->read($data);

    foreach ($jobsites as $jobsite) {
      $this->assertInstanceOf(Jobsite::class, $jobsite);
    }
  }
  
  /**
   * Test retrieving status after creating a jobsite
   *    Using wasSuccessful() which returns bool result
   *      0 => Success => True
   *      1 => Error => False
   */
  public function testCreateJobSite() {
    $data = [
      'jobId' => '1',
      'accountId' => '1069997',
      'name' => 'New Job Site Test',
      'address1' => '2020 Baltimore Ave',
      'city' => 'Kansas City',
      'state' => 'MO',
      'zip' => '64108',
      'contact' => 'Ryan Mott',
      'mobilePhone' => '816-218-2947',
      'email' => 'ryan.mott@vml.com',
      'notes' => 'use the south loading dock'
    ];
    
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"userid": "WVML","customerJobId": "","name": "New Job Site Test","address2": "","email": "ryan.mott@vml.com","status": "A","defaultDelivery": false,"tractorTrailer": false,"loadingDockRequired": false,"heightRestriction": "0","timeRestriction": "","notes": "","latitude": 0,"longitude": 0,"accountId": "1069997","address1": "2020 Baltimore Ave","city": "Kansas City","state": "MO","zip": "64108","contact": "Ryan Mott","mobilePhone": "816-218-2947","id": "1"},"result": [],"cached": false}';

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\JobsitesEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    
    $method = 'POST';
    $status = $endpoint->createOrUpdate($data, $method);
    $this->assertNotNull($status);
  }
  
  /**
   * Test retrieving status after creating a jobsite
   *    Using wasSuccessful() which returns bool result
   *      0 => Success => True
   *      1 => Error => False
   */
  public function testUpdateJobSite() {
    $data = [
      'jobId' => '15',
      'accountId' => '1069997',
      'name' => 'New VML update H Jobsite',
      'address1' => '2020 Baltimore Ave',
      'city' => 'Kansas City',
      'state' => 'MO',
      'zip' => '64108',
      'contact' => 'Ryan Mott',
      'mobilePhone' => '816-218-2947',
      'email' => 'ryan.mott@vml.com',
      'notes' => 'use the south loading dock'
    ];
    
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"userid": "WVML","customerJobId": "","name": "New VML update H Jobsite","address2": "","email": "ryan.mott@vml.com","status": "A","defaultDelivery": false,"tractorTrailer": false,"loadingDockRequired": false,"heightRestriction": "0","timeRestriction": "","notes": "","latitude": 0,"longitude": 0,"accountId": "1069997","address1": "2020 Baltimore Ave","city": "Kansas City","state": "MO","zip": "64108","contact": "Ryan Mott","mobilePhone": "816-218-2947","id": "15"},"result": [],"cached": false}';

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\JobsitesEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    
    $method = 'PUT';
    $status = $endpoint->createOrUpdate($data, $method);
    $this->assertNotNull($status);
  }
}