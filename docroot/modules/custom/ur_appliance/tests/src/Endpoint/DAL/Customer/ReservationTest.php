<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Data\DAL\Reservation as ReservationData;
use Drupal\ur_appliance\Endpoint\DAL\Customer\OrderStatusEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class ReservationTest extends BaseTest {
  
  protected $systemUnderTest;
  protected $baseUrl = 'https://testmobiledal.ur.com';
  protected $uri = 'customer/orderstatus';
  protected $method = 'GET';
  protected $allowedMethods = ['GET'];
  
  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/orderstatus';
    $this->rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"startRecord": 1,"maxRecords": 0,"openQuotes": "I","openRsvReq": "I","guid": "0e35d520-1297-d44f-8bc1-3a9ae944f648"},"result": [{"accountId": "1075311","requisitionId": "A000880245","requisitionCancelled": "N","reservationId": "149035742","quoteId": "","startDate": "08-09-2017","jobId": "8","jobName": "DFW LANDFILL","createdDate": "08-07-2017","orderStatus": "Scheduled","orderStatusCode": "SCD"}],"cached": false,"recordCount": 1,"startRecord": 1,"endRecord": 1,"moreRecords": false}';

    $this->setUri($this->uri);
    $this->setClassUnderTest(OrderStatusEndpoint::class);
  }
  
  public function testReadMock() {
    $data = ['guid' => '0e35d520-1297-d44f-8bc1-3a9ae944f648'];
    
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\OrderStatusEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $reservation_statuses = $endpoint->read($data);
  
    foreach ($reservation_statuses as $reservation_status) {
      $this->assertInstanceOf(ReservationData::class, $reservation_status);
    }
  }
}