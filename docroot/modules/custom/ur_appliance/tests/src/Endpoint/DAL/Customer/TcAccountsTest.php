<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Endpoint\DAL\Customer\TcAccountsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;


class TcAccountsTest extends BaseTest {
  protected $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"guid": ["a51fa8f2-36d3-7c45-8b4d-c51dc3cfffdc"],"accountId": ["1"]},"result": [{"a51fa8f2-36d3-7c45-8b4d-c51dc3cfffdc": {"1": {"location": "263","employeeId": "","corporateLink": false,"customerAdmin": true,"requestor": true,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "URTEST18","lastMaintainedDateTime": "2016-01-29T14:13:31","defaultJob": ""}}}],"cached": true}';

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/tcaccounts';
    $this->setClassUnderTest(TcAccountsEndpoint::class);
    $this->getSystemUnderTest();
  }
  
  /**
   * Test retrieving status result after linking account to the user.
   *      * Result data
   *    true  => Success
   *    false => Error
   */
  public function testLinkAccount() {
    $data = [
      'accountId' => '1',
      'guid' => 'a51fa8f2-36d3-7c45-8b4d-c51dc3cfffdc'
    ];

    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"guid": ["a51fa8f2-36d3-7c45-8b4d-c51dc3cfffdc"],"accountId": ["1"]},"result": [{"a51fa8f2-36d3-7c45-8b4d-c51dc3cfffdc": {"1": {"location": "263","employeeId": "","corporateLink": false,"customerAdmin": true,"requestor": true,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "URTEST18","lastMaintainedDateTime": "2016-01-29T14:13:31","defaultJob": ""}}}],"cached": true}';

    $endpoint = $this->getSystemUnderTest($rawResponseToMock);

    $status = $endpoint->post($data);
    $this->assertTrue($status);
  }
}