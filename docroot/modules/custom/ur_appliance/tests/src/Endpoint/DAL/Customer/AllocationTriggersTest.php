<?php
namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Endpoint\DAL\Customer\AllocationTriggersEndpoint;
use Drupal\ur_appliance\Data\DAL\CodeTrigger;
use Drupal\ur_appliance\Test\BaseTest;

class AllocationTriggersTest extends BaseTest {
  protected $uri = 'customer/allocationtriggers';
  protected $method = 'GET';
  protected $allowedMethods = ['GET'];

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/allocationtriggers';
    $this->setClassUnderTest(AllocationTriggersEndpoint::class);
  }

  /**
   * Ensure we are returned a list of allocation requisition code values with mock data.
   */
  public function testRead() {
    $accountId = '1278567';
    $codeType = 'A';

    $token = 'a6cb00299ee636f72c2f66aad671332cc29a0a5d';
    $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"accountId":"1278567","codeType":"A","idNum":""},"result":[{"accountId":"1278567","codeType":"A","idNum":"4","val":"ABCD","triggerId":"6","triggerName":"COMMENTS","triggerVal":"10"}],"cached":false,"recordCount":1}';
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserToken($token);

    $codeValues = $endpoint->readAll(['accountId' => $accountId, 'codeType' => $codeType]);

    foreach ($codeValues as $codeValue) {
      $this->assertInstanceOf(CodeTrigger::class, $codeValue);
    }
  }
}
