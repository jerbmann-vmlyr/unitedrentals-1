<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Data\DAL\Item;
use Drupal\ur_appliance\Endpoint\DAL\Gps\AempTelematicsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

/**
 * Class AempTelematicsTest
 *
 * @package Drupal\ur_appliance\Test\Endpoint\DAL\Customer
 */
class AempTelematicsTest extends BaseTest {
  protected $rawResponseToMock = '';

  public function setUp() {
    parent::setUp();
    $this->uri = 'gps/aemptelematics';
    $this->setClassUnderTest(AempTelematicsEndpoint::class);
  }

  public function loadTestResponse($filename = 'aemp.xml') {
    $file = getcwd() . "/tests/data/Endpoint/DAL/Gps/$filename";

    if (!file_exists($file)) {
      // Attempt to load from the root directory
      $file = getcwd() . "/../../../../data/Endpoint/DAL/Gps/$filename";
    }

    if (file_exists($file)) {
      return file_get_contents($file);
    }

    $this->fail('Unable to load the test file.');
    return false;
  }

  public function testRead() {
    $data = [
      'accountId' => '858702',
      'fromDate' => '20170901',
      'toDate' => '20170930',
    ];

    $response = $this->loadTestResponse();

    if ($response) {
      /** @var AempTelematicsEndpoint $endpoint */
      $endpoint = $this->getSystemUnderTest( $response );

      $endpoint->getAdapter()->processAsXML();
      $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
      $endpoint->getAdapter()->setUserToken(
        '0cd38f265d2d56607bd84d525d2c6b9476e199b2'
      );

      $itemList = $endpoint->read($data);

      $this->assertInstanceOf(Item::class, $itemList[0]);
    }
  }

  public function testReadWithHistory() {
    $data = [
      'accountId' => '858702',
      'fromDate' => '20170901',
      'toDate' => '20170910',
      'includeHistory' => 'Y',
      'includeChildren' => 'N'
    ];

    $response = $this->loadTestResponse('aemp_with_history.xml');

    if ($response) {
      /** @var AempTelematicsEndpoint $endpoint */
      $endpoint = $this->getSystemUnderTest( $response );

      $endpoint->getAdapter()->processAsXML();
      $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
      $endpoint->getAdapter()->setUserToken(
        '0cd38f265d2d56607bd84d525d2c6b9476e199b2'
      );

      $itemList = $endpoint->read($data);

      $this->assertInstanceOf(Item::class, $itemList[0]);
    }
  }

  /**
   * @expectedException \Drupal\ur_appliance\Exception\NoResultsException
   * @expectedExceptionMessage No results found.
   */
  public function testReadNoResults() {
    $mockResponse = 'No records found matching search criteria';

    $data = [
      'accountId' => '34',
      'fromDate' => '20170901',
      'toDate' => '20170930',
    ];

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\EquipmentEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest( $mockResponse );

    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $endpoint->getAdapter()->setUserToken(
      '0cd38f265d2d56607bd84d525d2c6b9476e199b2'
    );

    $itemList = $endpoint->read($data);
  }

  /**
   * @group live
   */
  public function testReadLive() {
    /**
     * Sample Mass List of Accounts:
     *    8,34,713,11217,12345,14178,50343,56225,67857,68655,75719,130086,295386,617778,770993,878436,1029707,1093013,1099179,1125636,1154709,1190901,1220341,1262244,1270066,1278567,1278717,1356414,3145609,3196751,3196754,3196755,3196756,3196757,3196758,4032360,4032362,7205489,8020087,8308315
     */
    $data = [
      'accountId' => '858702',
      'fromDate' => '20170901',
      'toDate' => '20170930',
    ];

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\EquipmentEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest(null, true);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');

    $itemList = $endpoint->read($data);

    $this->assertInstanceOf(Item::class, $itemList[0]);
  }
}