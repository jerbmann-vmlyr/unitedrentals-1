<?php
namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Data\DAL\TcUser;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Endpoint\DAL\Customer\TcUsersEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class TcUserTest extends BaseTest {
  protected $systemUnderTest;
  protected $baseUrl = 'https://testmobiledal.ur.com';
  protected $uri = 'customer/tcusers';
  protected $method = 'GET';
  protected $allowedMethods = ['GET'];

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/tcusers';
    $this->setClassUnderTest(TcUsersEndpoint::class);
    $this->getSystemUnderTest();
  }

  /**
   * Ensure we are returned a tcuser object with live request.
   *
   * @group live
   */
  public function testReadLive() {
    // Recent token changes mean that you can now get the user token to use in
    // our testing. You just have to set their email address.
    // $adapter->setUserEmail('someEmail@example.com');

    $adapter = ApiContainer::getService('dal_adapter');
    $endpoint = new TcUsersEndpoint($adapter);
    $data = 'ur.vml.7@gmail.com';

    $endpoint->getAdapter()->setUserEmail($data);
    $tcusers = $endpoint->read($data);
    $this->assertInstanceOf(TcUser::class, $tcusers);
  }

  /**
   * Ensure we are returned a tcuser object with live request.
   *
   * @group live
   */
  public function testReadSimpleLive() {
    // Recent token changes mean that you can now get the user token to use in
    // our testing. You just have to set their email address.
    // $adapter->setUserEmail('someEmail@example.com');

    $adapter = ApiContainer::getService('dal_adapter');
    $endpoint = new TcUsersEndpoint($adapter);
    $data = 'ur.vml.7@gmail.com';

    $endpoint->getAdapter()->setUserEmail($data);
    $tcusers = $endpoint->read($data);
    $this->assertInstanceOf(TcUser::class, $tcusers);
  }

  public function testReadMock() {
    $data = 'ur.vml.5@gmail.com';
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"id": "ur.vml.5@gmail.com","includeLinked": true},"result": [{"guid": "67cd599c-1df7-0348-80bc-04087b296cf3","email": "ur.vml.5@gmail.com","firstName": "RYAN","lastName": "MOTT","phone": "816-785-9271","altPhone": null,"language": "ENU","notified": false,"lastMaintainedBy": "TC-RMOTT","lastMaintainedDateTime": "2016-01-27T00:13:46","lastLoginTimestamp": "","pswdExpDays": "0","createdBy": "WURPHP","createdDateTime": "2016-01-27T00:11:52","urEmployee": false,"accounts": {"1075311": {"location": "","employeeId": "","corporateLink": false,"customerAdmin": false,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "WVML","lastMaintainedDateTime": "2016-12-13T13:30:49","level": "E"},"1406135": {"location": "","employeeId": "","corporateLink": false,"customerAdmin": false,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "WVML","lastMaintainedDateTime": "2016-10-05T12:56:57","level": "E"},"2668808": {"location": "263","employeeId": "PENDING","corporateLink": true,"customerAdmin": true,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "UR091EC1","lastMaintainedDateTime": "2017-05-17T16:47:49","level": "E"},"2878631": {"location": "","employeeId": "","corporateLink": false,"customerAdmin": false,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "WORS","lastMaintainedDateTime": "2017-04-20T09:50:00","level": "E"},"2878633": {"location": "","employeeId": "","corporateLink": false,"customerAdmin": false,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "WORS","lastMaintainedDateTime": "2017-04-20T10:12:58","level": "E"},"2878634": {"location": "","employeeId": "","corporateLink": false,"customerAdmin": false,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "WORS","lastMaintainedDateTime": "2017-04-20T10:17:16","level": "E"},"2878635": {"location": "","employeeId": "","corporateLink": false,"customerAdmin": false,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "WORS","lastMaintainedDateTime": "2017-04-20T10:22:10","level": "E"},"2878636": {"location": "","employeeId": "","corporateLink": false,"customerAdmin": false,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "WORS","lastMaintainedDateTime": "2017-04-20T10:25:22","level": "E"},"2878642": {"location": "","employeeId": "","corporateLink": false,"customerAdmin": false,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "WORS","lastMaintainedDateTime": "2017-04-20T16:04:41","level": "E"},"2878643": {"location": "","employeeId": "","corporateLink": false,"customerAdmin": false,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "WORS","lastMaintainedDateTime": "2017-04-20T16:38:39","level": "E"},"2878644": {"location": "","employeeId": "","corporateLink": false,"customerAdmin": false,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "WORS","lastMaintainedDateTime": "2017-04-20T16:39:43","level": "E"},"7205489": {"location": "","employeeId": "","corporateLink": false,"customerAdmin": false,"requestor": false,"approver": false,"reports": false,"approverEmail": "","fullName": " ","approverProxyEmail": "","lastMaintainedBy": "WURPHP","lastMaintainedDateTime": "2016-01-27T11:53:23","level": "E"}},"permissions": {"1075311": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"},"1406135": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"},"2668808": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"},"2878631": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"},"2878633": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"},"2878634": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"},"2878635": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"},"2878636": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"},"2878642": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"},"2878643": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"},"2878644": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"},"7205489": {"level": "E","AH": "AH","AL": "AL","CJ": "CJ","CO": "CO","CQ": "CQ","CS": "CS","CU": "CU","DR": "DR","ER": "ER","EU": "EU","EV": "EV","GP": "GP","IH": "IH","IN": "IN","K1": "K1","OO": "OO","PO": "PO","PS": "PS","RP": "RP","UT": "UT","VP": "VP"}},"authorizedUsers": {"67cd599c-1df7-0348-80bc-04087b296cf3": "ur.vml.5@gmail.com"},"loginCount": "1126","termsOfUseFlag": "Y","termsOfUseDate": "20160127"}],"cached": true}';
    
    $endpoint = $this->getSystemUnderTest($rawResponseToMock,null, null);

    $endpoint->getAdapter()->setUserEmail($data);
    $tcusers = $endpoint->read($data);

    $this->assertInstanceOf(TcUser::class, $tcusers);
  }
  
  /**
   * Test retrieving GUID after creating a TcUser
   */
  public function testCreateTcUser() {
    $data = [
      'email' => 'VML1212@VMLVML.com',
      'firstName' => 'VML1',
      'lastName' => 'VML11',
      'phone' => '785-555-5555',
      'language' => 'ENU',
      'pswdExpDays' => '90'
    ];
    
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"altPhone": "","language": "ENU","pswdExpDays": "90","notified": 0,"email": "VML1212@VMLVML.com","firstName": "VML1","lastName": "VML11","phone": "785-555-5555","password": "U1zezXHaVl"},"result": {"guid": "67cd599c-1df7-0348-80bc-04087b296cf3"},"cached": false}';
  
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\TcUsersEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserToken($this->defaultToken);
    
    $returnedGuid = $endpoint->postTcUser($data);
    $this->assertNotNull($returnedGuid);
  }
  
  /**
   * Test retrieving status after updating a TcUser
   */
  public function testUpdateTcUser() {
    $data = [
      'id' => 'a5e6680c-af23-164a-8914-e2fed65d3f2f',
      'phone' => '785-555-5555'
    ];
    
    $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"id": "a5e6680c-af23-164a-8914-e2fed65d3f2f","phone": "785-554-5555"},"result": [],"cached": false}';
    
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\TcUsersEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserToken($this->defaultToken);
    
    $returnedStatus = $endpoint->updateTcUser($data);
    $this->assertTrue($returnedStatus);
  }
}