<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\SsoLoginsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

/**
 * Class SsoLoginsTest
 *
 * @package Drupal\ur_appliance\Test\Endpoint\DAL
 */
class SsoLoginsTest extends BaseTest {
  protected $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"id":"ur.vml.5@gmail.com"},"result":{"token":"fa1992a03b2fb71464e0ae03bbd00266764a050d"},"cached":false}';

  public function setUp() {
    parent::setUp();
    $this->uri = 'ssologins';
    $this->setClassUnderTest(SsoLoginsEndpoint::class);
    $this->getSystemUnderTest();
  }

  public function testRead() {
    $mockEmail = 'ur.vml.5@gmail.com';
    $endpoint = $this->getSystemUnderTest();
    $response = $endpoint->read($mockEmail);

    $this->assertEquals('fa1992a03b2fb71464e0ae03bbd00266764a050d', $response);
  }

  /**
   * @group live
   */
  public function testLiveRead() {
    $mockEmail = 'ur.vml.5@gmail.com';
    $endpoint = $this->getSystemUnderTest(null, true);
    $response = $endpoint->read($mockEmail);

    $this->assertNotEmpty($response);
  }

  /**
   * Ensure when we submit we have something for the endpoint
   *
   * @expectedException \Drupal\ur_appliance\Exception\DAL\DalException
   * @expectedExceptionMessage Email, "" is not a valid email address.
   */
  public function testSsoHasEmail() {
    $emptyEmail = '';
    $endpoint = $this->getSystemUnderTest();
    $endpoint->read($emptyEmail);
  }

  /**
   * Ensure when we submit it's a valid email
   *
   * @expectedException \Drupal\ur_appliance\Exception\DAL\DalException
   * @expectedExceptionMessage Email, "boriskarloff" is not a valid email address.
   */
  public function testSsoValidEmail() {
    $invalidEmail = 'boriskarloff';
    $endpoint = $this->getSystemUnderTest();
    $endpoint->read($invalidEmail);
  }
}