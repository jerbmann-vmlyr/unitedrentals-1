<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Rental;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Data\DAL\Rate;
use Drupal\ur_appliance\Endpoint\DAL\Rental\RatesEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class RatesTest extends BaseTest {
  public function setUp() {
    parent::setUp();
    $this->uri = 'rental/rates';
    $this->rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"branchId":"J90","jobsiteId":"","startDate":"2017-05-11T14:05:30-04:00","includeRateTiers":false,"deliveryDistance":"","userId":"","webRates":"Y","catClass":["310-6001"]},"result":[{"branchId":"J90","catClass":"310-6001","dayRate":"485","weekRate":"1148","monthRate":"2534","minRate":"0","minRateDuration":"0","rateTypeCode":"H","rateType":"House","expirationDate":null,"currency":"USD","rpp":"0.15","dspRateCatalog":"Y","deliveryCharge":"","rateTiers":"","rateZone":"026"}],"cached":false}';

    $this->setUri('rental/rates');
    $this->setClassUnderTest(RatesEndpoint::class);
  }

  /**
   * Ensure we are returned a rate object.
   */
  public function testRead() {
    $data = ['catClass' => ['310-6001'], 'branchId' => 'J90'];
    $endpoint = $this->getSystemUnderTest();
    $rates = $endpoint->read($data);

    foreach ($rates as $rate) {
      $this->assertInstanceOf(Rate::class, $rate);
    }

    $this->assertCount(1, $rates);
  }

   /**
   * Ensure we are returned a array object.
   */
  public function testImport() {
    //Create variable with result data
    $responseData = json_decode('{"status":0,"messageId":null,"messageText":null,"request":{"branchId":"J90","jobsiteId":"","startDate":"2017-05-11T14:05:30-04:00","includeRateTiers":false,"deliveryDistance":"","userId":"","webRates":"Y","catClass":["310-6001"]},"result":[{"branchId":"J90","catClass":"310-6001","dayRate":"485","weekRate":"1148","monthRate":"2534","minRate":"0","minRateDuration":"0","rateTypeCode":"H","rateType":"House","expirationDate":null,"currency":"USD","rpp":"0.15","dspRateCatalog":"Y","deliveryCharge":"","rateTiers":"","rateZone":"026"}],"cached":false}');

    //Test function import
    $endpoint = $this->getSystemUnderTest();
    $processedResults = $endpoint->import($responseData);

    //Validate return is an array
    $this->assertInternalType('array', $processedResults);

    //Validate foreach rate is an object for the class Rate
    foreach ($processedResults as $rate) {
      $this->assertInstanceOf(Rate::class, $rate);
    }

    //Validate 1 element in the response
    $this->assertCount(1, $processedResults);
  }

  /**
   * Ensure our validation works prior to making any API call.
   *
   * @expectedException \Drupal\ur_appliance\Exception\EndpointException
   * @expectedExceptionMessage Missing required field: branchId
   */
  public function testRequiredFieldBranch() {
    $data = ['catClass' => ['310-6001']];
    $endpoint = $this->getSystemUnderTest();
    $rates = $endpoint->read($data);
  }

  /**
   * Ensure our validation works prior to making any API call.
   *
   * @expectedException \Drupal\ur_appliance\Exception\EndpointException
   * @expectedExceptionMessage Missing required field: catClass
   */
  public function testRequiredFieldsCatClass() {
    $data = ['branchId' => 'J90'];
    $endpoint = $this->getSystemUnderTest();
    $rates = $endpoint->read($data);
  }

  /**
   * Ensure not php error without rates in response.
   *
   * @TODO: Redo this test. It isn't testing anything.
   */
  public function testReadUnhappy() {
    //Change the response for un response without rates
    $this->rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"branchId":"J90","jobsiteId":"","startDate":"2017-05-11T14:05:30-04:00","includeRateTiers":false,"deliveryDistance":"","userId":"","webRates":"Y","catClass":["310-6001"]},"result":[{"error":"error"}],"cached":false}';

    $data = ['catClass' => ['310-6001'], 'branchId' => 'J90'];
    $endpoint = $this->getSystemUnderTest();
    $rates = $endpoint->read($data);
  }

  /**
   * Ensure not php error without rates in response.
   */
  public function testImportUnhappy() {
    //Create variable with result data
    $responseData = json_decode('{"status":0,"messageId":null,"messageText":null,"request":{"branchId":"J90","jobsiteId":"","startDate":"2017-05-11T14:05:30-04:00","includeRateTiers":false,"deliveryDistance":"","userId":"","webRates":"Y","catClass":["310-6001"]},"result":[{"error":"error"}],"cached":false}');

    //Test function import
    $endpoint = $this->getSystemUnderTest();
    $processedResults = $endpoint->import($responseData);

    //Validate return is an array
    $this->assertInternalType('array', $processedResults);

    //Validate foreach rate is an object for the class Rate
    foreach ($processedResults as $rate) {
      $this->assertInstanceOf(Rate::class, $rate);
    }

    //Validate 1 element in the response
    $this->assertCount(1, $processedResults);
  }

  /**
   * Ensure not php error without rates in response.
   */
  public function testImportUnhappyWithAObjectEmpty() {
    //Create object with result data
    $object = new \stdClass();
    $object->result = json_decode('');

    //Test function import
    $endpoint = $this->getSystemUnderTest();
    $processedResults = $endpoint->import($object);

    //Validate return is null
    $this->assertNull($processedResults);
  }
}