<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Catalog;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Data\DAL\Item;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Endpoint\DAL\Catalog\RecentRentalItemsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;


class RecentRentalItemsTest extends BaseTest {
  protected $systemUnderTest;
  protected $baseUrl = 'https://testmobiledal.ur.com';
  protected $uri = 'catalog/recentrentalitems';
  protected $method = 'GET';
  protected $allowedMethods = ['GET'];
  protected $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"accountId": "72","limit": 30},"result": [{"catClass": "300-2999","desc": "SCISSOR LIFT 30-35\' ELECTRIC 46-48\" WIDE"}],"cached": false}';

  protected $accountId = 18;
  protected $defaultData = ['accountId' => 18, 'limit' => 8];
  
  public function setUp() {
    parent::setUp();
    $this->uri = 'catalog/recentrentalitems';
    $this->setClassUnderTest(RecentRentalItemsEndpoint::class);
    $this->getSystemUnderTest();
  }
  
  /**
   * Ensure we are returned a recentRentalItem object with live request.
   *
   * @group live
   */
  public function testReadLive() {
    $adapter = ApiContainer::getService('dal_adapter');
    $endpoint = new RecentRentalItemsEndpoint($adapter);
  
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $data = $this->defaultData;
    $recent_rental_items = $endpoint->read($data);
    
    foreach ($recent_rental_items as $recent_rental_item) {
      $this->assertInstanceOf(Item::class, $recent_rental_item);
    }
  }
  
  /**
   * Ensure we are returned a recentRentalItem object with mock data.
   */
  public function testReadMock() {
    $data = $this->defaultData;
    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $recent_rental_items = $endpoint->read($data);
    
    foreach ($recent_rental_items as $recent_rental_item) {
      $this->assertInstanceOf(Item::class, $recent_rental_item);
    }
  }
  
  /**
   * Ensure we are returned an array object.
   */
  public function testImport() {
    //Create variable with result data
    $responseData = json_decode('{"status": 0,"messageId": null,"messageText": null,"request": {"accountId": "1075311","limit": 8},"result": [{"catClass": "100-3185","desc": "COMPRESSOR 175-195 CFM"},{"catClass": "750-1105","desc": "SWEEPER RIDE ON 8\' WINDROW THREE WHEEL"},{"catClass": "750-1107","desc": "SWEEPER 8\' PRESSURIZED CAB"},{"catClass": "953-2225","desc": "TRUCK WATER 2000-2999 GAL CDL"},{"catClass": "955-1115","desc": "UTV 2WD GAS 2SEAT CANOPY"},{"catClass": "320-4000","desc": "LIGHT TOWER TOWABLE SMALL"},{"catClass": "100-2140","desc": "COMPRESSOR 13-14.9 CFM GAS"},{"catClass": "520-9130","desc": "PUMP 2\" DEWATERING"}],"cached": false}');
    
    //Test function import
    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $processedResults = $endpoint->import($responseData);
    
    //Validate return is an array
    $this->assertInternalType('array', $processedResults);
    
    //Validate foreach rate is an object for the class Rate
    foreach ($processedResults as $rate) {
      $this->assertInstanceOf(Item::class, $rate);
    }
    
    //Validate 1 element in the response
    $this->assertCount(8, $processedResults);
  }
  
  /**
   * Ensure not php error without rates in response.
   */
  public function testReadUnhappy() {
    //Change the response for un response without recent rental items
    $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"accountId": "' . $this->accountId . '","limit": 30},"result":[{"error":"error"}],"cached":false}';
    $data = $this->defaultData;
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $recentRentalItems = $endpoint->read($data);
  }
  
  /**
   * Ensure not php error without items in response.
   */
  public function testImportUnhappy() {
    //Create variable with result data
    $responseData = json_decode('{"status": 0,"messageId": null,"messageText": null,"request": {"accountId": "' . $this->accountId . '","limit": 30},"result":[{"error":"error"}],"cached": false}');
    
    //Test function import
    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $processedResults = $endpoint->import($responseData);
    
    //Validate return is an array
    $this->assertInternalType('array', $processedResults);
    
    //Validate foreach rate is an object for the class Rate
    foreach ($processedResults as $recentRentalItems) {
      $this->assertInstanceOf(Item::class, $recentRentalItems);
    }
    
    //Validate 1 element in the response
    $this->assertCount(1, $processedResults);
  }
  
  /**
   * Ensure not php error without items in response.
   *
   * @expectedException \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function testImportUnhappyWithAObjectEmpty() {
    //Create object with result data
    $object = new \stdClass();
    $object->result = json_decode('');
  
    //Test function import
    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $processedResults = $endpoint->import($object);
    
    //Validate return is null
    $this->assertNull($processedResults);
  }
}