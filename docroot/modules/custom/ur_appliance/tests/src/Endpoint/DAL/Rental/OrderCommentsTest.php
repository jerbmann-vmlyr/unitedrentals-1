<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Comment;
use Drupal\ur_appliance\Endpoint\DAL\Rental\OrderCommentsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class OrderCommentsTest extends BaseTest {
  
  protected $systemUnderTest;
  protected $baseUrl = 'https://testmobiledal.ur.com';
  protected $uri = 'rental/ordercomments';
  protected $method = 'GET';
  protected $allowedMethods = ['GET', 'POST'];
  
  public function setUp() {
    parent::setUp();
    $this->uri = 'rental/ordercomments';
    $this->rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"transId": "147229912","transSeqId": 0,"commentType": "R"},"result": [{"transId": "147229912","commentType": "R","commentSeq": "1","commentText": "\u0014Chgs made 07/06/17 09:59 AM by"},{"transId": "147229912","commentType": "R","commentSeq": "2","commentText": "\u0082Suggested Delivery charge  $ 99.00"},{"transId": "147229912","commentType": "R","commentSeq": "3","commentText": "\u0082Delivery charge  $ 99.00"},{"transId": "147229912","commentType": "R","commentSeq": "4","commentText": "\u0082Suggested Pickup charge  $ 99.00"},{"transId": "147229912","commentType": "R","commentSeq": "5","commentText": "\u0082Pickup charge  $ 99.00"}],"cached": false,"recordCount": 5}';

    $this->setUri($this->uri);
    $this->setClassUnderTest(OrderCommentsEndpoint::class);
  }
  
  public function testReadMock() {
    $data = ['transId' => '147229912', 'accountId' => '2668808'];

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\OrderCommentsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $order_comments = $endpoint->read($data);
  
    foreach ($order_comments as $order_comment) {
      $this->assertInstanceOf(Comment::class, $order_comment);
    }
  }
  
  /**
   * Ensure we are returned an array object.
   */
  public function testImport() {
    //Create variable with result data
    $responseData = json_decode( '{"status": 0,"messageId": null,"messageText": null,"request": {"transId": "147229912","transSeqId": 0,"commentType": "R"},"result": [{"transId": "147229912","commentType": "R","commentSeq": "1","commentText": "\u0014Chgs made 07/06/17 09:59 AM by"},{"transId": "147229912","commentType": "R","commentSeq": "2","commentText": "\u0082Suggested Delivery charge  $ 99.00"},{"transId": "147229912","commentType": "R","commentSeq": "3","commentText": "\u0082Delivery charge  $ 99.00"},{"transId": "147229912","commentType": "R","commentSeq": "4","commentText": "\u0082Suggested Pickup charge  $ 99.00"},{"transId": "147229912","commentType": "R","commentSeq": "5","commentText": "\u0082Pickup charge  $ 99.00"}],"cached": false,"recordCount": 5}');
    
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\OrderCommentsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest();

    // Test function import
    $processedResults = $endpoint->import($responseData);
    
    //Validate return is an array
    $this->assertInternalType('array', $processedResults);
    
    //Validate foreach Comment is an object for the class Comment
    foreach ($processedResults as $order_comment) {
      $this->assertInstanceOf(Comment::class, $order_comment);
    }
    
    //Validate 1 element in the response
    $this->assertCount(5, $processedResults);
  }
  
  /**
   * Ensure not php error without files in response.
   *
   * @expectedException \Drupal\ur_appliance\Exception\DAL\DalException
   */
  public function testImportUnhappyWithAObjectEmpty() {
    //Create object with result data
    $object = new \stdClass();
    $object->result = json_decode('');
    
    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\OrderCommentsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest();

    // Test function import
    $processedResults = $endpoint->import($object);
    
    //Validate return is null
    $this->assertNull($processedResults);
  }
  
  /**
   * Test retrieving status result after linking account to the user.
   *      * Result data
   *    true  => Success
   *    false => Error
   *
   * @return boolean
   */
  public function testPostOrderComment() {
    $data = [
      'transId' => '148188997',
      'commentType' => 'D',
      'commentText' => 'This is test'
    ];
    
    $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"transId":"148188997","transSeqId":"0","commentType":"O","commentText":"150-2600 - Rammer, Large, 2900-3600 lbs. Impact : NA"},"result":[],"cached":false}';

    /** @var OrderCommentsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setUserToken($this->defaultToken);

    $status = $endpoint->post($data);
    $this->assertTrue($status);
  }
}