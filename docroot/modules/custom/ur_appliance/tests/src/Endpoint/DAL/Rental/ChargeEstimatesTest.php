<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Rental;

use Drupal\ur_appliance\Data\DAL\Estimate;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Rental\ChargeEstimatesEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class ChargeEstimatesTest extends BaseTest {
  protected $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"branchId":"C08","accountId":null,"jobsiteId":null,"startDate":"2017-07-05T09:00:00-05:00","returnDate":"2017-07-29T09:00:00-05:00","delivery":"Y","pickup":"Y","omitNonDspRates":"true","webRates":"Y","rpp":"Y","catClassAndQty":["310-9801=2"]},"result":{"details":[{"category":"310","class":"9801","qty":"2","outsource":"","vendor":"0","description":"","eqptype":"","minrate":"1886","dayrate":"1886","weekrate":"4500","monthrate":"10629","rentalamt":"21258","dspRateCatalog":"Y"}],"totals":{"totrentamt":21258,"totenvamt":75,"tottaxamt":2064.63,"totgstamt":0,"totdwamt":3188.7,"totdelamt":252,"totpuamt":252,"miscCharges":44.79,"authAmount":32508.4,"authPercent":120,"rateZone":"021"}},"cached":false}';

  public function setUp() {
    parent::setUp();
    $this->uri = 'rental/chargeestimates';
    $this->setClassUnderTest(ChargeEstimatesEndpoint::class);
    $this->getSystemUnderTest();
  }

  public function testRead() {
    $data = [
      'branchId' => 'C08',
      'startDate' => '2017-07-05T09%3A00%3A00-05%3A00', // Need a function that always generates a future date
      'returnDate' => '2017-07-29T09%3A00%3A00-05%3A00',
      'delivery' => true,
      'pickup' => true,
      'rpp' => true,
      'catClassAndQty' => '310-9801%3D2'
    ];

    $endpoint = $this->getSystemUnderTest();

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\ChargeEstimatesEndpoint $estimate */
    $estimate = $endpoint->read($data);

    $this->assertInstanceOf(Estimate::class, $estimate);
    $this->assertEquals('Y', $estimate->getDelivery());
    $this->assertEquals('Y', $estimate->getPickup());
    $this->assertEquals('Y', $estimate->getRpp());

    $totals = $estimate->getTotals();
    $this->assertNotEmpty($totals); // @TODO: Mock the response
  }
}