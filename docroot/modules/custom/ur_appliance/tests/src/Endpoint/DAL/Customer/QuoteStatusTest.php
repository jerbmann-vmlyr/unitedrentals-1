<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use Drupal\ur_appliance\Data\DAL\Quote as QuoteStatusData;
use Drupal\ur_appliance\Endpoint\DAL\Customer\QuotesEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class QuoteStatusTest extends BaseTest {

  protected $systemUnderTest;
  protected $baseUrl = 'https://testmobiledal.ur.com';
  protected $uri = 'customer/orderstatus';
  protected $method = 'GET';
  protected $allowedMethods = ['GET'];

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/orderstatus';
    $this->rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"startRecord":1,"maxRecords":0,"openQuotes":"I","openRsvReq":"E","guid":"0e35d520-1297-d44f-8bc1-3a9ae944f648","accountId":["18"]},"result":[{"accountId":"18","requisitionId":"A000880362","requisitionCancelled":"N","reservationId":"","quoteId":"149035917","startDate":"09-27-2017","jobId":"1","jobName":"KING ELECTRIC","createdDate":"08-15-2017","orderStatus":"Pending","orderStatusCode":"PND"}],"cached":false,"recordCount":1,"startRecord":1,"endRecord":1,"moreRecords":false}';

    $this->setUri($this->uri);
    $this->setClassUnderTest(QuotesEndpoint::class);
  }

  public function testReadMock() {
    $data = ['guid' => '0e35d520-1297-d44f-8bc1-3a9ae944f648'];

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Customer\QuotesEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserEmail('ur.vml.7@gmail.com');
    $order_statuses = $endpoint->read($data);

    foreach ($order_statuses as $order_status) {
      $this->assertInstanceOf(QuoteStatusData::class, $order_status);
    }
  }
}