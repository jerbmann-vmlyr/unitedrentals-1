<?php
namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Data\DAL\Requester;
use Drupal\ur_appliance\Endpoint\DAL\Customer\TcRequestersEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class TcRequestersTest extends BaseTest {
  protected $uri = 'customer/tcrequesters';
  protected $allowedMethods = ['GET'];

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/tcrequesters';
    $this->rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"accountId":"18"},"result":[{"accountId":"18","requesterGuid":"a06da162-b8f8-c542-93b6-1e7056248e78","requesterFirstName":"CURTIS12","requesterLastName":"LEFEBVRE","requesterEmail":"USER10@TOTALCONTROL.NET","contactPhone":"503-565-6555","contactAltPhone":"555-111-3333","status":"A"},{"accountId":"18","requesterGuid":"cd86ff40-a1a8-ba46-9948-1c11baa18847","requesterFirstName":"DAVE CASH","requesterLastName":"SMITH","requesterEmail":"USER10AA@TOTALCONTROL.NET","contactPhone":"555-111-2222","contactAltPhone":"","status":""},{"accountId":"18","requesterGuid":"124b3271-c106-3a4e-a73e-05508e3cafc1","requesterFirstName":"JOHN","requesterLastName":"SMITH","requesterEmail":"USER10AJOHN@TOTALCONTROL.NET","contactPhone":"555-333-1111","contactAltPhone":"","status":""},{"accountId":"18","requesterGuid":"d8b7c2f9-e499-3f46-b61a-8ada6f924e33","requesterFirstName":"USER10A LONGER NAME","requesterLastName":"LAST NAME LONGER NM","requesterEmail":"USER10ALONG@TOTALCONTROL.NET","contactPhone":"555-222-1111","contactAltPhone":"","status":""},{"accountId":"18","requesterGuid":"66c1dba7-1153-9c48-84a9-a617107ca7ba","requesterFirstName":"USER10AFN LONGER","requesterLastName":"USER10ALN LONGER","requesterEmail":"USER10A@TOTALCONTROL.NET","contactPhone":"555-222-3333","contactAltPhone":"888-999-7777","status":""}],"cached":false,"recordCount":5}';
    $this->setClassUnderTest(TcRequestersEndpoint::class);
  }

  /**
   * Ensure we are returned a list of requester objects with mock request.
   */
  public function testRead() {
    /**
     * Recent token changes mean that you can now get the user token to use in
     * our testing. You just have to set their email address.
     * $adapter->setUserEmail('someEmail@example.com');
     */
    $data = '18'; // A valid accountId
    /** @var TcRequestersEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserToken('c9112d311b5e4604ef6a8aa3e50f53ccf06d3d25');
    $tcrequesters = $endpoint->read($data);

    foreach ($tcrequesters as $requester) {
      $this->assertInstanceOf(Requester::class, $requester);
    }
  }
}