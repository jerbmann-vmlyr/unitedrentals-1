<?php

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Location;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Data\DAL\Branch;
use Drupal\ur_appliance\Endpoint\DAL\Location\BranchesEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class BranchesTest extends BaseTest {
  protected $rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"id":["A00","J90","990"]},"result":[{"id":"A00","name":"UNITED RENTALS","address1":"1005 WEST COLLEGE AVENUE","address2":"","city":"STATE COLLEGE","state":"PA","zip":"16801","phone":"814-237-3325","fax":"814-237-3385","weekdayhours":"7:00AM-5:00PM","saturdayhours":"CLOSED","sundayhours":"CLOSED","timeOffset":"0","managerName":"JEFFREY KNOTT","managerEmail":"jknott1@ur.com","reportHeading":"MA-STATE COLLEGE, PA (A00)","status":"A","langCode":"ENU","currency":"USD","countryCode":"US","businessType":"GR","webDisplay":true,"latitude":"40.786302","longitude":"-77.872269","formerRSC":false,"acquiredCompanyCode":"","businessTypes":[{"type":"GR"},{"type":"AE"}],"onSite":false,"serviceHub":"","hub":"","hubDesc":"","webRefreshRate":"","capacityPct":"","districtName":"","districtMgr":"","districtMgrEm":"","districtMgrPh":"","serviceContact1":"","serviceContact1Em":"","serviceContact1Ph":"","serviceContact2":"","serviceContact2Em":"","serviceContact2Ph":""},{"id":"J90","name":"UNITED RENTALS","address1":"215 EAST BASELINE ROAD","address2":"","city":"GILBERT","state":"AZ","zip":"85233","phone":"480-892-0033","fax":"480-507-7651","weekdayhours":"6:30AM-4:30PM","saturdayhours":"CLOSED","sundayhours":"CLOSED","timeOffset":"-300","managerName":"PHILIP HOWARD","managerEmail":"phoward@ur.com","reportHeading":"PW-GILBERT, AZ (J90)","status":"A","langCode":"ENU","currency":"USD","countryCode":"US","businessType":"GR","webDisplay":true,"latitude":"33.377500","longitude":"-111.827953","formerRSC":true,"acquiredCompanyCode":"R","businessTypes":[{"type":"GR"}],"onSite":false,"serviceHub":"O6S","hub":"07H","hubDesc":"SW-PHOENIX METRO","webRefreshRate":"","capacityPct":"","districtName":"","districtMgr":"","districtMgrEm":"","districtMgrPh":"","serviceContact1":"","serviceContact1Em":"","serviceContact1Ph":"","serviceContact2":"","serviceContact2Em":"","serviceContact2Ph":""},{"id":"990","name":"UNITED RENTALS","address1":"5704 S. TOYOTA PLACE","address2":"","city":"FRESNO","state":"CA","zip":"93725","phone":"559-442-8989","fax":"559-442-8992","weekdayhours":"7:00AM-5:00PM","saturdayhours":"CLOSED","sundayhours":"CLOSED","timeOffset":"-300","managerName":"CHRIS MCKEEVER","managerEmail":"cmckeeve@ur.com","reportHeading":"TS-FRESNO, CA (990)","status":"A","langCode":"ENU","currency":"USD","countryCode":"US","businessType":"TR","webDisplay":true,"latitude":"36.652379","longitude":"-119.709601","formerRSC":false,"acquiredCompanyCode":"","businessTypes":[{"type":"TS"}],"onSite":false,"serviceHub":"","hub":"","hubDesc":"","webRefreshRate":"","capacityPct":"","districtName":"","districtMgr":"","districtMgrEm":"","districtMgrPh":"","serviceContact1":"","serviceContact1Em":"","serviceContact1Ph":"","serviceContact2":"","serviceContact2Em":"","serviceContact2Ph":""}],"cached":true}';

  public function setUp() {
    parent::setUp();
    $this->uri = 'location/branches';
    $this->setClassUnderTest(BranchesEndpoint::class);
    $this->getSystemUnderTest();
  }

  /**
   * Assure our read method returns a single instance of a Branch object
   */
  public function testRead() {
    $endpoint = $this->getSystemUnderTest();
    $mockBranchId = 'A00';
    $branch = $endpoint->read($mockBranchId);
    $this->assertInstanceOf(Branch::class, $branch);
    $this->assertEquals('A00', $branch->getId());
  }

  /**
   * Ensure we can read multiple instances of Branch objects
   */
  public function testReadAll() {
    $endpoint = $this->getSystemUnderTest();
    $branches = $endpoint->readAll();

    foreach ($branches as $branch) {
      $this->assertInstanceOf(Branch::class, $branch);
    }

    $this->assertCount(3, $branches);
  }

  /**
   * Assure the Branch ID parameter of read() is always 3 characters
   *
   * @expectedException \Drupal\ur_appliance\Exception\DAL\DalException
   * @expectedExceptionMessage Branch ID parameter can be no more or less than 3 characters.
   */
  public function testReadArgumentsError() {
    $endpoint = $this->getSystemUnderTest();
    $mockBranchShortId = 'A0';

    $branch = $endpoint->read($mockBranchShortId);

    $endpoint = $this->getSystemUnderTest();
    $mockBranchLongId = 'A000';

    $branch = $endpoint->read($mockBranchLongId);
  }

  /**
   * Ensures the import() method returns an array of Branch data objects.
   */
  public function testImport() {
    $endpoint = $this->getSystemUnderTest();
    $response = $this->getMockResponse()->getBody()->getContents();
    $data = json_decode($response);
    $processedResults = $endpoint->import($data);

    foreach ($processedResults as $branch) {
      $this->assertInstanceOf(Branch::class, $branch);
    }
  }

  /**
   * Ensure the importSingle() method returns a single Branch data object.
   */
  public function testImportSingle() {
    $endpoint = $this->getSystemUnderTest();
    $data = json_decode($this->getMockResponse()->getBody()->getContents());
    $branchId = 'J90';
    $branch = $endpoint->importSingle($data, $branchId);
    $this->assertInstanceOf(Branch::class, $branch);
  }
}