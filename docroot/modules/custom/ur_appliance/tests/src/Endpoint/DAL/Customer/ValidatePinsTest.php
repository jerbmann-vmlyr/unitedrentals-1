<?php
namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\DAL\DalAdapter;
use Drupal\ur_appliance\Endpoint\DAL\Customer\ValidatePinsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class ValidatePinsTest extends BaseTest {
  protected $uri = 'customer/validatepins';
  protected $allowedMethods = ['GET'];
  protected $rawResponseToMock = '{"status": 0,"messageId": null,"messageText": null,"request": {"accountId": "7205489","pin": "1234"},"result": {"code": "N"},"cached": false}';

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/validatepins';
    $this->setClassUnderTest(ValidatePinsEndpoint::class);
  }
  
  /**
   * Ensure we are returned a string as code for link validation between user and accountID with mock data.
   *
   * We cannot call the read method without a valid token. So we are going to
   * test the import method instead.
   *
   * Result Data :
   *    Y (Yes - Authorized)
   *    N (No - Not Authorized)
   *    0 (Zeros - No pin# setup)
   *    D (Disabled)
   */
  public function testImport() {
    $endpoint = $this->getSystemUnderTest();

    $result = $endpoint->import( json_decode($this->rawResponseToMock) );
    $this->assertEquals('N', $result);
  }
}