<?php
namespace Drupal\ur_appliance\Test\Endpoint\DAL\Customer;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Data\DAL\Approver;
use Drupal\ur_appliance\Endpoint\DAL\Customer\TcApproversEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class TcApproversTest extends BaseTest {
  protected $uri = 'customer/tcapprovers';
  protected $allowedMethods = ['GET'];

  public function setUp() {
    parent::setUp();
    $this->uri = 'customer/tcapprovers';
    $this->rawResponseToMock = '{"status":0,"messageId":null,"messageText":null,"request":{"accountId":"18"},"result":[{"accountId":"18","approverGuid":"a06da162-b8f8-c542-93b6-1e7056248e78","approverFirstName":"CURTIS12","approverLastName":"LEFEBVRE","approverEmail":"USER10@TOTALCONTROL.NET","contactPhone":"503-565-6555","contactAltPhone":"555-111-3333","status":"A"},{"accountId":"18","approverGuid":"cd86ff40-a1a8-ba46-9948-1c11baa18847","approverFirstName":"DAVE CASH","approverLastName":"SMITH","approverEmail":"USER10AA@TOTALCONTROL.NET","contactPhone":"555-111-2222","contactAltPhone":"","status":""},{"accountId":"18","approverGuid":"124b3271-c106-3a4e-a73e-05508e3cafc1","approverFirstName":"JOHN","approverLastName":"SMITH","approverEmail":"USER10AJOHN@TOTALCONTROL.NET","contactPhone":"555-333-1111","contactAltPhone":"","status":""},{"accountId":"18","approverGuid":"d8b7c2f9-e499-3f46-b61a-8ada6f924e33","approverFirstName":"USER10A LONGER NAME","approverLastName":"LAST NAME LONGER NM","approverEmail":"USER10ALONG@TOTALCONTROL.NET","contactPhone":"555-222-1111","contactAltPhone":"","status":""},{"accountId":"18","approverGuid":"66c1dba7-1153-9c48-84a9-a617107ca7ba","approverFirstName":"USER10AFN LONGER","approverLastName":"USER10ALN LONGER","approverEmail":"USER10A@TOTALCONTROL.NET","contactPhone":"555-222-3333","contactAltPhone":"888-999-7777","status":""}],"cached":false,"recordCount":5}';
    $this->setClassUnderTest(TcApproversEndpoint::class);
  }

  /**
   * Ensure we are returned a list of approver objects with mock request.
   */
  public function testRead() {
    /**
     * Recent token changes mean that you can now get the user token to use in
     * our testing. You just have to set their email address.
     * $adapter->setUserEmail('someEmail@example.com');
     */
    $data = '18'; // A valid accountId
    /** @var TcApproversEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest();
    $endpoint->getAdapter()->setUserToken('c9112d311b5e4604ef6a8aa3e50f53ccf06d3d25');
    $tcapprovers = $endpoint->read($data);

    foreach ($tcapprovers as $approver) {
      $this->assertInstanceOf(Approver::class, $approver);
    }
  }
}