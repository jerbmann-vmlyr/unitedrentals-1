<?php
/**
 * Created by PhpStorm.
 * User: mjones2
 * Date: 11/15/17
 * Time: 2:09 PM
 */

namespace Drupal\ur_appliance\Test\Endpoint\DAL\Rental;

use stdClass;
use Drupal\ur_appliance\Data\DAL\Invoice;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\DAL\Rental\InvoicesEndpoint;
use Drupal\ur_appliance\Exception\EndpointException;
use Drupal\ur_appliance\Test\BaseTest;

class InvoiceTest extends BaseTest {
  protected $singleDataPath;
  protected $listDataPath;

  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(InvoicesEndpoint::class);
    $this->singleDataPath = MOCK_RESPONSE_ROOT . '/Rentals/invoice.json';
    $this->listDataPath = MOCK_RESPONSE_ROOT . '/Rentals/invoices.json';
  }

  /**
   * @group invoice
   */
  public function testImport() {

    $data = new stdClass();
    $data->results = [];

    for ($i = 0; $i < rand(0,100); $i++) {
      $data->result[] =
        (object) [
          'balance' => rand(0, 4) ? rand(1, 10000) : Invoice::EMPTY_VALUE,
          'accountId' => rand(0, 1000),
          'id' => rand(1001,9999)
        ];
    }

    /** @var InvoicesEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest();

    //array of invoices
    $invoices = $endpoint->import($data);

    /** @var Invoice $invoice */
    foreach ($invoices as $i => $invoice) {

      $this->assertInstanceOf(Invoice::class, $invoice);

      if ($data->result[$i]->balance === Invoice::EMPTY_VALUE) {
        $this->assertEquals(0, $invoice->getBalance());
      }
      else {
        $this->assertEquals($data->result[$i]->balance, $invoice->getBalance());
      }

      $this->assertEquals($data->result[$i]->accountId, $invoice->getAccountId());
      $this->assertEquals($data->result[$i]->id, $invoice->getId());
    }

  }

  /**
   * @group invoice
   */
  public function testRead() {

    $rawResponseToMock = file_get_contents($this->singleDataPath);

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\InvoicesEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setMethod('GET');
    $endpoint->getAdapter()->setUserEmail('ur.vml.5@gmail.com');

    $data = [
      'id' => '148189020',
      'accountId' => '1326960'
    ];

    $results = $endpoint->read($data);

    $this->assertInstanceOf(Invoice::class, $results);
  }

  /**
   * @group invoice
   */
  public function testReadIndex(){

  }

  /**
   * @group invoice
   */
  public function testReadRequiredFields() {
    $rawResponseToMock = file_get_contents($this->singleDataPath);

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\InvoicesEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setMethod('GET');
    $endpoint->getAdapter()->setUserEmail('ur.vml.5@gmail.com');

    //test missing accountId
    $data = [
      'id' => '148189020'
    ];

    $this->expectException(EndpointException::class);
    $endpoint->read($data);

    //test missing id
    $data = [
      'accountId' => 'a2034',
    ];

    $this->expectException(EndpointException::class);
    $endpoint->read($data);
  }

  /**
   * @group invoice
   */
  public function testReadAll() {
    $rawResponseToMock = file_get_contents($this->listDataPath);

    /** @var \Drupal\ur_appliance\Endpoint\DAL\Rental\InvoicesEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);
    $endpoint->getAdapter()->setMethod('GET');
    $endpoint->getAdapter()->setUserEmail('ur.vml.5@gmail.com');

    //test missing accountId
    $data = [
      'accountId' => '148189020'
    ];

    $resultInvoiceObject = $endpoint->readAllByItem($data);

    foreach ($resultInvoiceObject as $invoice) {
      $this->assertInstanceOf(Invoice::class, $invoice);
    }
  }

  /**
   * @group invoice
   */
  public function testReadAllMissingRequired() {
    //test missing accountId
    $data = [
    ];
    $rawResponseToMock = file_get_contents($this->listDataPath);

    /** @var InvoicesEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($rawResponseToMock);

    $this->expectException(EndpointException::class);

    $endpoint->readAllByItem($data);
  }
}