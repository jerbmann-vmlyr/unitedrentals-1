<?php

namespace Drupal\ur_appliance\Test\Endpoint\Google;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\Google\GoogleMapsAdapter;
use Drupal\ur_appliance\Data\Google\DetailsResponse;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\Google\DetailsEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class DetailsTest extends BaseTest {

  protected $singleDataPath;

  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(DetailsEndpoint::class);
    $this->singleDataPath = MOCK_RESPONSE_ROOT . '/Google/detail.json';
    $this->setDefaultAdapter(GoogleMapsAdapter::class);
    $this->getSystemUnderTest();
  }

  /**
   * @TODO: Mock this and make it different.
   */
  public function testRead() {
    $placeId = 'ChIJC03wb838wIcR6XgcfYWM4Ec';
    $mockResponse = file_get_contents($this->singleDataPath);

    /** @var DetailsEndpoint $endpoint */
    $endpoint = $this->getSystemUnderTest($mockResponse);

    // Commented this out because it makes a real call.
    // @TODO: Mock the response instead.
    $result = $endpoint->read($placeId);

    $this->assertNotEmpty($result);
    $this->assertInstanceOf(DetailsResponse::class, $result);
  }

  /**
   * @group live
   */
  public function testReadLive() {
    $placeId = 'ChIJC03wb838wIcR6XgcfYWM4Ec';
    $endpoint = ApiContainer::getService('endpoint.google.details');

    // Commented this out because it makes a real call.
    // @TODO: Mock the response instead.
    /** @var DetailsResponse $result */
    $result = $endpoint->read($placeId);

    $this->assertNotEmpty($result);
    $this->assertNotEquals('INVALID_REQUEST', $result->getStatus());
    $this->assertInstanceOf(DetailsResponse::class, $result);
  }
}