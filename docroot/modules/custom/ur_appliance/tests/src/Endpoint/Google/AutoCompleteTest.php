<?php

namespace Drupal\ur_appliance\Test\Endpoint\Google;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Tests\Logger;
use Drupal\ur_appliance\Adapter\Google\GoogleMapsAdapter;
use Drupal\ur_appliance\Data\Google\AutoCompleteResponse;
use Drupal\ur_appliance\DependencyInjection\ApiContainer;
use Drupal\ur_appliance\Endpoint\Google\AutoCompleteEndpoint;
use Drupal\ur_appliance\Test\BaseTest;

class AutoCompleteTest extends BaseTest {

  protected $singleDataPath;

  public function setUp() {
    parent::setUp();
    $this->setClassUnderTest(AutoCompleteEndpoint::class);
    $this->singleDataPath = MOCK_RESPONSE_ROOT . '/Google/autocomplete.json';
    $this->setDefaultAdapter(GoogleMapsAdapter::class);
    $this->getSystemUnderTest();
  }

  public function testRead() {
    $query = 'Overland Park, KS 66213';
    $mockResponse = file_get_contents($this->singleDataPath);
    
    $endpoint = $this->getSystemUnderTest($mockResponse);

    // Commented this out because it makes a real call.
    // @TODO: Mock the response instead.
    $result = $endpoint->read($query);

    $this->assertNotEmpty($result);
    $this->assertInstanceOf(AutoCompleteResponse::class, $result);
  }

  public function testReadLive() {
    $query = 'Overland Park, KS 66213';
    $endpoint = ApiContainer::getService('endpoint.google.autocomplete');

    // Commented this out because it makes a real call.
    // @TODO: Mock the response instead.
    $result = $endpoint->read($query);

    $this->assertNotEmpty($result);
    $this->assertInstanceOf(AutoCompleteResponse::class, $result);
  }
}