<?php

namespace Drupal\ur_item\Commands;

use Drush\Commands\DrushCommands;

/**
 * UR Item commands for Drush 9+
 */
class ItemCommands extends DrushCommands {

  /**
   * Runs batch process to import catalog data into drupal as "Item" nodes.
   *
   * @command ur_item:catalog_import
   * @aliases catalog_import
   * @usage ur_item:catalog_import
   *   Return output from Catalog Import
   */
  public function catalogImport():void {
    $this->output->writeln(dt('Starting Catalog Import'));

    /** @var \Drupal\ur_item\Batch\CatalogBatchProcessor $batchProcessor */
    $batchProcessor = \Drupal::service('ur_item.batch_processor');
    batch_set($batchProcessor->createBatch());
    drush_backend_batch_process();

    $this->output->writeln(dt('Finished Catalog Import'));
  }

}
