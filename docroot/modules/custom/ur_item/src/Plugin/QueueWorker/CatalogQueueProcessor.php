<?php

namespace Drupal\ur_item\Plugin\QueueWorker;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\file\FileInterface;
use Drupal\node\Entity\Node;
use Drupal\ur_frontend_api_v2\Models\CatClass;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @QueueWorker(
 *  id = "catalog_queue",
 *  title = "Catalog Queue Processor"
 * )
 */
class CatalogQueueProcessor extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The Drupal Database Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Processes single item for import.
   *
   * @see \Drupal\Core\Queue\QueueWorkerInterface::processItem()
   * @see \Drupal\ur_frontend_api_v2\Models\CatClass
   *
   * @param mixed $item
   *   A single item stored in queue. In our case a CatClass object
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function processItem($item) {
    $catClass = $item->data;

    if ($catClass instanceof CatClass) {
      /*
       * Determine if we're creating or updating a node based on cat-class.
       *
       * Note: this method could return false if none found.
       */
      $nids = $this->getItemNodeByCatClass($catClass->id);

      if ($nids == FALSE) {
        // Then we know we're creating one.
        $this->createNewItem($catClass);
        return;
      }

      /*
       * There could be instances where there are multiple nodes that share a
       * catclass. We will only work with exactly one. If we catch multiple we
       * will log the cat-class and move on.
       */
      if (is_array($nids) && count($nids) == 1) {
        $nodeId = reset($nids);
      }
      else {
        \Drupal::logger('catalog_queue')->error('multiple nodes with same catclass found for cat-class: ' . $catClass->id);
        return;
      }

      // If we found one, update it.
      if ($nodeId) {
        $this->updateExistingItem($nodeId, $catClass);
      }
    }
    else {
      \Drupal::logger('catalog_queue')->error('Not given instance of CatClass');
    }
  }

  /**
   * Queries Item node based a given field value (field_item_cat_class_code).
   *
   * @param string $catClass
   *   The catclass to query against
   *
   * @return array|int
   */
  private function getItemNodeByCatClass(string $catClass) {
    // Get all node IDs that are of type item and match cat-class value given.
    $node_ids = \Drupal::entityQuery('node')
      ->condition('type', 'item')
      ->condition('field_item_cat_class_code', $catClass)
      ->execute();

    if (!empty($node_ids)) {
      return $node_ids;
    }

    return FALSE;
  }

  /**
   * Helper method that determines publishing status based on CatClass values.
   *
   * @param \Drupal\ur_frontend_api_v2\Models\CatClass $catClass
   *   CatClass object to use for determining publishing status.
   *
   * @return bool
   *   True if it's active and we're to displayInCatalog, false otherwise.
   */
  private function shouldBePublished(CatClass $catClass) {
    return $catClass->active && $catClass->displayInCatalog;
  }

  /**
   * Gets taxonomy term ID given a mapLevel value.
   *
   * @param string $mapLevel
   *   The mapLevel string to search against.
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]|bool
   *   Taxonomy term ID(s) that map to the given mapLevel or false.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getTaxonomyTermByMapLevel($mapLevel) {
    $categories = $this->getCategories();

    if (array_key_exists($mapLevel, $categories)) {
      return $categories[$mapLevel];
    }

    return FALSE;
  }

  /**
   * Return associative array for helping determine node's taxonomy reference
   *
   * Queries to get the top 2 levels of "Category" taxonomy term IDs with their
   * associated field_map_level value.
   *
   * @return array
   *   associative array [ field_map_level_value => tid ].
   */
  private function getCategories() {
    $termsArr = &drupal_static('ur_catalog_categories');

    if (!isset($termsArr)) {
      $termsArr = \Drupal::state()->get('ur_catalog_categories');

      if (!$termsArr) {
        /*
       * Raw query for reference:

        SELECT t.tid, ml.field_map_level_value
        FROM taxonomy_term_data as t
        LEFT OUTER JOIN taxonomy_term__parent AS p ON p.entity_id = t.tid
        LEFT OUTER JOIN taxonomy_term__field_map_level AS ml ON ml.entity_id = t.tid
        WHERE p.parent_target_id IN (
          SELECT t.tid
          FROM taxonomy_term_data AS t
          LEFT OUTER JOIN taxonomy_term__parent AS p ON p.entity_id = t.tid
          WHERE t.vid='categories'
          AND p.parent_target_id=0
        )
        OR p.parent_target_id=0
        AND t.vid='categories';
       */

        // Build sub-query for getting top level (parent = 0) terms.
        $parentQuery = $this->database->select('taxonomy_term_data', 't');
        $parentQuery->leftJoin('taxonomy_term__parent', 'p', 'p.entity_id = t.tid');
        $parentQuery->condition('t.vid', 'categories');
        $parentQuery->condition('p.parent_target_id', 0);
        $parentQuery->fields('t', ['tid']);

        // Now query to get all top 2 levels of category taxonomy terms with their "field_map_level" values.
        $query = $this->database->select('taxonomy_term_data', 't');
        $query->join('taxonomy_term__field_map_level', 'ml', 'ml.entity_id = t.tid');
        $query->join('taxonomy_term__parent', 'p', 'p.entity_id = t.tid');

        // This ensures we get top level (parent = 0) OR immediate children
        $orGroup = $query->orConditionGroup()
          ->condition('p.parent_target_id', $parentQuery, 'IN')
          ->condition('p.parent_target_id', 0);
        $query->condition($orGroup);

        // Ensure we're only getting "Category" taxonomy terms.
        $query->condition('t.vid', 'categories');

        // These are the columns from the database we want.
        $query
          ->fields('ml', ['field_map_level_value'])
          ->fields('t', ['tid']);

        // Return in associative array format.
        $termsArr = $query->execute()->fetchAllKeyed();

        // Store these in database cache.
        \Drupal::state()->set('ur_catalog_categories', $termsArr);
      }
    }

    return $termsArr;
  }

  /**
   * Helper function to get array of Item node data we need.
   *
   * Loading nodes is expensive and should only be done when we absolutely know
   * we need to change field values. This little helper returns an associative
   * array of the the node ID and the couple of field values we're interested in
   * checking to save us from having to load nodes individually.
   *
   * Note: Drupal stores boolean field values as 1 or 0, and on query get
   * converted to strings.
   *
   * @return mixed|array
   *   Associative array keyed by nodeId
   *   [
   *     node ID => [
   *       'nid' => node ID,
   *       'field_dal_status_value' => boolean "1" or "0",
   *       'field_dal_display_in_catalog_value' => boolean "1" or "0",
   *     ],
   *     ...
   *   ]
   */
  private function getItemNodeData() {
    $node_data = \Drupal::state()->get('ur_catalog_node_data');

    if (!$node_data) {
      $query = $this->database->select('node', 'n');

      $query->leftJoin('node__field_dal_status', 'ds', 'ds.entity_id = n.nid');
      $query->leftJoin('node__field_dal_display_in_catalog', 'dc', 'dc.entity_id = n.nid');
      $query->leftJoin('node__field_quantity_owned', 'nfqo', 'nfqo.entity_id = n.nid');
      $query->leftJoin('node__field_quantity_on_rent', 'nfqor', 'nfqor.entity_id = n.nid');

      $query->condition('n.type', 'item');

      $query->fields('n', ['nid']);
      $query->fields('ds', ['field_dal_status_value']);
      $query->fields('dc', ['field_dal_display_in_catalog_value']);
      $query->fields('nfqo', ['field_quantity_owned_value']);
      $query->fields('nfqor', ['field_quantity_on_rent_value']);

      $node_data = $query->execute()->fetchAllAssoc('nid', \PDO::FETCH_ASSOC);
      \Drupal::state()->set('ur_catalog_node_data', $node_data);
    }

    return $node_data;
  }

  /**
   * Handles business logic for updating an existing Item node.
   *
   * We will only update a node if we know that we have to update it.
   * This checks if the returned $catClass object properties match what we
   * currently have stored.
   *
   * If the values are not the same then we need to update the node and save.
   *
   * Note: On the first run all 17,350 current item nodes will have to get
   * updated as the 2 new fields (DAL Status, Display In Catalog) will be null.
   *
   * @param string $nodeId
   *   The Node's ID to check.
   * @param \Drupal\ur_frontend_api_v2\Models\CatClass $catClass
   *   The CatClass object we will be comparing against.
   *
   * @return bool
   *   Returns nothing if node->save() worked properly. False otherwise
   */
  private function updateExistingItem(string $nodeId, CatClass $catClass) {
    $node_data = $this->getItemNodeData();

    if (!isset($node_data[$nodeId])) {
      \Drupal::logger('CatalogImport')->warning("Unable to find $nodeId in the node data during catalog import.");
      return FALSE;
    }

    $status = $node_data[$nodeId]['field_dal_status_value'];
    $display_in_catalog = $node_data[$nodeId]['field_dal_display_in_catalog_value'];
    $quantity_owned = $node_data[$nodeId]['field_quantity_owned_value'];
    $quantity_on_rent = $node_data[$nodeId]['field_quantity_on_rent_value'];

    $needs_updates = FALSE;

    // If any of these values don't match what's on the node, then we're out of sync.
    if ($status != $catClass->active || $display_in_catalog != $catClass->displayInCatalog
      || $quantity_owned != $catClass->qtyOwned || $quantity_on_rent != $catClass->qtyOnRent
    ) {
      $needs_updates = TRUE;
    }

    // If we need to update; load the node, set field values, and save.
    if ($needs_updates) {
      $node = Node::load($nodeId);
      $node->set('field_dal_status', $catClass->active);
      $node->set('field_dal_display_in_catalog', $catClass->displayInCatalog);
      $node->set('field_quantity_owned', $catClass->qtyOwned);
      $node->set('field_quantity_on_rent', $catClass->qtyOnRent);
      if (!$this->shouldBePublished($catClass)) {
        $node->setPublished(FALSE);
      }

      try {
        $node->save();
      }
      catch (EntityStorageException $e) {
        \Drupal::logger('catalog_queue')->error('Unable to save existing Item: ' . $e->getMessage());
        return FALSE;
      }
    }

    // Now that we have updated the node we need to set it's "processed" value
    // to 1 since the import has touched it. Creating new ones automatically adds it.
    $this->updateProcessedValue($nodeId);
  }

  /**
   * Handles business logic for creating a new Item node from CatClass data.
   *
   * @param \Drupal\ur_frontend_api_v2\Models\CatClass $catClass
   * @return bool
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function createNewItem(CatClass $catClass) {
    $node = Node::create([
      'type' => 'item',
      'status' => 0, // unpublished by default
      'title' => $catClass->description,
      'field_plain_long' => $catClass->description,
      'field_item_cat_class_code' => $catClass->id,
      'field_dal_status' => $catClass->active,
      'field_dal_display_in_catalog' => $catClass->displayInCatalog,
      'field_highlights' => $catClass->features,
      'field_quantity_owned' => $catClass->qtyOwned,
      'field_quantity_on_rent' => $catClass->qtyOnRent,
    ]);

    // Retrieve and set image if we can get it.
    if ($file = $this->createImage($catClass->id, $catClass->imageURL)) {
      $node->field_images_unlimited[] = [
        'target_id' => $file->id(),
        'alt' => $catClass->description,
        'title' => $catClass->description,
      ];
    }

    // Don't even attempt taxonomy lookup if mapLevel is 0.
    if (!empty($catClass->mapLevel) && $term_id = $this->getTaxonomyTermByMapLevel($catClass->mapLevel)) {
      // Set initial taxonomy term to item.
      $node->set('field_item_category', $term_id);
    }

    try {
      $node->save();
    }
    catch (EntityStorageException $e) {
      \Drupal::logger('catalog_queue')->error('Unable to save new Item: ' . $e->getMessage());
      return FALSE;
    }

    // Now that we have created the node we need to set it's "processed" value
    // to 1 since the import has touched it. Creating the node automatically adds it.
    $this->updateProcessedValue($node->id());
  }

  /**
   * Downloads and stores image from CatClass image URL.
   *
   * This is effectively a copy from ORS implementation of image retrieval.
   *
   * @param string $catClass
   * @param string $imgUrl
   *
   * @return bool|\Drupal\file\FileInterface
   *   File if successfully retrieved and stored image, false otherwise
   */
  private function createImage(string $catClass, string $imgUrl) {
    $url = strtolower(trim($imgUrl));

    // @TODO: Remove error suppression from file_gets_contents
    if ($image = @file_get_contents($url)) {
      $path = "public://catclass/$catClass/";

      if (!file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
        $path = 'public://';
      }

      /** @var \Drupal\file\FileInterface $file */
      $file = file_save_data($image, $path . $catClass . '.jpg', FILE_EXISTS_REPLACE);

      if ($file instanceof FileInterface) {
        return $file;
      }

      return FALSE;
    }

    return FALSE;
  }

  /**
   * Updates the custom ur_catclass_import table to set the node ID's processed
   * column to 1 (TRUE).
   *
   * @param $nid
   *   The node ID we will be setting "processed" value for.
   */
  private function updateProcessedValue($nid) {
    $connection = \Drupal::database();

    $connection->update('ur_catclass_import')->fields(['processed' => 1])
      ->condition('entity_id', $nid)
      ->execute();
  }

}
