<?php

namespace Drupal\ur_item\Batch;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\node\Entity\Node;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\ur_frontend_api_v2\Models\CatClass;

/**
 * Class CatalogBatchProcessor
 * @package Drupal\ur_item\Batch
 */
class CatalogBatchProcessor {

  /**
   * Creates a batch array of operations
   *
   * @return array
   */
  public function createBatch():array {
    $batch = [
      'title' => t('Importing Catalog Data...'),
      'init_message' => t('Starting API call to /equipment/catclasses. There are roughtly 39,000 items from this API. This will take some time.'),
      'operations' => [],
      'finished' => 'Drupal\ur_item\Batch\CatalogBatchProcessor::batchFinished',
    ];

    // Some pre-import steps that need to be ran.
    $batch['operations'][] = [
      'Drupal\ur_item\Batch\CatalogBatchProcessor::resetImportTable',
      [],
    ];

    // This operation calls the DAL and queues the data up.
    $batch['operations'][] = [
      'Drupal\ur_item\Batch\CatalogBatchProcessor::queueData',
      [],
    ];

    // ~39,000 items total / 100 batches ~ 390 per batch.
    for ($i = 0; $i < 100; $i++) {
      $batch['operations'][] = [
        'Drupal\ur_item\Batch\CatalogBatchProcessor::batchProcess',
        [],
      ];
    }

    // Need to unpublish any nodes that were not created/updated during the import.
    $batch['operations'][] = [
      'Drupal\ur_item\Batch\CatalogBatchProcessor::unpublishMissingCatClasses',
      [],
    ];

    // Some final cleanup operations
    $batch['operations'][] = [
      'Drupal\ur_item\Batch\CatalogBatchProcessor::cleanup',
      [],
    ];

    return $batch;
  }

  /**
   * Every time we start this batch process we need to reset this custom
   * "ur_catclass_import" table to reset the "processed" field.
   * 0 means it wasn't processed, 1 means it was.
   *
   * @param $context
   */
  public static function resetImportTable(&$context):void {
    \Drupal::logger('catalog_queue')->info('Resetting import table.');
    $connection = \Drupal::database();

    $connection->update('ur_catclass_import')->fields([
      'processed' => 0,
    ])->execute();
  }

  /**
   * Handles retrieving the data from our plugin and adding items to queue.
   *
   * @param $context
   */
  public static function queueData(&$context):void {
    \Drupal::logger('catalog_queue')->info('Queueing catalog data for import.');

    try {
      $results = CatClass::index();
    }
    catch (GuzzleException $e) {
      \Drupal::logger('catalog_queue')->error('Guzzle Exception: ' . $e->getMessage());
    }
    catch (\Exception $e) {
      \Drupal::logger('catalog_queue')->error($e->getMessage());
    }

    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = \Drupal::service('queue')->get('catalog_queue');

    // Delete all current items in queue when running again.
    $queue->deleteQueue();

    foreach ($results as $catClass) {
      $queue->createItem($catClass);
    }

    $message = 'Finished getting catalog data. Beginning import';
    \Drupal::logger('catalog_queue')->info($message);
    $context['message'] = t($message);
    $maxBatchSize = ceil($queue->numberOfItems() / 100);
    $context['results']['maxBatchSize'] = $maxBatchSize;
  }

  /**
   * Creates an instance of our QueueWorker and processes items in the batch.
   *
   * @param $context
   */
  public static function batchProcess(&$context):void {
    /** @var \Drupal\ur_item\Plugin\QueueWorker\CatalogQueueProcessor $queueWorker */
    $queueWorker = \Drupal::service('plugin.manager.queue_worker')->createInstance('catalog_queue');

    $queue = \Drupal::service('queue')->get('catalog_queue');
    $currentBatchSize = min($queue->numberOfItems(), $context['results']['maxBatchSize']);

    for ($i = 0; $i < $currentBatchSize; $i++) {
      if ($item = $queue->claimItem()) {
        try {
          $queueWorker->processItem($item);
          $context['message'] = t('Updated CatClass @id.', ['@id' => $item->data->id]);
          $queue->deleteItem($item);
        }
        catch (SuspendQueueException $e) {
          $queue->releaseItem($item);
          break;
        }
        catch (\Exception $e) {
          $message = $e->getMessage();
          \Drupal::logger('catalog_queue')
            ->error('Error during catalog sync %message', ['%message' => $message]);
          $queue->releaseItem($item);
        }
      }
    }
  }

  /**
   * Unpublish any Item nodes that did not exist in the catalog import.
   *
   * This will make use of the custom "ur_catclass_import" table that is used to
   * track whether or not a node ID was updated/created during the import process.
   *
   * Any node that is left with a "processed" value of 0, will be unpubished.
   */
  public static function unpublishMissingCatClasses():void {
    \Drupal::logger('catalog_queue')->info('Unpublishing non-existant catclass nodes.');

    $query = \Drupal::database()->select('ur_catclass_import', 'ci');
    $query->fields('ci', ['entity_id']);
    $query->condition('ci.processed', 0);
    $nids = $query->execute()->fetchCol();

    // Need to actually load the node and save, otherwise the hook_entity_presave()
    // never fires.
    foreach ($nids as $nid) {
      $node = Node::load($nid);
      $node->setPublished(FALSE);

      try {
        $node->save();
      }
      catch (EntityStorageException $e) {
        \Drupal::logger('Unable to unpublish node: ' . $e->getMessage());
      }
    }

    \Drupal::logger('catalog_queue')->info('Unpublishing ' . count($nids) . ' nodes.');
  }

  /**
   * Cleanup some leftover state variables
   */
  public static function cleanup():void {
    \Drupal::logger('catalog_queue')->notice('Finished catalog import. Cleaning up now.');
    \Drupal::state()->delete('ur_catalog_categories');
    \Drupal::state()->delete('ur_catalog_node_data');
  }

  /**
   * Batch Process completion callback.
   *
   * @param $success
   * @param $results
   * @param $operations
   */
  public static function batchFinished($success, $results, $operations):void {
    if ($success) {
      $message = t('Batch Process Finished. Catalog data was imported successfully.');
    }
    else {
      $error_operation = reset($operations);
      $message = t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]);
    }

    \Drupal::messenger()->addMessage($message);
  }

}
