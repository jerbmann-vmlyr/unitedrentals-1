<?php

namespace Drupal\ur_item\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CatalogImportForm.
 *
 * @package Drupal\ur_item\Form
 */
class CatalogImportForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId():string {
    return 'catalog_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state):array {
    $form['help'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Submitting this form will trigger the import of Catalog information from /catalog/catclasses API.') . '<br/>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Import Catalog',

    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * This will follow a similar path as the locations batch import form.
   * @see \Drupal\locations\Form\BranchesImportForm::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state):void {
    /** @var \Drupal\ur_item\Batch\CatalogBatchProcessor $item_processor */
    $item_processor = \Drupal::service('ur_item.batch_processor');
    $batch = $item_processor->createBatch();

    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_admin.settings'];
  }

}
