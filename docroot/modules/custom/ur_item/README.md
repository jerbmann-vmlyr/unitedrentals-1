## UR Item Module

This module handles a batch process that creates/updates Item type nodes from DAL data. This module also defines a custom database table that is used to track which items were touched in the import and which weren't. 

##### ur_item_update_8006() & ur_item_update_8007():

Per [UR-5174](https://unitedrentals.atlassian.net/browse/UR-5174) to get all items from UR's equipment catalog into our system we will need to start storing the "mapLevel" property that comes from UR's catalog group API (/catalog/totalcontrolgroups) to our Category taxonomy terms ([UR-5144](https://unitedrentals.atlassian.net/browse/UR-5144)).

To initially set up this "mapLevel" mapping we have created a CSV that was then converted to JSON (see categories.json). We just used a simple [CSV to JSON online converter](https://www.csvjson.com/csv2json) to generate the JSON from the CSV. This JSON file contains the taxonomy term ID and the equivalent mapLevel value.

Now to actually read from the JSON file and save the values from it during a hook_update_N() requires a little bit of extra work.

Currently our deployment process runs the following drush commands in this order: `drush updb`, then `drush cim`. Normally a new field wouldn't exist until after the database updates have run. This is slightly problematic when we need to store the values from this JSON file to this new field during database updates.
 
 This is why ur_item_update_8006() exists. During this update hook it reads from the configuration files for the field and saves the new field_map_level to the Category taxonomy terms.
 
Then through ur_item_update_8007() we read from this JSON file, lookup the taxonomy term with the ID specified and set the field_map_level field to the value of the "mapLevel" from the JSON object.



