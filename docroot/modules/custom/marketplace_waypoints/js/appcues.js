/* global Appcues*/
(function (Drupal) {
  'use strict';

  Drupal.behaviors.appCues = {
    attach: function (context, settings) {
      if (window.Appcues) {
        if (settings.user.uid !== 0) {
          Appcues.identify(settings.user.uid);
        }
        else {
          Appcues.anonymous();
        }
      }
    }
  };

})(Drupal);
