<?php

namespace Drupal\ur_location_filter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Class CatClassRegionMappingForm.
 *
 * @package Drupal\ur_location_filter\Form
 */
class CatClassRegionMappingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cat_class_region_mapping_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('CatClass Region Mapping File'),
      '#required' => TRUE,
      '#default_value' => '',
      '#upload_location' => 'temporary://ur_catclass_region/',
      '#upload_validators' => ['file_validate_extensions' => ['csv txt']],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Process CatClass Region Mapping'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file = $form_state->getValue('file');
    if ($file && !empty($file[0])) {
      $csv = file_load($file[0]);
      if (!$this->processFile($csv->uri[0])) {
        \Drupal::messenger()->addMessage($this->t('Could not process file, check logs for more detail'), 'error');
      }
    }
    else {
      \Drupal::messenger()->addMessage($this->t('CSV file is required.', 'error'));
    }
  }

  /**
   * Loop through and process accordingly.
   *
   * @param mixed $file
   *   The file object.
   */
  protected function processFile($file) {
    $message = '<h2><strong>' . $this->t('SUCCESS!') . '</strong></h2><br />';
    $real_path = drupal_realpath($file->getValue('uri')['value']);
    if (file_exists($real_path)) {
      $region_ids = [];
      $fh = fopen($real_path, 'r');

      // Insert the regions.
      $regions = fgetcsv($fh);

      // Truncate the ur_catclass_region table.
      if (count($regions)) {
        db_query('TRUNCATE {ur_catclass_region}');
      }

      // Now loop through the rest of the rows and process.
      $region_cnt = 0;
      $region_exist_cnt = 0;
      for ($i = 1; $i < count($regions); $i++) {
        $region_exists = db_query('SELECT region FROM {ur_region} WHERE region = :region', [':region' => trim($regions[$i])])->fetch();
        if (empty($region_exists)) {
          $result = db_insert('ur_region')->fields(
            ['region'],
            [trim($regions[$i])]
          )->execute();
          $region_ids[] = $result;
          $region_cnt++;
        }

        // Grab the region and add it.
        else {
          $region = db_query('SELECT ur_region_id,region FROM {ur_region} WHERE region = :region', [':region' => trim($regions[$i])])->fetch();
          $region_ids[] = $region->ur_region_id;
          $region_exist_cnt++;
        }
      }

      $message .= $region_cnt . $this->t(' new Regions added.') . "<br />";
      $message .= $region_exist_cnt . $this->t(' Regions already exist.') . "<br />";

      // Loop through the rest of the rows.
      $cnt = 0;
      while (($row = fgetcsv($fh)) !== FALSE) {
        $catclass = trim($row[0]);

        for ($j = 1; $j < count($row); $j++) {
          // CatClass should be in the region AND we found the region row.
          if ($row[$j] == 1 && isset($region_ids[$j - 1])) {
            $region_id = $region_ids[$j - 1];
            db_insert('ur_catclass_region')->fields(
              ['catclass', 'ur_region_id'],
              [$catclass, $region_id]
            )->execute();
            $cnt++;
          }
        }
      }

      // Add catclass count.
      $message .= $cnt . $this->t(' CatClass to Region rows added.') . "\n";

      \Drupal::messenger()->addMessage(Markup::create($message), 'status');
      return TRUE;
    }

    return FALSE;
  }

}
