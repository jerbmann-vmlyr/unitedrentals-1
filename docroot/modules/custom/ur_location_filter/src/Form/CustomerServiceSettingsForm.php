<?php

namespace Drupal\ur_location_filter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CustomerServiceSettingsForm.
 *
 * @package Drupal\ur_location_filter\Form
 */
class CustomerServiceSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'customer_service_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ur_admin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');

    $form['customer_service_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Customer Service Number'),
      '#description' => $this->t('The 24/7 Customer Care number. This number is displayed in the footer on all pages. Please enter the number in the following format <em>111.111.1111</em>.'),
      '#maxlength' => 12,
      '#size' => 64,
      '#default_value' => $config->get('customer_service_number'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ur_admin.settings');
    $config
      ->set('customer_service_number', $form_state->getValue('customer_service_number'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
