<?php

namespace Drupal\ur_location_filter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\ur_frontend_api\Traits\ApiTrait;
use Drupal\ur_appliance\Provider\DAL;
use Drupal\ur_frontend_api_v2\Models\EquipmentOwned;

/**
 * Class DefaultController.
 *
 * @TODO: Determine if we will need this entire controller. All of this is now
 * being handled in the views argument Plugin in this module.
 *
 * @package Drupal\ur_location_filter\Controller
 */
class URLocationFilterController extends ControllerBase {
  use ApiTrait;

  /**
   * Used in filtering by location the array of items coming back.
   *
   * @param mixed $branchId
   *   The branch id for filtering.
   * @param array $rates
   *   The array of rates to filter.
   *
   * @return array
   *   The filtered array.
   */
  public static function filterItemRates($branchId, array &$rates) {
    $branch = EquipmentOwned::read($branchId, $catClass = null);
    $result = [];

    foreach ($rates as &$row) {
      $catClass = $row['catClass'];
      $parsed = array_search($catClass, array_column($branch, 'catClass'));

      // locationFiltered = true => catClass is available in the branch
      // locationFiltered = false => catClass is not available in this branch
      if (!is_bool($parsed)) {
        $row['locationFiltered'] = true;
      }
      else {
        $row['locationFiltered'] = false;
      }

      array_push($result, $row);
    }

    return $result;
  }
}
