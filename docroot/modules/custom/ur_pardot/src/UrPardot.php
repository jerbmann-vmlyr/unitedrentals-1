<?php

namespace Drupal\ur_pardot;

use GuzzleHttp\Client;
use Drupal\Core\Session\AccountProxy;

/**
 * Class UrPardot.
 *
 * @package Drupal\ur_pardot
 */
class UrPardot {
  /**
   * Aggregated carts fetched and formatted for use.
   *
   * @var array
   */
  public $cartList = [];

  /**
   * Aggregated carts fetched and formatted for use.
   *
   * @var array
   */
  public $checkoutList = [];

  /**
   * Current User object.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Http Client object.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * Link to form handler in Pardot.
   *
   * @var array
   */
  protected $formLink;

  /**
   * UrPardot constructor.
   *
   * Determine which Pardot list we are sending to based on environment.
   *
   * @param AccountProxy|null $current_user
   * @param Client $client
   */
  public function __construct(AccountProxy $current_user = NULL, Client $client) {
    if ($current_user instanceof AccountProxy) {
      $this->currentUser = $current_user;
    }

    $this->client = $client;
    $this->formLink = \Drupal::config('ur_admin.settings')->get('pardot_form_link');
  }

  /**
   * @param $params
   * @return mixed|\Psr\Http\Message\ResponseInterface
   */
  public function sendRequest($params) {
    $params = ['body' => $params, 'verify' => FALSE];

    try {
      $response = $this->client->request('POST', $this->formLink, $params);
    }
    catch (\Exception $e) {
      $msg = $e->getMessage();

      if (!empty($msg)) {
        // Do nothing on fail other than log it
        \Drupal::logger('UrPardot')->error('Error submitting to Pardot: ' . $msg);
      }
    }

    return $response;
  }

  /**
   * Send data to Pardot about a user starting into checkout.
   *
   * @param int $cart_id
   *   The cart that the checkout process is being started on.
   * @return mixed
   */
  public function sendCheckoutStart($cart_id) {
    $fields = [
      'Abandoned Cart ID' => $cart_id,
      'Abandoned Checkout ID' => $cart_id,
    ];

    if (FALSE !== $params = $this->buildParams($fields)) {
      $response = $this->sendRequest($params);

      return $response;
    }
  }

  /**
   * Send a completed transaction to Pardot.
   *
   * @param string $transaction_id
   *   The id returned from rentalman to the site on a successful transaction.
   * @param int $cart_id
   *   The completed cart ID.
   */
  public function sendCompletedSale($transaction_id, $cart_id) {
    /*
     * Overwrite old cart id with the same id plus "XD", a pattern the
     * client  needs for Pardot ops.
     */
    $fields = [
      'Abandoned Cart ID' => $cart_id,
      'Abandoned Checkout ID' => $cart_id . 'XD',
      'Sales Confirmed ID#' => $transaction_id,
    ];

    if (FALSE !== $params = $this->buildParams($fields)) {
      // Must set verify to FALSE otherwise (at least locally) we won't be able to
      // submit due to invalid certs.
      $this->sendRequest($params);
    }
  }

  /**
   * Process abandoned carts to Pardot.
   */
  public function sendAbandonedCarts() {
    $this->getAbandonedCarts();
    drush_print('Sending ' . count($this->cartList) . ' abandoned cart(s)');

    foreach ($this->cartList as $cart) {
      $query_string = http_build_query([
        'First Name' => $cart->FirstName,
        'Last Name' => $cart->LastName,
        'email' => $cart->Email,
        'Abandoned Cart ID' => $cart->CartID,
      ]);

      $this->sendRequest($query_string);
    }
  }

  /**
   * Process abandoned checkouts to Pardot.
   */
  public function sendAbandonedCheckouts() {
    $this->getAbandonedCheckouts();
    drush_print('Sending ' . count($this->checkoutList) . ' abandoned checkout(s)');

    foreach ($this->checkoutList as $checkout) {
      $query_string = http_build_query([
        'First Name' => $checkout->FirstName,
        'Last Name' => $checkout->LastName,
        'email' => $checkout->Email,
        'Abandoned Checkout ID' => $checkout->CheckoutID,
        'Abandoned Cart ID' => $checkout->CheckoutID,
      ]);

      $this->sendRequest($query_string);
    }
  }

  /**
   * Build the query string with standard and additional fields.
   *
   * @param array $fields
   *   An array of fields to add to the standard params.
   *
   * @return string
   *   The query string prepped for our request.
   */
  private function buildParams(array $fields) {
    if ($this->currentUser->isAuthenticated()) {
      $account = \Drupal::entityTypeManager()->getStorage('user')->load($this->currentUser->id());

      $params = [
        'First Name' => $account->get('field_first_name')->value,
        'Last Name' => $account->get('field_last_name')->value,
        'email' => $this->currentUser->getEmail(),
      ];

      foreach ($fields as $key => $value) {
        $params[$key] = $value;
      }

      return http_build_query($params);
    }

    return FALSE;
  }

  /**
   * Fetch abandoned carts according to business rules.
   */
  private function getAbandonedCarts() {

    /*
     * Query breakdown:
     *  1. Get all carts that are older than 30 minutes, newer
     *      than 3 days AND are attached to a uid
     *  2. Ensure that the cart HAS NOT started checkout
     *  3. Only return one cart per user, which should be their
     *      most recent. Accomplished by checking MAX on changed column
     *  4. Join on users table to fetch email and last login for each user
     *  5. Join users meta from meta tables for first and last name
     *  6. Don't return any carts that are empty by checking cart_item table
     */
    $connection = \Drupal::database();
    $carts = $connection->query('
        SELECT
          carts.id AS `CartID`,
          carts.user_id AS `UserID`,
          user_data.mail AS `Email`,
          FROM_UNIXTIME(carts.changed) AS `LastChange`,
          FROM_UNIXTIME(user_data.access) AS `LastAccess`,
          fn.field_first_name_value AS `FirstName`,
          ln.field_last_name_value AS `LastName`,
          cart_items.catClass as `CatClass`,
          cart_items.quantity as `Quantity`
        FROM {cart} AS carts
          INNER JOIN users AS users ON carts.user_id = users.uid
          LEFT JOIN users_field_data AS user_data ON users.uid = user_data.uid
          LEFT JOIN user__field_first_name AS fn ON carts.user_id = fn.entity_id
          LEFT JOIN user__field_last_name AS ln ON carts.user_id = ln.entity_id
          LEFT JOIN cart_item AS cart_items ON carts.id = cart_items.cartId
        WHERE
          carts.user_id != 0
          AND carts.changed < ( UNIX_TIMESTAMP() - (60 * 30) )
          AND carts.changed > ( UNIX_TIMESTAMP() - (60 * 60 * 24 * 7) ) 
          AND carts.started_checkout = 0
          AND carts.changed = (
            SELECT MAX(changed) FROM cart AS t2 WHERE carts.user_id = t2.user_id
          )
          AND 0 < (
            SELECT SUM(quantity) FROM cart_item AS items WHERE carts.id = items.cartId
          )'
    )->fetchAll();

    foreach ($carts as $cart) {
      $this->maybeAddToCartList($cart);
    }
  }

  /**
   * Map fields from an abandoned cart to pardot friendly result.
   *
   * @param \stdClass $cart
   *   The cart data needed for pardot mapping.
   */
  private function maybeAddToCartList(\stdClass $cart) {
    if (!isset($this->cartList[$cart->CartID])) {
      $this->cartList[$cart->CartID] = $cart;
    }

    $this->cartList[$cart->CartID]->items = [$cart->CatClass => $cart->Quantity];
  }

  /**
   * Fetch abandoned checkouts according to business rules.
   */
  private function getAbandonedCheckouts() {

    /*
     * Query breakdown:
     *  1. Get all carts that are older than 30 minutes, newer
     *      than 3 days AND are attached to a uid
     *  2. Ensure that the cart HAS started checkout
     *  3. Only return one cart per user, which should be their
     *      most recent. Accomplished by checking MAX on changed column
     *  4. Join on users table to fetch email and last login for each user
     *  5. Join users meta from meta tables for first and last name
     */
    $connection = \Drupal::database();
    $checkouts = $connection->query('
        SELECT
          carts.id AS `CheckoutID`,
          carts.user_id AS `UserID`,
          user_data.mail AS `Email`,
          FROM_UNIXTIME(carts.changed) AS `LastChange`,
          fn.field_first_name_value AS `FirstName`,
          ln.field_last_name_value AS `LastName`
        FROM cart AS carts
          INNER JOIN users AS users ON carts.user_id = users.uid
          LEFT JOIN users_field_data AS user_data ON users.uid = user_data.uid
          LEFT JOIN user__field_first_name AS fn ON carts.user_id = fn.entity_id
          LEFT JOIN user__field_last_name AS ln ON carts.user_id = ln.entity_id
        WHERE
          carts.started_checkout = 1
          AND carts.changed < ( UNIX_TIMESTAMP() - (60 * 30) )
          AND carts.changed > ( UNIX_TIMESTAMP() - (60 * 60 * 24 * 7) )'
    )->fetchAll();

    foreach ($checkouts as $checkout) {
      $this->checkoutList[] = $checkout;
    }
  }

}
