<?php

namespace Drupal\ur_pardot\Commands;

use Drupal\ur_pardot\UrPardot;
use Drush\Commands\DrushCommands;

/**
 * Class PardotCommands
 * @package Drupal\ur_pardot\Commands
 */
class PardotCommands extends DrushCommands {

  /**
   * @var \Drupal\ur_pardot\UrPardot
   */
  protected $pardot;

  /**
   * PardotCommands constructor.
   * @param \Drupal\ur_pardot\UrPardot $pardot
   */
  public function __construct(UrPardot $pardot) {
    $this->pardot = $pardot;
  }

  /**
   * Send abandoned cart data to Pardot.
   *
   * @command pardot:carts
   * @aliases pardotCarts
   * @validate-module-enabled ur_pardot
   */
  public function pardotCarts() {
    $this->pardot->sendAbandonedCarts();
  }

  /**
   * Send abandoned checkout data to Pardot.
   *
   * @command pardot:checkouts
   * @aliases pardotCheckouts
   * @validate-module-enabled ur_pardot
   */
  public function pardotCheckouts() {
    $this->pardot->sendAbandonedCheckouts();
  }

}
