# This is a sample build configuration for PHP.
# Check our guides at https://confluence.atlassian.com/x/e8YWN for more examples.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Docker Hub as your build environment.
image:
  name: docksal/cli:2.10-php7.3
  run-as-user: 1000

clone:
  depth: 1

definitions:
  caches:
    node-source: app/node_modules
  steps:
    - step: &build-package
        name: Build package
        caches:
          - composer
          - node-source
        script:
          # Initialize to mimic as if we're running docksal.
          - sudo chown -R docker:docker .
          - export PROJECT_ROOT="${BITBUCKET_CLONE_DIR}"
          - export $(grep '^DOCROOT=' .docksal/docksal.env)
          - export DOCROOT=${DOCROOT:-docroot}

          # Run init script in CI mode.
          - .docksal/commands/init ci
        artifacts:
          - "**"
    - step: &test-drupal-js
        name: Test Drupal JS
        script:
          # Initialize to mimic as if we're running docksal.
          - sudo chown -R docker:docker .
          - export PROJECT_ROOT="${BITBUCKET_CLONE_DIR}"
          - export $(grep '^DOCROOT=' .docksal/docksal.env)
          - export DOCROOT=${DOCROOT:-docroot}

          # Run service initialization to make sure we have access to all installed services.
          - .docksal/commands/init-services ci

          # Lint Drupal JS.
          - .docksal/commands/npm run gulp jsbuild
        artifacts:
          - "**"
    - step: &test-drupal-sass
        name: Test Drupal Sass
        script:
          # Initialize to mimic as if we're running docksal.
          - sudo chown -R docker:docker .
          - export PROJECT_ROOT="${BITBUCKET_CLONE_DIR}"
          - export $(grep '^DOCROOT=' .docksal/docksal.env)
          - export DOCROOT=${DOCROOT:-docroot}

          # Run service initialization to make sure we have access to all installed services.
          - .docksal/commands/init-services ci

          # Lint Drupal JS.
          - .docksal/commands/npm run gulp sassbuild
        artifacts:
          - "**"
    - step: &test-phpcs
        name: Test PHPCS
        script:
          # Initialize to mimic as if we're running docksal.
          - sudo chown -R docker:docker .
          - export PROJECT_ROOT="${BITBUCKET_CLONE_DIR}"
          - export $(grep '^DOCROOT=' .docksal/docksal.env)
          - export DOCROOT=${DOCROOT:-docroot}

          # Run service initialization to make sure we have access to all installed services.
          - .docksal/commands/init-services ci

          # Run PHPCS on modules and themes (Using phpcs.xml).
          - /home/docker/.composer/vendor/bin/phpcs -p --colors
        artifacts:
          - "**"
    - step: &test-phpstan
        name: Test PHPStan
        script:
          # Initialize to mimic as if we're running docksal.
          - sudo chown -R docker:docker .
          - export PROJECT_ROOT="${BITBUCKET_CLONE_DIR}"
          - export $(grep '^DOCROOT=' .docksal/docksal.env)
          - export DOCROOT=${DOCROOT:-docroot}

          # Run service initialization to make sure we have access to all installed services.
          - .docksal/commands/init-services ci

          # Lets find our bearings.
          - pwd && ls -lah
          - cd ${BITBUCKET_CLONE_DIR} && ls -lah

          # Run PHPStan on modules and themes.
          - /home/docker/.composer/vendor/bin/phpstan analyse ./docroot/modules/custom
          - /home/docker/.composer/vendor/bin/phpstan ./docroot/themes/custom
        artifacts:
          - "**"
    - step: &test-vuejs
        name: Test VueJS
        script:
          # Initialize to mimic as if we're running docksal.
          - sudo chown -R docker:docker .
          - export PROJECT_ROOT="${BITBUCKET_CLONE_DIR}"
          - export $(grep '^DOCROOT=' .docksal/docksal.env)
          - export DOCROOT=${DOCROOT:-docroot}

          # Run service initialization to make sure we have access to all installed services.
          - .docksal/commands/init-services ci

          # Lint Drupal JS.
          - .docksal/commands/npm run lint
        artifacts:
          - "**"
    - step: &deploy-package
        name: Deploy package
        deployment: Development
        script:
          # Initialize to mimic as if we're running docksal.
          - sudo chown -R docker:docker .
          - export PROJECT_ROOT="${BITBUCKET_CLONE_DIR}"
          - export $(grep '^DOCROOT=' .docksal/docksal.env)
          - export DOCROOT=${DOCROOT:-docroot}

          # Set variables needed for the commit.
          - COMMIT_MESSAGE=$(git log -1 --pretty=%B)
          - TAG="build-${BITBUCKET_BUILD_NUMBER}"
          - TAG_MESSAGE="Bitbucket Pipelines build ${BITBUCKET_BUILD_NUMBER}"
          - USER_NAME="bitbucket-pipelines"
          - USER_EMAIL="commits-noreply@bitbucket.org"

          # Run pre-deploy script, then remove files unwanted for deploy.
          - .docksal/commands/pre-deploy ci
          - rm -rf .docksal .git .gitignore bitbucket-pipelines.yml

          # Pull destination repository and branch (creating branch if it doesn't exist; allows for "feature" branches).
          - mkdir ~/deploy && cd ~/deploy
          - git clone $DESTINATION_REPOSITORY .
          - git config user.name $USER_NAME
          - git config user.email $USER_EMAIL
          - git checkout -B $DESTINATION_REPOSITORY_BRANCH

          # Sync build files to deploy directory, removing files with exception of the .git folder.
          - rsync -rav --delete --exclude ".git/" "${BITBUCKET_CLONE_DIR}/" .

          # Add changes to repository and let user know what all changed.
          - git add -A --force

          # Commit and tag the build.
          - git commit -m "${COMMIT_MESSAGE}" --no-verify
          - git tag -a $TAG -m "${TAG_MESSAGE}"

          # Push the updated code.
          - git push origin $DESTINATION_REPOSITORY_BRANCH --force
          - git push origin $TAG

pipelines:
  branches:
    develop:
      - step:
          script:
            - build/jenkins/trigger_build.sh
    master:
      - step: *build-package
      - step:
          <<: *deploy-package
          deployment: Development
  custom:
    feature:
      - variables:
          - name: 'DESTINATION_REPOSITORY_BRANCH'
      - step: *build-package
      - step:
          <<: *deploy-package
          deployment: Feature
  pull-requests:
    '**':
      - step: *build-package
      - parallel:
          - step: *test-drupal-js
          - step: *test-drupal-sass
          - step: *test-vuejs
          - step: *test-phpcs
          - step: *test-phpstan
