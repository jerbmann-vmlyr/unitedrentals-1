/**
 * Set Vue App Dev Server.
 *
 * This setting is used to tell Drupal to attach the local dev server library.
 * We have 2 different libraries for the vue app, one for production and one
 * local development. Locally the app server is an external node server at
 * localhost:8443/app.js, but in production is compiled down to a app.js file.
 * Refer to the 2 libraries set up in URone.libraries.yml
 * (URone/app.devserver vs URone/app) and then see the conditional set in the
 * theme's preprocess_page function.
 *
 * @see \URone_preprocess_page()
 */
$settings['app_dev_server'] = TRUE;
