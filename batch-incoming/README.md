# UR Batch Incoming File Folder #
Nightly uploads should be made here via SFTP or rsync.

For batch processing we look for the following files:
- `UR_CatClass.csv`

These are expected but not yet supported/processed:
- `UR_Hours_Ops.csv`
- `UR_Holiday.csv`
- `UR_CatClass_Location.csv`
- `UR_Suggested_Equip.csv`
