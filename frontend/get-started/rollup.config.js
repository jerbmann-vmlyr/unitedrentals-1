import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';
import postcss from 'rollup-plugin-postcss';
import cssnano from 'cssnano';
const tailwindcss = require('tailwindcss');
const cssvariables = require('postcss-css-variables');

const cssDestination = '../../docroot/themes/custom/slab/css';
const jsDestination = '../../docroot/themes/custom/slab/js';

const cssBundle = {
  input: './styles/main.css',
  output: {
    file: `${cssDestination}/odo.css`,
    format: 'umd',
    name: 'odo',
  },
  plugins: [
    postcss({
      extract: true,
      plugins: [
        require('postcss-import'),
        require('postcss-each'),
        cssvariables(),
        tailwindcss('./tailwind.config.js'),
        require('postcss-nested'),
        require('postcss-sass-color-functions'),

        require('autoprefixer')({
          browsers: ['last 2 version'],
        }),
        cssnano({
          preset: 'default',
        }),
      ],
    }),
  ],
};

const jsPlugins = [
  resolve(), // so Rollup can find `ms`
  commonjs(), // so Rollup can convert `ms` to an ES module
  babel({ exclude: ['node_modules/**'] }),
];

const jsFiles = {
  emphasis: 'effects/emphasis.js',
  'grid-rays': 'effects/grid-rays.js',
  parallax: 'effects/parallax.js',
  reveal: 'effects/reveal.js',
};

const jsBundles = Object.entries(jsFiles).map(([name, input]) => ({
  input,
  output: {
    file: `${jsDestination}/${name}.bundle.js`,
    format: 'umd',
  },
  plugins: jsPlugins,
}));

export default [cssBundle, ...jsBundles];
