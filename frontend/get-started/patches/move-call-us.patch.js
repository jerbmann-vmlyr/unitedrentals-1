/**
 * The .parallax-wrapper class imposes the CSS `perspective` property,
 * which creates a new CSS stacking context and therefore breaks any fixed
 * position elements.
 *
 * The <call-us> component's position is fixed. This patch pulls that block out
 * its current wrapper and moves it to the parent wrapper.
 */
export const moveCallUsComponent = () => {
  // If something goes wrong here, it must not effect the execution of the calling script!
  try {
    const callUsEl = document.getElementById('block-urcallusblock');
    const { parentNode } = callUsEl;
    parentNode.removeChild(callUsEl);
    parentNode.parentNode.appendChild(callUsEl);
  }
  catch (error) {
    console.warn(`The move <call-us> patch failed.`, error);
  }
}
