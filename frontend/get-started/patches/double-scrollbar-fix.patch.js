/**
 * An iframe and img tag placed by marketing scripts are forcing a
 * double scrollbar to appear. This patch addes a css style block to make
 * those tags position: fixed; so it doesnt overflow the screen.
 */
export const fixMarketingTags = () => {
  // If something goes wrong here, it must not effect the execution of the calling script!
  try {
    const fixMarketingCss = document.createElement('style');
    fixMarketingCss.type = 'text/css';

    const fixMarketingStyles = 'body > iframe, body > img { position: fixed; }';

    if (fixMarketingCss.styleSheet) {
      fixMarketingCss.styleSheet.cssText = fixMarketingStyles;
    } else {
      fixMarketingCss.appendChild(document.createTextNode(fixMarketingStyles));
    }

    document.querySelector('head').appendChild(fixMarketingCss);
  } catch (error) {
    console.error('Fix marketing CSS patch failed.', error);
  }
};
