import { initEffects } from './utils';

const revealEffect = {
  name: 'reveal',
  className: 'reveal',
  duration: 333,
  effectDefaults: {
    trigger: 'load',
    unitDelay: 0,
  },
};

const initReveal = () =>
  document
    .querySelectorAll('[component-root]')
    .forEach(initEffects(revealEffect));

if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window) {
  window.addEventListener('load', initReveal);
} else {
  document.querySelector('html').classList.add('no-intersection-observer');
}
