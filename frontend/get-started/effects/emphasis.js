import { initEffects } from './utils';

const emphasisEffect = {
  name: 'emphasis',
  className: 'emphasis',
  duration: 333,
  effectDefaults: {
    trigger: 'load',
    unitDelay: 0,
  },
};

const initEmphasis = () =>
  document
    .querySelectorAll('[component-root]')
    .forEach(initEffects(emphasisEffect));

if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window) {
  window.addEventListener('load', initEmphasis);
}
