/**
 * TODO!
 *
 * These are copied from tailwind.config.js and they should ultimately be defined
 * and imported from a site-wide shared file.
 */
// eslint-disable-next-line import/prefer-default-export
export const COLORS = {
  transparent: 'transparent',

  // Blue
  blue: '#0050a9',
  'blue-lowlight': '#002f55',
  'blue-highlight': '#008cff',
  'blue-subtle': '#edf8ff',
  // Orange
  orange: '#f99222',
  'orange-lowlight': '#b03712',
  'orange-highlight': '#ffcf3d',
  'orange-subtle': '#fff2ec',
  // Bright Red
  'bright-red': '#e10000',
  'bright-red-lowlight': '#840000',
  'bright-red-highlight': '#ff4f4f',
  'bright-red-subtle': '#ffecec',
  // Green
  green: '#48bd00',
  'green-lowlight': '#237000',
  // Grey
  white: '#ffffff',
  'grey-2': '#f9f9f9',
  'grey-5': '#f2f2f2',
  'grey-11': '#dde0e2',
  'grey-38': '#91969b',
  'grey-69': '#414b50',
  black: '#000000',
  text: '#414b50',
};
