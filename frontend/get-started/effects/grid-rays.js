import { COLORS } from './colors';

const MINIMUM_RAY_SPACING = 220;

const DEFAULT_RAY_OPTIONS = {
  beamColor: COLORS['grey-38'],
  trackColor: COLORS['grey-5'],
};

const svgNS = 'http://www.w3.org/2000/svg';

const createSvg = () => {
  const svg = document.createElementNS(svgNS, 'svg');
  svg.setAttribute('version', '1.1');
  svg.setAttribute('xmlns', svgNS);
  svg.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
  svg.setAttribute('xml:space', 'preserve');
  svg.setAttribute('preserveAspectRatio', 'none');
  svg.classList.add('grid-rays');
  return svg;
};

const getBeamColorFromAttribute = el => {
  const color = el.getAttribute('grid-rays-beam-color') || 'grey-11';
  if (!Object.keys(COLORS).includes(color)) {
    throw new Error(
      `"${color}" is not a valid value for the [grid-rays-beam-color] attribute!`,
    );
  }
  return COLORS[color];
};

const horizontalPathFactory = w => y => {
  const path = document.createElementNS(svgNS, 'path');
  path.setAttribute('d', `M 0 ${y} h ${w}`);
  return path;
};

const verticalPathFactory = h => x => {
  const path = document.createElementNS(svgNS, 'path');
  path.setAttribute('d', `M ${x} 0 v ${h} 0`);
  return path;
};

/**
 * @interface RayOptions {
 *  beamColor: string;
 *  trackColor: string;
 * }
 */
const createRayGroup = (position, pathCreator, rayOptions) => {
  const g = document.createElementNS(svgNS, 'g');
  g.classList.add('ray');
  const track = pathCreator(position);
  track.classList.add('track');
  track.setAttribute('stroke', rayOptions.trackColor);
  const beam = pathCreator(position);
  beam.classList.add('beam');
  beam.setAttribute('stroke', rayOptions.beamColor);
  g.appendChild(track);
  g.appendChild(beam);
  return g;
};

const getRayCounts = gridParent => {
  const parsedHorizontalCount = Number.parseInt(
    gridParent.getAttribute('grid-rays-horizontal-count'),
    10,
  );
  const parsedVerticalCount = Number.parseInt(
    gridParent.getAttribute('grid-rays-vertical-count'),
    10,
  );

  return {
    horizontalRayCount:
      !isNaN(parsedHorizontalCount) && parsedHorizontalCount > 0
        ? parsedHorizontalCount
        : 0,
    verticalRayCount:
      !isNaN(parsedVerticalCount) && parsedVerticalCount > 0
        ? parsedVerticalCount
        : 0,
  };
};

const resizeViewBox = grid => {
  const { height, width } = grid.parentElement.getBoundingClientRect();
  grid.setAttributeNS(
    svgNS,
    'viewBox',
    `0 0 ${Math.ceil(width)} ${Math.ceil(height)}`,
  );
  return grid;
};

const initRays = () => {
  document.querySelectorAll('[grid-rays]').forEach(root => {
    const grid = createSvg();
    root.prepend(grid);
    resizeViewBox(grid);
    const rayOptions = {
      ...DEFAULT_RAY_OPTIONS,
      beamColor: getBeamColorFromAttribute(root),
    };

    const { height, width } = grid.getBoundingClientRect();
    const { horizontalRayCount, verticalRayCount } = getRayCounts(root);

    if (horizontalRayCount > 0) {
      const createHorizontalPath = horizontalPathFactory(width);
      const ySpacing = Math.max(
        height / horizontalRayCount,
        MINIMUM_RAY_SPACING,
      );

      [...Array(horizontalRayCount)]
        .map((_, i) => i * ySpacing + ySpacing / 2)
        .filter(y => y < height)
        .forEach(y => {
          grid.appendChild(createRayGroup(y, createHorizontalPath, rayOptions));
        });
    }

    if (verticalRayCount > 0) {
      const createVerticalPath = verticalPathFactory(height);
      const xSpacing = Math.max(width / verticalRayCount, MINIMUM_RAY_SPACING);

      [...Array(verticalRayCount)]
        .map((_, i) => i * xSpacing + xSpacing / 2)
        .filter(x => x < width)
        .forEach(x => {
          grid.appendChild(createRayGroup(x, createVerticalPath, rayOptions));
        });
    }
  });
};

if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window) {
  window.addEventListener('load', initRays);
}
