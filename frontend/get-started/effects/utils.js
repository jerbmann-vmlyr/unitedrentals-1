const isDesktop = () => matchMedia('(min-width: 768px)').matches;

const getEffectTrigger = (effectName, el) => {
  const trigger = el.getAttribute(`${effectName}-trigger`) || 'load';
  if (!['load', 'scroll'].includes(trigger)) {
    throw new Error(`"${trigger}" is not a valid value for the [${effectName}-trigger] attribute!`);
  }
  return trigger;
};

const getScrollTargetAttribute = (effectName, el) => {
  const target = el.getAttribute(`${effectName}-trigger-scroll-target`) || 'self';
  if (!['self', 'parent', 'root'].includes(target)) {
    throw new Error(`"${target}" is not a valid value for the [${effectName}-trigger-scroll-target] attribute!`);
  }
  return target;
};

const getScrollThreshold = (effectName, el) => {
  const parsedThreshold = Number.parseInt(el.getAttribute(`${effectName}-trigger-scroll-threshold`), 10);
  return !isNaN(parsedThreshold) && parsedThreshold >= 0 && parsedThreshold <= 100
    ? parsedThreshold
    : 100;
};

const getUnitDelay = (effectName, el) => {
  const [
    desktopDelay,
    mobileDelay = desktopDelay,
  ] = (el.getAttribute(`${effectName}-delay`) || '')
        .replace(/ /g, '')
        .split(',')
        .map(delayStr => Number.parseInt(delayStr, 10))
        .map(parsedDelay => !isNaN(parsedDelay) && parsedDelay > 0 ? parsedDelay : 0);

  return isDesktop() ? desktopDelay : mobileDelay;
};

/**
 * interface EffectDefinition {
 *   name: string;
 *   className: string;
 *   duration: number; // milliseconds
 *   defaultConfig: object;
 * }
 *
 * interface EffectRunner extends EffectDefinition {
 *   el: HTMLElement;
 *   rootEl: HTMLElement;
 *   unitDelay: number; // 0 - N
 *   direction: 'up' | 'down' | 'left' | 'right';
 *   trigger: 'load' | 'scroll';
 *   scrollTarget: 'self' | 'parent' | 'root;
 *   scrollThreshold: number; // 0 - 100
 *   run: () => void;
 *   runImmediate: () => void;
 * }
 */
const createEffectRunner = (effectDefinition, rootEl) => (el) => {
  const unitDelay = getUnitDelay(effectDefinition.name, el);
  const trigger = getEffectTrigger(effectDefinition.name, el);
  const scrollThreshold = getScrollThreshold(effectDefinition.name, el) / 100;
  const scrollTarget = (() => {
    switch (getScrollTargetAttribute(effectDefinition.name, el)) {
      case 'root': return rootEl;
      case 'parent': return el.parentElement;
      default: return el;
    }
  })();

  return {
    ...effectDefinition.effectDefaults,
    el,
    run: () => setTimeout(() => {
      el.classList.add(effectDefinition.className);
    }, unitDelay * effectDefinition.duration),
    runImmediate: () => el.classList.add(effectDefinition.className),
    rootEl,
    scrollTarget,
    scrollThreshold,
    trigger,
    unitDelay,
  };
};

const attachIntersectionObservers = (effectRunner) => {
  const observerCallback = ([entry], observer) => {
    /**
     * Run effect if:
     * - The scroll threshold has been crossed
     * - The bounding rectangle of the target is above the top of the viewport,
     *   this happens when the site is reloaded while the user has already
     *   scrolled some amount down the page
     */
    const thresholdCrossed = entry && entry.intersectionRatio >= effectRunner.scrollThreshold;
    const aboveViewport = entry && entry.boundingClientRect.top < 0;
    if (thresholdCrossed || aboveViewport) {
      /** Once the effect has been run, no need to observe anymore; disconnect the observer */
      observer.disconnect();
      if (aboveViewport) effectRunner.runImmediate();
      else effectRunner.run();
    }
  };

  const observerOptions = {
    threshold: effectRunner.scrollThreshold,
  };

  const observer = new IntersectionObserver(observerCallback, observerOptions);
  observer.observe(effectRunner.scrollTarget);
};

// eslint-disable-next-line import/prefer-default-export
export const initEffects = effectDefinition => (rootEl) => {
  /**
   * Create the effect runners for rootEl and its children
   */
  const effectRunners = Array.from(rootEl.querySelectorAll(`[${effectDefinition.name}]`)).map(createEffectRunner(effectDefinition, rootEl));

  /**
   * Automatically run the effects of the elements whose trigger is on "load"
   */
  effectRunners
    .filter(({ trigger }) => trigger === 'load')
    .forEach(({ run }) => run());

  /**
   * Filter down to the effect runners whose trigger is on "scroll"
   * and finally have their intersection observers connected
   */
  effectRunners
    .filter(({ trigger }) => trigger === 'scroll')
    .forEach(attachIntersectionObservers);
};
