import { moveCallUsComponent } from '../patches/move-call-us.patch';
import { fixMarketingTags } from '../patches/double-scrollbar-fix.patch';

const enableParallax = () => {
  // Add the .parallax-wrapper class to the root #app element
  document.getElementById('app').classList.add('parallax-wrapper');

  /**
   * Apply a fix for the Firefox perspective bug
   * http://blog.joelambert.co.uk/2012/02/13/3d-transformations-with-firefox-10/
   *
   * Set the transform-style to `preserve-3d` of all of the elements from the
   * parent `.parallax-wrapper` down to the element with `.parallax` class.
   */
  // document.querySelectorAll('.parallax').forEach(el => {
  //   let parent = el.parentElement;
  //   while (parent && !parent.classList.contains('parallax-wrapper')) {
  //     parent.style.MozTransformStyle = 'preserve-3d';
  //     parent = parent.parentElement;
  //   }
  // });

  /**
   * The bit below is a complete hack. Firefox, iOS and Edge both claim to
   * support certin css properties but do not. Using crappy feature detection
   * and UA sniffing to blunt force them into looking decent.
   * I am sorry for hurting your eyes.
   */
  if (
    typeof document.querySelector('.parallax').style.MozTransform ===
      'string' ||
    typeof document.querySelector('.parallax').style.msGridRow === 'string' ||
    (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream)
  ) {
    document.querySelectorAll('.parallax').forEach(el => {
      el.style.transform = 'none';
    });
  }

  // Apply patches
  moveCallUsComponent();
  fixMarketingTags();
};

if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window) {
  window.addEventListener('load', enableParallax);
}
