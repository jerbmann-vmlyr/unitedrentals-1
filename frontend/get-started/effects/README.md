# Effects Framework

`[component-root=<name>]` must be added to the root of all "components". The name can be whatever you want.

```
<section component-root="hero">
  ...
</section>
```

---

## Reveal `[reveal]`

### Delay `[reveal-delay=<number>]`

Delays are a **unitless** value that act as a multiplier of a time constant defined in JS.

Default reveal delay unit is `0`.

### Direction `[reveal-direction=<'up' | 'down' | 'left' | 'right'>]`

Default reveal direction is `down`.

### Trigger `[reveal-trigger=<'load' | 'scroll'>]

The type of event that triggers the element reveal.

Default trigger is `load`.

### Scroll trigger target `[reveal-trigger-scroll-target=<'self' | 'parent' | 'root'>]

**Only applies to the `scroll` reveal trigger type.**

The element whose scroll state will be observed.

Default target is `self`.

### Scroll trigger threshold `[reveal-trigger-scroll-threshold=<0 - 100>]

**Only applies to the `scroll` reveal trigger type.**

The percentage (number between 0 and 100) of the scroll target element that must be visible for the reveal to be triggered.

Default threshold is `100`.

---

## Grid Rays `[grid-rays]`

Should only be applied to component roots.

### Horizontal Ray Count `[grid-rays-horizontal-count=<number>]`

Default horizontal ray count is `0`.

### Vertical Ray Count `[grid-rays-vertical-count=<number>]`

Default vertical ray count is `0`.
