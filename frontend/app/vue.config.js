module.exports = {
  outputDir: '../../docroot/themes/custom/unitedrentals/dist/vue',
  productionSourceMap: false,
  runtimeCompiler: true,
  devServer: {
    hot: true,
    host: 'localhost',
    port: 8443,
    disableHostCheck: true,
  },
  pages: {
    general: {
      entry: 'src/pages/general/main.ts',
      title: 'General Bundle',
    },
  },
};
