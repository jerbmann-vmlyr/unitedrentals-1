import Vue from 'vue';
import store from './store';
import HelloWorld from '@/pages/general/components/HelloWorld.vue';

Vue.config.productionTip = false;
Vue.config.devtools = new RegExp('dev|qa|stage|docksal').test(window.location.host);

new Vue({
  store,
  components: {
    HelloWorld,
  },
  comments: true,
}).$mount('#app');
