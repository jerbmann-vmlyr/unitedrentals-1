<?php

/**
 * @file
 * Config settings for authentication sources for simplesaml.
 */

$config = array(

  // This is a authentication source which handles admin authentication.
  'admin' => array(
    // The default is to use core:AdminPassword, but it can be replaced with
    // any authentication source.

    'core:AdminPassword',
  ),

  /**
   * ================================================================
   * Acquia Cloud sources
   * ================================================================
   */

  // Acquia Cloud: DEV0
  'dev0' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://dev0.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  // Acquia Cloud: DEV1
  'dev1' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://dev1.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  // Acquia Cloud: DEV2
  'dev2' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://dev2.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  // Acquia Cloud: DEV3
  'dev3' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://dev3.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  // Acquia Cloud: QA1
  'qa1' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://qa1.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  // Acquia Cloud: QA2
  'qa2' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://qa2.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  // Acquia Cloud: TEST
  'stage' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://stage.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  // Acquia Cloud: TEST2
  'sandbox' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://sandbox.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  // Acquia Cloud: PREPROD
  'preprod' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://preprod.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://login.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  // Acquia Cloud: PROD
  'prod' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://www.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://login.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  /**
   * ================================================================
   * Foreign language sources
   * ================================================================
   */

  'test_fr' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://frca-unitedrentals-kelex.onelink-translations.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  'test_es' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://esus-unitedrentals-kelex.onelink-translations.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  'prod_fr' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://fr.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://fr-login.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  'prod_es' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://es.unitedrentals.com/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://es-login.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

  /**
   * ================================================================
   * Local development sources
   * ================================================================
   */

  'local' => array(
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
    'entityID' => 'https://unitedrentals.docksal/simplesaml/module.php/saml/sp/metadata.php',

    // The entity ID of the IdP this should SP should contact.
    // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
    'idp' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin discovery service will be used.
    'discoURL' => NULL,
  ),

);
