<?php


if (!function_exists('acquia_session_store_config')) {
  function acquia_session_store_config($config, $ah_options) {
    if ($ah_options['session_store'][$ah_options['env']] == 'memcache') {
      $config = mc_session_store($config);
    }
    else if ($ah_options['session_store'][$ah_options['env']] == 'database') {
      $config = sql_session_store($config, $ah_options['database_name']);
    }

    return $config;
  }
}

if (!function_exists('acquia_logging_config')) {
  function acquia_logging_config($config) {
    // Set log location, as specified by Acquia
    $config['logging.handler'] = 'file';
    $config['loggingdir'] = dirname($_ENV['ACQUIA_HOSTING_DRUPAL_LOG']);
    $config['logging.logfile'] = 'simplesamlphp-' . date("Ymd") . '.log';

    return $config;
  }
}

if (!function_exists('mc_session_store')) {
  function mc_session_store($config) {
    $config['store.type'] = 'memcache';
    $config['memcache_store.servers'] = mc_info();

    return $config;
  }
}

if (!function_exists('mc_info')) {
  function mc_info() {
    $creds_json = file_get_contents('/var/www/site-php/' . $_ENV['AH_SITE_NAME'] . '/creds.json');
    $creds = json_decode($creds_json, TRUE);
    $mc_server = array();
    $mc_pool = array();

    foreach ($creds['memcached_servers'] as $fqdn) {
      $mc_server['hostname'] = preg_replace('/:.*?$/', '', $fqdn);
      array_push($mc_pool, $mc_server);
    }

    return array($mc_pool);
  }
}

if (!function_exists('sql_session_store')) {
  function sql_session_store($config, $database_name) {
    $creds = db_info($database_name);
    $config['store.type'] = 'sql';
    $config['store.sql.dsn'] = sprintf('mysql:host=%s;port=%s;dbname=%s', $creds['host'], $creds['port'], $creds['name']);
    $config['store.sql.username'] = $creds['user'];
    $config['store.sql.password'] = $creds['pass'];
    $config['store.sql.prefix'] = 'simplesaml';

    return $config;
  }
}

if (!function_exists('db_info')) {
  function db_info($db_name) {
    $creds_json = file_get_contents('/var/www/site-php/' . $_ENV['AH_SITE_NAME'] . '/creds.json');
    $databases = json_decode($creds_json, TRUE);
    $db = $databases['databases'][$db_name];
    $db['host'] = ($host = ah_db_current_host($db['db_cluster_id'])) ? $host : key($db['db_url_ha']);

    return $db;
  }
}

if (!function_exists('ah_db_current_host')) {
  function ah_db_current_host($db_cluster_id) {
    require_once("/usr/share/php/Net/DNS2_wrapper.php");

    try {
      $resolver = new Net_DNS2_Resolver(array('nameservers' => array('127.0.0.1', 'dns-master')));
      $response = $resolver->query("cluster-{$db_cluster_id}.mysql", 'CNAME');
      $cached_id = $response->answer[0]->cname;
    }
    catch (Net_DNS2_Exception $e) {
      $cached_id = "";
    }

    return $cached_id;
  }
}

