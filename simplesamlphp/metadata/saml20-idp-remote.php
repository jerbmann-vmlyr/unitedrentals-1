<?php
/**
 * SAML 2.0 remote IdP metadata for simpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://rnd.feide.no/content/idp-remote-metadata-reference
 */

/*
 * Guest IdP. allows users to sign up and register. Great for testing!
$metadata['https://openidp.feide.no'] = array(
	'name' => array(
		'en' => 'Feide OpenIdP - guest users',
		'no' => 'Feide Gjestebrukere',
	),
	'description'          => 'Here you can login with your account on Feide RnD OpenID. If you do not already have an account on this identity provider, you can create a new one by following the create new account link and follow the instructions.',

	'SingleSignOnService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SingleLogoutService.php',
	'certFingerprint'      => 'c9ed4dfb07caf13fc21e0fec1572047eb8a7a4cb'
);
 */

$metadata['https://devlogin.ur.com/public/saml2/idp/metadata.php'] = array (
  'entityid' => 'https://devlogin.ur.com/public/saml2/idp/metadata.php',
  'contacts' =>
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' =>
  array (
    0 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://devlogin.ur.com/public/saml2/idp/SSOService.php',
    ),
  ),
  'SingleLogoutService' =>
  array (
    0 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://devlogin.ur.com/public/saml2/idp/SingleLogoutService.php',
    ),
  ),
  'ArtifactResolutionService' =>
  array (
  ),
  'keys' =>
  array (
    0 =>
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIID+TCCAuGgAwIBAgIJALeW4PNH/rPaMA0GCSqGSIb3DQEBCwUAMIGSMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDETMBEGA1UEAwwKaWRwLnVyLmNvbTEaMBgGCSqGSIb3DQEJARYLdGVjaEB1ci5jb20wHhcNMTYwMTI3MDYzNjI5WhcNMjYwMTI0MDYzNjI5WjCBkjELMAkGA1UEBhMCVVMxFDASBgNVBAgMC0Nvbm5lY3RpY3V0MRAwDgYDVQQHDAdTaGVsdG9uMR0wGwYDVQQKDBRVbml0ZWQgUmVudGFscywgSW5jLjELMAkGA1UECwwCSVQxEzARBgNVBAMMCmlkcC51ci5jb20xGjAYBgkqhkiG9w0BCQEWC3RlY2hAdXIuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwgaM1gYmc94N3nryGJSlXDRvXwec436NRJGiNDOgcAWPKQvoImlttHUFE+7M8xj9h7HfPbAxmL9U1bibqPAgN9az8G66VSR62Kuvjx5eIPTNhVUPFn5lA1CwmlKiGMMPgdc8ZILNieI47J9b27Bvl9pV1K8dcSewlMRKgqiAHd3vFH1GXkDpHF4OjeVFqABLvOGhh5j6Q7yA1N4mmtf8vcPV80FvHQtHxDARFNHCCsglLNK9c1NK3akVrJ8dUHYs71mEkQEpx+F3Xnxye/+WUPCJlxHvioO1w6XbScqrqLTT5HurUkNyYxrM4nuI/+g2ejKmIuZ+59VP7/Dr2AnZzwIDAQABo1AwTjAdBgNVHQ4EFgQUWNNp41lRy6wgLsl4BeiY3ANGejcwHwYDVR0jBBgwFoAUWNNp41lRy6wgLsl4BeiY3ANGejcwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAOTB+YBc03ct4rG7CN/K/htXRn9C+0xDB2zXmC8YpxShFMnW4YXbxrglzscdksNkn5CrteoikuJWjlNQMFymr+qIc+DVC2UMv45XD1M9UHVUbcG72SLXdBSMl3iQYQqRcl8FEGs5CbIq3HcLzXLzSdYsph+k3845odZ38UlC12Dn+ay+Y16SEHQXKcJbn74/AzrDNKu8X6uJT1j8E5eKVtaAML7T37LlqCmHUZczWcaUHSSS3wjU/DdLCcBXPlCBeGqN2tpjAVSOLjglSptzQIni1UBX3mbkclRYsRafUItzhreQqXDRhtZIihVO9OyTPu4hyNCxIpwY+EmiG+CrFOA==',
    ),
    1 =>
    array (
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIID+TCCAuGgAwIBAgIJALeW4PNH/rPaMA0GCSqGSIb3DQEBCwUAMIGSMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDETMBEGA1UEAwwKaWRwLnVyLmNvbTEaMBgGCSqGSIb3DQEJARYLdGVjaEB1ci5jb20wHhcNMTYwMTI3MDYzNjI5WhcNMjYwMTI0MDYzNjI5WjCBkjELMAkGA1UEBhMCVVMxFDASBgNVBAgMC0Nvbm5lY3RpY3V0MRAwDgYDVQQHDAdTaGVsdG9uMR0wGwYDVQQKDBRVbml0ZWQgUmVudGFscywgSW5jLjELMAkGA1UECwwCSVQxEzARBgNVBAMMCmlkcC51ci5jb20xGjAYBgkqhkiG9w0BCQEWC3RlY2hAdXIuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwgaM1gYmc94N3nryGJSlXDRvXwec436NRJGiNDOgcAWPKQvoImlttHUFE+7M8xj9h7HfPbAxmL9U1bibqPAgN9az8G66VSR62Kuvjx5eIPTNhVUPFn5lA1CwmlKiGMMPgdc8ZILNieI47J9b27Bvl9pV1K8dcSewlMRKgqiAHd3vFH1GXkDpHF4OjeVFqABLvOGhh5j6Q7yA1N4mmtf8vcPV80FvHQtHxDARFNHCCsglLNK9c1NK3akVrJ8dUHYs71mEkQEpx+F3Xnxye/+WUPCJlxHvioO1w6XbScqrqLTT5HurUkNyYxrM4nuI/+g2ejKmIuZ+59VP7/Dr2AnZzwIDAQABo1AwTjAdBgNVHQ4EFgQUWNNp41lRy6wgLsl4BeiY3ANGejcwHwYDVR0jBBgwFoAUWNNp41lRy6wgLsl4BeiY3ANGejcwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAOTB+YBc03ct4rG7CN/K/htXRn9C+0xDB2zXmC8YpxShFMnW4YXbxrglzscdksNkn5CrteoikuJWjlNQMFymr+qIc+DVC2UMv45XD1M9UHVUbcG72SLXdBSMl3iQYQqRcl8FEGs5CbIq3HcLzXLzSdYsph+k3845odZ38UlC12Dn+ay+Y16SEHQXKcJbn74/AzrDNKu8X6uJT1j8E5eKVtaAML7T37LlqCmHUZczWcaUHSSS3wjU/DdLCcBXPlCBeGqN2tpjAVSOLjglSptzQIni1UBX3mbkclRYsRafUItzhreQqXDRhtZIihVO9OyTPu4hyNCxIpwY+EmiG+CrFOA==',
    ),
  ),
);

$metadata['https://login.ur.com/public/saml2/idp/metadata.php'] = array (
  'entityid' => 'https://login.ur.com/public/saml2/idp/metadata.php',
  'contacts' =>
  array (
    0 =>
    array (
      'contactType' => 'technical',
      'givenName' => 'Administrator',
      'emailAddress' =>
      array (
        0 => 'tech@ur.com',
      ),
    ),
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' =>
  array (
    0 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://login.ur.com/public/saml2/idp/SSOService.php',
    ),
  ),
  'SingleLogoutService' =>
  array (
    0 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://login.ur.com/public/saml2/idp/SingleLogoutService.php',
    ),
  ),
  'ArtifactResolutionService' =>
  array (
  ),
  'keys' =>
  array (
    0 =>
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIID/TCCAuWgAwIBAgIJAJAL2EVDNDFmMA0GCSqGSIb3DQEBCwUAMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTAeFw0xNjA0MDEyMjUyMTRaFw0yNjAzMzAyMjUyMTRaMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKcmXSJ5XooXB720GLJx0zgK8eejwdD7w82drQyNijTELXZ+tI75PPAIzUSnHGVqK62m+3/qJ32xrjbhw7CI0FXUwKRBG2iebKKIXeYzWyzxVB1rBANoeb3kMH/HH49TR5SRPbPDMEj1loOjrFspc1JEmajCfVBp41sx8TlK7h3psHX7HXf9fL6oBeW+mbOiGpB2zepTJdqyjkGE2Xi8eR276eKf9znC9xErnqtP9HybrFSQIpuwNNrVMq0ZmblzxuyBvPxtf//gtiAhK34UTiTnfvNMGFSupXGyAPpQx1b9p2g6QWu4q7w0td2vYaHuQHzWCbDYffOBVWugJuOPSQMCAwEAAaNQME4wHQYDVR0OBBYEFCYgg7LrCAOD+1roytA/Yq5d5jKtMB8GA1UdIwQYMBaAFCYgg7LrCAOD+1roytA/Yq5d5jKtMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAB1PX16UTyYhEJdQseI3LLLQJWatlLGXGc7UgJOl7ExKcE0s8YfvACcslaLXuocSo2bKSOREbuBRTeogr+mLvPWXcvTDcC/d2aRV5hVq61Lnv1KT8Z69FtJ57sK7EEWDZuEQFI1GlPAQkZOCvdhVNsz21V7gVdephZXpIkWOo8f8flA/1avTD5KJsr85z5q8jJwfJOUr2PXe4MwBYPFdP2VZbi4KLhO6h7QUcjQzvOoSDOAUvDbYPGlg+yEsIQP/Ux7FjzLwlaBcFj/4Al1rgy18APxeyJHBFIrUCbMlx2hhYFauNFqPSz3GCLIT/qJomuhSUdM1EY7Os2KRIjNZ32o=',
    ),
    1 =>
    array (
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIID/TCCAuWgAwIBAgIJAJAL2EVDNDFmMA0GCSqGSIb3DQEBCwUAMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTAeFw0xNjA0MDEyMjUyMTRaFw0yNjAzMzAyMjUyMTRaMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKcmXSJ5XooXB720GLJx0zgK8eejwdD7w82drQyNijTELXZ+tI75PPAIzUSnHGVqK62m+3/qJ32xrjbhw7CI0FXUwKRBG2iebKKIXeYzWyzxVB1rBANoeb3kMH/HH49TR5SRPbPDMEj1loOjrFspc1JEmajCfVBp41sx8TlK7h3psHX7HXf9fL6oBeW+mbOiGpB2zepTJdqyjkGE2Xi8eR276eKf9znC9xErnqtP9HybrFSQIpuwNNrVMq0ZmblzxuyBvPxtf//gtiAhK34UTiTnfvNMGFSupXGyAPpQx1b9p2g6QWu4q7w0td2vYaHuQHzWCbDYffOBVWugJuOPSQMCAwEAAaNQME4wHQYDVR0OBBYEFCYgg7LrCAOD+1roytA/Yq5d5jKtMB8GA1UdIwQYMBaAFCYgg7LrCAOD+1roytA/Yq5d5jKtMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAB1PX16UTyYhEJdQseI3LLLQJWatlLGXGc7UgJOl7ExKcE0s8YfvACcslaLXuocSo2bKSOREbuBRTeogr+mLvPWXcvTDcC/d2aRV5hVq61Lnv1KT8Z69FtJ57sK7EEWDZuEQFI1GlPAQkZOCvdhVNsz21V7gVdephZXpIkWOo8f8flA/1avTD5KJsr85z5q8jJwfJOUr2PXe4MwBYPFdP2VZbi4KLhO6h7QUcjQzvOoSDOAUvDbYPGlg+yEsIQP/Ux7FjzLwlaBcFj/4Al1rgy18APxeyJHBFIrUCbMlx2hhYFauNFqPSz3GCLIT/qJomuhSUdM1EY7Os2KRIjNZ32o=',
    ),
  ),
);

$metadata['https://es-login.ur.com/public/saml2/idp/metadata.php'] = array (
  'entityid' => 'https://es-login.ur.com/public/saml2/idp/metadata.php',
  'contacts' =>
    array (
      0 =>
        array (
          'contactType' => 'technical',
          'givenName' => 'Administrator',
          'emailAddress' =>
            array (
              0 => 'tech@ur.com',
            ),
        ),
    ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' =>
    array (
      0 =>
        array (
          'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
          'Location' => 'https://es-login.ur.com/public/saml2/idp/SSOService.php',
        ),
    ),
  'SingleLogoutService' =>
    array (
      0 =>
        array (
          'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
          'Location' => 'https://es-login.ur.com/public/saml2/idp/SingleLogoutService.php',
        ),
    ),
  'ArtifactResolutionService' =>
    array (
    ),
  'keys' =>
    array (
      0 =>
        array (
          'encryption' => false,
          'signing' => true,
          'type' => 'X509Certificate',
          'X509Certificate' => 'MIID/TCCAuWgAwIBAgIJAJAL2EVDNDFmMA0GCSqGSIb3DQEBCwUAMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTAeFw0xNjA0MDEyMjUyMTRaFw0yNjAzMzAyMjUyMTRaMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKcmXSJ5XooXB720GLJx0zgK8eejwdD7w82drQyNijTELXZ+tI75PPAIzUSnHGVqK62m+3/qJ32xrjbhw7CI0FXUwKRBG2iebKKIXeYzWyzxVB1rBANoeb3kMH/HH49TR5SRPbPDMEj1loOjrFspc1JEmajCfVBp41sx8TlK7h3psHX7HXf9fL6oBeW+mbOiGpB2zepTJdqyjkGE2Xi8eR276eKf9znC9xErnqtP9HybrFSQIpuwNNrVMq0ZmblzxuyBvPxtf//gtiAhK34UTiTnfvNMGFSupXGyAPpQx1b9p2g6QWu4q7w0td2vYaHuQHzWCbDYffOBVWugJuOPSQMCAwEAAaNQME4wHQYDVR0OBBYEFCYgg7LrCAOD+1roytA/Yq5d5jKtMB8GA1UdIwQYMBaAFCYgg7LrCAOD+1roytA/Yq5d5jKtMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAB1PX16UTyYhEJdQseI3LLLQJWatlLGXGc7UgJOl7ExKcE0s8YfvACcslaLXuocSo2bKSOREbuBRTeogr+mLvPWXcvTDcC/d2aRV5hVq61Lnv1KT8Z69FtJ57sK7EEWDZuEQFI1GlPAQkZOCvdhVNsz21V7gVdephZXpIkWOo8f8flA/1avTD5KJsr85z5q8jJwfJOUr2PXe4MwBYPFdP2VZbi4KLhO6h7QUcjQzvOoSDOAUvDbYPGlg+yEsIQP/Ux7FjzLwlaBcFj/4Al1rgy18APxeyJHBFIrUCbMlx2hhYFauNFqPSz3GCLIT/qJomuhSUdM1EY7Os2KRIjNZ32o=',
        ),
      1 =>
        array (
          'encryption' => true,
          'signing' => false,
          'type' => 'X509Certificate',
          'X509Certificate' => 'MIID/TCCAuWgAwIBAgIJAJAL2EVDNDFmMA0GCSqGSIb3DQEBCwUAMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTAeFw0xNjA0MDEyMjUyMTRaFw0yNjAzMzAyMjUyMTRaMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKcmXSJ5XooXB720GLJx0zgK8eejwdD7w82drQyNijTELXZ+tI75PPAIzUSnHGVqK62m+3/qJ32xrjbhw7CI0FXUwKRBG2iebKKIXeYzWyzxVB1rBANoeb3kMH/HH49TR5SRPbPDMEj1loOjrFspc1JEmajCfVBp41sx8TlK7h3psHX7HXf9fL6oBeW+mbOiGpB2zepTJdqyjkGE2Xi8eR276eKf9znC9xErnqtP9HybrFSQIpuwNNrVMq0ZmblzxuyBvPxtf//gtiAhK34UTiTnfvNMGFSupXGyAPpQx1b9p2g6QWu4q7w0td2vYaHuQHzWCbDYffOBVWugJuOPSQMCAwEAAaNQME4wHQYDVR0OBBYEFCYgg7LrCAOD+1roytA/Yq5d5jKtMB8GA1UdIwQYMBaAFCYgg7LrCAOD+1roytA/Yq5d5jKtMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAB1PX16UTyYhEJdQseI3LLLQJWatlLGXGc7UgJOl7ExKcE0s8YfvACcslaLXuocSo2bKSOREbuBRTeogr+mLvPWXcvTDcC/d2aRV5hVq61Lnv1KT8Z69FtJ57sK7EEWDZuEQFI1GlPAQkZOCvdhVNsz21V7gVdephZXpIkWOo8f8flA/1avTD5KJsr85z5q8jJwfJOUr2PXe4MwBYPFdP2VZbi4KLhO6h7QUcjQzvOoSDOAUvDbYPGlg+yEsIQP/Ux7FjzLwlaBcFj/4Al1rgy18APxeyJHBFIrUCbMlx2hhYFauNFqPSz3GCLIT/qJomuhSUdM1EY7Os2KRIjNZ32o=',
        ),
    ),
);

$metadata['https://fr-login.ur.com/public/saml2/idp/metadata.php'] = array (
  'entityid' => 'https://fr-login.ur.com/public/saml2/idp/metadata.php',
  'contacts' =>
    array (
      0 =>
        array (
          'contactType' => 'technical',
          'givenName' => 'Administrator',
          'emailAddress' =>
            array (
              0 => 'tech@ur.com',
            ),
        ),
    ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' =>
    array (
      0 =>
        array (
          'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
          'Location' => 'https://fr-login.ur.com/public/saml2/idp/SSOService.php',
        ),
    ),
  'SingleLogoutService' =>
    array (
      0 =>
        array (
          'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
          'Location' => 'https://fr-login.ur.com/public/saml2/idp/SingleLogoutService.php',
        ),
    ),
  'ArtifactResolutionService' =>
    array (
    ),
  'keys' =>
    array (
      0 =>
        array (
          'encryption' => false,
          'signing' => true,
          'type' => 'X509Certificate',
          'X509Certificate' => 'MIID/TCCAuWgAwIBAgIJAJAL2EVDNDFmMA0GCSqGSIb3DQEBCwUAMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTAeFw0xNjA0MDEyMjUyMTRaFw0yNjAzMzAyMjUyMTRaMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKcmXSJ5XooXB720GLJx0zgK8eejwdD7w82drQyNijTELXZ+tI75PPAIzUSnHGVqK62m+3/qJ32xrjbhw7CI0FXUwKRBG2iebKKIXeYzWyzxVB1rBANoeb3kMH/HH49TR5SRPbPDMEj1loOjrFspc1JEmajCfVBp41sx8TlK7h3psHX7HXf9fL6oBeW+mbOiGpB2zepTJdqyjkGE2Xi8eR276eKf9znC9xErnqtP9HybrFSQIpuwNNrVMq0ZmblzxuyBvPxtf//gtiAhK34UTiTnfvNMGFSupXGyAPpQx1b9p2g6QWu4q7w0td2vYaHuQHzWCbDYffOBVWugJuOPSQMCAwEAAaNQME4wHQYDVR0OBBYEFCYgg7LrCAOD+1roytA/Yq5d5jKtMB8GA1UdIwQYMBaAFCYgg7LrCAOD+1roytA/Yq5d5jKtMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAB1PX16UTyYhEJdQseI3LLLQJWatlLGXGc7UgJOl7ExKcE0s8YfvACcslaLXuocSo2bKSOREbuBRTeogr+mLvPWXcvTDcC/d2aRV5hVq61Lnv1KT8Z69FtJ57sK7EEWDZuEQFI1GlPAQkZOCvdhVNsz21V7gVdephZXpIkWOo8f8flA/1avTD5KJsr85z5q8jJwfJOUr2PXe4MwBYPFdP2VZbi4KLhO6h7QUcjQzvOoSDOAUvDbYPGlg+yEsIQP/Ux7FjzLwlaBcFj/4Al1rgy18APxeyJHBFIrUCbMlx2hhYFauNFqPSz3GCLIT/qJomuhSUdM1EY7Os2KRIjNZ32o=',
        ),
      1 =>
        array (
          'encryption' => true,
          'signing' => false,
          'type' => 'X509Certificate',
          'X509Certificate' => 'MIID/TCCAuWgAwIBAgIJAJAL2EVDNDFmMA0GCSqGSIb3DQEBCwUAMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTAeFw0xNjA0MDEyMjUyMTRaFw0yNjAzMzAyMjUyMTRaMIGUMQswCQYDVQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB1NoZWx0b24xHTAbBgNVBAoMFFVuaXRlZCBSZW50YWxzLCBJbmMuMQswCQYDVQQLDAJJVDEVMBMGA1UEAwwMbG9naW4udXIuY29tMRowGAYJKoZIhvcNAQkBFgt0ZWNoQHVyLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKcmXSJ5XooXB720GLJx0zgK8eejwdD7w82drQyNijTELXZ+tI75PPAIzUSnHGVqK62m+3/qJ32xrjbhw7CI0FXUwKRBG2iebKKIXeYzWyzxVB1rBANoeb3kMH/HH49TR5SRPbPDMEj1loOjrFspc1JEmajCfVBp41sx8TlK7h3psHX7HXf9fL6oBeW+mbOiGpB2zepTJdqyjkGE2Xi8eR276eKf9znC9xErnqtP9HybrFSQIpuwNNrVMq0ZmblzxuyBvPxtf//gtiAhK34UTiTnfvNMGFSupXGyAPpQx1b9p2g6QWu4q7w0td2vYaHuQHzWCbDYffOBVWugJuOPSQMCAwEAAaNQME4wHQYDVR0OBBYEFCYgg7LrCAOD+1roytA/Yq5d5jKtMB8GA1UdIwQYMBaAFCYgg7LrCAOD+1roytA/Yq5d5jKtMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAB1PX16UTyYhEJdQseI3LLLQJWatlLGXGc7UgJOl7ExKcE0s8YfvACcslaLXuocSo2bKSOREbuBRTeogr+mLvPWXcvTDcC/d2aRV5hVq61Lnv1KT8Z69FtJ57sK7EEWDZuEQFI1GlPAQkZOCvdhVNsz21V7gVdephZXpIkWOo8f8flA/1avTD5KJsr85z5q8jJwfJOUr2PXe4MwBYPFdP2VZbi4KLhO6h7QUcjQzvOoSDOAUvDbYPGlg+yEsIQP/Ux7FjzLwlaBcFj/4Al1rgy18APxeyJHBFIrUCbMlx2hhYFauNFqPSz3GCLIT/qJomuhSUdM1EY7Os2KRIjNZ32o=',
        ),
    ),
);
