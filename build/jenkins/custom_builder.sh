#!/usr/bin/env bash

### -------------------------------------------- ###
### Custom Builder
### -------------------------------------------- ###
### This script builds from a specific, passed
### in branch name that is selected from the
### build form in Jenkins.
###
### In the future, we may do specifically commit
### hashes to be as precise as possible.
### -------------------------------------------- ###


if [ "$source" == "" ]; then
  echo "ERROR: No source branch defined."
  exit 1;
fi

if [ "$build_name" == "" ]; then
  echo "ERROR: No build name defined."
  exit 1;
fi

cd /docksal/projects/ur

PROJECT="ur_custom_build"
PROJECT_BASE="/docksal/projects/ur/ur_custom_build"
GITREPO="git@bitbucket.org:unitedrentals/unitedrentals.git"
BRANCH_NAME="${source/origin\//}"

if [ ! -d "$PROJECT" ]; then
  git clone ${GITREPO} ${PROJECT}
fi

cd ${PROJECT}

git remote | grep acquia || git remote add acquia unitedrentals@svn-16694.prod.hosting.acquia.com:unitedrentals.git

# Saving this method for later.
# git config lfs.https://svn-16694.prod.hosting.acquia.com/unitedrentals.git/info/lfs.locksverify false
git clean -fd

echo "Received Source '${source}' and build name '${build_name}'."
BUILD="build/ur-one/${build_name}"
echo "Branch name will be ${BUILD} upon build completion."

git fetch origin # Remember, origin is at Bitbucket
git checkout develop
git pull origin develop

git checkout --track $source

# Verify that it checked out the branch we want. If not, then we try to check it out as if it is already
# available (usually the case when this fails)
git rev-parse --abbrev-ref HEAD | grep $BRANCH_NAME || git checkout $BRANCH_NAME

# Check one more time that we succeeded in checking out the branch. If not then we cannot proceed.
git rev-parse --abbrev-ref HEAD | grep $BRANCH_NAME || (echo "Failed to checkout $BRANCH_NAME" && exit 1;)

# For troubleshooting purposes, keep the log informed
echo "Current branch is:"
git rev-parse --abbrev-ref HEAD

# Make sure it accepts merge messages with any previously checked out branches. Message does not matter.
git pull --no-edit # Might consider using the "-f" flag to force if needed.

echo "Creating build branch '$BUILD'..."

# Checkout a copy of the source branch as the build destination branch
git checkout -b $BUILD # | grep "fatal: A branch name" || (echo "Cannot use the build name: $BUILD. It is already in use." && exit 1)

rm -rf vendor/ docroot/modules/contrib/ docroot/libraries/
rm -rf docroot/themes/custom/unitedrentals/node_modules/ docroot/themes/custom/unitedrentals/vue/node_modules/
echo "Checkout of basics has been completed. Starting build..."

# set up local builder overrides and global composer cache
cp .docksal/docksal-local-builder.yml .docksal/docksal-local.yml
cp .docksal/docksal-local-builder.env .docksal/docksal-local.env
fin docker volume list | grep composer_cache || fin docker volume create composer_cache

fin up && fin init-deps builder

# Removed as stock Docksal container now has proper node version
# echo "Switching node versions..."
# fin exec nvm install
# fin exec nvm alias default $(cat .nvmrc)
# fin exec npm -g i npm@$(cat .npmrc)

# if [ $? -ne 0 ]; then
#    echo "Failed to switch node versions."
#    exit 1
# fi

cd docroot/themes/custom/unitedrentals

echo "Installing node_modules..."
fin exec npm run setup

if [ $? -ne 0 ]; then
    echo "Failed to install node_modules."
    exit 1
fi

echo "Installing PL build dependency..."
fin exec sudo apt-get update -y
fin exec sudo apt-get install bsdtar -y

echo "Building patternlab..."
fin exec npm run pl-postinstall

if [ $? -ne 0 ]; then
    echo "Failed to build patternlab."
    exit 1
fi

echo "Building theme..."
fin exec npm run theme:build

if [ $? -ne 0 ]; then
    echo "Failed to build theme."
    exit 1
fi

echo "Building JavaScript..."
fin exec npm run vue:build

if [ $? -ne 0 ]; then
    echo "Failed to build javascript."
    exit 1
fi

echo "Build processes have been completed. Adding to git..."
echo "Changing to path: ${PROJECT_BASE}"
cd ${PROJECT_BASE}

# Once built, we no longer need the node_modules directories
rm -rf docroot/themes/custom/unitedrentals/node_modules/ docroot/themes/custom/unitedrentals/vue/node_modules/

gitSubmodules=$(fin exec sudo find docroot/modules/contrib -type d -name ".git" -print)
submodules=$(echo $gitSubmodules | sed -e 's/\/.git//g')

fin exec sudo find docroot/modules/contrib -type d -name ".git" -prune -exec rm -rf {} \;

### clear .git folder out of any composer-cloned modules or the `git add` will miss them.
fin exec find docroot/modules/contrib/ | grep "\.git/" | xargs rm -rf || :

# For some reason doing this again, locally, seems to finally remove the submodules
fin exec sudo find docroot/modules/contrib -type d -name ".git" -prune -exec rm -rf {} \;

fin stop

while read -r brokenDir; do
  if [ ! -z "$brokenDir" ]
  then
    # Trim the whitespace
    CLEANED_DIR="$(echo -e "${brokenDir}" | tr -d '[:space:]')"

    echo "Cleaning directory $CLEANED_DIR"
    git rm -r --cached $CLEANED_DIR

    echo "Adding directory $CLEANED_DIR"
    git add -f $CLEANED_DIR
  fi
done <<< "$submodules"

# Make sure git does not add the docksal and local settings files
git rm docroot/sites/default/settings.docksal.php
git rm docroot/sites/default/settings.local.php

# Just in case git doesn't have this added we will remove it normally as well
rm docroot/sites/default/settings.docksal.php
rm docroot/sites/default/settings.local.php

git add . --force
git add -f vendor/
git add -f docroot/core/
git add -f docroot/modules/contrib/

git status
rm .git/hooks/pre-commit

echo "Third-party files added to git. Starting commit process..."

if git diff-index --quiet HEAD --; then
    echo "Nothing to commit."
else
  git commit -m "Adding compiled assets."
  echo "Compiled assets have been committed."
fi

echo "Pushing build to Acquia..."
git push acquia $BUILD

echo "Checking out develop once more...."
git checkout develop

echo "Deleting finished branches on build server."
git branch -D $BRANCH_NAME

echo "Built from Source '${source}' and used build name '${build_name}'."
echo "Branch name is now: ${BUILD}."

echo "Done!"