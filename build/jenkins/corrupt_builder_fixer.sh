#!/usr/bin/env bash
set +e

### -------------------------------------------- ###
### Builder Fixer
### -------------------------------------------- ###
### This script fixes the project directory for
### a selected builder project in Jenkins.
### -------------------------------------------- ###

if [ "$builder" == "*** Select One ***" ]; then
  echo "ERROR: No build number defined."
  exit 1;
fi

PROJECT="ur_dev"
PROJECT_BASE="/docksal/projects/ur/ur_dev"

if [ "$builder" == "custom" ]; then
  PROJECT="ur_custom_build"
  PROJECT_BASE="/docksal/projects/ur/ur_custom_build"
fi

echo "Project set to: $builder - Operations will be run in the $PROJECT_BASE directory."
cd /docksal/projects/ur
pwd

echo "Removing builder directory..."
rm -rf ${PROJECT}

GITREPO="git@bitbucket.org:unitedrentals/unitedrentals.git"

if [ ! -d "$PROJECT" ]; then
  echo "Re-cloning project directory..."
  git clone ${GITREPO} ${PROJECT}
fi

cd ${PROJECT}
pwd

echo "Loading everything from git base..."
git fetch

echo "Adding Acquia repo as a remote..."
git remote | grep acquia || git remote add acquia unitedrentals@svn-16694.prod.hosting.acquia.com:unitedrentals.git

echo "Pulling down everything from Acquia..."
git fetch acquia

echo "Quick directory cleanup..."
git clean -fd

echo "Running git prune..."
git prune

echo "Done! You are ready to resume using the $builder builder".