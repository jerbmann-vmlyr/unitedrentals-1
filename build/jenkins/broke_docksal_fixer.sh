#!/usr/bin/env bash
set +e

### -------------------------------------------- ###
### Builder Fixer
### -------------------------------------------- ###
### This script fixes the project directory for
### a selected builder project in Jenkins.
### -------------------------------------------- ###

if [ "$builder" == "*** Select One ***" ]; then
  echo "ERROR: No build number defined."
  exit 1;
fi

PROJECT_BASE="/docksal/projects/ur/ur_dev"

if [ "$builder" == "merge" ]; then
  PROJECT_BASE="/docksal/projects/ur/ur_dev"
fi

if [ "$builder" == "custom" ]; then
  PROJECT_BASE="/docksal/projects/ur/ur_custom_build"
fi

if [ "$builder" == "config" ]; then
  PROJECT_BASE="/docksal/projects/ur/ur_config_build"
fi

if [ "$builder" == "legacy" ]; then
  PROJECT_BASE="/docksal/projects/ur/ur_legacy"
fi

echo "Project set to: $builder - Operations will be run in the $PROJECT_BASE directory."

cd ${PROJECT_BASE}
pwd

echo "Git cleanup..."
git clean -fd
git checkout develop
git reset --hard

echo "Removing the Docksal containers..."
echo y | fin remove

echo "Re-Creating the Docksal containers..."
fin start

echo "Now sleeping Docksal container for the next build."
fin stop

echo "Done! You are ready to resume using the $builder builder".