#!/usr/bin/env bash

cd /docksal/projects/ur

PROJECT="ur_release"
GITREPO="unitedrentals@svn-16694.prod.hosting.acquia.com:unitedrentals.git" # We only care about the Acquia repo
TAG="release/$release_name"
SOURCE="${source_branch/origin\//}"

if [ ! -d "$PROJECT" ]; then
  git clone ${GITREPO} ${PROJECT}
fi

cd ${PROJECT}

echo "Release creation requested. Release will be created from '$SOURCE'. The release name will be: '$TAG'."

git clean -fd
git fetch

echo "Checking out '$SOURCE'..."
git checkout --track $source_branch

echo "Updating branch '$source_branch'..."
git pull origin $SOURCE

echo "Creating tag: '$TAG'..."
git tag -a $TAG -m "Release $release_name created from $SOURCE"

echo "Tag created. Pushing tag '$TAG' to origin..."
git push origin $TAG

echo "Release $TAG created from $SOURCE"
echo "Ready to deploy. Deploys are done from the Acquia dashboard."
