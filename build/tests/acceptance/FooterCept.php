<?php

$I = new AcceptanceTester($scenario);
$I->wantTo('Check for presence of the footer');
$I->amOnPage('/');
$I->canSee('United Rentals, Inc.');
$I->canSee('Contact Us');
$I->canSee('Careers');
$I->canSee('Our Company');
$I->canSee('Investor Relations');
$I->canSee('News & Events');
