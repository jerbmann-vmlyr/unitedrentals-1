<?php

/**
 * @file
 */

$i = new AcceptanceTester($scenario);
$i->wantTo('Test a path from home page through alternate pricing');

$i->amOnPage('/');
$i->canSee('Your Job Starts Here.');

// TODO: make this work
// $url = $i->_getUrl();
// codecept_debug($url);

if (in_array($scenario->current('env'), array('ie10', 'ie11'))) {
  // Force reload the page w/o inline basic auth to work around an IE bug
  // $url.
  $i->amOnUrl('https://dev0.unitedrentals.com/');
}

$i->click(['css' => '.location-autocomplete label']);
$i->fillField(['css' => 'input.autocomplete-input'], 'Atlan');
$i->waitForElement(['css' => 'div.location-autocomplete > ul > li > a']);
// $i->canSee('Atlanta, GA');
// $i->canSee('Atlanta, TX');.
$i->wait(1);
$i->click(['link' => 'Atlanta, GA']);
// TODO: debug IE race condition without this delay.
$i->wait(3);
$i->click(['css' => '.picker.before-select span']);
$i->wait(3);
// $i->canSee('Aerial Work Platforms');.
$i->canSee('Air Compressors & Air Tools');
$i->canSee('Compaction');
$i->canSee('Concrete & Masonry');
$i->canSee('Earthmoving Equipment');

$i->click('Aerial Work Platforms');
$i->canSee('Equipment Type');
$i->canSee('Boom Lifts');
$i->canSee('Personnel Lifts');
$i->canSee('Scaffolding');

$i->click('Boom Lifts');
$i->canSee('Platform Height');
$i->canSee("30' - 60'");
$i->canSee("60' - 95'");
$i->canSee("100' - 150'");

$i->click("60' - 95'");
$i->click('Find Results');

$i->waitForElement('div.term-listing-heading', 30);
$i->see('Boom Lifts', 'h1');
$i->see('Cat. Class Code: 310-6001');
$i->see("Articulating Boom Lift, 60', 4WD");
$i->waitForElement('form.add-item-to-cart', 30);
$i->canSee('Daily: $650', 'div[data-cat-class="310-6001"] ~ .add-item-to-cart__price__row li:first-of-type');

$i->fillField(['css' => 'input.autocomplete-input'], 'Scotts');
$i->waitForElement(['css' => 'div.location-autocomplete > ul > li > a']);
$i->click(['link' => 'Scottsdale, AZ']);
// $i->waitForElement('i.fa-spin');.
$i->wait(1);
$i->waitForElement('i.ur-location');
$i->canSee('Daily: $418', 'div[data-cat-class="310-6001"] ~ .add-item-to-cart__price__row li:first-of-type');

$i->fillField(['css' => 'input.autocomplete-input'], 'Denv');
$i->waitForElement(['css' => 'div.location-autocomplete > ul > li > a']);
// $i->canSee('Denver, CO');.
$i->click(['link' => 'Denver, CO']);
// $i->waitForElement('i.fa-spin');.
$i->wait(1);
$i->waitForElement('i.ur-location');

// $i->pauseExecution();
$i->canSee('We need additional information to give you an accurate price', 'div[data-cat-class="310-6001"] ~ .add-item-to-cart--call-for-quote span');
