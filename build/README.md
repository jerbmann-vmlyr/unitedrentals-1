Codeception Testing
===================

1. `composer.phar install`
2. `vendor/bin/codecept run acceptance SampleCept --env chrome --env firefox --env ie11 --env ie10 --env safari --env ios --env android`

Choose one or many environments with `--env` switches, configured in `test/acceptance.suite.yml`

Browserstack from local environment
===================================

1. Uncomment local capabilities in `tests/acceptance.suite.yml`:
    - `'browserstack.local':'true'`
    - `'browserstack.localIdentifier': 'your-hostname'`
2. run `browserstacklocal/osx/BrowserStackLocal -localIdentifier [your-hostname] [browserstack-api-key] &` (in background or another terminal)
3. `vendor/bin/codecept run acceptance SampleCept --env ie10`

