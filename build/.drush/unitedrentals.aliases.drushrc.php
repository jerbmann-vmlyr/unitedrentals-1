<?php

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site unitedrentals, environment dev
$aliases['dev'] = array(
  'root' => '/var/www/html/unitedrentals.dev/docroot',
  'ac-site' => 'unitedrentals',
  'ac-env' => 'dev',
  'ac-realm' => 'prod',
  //'uri' => 'unitedrentalsdev.prod.acquia-sites.com',
  'uri' => 'catalogdev.unitedrentals.com',
  'remote-host' => 'unitedrentalsdev.ssh.prod.acquia-sites.com',
  'remote-user' => 'unitedrentals.dev',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev.livedev'] = array(
  'parent' => '@unitedrentals.dev',
  'root' => '/mnt/gfs/unitedrentals.dev/livedev/docroot',
);
$aliases['dev.xdebug'] = array(
  'parent' => '@unitedrentals.dev',
  'ssh-options' => '-R 9001:127.0.0.1:9000',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site unitedrentals, environment dev2
$aliases['dev2'] = array(
  'root' => '/var/www/html/unitedrentals.dev2/docroot',
  'ac-site' => 'unitedrentals',
  'ac-env' => 'dev2',
  'ac-realm' => 'prod',
  'uri' => 'kelexdev.unitedrentals.com',
  'remote-host' => 'unitedrentalsdev2.ssh.prod.acquia-sites.com',
  'remote-user' => 'unitedrentals.dev2',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev2.livedev'] = array(
  'parent' => '@unitedrentals.dev2',
  'root' => '/mnt/gfs/unitedrentals.dev2/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site unitedrentals, environment prod
$aliases['prod'] = array(
  'root' => '/var/www/html/unitedrentals.prod/docroot',
  'ac-site' => 'unitedrentals',
  'ac-env' => 'prod',
  'ac-realm' => 'prod',
  //'uri' => 'unitedrentals.prod.acquia-sites.com',
  'uri' => 'www.unitedrentals.com',
  'remote-host' => 'unitedrentals.ssh.prod.acquia-sites.com',
  'remote-user' => 'unitedrentals.prod',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['prod.livedev'] = array(
  'parent' => '@unitedrentals.prod',
  'root' => '/mnt/gfs/unitedrentals.prod/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site unitedrentals, environment prod2
$aliases['prod2'] = array(
  'root' => '/var/www/html/unitedrentals.prod2/docroot',
  'ac-site' => 'unitedrentals',
  'ac-env' => 'prod2',
  'ac-realm' => 'prod',
  'uri' => 'kelex.unitedrentals.com',
  'remote-host' => 'unitedrentalsprod2.ssh.prod.acquia-sites.com',
  'remote-user' => 'unitedrentals.prod2',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['prod2.livedev'] = array(
  'parent' => '@unitedrentals.prod2',
  'root' => '/mnt/gfs/unitedrentals.prod2/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site unitedrentals, environment qa
$aliases['qa'] = array(
  'root' => '/var/www/html/unitedrentals.qa/docroot',
  'ac-site' => 'unitedrentals',
  'ac-env' => 'qa',
  'ac-realm' => 'prod',
  //'uri' => 'unitedrentalsqa.prod.acquia-sites.com',
  'uri' => 'catalogqa.unitedrentals.com',
  'remote-host' => 'unitedrentalsqa.ssh.prod.acquia-sites.com',
  'remote-user' => 'unitedrentals.qa',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['qa.livedev'] = array(
  'parent' => '@unitedrentals.qa',
  'root' => '/mnt/gfs/unitedrentals.qa/livedev/docroot',
);
$aliases['qa.xdebug'] = array(
  'parent' => '@unitedrentals.qa',
  'ssh-options' => '-R 9003:127.0.0.1:9000',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site unitedrentals, environment qa2
$aliases['qa2'] = array(
  'root' => '/var/www/html/unitedrentals.qa2/docroot',
  'ac-site' => 'unitedrentals',
  'ac-env' => 'qa2',
  'ac-realm' => 'prod',
  'uri' => 'kelexqa.unitedrentals.com',
  'remote-host' => 'unitedrentalsqa2.ssh.prod.acquia-sites.com',
  'remote-user' => 'unitedrentals.qa2',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['qa2.livedev'] = array(
  'parent' => '@unitedrentals.qa2',
  'root' => '/mnt/gfs/unitedrentals.qa2/livedev/docroot',
);
$aliases['qa2.xdebug'] = array(
  'parent' => '@unitedrentals.qa2',
  'ssh-options' => '-R 9004:127.0.0.1:9000',
);
if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site unitedrentals, environment ra
$aliases['ra'] = array(
  'root' => '/var/www/html/unitedrentals.ra/docroot',
  'ac-site' => 'unitedrentals',
  'ac-env' => 'ra',
  'ac-realm' => 'prod',
  'uri' => 'unitedrentalsra.prod.acquia-sites.com',
  'remote-host' => 'unitedrentalsra.ssh.prod.acquia-sites.com',
  'remote-user' => 'unitedrentals.ra',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['ra.livedev'] = array(
  'parent' => '@unitedrentals.ra',
  'root' => '/mnt/gfs/unitedrentals.ra/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site unitedrentals, environment test
$aliases['test'] = array(
  'root' => '/var/www/html/unitedrentals.test/docroot',
  'ac-site' => 'unitedrentals',
  'ac-env' => 'test',
  'ac-realm' => 'prod',
  //'uri' => 'unitedrentalstest.prod.acquia-sites.com',
  'uri' => 'catalogstage.unitedrentals.com',
  'remote-host' => 'unitedrentalstest.ssh.prod.acquia-sites.com',
  'remote-user' => 'unitedrentals.test',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['test.livedev'] = array(
  'parent' => '@unitedrentals.test',
  'root' => '/mnt/gfs/unitedrentals.test/livedev/docroot',
);
$aliases['test.xdebug'] = array(
  'parent' => '@unitedrentals.test',
  'ssh-options' => '-R 9005:127.0.0.1:9000',
);
if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site unitedrentals, environment test2
$aliases['test2'] = array(
  'root' => '/var/www/html/unitedrentals.test2/docroot',
  'ac-site' => 'unitedrentals',
  'ac-env' => 'test2',
  'ac-realm' => 'prod',
  'uri' => 'kelexstage.unitedrentals.com',
  'remote-host' => 'staging-17516.prod.hosting.acquia.com',
  'remote-user' => 'unitedrentals.test2',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['test2.livedev'] = array(
  'parent' => '@unitedrentals.test2',
  'root' => '/mnt/gfs/unitedrentals.test2/livedev/docroot',
);
$aliases['test2.xdebug'] = array(
  'parent' => '@unitedrentals.test2',
  'ssh-options' => '-R 9000:127.0.0.1:9000',
);
