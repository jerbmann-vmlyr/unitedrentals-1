#!/bin/bash

#
# Implements Cloud_hook post_code_deploy
#

site="$1"
target_env="$2"
db_name="$3"
source_env="$$"

domainPrepend='catalog'

if [[ $target_env = *"2"* ]]; then
  domainPrepend='kelex'
  target_env="${target_env/2/}"
fi

qaDomain="${domainPrepend}${target_env}.unitedrentals.com"

echo "QA Domain: ${qaDomain}"

SUBDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${SUBDIR}/run-jenkins-job.sh"

# Run the standard automated test for QA
if [ $target_env != "dev" ] && [ $target_env != "dev2" ]; then
  # run_jenkins mhontz 0920775a265d7cead222ce5fc81bb512 "United Rentals/United-Rentals-Smoke-Test" bleepblurp "url=${qaDomain}"

  slack "#6DC84B" "QA Automation Script IS DISABLED!! Remove mhontz from this script!" "The automated test scripts from QA have been launched."
fi
