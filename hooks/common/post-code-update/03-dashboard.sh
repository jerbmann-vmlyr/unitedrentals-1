#!/bin/bash
#
# This sample a Cloud Hook script to update environment release information whenever there is a new code deployment

site=$1         # The site name. This is the same as the Acquia Cloud username for the site.
targetenv=$2    # The environment to which code was just deployed.
sourcebranch=$3 # The code branch or tag being deployed.
deployedtag=$4  # The code branch or tag being deployed.
repourl=$5      # The URL of your code repository.
repotype=$6     # The version control system your site is using; "git" or "svn".

email=vml.reviewer@gmail.com
password=mabDeGybkQTYyW

secureToken=$(curl --silent -H "Content-Type:application/x-www-form-urlencoded" \
    -d "email=$email" \
    -d "password=$password" \
    -d "returnSecureToken=true" \
    https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyAUo6BxFydokw-9O64BDlGx8efXyp64zF4 \
    | grep idToken)

# Chop off "idToken" in JSON response.
secureToken=${secureToken:12}

# Chop off end comma in JSON response.
secureToken=${secureToken%?}

# Chop off first and last double quotes.
secureToken="${secureToken//\"}"

# Push Firebase request.
status_code=$(curl --write-out %{http_code} --silent --output /dev/null -X PATCH \
  -H "Authorization: Bearer${secureToken}" \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d "{ \"fields\": { \"targetEnv\": { \"stringValue\": \"${targetenv}\" }, \"deployedTag\": { \"stringValue\": \"${deployedtag}\" }, \"deployedTimestamp\": { \"stringValue\": \"$(date)\" } } }" \
  https://firestore.googleapis.com/v1beta1/projects/ur-dashboard/databases/%28default%29/documents/environments/{$targetenv})

# Check Firebase response for 200
if [[ "$status_code" -ne 200 ]] ; then
    echo "Firebase push failed."
else
    echo "Firebase push succeeded."
fi