# United Rentals

## Table of Contents
* [About United Rentals](#markdown-header-about-united-rentals)
* [Install Tools](#markdown-header-install-tools)
* [Setup Your Local Environment](#markdown-header-setup-your-local-environment)
    * [Clone Repository](#markdown-header-clone-repository)
    * [Create Environment](#markdown-header-create-environment)
    * [Initialize Drupal](#markdown-header-initialize-drupal)
    * [Environment Sync](#markdown-header-environment-sync)
* [Common Commands](#markdown-header-common-commands)
* [Drupal Configuration](#markdown-header-drupal-configuration)
* [Common Issues](#markdown-header-common-issues)
* [Jargon](#markdown-header-jargon)

### About United Rentals
Links to common areas of interest.
* [JIRA Board](https://unitedrentals.atlassian.net/secure/RapidBoard.jspa?projectKey=UR)
* [United Rentals Bitbucket](https://bitbucket.org/unitedrentals/unitedrentals/)
* [Drupal Jenkins](https://drupal-jenkins.vmlapps.com/job/UnitedRentals/)
* [Developer Cheat Sheet](https://unitedrentals.atlassian.net/wiki/spaces/KDEV/pages/251396110/Developer+Cheat+Sheet)

### Install Tools
Setting up the following tools to enable local development:
* [Docksal](https://docksal.io/)
* [git](https://git-scm.com/)

### Setup Your Local Environment
Assuming a fresh install these are the steps needed to get a local version of the website setup:

#### Clone Repository
Replace any instance of `{$USERNAME}` with your Bitbucket username.

1. [Fork United Rentals repository](https://bitbucket.org/unitedrentals/unitedrentals/fork) to your Bitbucket account.
2. Clone a local version of the repo: `git clone git@bitbucket.org:{$USERNAME}/unitedrentals.git`
3. Change into directory to the cloned repo: `cd unitedrentals`
4. Switch to the develop branch: `git checkout develop`
5. Add upstream remote: `git remote add upstream git@bitbucket.org:unitedrentals/unitedrentals.git`

#### Initialize containers
Once the VM is up and running, you make sure all the containers are initialized and ready:
```bash
fin up
```

#### Initialize Drupal
To initialize Drupal and all it's dependencies just run:
```bash
fin init
```
This will install composer dependencies, npm dependencies, bootstrap local Drupal settings files. Only run this once when first setting up the site.

#### Environment Sync
With everything installed. We can now sync the database and files from an Acquia environment by running:
```bash
fin drush kit:sync unitedrentals dev0 local --skip-composer -y
```
Typically you can accept the defaults and sync from dev0 to local.

#### Import Configuration
You can now import settings to support your local environment (and remove those specifically for dev0) by running:
```bash
fin drush kit:conf import unitedrentals local --yes
```

## Common Commands

### Start Docksal up
To make sure all containers are initialized and ready
```bash
fin up
```

### Environment Sync
To sync your local environment to a dev, stage, test environment run this command and follow the on screen prompts. All defaults sync your local to dev.
```bash
fin drush kit:sync
```

### Import Configuration
Import configuration, typically as local or other environments for testing:
```bash
fin drush kit:conf
```

### Clear Drupal cache
Run to clear the local Drupal cache if you've made a CMS update but it's not appearing locally.
```bash
fin drush cr
```

### User Login
To log in as a specific user run the following Drush command:
```bash
fin drush @unitedrentals.local uli --name="vml-test-12@gmail.com"
```

### User Password Reset
To reset a local user's password:
```bash
fin drush @unitedrentals.local upwd vml-test-12@gmail.com Test123
```

### Get bash access to a container
Shortcut for launching a bash session within a container
```bash
fin bash [container name, defaults to cli]
```

### Get MySQL command line access
Shortcut for launching MySQL CLI
```bash
fin db cli
```

### Restart VM
To restart your VM:
```bash
fin system stop
fin system start
```

### Reset Docksal containers
To reset your Docksal containers (Please note you can lose all work that is located inside your DB container):
```bash
fin system reset
```

### Reset VirtualBox
To reset Virtualbox completely (Nuclear option and should be reserved when all else fails):
```bash
fin system reset
```

## Drupal Configuration

### Normal Configuration Updates
When doing Drupal configuration we have a work flow of:
* Import current configuration `fin drush kit:conf import unitedrentals local -y`
* Make the Drupal edits
* Export out the current configuration `fin drush kit:conf export unitedrentals local -y`

Ninety-nine percent of all configuration from here on out will go to the "default"
configuration split. Which means all environments will take on this change.

Sometimes it's necessary to cherry pick your updated YML files only and commit those
specifically to prevent merge conflicts. This happens when your local database is out
of sync as well as not importing current configuration updates before exporting
out Drupal changes.

### Configuration Splits
Configuration splits are necessary to have certain Drupal environments
different than having a default setting.

The most common configuration split is a local configuration split. This
allows local development settings such as turning off advagg and caching off.

First step is to go to the configuration split page - greylisting the appropriate
configuration setting and then second step is exporting as the environment(s)
you want the configuration to have different values.

## Common Issues

## Update Docksal
Docksal publishes new container images and updates pretty regularly. Before troubleshooting anything else make sure you have the latest version:
```bash
fin update
```

### Out of memory errors.
Your VM comes configured with finite memory resources (2 gigabytes) to keep it from bogging down the host machine. Some memory issues can be remedied by increasing your RAM to 4 gigabytes:
```bash
fin vm ram 4096
```

### PHP Errors.
When there are PHP errors it usually stems from outdated PHP packages. You can run the following commands to make sure you have the latest code:
```bash
fin composer install
fin drush updb -y
fin drush cr
```

### CSS/JS is being cached.
You will need to enable the local Drupal configuration splits - select `import`, `unitedrentals` and `local`:
```bash
fin drush kit:conf import unitedrentals local -y
```

### Simplesaml Errors
If you receive a lot of Simplesaml errors, you probably haven't imported your local config, as the module is disabled locally.:
```bash
fin drush kit:conf import unitedrentals local -y
```

## Jargon
* Kelex: Old codename for the current Drupal 8 website, renamed to UR One
* UR One: Nickname for current Drupal 8 website
* Mercator: Codename for the AWS cloud API project
* Envoy: Codename for Argo's notifications system design
* DAL: Generic name for United Rental's API
* API Appliance: Custom module that acts as middleware between UR One and the DAL.
* MEDP: Marketplace Equipment Details Page
* MELP: Marketplace Equipment Listing Page
* Manage: the section on the site where users can manage equipment they've rented. Includes /manage/*
  This section was originally referred to as Workplace, however, a year later it is now referred to as Manage.  The VML Team
  can use these terms interchangeably, however, the code base refers to it as Workplace.
* Marketplace: the section on the site where users rent equipment. Includes /marketplace/equipment/* and /cart
* Quote: a type of transaction users create when going through any system's Checkout flow (Branch POS terminal, TotalControl, UROne).
  A user is not billed for a Quote, and has the option of converting a Quote to an actual rental Reservation.
  * Quotes have a RequisitionID, and TransactionID.
  * The transactionID is a QuoteID because it is a transaction of type 'X'
  * Other apis will refer to this TransactionID as the QuoteID
* Requisition: a type of rental transaction. A Requisition is a precursor to a potential Reservation/Transaction.
  * When a user creates a rental transaction, RentalMan determines if it is a Requisition (only), or a Confirmed Reservation
    (which has a RequisitionID & TransactionID). Requisition-Only Transactions are manually evaluated and potentially become Reservations.
  * Requisitions are always created through UROne or TotalControl, and can be created through the Branch POS Terminal (though generally are not).
  * Requisitions may automatically be confirmed as Reservations, or may require a manual review before this can happen
* Reservation: a type of rental transaction created through any Branch POS Terminal or online application.
  * All Reservations created through UROne will have a RequisitionID, TransactionID.
    * The transactionID is a ReservationID because it is a transaction of type 'R'
    * Other apis will refer to this TransactionID as the ReservationID
* Transaction: any Quote, Requisition, or Reservation
* WELD: Workplace Equipment Details Page
* WELP: Workplace Equipment Listing Page
* Workplace: see Manage

# Jenkins Build / Acquia Deploy Troubleshooting

* Problem: Generic Jenkins failure
  * Solution: Run Patrick's "fixit scripts" from the Jenkins UI: `UR One - Kill the node_modules folders`, `UR One - Corrupt Builder Fixer`, and `UR One - Broke Docksal Fixer`

* Problem: Jenkins fails to build, logs reveal an error similar to this
    ```
    Creating ur_dev_memcached_1 ... done

    Creating ur_dev_solr_1      ... done

    Creating ur_dev_db_1        ... done

    10:12:17 ERROR: for ur_dev_cli_1  UnixHTTPConnectionPool(host='localhost', port=None): Read timed out. (read timeout=60)
    10:12:17
    10:12:17 ERROR: for cli  UnixHTTPConnectionPool(host='localhost', port=None): Read timed out. (read timeout=60)
    10:12:17 ERROR: An HTTP request took too long to complete. Retry with --verbose to obtain debug information.
    10:12:17 If you encounter this issue regularly because of slow network conditions, consider setting COMPOSE_HTTP_TIMEOUT to a higher value (current value: 60).
    ```
  * Solution 1: SSH into our DocBrown server and log in (see a lead for details on how to do this). Navigate to our project `/docksal/projects/ur/[project name]`,
  and run `fin project reset`
  * Solution 2: if (1) fails, try running `fin cleanup` from the same dir
  * Solution 3: if (1) & (2) fail, try removing the [project] directory.  (It will get rebuilt automatically)

* Problem: Jenkins fails to build revealing the following error:
    ```
    Unpacking objects:  95% (21/22)
    Unpacking objects: 100% (22/22)
    Unpacking objects: 100% (22/22), done.
    15:08:12 From bitbucket.org:unitedrentals/unitedrentals
    15:08:12  * branch            develop    -> FETCH_HEAD
    15:08:12    4911588..a33fcbd  develop    -> origin/develop
    15:20:12 (B)0===[ Reading File ]
     7 lines
      GNU nano 2.5.3   File: /docksal/projects/ur/ur_dev/.git/MERGE_MSG             Merge branch 'develop' of bitbucket.org:unitedrentals/unitedrentals into acquia$# Please enter a commit message to explain why this merge is necessary,
    # especially if it merges an updated upstream into a topic branch.
    ## Lines starting with '#' will be ignored, and an empty message aborts
    # the commit.
    ^G Get Help  ^O Write Out ^W Where Is  ^K Cut Text  ^J Justify   ^C Cur Pos
    ^X Exit      ^R Read File ^\ Replace   ^U Uncut Text^T To Spell  ^_ Go To Line
    Build timed out (after 12 minutes). Marking the build as failed.
    15:20:12 Build was aborted
    15:20:12 Finished: FAILURE
    ```
  * Solution: SSH into our DocBrown server and log in (see a lead for details on how to do this). Navigate to our folder `/docksal/projects/ur`,
  and run `rm -rf [project name]`, where [project name] is `ur_dev` (for the merge builder) or `ur_custom_build` (for the custom builder)

