Migration Information
=========================================

Secondary Database Config and create migration files
----------------------------------------------------

Create a new content type in D8 Kelex project and a new feature. Write it in `modules/features` directory with `UR` bundle, if you already have the content type, you're ready to go...

1- Pull down the latest D7 production database and import it in a local database. (ur_unitedrentals) 

2- Add below code to your settings.local.php file (not the committed one). Change settings to match your local DB connection.

```bash
$databases['unitedrentals']['default'] = array(
  'database' => 'ur_unitedrentals',
  'username' => 'root',
  'password' => '',
  'host' => '127.0.0.1',
  'port' => 33067,
  'driver' => 'mysql',
  'prefix' => '',
);
```
3- In `/docroot/modules/custom/ur_migration/src/Plugin/migrate/source`, create a new `php class` and name it like "Kelex + [migration_name]".

4- You can copy and paste any other Kelex migration classes which is more similar to yours, and change the class name and `prepareRow` function.

5- Don't forget to match the MigrateSource id with the config file that you'll create in next steps.

6- Create a `.yml` file in `/docroot/modules/custom/ur_migration/config/install` and use your class name to name the file then copy a sample from another file in this directory.

7- Change the yml file content like this :
    
    - source:
        plugin: [class_name]
        node_type: [D7_content_machine_name] 
    - process:
        type:
          plugin: default_value
          default_value: [D8_content_machine_name]
    - # Field API mappings (for each field)
        [D8_field_machine_name]: [D7_field_machine_name]
          
8- Now, 
```bash 
$ cd /docroot
```
To check the migrate status, run : 
```bash
 $ drush ms
 OR 
 $ drush migrate-status
```

Your migration should be in the list, if not, run: 
```bash
$ drush config-import --partial --source=modules/custom/ur_migration/config/install -y
```
run the migrate status again, if your module doesn't appear in the list yet, uninstall the `UR Migration`  module and reinstall it again.

9- Run a query in your mysql console in D7 DB for an accurate total rows for migration.

```bash
SELECT COUNT(nid) FROM node WHERE type = `[node_type]`
```
> Whenever you got a fairly accurate result, run the migration.

```bash
$ drush mi {name of migration}
OR 
$ drush migrate-import {name of migration}
```

> If for whatever reason a migration gets stuck on importing, run the roll-back command :
```bash
$ drush  mr {name of migration} 
OR
$ drush migrate-rollback {name of migration}
```
> After roll-back, uninstall and reinstall the `UR Migration` module to reload the migration config files.

10- After successful import, check your contents in Drupal site and You're done.

##### * If your content type includes `Image` field, you need to create two migrations.
