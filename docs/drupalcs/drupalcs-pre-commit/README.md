United Rentals
==============

This the README file to config `drupalcs-precommit`.

Requirements :
* Drush
* PHP Code Sniffer and Drupal Code Sniffer

## Install Coder and PHPCS

* First, follow the instructions to install and config drupalcs 
 ```[PROJECT_ROOT]/docs/drupalcs/README.md```

## Install drupalcs pre-commit
1- Download or clone the repo below to the project location.
```https://github.com/klaasvw/drupalcs-precommit/archive/master.zip```

2- Make a symbolic link from the git repistory's hook directory to drupalcs.sh, 
named pre-commit:
```$ ln -s /path/to/drupalcs.sh .git/hooks/pre-commit```

## Usage

* Commit your changes.
* The drupalcs.sh file will be executed and checks all modified files.
* Your commit will abort when drupalcs found errors and show the output.
* If you want to skip the checks use the --no-verify option.

For more documentation, see <https://github.com/klaasvw/drupalcs-precommit>
