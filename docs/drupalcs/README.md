United Rentals
==============

This the README file to config `PHPCode Sniffer` for drupal Coding standards.

## Install Coder and PHPCS with Composer

* First, make sure Composer installed correctly :
 ``` $ which composer ```
* Install Coder (8.x-2.x) in your global Composer directory in your home directory :

    ``` $ composer global require drupal/coder ```
    --> On many systems this will install coder in ~/.composer/vendor/drupal/coder
    * For checking the installed location, run : ``` $ composer global show -P ```
* To make the phpcs and phpcbf commands available globally, add those to your $PATH 
variable in ~/.profile, ~/.bash_profile, ~/.bashrc : 
    * ``` $ open -a phpstorm ~/.bashrc ```, add  
        ```export PATH="$PATH:$HOME/.composer/vendor/bin"```
    * OR just run :  ``` $ set PATH $PATH $HOME/.composer/vendor/bin ```
* Register the Drupal and DrupalPractice Standard with PHPCS:

    ``` $ phpcs --config-set installed_paths ~/.composer/vendor/drupal/coder/coder_sniffer ```

##### See <https://www.drupal.org/node/1419988> for more documentation... 

## Integrate Drupal with PHPStorm 

1. Open PHPStorm Preferences -> Languages & Frameworks -> PHP -> Drupal 
2. Check the "Enable Drupal Integration" 
3. Drupal installation path would be : ```[PROJECT_NAME]/docroot```
4. Choose Drupal Version => 8
5. OK

## Coder and PHP Code Sniffer Integration

PHPStorm and Drupal Code Sniffer Integration  
1. Download the Coder module from the link below :

    <https://www.drupal.org/project/coder>
    
    * You do NOT need to install or use the Drupal Coder module - it contains the Drupal Coding Standards inside.
2. Unpack the downloaded archive and find the coder_sniffer/Drupal subdirectory inside.
3. Move the Drupal directory contents to  ```/Users/[USR_NAME]/.composer/vendor/squizlabs/php_codesniffer/CodeSniffer/Standards```
4. Configure PHP Code Sniffer by providing the path in Settings | PHP | Code Sniffer. 
    
    1. PHPStorm Preferences -> Languages & Frameworks -> PHP -> Code Sniffer -> Local browse button ( [...] )
    2. Code Sniffer Window -> PHP Code Sniffer (phpcs) path :
    ``` /Users/[USR_NAME]/.composer/vendor/squizlabs/php_codesniffer/scripts/phpcs ```
    
    3. Click the Validate button to check if the Code Sniffer tool can be reached. 
    4. Turn on PHP Code Sniffer inspections in PHPStorm Preferences -> Inspections -> PHP -> ```PHP Code Sniffer 
    validation```. 
    5. Select Drupal coding standard in the list (use the refresh button if needed). Click OK.

## Create Alias for Run the CodeSniffer in Command line
1. Run ``` $ open -a phpstorm ~/.bashrc``` in Terminal .
2. Add these lines at the end of the file :
``` 
alias drupalcs="phpcs --standard=Drupal --extensions='php,module,inc,install,test,profile,theme,css,info,txt,md'"
alias drupalcsp="phpcs --standard=DrupalPractice --extensions='php,module,inc,install,test,profile,theme,css,info,txt,md'"
alias drupalcbf="phpcbf --standard=Drupal --extensions='php,module,inc,install,test,profile,theme,css,info,txt,md'"
```

3. Run ``` $ source ~/.bashrc ``` in Terminal.
4. Now you can run ``` $ drupalcs [FILE_PATH]/[FILE_NAME.php]``` for any files or directories.  
   