<?php
/**
 * Acquia Site Scripts
 * 
 * Acquia puts proprietary files outside our repo in `/mnt/www/site-scripts`.
 * Within that, `prepend.php` contains the first line of code served on every
 * request. We stub it out here to ease remote debugging. Without it, we never
 * get as far as our own `index.php`.
 */
