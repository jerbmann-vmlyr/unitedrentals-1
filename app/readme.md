# Intro to `/app`

This directory contains Drupal setup options and the source code for theme assets & the frontend Vue SPA. The built source is output to the `/docroot` directory.

All of the commands mentioned here should be run inside this directory (`/app`)

# Getting up and running

Install dependencies...

```
$   npm install
```

> If dependency installation somehow fails, try `$ npm ci`.


Import config from local...

```
$   fin conf
```

Generate SSL certs used by the Vue app's dev server...

```
$ npm run generate-certs
```

You'll be prompted to fill in the SSL configuration fields. Just hit enter until it asks you for your password. Giving it your user password will allow it to automatically store the certificate in Keychain.

# Developing the Vue SPA

The building and testing of the frontend Vue app inside `./src` is facilitated by the Vue CLI service (which uses webpack underneath). The entire build configuration is inside `vue.config.js`.

Command | Description
---|---
`npm run serve` | Spins up a dev server that's accessible at `unitedrentals.docksal`; includes HMR and active source linting
`npm run build` | Builds the app and outputs the files to `/docroot`
`npm run lint` | Lints the app's source files and prints issues to terminal
`npm run fix-lint-errors` | Fixes any (mostly any) lint errors in the source files; make sure you have a clean git tree before running this!

# Developing themes

The building and testing of theme source files inside `./theme/custom` is facilitated by Gulp.

> Gulp does **not** need to be installed globally. Prefix your `gulp` commands with `npx` to use the local installation inside `node_modules`.

Command | Description
---|---
`npx gulp` | Runs all theme build tasks
`npx gulp test` | Runs all theme testing tasks (a pre-build without piping to the dest)
`npx gulp lint` | Runs all theme linting tasks (sass-lint and es-lint)
`npx gulp watch` | Runs theme build and linting tasks and reruns when files change


# Developing crossover code

For items that **must** live on the current and new site we have to package code from the new site to be implemented in the current site.

[Package and implementation info is documented in confluence.](https://unitedrentals.atlassian.net/wiki/spaces/KDEV/pages/626131191/New+Paragraph+Migration)

### Staying up-to-date with develop

To stay in sync with the current site daily merges will be done from the **development** branch into the **theme-refactor** branch.

Commands to run after the merge and after pulling down the changes.

_*run commands from the **root** directory_

1. pull down the latest code from **upstream theme-refactor**
2. run `$ fin sync --skip-db-sync --skip-files`

    options will be presented to select:
    * source site: **unitedrentals**
    * source environment: **dev0**
    * confirm overwrite of database: **yes**
3. run `$ fin conf`

    options will be presented to select:
    * operation to preform: **import**
    * site to import as: **unitedrentals**
    * import as: **local**
    * confirm import: **yes**
4. run `$ fin drush updb`
5. run `$ fin drush cr`

If the site shows up in maintenance mode, run `$ fin drush sset system.maintenance_mode 0`

### If VM is disconnected, steps to get back up and running

```
    fin update
    fin up
    fin sync
```

If, during sync, you receive the error message `Unknown column 'tags' in 'field list'`, try these steps:

```
    fin db cli
    use default;
    drop table cache_bootstrap;
    drop table cache_page;
    drop table cache_menu;
    exit;
    fin drush cr
```

Then continue with...

```
    fin conf   # (if styles / icons don't come in correctly)
    
    cd app
    gulp
```

If you need to login without SAML (NOTE: in this command, `United123` is the password. It's not secure, so use whatever non-secure password you prefer)...

```
    fin drush upwd vml-test-12@gmail.com United123 
```

    And when finished...

```
    fin stop
    fin system stop
```

# Project Improvement Recommendations

- Integrate [Prettier](https://prettier.io) formatting
- Integrate [Husky](https://www.npmjs.com/package/husky) commit hooks
- Incrementally turn on more [Vue + ESLint](https://vuejs.github.io/eslint-plugin-vue/) rules
- Split the code ([use the `pages` object in the Vue config](https://cli.vuejs.org/config/#pages)) and implement dynamic routing using `import()` expressions
- Break up the Vue project structure by business-domain and implement a fractal file pattern
