/* eslint-disable no-console,@typescript-eslint/no-var-requires */
const fs = require('fs');
const path = require('path');
// const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

const isDev = process.env.NODE_ENV === 'development';

function fromProjectRoot(dir) {
  return path.join(__dirname, '..', dir);
}

module.exports = {
  lintOnSave: process.env.NODE_ENV !== 'production',
  filenameHashing: false,
  outputDir: path.resolve('../docroot/themes/custom/urone/vue'),
  // Note: We NEED this for all builds, as theme code outputs HTML with components like:
  //  - <ur-global-search>
  //  - <toast-manager>
  //  - <vueml-slide>
  runtimeCompiler: true,
  pages: {
    app: 'src/main.js',
  },
  css: {
    loaderOptions: {
      sass: {
        includePaths: [
          path.resolve('./themes/custom/urone'),
          fromProjectRoot('docroot/themes/custom/unitedrentals/components/_patterns/00-base'),
        ],
      },
    },
  },
  chainWebpack: config => {
    config.externals({
      ...config.get('externals'),
      leaflet: 'L',
    });
  },
  configureWebpack: {
    plugins: [
      // Moment ships with 160kb of languages by default.
      // We only need english (included by default), spanish, french.
      // new MomentLocalesPlugin({
      //   localesToKeep: ['es-us', 'fr-ca'],
      // }),
    ],
  },
  devServer: {
    https: true,
    host: 'localhost',
    port: 8443,
    logLevel: 'debug',
    disableHostCheck: true, // prevents broken websockets
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },
  publicPath: isDev ? 'https://localhost:8443/' : '/',
  transpileDependencies: [
    'vuex-persist',
  ],
};
