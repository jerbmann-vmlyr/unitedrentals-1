#/bin/bash
echo "Generating private key..."
openssl genrsa -des3 -passout pass:x -out server.pass.key 2048 || exit $?
openssl rsa -passin pass:x -in server.pass.key -out ./server.key || exit $?
rm server.pass.key || exit $?

echo "Generating a certificate signing request..."
openssl req -new -config ./scripts/ssl.conf -key ./server.key -out ./server.csr || exit $?

echo "Generating a certificate...\n"
openssl x509 -req -sha256 \
  -days 365 \
  -in ./server.csr \
  -signkey ./server.key \
  -out ./server.crt \
  || exit $?

echo "Adding the certificate to Keychain..."
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ./server.crt \
    || exit $?

echo "Done!"
