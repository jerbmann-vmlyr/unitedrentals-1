/*
 * V 2.1
 *
 * This file is set up to run standard build and testing tasks over
 * ALL themes in the theme/custom directory.
 *
 * 😃 No changes are needed on startup. 😃
 *
 * !!! Files are NOT minified !!!
 * Minification should be handled through Advanced Aggregation Drupal module
 *
 * === Piping ===
 * Provided in this file are the default path and task configurations.
 * Files follows the same path as where it started from.
 * scss to css - js to js - images to images - icons to icons - fonts to fonts
 *
 * `app/themes/custom/project/js`
 *  is piped to
 * `docroot/themes/custom/project/js`
 *
 * === Example files ===
 * An `example.` prefix can be added to scss and js files.
 * The prefixed files will not be linted or piped.
 * ex: `styles/libraries/node/example.full.scss`
 *
 * === Preset tasks ===
 * gulp = runs all build tasks
 * gulp test = runs all testing tasks (a pre-build without piping to the dest)
 * gulp lint = runs all linting tasks (sass-lint and es-lint)
 * gulp watch = starts the watchers (compiles builds and tests)
 *
 * === Gulp file structure ===
 * 1. Configs
 * 2. Build Tasks
 * 3. Testing Tasks
 * 4. Watchers
 * 5. Exports
 */

const buble = require('gulp-buble');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const changed = require('gulp-changed');
const imagemin = require('gulp-imagemin');
const iconfont = require('gulp-iconfont');
const iconfontCss = require('gulp-iconfont-css');
const sassLint = require('gulp-sass-lint');
const eslint = require('gulp-eslint');
const fs = require('fs');
const merge  = require('merge2');
const autoprefixer = require('gulp-autoprefixer');
const packageJSON = require('./package.json');
const { src, dest, watch, parallel, series } = require('gulp');

/***************************************
 * 1. Configs                          *
 **************************************/

// === Theme name === //
// Gets an array of all the theme names
const files = fs.readdirSync('themes/custom/');
const config = [];

// Removes hidden files and update the key value
for (const value of files) {
  if (value.startsWith('.')) {
    continue;
  }
  config.push({themeName: value});
}

/***************************************
 * 2. Build Tasks                      *
 **************************************/
function buildSass() {
  const tasks = config.map((entry) => {
    return src([
      'themes/custom/' + entry.themeName + '/styles/**/*.scss',
      '!themes/custom/' + entry.themeName + '/styles/**/example.*.scss'
    ])
    .pipe(sourcemaps.init())
    .pipe(sass({
      noCache: true,
      outputStyle: 'compact',
      lineNumbers: false,
      includePaths: [],
      sourceMap: true,
    }))
    .pipe(autoprefixer({
      overrideBrowserslist: packageJSON.browserslist,
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(dest('../docroot/themes/custom/' + entry.themeName + '/css'));
  });

  return merge(tasks);
}

function buildJavascript() {
  const tasks = config.map((entry) => {
    return src([
      'themes/custom/' + entry.themeName + '/scripts/**/*.js',
      '!themes/custom/' + entry.themeName + '/scripts/**/example.*.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(buble())
    .pipe(changed('../docroot/themes/custom/' + entry.themeName + '/js'))
    .pipe(sourcemaps.write('./maps'))
    .pipe(dest('../docroot/themes/custom/' + entry.themeName + '/js'));
  });

  return merge(tasks);
}

function buildImages() {
  const tasks = config.map((entry) => {
    return src('themes/custom/' + entry.themeName + '/images/**/*')
      .pipe(changed('../docroot/themes/custom/' + entry.themeName + '/img'))
      .pipe(imagemin({progressive: true}))
      .pipe(dest('../docroot/themes/custom/' + entry.themeName + '/images'));
  });

  return merge(tasks);
}

function buildFonts() {
  const tasks = config.map((entry) => {
    return src('themes/custom/' + entry.themeName + '/fonts/**/*')
      .pipe(changed('../docroot/themes/custom/' + entry.themeName + '/fonts'))
      .pipe(dest('../docroot/themes/custom/' + entry.themeName + '/fonts'));
  });

  return merge(tasks);
}

function buildIcons() {
  const runTimestamp = Math.round(Date.now() / 1000);

  const tasks = config.map((entry) => {
    return src([
      'themes/custom/' + entry.themeName + '/icons/**/*.svg',
      '!themes/custom/' + entry.themeName + '/icons/**/example.*.svg'
    ])
    .pipe(dest('../docroot/themes/custom/' + entry.themeName + '/icons'))
    .pipe(iconfontCss({
      fontName: 'themeIcons',
      path: 'themes/custom/' + entry.themeName + '/styles/vendor/_icons-template.scss',
      targetPath: '../../../../../../app/themes/custom/' + entry.themeName + '/styles/vendor/_icons.scss',
      fontPath: 'themes/custom/' + entry.themeName + '/fonts/icons',
    }))
    .pipe(iconfont({
      fontName: 'themeIcons', // required
      prependUnicode: false, // recommended option
      formats: ['ttf', 'eot', 'woff'], // default, 'woff2' and 'svg' are available
      timestamp: runTimestamp, // recommended to get consistent builds when watching files
      normalize: true, // scale them to the height of the highest icon
      fontHeight: 1001,
    }))
    .on('glyphs', (glyphs, options) => {
      // CSS templating, e.g.
      // uncomment to see full logging output
      // console.log(glyphs, options);
    })
    .pipe(dest('../docroot/themes/custom/' + entry.themeName + '/fonts/icons'));
  });

  return merge(tasks);
}

/***************************************
 * 3. Testing Tasks                    *
 **************************************/

/**
 * Test Sass Build for CI process.
 */
function testSassBuild() {
  const tasks = config.map((entry) => {
    return src([
      'themes/custom/' + entry.themeName + '/styles/**/*.scss',
      '!themes/custom/' + entry.themeName + '/styles/libraries/theme-crossover/**/*.scss',
      '!themes/custom/' + entry.themeName + '/styles/**/example.*.scss'
    ])
    .pipe(sassLint({ sasslintConfig: '.sass-lint.yml' }))
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError());
  });

  return merge(tasks);
}

/**
 * Test JS Build for CI process.
 */
function testJsBuild() {
  var options = {
    warnFileIgnored: true
  };

  const tasks = config.map((entry) => {
    return src([
      'themes/custom/' + entry.themeName + '/scripts/**/*.js',
      '!themes/custom/' + entry.themeName + '/scripts/**/example.*.js'
    ])
    .pipe(eslint(options))
    .pipe(eslint.format('pretty'))
    .pipe(eslint.failAfterError());
  });

  return merge(tasks);
}

/**
 * Test Vuejs Build for CI process.
 */
function testVueJsBuild() {
  var options = {
    warnFileIgnored: true
  };

  const tasks = config.map((entry) => {
    return src([
      'src/**/*'
    ])
      .pipe(eslint(options))
      .pipe(eslint.format())
      .pipe(eslint.failAfterError());
  });

  return merge(tasks);
}

/**
 * Test Sass Lint for local linting.
 */
function testSassLint() {
  const tasks = config.map((entry) => {
    return src([
      'themes/custom/' + entry.themeName + '/styles/**/*.scss',
      '!themes/custom/' + entry.themeName + '/styles/libraries/theme-crossover/**/*.scss',
      '!themes/custom/' + entry.themeName + '/styles/**/example.*.scss'
    ])
    .pipe(sassLint({ sasslintConfig: '.sass-lint.yml' }))
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError());
  });

  return merge(tasks);
}

/**
 * Test JS Lint for local linting.
 */
function testJsLint() {
  var options = {
    warnFileIgnored: true
  };

  const tasks = config.map((entry) => {
    return src([
      'themes/custom/' + entry.themeName + '/scripts/**/*.js',
      '!themes/custom/' + entry.themeName + '/scripts/**/example.*.js'
    ])
    .pipe(eslint(options))
    .pipe(eslint.format('pretty'))
    .pipe(eslint.failAfterError());
  });

  return merge(tasks);
}

/**
 * Test VueJS Lint for local linting.
 */
function testVueJsLint() {
  var options = {
    warnFileIgnored: true
  };

  const tasks = config.map((entry) => {
    return src([
      'src/**/*'
    ])
    .pipe(eslint(options))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
  });

  return merge(tasks);
}

/***************************************
 * 4. Watchers                         *
 **************************************/

function watchSass() {
  var options = {
    usePolling: true
  };

  config.map((entry) => {
    watch(
      ['themes/custom/' + entry.themeName + '/styles/**/*.scss', '!themes/custom/' + entry.themeName + '/styles/libraries/theme-crossover/*.scss', '!themes/custom/' + entry.themeName + '/styles/**/example.*.scss'],
      options,
      series(buildSass, testSassLint));
  });
}

function watchJavascript() {
  var options = {
    usePolling: true
  };

  config.map((entry) => {
    watch(['themes/custom/' + entry.themeName + '/scripts/**/*.js', '!themes/custom/' + entry.themeName + '/scripts/**/example.*.js'],
      options,
      series(buildJavascript, testJsLint));
  });
}

function watchImages() {
  var options = {
    usePolling: true
  };

  config.map((entry) => {
    watch('themes/custom/' + entry.themeName + '/images/**/*',
      options,
      buildImages);
  });
}

function watchFonts() {
  var options = {
    usePolling: true
  };

  config.map((entry) => {
    watch('themes/custom/' + entry.themeName + '/fonts/**/*',
      options,
      buildFonts);
  });
}

function watchIcons() {
  var options = {
    usePolling: true
  };

  config.map((entry) => {
    watch('themes/custom/' + entry.themeName + '/icons/**/*.svg',
      options,
      buildIcons);
  });
}

/***************************************
 * 5. Fix.                             *
 **************************************/

function fixJsLint() {
  var options = {
    fix: true
  };

  const tasks = config.map((entry) => {
    return src([
      'themes/custom/' + entry.themeName + '/scripts/**/*.js',
      '!themes/custom/' + entry.themeName + '/scripts/**/example.*.js',
      'src/**/*'
    ])
      .pipe(eslint(options))
      .pipe(dest('themes/custom/' + entry.themeName + '/scripts'));
  });

  return merge(tasks);
}

function fixVueJsLint() {
  var options = {
    fix: true
  };

  const tasks = config.map((entry) => {
    return src([
      'src/**/*'
    ])
      .pipe(eslint(options))
      .pipe(dest('src/'));
  });

  return merge(tasks);
}

/***************************************
 * 6. Exports                          *
 **************************************/

exports.default = series(buildFonts, buildIcons, buildImages, buildJavascript, buildSass);
exports.test = series(testJsBuild, testSassBuild);
exports.lint = series(testJsLint, testSassLint);
exports.watch = parallel(watchFonts, watchImages, watchIcons, watchJavascript, watchSass);
exports.fixjs = series(fixJsLint, fixVueJsLint);
// Local lint tasks.
exports.sasslint = testSassLint;
exports.jslint = testJsLint;
exports.vuejslint = testVueJsLint;
// CI build scripts,
exports.sassbuild = testSassBuild;
exports.jsbuild = testJsBuild;
exports.vuejsbuild = testVueJsLint;
