import Vue from 'vue';
import { startsWith } from 'lodash';
import AsyncComputed from 'vue-async-computed';

if (!window.Vue) window.Vue = Vue;

import './components/legacy';
import './shared-data';

/**
 * Plugins
 */
import VueCarousel from 'vue-carousel';
import VueMLCarousel from 'vueml-carousel';
import VueChart from 'vue-chartjs';
import * as VueGoogleMaps from 'vue2-google-maps';
import GmapCustomMarker from '@/components/map/gmap-custom-marker.vue';
import GmapCluster from 'vue2-google-maps/dist/components/cluster';
import Vuelidate from 'vuelidate';
import VuePaginate from 'vue-paginate';
import quark from 'quark';
import 'quark/dist/quark.css';
import VTooltip from 'v-tooltip';
import VueFindChild from '@/lib/plugins/vue-find-child';
import VueGetAllChildren from '@/lib/plugins/vue-get-all-children';
import VueValidateChildren from '@/lib/plugins/vue-validate-children';
import VueI18nMapLabels from '@/lib/plugins/vue-i18n-map-labels';
import VueTimeago from 'vue-timeago';
import VuePopup from '@/utils/popup';
import lineClamp from 'vue-line-clamp';

Vue.use(VuePopup);
Vue.use(VueCarousel);
Vue.use(VueMLCarousel);
Vue.use(VueChart);
Vue.use(quark);
Vue.use(AsyncComputed);

const makeVueGoogleMapsConfig = () => {
  // The /locations page is being de-vued, so avoid loading our own Gmap API
  // @todo this is temporary spot for this
  const isLocationsPage = startsWith(window.location.pathname, '/locations');
  return {
    load: isLocationsPage
      ? false
      : {
          client: 'gme-unitedrentalsinc',
          libraries: 'places',
          v: '3.38'
        }
  };
};

Vue.use(VueGoogleMaps, makeVueGoogleMapsConfig());
Vue.component('GmapCustomMarker', GmapCustomMarker);
Vue.component('GmapCluster', GmapCluster);
Vue.use(Vuelidate);
Vue.use(VuePaginate);
Vue.use(VTooltip);
Vue.use(VueFindChild);
Vue.use(VueGetAllChildren);
Vue.use(VueValidateChildren);
Vue.use(VueI18nMapLabels);
Vue.use(VueTimeago, {
  name: 'timeago',
  locale: 'en-US',
  locales: {
    'en-US': ['just now', '%ss ago', '%sm ago', '%sh ago', '%sd ago', '%sw ago', '%sm ago']
  }
});
Vue.use(lineClamp);

/**
 * Filters
 */
import filters from '@/lib/filters';

Object.entries(filters).forEach(([name, filter]) => {
  Vue.filter(name, filter);
});

/**
 * Directives
 */
import applyClassesOnHover from '@/lib/directives/apply-classes-on-hover';
import clickOutside from '@/lib/directives/click-outside';
import openover from '@/lib/directives/openover';
import sticky from '@/lib/directives/sticky';

Vue.directive('apply-classes-on-hover', applyClassesOnHover);
Vue.directive('click-outside', clickOutside);
Vue.directive('openover', openover);
Vue.directive('sticky', sticky);

// UR-4089. Vue Routes aren't triggered in IE 11 with Manual Hash Changes.
// In our case, this means the Workplace Dashboard and Equipment
// links (rendered by Drupal) _do not update Vue Router_.
// https://github.com/vuejs/vue-router/issues/1849
// To fix this, we manually listen for Hash Change Events (ie11 only) and shove the new hash into Vue Router.
// As of July 2018, a PR to fix Vue Router has not been accepted.
// https://github.com/vuejs/vue-router/pull/1988/commits/23bfa089a0d3933896baae77d59fcfd229cf364d
const isIE11 = window.navigator.userAgent.indexOf('rv:11.0') !== -1;
const isEdge = window.navigator.userAgent.indexOf('Edge') !== -1;
if (isIE11 || isEdge) {
  window.addEventListener('hashchange', () => {
    const route = window.location.hash.split('#')[1];
    window._app.$router.push(route);
  });
}

/**
 * Configuration
 */
Vue.config.productionTip = false;
Vue.config.devtools = new RegExp('dev|qa|stage|docksal').test(window.location.host);
