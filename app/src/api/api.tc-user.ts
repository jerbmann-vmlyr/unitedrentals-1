import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

export interface CreateTcUserRequest {
  email: string;
  firstName: string;
  lastName: string;
  phone: string; // must be 234-234-2342, no spaces/parentheses; must be dash-delimited
  password: string;
  altPhone?: string;
}

class TcUserApi {
  @TrackFlight(FlightToken.createTcUser)
  create(params: CreateTcUserRequest) {
    params.phone = params.phone.replace(/[()]/g, '');
    params.phone = params.phone.replace(/\s/g, '-'); // (234) 234-2344 --> 234-234-2344
    return axios.post('/api/v1/guest-user/create', params);
  }

  @TrackFlight(FlightToken.fetchTcUser)
  fetch() {
    return axios.get('/api/v1/tcuser-simple');
  }

  @TrackFlight(FlightToken.fetchOneTcUser)
  fetchOne(email: string) {
    return axios.get(`/api/v1/tcuser-simple?useAltAuth=true&id=${email}`).then(({ data }) => {
      if (data === 'No Account found') {
        throw new Error(data);
      }
      return data;
    });
  }

  @TrackFlight(FlightToken.saveTcUser)
  save(tcUserForm) {
    tcUserForm.phone = tcUserForm.phone.replace(/[()]/g, '').replace(' ', '-');
    return axios.put('/api/v1/tcuser-update', JSON.stringify(tcUserForm));
  }
}

export default new TcUserApi();
