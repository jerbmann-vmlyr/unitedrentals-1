import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';
import { Branch } from '@/lib/branches';
import { UrApiResponse } from '@/lib/apiv2';



/**
 * Branches API Class
 *
 *
 */
class BranchesApi {


  /**
   * Get All method
   *
   * Returns all the branches
   *
   * @return {AxiosPromise} promise from the axios request
   */
  @TrackFlight(FlightToken.getDefaultBranches)
  getAll() {
    return axios.get<UrApiResponse<Branch[]>>('/api/v2/branches');
  }


  /**
   * Get Nearby Branches (branch types AR|GR only).
   *
   * This endpoint is identical to `getNearby` except:
   *   - it's a different drupal endpoint
   *   - this endpoint excludes all stores EXCEPT ones that sell equipment (AR|GR branch types)
   *   - this endpoint should line up with branches returned in `/api/v1/rates` results
   *
   * @param {function} lat get latitude
   * @param {function} lng get longitude
   * @param {number} page result offset
   * @return {AxiosPromise} | response promise
   */
  @TrackFlight(FlightToken.getNearbyBranchData)
  getNearbyBranch({lat, lng}) {
    lat = typeof lat === 'function' ? lat() : lat;
    lng = typeof lng === 'function' ? lng() : lng;
    const options = {
      params: {
        latitude: `${lat}`,
        longitude: `${lng}`,
      },
      page: 0,
      page_size: 25,
    };
    return axios.get<UrApiResponse<Branch[]>>('/api/v2/branches', options);
  }

  /**
   * Gets all the branch information for a specific location
   *
   * @param {string} branchId | the branch id to get data for
   * @return {AxiosPromise} promise from the axios request
   */
  @TrackFlight(FlightToken.getBranchData)
  getBranchData(branchId) {
    return axios.get(`/api/v2/branch/${branchId}`);
  }
}

export default new BranchesApi();
