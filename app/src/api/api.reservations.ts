import axios from 'axios';
import { parseToUrl } from '../utils';
import { TrackFlight, FlightToken } from './flight-track';

class ReservationsApi {
  /*
   This fetch function only pulls past reservations... Not related to the cart.
   */
  @TrackFlight(FlightToken.fetchReservations)
  fetch(params = null) {
    let getString = '';
    if (params) {
      getString = getString + parseToUrl(params);
    }
    return axios.get(`/api/v1/reservations${getString}`);
  }
}

export default new ReservationsApi();
