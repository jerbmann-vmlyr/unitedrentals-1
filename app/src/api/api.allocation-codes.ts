import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

class AllocationCodesApi {
  @TrackFlight(FlightToken.fetchAllocationCodes)
  fetch({ accountId, codeType }) {
    return axios.get(`/api/v1/allocation-codes?accountId=${accountId}&codeType=${codeType}`);
  }
}

export default new AllocationCodesApi();
