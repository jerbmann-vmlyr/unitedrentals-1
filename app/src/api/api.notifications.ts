import MockNotifications from '@/data/mock.notifications.json';
import { TrackFlight, FlightToken } from './flight-track';

class NotificationsApi {
  @TrackFlight(FlightToken.fetchNotifications)
  fetch() {
    return Promise.resolve(MockNotifications);
  }
}

export default new NotificationsApi();
