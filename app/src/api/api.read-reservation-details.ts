import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';
import { RequisitionMap } from '@/lib/transactions';

const headers = {
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};

class ReadReservationDetailsApi {

  /*
   This fetch function only pulls the requisitions (Transactions without reservation ID)...
   Not related to the cart.
   */
  @TrackFlight(FlightToken.fetchReservationDetails)
  fetch(
    accountId: string,
    requestId: string, // can also be requisitionId
    reservationId?: string,
  ) {
    return axios.get<RequisitionMap>(`/api/v1/reservationDetails?id=${accountId}&requestId=${requestId}&reservationId=${reservationId || ''}`);
  }

  // Update an existing jobsite information
  @TrackFlight(FlightToken.saveRequisitionJobsite)
  saveRequisitionJobsite(params, email, contact, mobilePhone) {
    const paramsq = {
      email,
      contact,
      mobilePhone,
      jobId : params.jobId,
      address1 : params.address1,
      name : params.name,
      city : params.city,
      state : params.state,
      accountId : params.accountId,
      zip : params.zip,
    };

    return axios.put('/api/v1/jobsite', paramsq, headers);
  }

  // Update an existing transaction/requisition delivery/pickup information
  @TrackFlight(FlightToken.saveRequisitionTransactionDateTimeChange)
  saveRequisitionTransactionDateTimeChange(params) {
    if (params.startDate && (params.endDate || params.returnDate)) {
      /**
       * @TODO: Change branchId and delivery to use their own methods
       * not this one. This should not be a catch-all for requisitions.
       */
      const paramsq = {
        requestId: params.requestId,
        transId: params.transId,
        branchId: params.branchId,
        delivery: params.delivery,
        pickup: params.pickup,
        pickupFirm: params.pickupFirm,
        updateType: 'transaction',
        startDateTime : params.startDate,
        returnDateTime : params.returnDate,
        id : params.id,
        po: params.po,
        endDateTime : params.endDate,
      };

      return axios.put('/api/v1/transaction-form-update', paramsq, headers);
    }
    else {
      return;
    }

    /**
     * @TODO: Find out if this code is necessary any more, now
     * that we have the new thought process of breaking out
     * all updates to reservations into separate
     * methods for each type of update
     *
     * if (params['details']) {
     *  const paramsq = {
     *  id: params['id'],
     *  transId: params['transId'] || null,
     *  requestId: params['requestId'],
     *  catclass: params['catclass'],
     *  updateType: 'transaction',
     *  transSource: 'W',
     *  custOwn: 'N',
     * };
     *
     * return axios.put('/api/v1/transaction-form-update', paramsq, headers);
     *
     * }
     *
     */
  }

  @TrackFlight(FlightToken.saveRequisitionQuantityChange)
  saveRequisitionQuantityChange(params) {
    return axios.put('/api/v1/transaction-form-update', params, headers);
  }


  // Updates an existing jobsite information for an existing quote
  @TrackFlight(FlightToken.updateJobsiteInfoOrTransactionJobId)
  updateJobsiteInfoOrTransactionJobId(params) {
    const paramsq = {
      jobEmail : params.email,
      jobContact : params.contact,
      jobId : params.jobId,
      jobPhone: params.mobilePhone,
      accountId : params.accountId,
      name: params.name,
      address1: params.address1,
      city: params.city,
      state: params.state,
      zip: params.zip,
      mobilePhone : params.mobilePhone,
      requisitionId: params.requisitionId,
      requestId: params.requisitionId,
      quoteId: params.quoteId,
      transId: params.reservationId,
      jobsiteQuoteEditFlag: params.jobsiteQuoteEditFlag,
      status: params.status,
    };
    return axios.put('/api/v1/transaction-job-update', paramsq, headers);
  }

}

export default new ReadReservationDetailsApi();
