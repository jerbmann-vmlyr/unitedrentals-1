import axios from 'axios';
import { parseToUrl } from '../utils';
import { TrackFlight, FlightToken } from './flight-track';

class RequisitionsApi {
  /*
   This fetch function only pulls current requisitions... Not related to the cart.
   */
  @TrackFlight(FlightToken.fetchRequisitions)
  fetch(params = null) {
    let getString = '';
    if (params) {
      getString = getString + parseToUrl(params);
    }
    return axios.get(`/api/v1/reservations${getString}`);
  }

  @TrackFlight(FlightToken.updateRequisition)
  update({ accountId, requestId, branchId }) {
    return axios.put('/api/v1/requisition-update', { accountId, requestId, branchId });
  }

  @TrackFlight(FlightToken.updateRequisitionAndTransaction)
  updateBoth(input) {
    return axios.put('/api/v1/transaction-form-update', input);
  }
}

export default new RequisitionsApi();
