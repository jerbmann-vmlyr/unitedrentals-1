import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

const headers = {
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};

class JobsApi {
  // Endpoint to add a jobsite to a registered user's own account
  @TrackFlight(FlightToken.addJobsite)
  addJobsite(job) {
    return axios.post('/api/v1/jobsite-post', job, headers);
  }

  @TrackFlight(FlightToken.fetchJobsites)
  fetch(accountId) {
    return axios.get('/api/v1/jobsites', { params: { accountId } });
  }

  // Update an existing jobsite
  @TrackFlight(FlightToken.saveJobsite)
  saveJobsite(jobsite, id) {
    const params = { ...jobsite, ...{ id } };

    delete params.country;
    delete params.code;

    params.jobId = params.id;

    return axios.put('/api/v1/jobsite', params, headers);
  }
}

export default new JobsApi();
