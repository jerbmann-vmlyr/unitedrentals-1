/**
 * Get Array of Popular Equipment Types
 */

import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

interface PopularEquipmentTypesResponse {
  data: Array<{
    name: string;
    image: string;
    weight: number;
    description: string;
    url: string;
  }>
}

class PopularEquipmentTypesApi {

  @TrackFlight(FlightToken.fetchPopularEquipmentTypes)
  fetchPopularEquipmentTypes() {
    return axios.get('/api/v1/catalog/popular-types')
      .then(response => PopularEquipmentTypesApi.popularEquipmentTypesDataIsValid(response)
        ? response.data
        : Promise.reject(response),
      );
  }

  static popularEquipmentTypesDataIsValid(response): response is PopularEquipmentTypesResponse {
    return response !== null
      && Array.isArray(response.data);
  }
}

export default new PopularEquipmentTypesApi();
