import axios from 'axios';
import { parseToUrl } from '../utils';
import { TrackFlight, FlightToken } from './flight-track';

class QuotesApi {
  /*
   This fetch function only pulls past quotes... Not related to the cart.
   */
  @TrackFlight(FlightToken.fetchQuotes)
  fetch(params = null) {
    let getString = '';
    if (params) {
      getString = getString + parseToUrl(params);
    }
    return axios.get(`/api/v1/quotes${getString}`);
  }

  @TrackFlight(FlightToken.updateQuote)
  update(input) {
    return axios.put('/api/v1/transaction-update', input);
  }

  @TrackFlight(FlightToken.convertQuoteToCart)
  convertQuoteToCart(quoteItem) {
    return axios.put('/api/v1/transaction-update', quoteItem)
      .then(response => {
        return response.data;
      });
  }
}

export default new QuotesApi();
