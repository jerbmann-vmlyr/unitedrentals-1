import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';
const endpointUrl = '/api/v2/creditcards';

class CreditCardsApi {
  @TrackFlight(FlightToken.fetchCreditCards)
  fetch() {
    return axios.get(endpointUrl);
  }

  @TrackFlight(FlightToken.addCreditCard)
  add(data) {
    return axios.post(endpointUrl, data, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  }

  @TrackFlight(FlightToken.updateCreditCard)
  update(cardId, data) {
    return axios.patch(`${endpointUrl}/${cardId}`, data, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .catch(error => error);
  }

  @TrackFlight(FlightToken.removeCreditCard)
  remove(cardId) {
    return axios.delete(`${endpointUrl}/${cardId}`)
      .catch(error => error);
  }
}

export default new CreditCardsApi();
