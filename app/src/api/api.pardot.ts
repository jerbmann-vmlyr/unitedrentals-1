/**
 * These API calls are for analytics that track a user's progress
 * through cart / checkout.
 */

import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

class PardotApi {
  @TrackFlight(FlightToken.initiatingCheckout)
  initiatingCheckout() {
    return axios.get('/api/v1/checkout-start');
  }
}

export default new PardotApi();
