import axios from 'axios';
import { forOwn } from 'lodash';
import { TrackFlight, FlightToken } from './flight-track';

class CartApi {
  @TrackFlight(FlightToken.fetchCart)
  fetch() {
    return axios.get('/api/v1/get-cart')
      .then(response => {
        forOwn(response.data.items, transform);
        return response;
      });
  }

  @TrackFlight(FlightToken.addCartItem)
  addItem(cartItem) {
    return axios.post('/api/v1/add-item', cartItem.export())
      .then(response => {
        forOwn(response.data.items, transform);
        return response;
      });
  }

  @TrackFlight(FlightToken.removeCartItem)
  removeItem(id) {
    return axios.delete(`/api/v1/remove-item/${id}`).then(response => {
      forOwn(response.data.items, transform);
      return response;
    });
  }

  /**
   * Updates an item in the cart.
   * @param {CartItem} item | a CartItem instance
   * @return {Promise} response
   */
  @TrackFlight(FlightToken.updateCartItem)
  updateItem(item) {
    return axios.post(`/api/v1/update-item/${item.id}`, item.export()).then(response => {
      forOwn(response.data.items, transform);
      return response;
    });
  }

  @TrackFlight(FlightToken.updateCartItems)
  updateItems(items) {
    return axios.post(`/api/v1/update-items`, items);
  }
}

// Fixes things like turning Quantity into type Number, and delivery/pickup from "0"/"1" to boolean
function transform(item) {
  item.quantity = Number(item.quantity);
  item.delivery = !!Number(item.delivery);
  item.pickup = !!Number(item.pickup);
}

export default new CartApi();
