import axios from 'axios';
import { forOwn } from 'lodash';
import { TrackFlight, FlightToken } from './flight-track';

class FacetsApi {

  /**
   * Fetch categories, subcategories, facets, and filters (as one JSON tree)
   * @return {AxiosPromise} promise
   */
  @TrackFlight(FlightToken.fetchFacets)
  fetch() {
    return axios.get(`/api/v2/facets`).then(({ data }) => ({ data: transform(data) }));
  }

  /**
   * Fetch a category (or subcategory) description.
   * (This gets displayed at the top of an Object Listing page.)
   * @param {string} categoryTid | required TID of category
   * @param {string} subcategoryTid | optional TID of a subcategory
   * @return {AxiosPromise} promise
   */
  @TrackFlight(FlightToken.fetchFacetDescriptions)
  fetchDescription(categoryTid, subcategoryTid) {
    return axios.get(`/api/v2/facets/description/${categoryTid}${subcategoryTid ? `/${subcategoryTid}` : ''}`);
  }
}


/**
 * Recursively transforms `tid` keys to `id`, which allows for seamless
 * consumption of both Equipment Filters (from this api) and Contextual Filters.
 *
 * @param {array} data | collection of filter data from Drupal
 * @return {array} data | same data with renamed keys
 */
function transform(data) {
  const mutate = item => {
    item.id = item.tid;

    delete item.tid;
    delete item.parents;

    // Fix the name of Equipment Attributes.
    // E.g. "Boom Lifts > Platform Height" ---> "Boom Lifts &gt;<br>Platform Height"
    if (item.depth === 2) {
      item.name = item.name.replace('>', '&gt;<br>');
    }

    forOwn(item.children, mutate);
  };

  forOwn(data.result, mutate);

  return data;
}

export default new FacetsApi();
