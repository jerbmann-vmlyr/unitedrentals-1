import axios, { AxiosResponse } from 'axios';
import { TrackFlight, FlightToken } from './flight-track';
import { CatalogEquipment } from '@/lib/catalog';

class CatalogApi {

  // Fetches equipment by category/subcategory term id
  @TrackFlight(FlightToken.fetchEquipmentByTermId)
  fetchEquipmentByTermId({ termId, branchId, offset }): Promise<AxiosResponse<CatalogEquipment[]>> {
    const params = branchId ? `?branchId=${branchId}` : '';
    const url = `/api/v1/items/${termId}/*${params}`;
    return axios.get(url, { params: offset })
      .then(response => {
        transformEquipmentImages(response.data);
        transformEquipmentNodeLinks(response.data);
        return response;
      });
  }

  @TrackFlight(FlightToken.fetchCatClassByEquipmentId)
  async fetchCatClassByEquipmentId(equipmentId: string): Promise<string> {
    const url = `/api/v2/equipment/${equipmentId}`;
    const response = await axios.get(url);
    const { data } = response.data;
    return data !== null && typeof data.catClass === 'string'
      ? data.catClass
      : Promise.reject(response);
  }

  @TrackFlight(FlightToken.fetchEquipmentByCatClass)
  fetchEquipmentByCatClass(catClasses): Promise<AxiosResponse<CatalogEquipment[]>> {
    if (!catClasses.length) {
      const message = 'Cannot Fetch Equipment By Cat. Class (no cat. classes given)';
      return Promise.reject(message);
    }

    const url = `/api/v1/items/0/${catClasses.join(',')}`;
    return axios.get(url)
      .then(response => {
        transformEquipmentImages(response.data);
        transformEquipmentNodeLinks(response.data);
        return response;
      });
  }

  /**
   *
   * @param {string|undefined} catClass   Either catClass or catClasses is required, but not both
   * @param {string|undefined} catClasses Either catClass or catClasses is required, but not both
   * @param {string|undefined} branchId   Either placeId, branchId, or address is required
   * @param {string|undefined} placeId    Either placeId, branchId, or address is required
   * @param {string|undefined} address    Either placeId, branchId, or address is required
   * @param {string|undefined} deliveryDistance    Distance between branch and location. Not required.
   * @return {AxiosPromise} an A+ promise
   */
  @TrackFlight(FlightToken.fetchRates)
  fetchRates({
    catClass, catClasses, address, branchId, placeId, accountId, deliveryDistance,
  }) {
    const catClassData = catClass ? [catClass] : catClasses;
    const placeIdParam = placeId ? `placeId=${placeId}` : '';
    const accountIdParam = accountId ? `&accountId=${accountId}` : '';
    const branchIdParam = branchId ? `&branchId=${branchId}` : '';
    const addressParam = address ? `&address=${address}` : '';
    const deliveryDistanceParam = deliveryDistance ? `&deliveryDistance=${deliveryDistance}` : '';
    const catClassParam = catClassData.map(item => {
      if (item === undefined || (typeof item !== 'string' && item.catClass === undefined)) {
        console.warn(`Cat class "${item}" isn't set correctly`);
        console.trace();
        return;
      }
      else if (item.catClass !== undefined) {
        return `&catClass[]=${item.catClass}`;
      }
      else {
        return `&catClass[]=${item}`;
      }
    }).join('');
    const url = `/api/v1/rates?${placeIdParam}${branchIdParam}${addressParam}${catClassParam}${accountIdParam}${deliveryDistanceParam}`;
    return axios.get(url).then(transformRates);
  }
}

/**
 * Each equipment item's image field is a giant, gunked-up string.
 * This fn removes drupal theme-debugger html comments, and \n and converts what's
 * left into an array of image urls.
 * @param {array} equipment listing
 */
function transformEquipmentImages(equipment) {
  try {
    equipment.forEach(item => {
      item.imageUrls = item.images.split(', ');

      item.images = item.images_json === ''
        ? [{link: '', alt: ''}]
        : item.images_json.split('|~|')
          .map(imageJSON => JSON.parse(imageJSON))
          .map(({link, alt}) => ({link: link.replace(/<!--[\s\S]*?-->|\\n|\s/g, ''), alt}));
    });
  }
  catch (e) {
    // fallback in case I did something dumb
    console.warn('New Images implementation error: ', e.message);
    equipment.forEach(item => {
      item.images = item.images.replace(/<!--[\s\S]*?-->|\\n|\s/g, '').split(',');
    });
  }
}

/**
 * Each equipment item's node-link field is gunked-up with an HTML anchor tag.
 * This fn replaces that with the anchor's URL.
 * @param {array} equipment listing
 */
function transformEquipmentNodeLinks(equipment) {
  equipment.forEach(item => {
    item['node-link'] = item['node-link'].match(/href="(.*?)"/g)[0].replace(/href=|"/g, '');
  });
}

/**
 * Casts Day Rate, Week Rate, and Month Rate as integers.
 * Adds a new prop "noCost" if day/week/month rate are all zero.
 * Deletes props that aren't needed.
 *
 * @param {object} rate from api
 */
function transformRates(response) {
  const transform = rate => {
    if (typeof rate === 'string') return rate;

    const props = ['dayRate', 'weekRate', 'monthRate', 'deliveryCharge'];
    props.forEach(prop => {
      rate[prop] = Number(rate[prop]);
    });

    rate.noCost = rate.dayRate + rate.weekRate + rate.monthRate === 0;

    // Delete unused keys.  Eventually this should move to the backend for api v2
    const deletableProps = ['dspRateCatalog', 'expirationDate', 'minRateDuration', 'rateTiers', 'rateType', 'rateZone', 'rpp'];
    deletableProps.forEach(prop => {
      delete rate[prop];
    });

    return rate;
  };


  response.data = response.data.map(transform);

  return response;
}

export default new CatalogApi();
