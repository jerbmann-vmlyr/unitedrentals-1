import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';
import * as Qs from 'qs';

export interface FetchMessageRouteRequest {
  accountId: string,
  requisitionId: string,
  catClass: string,
  messageType: string,
}

class MessageRoutingApi {

  @TrackFlight(FlightToken.fetchMessageRoute)
  async fetchMessageRoute(request: FetchMessageRouteRequest) {
    const query = Qs.stringify(request, { addQueryPrefix: true });
    return axios.get(`/api/v2/messaging/router${query}`);
  }

}

export default new MessageRoutingApi();
