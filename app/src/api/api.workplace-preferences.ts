import {
  WorkplaceEquipmentColumnSettings,
  WorkplaceOrderColumnSettings,
} from '@/lib/workplace';

class WorkplacePreferencesApi {
  static equipmentListingColumnsAreValid(data: any): data is WorkplaceEquipmentColumnSettings[] {
    // Data spec requirements:
    // - Must be an array
    // - Must not be empty
    // - Each element must be a non-null object
    // - Each element must have a "columnKey" property of type string
    // - Each element's "columnKey" property must be unique
    if (Array.isArray(data) && data.length > 0 && data.every(col => col !== null && typeof col === 'object')) {
      const columnKeys = data.map(col => col && col.columnKey);
      return columnKeys.every(key => typeof key === 'string')
        && columnKeys.length === Array.from(new Set(columnKeys)).length;
    }
    else return false;
  }

  static ordersListingColumnsAreValid(data: any): data is WorkplaceOrderColumnSettings[] {
    // Data spec requirements:
    // - Must be an array
    // - Must not be empty
    // - Each element must be a non-null object
    // - Each element must have a "columnKey" property of type string
    // - Each element's "columnKey" property must be unique
    if (Array.isArray(data) && data.length > 0 && data.every(col => col !== null && typeof col === 'object')) {
      const columnKeys = data.map(col => col && col.columnKey);
      return columnKeys.every(key => typeof key === 'string')
        && columnKeys.length === Array.from(new Set(columnKeys)).length;
    } else return false;
  }
}

export default new WorkplacePreferencesApi();
