import axios from 'axios';
import moment from 'moment';
import { TrackFlight, FlightToken } from './flight-track';

class AccountsPoNumbersApi {
  @TrackFlight(FlightToken.fetchPoNumbers)
  fetchNumbers(accountId) {
    return axios.get(`/api/v2/purchase-orders/${accountId}`);
  }

  @TrackFlight(FlightToken.fetchPoFormats)
  fetchFormats(accountId) {
    return axios.get(`/api/v2/purchase-orders/${accountId}/formats`);
  }
}

/*
 DEV NOTES: UNCOMMENT & APPEND THIS FN TO fetchFormats's return statement
 to mock dates for local testing.

 e.g. axios.get('yada yada').then(mockDates);
 */
// @ts-ignore
const mockDates = response => {
  response.data.data = response.data.data.map(format => {
    return {
      ...format,
      effectiveDate: moment('20181101', 'YYYYMMDD').format('X'),
      expirationDate: moment('20281101', 'YYYYMMDD').format('X'),
    };
  });
  return response;
};

export default new AccountsPoNumbersApi();
