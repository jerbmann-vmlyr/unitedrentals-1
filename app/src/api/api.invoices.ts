
import axios from 'axios';
import * as Qs from 'qs';
import { TrackFlight, FlightToken } from './flight-track';

const API_URL = '/api/v1/get-invoices';

class InvoicesApi {
  @TrackFlight(FlightToken.fetchInvoices)
  async fetchInvoices(request: {
    accountId: string,
    fromDate: string,
    toDate: string,
  }) {
    const query = Qs.stringify(request, { addQueryPrefix: true });

    return axios.get(`${API_URL}${query}`);
  }
}

export default new InvoicesApi();
