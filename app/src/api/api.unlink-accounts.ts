import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

class UnlinkAccountsApi {

  @TrackFlight(FlightToken.deleteAccount)
  delete({ accountId }) {
    return axios.delete('/api/v1/accounts', {params: { accountId }});
  }
}

export default new UnlinkAccountsApi();
