import axios from 'axios';
import * as Qs from 'qs';
import { TrackFlight, FlightToken } from './flight-track';

export interface ExtendContractRequest {
  extensionDateTime: string;
  requester: string;
  transId: string;
}

export class ContractsApi {
  @TrackFlight(FlightToken.sendExtendContractRequest)
  sendExtendContractRequest(request: ExtendContractRequest) {
    const query = Qs.stringify(
      { ...request, type: 'extend' },
      { addQueryPrefix: true },
    );
    return axios.put(`/api/v1/contracts/extend${query}`);
  }
}

export default new ContractsApi();
