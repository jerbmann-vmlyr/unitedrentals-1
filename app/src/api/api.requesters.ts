import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

class RequestersApi {
  @TrackFlight(FlightToken.fetchRequesters)
  fetch(accountId) {
    return axios.get(`/api/v1/requesters?accountId=${accountId}`);
  }
}

export default new RequestersApi();
