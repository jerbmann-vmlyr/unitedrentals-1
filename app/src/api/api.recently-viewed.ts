import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

class RecentlyViewedApi {
  @TrackFlight(FlightToken.fetchRecentlyViewed)
  fetch() {
    return axios.get('/api/v1/recently-viewed', {
      headers: {
        'Cache-Control': 'private, must-revalidate, max-age=0, no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
      },
    });
  }
}

export default new RecentlyViewedApi();
