import { StatusUtils } from '@/lib/statuses';
import axios, { AxiosResponse } from 'axios';
import { FlightToken, TrackFlight } from '@/api/flight-track';
import {
  WorkplaceOrder,
  WorkplaceOrderStatusUtils,
  WorkplaceEquipment,
  WorkplaceEquipmentUtils,
  WORKPLACE_EQUIPMENT_STATUSES,
  workplaceEquipmentStatusRules,
} from '@/lib/workplace';

export interface FetchOrdersRequest {
  type: number;
  accountId: string;
  skipCache?: 0 | 1;
}

export class WorkplaceOrderApi {
  @TrackFlight(FlightToken.fetchOrders)
  async fetchOrders(request: FetchOrdersRequest): Promise<WorkplaceOrder[]> {
    const { accountId, ...params } = request;
    const { data } = await axios.get(`/api/v2/orders/${accountId}`, { params });

    return WorkplaceOrderApi.transformOrdersResponse(data);
  }
  static transformOrdersResponse({ data }: AxiosResponse<WorkplaceOrder[]>): WorkplaceOrder[] {
    return Object.values(data)
      .filter(item => typeof item === 'object' && typeof item.orderedBy === 'string')
      .map(item => ({
        ...item,
        equipment: Object.values(item.equipment)
          .filter(equipment => typeof equipment === 'object' && typeof equipment.catClass === 'string')
          .reduce((allEquipment, equipment): WorkplaceEquipment[] => [
            ...allEquipment,
            {
              ...equipment,
              accountId: equipment.accountId,
              daysOnRent: WorkplaceEquipmentUtils.calculateDaysOnRent(equipment),
              // Cast rates as type Number
              dayRate: +equipment.dayRate,
              minRate: +equipment.minRate,
              monthRate: +equipment.monthRate,
              type: +equipment.type,
              weekRate: +equipment.weekRate,
              year: +equipment.year,
              // The following properties must be populated downstream using data from non-equipment APIs
              catClassIsFavorite: false,
              eqpType: null,
            },
          ], [] as WorkplaceEquipment[])
          .map(eqp => ({
            ...eqp,
            equipmentStatuses: StatusUtils.getApplicableStatuses(
              WORKPLACE_EQUIPMENT_STATUSES,
              eqp,
              workplaceEquipmentStatusRules,
            ),
          })),
      }))
      .map(item => ({
        ...item,
        orderStatuses: Object.values(item.equipment).reduce(
          (statuses, eqp) => !statuses.length ? [ ...eqp.equipmentStatuses, ...statuses ] : [
              ...eqp.equipmentStatuses.filter(
                ({ id }) => statuses.filter(item => item.id !== id).length,
              ),
              ...statuses,
            ],
          [] as any[],
        ),
      }))
      .map(item => WorkplaceOrderStatusUtils.transform(item));
  }
}

export default new WorkplaceOrderApi();
