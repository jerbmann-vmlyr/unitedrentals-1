
import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

const API_URL = '/api/v1/user/preferences/save-for-later';

class SaveForLaterApi {
  @TrackFlight(FlightToken.fetchSaveForLater)
  async fetchItems() {
    try {
      const response = await axios.get(API_URL);
      Object.values(response.data.items).forEach(transform);
      return response;
    }
    catch (e) {
      return {data: {}};
    }
  }

  @TrackFlight(FlightToken.setSaveForLater)
  setItems(allSflItems) {
    return axios.post(API_URL, {items: allSflItems});
  }
}

export default new SaveForLaterApi();

/**
 * Private methods
 */

// Fixes things like turning Quantity into type Number, and delivery/pickup from "0"/"1" to boolean
function transform(item) {
  item.quantity = Number(item.quantity);
  item.delivery = !!Number(item.delivery);
  item.pickup = !!Number(item.pickup);
}
