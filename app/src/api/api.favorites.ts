import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

interface FavoritesResponse {
  favorites: string[];
}

class FavoritesApi {
  @TrackFlight(FlightToken.addFavorite)
  async add(catClass: string): Promise<string[]> {
    const { data } = await axios.post(`/api/v1/favorite?cat-class=${catClass}`);
    return FavoritesApi.favoritesDataIsValid(data)
      ? data.favorites
      : Promise.reject(data);
  }

  @TrackFlight(FlightToken.fetchFavorites)
  async fetch(): Promise<string[]> {
    const { data } = await axios.get('/api/v1/all-favorites');
    return FavoritesApi.favoritesDataIsValid(data)
      ? data.favorites
      : Promise.reject(data);
  }

  @TrackFlight(FlightToken.removeFavorite)
  async remove(catClass): Promise<string[]> {
    const { data } = await axios.delete(`/api/v1/unfavorite?cat-class=${catClass}`);
    return FavoritesApi.favoritesDataIsValid(data)
      ? data.favorites
      : Promise.reject(data);
  }

  static favoritesDataIsValid(data: any): data is FavoritesResponse {
    return data !== null
      && typeof data === 'object'
      && typeof data.favorites === 'object'
      && Array.isArray(data.favorites);
  }
}

export default new FavoritesApi();
