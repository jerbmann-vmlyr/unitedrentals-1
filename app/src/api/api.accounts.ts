import axios from 'axios';
import * as Qs from 'qs';
import { Account } from '@/lib/accounts';
import { TrackFlight, FlightToken } from './flight-track';
import { UrApiResponse } from '@/lib/apiv2';

export interface AccountSearchRequestParams {
  name: string;
  name_filter: 'contains' | 'startswith'; // Default is 'contains'
  id: string;
  city: string;
  state: string;
  zip: string;
  parentId: string;
  limit: number; // Default is 200
  favorites?: boolean;
}

class AccountsApi {
  @TrackFlight(FlightToken.fetchAccount)
  async fetch(): Promise<Account[]> {
    try {
      const url = '/api/v1/accounts';
      const { data } = await axios.get<{ [accountId: string]: Account }>(url);

      if (AccountsApi.fetchAccountsResponseIsValid(data)) {
        return AccountsApi.transformAccounts(Object.values(data));
      }
      else {
        return Promise.reject(`Response data from ${url} is invalid`);
      }
    }
    catch (err) {
      return err;
    }
  }

  @TrackFlight(FlightToken.searchAccounts)
  async search(searchParams: Partial<AccountSearchRequestParams> = {}): Promise<Account[]> {
    const response = await axios.get<UrApiResponse<Account[]>>(`/api/v2/accounts?${Qs.stringify(searchParams)}`);
    return AccountsApi.transformAccounts(response.data.data);
  }

  @TrackFlight(FlightToken.saveAccountFavorite)
  async saveFavorited(accountId: string, isFavorited: boolean) {
    return axios.patch<UrApiResponse<Account[]>>(`/api/v2/accounts/${accountId}`, Qs.stringify({ isFavorited }));
  }

  @TrackFlight(FlightToken.saveAccount)
  save(accountForm) {
    return axios.put('/api/v1/accounts', JSON.stringify(accountForm));
  }

  @TrackFlight(FlightToken.createFirstAccount)
  createFirstAccount(jobsiteForm) {
    return axios.post('/api/v1/first-account', JSON.stringify(jobsiteForm)); // possibly deprecating due to bug
  }

  @TrackFlight(FlightToken.createAccount)
  create(data) {
    return axios.post<number>('/api/v1/accounts', JSON.stringify(data));
  }

  private static fetchAccountsResponseIsValid(data: unknown): data is { [accountId: string]: Account } {
    return typeof data === 'object'
      && data !== null
      && !Array.isArray(data)
      && !('error' in data);
  }

  private static transformAccounts(data: Account[]): Account[] {
    return data
      .filter(account => !!account)
      .map(account => ({
        ...account,
        address1: (account.address1 || '').toLowerCase().trim(),
        address2: (account.address2 || '').toLowerCase().trim(),
        city: (account.city || '').toLowerCase().trim(),
        tcLevelLabel: account.tcLevel === 'C' ? 'Total Control' : 'UR Control',
      }));
  }
}


export default new AccountsApi();
