import axios from 'axios';

import { TrackFlight, FlightToken } from './flight-track';
import { BusinessType } from '@/lib/business-types';
import { UrApiResponse } from '@/lib/apiv2';

/**
 * Business Types API Class
 *
 *
 */
class BusinessTypesApi {
  /**
   * Get All method
   *
   * Returns all the business types
   *
   * @return {AxiosPromise} promise from the axios request
   */
  @TrackFlight(FlightToken.fetchBusinessTypes)
  getAll() {
    return axios.get<UrApiResponse<BusinessType[]>>('/api/v2/branches/business-type');
  }
}

export default new BusinessTypesApi();
