import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

class LinkAccountsApi {
  @TrackFlight(FlightToken.saveValidatePins)
  save({ accountId, pin }) {
    return axios.post('/api/v1/validatepins', JSON.stringify({ accountId, pin }));
  }
}

export default new LinkAccountsApi();
