import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';
import { UserPreferences } from '@/lib/user-preferences';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

class UserApi {
  @TrackFlight(FlightToken.getGuestLogout)
  logout() {
    return axios.get('/user/guest-logout');
  }

  @TrackFlight(FlightToken.fetchUserPreferences)
  async fetchUserPreferences<K extends keyof UserPreferences>(
    preferenceKey: K,
  ): Promise<UserPreferences[K]> {
    const { data } = await axios.get(`/api/v1/user/preferences/${preferenceKey}`);
    return data;
  }

  @TrackFlight(FlightToken.saveUserPreferences)
  async saveUserPreferences<K extends keyof UserPreferences>(
    preferenceKey: K,
    preferenceData: UserPreferences[K],
  ): Promise<boolean> {
    const { data } = await axios.post(`/api/v1/user/preferences/${preferenceKey}`, preferenceData);
    return data;
  }

  @TrackFlight(FlightToken.getLoggedInUsername)
  async getLoggedInUsername(): Promise<string> {
    const { data } = await axios.get(`/api/v1/user/name`);
    return data;
  }
}

export default new UserApi();
