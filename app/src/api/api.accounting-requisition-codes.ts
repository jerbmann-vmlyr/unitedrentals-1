import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

class AccountingRequisitionCodesApi {
  /*
  This fetch function pulls the Accounting Codes for reservation details page... Not related to the cart.
   */
  @TrackFlight(FlightToken.fetchRequisitionCodes)
  fetch(accountId, transId) {
    return axios.get(`/api/v1/requisition-codes?accountId=${accountId}&transId=${transId}`);
  }
}

export default new AccountingRequisitionCodesApi();
