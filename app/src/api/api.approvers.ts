import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

class ApproversApi {
  @TrackFlight(FlightToken.fetchApprovers)
  fetch(accountId) {
    return axios.get(`/api/v1/approvers?accountId=${accountId}`);
  }
}

export default new ApproversApi();
