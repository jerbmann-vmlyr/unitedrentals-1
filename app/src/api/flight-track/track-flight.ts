import 'reflect-metadata';
import { FlightToken, TrackerTokenMetadataKey } from './tokens';
import { Tracker } from './tracker';
import { Trackable } from './types';

export function TrackFlight(token: FlightToken): MethodDecorator {
  return (target, propertyKey, descriptor): void => {
    // Make sure the descriptor value is a function
    if (typeof descriptor.value !== 'function') {
      throw new Error(`Can't apply @TrackFlight to "${propertyKey.toString()} because it's not a function!`);
    }

    const originalMethod: unknown = descriptor.value;
    const tracker = new Tracker(token, originalMethod as Trackable);
    descriptor.value = tracker.wrappedTrackable() as any;
    Reflect.defineMetadata(token, tracker, target);
    const metatokens: FlightToken[] = Reflect.getMetadata(TrackerTokenMetadataKey, target) || [];
    Reflect.defineMetadata(TrackerTokenMetadataKey, [...metatokens, token], target);
  };
}
