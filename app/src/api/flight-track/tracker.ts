import { FlightToken } from './tokens';

import {
  TrackerListener,
  Trackable,
  TrackerEventType,
  TrackerCrashEvent,
  TrackerArrivalEvent,
  TrackerDepartureEvent,
} from './types';

export class Tracker {

  private listeners: TrackerListener[] = [];
  private executionCount = 0;

  constructor(
    readonly token: FlightToken,
    private readonly trackable: Trackable,
  ) {}

  on(eventType: TrackerEventType.Departure, cb: (event: TrackerDepartureEvent) => void): this;
  on(eventType: TrackerEventType.Arrival, cb: (event: TrackerArrivalEvent) => void): this;
  on(eventType: TrackerEventType.Crash, cb: (event: TrackerCrashEvent) => void): this;
  on(eventType: TrackerEventType, cb: (event: any) => void): this {
    this.listeners = [
      {
        type: eventType,
        callback: cb,
      },
      ...this.listeners,
    ];
    return this;
  }

  private emit(event: TrackerDepartureEvent | TrackerArrivalEvent | TrackerCrashEvent) {
    this.listeners
      .filter(listener => listener.type === event.type)
      .map(({ callback }) => callback(event));
  }

  private getExecutionId() {
    return this.executionCount++;
  }

  wrappedTrackable(): Trackable {
    return async (...args: any[]) => {
      const eventBase = {
        executionId: this.getExecutionId(),
        token: this.token,
        timestamp: Date.now(),
      };

      try {
        this.emit({ ...eventBase, type: TrackerEventType.Departure });
        const result = await this.trackable(...args);
        this.emit({ ...eventBase, type: TrackerEventType.Arrival });
        return result;
      }
      catch (error) {
        this.emit({ ...eventBase, type: TrackerEventType.Crash, error });
        return Promise.reject(error);
      }
    };
  }
}
