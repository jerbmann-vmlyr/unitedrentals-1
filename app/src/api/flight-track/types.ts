import { FlightToken } from './tokens';

export type Trackable<T = any> = (...args: any[]) => Promise<T>;

export enum TrackerEventType {
  Departure = 'departure',
  Arrival = 'arrival',
  Crash = 'crash',
}

export interface TrackerEvent {
  executionId: number;
  type: TrackerEventType;
  token: FlightToken;
  timestamp: number;
}

export interface TrackerDepartureEvent extends TrackerEvent {
  type: TrackerEventType.Departure;
}

export interface TrackerCrashEvent extends TrackerEvent {
  type: TrackerEventType.Crash;
  error: Error;
}

export interface TrackerArrivalEvent extends TrackerEvent {
  type: TrackerEventType.Arrival;
}

export interface TrackerListener {
  type: TrackerEventType;
  callback: (event: TrackerEvent) => void;
}
