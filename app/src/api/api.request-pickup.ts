import axios from 'axios';

import { TrackFlight, FlightToken } from './flight-track';
import { RequestPickup } from '@/lib/request-pickup';
import { UrApiResponse } from '@/lib/apiv2';

/**
 * Request Pickup API Class
 *
 *
 */
class RequestPickupApi {
  /**
   * Get All method
   *
   * Returns all details for the request pickup page
   *
   * @return {AxiosPromise} promise from the axios request
   */
  @TrackFlight(FlightToken.fetchRequestPickupData)
  getAll(transId, equipmentId, phone, email, accountId) {
    return axios.get<UrApiResponse<RequestPickup[]>>(
      `/api/v2/requestpickup?
      transId=${transId}&
      equipmentId=${equipmentId}&
      phone=${phone}&
      email=${email}&
      accountId=${accountId}`);
  }
}

export default new RequestPickupApi();
