import axios from 'axios';
import { TrackFlight, FlightToken } from './flight-track';

class GlobalSearchApi {
  @TrackFlight(FlightToken.getGlobalSearchResults)
  getResults(search) {
    return axios.get('/api/v1/global-search', {
      params: {
        search,
      },
    });
  }
}

export default new GlobalSearchApi();
