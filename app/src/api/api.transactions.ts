/*
  These functions are related to the Profile & Workplace pages. They are
  not used in the Marketplace (the Cart/Checkout process).
*/
import axios from 'axios';
import { TransactionMap } from '@/lib/transactions';
import { TrackFlight, FlightToken } from './flight-track';
import * as Qs from 'qs';

export interface FetchTransactionsRequest {
  accountId: string;
  transType: string;
  skipCache?: number;
}

/**
 * 1: Open Contract
 *    An open contract can have its rental extended, along with all other actions.
 * 2: Open Quotes
 * 3: Open Reservation
 *    An open reservation can have its PO changed, but cannot have its rental extended.
 * 4: Closed Contract
 * 6: Pickup Requests (must be the only type)
 * 7: Past Due (must be the only type)
 * 8: Canceled Quotes can only be grouped with type 9)
 * 9: Canceled Reservations (can only be grouped with type 8)
 */
interface WorkplaceEquipmentUpdatePoParams {
  po: string;
  transId: string;
  accountId: string;
  updateType: 'update';
}

class TransactionsApi {
  @TrackFlight(FlightToken.fetchTransactions)
  async fetch(request: FetchTransactionsRequest): Promise<TransactionMap> {
    const query = Qs.stringify(request, { addQueryPrefix: true });
    const { data } = await axios.get(`/api/v1/transactions${query}`);

    if (TransactionsApi.transactionsAreValid(data)) {
      return data;
    }
    else {
      const extractedMessage = Array.isArray(data) && data.length > 0 ? data[0] : data;
      return Promise.reject(extractedMessage);
    }
  }

  @TrackFlight(FlightToken.updateTransactionPo)
  updatePo({ transId, po, accountId, updateType = 'update' }: Partial<WorkplaceEquipmentUpdatePoParams>) {
    return axios.put('/api/v1/transaction-update', { transId, po, accountId, updateType });
  }

  static transactionsAreValid(data: any): data is TransactionMap {
    /*
      Even if the call fails, or there is no listed equipment,
      the API service will still return a 200 response, with a single element
      array containing a string indicating what went wrong, e.g. "No data returned."
      We need to ignore these items.
    */
    return data !== null
      &&  typeof data === 'object'
      &&  !Array.isArray(data)
      &&  Object.values(data).every((trans: { [k: string]: any }) =>
            typeof trans === 'object'
            && typeof trans.transId === 'string',
          );
  }
}

export default new TransactionsApi();
