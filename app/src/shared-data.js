/* eslint-disable no-multi-assign */
import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import get from 'lodash/get';

Vue.use(Vuex);

// Like Google does, make our global ur variable.
// Do the || in case of multiple loads.
const ur = window.ur || {};
window.ur = ur;

// Logger that can be filtered easily
export const log = {
  info: (message) => console.info('[ur]', message), // eslint-disable-line
  log: (message) => console.log('[ur]', message), // eslint-disable-line
  warn: (message) => console.warn('[ur]', message), // eslint-disable-line
  error: (message) => console.error('[ur]', message) // eslint-disable-line
};
ur.log = log;

// Store
window.ur.storageEngine = window.ur.storageEngine || window.localStorage;

window.ur.store = window.ur.store || new Vuex.Store({
  state: {},
  plugins: [
    createPersistedState({
      key: 'ur-shared-data',
      storage: window.ur.storageEngine,
    }),
  ],
  mutations: {},
});

export const bus = new Vue();
ur.bus = bus;

// Easy listeners on ur bus to open up modals
const modalRouteMap = {
  'link-account': 'linkAccountModal',
  'unlink-account': 'unlinkAccountModal',
  'add-to-cart': 'addToCartModal',
  'choose-location': 'lfChooseLocationModal',
  'user-details': 'userDetailsModal',
};

Object.entries(modalRouteMap).forEach(([name, routeName]) => {
  bus.$on(`modal:${name}`, params => {
    ur.log.info(`Triggering route for modal ${name}`);
    window._app.$router.push({ name: routeName, params });
  });
});

// Shortcut for brevity
ur.showModal = (name, params) => ur.bus.$emit(`modal:${name}`, params);


bus.$on('showLocationSearchOverlay', (selector, data = {}) => {
  const { UrGlobalSearch, UrLocationSearchAutoComplete } = window._app.$options.components;
  const useLegacyStyles = get(data, 'useLegacyStyles', true);
  const Constructor = Vue.extend({
    name: 'LocationSearchOverlay',
    components: { UrGlobalSearch, UrLocationSearchAutoComplete },
    template: `
    ${useLegacyStyles ? '<ur-legacy-styles>' : ''}
    <div class="global-search">
      <ur-location-search-auto-complete class="global-search__field global-search__location"></ur-location-search-auto-complete>
      <ur-global-search placeholder="equipment and tools"></ur-global-search>
    </div>
    ${useLegacyStyles ? '</ur-legacy-styles>' : ''}`,
    mounted() {
      this.$once('close', () => this.close());
    },
    methods: {
      close() {
        // destroy the vue listeners, etc
        this.$destroy();

        // remove the element from the DOM
        this.$el.parentNode.removeChild(this.$el);
      },
    },
  });
  const instance = new Constructor({
    parent: window._app,
  });
  instance.$mount();
  // If passed selector, append it to that element. Otherwise append it to root vm.
  const target = selector ? document.querySelector(selector) : instance.$root.$el;
  target.appendChild(instance.$el);
});

export default window.ur;
