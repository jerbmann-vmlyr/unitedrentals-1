/**
 * Common components
 */
import UrModal from '@/components/common/ur-modal.vue';

/**
 * Profile components
 */
import UserDetailsModal from '@/components/user-profile/user-details-modal.vue';
import EditCardModal from '@/components/user-profile/edit-card-modal.vue';
import RemoveCardModal from '@/components/user-profile/remove-card-modal.vue';
import PasswordExpirationModal from '@/components/user-profile/password-expiration-modal.vue';

export default [
  {
    path: '/profile/editUserDetails',
    name: 'userDetailsModal',
    components: {
      modal: UrModal,
    },
    props: {
      modal: () => ({
        classAdditional: 'edit-user-details-modal',
        content: UserDetailsModal,
      }),
    },
  },
  {
    path: '/profile/editCreditCard/:cardId',
    name: 'editCardModal',
    components: {
      modal: UrModal,
    },
    props: {
      modal: route => ({
        classAdditional: 'edit-card-modal',
        content: EditCardModal,
        contentProps: {
          cardId: route.params.cardId,
        },
      }),
    },
  },
  {
    path: '/profile/removeCreditCard/:cardId',
    name: 'removeCardModal',
    components: {
      modal: UrModal,
    },
    props: {
      modal: route => ({
        classAdditional: 'removeCardModal',
        content: RemoveCardModal,
        contentProps: {
          cardId: route.params.cardId,
        },
      }),
    },
  },
  {
    path: '/profile/editPasswordExpiration',
    name: 'passwordExpirationModal',
    components: {
      modal: UrModal,
    },
    props: {
      modal: () => ({
        classAdditional: 'password-expiration-modal',
        content: PasswordExpirationModal,
      }),
    },
  },
];
