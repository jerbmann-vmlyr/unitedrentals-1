import AccountRoutes from './account.routes';
import AddToCartRoutes from './add-to-cart.routes';
import CartCheckoutRoutes from './cart-checkout.routes';
import LocationFinderRoutes from './location-finder.routes';
import MessagingRoutes from './messaging.routes';
import PlatformAccountRoutes from './platform-account.routes';
import ProfileRoutes from './profile.routes';
import WorkplaceRoutes from './workplace.routes';

import ViewRoutes from '@/views/routes';

export const allRoutes = [
  ...AccountRoutes,
  ...AddToCartRoutes,
  ...CartCheckoutRoutes,
  ...LocationFinderRoutes,
  ...MessagingRoutes,
  ...PlatformAccountRoutes,
  ...ProfileRoutes,
  ...WorkplaceRoutes,
  ...ViewRoutes,
];

export function mergeRoutes(routes) {
  // Fancier way is https://router.vuejs.org/api/#router-addroutes
  // Thats what nuxt and proper module libs use. This is an easy babystep.
  allRoutes.push(...routes);
}

export default allRoutes;
