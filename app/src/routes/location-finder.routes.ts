/**
 * Common components
 */
import UrModal from '@/components/common/ur-modal.vue';
import ChooseLocationModal from '@/components/location-finder/choose-location-modal.vue';

export default [
  {
    path: '/locations/chooseLocationModal',
    name: 'lfChooseLocationModal',
    components: {
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: ChooseLocationModal,
        classAdditional: 'modal--choose-location-modal',
        contentProps: {
          contentSize: 'small',
          backOnClose: true,
        },
      }),
    },
  },

  {
    name: 'lfBase',
    path: '/locations',
    children: [
      {
        name: 'lfState',
        path: '/locations/:state',
        children: [
          {
            name: 'lfCity',
            path: ':city',
            children: [
              {
                name: 'lfType',
                path: ':branchType',
                children: [
                  {
                    name: 'lfBranch',
                    path: ':branch',
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    name: 'lfFilter',
    path: '/locations/search/:filter',
    children: [
      {
        name: 'lfSearch',
        path: ':search',
      },
    ],
  },
];
