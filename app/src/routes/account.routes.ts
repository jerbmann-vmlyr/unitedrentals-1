/**
 * Common components
 */
import UrModal from '@/components/common/ur-modal.vue';

/**
 * Account components
 */
import LinkAccountModal from '@/components/user-profile/link-account-modal.vue';
import UnlinkAccountModal from '@/components/user-profile/unlink-account-modal.vue';

export default [
  {
    path: '/account/link',
    name: 'linkAccountModal',
    components: {
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: LinkAccountModal,
        classAdditional: 'profile-link-account',
      }),
    },
  },

  {
    path: '/account/unlink/:acctId',
    name: 'unlinkAccountModal',
    components: {
      modal: UrModal,
    },
    props: {
      modal: route => ({
        content: UnlinkAccountModal,
        classAdditional: 'profile-unlink-account',
        contentProps: {
          acctId: route.params.acctId,
        },
      }),
    },
  },
];
