/**
 * Route names
 */
import { RouteConfig } from 'vue-router';
import { WorkplaceRouteNames } from '@/lib/workplace';

/**
 * Common components
 */
import UrModal from '@/components/common/ur-modal.vue';

/**
 * Workplace Equipment components
 */
import WorkplaceAccount from '@/components/workplace/workplace-account.vue';
import WorkplaceDashboard from '@/components/workplace/dashboard/dashboard.vue';
import WorkplaceUnauthenticatedDashboard from '@/components/workplace/dashboard/unauthenticated-dashboard.vue';
import WorkplaceNullStateDashboard from '@/components/workplace/dashboard/null-state-dashboard.vue';
import WorkplaceEquipment from '@/components/workplace/equipment-listing/equipment.vue';
import WorkplaceEquipmentDetails from '@/components/workplace/equipment-details/details.vue';
import WorkplaceEquipmentListing from '@/components/workplace/equipment-listing/equipment-list.vue';
import WorkplaceEquipmentUpdatePoModal from '@/components/workplace/equipment-listing/equipment-update-po-modal.vue';
import WorkplaceEquipmentRescheduleModal from '@/components/workplace/reschedule-request/reschedule-request-modal.vue';
import WorkplaceEquipmentRescheduleConfirmationModal from '@/components/workplace/reschedule-request/reschedule-confirmation-modal.vue';
import WorkplaceEquipmentVisibilitySettings from '@/components/workplace/equipment-listing/equipment-visibility-settings.vue';
import WorkplaceOrder from '@/components/workplace/order-listing/order.vue';
import WorkplaceOrderListing from '@/components/workplace/order-listing/order-list.vue';
import WorkplaceOrderDetails from '@/components/workplace/order-details/order-details.vue';
import WorkplaceOrderVisibilitySettings from '@/components/workplace/order-listing/order-visibility-settings.vue';

export const WorkplaceRoutesPathRoot = '/manage';

const workplaceRoutes: RouteConfig[] = [
  {
    path: `${WorkplaceRoutesPathRoot}/unauthenticated`,
    name: WorkplaceRouteNames.Unauth,
    component: WorkplaceUnauthenticatedDashboard,
  },
  {
    path: `${WorkplaceRoutesPathRoot}/no-account`,
    name: WorkplaceRouteNames.NoAccount,
    component: WorkplaceNullStateDashboard,
  },
  {
    path: `${WorkplaceRoutesPathRoot}/:accountId`,
    component: WorkplaceAccount,
    children: [
      {
        path: '',
        name: WorkplaceRouteNames.Root,
        redirect: 'dashboard',
      },
      {
        path: 'dashboard',
        name: WorkplaceRouteNames.Dashboard,
        component: WorkplaceDashboard,
      },
      {
        path: 'no-equipment',
        name: WorkplaceRouteNames.NoEquipment,
        component: WorkplaceNullStateDashboard,
      },
      {
        path: 'orders',
        component: WorkplaceOrder,
        children: [
          {
            path: '',
            name: WorkplaceRouteNames.OrderList,
            components: {
              default: WorkplaceOrderListing,
            },
          },
          {
            path: ':orderId', // maps to order transId
            name: WorkplaceRouteNames.OrderDetails,
            components: {
              default: WorkplaceOrderDetails,
            },
          },
          {
            path: 'customize',
            name: WorkplaceRouteNames.CustomizeOrderVisibility,
            components: {
              default: WorkplaceOrderListing,
              overlay: UrModal,
            },
            props: {
              overlay: () => ({
                closeOnBackgroundClick: true,
                closeOnEscapeKey: true,
                content: WorkplaceOrderVisibilitySettings,
              }),
            },
          },
        ],
      },
      {
        path: 'equipment',
        component: WorkplaceEquipment,
        children: [
          {
            path: '',
            name: WorkplaceRouteNames.EquipmentList,
            components: {
              default: WorkplaceEquipmentListing,
            },
          },
          {
            path: 'customize',
            name: WorkplaceRouteNames.CustomizeEquipmentVisibility,
            components: {
              default: WorkplaceEquipmentListing,
              overlay: UrModal,
            },
            props: {
              overlay: () => ({
                closeOnBackgroundClick: true,
                closeOnEscapeKey: true,
                content: WorkplaceEquipmentVisibilitySettings,
              }),
            },
          },
          {
            path: ':equipmentId/:transId/:transLineId',
            name: WorkplaceRouteNames.EquipmentDetails,
            components: {
              default: WorkplaceEquipmentDetails,
            },
            children: [
              {
                path: 'reschedule/:rescheduleType',
                name: WorkplaceRouteNames.RequestEquipmentReschedule,
                component: UrModal,
                props: route => ({
                  content: WorkplaceEquipmentRescheduleModal,
                  contentProps: {...route.params},
                }),
              },
              {
                path: 'confirm/:rescheduleType',
                name: WorkplaceRouteNames.ConfirmEquipmentReschedule,
                component: UrModal,
                beforeEnter: (to, from, next) => {
                  // if confirmation not from previous modal, then reroute
                  if (from.name !== WorkplaceRouteNames.RequestEquipmentReschedule) {
                    next({
                      replace: true,
                      name: WorkplaceRouteNames.EquipmentDetails,
                      ...to.params,
                    });
                  } else {
                    next();
                  }
                },
                props: route => ({
                  content: WorkplaceEquipmentRescheduleConfirmationModal,
                  contentProps: {...route.params},
                }),
              },
              {
                path: 'update-po/:po',
                name: WorkplaceRouteNames.UpdateEquipmentPo,
                component: UrModal,
                props: route => ({
                  content: WorkplaceEquipmentUpdatePoModal,
                  contentProps: {...route.params, contentSize: 'po'},
                }),
              },
            ],
          },
        ],
      },
    ],
  },
];

export default workplaceRoutes;
