/**
 * Common components
 */
import UrModal from '@/components/common/ur-modal.vue';

/**
 * Marketplace Equipment Listing components
 */
import AddToCartModal from '@/components/add-to-cart/add-to-cart-modal.vue';

export default [
  {
    components: {
      modal: UrModal,
    },
    name: 'addToCartModal',
    props: {
      modal: route => ({
        content: AddToCartModal,
        contentProps: {
          catClass: route.params.catClass,
          backOnClose: true,
        },
      }),
    },
    path: '/add-to-cart/:catClass',
  },
];
