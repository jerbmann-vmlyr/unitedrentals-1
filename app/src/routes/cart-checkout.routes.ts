import { CheckoutRouteNames } from '@/lib/checkout';

/**
 * Common components
 */
import CreateJobsiteFormModal from '@/components/jobsite/create-jobsite-form-modal.vue';
import UrModal from '@/components/common/ur-modal.vue';

/**
 * Cart & Checkout components
 */
import AccountDetailsFormModal from '@/components/account/account-details-form-modal.vue';
import AddCreditCard from '@/components/cart/reservation-block/billing/add-credit-card-modal.vue';
import AuthenticateToContinueCheckoutModal from '@/components/cart/authenticate-to-continue-checkout-modal.vue';
import BillingAccountModal from '@/components/cart/reservation-block/billing/edit-account-modal.vue';
import ConfirmAccountModal from '@/components/cart/confirm-account-info-modal.vue';
import CannotProceedModal from '@/components/cart/cannot-proceed-modal.vue';
import Cart from '@/components/cart/cart.vue';
import CheckoutDateChangeWarning from '@/components/cart/date-change-warning.vue';
import EditableCartItem from '@/components/cart/items/editable-cart-item.vue';
import ReservationBlockEditBranch from '@/components/cart/reservation-block/delivery-pickup/edit-branch.vue';
import ReservationBlockRppModal from '@/components/cart/reservation-block/billing/edit-rpp-modal.vue';
import SelectCreditCard from '@/components/cart/reservation-block/billing/select-credit-card.vue';
import TransactionComplete from '@/components/cart/transaction-complete.vue';

export default [
  {
    path: '/checkout',
    components: {
      checkout: Cart,
    },
  },
  {
    path: '/checkout/mobile-cart-item-form/:itemId/:isSavedForLater/:accountId?',
    components: {
      modal: UrModal,
      checkout: Cart,
    },
    props: {
      modal: route => ({
        content: EditableCartItem,
        contentProps: {
          isModal: true,
          isSavedForLater: route.params.isSavedForLater
            ? route.params.isSavedForLater === 'true'
            : false,
          itemId: route.params.itemId,
          startInEditMode: true,
          backOnClose: true
        },
      }),
    },
  },
  {
    path: '/checkout/transaction/:reservationBlockId',
    components: {
      checkout: TransactionComplete,
    },
    props: {
      checkout: true,
    },
  },
  {
    path: '/checkout/cannot-proceed',
    components: {
      checkout: Cart,
      modal: UrModal,
    },
    props: {
      checkout: true,
      modal: () => ({
        content: CannotProceedModal,
        contentProps: {
          closeDisabled: true,
          contentSize: 'small',
        },
      }),
    },
  },
  {
    path: '/checkout/item-unavailable',
    components: {
      checkout: Cart,
      modal: UrModal,
    },
    props: {
      checkout: true,
      modal: () => ({
        content: CannotProceedModal,
        contentProps: {
          closeDisabled: true,
          contentSize: 'small',
          itemsUnavailableMsg: true,
        },
      }),
    },
  },
  {
    name: 'checkout-error',
    path: '/checkout/error',
    components: {
      checkout: Cart,
      modal: UrModal,
    },
    props: {
      checkout: true,
      modal: router => ({
        content: CannotProceedModal,
        contentProps: {
          msg: router.params.msg,
          contentSize: 'small',
        },
      }),
    },
  },
  {
    path: '/checkout/transaction/:reservationBlockId/confirm-account',
    components: {
      checkout: TransactionComplete,
      modal: UrModal,
    },
    props: {
      checkout: true,
      modal: () => ({
        content: ConfirmAccountModal,
        classAdditional: 'confirm-account-modal',
        contentProps: {
          closeDisabled: true,
          contentSize: 'small',
        },
      }),
    },
  },
  {
    path: '/checkout/date-change-warning',
    components: {
      checkout: Cart,
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: CheckoutDateChangeWarning,
        classAdditional: 'messaging-modal',
        contentProps: {
          backOnClose: true
        },
      }),
    },
  },
  {
    path: '/checkout/authenticate-to-continue-checkout',
    components: {
      checkout: Cart,
      modal: UrModal,
    },
    props: {
      modal: () => ({
        classAdditional: 'ur-authentication-modal--wrapper',
        content: AuthenticateToContinueCheckoutModal,
        contentProps: {
          closeDisabled: true,
          backOnClose: true
        },
      }),
    },
  },
  {
    path: '/checkout/edit-branch/:reservationBlockId',
    components: {
      checkout: Cart,
      modal: UrModal,
    },
    props: {
      modal: () => ({ content: ReservationBlockEditBranch, classAdditional: 'location-finder-modal' }),
    },
    children: [
      {
        name: CheckoutRouteNames.LocationFinderFilter,
        path: 'locations/search/:filter',
        children: [
          {
            name: CheckoutRouteNames.LocationFinderSearch,
            path: ':search',
          },
        ],
      },
      {
        name: CheckoutRouteNames.LocationFinderState,
        path: 'locations/:state',
        children: [
          {
            name: CheckoutRouteNames.LocationFinderCity,
            path: ':city',
            children: [
              {
                name: CheckoutRouteNames.LocationFinderType,
                path: ':branchType',
                children: [
                  {
                    name: CheckoutRouteNames.LocationFinderBranch,
                    path: ':branch',
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },

  // Change the account for the reservation
  {
    path: '/checkout/edit-account/:reservationBlockId',
    components: {
      checkout: Cart,
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: BillingAccountModal,
        contentProps: {
          backOnClose: true
        }
      }),
    },
  },

  // Edit fields on the account (but keep the same account on the reservation)
  {
    path: '/checkout/modal/account-details-form/:blockId/:accountId',
    components: {
      checkout: Cart,
      modal: UrModal,
    },
    props: {
      modal: route => ({
        content: AccountDetailsFormModal,
        contentProps: {
          blockId: route.params.blockId,
          accountId: route.params.accountId,
        },
      }),
    },
  },

  {
    path: '/checkout/edit-rpp',
    name: CheckoutRouteNames.RppDisclaimerModal,
    components: {
      checkout: Cart,
      modal: UrModal,
    },
    props: {
      modal: () => ({ content: ReservationBlockRppModal }),
    },
  },
  {
    path: '/checkout/select-credit-card/:reservationBlockId',
    components: {
      checkout: Cart,
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: SelectCreditCard,
        contentProps: {
          closeDisabled: true,
        },
      }),
    },
  },
  {
    path: '/checkout/add-credit-card/:reservationBlockId',
    components: {
      checkout: Cart,
      modal: UrModal,
      contentProps: {
        backOnClose: true
      },
    },
    props: {
      modal: () => ({ content: AddCreditCard, classAdditional: 'add-credit-card-modal' }),
    },
  },
  // Add credit card without reservation block id - from Profile screen
  {
    path: '/checkout/add-credit-card',
    components: {
      checkout: Cart,
      modal: UrModal,
      contentProps: {
        backOnClose: true
      },
    },
    props: {
      modal: () => ({ content: AddCreditCard, classAdditional: 'add-credit-card-modal' }),
    },
  },
  {
    // There are THREE journeys that utilize this route:  Entering this route via the Cart Item Form, the Edit Jobsite Form,
    // and the Save for Later Form. The latter will not pass a reservationBlockId param, but the first two journeys will.
    path: '/checkout/create-jobsite/:accountId/:reservationBlockId?',
    components: {
      checkout: Cart,
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: CreateJobsiteFormModal,
        contentProps: {
          backOnClose: true
        }
      }),
    },
  },

  // Catch all for create jobsite modal
  {
    path: '*/create-jobsite/:accountId/',
    components: {
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: CreateJobsiteFormModal,
        contentProps: {
          backOnClose: true
        }
      }),
    },
  },
];
