/**
 * Cart & Checkout components
 */
import ReservationBlockRppModal from '@/components/cart/reservation-block/billing/edit-rpp-modal.vue';
import AddCreditCard from '@/components/cart/reservation-block/billing/add-credit-card-modal.vue';

/**
 * Convert Quotes components
 */
import QuoteConvert from '@/components/platform-account/quotes/quote-convert.vue';
import ConvertQuotesBillingAccountModal from '@/components/platform-account/quotes/convert-quote/billing/edit-account-modal.vue';
import ConvertQuotesSelectCreditCard from '@/components/platform-account/quotes/convert-quote/billing/select-credit-card.vue';
import NewQuoteNumberModal from '@/components/platform-account/quotes/convert-quote/new-quote-number-modal.vue';
import AccountDetailsForm from '@/components/platform-account/quotes/convert-quote/billing/convert-quotes-account-modal.vue';
import ConfirmConvertQuoteModal from '@/components/platform-account/quotes/convert-quote/confirm-convert-modal.vue';

/**
 * Common components
 */
import UrModal from '@/components/common/ur-modal.vue';
import CreateJobsiteFormModal from '@/components/jobsite/create-jobsite-form-modal.vue';

/**
 * Edit Quotes Components
 */
import EditQuotes from '@/components/platform-account/quotes/quote-detail.vue';
import QuoteEditBranch from '@/components/platform-account/quotes/src/quote-edit-branch.vue';

/**
 * Platform Account components
 */
import ReservationDetails from '@/components/platform-account/reservations/reservation-details.vue';
import ReservationsList from '@/components/platform-account/reservations/reservations-list.vue';
import CannotProceedModal from '@/components/platform-account/quotes/cannot-proceed-modal.vue';
import QuoteNotFound from '@/components/platform-account/quotes/quote-not-found.vue';

export default [
  {
    component: ReservationDetails,
    name: 'ObjectListingDetail',
    path: '/reservation-details/:requisitionId',
  },

  {
    component: ReservationsList,
    name: 'ReservationsList',
    path: '/reservation-list',
  },
  {
    components: {
      quotes: EditQuotes,
    },
    name: 'EditQuote',
    path: '/quote-details/:quoteId/:accountId?',
  },
  {
    name: 'EditQuoteCreateJobsite',
    path: '/quote-details/:quoteId/:accountId?/create-jobsite/:afterAddJobsiteAction/:blockId',
    components: {
      quotes: EditQuotes,
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: CreateJobsiteFormModal,
      }),
    },
  },
  {
    name: 'EditQuoteEditBranch',
    path: '/quote-details/quote-edit-branch/:quoteId/:accountId?',
    components: {
      quotes: EditQuotes,
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: QuoteEditBranch,
        classAdditional: 'location-finder-modal',
        contentProps: {
          returnTo: 'quote-details',
        },
      }),
    },
    children: [
      {
        name: 'lfFilterQuoteDetails',
        path: 'locations/search/:filter',
        children: [
          {
            name: 'lfSearchQuoteDetails',
            path: ':search',
          },
        ],
      },
      {
        name: 'lfStateQuoteDetails',
        path: 'locations/:state',
        children: [
          {
            name: 'lfCityQuoteDetails',
            path: ':city',
            children: [
              {
                name: 'lfTypeQuoteDetails',
                path: ':branchType',
                children: [
                  {
                    name: 'lfBranchQuoteDetails',
                    path: ':branch',
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    name: 'ConvertQuote',
    path: '/convert-quote/:quoteId/:accountId?',
    components: {
      quotes: QuoteConvert,
    },
  },
  {
    name: 'ConvertQuoteSuccess',
    path: '/convert-quote-success/:quoteId/:accountId?',
    components: {
      quotes: QuoteConvert,
    },
  },
  {
    name: 'ConvertQuoteEditAccount',
    path: '/convert-quote/:quoteId/:accountId?/edit-account',
    components: {
      quotes: QuoteConvert,
      modal: UrModal,
    },
    props: {
      modal: () => ({ content: ConvertQuotesBillingAccountModal }),
    },
  },
  {
    name: 'ConvertQuoteSelectCard',
    path: '/convert-quote/:quoteId/:accountId?/select-credit-card',
    components: {
      quotes: QuoteConvert,
      modal: UrModal,
    },
    props: {
      modal: () => ({ content: ConvertQuotesSelectCreditCard }),
    },
  },
  {
    name: 'ConvertQuoteEditRpp',
    path: '/convert-quote/:quoteId/:accountId?/edit-rpp',
    components: {
      quotes: QuoteConvert,
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: ReservationBlockRppModal,
        classAdditional: 'modal--quotes-rpp',
      }),
    },
  },
  {
    name: 'ConvertQuoteChangeAlert',
    path: '/convert-quote/:quoteId/:accountId?/quote-number-change-alert',
    components: {
      checkout: QuoteConvert,
      modal: UrModal,
    },
    props: {
      modal: () => ({ content: NewQuoteNumberModal }),
    },
  },
  {
    name: 'ConvertQuoteCreateJobsite',
    path: '/convert-quote/:quoteId/:accountId?/create-jobsite/:afterAddJobsiteAction/:blockId',
    components: {
      quotes: QuoteConvert,
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: CreateJobsiteFormModal,
      }),
    },
  },
  {
    name: 'ConvertQuoteAccountDetails',
    path: '/convert-quote/:accountId/account-details-form/:requisitionId',
    components: {
      quotes: QuoteConvert,
      modal: UrModal,
    },
    props: {
      modal: route => ({
        content: AccountDetailsForm,
        contentProps: {
          quoteId: route.params.quoteId,
          accountId: route.params.accountId,
        },
      }),
    },
  },
  {
    name: 'lfBaseConvertQuote',
    path: '/convert-quote/quote-edit-branch/:quoteId/:accountId?',
    components: {
      quotes: QuoteConvert,
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: QuoteEditBranch,
        classAdditional: 'location-finder-modal',
        contentProps: {
          returnTo: 'convert-quote',
        },
      }),
    },
    children: [
      {
        name: 'lfFilterMid',
        path: 'locations/search/:filter',
        children: [
          {
            name: 'lfSearchConvertQuote',
            path: ':search',
          },
        ],
      },
      {
        name: 'lfStateConvertQuote',
        path: 'locations/:state',
        children: [
          {
            name: 'lfCityConvertQuote',
            path: ':city',
            children: [
              {
                name: 'lfTypeConvertQuote',
                path: ':branchType',
                children: [
                  {
                    name: 'lfBranchConvertQuote',
                    path: ':branch',
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    name: 'ConvertQuoteAddCard',
    path: '/convert-quote/:quoteId/:accountId?/add-credit-card',
    components: {
      quotes: QuoteConvert,
      modal: UrModal,
    },
    props: {
      modal: () => ({
        content: AddCreditCard,
      }),
    },
  },
  {
    name: 'ConvertQuoteConfirmConvert',
    path: '/quote-list/quote-convert/:transId/:quoteId',
    components: {
      quotes: QuoteConvert,
      modal: UrModal,
    },
    props: {
      modal: route => ({
        classAdditional: 'confirm-convert-quote-modal',
        content: ConfirmConvertQuoteModal,
        contentProps: {
          transId: route.params.transId,
          quoteId: route.params.quoteId,
          closeOnBackgroundClick: true,
          closeOnEscapeKey: true,
        },
      }),
    },
  },
  {
    path: '/cannot-proceed',
    name: 'cannot-proceed',
    components: {
      modal: UrModal,
    },
    props: {
      checkout: true,
      modal: router => ({
        content: CannotProceedModal,
        contentProps: {
          closeDisabled: true,
          contentSize: 'small',
          errorMsg: router.params.errorMsg,
          accountId: router.params.accountId,
          reqId: router.params.reqId,
        },
      }),
    },
  },
  {
    components: {
      quotes: QuoteNotFound,
    },
    name: 'quote-not-found',
    path: '/quote-not-found',
  },
];
