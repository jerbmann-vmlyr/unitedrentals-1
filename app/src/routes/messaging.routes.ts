import MessagingRouter from '@/components/messaging/messaging-router.vue';

export default [
  {
    path: '/router',
    name: 'MessagingRouter',
    component: MessagingRouter,
  },
];
