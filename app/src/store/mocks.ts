import * as modules from './mocks.modules';
import * as plugins from './mocks.plugins';
import { cloneDeep } from 'lodash';

const getModules = () => {
  return cloneDeep(modules);
};

export { getModules, plugins };
