import Vue from 'vue';
import { ActionTree, MutationTree, GetterTree } from 'vuex';
// import { uniqBy } from 'lodash';
import { Jobsite } from '@/lib/common-types';
import { Colors } from '@/lib/constants';
import { StatusUtils } from '@/lib/statuses';
import {
  GoogleMapsUtils,
  WrappedPlaceResult,
} from '@/lib/google-maps';
// WorkplaceEquipmentJobsiteClusterWithMarkers,
import {
  WorkplaceEquipment,
  WorkplaceEquipmentJobsiteLocation,
  WorkplaceEquipmentJobsiteLocationMap,
  WorkplaceEquipmentJobsiteCluster,
  WorkplaceEquipmentMapsUtils,
  WorkplaceEquipmentStatusId,
  WorkplaceEquipmentUtils,
} from '@/lib/workplace';
import { WorkplaceRootState } from './module.workplace';
import types from './types';
const {
  getMapMarkerColorByStatus,
  getMapMarkerScale,
} = WorkplaceEquipmentMapsUtils;


export interface WorkplaceEquipmentLocationState {
  jobsiteLocations: {
    [accountId: string]: {
      [jobsiteId: string]: WorkplaceEquipmentJobsiteLocation;
    };
  };
}


const state: WorkplaceEquipmentLocationState = {
  jobsiteLocations: {},
};


const actions: ActionTree<WorkplaceEquipmentLocationState, WorkplaceRootState> = {
  async [types.workplace.equipmentLocation.getJobsiteLocationsFromEquipment](
    { commit, getters, dispatch },
    { accountId, equipment, jobsites }: {
      accountId: string,
      equipment: WorkplaceEquipment[],
      jobsites: Jobsite,
    },
  ): Promise<WorkplaceEquipmentJobsiteLocation[]> {
    // We only care about the jobsites associated with this equipment set...
    const validJobsites = Object.values(jobsites).filter(jobsite => (equipment.some(eq => eq.jobsite.id === jobsite.id)));
    // create an object keyed by jobsite.id of jobsites with valid lat, lng (provided by DAL)
    // if those objects exists in state.jobs.all for the current account
    const jobsitesWithGeolocationFromDal = Object.entries(validJobsites)
      .filter(([, { latitude }]) => !!latitude && parseInt(latitude, 10) !== 0)
      .reduce<{ [jobsiteId: string]: Jobsite }>((accumulator, [jobsiteId, jobsite]) => ({ ...accumulator, [jobsiteId]: jobsite }), {});

    // prepare an array of UNIQUE {address, jobsite} from equipment
    // that are NOT included in the jobsitesWithGeolocationFromDall array
    const compare = arr => current => arr.filter(other => other.id === current.id).length === 0;

    const invalidJobsites = validJobsites.filter(
      compare(Object.values(jobsitesWithGeolocationFromDal)));

    const jobsiteLocationRequests = invalidJobsites.map(jobsite => ({
      jobsite,
      address: WorkplaceEquipmentUtils.serializeJobsiteAddress(jobsite),
    }));

    // Send off the requests - If a jobsite doesn't have a valid location, return null
    const results  = await Promise.all(
      jobsiteLocationRequests.map(
        async ({ jobsite, address }): Promise<WorkplaceEquipmentJobsiteLocation | null> => {
          let out;
          const { place }: WrappedPlaceResult = await dispatch(
            types.googleMaps.getPlaceFromAddress,
            address,
          );

          if (place) {
            jobsite.latitude = place.geometry.location.lat();
            jobsite.longitude = place.geometry.location.lng();
            out = {
              accountId,
              address,
              jobsite,
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng(),
              place,
            };
          }
          else { // there was no place returned for this address, so...
            // we need to do a geolocation...
            const location = await dispatch(
              types.googleMaps.geocodeFromAddress,
              address,
            );
            if (location.location) {
              const lat = location.location.geometry.location.lat();
              const lng = location.location.geometry.location.lng();
              jobsite.latitude = lat;
              jobsite.longitude = lng;
              out = {
                accountId,
                address,
                jobsite,
                lat,
                lng,
                place: null,
              };
            }
          }
          return out;
        },
      ),
    );

    // merge results (from types.googleMaps.getPlaceFromAddress) with jobsitesWithGeolocationFromDal
    const combinedResults = [
      ...results,
      ...Object.values(jobsitesWithGeolocationFromDal)
        .map(WorkplaceEquipmentUtils.convertJobsiteIntoEquipmentLocation),
    ];

    // Filter out the results that have no valid location and
    // commit the objects to the store
    const out = combinedResults
      .filter<WorkplaceEquipmentJobsiteLocation>(
        (result): result is WorkplaceEquipmentJobsiteLocation => result !== null,
      )
      .map(result => {
        commit(types.workplace.equipmentLocation.saveJobsiteLocation, result);
        return result;
      });
    return out;
  },
};


const getters: GetterTree<WorkplaceEquipmentLocationState, WorkplaceRootState> = {
  [types.workplace.equipmentLocation.jobsiteLocationsForCurrentAccount](
    state,
    getters,
    rootState,
  ): WorkplaceEquipmentJobsiteLocationMap {
    return state.jobsiteLocations[(rootState.user as any).defaultAccountId] || {};
  },


  [types.workplace.equipmentLocation.equipmentWithValidJobsiteLocations](
    state,
    getters,
  ): WorkplaceEquipment[] {
    const equipment: WorkplaceEquipment[] = getters[
      types.workplace.equipmentForCurrentAccount
    ];

    const jobsiteLocations: WorkplaceEquipmentJobsiteLocationMap = getters[
      types.workplace.equipmentLocation.jobsiteLocationsForCurrentAccount
    ];

    const jobsiteLocationIds = Object.keys(jobsiteLocations);

    return equipment.filter(({ jobsite }) => jobsiteLocationIds.includes(jobsite.id));
  },

  // : WorkplaceEquipmentJobsiteCluster[]
  [types.workplace.equipmentLocation.jobsiteClusters]: (
    state,
    getters,
  ) => {
    // Get the jobsites and their locations for the currently selected accountId
    const jobsiteLocations: WorkplaceEquipmentJobsiteLocationMap = getters[
      types.workplace.equipmentLocation.jobsiteLocationsForCurrentAccount
    ];
    // Get the equipment for the currently selected accountId
    const equipment: WorkplaceEquipment[] = getters[
      types.workplace.equipmentForCurrentAccount
    ];

    const WorkplaceCountsByStatus = {
      AdvancedPickup: 'quantityPickedUp',
      OnRent: 'quantityOnRent',
      Overdue: 'quantityOnRent',
      DueSoon: 'quantityOnRent',
      PickupRequested: 'quantityPickedUp',
      ReservedForDelivery: 'quantityOnRent',
      ReservedForBranchPickup: 'quantityOnRent',
    };

    // Mux the jobsites and equipment into a usable object and return the object
    return Object.entries(jobsiteLocations)
      .map(([ jobsiteId, location ]) => {
        const jobsiteEquipment = equipment.filter(eqp => eqp.jobsite.id === jobsiteId);
        const groupedByStatus = StatusUtils.groupByStatuses(
          eqp => eqp.equipmentStatuses,
          jobsiteEquipment,
        );

        const statusPopulations = Object.entries(groupedByStatus).reduce((counts, [status, sites]) => ({
          ...counts,
          [status]: sites!.reduce((i, s) => WorkplaceCountsByStatus[status] ? i + s[WorkplaceCountsByStatus[status]] : 0, 0),
        }), {}) as { [S in WorkplaceEquipmentStatusId]?: number };

        return {
          ...location,
          statusPopulations,
          population: jobsiteEquipment.length,
        };
      });
  },

  // : WorkplaceEquipmentStatusId[]
  // : WorkplaceEquipmentJobsiteClusterWithMarkers[]
  [types.workplace.equipmentLocation.jobsiteMapMarkersFromStatuses]: (
    state,
    getters,
  ) => statuses => {
    let clusters: WorkplaceEquipmentJobsiteCluster[] = getters[
      types.workplace.equipmentLocation.jobsiteClusters
    ];

    clusters = clusters.filter(ea => Object.keys(ea.statusPopulations).some(r => statuses.includes(r)));

    // Generate and attach an array of markers to each cluster
    const out = clusters.map(cluster => {

      const { population, jobsite, statusPopulations, lat, lng } = cluster;

      // For each status in the equipmentStatuses prop, create a marker for this jobsite
      const markers = statuses.map(status => {
        const statusPopulation = statusPopulations[status]!;
        const fillColor = getMapMarkerColorByStatus(status);
        const scale = getMapMarkerScale(statusPopulation / population);
        return {
          options: {
            status,
            statusPopulation,
            jobsite,
          },
          position: { lat, lng },
          icon: {
            path: 0, // google.maps.SymbolPath.CIRCLE
            scale,
            fillColor,
            fillOpacity: 1,
            strokeWeight: 0,
          },
          label: {
            color: Colors.White,
            fontSize: '0.8571428571rem',
            fontWeight: '700',
            text: `${statusPopulation}`,
          },
        };
      });
      const adjustedMarkers = GoogleMapsUtils.adjustMarkerSpread(markers);

      return {
        ...cluster,
        markers: adjustedMarkers,
      };
    });
    return out;
  },
};


const mutations: MutationTree<WorkplaceEquipmentLocationState> = {
  [types.workplace.equipmentLocation.saveJobsiteLocation](
    state,
    location: WorkplaceEquipmentJobsiteLocation,
  ) {
    // Make sure an object with the location's accountId exists in state
    const { accountId, jobsite } = location;
    if (accountId) {
      state.jobsiteLocations = {
        ...state.jobsiteLocations,
        [accountId]: state.jobsiteLocations[accountId] || {},
      };
      Vue.set(state.jobsiteLocations[accountId], jobsite.id, location);
    }
  },
};


export default {
  state,
  actions,
  getters,
  mutations,
};
