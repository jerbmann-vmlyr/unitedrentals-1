import { ActionTree, GetterTree, MutationTree } from 'vuex';
import api from '@/api';
import types from '@/store/types';

export interface Po {
  accountId: string;
  balanceAmt: string;
  currencyCode: string;
  dataService: any; // only seeing NULL from api... Not sure what this is.
  poAmount: number;
  statusCode: string;
  statusCodeDesc: string;
}

export interface AccountPoNumberState {
  all: Po[],
  formats: [],
}

const state: AccountPoNumberState = {
  all: [],
  formats: [],
};

const getters: GetterTree<AccountPoNumberState, any> = {
  [types.accountPoNumbers.getPoNumbersByAccountId]: state => accountId => {
    return state.all.filter(po => po.accountId === accountId) as Po[];
  },
};

const actions: ActionTree<AccountPoNumberState, any> = {
  [types.accountPoNumbers.fetchNumbers]({commit}, accountId) {
    return api.accountPoNumbers.fetchNumbers(accountId)
      .then(({data}) => {
        commit(types.accountPoNumbers.fetchNumbers, { data: data.data });
      })
      .catch(() => {
        commit(types.accountPoNumbers.fetchNumbers, []);
      });
  },
  [types.accountPoNumbers.fetchFormats]({commit}, accountId) {
    if (!accountId){
      throw new Error(`No accountId selected`);
    }
    return api.accountPoNumbers.fetchFormats(accountId)
      .then(({data}) => {
        commit(types.accountPoNumbers.fetchFormats, { data: data.data });
      })
      .catch(() => {
        commit(types.accountPoNumbers.fetchFormats, []);
      });
  },
  [types.accountPoNumbers.reset]({ commit }) {
    commit(types.accountPoNumbers.reset);
  },
};

const mutations: MutationTree<AccountPoNumberState> = {
  [types.accountPoNumbers.fetchNumbers](state, input) {
    if (input.data) {
      state.all = input.data;
    }
  },
  [types.accountPoNumbers.fetchFormats](state, input) {
    state.formats = input.data || [];
  },
  [types.accountPoNumbers.reset](state) {
    state.all = [];
    state.formats = [];
  },
};

export default {
  getters,
  actions,
  mutations,
  state,
};
