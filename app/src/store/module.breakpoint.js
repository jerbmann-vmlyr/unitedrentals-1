/**
 * Exposes the current CSS @media breakpoint to JavaScript.
 *
 * Your components should use the `mapGetters` vuex utility to understand the current breakpoint.
 *
 * There is a getter for each media breakpoint. Map only the ones you need.
 *
 * Example:
 *
 * <template>
 *   <div>
 *     <p v-if="small">Your Browser is in Mobile Layout!</p>
 *     <p v-if="medium">Your Browser is in Tablet Layout!</p>
 *     <p v-if="large">Your Browser is in Desktop Layout!</p>
 *     <p v-if="xlarge">Your Browser is in the Super Wide Desktop Layout!</p>
 *     <p v-if="small || medium">This renders when window is in desktop or tablet breakpoints</p>
 *     <p v-if="!small">I render in all BUT mobile layouts!</p>
 *   </div>
 * </template>
 * <script>
 *   import {mapGetters} from 'vuex';
 *
 *   export default {
 *     // ...
 *     computed: {
 *       ...mapGetters([
 *         'small',
 *         'medium',
 *         'large',
 *         'xlarge'
 *       ])
 *     }
 *   }
 * </script>
 */

import { debounce } from 'lodash';
import types from '@/store/types';

const breakpoints = {
  small: 640,
  medium: 1024,
  large: 1440,
  xlarge: 1920,
};

const state = {
  currentWidth: null,
};

const getters = {
  small: ({ currentWidth }) => currentWidth <= breakpoints.small,
  medium: ({ currentWidth }) => currentWidth > breakpoints.small && currentWidth < breakpoints.medium,
  large: ({ currentWidth }) => currentWidth >= breakpoints.medium && currentWidth <= breakpoints.large,
  xlarge: ({ currentWidth }) => currentWidth > breakpoints.large,
};

const actions = {
  [types.breakpoint.watch]({ commit }) {
    commit(types.breakpoint.watch);
    window.onresize = debounce(() => {
      commit(types.breakpoint.watch);
    }, 150);
  },
};

const mutations = {
  [types.breakpoint.watch](state) {
    state.currentWidth = getBrowserWidth();
  },
};

function getBrowserWidth() {
  return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
}

export default {
  actions,
  getters,
  mutations,
  state,
};

export { breakpoints };
