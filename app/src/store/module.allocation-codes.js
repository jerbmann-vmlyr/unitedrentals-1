import Vue from 'vue';
import types from '@/store/types';
import api from '@/api';

const state = {

  /**
   * All Allocation Codes returned from /api/v1/allocation-codes.
   */
  allAccountingCodes: {},

  /**
   * All Requisition Codes returned from /api/v1/allocation-codes.
   */
  allRequisitionCodes: {},

  // Whether or not a fetch is in flight
  fetching: false,
};

const actions = {

  /**
   * Allows components to fetch accounting codes.
   *
   * TODO this should be renamed to fetchAccountingCodes!!!!
   *
   * @param {function} dispatch | vuex
   * @param {string} accountId | account id
   * @return {promise} ajax promise
   */
  [types.allocationCodes.fetchAllocationCodes]({ dispatch, state }, accountId) {
    if (Object.keys(state.allAccountingCodes[accountId] || {}).length > 0) {
      return Promise.resolve(state.allAccountingCodes[accountId]);
    }

    return dispatch(types.allocationCodes._fetch, { accountId, codeType: 'A' });
  },

  /**
   * Allows components to fetch requisition codes.
   * @param {function} dispatch | vuex
   * @param {string} accountId | account id
   * @return {promise} ajax promise
   */
  [types.allocationCodes.fetchRequisitionCodes]({ dispatch, state }, accountId) {
    if (Object.keys(state.allRequisitionCodes[accountId] || {}).length > 0) {
      return Promise.resolve(state.allRequisitionCodes[accountId]);
    }

    return dispatch(types.allocationCodes._fetch, { accountId, codeType: 'R' });
  },

  // Private. Components should use the above actions to get codes.
  [types.allocationCodes._fetch]({ commit }, { accountId, codeType }) {
    // Prevent allocation codes from ever being fetched on the service account
    if (!accountId) {
      return;
    }
    return api.allocationCodes.fetch({ accountId, codeType })
      .then(({ data }) => {
        commit(types.allocationCodes._fetch, { accountId, codeType, data });
      });
  },
};

const mutations = {
  [types.allocationCodes._fetch](state, { accountId, codeType, data }) {
    const field = codeType === 'R' ? 'allRequisitionCodes' : 'allAccountingCodes';
    if (Array.isArray(data) && data.length) {
      Vue.set(state[field], accountId, data);
    }
  },
  'allocationCodes/reset': function (state) {
    state.allAllocationCodes = {};
    state.allRequisitionCodes = {};
  },
};

export default {
  state,
  actions,
  mutations,
};
