import Vue from 'vue';
import { ActionTree, GetterTree, MutationTree } from 'vuex';

import types from './types';
import api from '@/api';
import { Invoice } from '@/lib/workplace';

export interface InvoicesState {
  all: {
    [accountId: string]: Invoice[];
  };
}

const state: InvoicesState = {
  all: {},
};

const actions: ActionTree<InvoicesState, any> = {
  async [types.invoices.fetch]({ commit, state }, data: {
    accountId: string,
    fromDate: string,
    toDate: string,
  }) {
    if (state.all[data.accountId]) {
      return;
    }

    const results = await api.invoices.fetchInvoices(data);
    commit(types.invoices.update, {
      accountId: data.accountId,
      invoices: results.data,
    });
  },
};

const mutations: MutationTree<InvoicesState> = {
  [types.invoices.update](state, data: {
    accountId: string,
    invoices: Invoice[],
  }) {
    Vue.set(state.all, data.accountId, data.invoices);
  },
};

type InvoiceGetter = (accountId: string, requisitionId: string) => Invoice | null;

const getters: GetterTree<InvoicesState, any> = {
  [types.invoices.getLastInvoiceForItem](state: InvoicesState): InvoiceGetter {
    return (accountId: string, transId: string) => {
      const invoices = (state.all[accountId] || [])
        .filter(item => item.id === transId && item.type === 'C')
        .sort((a, b) => Number(a.seqId) > Number(b.seqId) ? -1 : 1);

      return invoices[0] || null;
    };
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
};
