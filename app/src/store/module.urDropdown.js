import types from '@/store/types';

const state = {
  /**
   * UID for the one-and-only open UrDropdown
   */
  activeId: '',
};

const actions = {
  [types.urDropdown.setActiveId]({ commit }, id) {
    commit(types.urDropdown.setActiveId, id);
  },
};

const mutations = {
  [types.urDropdown.setActiveId](state, id) {
    state.activeId = id;
  },
};

export default {
  state,
  actions,
  mutations,
};
