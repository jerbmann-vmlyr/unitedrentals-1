import { GetterTree } from 'vuex';
import {
  SortUtils,
  GroupUtils,
  FilterUtils,
  SortDirection,
  CollectionUtils,
} from '@/lib/collections';
import {
  WorkplaceOrder,
  WorkplaceOrderGroup,
  WorkplaceOrderSorter,
  WorkplaceOrderFilter,
  WorkplaceOrderGrouper,
  WORKPLACE_ORDER_SORTERS,
  WORKPLACE_ORDER_GROUPERS,
  ParsedWorkplaceOrderFilter,
  WORKPLACE_ORDER_FILTERS,
  WorkplaceOrderFilterId,
} from '@/lib/workplace';

import { types } from '@/store';
import { WorkplaceRootState } from './module.workplace';

const { orderListing } = types.workplace;

const getters: GetterTree<{}, WorkplaceRootState> = {
  [orderListing.currentPage](_, __, { route }): number {
    const currentPageQuery = route && route.query ? route.query.page : '';
    return Number.isSafeInteger(+currentPageQuery)
      ? Math.max(0, +currentPageQuery)
      : 0;
  },

  [orderListing.filters](_, getters, { route }): WorkplaceOrderFilter[] {
    /**
     * We gotta patch the labels for any currently-applied jobsite filters
     * so that the label includes the jobsite name, rather than just the ID.
     *
     * Incredibly hacky, but this is technically the best place to do it
     * so that we can keep the filter badge component generic.
     */
    const resources: WorkplaceOrder[] = getters[types.workplace.ordersForCurrentAccount];
    const patchJobsiteFilterLabels = (parsed: ParsedWorkplaceOrderFilter) => {
      return {
        ...parsed,
        filters: parsed.filters!.map(f => {
          const matchingResource = resources.find(resource => resource.jobsite.id === f.queryValue);
          return {
            ...f,
            label: !matchingResource
              ? f.label
              : [
                matchingResource.jobsite.name,
                matchingResource.customerJobId
                  ? matchingResource.customerJobId
                  : `${matchingResource.jobsite.city}, ${matchingResource.jobsite.state}`,
              ].join(' - '),
          };
        }),
      };
    };

    const queryObject = route && route.query || {};

    return FilterUtils.parseFiltersFromQuery(WORKPLACE_ORDER_FILTERS, queryObject)
      .map(f => f.id === WorkplaceOrderFilterId.Jobsite
        ? patchJobsiteFilterLabels(f)
        : f,
      );
  },

  [orderListing.grouper](_, __, { route }): WorkplaceOrderGrouper {
    return CollectionUtils.selectOne(WORKPLACE_ORDER_GROUPERS, route.query.groupBy || '');
  },

  [orderListing.sorter](_, __, { route }): WorkplaceOrderSorter {
    return CollectionUtils.selectOne(WORKPLACE_ORDER_SORTERS, route.query.sortBy || '');
  },

  [orderListing.sortDirection](_, __, { route }): SortDirection {
    const directionParam = route.query.sortDirection;
    return SortUtils.isSortDirection(directionParam) ? directionParam : SortDirection.Ascending;
  },

  [orderListing.filteredResources](_, getters): WorkplaceOrder[] {
    return FilterUtils.applyFilters(
      getters[orderListing.filters],
      getters[types.workplace.ordersForCurrentAccount],
    );
  },

  [orderListing.groupedResources](_, getters): WorkplaceOrderGroup[] {
    return GroupUtils.applyGrouper(
      getters[orderListing.grouper],
      getters[orderListing.filteredResources],
    );
  },

  [orderListing.sortedResources](_, getters): WorkplaceOrderGroup[] {
    return SortUtils.applySorterToGroups(
      getters[orderListing.sorter],
      getters[orderListing.sortDirection],
      getters[orderListing.groupedResources],
    );
  },
};

export default {
  getters,
};
