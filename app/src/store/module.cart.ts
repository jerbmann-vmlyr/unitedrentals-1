import * as moment from 'moment';
import Vue from 'vue';
import { ActionTree, MutationTree, GetterTree } from 'vuex';

import api from '@/api';
import types from '@/store/types';
import { groupBy } from 'lodash';
import CartItem from '@/lib/cartItem';
import { TRANSACTION_TYPE_RESERVATION } from '@/lib/transaction';
import ReservationBlock from '@/lib/reservation-block.js';

import { FlightToken } from '@/api/flight-track';

export interface CartState {
  cartId: string;
  items: { [itemId: string]: CartItem };
  confirmed: boolean,
}

const state: CartState = {
  cartId: '',
  /**
   * Cart Items, keyed by cart item id. Data is returned from the GET cart API
   */
  items: {},
  confirmed: false,
};

const getters: GetterTree<CartState, any> = {
  /**
   * Returns the number of actual items in the cart (by adding up cart item quantity fields)
   *
   * @param {object} state - cart module state
   * @return {number} sum of each cart item's quantity field
   */
  cartSize: state => Object.values(state.items).reduce((total: number, item) => total + Number(item.quantity), 0),

  [types.cart.fetching](state, getters) {
    return getters[types.inFlightRequests.tokensAreInFlight](
      FlightToken.fetchCart,
    );
  },

  // We have two master-records for the cart.
  // Unauthenticated users utilize a sessionStorage-based cart for their source of truth. ("Local" cart.)
  // Authenticated users utilize the drupal cart api for their source of truth. ("Drupal" cart.)
  [types.cart.isLocal](state, getters) {
    return !state.cartId && getters.cartSize > 0;
  },

  // Computes and instantiates Reservation Blocks from Cart Items.
  reservationBlocks(state, getters, rootState) {
    // Group the cart items into sets of reservations. In other words, these are the groups of items we can
    // rent together in a single transaction.
    const blocks: object = groupBy(state.items, ({
      pickup, delivery, jobsiteId, accountId, locationString, startDate, endDate, requesterId,
    }) => `${pickup} ${delivery} ${jobsiteId || locationString} ${accountId} ${startDate} ${endDate} ${requesterId}`);

    // Instantiate Reservation Blocks from the items
    Object.entries(blocks).forEach(([key, items]) => {
      blocks[key] = new ReservationBlock(items, rootState);
    });

    // Rekey the blocks object by block.id and return it
    return Object.values(blocks).reduce((allBlocks, block) => ({
      ...allBlocks,
      [block.id]: block,
    }), {});
  },

  reservationsCount(state, getters) {
    return Object.keys(getters.reservationBlocks).length;
  },

  expectedTotalTransactionCount(state, getters) {
    const numReservationBlocks = Object.keys(getters.reservationBlocks).length;
    return numReservationBlocks;
  },

  transactionResultsCount(state, getters, rootState) {
    const numCompletedTransactions =
      Object.values(rootState.checkout.transactionBlocks)
        .filter((transaction: any) => transaction.transType === TRANSACTION_TYPE_RESERVATION).length;
    return numCompletedTransactions;
  },
};

const actions: ActionTree<CartState, any> = {
  [types.cart.fetch]({ commit }) {
    return api.cart.fetch()
      .then(({ data }) => {
        const items = data && data.items ? data.items : {};
        const cartId = data.cart_id;
        commit(types.cart.fetch, { cartId, items });
      });
  },

  /**
   * Adds an item to the cart.
   *
   * @param {object} context - Vuex context
   */
  [types.cart.addItem](context, cartItem) {
    const {
      commit,
      state,
      rootState,
    } = context;
    cartItem = new CartItem(cartItem);

    // Add the item to the Cart (either via the Drupal API or not)
    if (rootState.user.isLoggedIn) {
      delete cartItem.id;
      return api.cart.addItem(cartItem)
        .then(({ data }): any => {
          if (data.error) {
            return Promise.reject(data.message);
          }

          // This is called so all of our items get drupal id's, not browser-generated id's, and in some cases
          // to add a cartId. (For anonymous users with a cart who then logged in, and got the saveLocalCart method invoked.)
          const { cartId } = Object.values(data.items)[0] as any; // grab the cart id off an arbitrary item in the cart.
          commit(types.cart.fetch, {items: data.items, cartId });
        });
    }
    else {
      // Anonymous/Guest users will NOT persist their cart through the API.
      // Their cart exists only in Vuex and is persisted in the browser's session storage.
      // The front-end will try to merge cart items if possible, otherwise it will add the item.

      const existingItem = cartUtils.canMerge(cartItem, state.items, Object.keys(state.items));

      if (existingItem) {
        commit(types.cart.updateItem, {id: existingItem.id, field: { quantity: existingItem.quantity + cartItem.quantity }});
      }
      else {
        commit(types.cart.addItem, cartItem);
      }

      return null;
    }
  },

  [types.cart.removeItem]({ commit, rootState, getters }, id) {
    const isLocalCart = !getters[types.cart.isLocal]; // this must be declared BEFORE we possibly remove the last item from the cart.
    // TODO it would be nice to have the cart.isLocal getter not require a cart length.
    commit(types.cart.removeItem, id);
    return (rootState.user.isLoggedIn && isLocalCart)
      ? api.cart.removeItem(id)
      : null;
  },

  // Updates one or many fields on a single cart item
  [types.cart.updateItem]({ commit, state, rootState, getters }, { id, field }) {
    commit(types.cart.updateItem, { id, field });
    return rootState.user.isLoggedIn && !getters[types.cart.isLocal]
      ? api.cart.updateItem(state.items[id])
      : null;
  },

  // Makes changes to items within a reservation. Does not persist changes to server.
  // (This action is usually called separately.)
  [types.cart.updateItemsByReservationId]({ commit, getters, state }, { reservationBlockId, field }) {
    try {
      getters.reservationBlocks[reservationBlockId].items.forEach(({ id }) => {
        commit(types.cart.updateItem, { id, field });
      });
    }
    catch (e) {
      console.warn(`module.cart.ts: Failed to update items by reservation id (${reservationBlockId}). ${e}`);
    }
  },

  // Saves changes to all items within a reservation to the Drupal database
  [types.cart.saveItemsByReservationId]({ commit, getters, rootState }, payload) {
    if (rootState.user.isLoggedIn && !getters[types.cart.isLocal]) {
      return api.cart.updateItems(payload);
    }
    else {
      return console.warn('Will not save items to API. User is either not logged in or utilizing a local cart');
    }
  },

  /**
   * Updates all cart items. Accepts new field(s) data, and then persists changes to server.
   *
   * Cannot update special fields like startDate or endDate (nor times)
   *
   * @param {any} commit
   * @param {prop: value} field - can have one or many props
   * @returns {any | Promise<AxiosResponse>}
   */
  [types.cart.updateAllItems]({ commit, state, rootState, getters }, field) {
    Object.keys(state.items).forEach(id => commit(types.cart.updateItem, { id, field }));
    if (rootState.user.isLoggedIn && !getters[types.cart.isLocal]) {
      api.cart.updateItems(state.items);
    }
  },

  /*
  Saves each item in the local cart to drupal, and then destroys itself.
  This action is invoked by app.vue when a user is logged in (and has a localCart)
  Note: items MUST BE ADDED SEQUENTIALLY (as opposed to firing off multiple requests in parallel).
  Failure to do this will potentially cause Drupal to create a new session for each item added.
  (This happens because Drupal is expecting this call to be the one that creates a session for the user,
  which won't do until the first request completes.)
   */
  async [types.cart.saveLocalCart]({ state }) {
    // Serialize requests. https://stackoverflow.com/questions/37576685/using-async-await-with-a-foreach-loop#answer-37576787
    for (const id in state.items) {
      if (state.items.hasOwnProperty(id)) {
        const payload = new CartItem(state.items[id]);
        const response = await api.cart.addItem(payload);
        try {
          const { cartId } = Object.values(response.data.items)[0] as any;
          state.cartId = cartId; // TODO this should be in a mutation!!!! Never here!
        }
        catch (e) {
          // no op
        }
      }
    }
  },

  /*
  Saves anonymous Drupal cart.
  This action is invoked by app.vue an anonymous user's cart is updated.
  Note: items MUST BE ADDED SEQUENTIALLY (as opposed to firing off multiple requests in parallel).
  Failure to do this will potentially cause Drupal to create a new session for each item added.
  (This happens because Drupal is expecting this call to be the one that creates a session for the user,
  which won't do until the first request completes.)
   */
  async [types.cart.saveAnonymousCart]({ state }) {
    // Start by fetching the user's current cart.
    api.cart.fetch()
      .then(({ data }) => {
        // Loop through saved cart and remove items no longer in incoming cart ( if same cart ).
        const savedCartItems = data && data.items ? data.items : {};
        if (state.cartId === data.cart_id) {
          for (const id in savedCartItems) {
            if (!state.items.hasOwnProperty(id)) {
              api.cart.removeItem(id);
            }
          }

          // Loop through items coming in and add / update them.
          for (const id in state.items) {
            if (state.items.hasOwnProperty(id)) {
              const payload = new CartItem(state.items[id]);
  
              // Add item if it doesn't exist yet.
              if(!savedCartItems.hasOwnProperty(id)) {
                api.cart.addItem(payload)
                  .then(({ data }) => {
                    try {
                      const { cartId } = Object.values(data.data.items)[0] as any;
                      state.cartId = cartId;
                    }
                    catch (e) {
                      // no op
                    }
                  });
              }
              // Otherwise update the item in cart.
              else {
                api.cart.updateItem(payload)
                  .then(({ data }) => {
                    try {
                      const { cartId } = Object.values(data.data.items)[0] as any;
                      state.cartId = cartId;
                    }
                    catch (e) {
                      // no op
                    }
                  });
              }
            }
          }
        }
        // Otherwise the items need to be added to state.
        else {
          state.cartId = data.cart_id;
          for (const id in savedCartItems) {
            if (!state.items.hasOwnProperty(id)) {
              let item = new CartItem(savedCartItems[id]);
              Vue.set(state.items, item.id, item);
            }
          }
        }
      });
  },

  [types.cart.setConfirmed]({ commit }, input) {
    commit(types.cart.setConfirmed, input);
  },
};

const mutations: MutationTree<CartState> = {
  [types.cart.fetch](state, { cartId, items }) {
    state.cartId = cartId;
    state.items = {};
    Object.values<CartItem>(items).forEach(item => {
      Vue.set(state.items, item.id, new CartItem(item));
    });
  },

  [types.cart.addItem](state, item) {
    // Add the item to the cart
    item = new CartItem(item);
    Vue.set(state.items, item.id, item);
  },

  [types.cart.removeItem](state, id) {
    Vue.delete(state.items, id);
  },

  /**
   * Update Item Field(s)
   *
   * Updates an items field(s) based off the passed parameters.
   *
   * WARNING:
   * merely setting the moment WILL NOT BE REACTIVE IN VUE
   * We MUST make a reassignment.  Here, we reassign the moment back to the field
   * after `null`ing it which seems overly complicated--but is indeed necessary.
   *
   * @param {object} state | current state
   * @param {object} id, field | object in the format of {id: itemIdToUpdate, field: { itemPropToUpdate: valueToSet} }
   */
  [types.cart.updateItem](state, { id, field }: { id: string, field: Partial<CartItem> }) {
    Object.entries(field).forEach(([key, newValue]) => {
      // TODO if key isn't a prop on item, throw an error.
      switch (key as keyof CartItem) {
        // Special strategy for dealing with updating hours (strings) or dates (moment objects):
        case 'startDate':
        case 'endDate':
          state.items[id][key] = null;
          state.items[id][key] = newValue;
          break;
        default:
          // general update strategy:
          state.items[id][key] = newValue;
      }
    });
  },

  // Cart Items have properties that are Moment instances. When persisted to browser session storage, they get
  // serialized as UTC date strings.  When rebuilding these properties from sessionStorage (e.g., after a page refresh)
  // we convert this UTC date string back into a proper, local-time Moment instance.
  [types.cart.fixHydrationFromVuexPersistedState](state) {
    Object.values(state.items).forEach(item => {
      if (item.startDate) {
        item.startDate = moment.utc(item.startDate).local();
        item.endDate = moment.utc(item.endDate).local();
      }

      const newCartItem = new CartItem(item);

      Vue.set(state.items, item.id, newCartItem);
    });
  },

  ['cart/reset'](state) {
    state.cartId = '';
    state.items = {};
  },

  [types.cart.setConfirmed](state, input) {
    state.confirmed = input;
  },
};

export const cartUtils = {
  canMerge,
  merge,
};

export default {
  actions,
  getters,
  mutations,
  state,
};

/**
 * Utility methods
 */
function canMerge(newItem, items, [id, ...ids]: string[]) {
  const existingItem = items[id];

  if (!existingItem) {
    return false;
  }

  const itemExists = newItem.id !== existingItem.id
    && newItem.catClass === existingItem.catClass
    && newItem.startDate.format('YYYYMMDD') === existingItem.startDate.format('YYYYMMDD')
    && newItem.endDate.format('YYYYMMDD') === existingItem.endDate.format('YYYYMMDD')
    && (newItem.jobsiteId === existingItem.jobsiteId || newItem.locationString === existingItem.locationString);

  if (itemExists) {
    return existingItem;
  }

  return ids.length
    ? canMerge(newItem, items, ids)
    : false;
}

function merge(newItem, existingItem) {
  existingItem.notes = existingItem.notes || newItem.notes || '';
  existingItem.quantity += newItem.quantity;

  return existingItem;
}
