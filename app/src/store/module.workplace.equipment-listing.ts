import { GetterTree } from 'vuex';
import {
  CollectionUtils,
  FilterUtils,
  SortDirection,
  GroupUtils,
  SortUtils,
} from '@/lib/collections';
import {
  WORKPLACE_EQUIPMENT_FILTERS,
  WORKPLACE_EQUIPMENT_GROUPERS,
  WORKPLACE_EQUIPMENT_SORTERS,
  WorkplaceEquipmentFilterId,
  WorkplaceEquipment,
  WorkplaceEquipmentFilter,
  WorkplaceEquipmentSorter,
  WorkplaceEquipmentGrouper,
  WorkplaceEquipmentGroup,
  ParsedWorkplaceEquipmentFilter,
} from '@/lib/workplace';
import { WorkplaceRootState } from './module.workplace';

import types from './types';

const getters: GetterTree<{}, WorkplaceRootState> = {

  [types.workplace.equipmentListing.currentPage](_, __, { route }): number {
    const currentPageQuery = route && route.query
      ? route.query.page
      : '';
    return Number.isSafeInteger(+currentPageQuery)
      ? Math.max(0, +currentPageQuery)
      : 0;
  },

  [types.workplace.equipmentListing.filters](_, getters, { route }): WorkplaceEquipmentFilter[] {
    /**
     * We gotta patch the labels for any currently-applied jobsite filters
     * so that the label includes the jobsite name, rather than just the ID.
     *
     * Incredibly hacky, but this is technically the best place to do it
     * so that we can keep the filter badge component generic.
     */
    const equipment: WorkplaceEquipment[] = getters[types.workplace.equipmentForCurrentAccount];
    const patchJobsiteFilterLabels = (parsed: ParsedWorkplaceEquipmentFilter) => {
      return {
        ...parsed,
        filters: parsed.filters!.map(f => {
          const matchingEqp = equipment.find(eqp => eqp.jobsite.id === f.queryValue);
          return {
            ...f,
            label: !matchingEqp
              ? f.label
              : [
                  matchingEqp.jobsite.name,
                  matchingEqp.customerJobId.length > 0
                    ? matchingEqp.customerJobId
                    : `${matchingEqp.jobsite.city}, ${matchingEqp.jobsite.state}`,
                ].join(' - '),
          };
        }),
      };
    };

    const queryObj = route && route.query || {};

    return FilterUtils.parseFiltersFromQuery(WORKPLACE_EQUIPMENT_FILTERS, queryObj)
      .map(f => f.id === WorkplaceEquipmentFilterId.Jobsite
        ? patchJobsiteFilterLabels(f)
        : f,
      );
  },

  [types.workplace.equipmentListing.grouper](_, __, { route }): WorkplaceEquipmentGrouper {
    return CollectionUtils.selectOne(WORKPLACE_EQUIPMENT_GROUPERS, route.query.groupBy || '');
  },

  [types.workplace.equipmentListing.sorter](_, __, { route }): WorkplaceEquipmentSorter {
    return CollectionUtils.selectOne(WORKPLACE_EQUIPMENT_SORTERS, route.query.sortBy || '');
  },

  [types.workplace.equipmentListing.sortDirection](_, __, { route }): SortDirection {
    const directionParam = route.query.sortDirection;
    return SortUtils.isSortDirection(directionParam) ? directionParam : SortDirection.Ascending;
  },

  [types.workplace.equipmentListing.filteredEquipment](_, getters): WorkplaceEquipment[] {
    return FilterUtils.applyFilters(
      getters[types.workplace.equipmentListing.filters],
      getters[types.workplace.equipmentForCurrentAccount],
    );
  },

  [types.workplace.equipmentListing.groupedEquipment](_, getters): WorkplaceEquipmentGroup[] {
    return GroupUtils.applyGrouper(
      getters[types.workplace.equipmentListing.grouper],
      getters[types.workplace.equipmentListing.filteredEquipment],
    );
  },

  [types.workplace.equipmentListing.sortedEquipment](_, getters): WorkplaceEquipmentGroup[] {
    return SortUtils.applySorterToGroups(
      getters[types.workplace.equipmentListing.sorter],
      getters[types.workplace.equipmentListing.sortDirection],
      getters[types.workplace.equipmentListing.groupedEquipment],
    );
  },
};

export default {
  getters,
};
