/**
 * The Cart Item Editor vuex module is consumed by both the Add to Cart and (Cart) Edit-Item forms (components).
 *
 * A (Edited) Cart Item, just like things staged in Git, is an item that staged for adding to the cart.
 * The Add to Cart action will forward the stagedItem to the API to get added to the cart.
 *
 * Both the Add to Cart and (Cart's) Edit Item forms have the following fields (with one exception noted below)
 *    - catClass
 *    - quantity
 *    - startDate
 *    - endDate
 *    - accountId
 *    - projectId
 *    - jobId
 *    - requesterId
 *    - notes (this field is only available via the Edit Item on the Cart page)
 *    - location
 *
 * Can the user edit more than one product simultaneously? No. The design, and this architecture, demand
 * there be only one instance of an edited product at a time.
 * In desktop creative, this form is inline with other dom. In tablet/mobile, it's in a modal.
 *
 * For authenticated users, neither form supports a Location field. However, unauthenticated users
 * are allowed to use the Autocomplete widget to set a Location (aka, state.user.ratesPlace).
 *
 */
import types from '@/store/types';
import moment from 'moment';
import utils from '@/utils';
import CartItem from '@/lib/cartItem';

import Vue from 'vue';

const state = {
  id: '',

  catClass: '',

  quantity: 1,

  accountId: '',
  branchId: '',
  locationString: '',
  jobsiteId: '',
  projectId: '',
  requesterId: '',

  startDate: '',
  endDate: '',

  notes: '',

  delivery: true,
  pickup: true,

  isActive: [],
};

const getters = {

};

const actions = {
  [types.cartItemForm.decrement]({ commit, state }) {
    commit(types.cartItemForm.quantity, state.quantity - 1);
  },

  [types.cartItemForm.increment]({ commit, state }) {
    commit(types.cartItemForm.quantity, state.quantity + 1);
  },

  [types.cartItemForm.quantity]({ commit }, quantity) {
    commit(types.cartItemForm.quantity, quantity);
  },

  [types.cartItemForm.loadCartItem]({ commit, rootState }, data) {
    let cartModule = 'cart';

    if (data.isSavedForLater) {
      cartModule = 'cartSaveForLater';
    }

    commit(types.cartItemForm.loadCartItem, rootState[cartModule].items[data.id]);
  },

  [types.cartItemForm.loadDefaults]({ commit, getters, rootState }, catClass) {
    const {
      defaultAccountId, defaultJobId, ratesPlaceBranch, defaultStartDate, defaultEndDate, defaultRequesterId,
    } = rootState.user;
    const ratesPlaceAddress = getters[types.user.ratesPlaceAddress];

    commit(types.cartItemForm.loadDefaults, {
      catClass, defaultAccountId, defaultRequesterId, defaultJobId, ratesPlaceBranch, ratesPlaceAddress, defaultStartDate, defaultEndDate,
    });
  },

  [types.cartItemForm.updateActive]({ commit }, { itemId, isActive }) {
    commit(types.cartItemForm.updateActive, { itemId, isActive });
  },

  [types.cartItemForm.updateFormField]({ commit }, field) {
    commit(types.cartItemForm.updateFormField, field);
  },
};

const mutations = {
  [types.cartItemForm.loadCartItem](state, cartItem) {
    const {
      accountId,
      branchId,
      catClass,
      delivery,
      endDate,
      id,
      jobsiteId,
      locationString,
      pickup,
      notes,
      quantity,
      projectId,
      requesterId,
      startDate,
    } = new CartItem(cartItem);

    state.accountId = accountId;
    state.branchId = branchId;
    state.catClass = catClass;
    state.endDate = endDate;
    state.id = id;
    state.jobsiteId = jobsiteId;
    state.locationString = locationString;
    state.notes = notes;
    state.quantity = quantity;
    state.projectId = projectId;
    state.requesterId = requesterId;
    state.startDate = startDate;
    state.delivery = delivery;
    state.pickup = pickup;
  },

  [types.cartItemForm.loadDefaults](state, {
    catClass, defaultAccountId, defaultJobId, defaultStartDate, defaultEndDate, defaultRequesterId, ratesPlaceBranch, ratesPlaceAddress,
  }) {
    state.accountId = defaultAccountId;
    state.catClass = catClass;
    state.jobsiteId = defaultJobId;
    state.branchId = ratesPlaceBranch;
    state.locationString = ratesPlaceAddress;
    state.id = '';
    state.quantity = 1;
    state.startDate = moment(defaultStartDate, utils.dateFormat.iso);
    state.endDate = moment(defaultEndDate, utils.dateFormat.iso);
    state.notes = '';
    state.delivery = false;
    state.pickup = false;
    state.requesterId = defaultRequesterId;
  },

  [types.cartItemForm.quantity](state, quantity) {
    quantity = Number(quantity);
    const maxQantityValue = 999;

    if (quantity > maxQantityValue) {
      state.quantity = maxQantityValue;
    }
    else {
      state.quantity = quantity > 0 ? quantity : 1;
    }
  },

  // Adds or removes a unique item id to the list of actively-edited
  [types.cartItemForm.updateActive](state, { itemId, isActive }) {
    if (isActive) {
      state.isActive.push(itemId);
    }
    else {
      state.isActive = state.isActive.filter(existingId => existingId !== itemId);
    }
  },

  [types.cartItemForm.updateFormField](state, field) {
    const key = Object.keys(field)[0];

    Vue.set(state, key, field[key]);
  },
};

export default {
  actions,
  getters,
  mutations,
  state,
};
