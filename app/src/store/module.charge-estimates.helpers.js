/* eslint-disable no-console */
import { isPopulatedObject, buildParams } from '@/store/module.catalog.helpers';
import types from '@/store/types';
import { pick } from 'lodash';

// Need to build items
const makeEstimateItems = items => items.map(x => pick(x, [
  'quantity',
  'catClass',
  'currency',
  'dayRate',
  'minRate',
  'monthRate',
  'rate',
  'weekRate',
])).reduce((acc, obj) => {
  acc[obj.catClass] = obj;
  return acc;
}, {});

const makeEstimateParams = ({
  accountId, branchId, requisitionId, pickup, jobsite, returnDateTime, startDateTime, urDeliver, rpp,
}) => ({
  accountId,
  branchId,
  delivery: urDeliver ? 'Y' : 'N',
  jobsiteId: jobsite.id,
  requisitionId,
  returnDateTime,
  rpp: rpp === 'Y',
  startDateTime,
  urWillPickup: pickup ? 'Y' : 'N',
  webRates: false,
  useCache: false,
  items: {},
});

/**
 * ----------------------------------------------------------------------------
 * Charge Estimate param
 * ----------------------------------------------------------------------------
 */
const autoChargeEstimateParam = {
  validator: isPopulatedObject,
  getter: ({ state }, { requisitionId }) => state.estimates.all[requisitionId],
  fetcher({ dispatch }, { equipment }) {
    return dispatch(types.estimates.fetch, {
      ...makeEstimateParams(equipment[0]),
      items: makeEstimateItems(equipment),
    }).then((result) => {
      if (result || result.totals) {
        return result;
      }
      else {
        console.warn('Estimate or estimate totals is missing.');
      }
    });
  },
};

export const chargeEstimate = buildParams(autoChargeEstimateParam);

export default {};
