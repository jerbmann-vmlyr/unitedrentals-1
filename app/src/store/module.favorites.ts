import { ActionTree, GetterTree, MutationTree } from 'vuex';
import { FlightToken } from '@/api/flight-track';
import api from '@/api';
import types from '@/store/types';

export interface FavoritesState {
  all: string[];
}

const state: FavoritesState = {
  all: [],
};

const actions: ActionTree<FavoritesState, any> = {

  // Fetch the list of favorite cat. classes then that equipment
  async [types.favorites.fetchAll]({ dispatch, state, commit }) {
    await dispatch(types.favorites.fetchList);
    if (!state.all.length) {
      return;
    }
    else {
      const { data } = await api.catalog.fetchEquipmentByCatClass(state.all);
      commit(types.catalog.fetchEquipment, data);
    }
  },

  // Fetch the list of favorites
  async [types.favorites.fetchList]({ commit }) {
    const favorites = await api.favorites.fetch();
    commit(types.favorites.fetchList, favorites);
    return favorites;
  },

  // Add a cat. class to the favorites list
  async [types.favorites.add]({ commit, state }, catClass) {
    const favorites = await api.favorites.add(catClass);
    commit(types.favorites.fetchList, favorites);
    return favorites;
  },

  // Remove a cat. class from the favorites list
  async [types.favorites.remove]({ commit }, catClass: string) {
    const favorites = await api.favorites.remove(catClass);
    commit(types.favorites.fetchList, favorites);
    return favorites;
  },
};

const getters: GetterTree<FavoritesState, any> = {
  [types.favorites.favoriteRequestsPending](state, getters): boolean {
    return getters[types.inFlightRequests.tokensAreInFlight](
      FlightToken.addFavorite,
      FlightToken.fetchFavorites,
      FlightToken.removeFavorite,
    );
  },

  favoriteEquipment: (state, getters, rootState) => state.all.map(catClass => rootState.catalog.equipment[catClass] || {}),
};

const mutations: MutationTree<FavoritesState> = {
  [types.favorites.fetchList](state, catClasses) {
    state.all = catClasses;
  },
};

export default {
  state,
  actions,
  getters,
  mutations,
};
