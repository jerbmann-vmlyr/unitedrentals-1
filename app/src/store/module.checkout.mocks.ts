import types from '@/store/types';
import CheckoutModule from './module.checkout';

export default {
  state: {
    billing: {},
    estimates: {},
    startUnauthenticatedCheckout: false,
    hasShownAuthenticateToContinueModal: false,
    transactionResults: {},
    transactionBlocks: {},
    promotionCode: '',
    progressOfAllReservationBlocks: [],
    isSubmitting: false,
    isReservation: false,
    hideOverlayImmediately: false,
  },
  getters: CheckoutModule.getters,
  actions: {
    [types.checkout.fetchEstimates]: jest.fn(),
  },
};

/*
See module.cart.mocks.ts for the "item1" which will be
paired with this estimate in testing.
*/

const estimateStandardItem1 = {
  branchId: '639',
  startDate: '2019-01-24T00:00:00',
  returnDate: '2019-01-25T00:00:00',
  delivery: false,
  pickup: false,
  rpp: false,
  address: '',
  city: 'Kansas City',
  state: 'MO',
  postalCode: '64118',
  webRates: true,
  omitNonDspRates: true,
  costOverride: false,
  items: {
    '310-9801': {
      quantity: 1,
      minRate: 1138,
      dayRate: 1138,
      weekRate: 3638,
      monthRate: 7756,
      total: 1138,
      individualTotal: 1138,
      availableEquip: true,
    },
  },
  totals: {
    totalRentalAmount: 1138,
    environmentFee: 20.93,
    tax: 86.63,
    damageWaiverFee: 0,
    deliveryFee: 0,
    pickupFee: 0,
    miscellaneousFees: 0,
    authAmount: 1556.95,
    totalsOverwritten: false,
  },
};

const estimateZeroDollarItem1 = { ...estimateStandardItem1 };
estimateZeroDollarItem1.items['310-9801'] = {
  ...estimateZeroDollarItem1.items['310-9801'],
  ...{
    minRate: 0,
    dayRate: 0,
    weekRate: 0,
    monthRate: 0,
    total: 0,
    individualTotal: 0,
  },
};

const estimateUnavailableItem1 = { ...estimateStandardItem1 };
estimateUnavailableItem1.items['310-9801'] = {
  ...estimateUnavailableItem1.items['310-9801'],
  ...{
    availableEquip: false,
  },
};

export { estimateStandardItem1, estimateZeroDollarItem1, estimateUnavailableItem1 };
