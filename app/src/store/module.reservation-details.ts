import Vue from 'vue';
import { ActionTree, MutationTree, GetterTree } from 'vuex';
import api from '@/api';
import types from '@/store/types';
import { Requisition } from '@/lib/transactions';

export interface ReservationDetailsState {
  all: {
    [reqId: string]: Requisition;
  };
  form: {
    contact: string;
    mobilePhone: string;
    email: string;
  };
  fetching: {
    [fieldName: string]: boolean;
  };
}


const state: ReservationDetailsState = {

  /**
   * Depends on reservationId existence the api either transaction or requisition API will be called
   * keyed by reservationId, requestId, accountId and transType.
   */
  all: {},

  /**
   * @type {object} form fields for a (current, edited) jobsite info for a specific requisition ID.
   * Note, this architecture requires users to edit one requisition at a time.
   */
  form: {
    contact: '', // required
    mobilePhone: '', // required
    email: '',
  },

  fetching: {
    // Example: [getFetchingKey(/*...*/)]: Boolean
  },
};

const actions: ActionTree<ReservationDetailsState, any> = {
  async [types.readReservationDetails.fetch]({ commit }, {
    accountId, requisitionId, reservationId,
  }) {
    const fetchingKey = getFetchingKey({ accountId, requisitionId, reservationId });
    commit(types.readReservationDetails.loading, { [fetchingKey]: true });

    const response = await api.readReservationDetails.fetch(accountId, requisitionId, reservationId);
    commit(types.readReservationDetails.loading, { [fetchingKey]: false });
    commit(types.readReservationDetails.fetch, { id: requisitionId, data: response.data[requisitionId] });
    return response;
  },
  // Updates a form field. @param {object<name:val>} field
  [types.readReservationDetails.updateFormField]({ commit }, field) {
    commit(types.readReservationDetails.updateFormField, field);
  },

  // Sends data in state.form to PATCH /api/v1/transaction-update
  async [types.readReservationDetails.saveFormDateTime]({ commit }, params) {
    const response = await api.readReservationDetails.saveRequisitionTransactionDateTimeChange(params);
    if (!!response) {
      commit(types.readReservationDetails.saveFormDateTime, params);
    }
  },

  [types.readReservationDetails.updateAccountId]({ commit }, params) {
    commit(types.readReservationDetails.updateAccountId, params);
  },

  [types.readReservationDetails.updateReservationFields]({ commit }, params) {
    commit(types.readReservationDetails.updateReservationFields, params);
  },

  // Sends data in state.form to PATCH /api/v1/transaction-update
  async [types.readReservationDetails.saveFormQuantity]({ commit }, params) {
    const call = await api.readReservationDetails.saveRequisitionQuantityChange(params);
    commit(types.readReservationDetails.saveFormQuantity, params);
    return call;
  },

  // Sends either updated jobId or needs to update the job contact info.
  async [types.readReservationDetails.updateJobsiteInfoOrTransactionJobId]({ commit }, params) {
    const call = await api.readReservationDetails.updateJobsiteInfoOrTransactionJobId(params);
    commit(types.readReservationDetails.updateFormField, params);
    return call;
  },
};

const mutations: MutationTree<ReservationDetailsState> = {
  [types.readReservationDetails.fetch](state, input) {
    Vue.set(state.all, input.id, input.data);
  },
  [types.readReservationDetails.loading](state, field) {
    const fieldName = Object.keys(field)[0];
    Vue.set(state.fetching, fieldName, field[fieldName]);
  },
  [types.readReservationDetails.updateFormField](state, fields) {
    Object.keys(fields).forEach(ea => {
      Vue.set(state.form, ea, fields[ea]);
    });
  },
  [types.readReservationDetails.updateAccountId](state, params) {
    const { reservationId, accountId } = params;
    Vue.set(state.all[reservationId], 'accountId', accountId);
  },
  [types.readReservationDetails.updateReservationFields](state, params) {
    const { reservationId, fields } = params;
    Object.keys(fields).map(key => (
      Vue.set(state.all[reservationId], key, fields[key])
    ));
  },
  [types.readReservationDetails.saveFormDateTime](state, {
    transId, requestId, startDate, endDate, returnDate, po, id, delivery, pickup,
  }) {
    state.all[requestId] = {
      ...state.all[requestId],
      requestId,
      transId,
      startDate,
      endDate,
      returnDate,
      delivery,
      pickup,
      po,
      id,
    };
  },
  [types.readReservationDetails.saveFormQuantity](state, params) {
    const catList = params.catClassList.split(',');
    const qtyList = params.qtyList.split(',');

    Object.keys(state.all[params.requestId].items).forEach(ea => {
      if (catList.indexOf(ea) > -1) {
        const item = { ...state.all[params.requestId].items[ea] };
        item.quantity = qtyList[catList.indexOf(ea)];
        state.all[params.requestId].items[ea] = item;
      }
    });
  },
  ['readReservationDetails/reset'](state) {
    state.all = {};
  },
};

const getters: GetterTree<ReservationDetailsState, any> = {
  [types.readReservationDetails.currentReservation](state, getters, rootState) {
    const { route } = rootState;
    if ((route.name !== 'ConvertQuote') || (route.name === 'EditQuote')) {
      return Object.values(state.all)[0];
    }
    return false;

    const requisitions = Object.values(state.all);
    const current =  requisitions.find(item => item.quoteId === route.params.quoteId);

    return current;
  },
};

export default {
  actions,
  mutations,
  state,
  getters,
};

export const getFetchingKey = ({ accountId, requisitionId, reservationId }) => `${accountId}${requisitionId}${reservationId}`;
