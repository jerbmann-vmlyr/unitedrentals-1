import i18n from '@/data/i18n';
export const $i18n = key => i18n[key];
