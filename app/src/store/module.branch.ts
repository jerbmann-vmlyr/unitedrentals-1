import Vue from 'vue';
import { ActionTree, MutationTree } from 'vuex';
import api from '@/api';
import types from '@/store/types';
import { ApiV2Utils } from '@/lib/apiv2';
import { Branch } from '@/lib/branches';
const module = types.branch;

export interface BranchState {
  /**
   * Branch data returned from a branch api call, keyed by branch ID.
   * This state gets persisted in the browsers Session Storage.
   *
   * Example: state.all['1234'] is {id: '1234', address1: '123 Main St.', city: 'Kansas City', ...}
   */
  show: {},
  all: { [branchId: string]: Branch };
}

/* State */
const state: BranchState = {
  all: {},
  show: {},
};

/* Actions */
const actions: ActionTree<BranchState, any> = {

  /* Single Branch By Id */
  async [module.show]({ commit, state }, branchId) {
    return api.branches.getBranchData(branchId).then(({ data }) => commit(module.show, data.data));
  },

  /* Fetch Branches */
  async [module.fetch]({ commit }) {
    const branches = await api.branches.getAll().then(ApiV2Utils.unwrapResponseData);
    commit(module.fetch, branches);
    return branches;
  },

  /* Fetch Branch By Id */
  async [module.fetchById]({ commit, state }, branchId): Promise<Branch> {
    // Try for cached copy first
    if (!branchId){
      throw new Error(`No branchID selected`);
    }
    if (Object.keys(state.all).includes(branchId)) return state.all[branchId];

    const branch = await api.branches.getBranchData(branchId).then<Branch>(ApiV2Utils.unwrapResponseData);
    commit(module.fetchById, branch);
    return branch;
  },
};

/* Mutations */
const mutations: MutationTree<BranchState> = {
  [module.show](state, branch) {
    state.show = branch;
  },

  [module.fetch](state, branchData) {
    state.all = branchData.reduce((map, obj) => {
      map[obj.branchId] = obj;
      return map;
    });
  },

  [module.fetchById](state, branchData) {
    Vue.set(state.all, branchData.branchId, branchData);
  },
};

export default {
  actions,
  mutations,
  state,
};
