/* eslint-disable valid-jsdoc */
import Vue from 'vue';
import { every, get, isEmpty, isNil, keyBy, some, uniq, values } from 'lodash';

import api from '@/api';
import types from '@/store/types';

const state = {

  /**
   * An array of cat. classes for equipment currently loaded into a view.
   * E.g., on the /equipment pages, this would be all equipment from a category or subcategory.
   * E.g., on the /favorites page, this would be all equipment from a favorites list.
   * @type {string[]} an array of cat. classes
   */
  currentEquipment: [],

  /**
   * A Collection of Rental Equipment Data from an /api call. Keyed by catClass.
   */
  equipment: {},

  /**
   * Flag when rates are being loaded
   */
  fetchingRates: false,

  /**
   * A Collection of Rates from an /api call keyed by catClass then ratesPlaceBranch
   */
  rates: {},

  loading: false,
  loaded: false,
};

const getters = {

  /**
   * Maps state.currentEquipment to state.equipment.
   * @param {object} state | vuex
   * @return {object[]} an array of equipment objects
   */
  currentEquipment: state => state.currentEquipment.map(catClass => state.equipment[catClass]),


  /**
   * Returns the rates for given branch as a flat map.
   *
   * From this:
   *    $store.state.catalog.rates == {
   *      [catClass]: {
   *        [branchId]: {
   *          dayRate: x,
   *          ...
   *        }
   *      },
   *    };
   *
   *
   * To this:
   *    $store.getters.branchRates(branchId) == {
   *      [catClass]: {
   *        dayRate: x,
   *        ...
   *      },
   *    };
   */
  branchRates: state => (branchId) => {
    const rates = Object.values(state.rates).map(x => x[branchId]);
    return keyBy(rates, 'catClass');
  },

  /**
   * Returns the rates for users current branch as a flat map.
   * See the branchRates getter for more info.
   */
  currentBranchRates: (getters, rootState) => {
    const branchId = get(rootState, ['user', 'ratesPlaceBranch']);
    if (!isNil(branchId)) {
      return getters.branchRates(branchId);
    }
  },

  /**
   * FilteredEquipment is the subset of equipment matching a list of cat. classes produced by
   * a user's selected filters.
   *
   * There are two types of filters at play here, each with its own logic.
   * (Contextual Filters and Equipment (attribute) Filters.)
   *
   * Equipment Attribute Filters filter equipment using OR logic. This means if 2 filters are active, a piece of equipment need
   * only be in one OR the other set to be valid.
   *
   * Contextual Filters filter equipment via AND logic. This means if 2 filters are active, a piece of equipment
   * must belong to EVERY set to be valid.
   *
   * Also, any item with rates.locationFiltered gets removed
   *
   * Logic behind rates.locationFiltered has changed
   * locationFiltered = true => catClass is available in the branch
   * locationFiltered = false => catClass is not available in the branch
   *
   * @param {object} state | vuex
   * @param {object} getters | vuex
   * @param {object} rootState | vuex
   * @return {object[]} filtered equipment list
   */
  filteredEquipment(state, getters, rootState) {
    // @type {object} contextualFilters | a dictionary of contextual-filter data keyed by filter id
    // @type {object} equipmentFilters | a dictionary of equipment-filter data keyed by filter id
    // @type {string[]} selectedFilters | a collection of *any* active filters' IDs (be it an equipment- or contextual-filter)
    const { contextualFilters, equipmentFilters, selectedFilters } = rootState.facets;

    const allEquipment = getters.currentEquipment;

    // 0. If no filters are active, return all equipment
    if (!selectedFilters.length) {
      return allEquipment;
    }

    // 1. Get a flat listing of all cat. classes of all active equipment-filters
    const catClassesOfEquipmentFilters = values(equipmentFilters)
      .filter(({ id }) => selectedFilters.includes(id))
      .reduce((catClasses, filter) => catClasses.concat(filter.catClasses), []);

    // 2. Get a listing of all equipment filtered by those cat. classes
    const equipmentFilteredByEquipmentFilters = allEquipment
      .filter(({ catClass }) => catClassesOfEquipmentFilters.includes(catClass));

    // 3. Get a list of selected contextual filters. (The objects, not just their ids.)
    const selectedContextualFilterObjects = values(contextualFilters)
      .filter(({ id }) => selectedFilters.includes(id));

    // 4. If there are no active contextual filters, we're done, and return the subset of all equipment
    //    filtered by the selected equipment-filters.
    if (isEmpty(selectedContextualFilterObjects)) {
      return equipmentFilteredByEquipmentFilters;
    }

    // However, if we reach this point, we now want to return the subset of
    // equipment that contains all terms in the contextual filters

    // 5. To begin, first we'll store a list of each contextual filters' cat. class list. (A list of lists, so to speak.)
    const catClassesOfContextualFilters = selectedContextualFilterObjects
      .map(obj => obj.catClasses);

    // 6. Get a list of equipment (cat. classes) where each equipment item contains every contextual filters' term
    const validCatClasses = selectedContextualFilterObjects
      .reduce((validCatClasses, filter) => {
        const intersectingCatClasses = filter.catClasses
          .filter(catClass => every(catClassesOfContextualFilters, catClasses => catClasses.includes(catClass)));

        return validCatClasses.concat(intersectingCatClasses);
      }, []);

    // 7. Filter then return the remaining equipment
    const equipment = isEmpty(equipmentFilteredByEquipmentFilters) ? allEquipment : equipmentFilteredByEquipmentFilters;

    return equipment.filter(({ catClass }) => validCatClasses.includes(catClass));
  },
};

const actions = {
  [types.catalog.fetchCategoricalEquipment]({ commit }, { termId, branchId, offset }) {
    commit('equipment/loading', true);
    return api.catalog.fetchEquipmentByTermId({ termId, branchId, offset })
      .then(({ data }) => {
        commit('equipment/loading', false);
        commit('equipment/loaded', true);
        commit(types.catalog.fetchEquipment, data);

        // TODO Brewer - remove `uniq` once api is fixed (/* is removed)
        commit(types.catalog.currentEquipment, uniq(data.map(equipment => equipment.catClass)));
      });
  },

  /**
   * Fetch Equipment By Cat Class action
   *
   * Loads equipment data given by the array of cat classes
   *
   * @param {object} commit | commit mapping
   * @param {array} catClasses | array of cat classes
   * @return {object} equipment data
   */
  [types.catalog.fetchEquipmentByCatClass]({ commit }, catClasses) {
    commit('equipment/loading', true);
    return api.catalog.fetchEquipmentByCatClass(catClasses)
      .then(({ data }) => {
        commit('equipment/loading', false);
        commit('equipment/loaded', true);
        commit(types.catalog.fetchEquipment, data);
        return data;
      });
  },

  [types.catalog.fetchRates]({ commit, rootState }, {
    catClass, catClasses, placeId, branchId, address, accountId, deliveryDistance
  }) {
    if (!some([placeId, branchId, address]) || !some([catClass, catClasses])) {
      const error = new Error('The catalog.fetchRates action was invoked w/o the required params. Continuing anyway.');
      return Promise.reject(error).catch((error) => {
        console.warn(error.message); // eslint-disable-line
      });
    }

    // If deliveryDistance is not explicitly passed, try to find it in the store instead
    if (typeof deliveryDistance === 'undefined' && typeof rootState.branch.all[branchId].distance !== undefined) {
      deliveryDistance = rootState.branch.all[branchId].distance;
    }

    commit(types.catalog.fetchingRates, true);

    return api.catalog.fetchRates({
      catClass, catClasses, address, placeId, branchId, accountId, deliveryDistance
    })
      .then(({ data }) => {
        commit(types.catalog.fetchingRates, false);
        if (!Array.isArray(data)) {
          // eslint-disable-next-line
          console.error(`Error in catalog.fetchRates action: ${data}`);
        }
        commit(types.catalog.fetchRates, data);
        return data;
      });
  },
};

const mutations = {
  [types.catalog.currentEquipment](state, catClasses) {
    state.currentEquipment = catClasses;
  },
  [types.catalog.fetchEquipment](state, data) {
    state.equipment = {
      ...state.equipment,
      ...keyBy(data, 'catClass'),
    };
  },

  /**
   * Rates are stored by catclass then by branchId.
   * E.g. JSON.stringify(state.rates[catClass][branchId]); // {"rateType": "House, "dayRate": "434", ... }
   *
   * @param {object} state vuex state
   * @param {array} data | server response
   * @return {void}
   */
  [types.catalog.fetchRates](state, data) {
    if (!Array.isArray(data)) {
      console.warn('Unable to store rates. (Invalid rates data)'); // eslint-disable-line
      return;
    }
    data.forEach((rate) => {
      const { branchId, catClass } = rate;
      if (typeof state.rates[catClass] === 'undefined') {
        Vue.set(state.rates, catClass, {});
      }

      Vue.set(state.rates[catClass], branchId, rate);
    });
  },

  [types.catalog.fetchingRates](state, bool) {
    state.fetchingRates = bool;
  },

  'equipment/loading': function (state, bool) {
    state.loading = bool;
  },
  'equipment/loaded': function (state, bool) {
    state.loaded = bool;
  },
};

export default {
  actions,
  getters,
  mutations,
  state,
};
