import _ from 'lodash';
import types from '@/store/types';

export const isPromise = val => val && _.isFunction(val.then);
export const isPopulatedObject = value => _.isObject(value) && !_.isEmpty(value);
export const isNotEmpty = value => !_.isEmpty(value);

export function makeAsyncGet({
  getter,
  fetcher,
  validator = isNotEmpty,
  debounceRate = 500,
} = {}) {
  const _fetcher = _.debounce(fetcher, debounceRate);
  return (vm, payloadObj) => new Promise((resolve, reject) => {
    const store = vm.$store;
    const payload = _.isFunction(payloadObj) ? payloadObj.call(vm, vm) : payloadObj;
    // Check if we can already get the value, and it's valid;
    const value = getter(store, payload);
    if (validator(value)) {
      return resolve(value);
    }

    // Fetch the value from API and rerun the getter for result.
    let fetched = _fetcher(store, payload);
    if (!isPromise(fetched)) {
      fetched = Promise.resolve(fetched);
    }
    return fetched
      .then(() => resolve(getter(store, payload)))
      .catch(error => reject(new Error(error)));
  });
}

export const buildParams = paramsObj => payloadObj => {
  const asyncGetter = makeAsyncGet(paramsObj);
  return {
    lazy: true,
    get() {
      return asyncGetter(this, payloadObj);
    },
    default: _.stubObject,
  };
};


/**
 * ----------------------------------------------------------------------------
 * Equipment param
 *
 * Given Catclass, will resolve to the equipment info for item.
 * ----------------------------------------------------------------------------
 */
const autoEquipmentParam = {
  validator: isPopulatedObject,
  getter({ state }, { catClass }) {
    return _.get(state.catalog.equipment, catClass, {});
  },
  fetcher({ dispatch }, { catClass }) {
    return dispatch(types.catalog.fetchEquipmentByCatClass, [catClass]);
  },
};

export const equipmentAsyncComputed = buildParams(autoEquipmentParam);


/**
 * ----------------------------------------------------------------------------
 * Item Rates param
 *
 * Given Catclass/branchId/accountId, will resolve to the rates for the item.
 * ----------------------------------------------------------------------------
 */
const autoItemRatesParam = {
  validator: isPopulatedObject,
  getter({ getters }, { branchId, catClass }) {
    return _.get(getters.branchRates(branchId), catClass);
  },
  fetcher({ dispatch }, { branchId, catClass, accountId, deliveryDistance }) {
    return dispatch(types.catalog.fetchRates, { catClass, branchId, accountId, deliveryDistance });
  },
};

export const itemRatesAsyncComputed = buildParams(autoItemRatesParam);


/**
 * ----------------------------------------------------------------------------
 * Equipment category params
 *
 * Given Catclass, will resolve to the Drupal-configured category/subcategory.
 * ----------------------------------------------------------------------------
 */
const autoEquipmentCategoryParam = {
  validator: _.isString,
  getter({ getters }, { catClass }) {
    return getters.categoryByCatClass(catClass);
  },
  fetcher({ dispatch }) {
    return dispatch(types.facets.fetch);
  },
};
export const equipmentCategoryAsyncComputed = buildParams(autoEquipmentCategoryParam);


// Same thing basically
export const autoEquipmentSubCategoryParam = {
  ...autoEquipmentCategoryParam,
  getter({ getters }, { catClass }) {
    return getters.subCategoryByCatClass(catClass);
  },
};
export const equipmentSubCategoryComputed = buildParams(autoEquipmentSubCategoryParam);


/**
 * ----------------------------------------------------------------------------
 * Rates place branch param
 *
 * Given google place, will set/resolve the branchId
 *
 * Note: there's a lot of logic that goes on here, check the app.vue
 * for GooglePlace logic.
 * ----------------------------------------------------------------------------
 */
const autoRatesPlaceBranchParam = {
  validator: _.isString,
  getter: ({ state }) => state.user.ratesPlaceBranch,
  fetcher({ dispatch }, { ratesPlace }) {
    return dispatch(types.user.ratesPlaceBranch, ratesPlace);
  },
};
export const ratesPlaceBranchAsyncComputed = buildParams(autoRatesPlaceBranchParam);


/**
 * ----------------------------------------------------------------------------
 * Branch Param
 *
 * Given a branchId, will resolve to the branch details.
 * ----------------------------------------------------------------------------
 */
const autoBranchParam = {
  validator: isPopulatedObject,
  getter({ state }, { branchId }) {
    return state.branch.all[branchId] || {};
  },
  fetcher({ dispatch }, { branchId }) {
    return dispatch(types.branch.fetchById, branchId);
  },
};

export const branchAsyncComputed = buildParams(autoBranchParam);

export default {
  equipmentAsyncComputed,
  itemRatesAsyncComputed,
  equipmentCategoryAsyncComputed,
  equipmentSubCategoryComputed,
  ratesPlaceBranchAsyncComputed,
  branchAsyncComputed,
};
