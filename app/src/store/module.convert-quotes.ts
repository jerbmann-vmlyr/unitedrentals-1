import api from '@/api';
import types from '@/store/types';

const state = {
  billing: {
    cardId: null,
  },
  jobsiteId: 0,
};

const actions = {
  [types.convertQuotes.billingCreditCard]({ commit }, card) {
    commit(types.convertQuotes.billingCreditCard, card);
  },
  async [types.convertQuotes.sendQuote]({ commit }, quoteItem) {
    const res = await api.quotes.convertQuoteToCart(quoteItem);
    return res;
  },
  [types.convertQuotes.setConvertSuccess]({ commit }, formState) {
    commit(types.convertQuotes.setConvertSuccess, formState);
  },
  [types.convertQuotes.setNewJobsiteId]({ commit }, input) {
    commit(types.convertQuotes.setNewJobsiteId, input);
  },
};

const mutations = {
  [types.convertQuotes.billingCreditCard](state, card) {
      state.billing.cardId = card.cardId;
  },
  [types.convertQuotes.setNewJobsiteId](state, input) {
    state.jobsiteId = input;
  },
};

export default {
  actions,
  mutations,
  state,
};
