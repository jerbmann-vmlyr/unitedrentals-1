import api from '@/api';
import types from '@/store/types';
import Vue from 'vue';

const defaultForm = {
  email: '',
  firstName: '',
  lastName: '',
  password: '',
  phone: '',
};

const state = {
  altPhone: '',
  firstName: '',
  lastName: '',
  guid: '',
  email: '',
  phone: '',
  pswdExpDays: 0,
  form: { ...defaultForm },
};

const getters = {
  activeUserDetails(state) {
    return state;
  },
};

const actions = {
  async [types.tcUser.create]({ state, commit }) {
    return api.tcUser.create(state.form).then(({ data }) => {
      const { guid } = data;
      commit(types.tcUser.updateDetails, { guid });
    });
  },

  async [types.tcUser.fetch]({ commit }) {
    const response = await api.tcUser.fetch();
    if (typeof response.data === 'string') {
      console.warn('The TC user API returned invalid data!'); // eslint-disable-line
      return;
    }
    commit(types.tcUser.fetch, response.data);
  },

  async [types.tcUser.fetchOne](context, email) {
    return api.tcUser.fetchOne(email);
  },

  [types.tcUser.updateDetails]({ commit }, userDetails) {
    return api.tcUser.save(userDetails)
      .then(() => {
        commit(types.tcUser.updateDetails, userDetails);
      });
  },

  [types.tcUser.updateFormField]({ commit }, field) {
    commit(types.tcUser.updateFormField, field);
  },
};

const mutations = {
  [types.tcUser.fetch](state, data) {
    const {
      altPhone, firstName, lastName, email, guid, phone, pswdExpDays,
    } = data;
    state.altPhone = altPhone;
    state.firstName = firstName;
    state.lastName = lastName;
    state.email = email;
    state.guid = guid;
    state.pswdExpDays = pswdExpDays;
    state.phone = phone;
  },

  /**
   * Update tcUser mutator
   *
   * Updates the state's tcUser values to those in the passed object
   *
   * @param {object} state | vuex
   * @param {object} tcUser | tcUser to set to the state's tcUser
   */
  [types.tcUser.updateDetails](state, tcUser) {
    Object.entries(tcUser).forEach(([key, value]) => {
      if (value !== null) {
        state[key] = value;
      }
    });
  },

  [types.tcUser.reset](state) {
    state.altPhone = '';
    state.firstName = '';
    state.lastName = '';
    state.guid = '';
    state.email = '';
    state.phone = '';
    state.pswdExpDays = 0;
    state.form = { ...defaultForm };
  },

  [types.tcUser.updateFormField](state, field) {
    Object.entries(field).forEach(([key, value]) => {
      if (!state.form.hasOwnProperty(key)) {
        throw new Error(`Cannot assign property "${key}" to state.tcUser.form.`);
      }
      Vue.set(state.form, key, value);
    });
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
