import api from '@/api';
import types from '@/store/types';

const state = {
  all: [],
};

const getters = {
  recentlyViewedEquipment: (state, getters, rootState) => state.all.map(catClass => rootState.catalog.equipment[catClass] || {}),
};

const actions = {
  // Fetch the list of cat classes of recently viewed equipment
  [types.recentlyViewed.fetch]({ commit }) {
    return api.recentlyViewed.fetch()
      .then(({ data }) => {
        commit(types.recentlyViewed.fetch, data);
      });
  },

  // Fetch the list of favorite cat. classes then that equipment
  [types.recentlyViewed.fetchAll]({ dispatch, state }) {
    return new Promise((resolve) => {
      dispatch(types.recentlyViewed.fetch)
        .then(() => {
          if (!state.all.length) {
            return resolve();
          }
          dispatch(types.recentlyViewed.fetchEquipment)
            .then(resolve);
        });
    });
  },

  // Fetch the equipment on the favorites list
  [types.recentlyViewed.fetchEquipment]({ commit, state }) {
    return api.catalog.fetchEquipmentByCatClass(state.all)
      .then(({ data }) => {
        commit(types.catalog.fetchEquipment, data);
      });
  },
};

const mutations = {
  [types.recentlyViewed.fetch](state, data) {
    state.all = data.recently_viewed;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
