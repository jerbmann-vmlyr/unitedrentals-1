import { types } from '@/store';

const state = {
  options: [
    {
      returnType: 'userReturn',
      text: 'checkout.userReturn',
      pickupType: 'N',
      currency: '0',
      pickup: false,
      pickupFirm: false,
      selected: false,
    },
    {
      returnType: 'pickupLater',
      text: 'checkout.urPickupLater',
      pickupType: 'Y',
      pickup: true,
      pickupFirm: false,
      selected: false,
    },
    {
      returnType: 'schedulePickup',
      text: 'checkout.prescheduledPickup',
      pickupType: 'A',
      pickup: true,
      pickupFirm: true,
      selected: false,
    },
  ],
};

const actions = {};

const mutations = {
  [types.pickupDelivery.select](state, item) {
    const [updating] = state.options.filter(({ returnType }) => item.returnType === returnType)
      || state.options.filter(({ returnType }) => returnType === 'userReturn');

    state.options = state.options.reduce((options, option) =>
      option.returnType === updating.returnType
        ? [...options, { ...option, selected: true }]
        : [...options, { ...option, selected: false }],
    []);
  },
};
const getters = {
  [types.pickupDelivery.selected](state) {
    return state.options.find(({ selected }) => selected) || state.options.find(({ returnType }) => returnType === 'userReturn');
  },
  [types.pickupDelivery.options](state) {
    return state.options;
  },
};

export { state, getters, actions, mutations };
export default { state, getters, actions, mutations };
