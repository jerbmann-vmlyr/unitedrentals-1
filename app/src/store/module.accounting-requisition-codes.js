// Note: This is not used in checkout. It is only used in platform accounts.
// Checkout uses module.allocation-codes.js

import api from '@/api';
import types from '@/store/types';
import Vue from 'vue';

const state = {

  /**
   * Depends on accountID and transID passing to the request,the
   * transactionCostAllocation and allocationrequisitioncodetitles will be called
   * keyed by accountId, transId and transactioncostallocations.allocationId =
   * allocationrequisitioncodetitles.id .
   */
  all: {},

  fetching: false,
};

const actions = {
  [types.accountingRequisitionCodes.fetch]({ commit }, { accountId, transId }) {
    if (!accountId) {
      console.warn('Action Error: types.accountingRequisitionCodes.fetch dispatched without an accountId and transId.'); // eslint-disable-line
      return;
    }
    commit(types.accountingRequisitionCodes.loading, true);

    return api.accountingRequisitionCodes.fetch(accountId, transId)
      .then(({ data }) => {
        commit(types.readReservationDetails.loading, false);

        if (data === null) {
          return Promise.reject(new Error(`No data returned for ${transId}. (Action: types.accountingRequisitionCodes.fetch)`));
        }

        commit(types.accountingRequisitionCodes.fetch, { data, transId });
      });
  },
};

const mutations = {
  [types.accountingRequisitionCodes.fetch](state, {
    transId, data,
  }) {
    Vue.set(state.all, transId, data);
    // Vue.set(state.all[accountId], requisitionId, reservationId, data);
  },
  [types.accountingRequisitionCodes.loading](state, bool) {
    state.fetching = bool;
  },
  'accountingRequisitionCodes/reset': function (state) {
    state.all = {};
  },
};

const getters = {
  getAllocationCodes: (state, getters, rootState) => (requisition, transTypeId) => {
    if (!requisition) {
      return [];
    }

    const isReservation = transTypeId === 'R' && (requisition.reservationId || requisition.requisitionId);
    const isQuote = transTypeId === 'Q' && requisition.quoteId;

    if (!isReservation && !isQuote) {
      return [];
    }

    const reservationAllocationCodeAccessor = requisition.reservationId || requisition.requisitionId;
    const reservationAllocationCodes = rootState.accountingRequisitionCodes[reservationAllocationCodeAccessor];
    const quoteAllocationCodes = rootState.accountingRequisitionCodes.all[requisition.quoteId];

    if (!reservationAllocationCodes && !quoteAllocationCodes) {
      return [];
    }

    return Object.values(reservationAllocationCodes || quoteAllocationCodes);
  },

  getRequisitionCodes: (state, getters) => (requisition, transTypeId) => { // eslint-disable-line
    return getters.getAllocationCodes(requisition, transTypeId).filter(({ codeType }) => codeType === 'R');
  },

  getAccountingCodes: (state, getters) => (requisition, transTypeId) => { // eslint-disable-line
    return getters.getAllocationCodes(requisition, transTypeId).filter(({ codeType }) => codeType === 'A');
  },
};

export default {
  actions,
  mutations,
  state,
  getters,
};
