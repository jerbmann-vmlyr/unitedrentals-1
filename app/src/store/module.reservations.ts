import api from '@/api';
import types from '@/store/types';
import { SearchByTermLexicon, SearchByTermFilter } from '@/lib/listings';

const state = {

  /**
   * Reservations returned from a api-appliance call, keyed by guid, and
   * accountId(not required).
   */
  all: {},

  filters: [] as SearchByTermFilter[],

  fetching: false,

  viewFilter: '',

  viewSort: '',
};

const actions = {
  [types.reservations.fetch]({ commit }) {
    commit(types.reservations.loading, true);

    return api.reservations.fetch()
      .then(({ data }) => {
        commit(types.reservations.loading, false);

        if (data === null) {
          return Promise.reject(new Error(`No reservations data returned. (Action: types.reservations.fetch)`));
        }

        // if the data object has an exception property, just return an empty object
        if ('exception' in data) {
          return {};
        }

        commit(types.reservations.fetch, { data });
        return data;
      });
  },
  [types.reservations.updateViewFilter]({ commit }, option) {
    commit(types.reservations.updateViewFilter, option);
  },
  [types.reservations.updateViewSort]({ commit }, option) {
    commit(types.reservations.updateViewSort, option);
  },
  [types.reservations.updateReservations]({ commit }, input) {
    commit(types.reservations.updateReservations, input);
  },
  /* listing filters */
  [types.reservations.addFilter]({ commit, state }, filter) {
    if (state.filters.some(f => f.term === filter.term)) {
      return;
    }
    commit(types.reservations.filters, [...state.filters, filter]);
  },
  [types.reservations.removeFilter]({ commit, state }, filter) {
    commit(types.reservations.filters, state.filters.filter(f => f.id !== filter.id));
  },
  [types.reservations.removeAllFilters]({ commit }) {
    commit(types.reservations.filters, []);
  },
};

const mutations = {
  [types.reservations.fetch](state, { data }) {
    state.all = data.reduce((dataAsObj, item) => {
      dataAsObj[item.requisitionId] = item;
      return dataAsObj;
    }, {});
  },
  [types.reservations.loading](state, bool) {
    state.fetching = bool;
  },
  [types.reservations.filters](state, filters: SearchByTermFilter[]) {
    state.filters = filters;
  },
  [types.reservations.updateReservations](state, input) {
    state.all[input.requisitionId] = input;
  },
  'reservations/reset'(state) {
    state.all = {};
  },
  [types.reservations.updateViewFilter](state, option) {
    state.viewFilter = option;
  },
  [types.reservations.updateViewSort](state, option) {
    state.viewSort = option;
  },
};

const getters = {
  /* reservations that belong to the currently selected account */
  reservationsForCurrentAccount(state, getters, rootState) {
    return Object.values(state.all).filter((reservation: any) => reservation.accountId === rootState.user.defaultAccountId);
  },
  /* reservations that belong to the currently selected account AND with filters applied */
  filteredReservations(state, getters) {
    return getters.reservationsForCurrentAccount
      .filter(quote =>
        getters.reservationsFilterFunctions.every(filter => filter(quote)),
      );
  },

  /* converts all the different filter types (search term, view, etc) into a list of functions */
  reservationsFilterFunctions(state) {
    return state.filters.map(filter => filter.asFunction);
  },

  reservationSearchTermFilters(state) {
    return state.filters.filter(filter => filter.type === 'SEARCH_BY_TERM');
  },

  reservationLexicon(): SearchByTermLexicon {
    return ({ accountId, requisitionId, reservationId, quoteId, jobName, orderStatus, jobId, contact, address, transactions }) => {
      return [
        accountId,
        requisitionId,
        reservationId,
        quoteId,
        jobName,
        orderStatus,
        jobId,
        contact,
        address,
        ...transactions.map(({ catClass }) => catClass),
        ...transactions.map(({ dalDescription }) => dalDescription),
      ].map(term => `${term}`.toLowerCase());
    };
  },

  [types.reservations.reservationsCount](state, getters) {
    return getters.filteredReservations.length;
  },

};

export default {
  actions,
  mutations,
  state,
  getters,
};
