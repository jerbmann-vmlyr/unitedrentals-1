import { ActionTree, MutationTree, GetterTree } from 'vuex';
import Vue from 'vue';
import { sortBy } from 'lodash';
import { FlightToken } from '@/api/flight-track';
import api from '@/api';
import types from '@/store/types';
import {
  Account,
  AccountColumnKey,
  AccountForm,
  AccountSearchCriteria,
  AccountSearchParams,
  AccountUtils,
  TcUserForm,
} from '@/lib/accounts';

export interface AccountsState {
  accountsFetched: boolean;
  all: { [accountId: string]: Account };
  form: AccountForm;
  latestSuccessfulSearchParams: Partial<AccountSearchParams>;
  searchCriteria: AccountSearchCriteria;
  searchResults: { [accountId: string]: Account };
  // An Account ID selected by a user via the `account-selector-modal.vue`
  // This differs from the module.user.defaultAccountId in that THIS id is one
  // INTENTIONALLY selected by the user, whereas the defaultAccountId gets set automatically
  // when app.vue mounts.
  selectedAccountId: string;
  sortColumnKey: AccountColumnKey;
  sortAscending: boolean;
  tcUserForm: TcUserForm;
}

const state: AccountsState = {
  accountsFetched: false,
  all: {},
  form: AccountUtils.createEmptyAccountForm(),
  latestSuccessfulSearchParams: {},
  searchCriteria: AccountUtils.createEmptyAccountSearchCriteria(),
  searchResults: {},
  selectedAccountId: '',
  sortColumnKey: AccountColumnKey.AccountName,
  sortAscending: true,
  tcUserForm: AccountUtils.createEmptyTcUserForm(),
};

const getters: GetterTree<AccountsState, any> = {
  [types.accounts.selectableAccounts](state, _, rootState): Account[] {
    const accounts = Object.values(rootState.user.isEmployee ? state.searchResults : state.all);
    const sortedBy = (() => {
      switch (state.sortColumnKey) {
        case AccountColumnKey.CityState: return ['city', 'state'];
        case AccountColumnKey.AccountId: return (acc: Account) => +acc.id;
        case AccountColumnKey.ParentAccountId: return (acc: Account) => +acc.parentId;
        default: return state.sortColumnKey;
      }
    })();
    const sortedAccounts = [...sortBy(accounts, sortedBy)];
    return state.sortAscending ? sortedAccounts : sortedAccounts.reverse();
  },

  [types.accounts.fetchRequestPending](state, getters): boolean {
    return getters[types.inFlightRequests.tokensAreInFlight](FlightToken.fetchAccount, FlightToken.searchAccounts);
  },

  [types.accounts.firstAccount](state) {
    const key = Object.keys(state.all)[0];
    return key ? state.all[key] : null;
  },

  [types.accounts.isValidAccountId]: state => (accountId: string) => {
    if (!state.accountsFetched) {
      throw new Error(`You have invoked types.accounts.isValidAccountId too early! AccountID ${accountId} cannot be validated yet. Invoke types.accounts.isValidAccountId after state.accounts.ready is TRUE.`); /* tslint-disable-line */
    }
    return Object.keys(state.all).includes(accountId);
  },

  [types.accounts.exist](state) {
    return Object.keys(state.all).length > 0;
  },

  [types.accounts.noAccounts](state, getters) {
    return !getters[types.accounts.exist];
  },

  [types.accounts.searchParams](state): Partial<AccountSearchParams> {
    return Object.entries(state.searchCriteria)
      .filter(([key, { enabled, value }]) => enabled && value)
      .reduce((accum, [key, { value }]) => ({ ...accum, [key]: value }), {});
  },

  [types.accounts.searchRequestPending](state, getters): boolean {
    return getters[types.inFlightRequests.tokensAreInFlight](FlightToken.searchAccounts);
  },
};

const actions: ActionTree<AccountsState, any> = {
  /**
   * Fetch the main bank of account state.
   * If the user is NOT an employee, then we can fetch the static list of
   * accounts. Otherwise, if the user is an employee, there is no static list,
   * so we need to get the single account specified by defaultAccountId using
   * a different API endpoint.
   *
   * If defaultAccountId is not set for an employee, do not make the API call.
   */
  async [types.accounts.fetch]({ commit, rootState }) {
    try {
      const data = await (!rootState.user.isEmployee
        ? api.accounts.fetch()
        : rootState.user.defaultAccountId
          ? api.accounts.search({ id: rootState.user.defaultAccountId })
          : Promise.resolve([]) // no-op
      );
      commit(types.accounts.saveAccounts, data);
    }
    catch (err) {
      console.error(err);
    }
    finally {
      commit(types.accounts.setAccountsFetched);
    }
  },

  [types.accounts.resetForm]({ commit }) {
    commit(types.accounts.resetForm);
  },

  [types.accounts.resetSearchResultsAndCriteria]({ commit, state }) {
    commit(types.accounts.saveSearchResults, []);
    const updatedCriteria: AccountSearchCriteria = Object.entries(state.searchCriteria)
      .reduce(
        (accum, [k, v]) => ({ ...accum, [k]: { ...v, enabled: false }}),
        {}  as AccountSearchCriteria,
      );

    commit(types.accounts.saveAccountSearchCriteria, updatedCriteria);
  },

  async [types.accounts.saveActiveSearch](
    { commit },
    { params, accounts }: { params: Partial<AccountSearchParams>, accounts: Account[] },
  ) {
    if (accounts.length > 0) {
      commit(types.accounts.saveSearchResults, accounts);
      commit(types.accounts.setLatestSuccessfulSearchParams, params);
    }
  },

  async [types.accounts.saveForm]({ commit, state }) {
    try {
      await api.accounts.save(state.form);
      commit(types.accounts.saveForm);
      commit(types.accounts.resetForm);
    }
    catch (err) {
      console.error(err);
    }
  },

  async [types.accounts.searchAccounts](
    { commit },
    params: Partial<AccountSearchParams>,
  ): Promise<Account[]> {
    const accounts = await api.accounts.search(params).catch(() => []);
    if (accounts.length > 0) commit(types.accounts.saveAccounts, accounts);
    return accounts;
  },

  [types.accounts.setSelectedAccountId]({ commit }, id) {
    commit(types.accounts.setSelectedAccountId, id);
  },

  [types.accounts.updateFormField]({ commit }, field) {
    commit(types.accounts.updateFormField, field);
  },

  [types.accounts.updateSearchCriteriaEnabled](
    { commit, state },
    { enabled, keys }: { enabled: boolean, keys: (keyof AccountSearchParams)[] },
  ) {
    const updatedCriteria: AccountSearchCriteria = Object.entries(state.searchCriteria)
      .reduce((accum, [k, v]) =>
        keys.includes(k as any) ? { ...accum, [k]: { ...v, enabled } } : accum,
        {} as AccountSearchCriteria,
      );

    commit(types.accounts.saveAccountSearchCriteria, updatedCriteria);
  },

  [types.accounts.updateSearchCriteriaValue](
    { commit, state },
    { key, value }: { key: keyof AccountSearchParams, value: any },
  ) {
    const updatedCriteria: AccountSearchCriteria = Object.entries(state.searchCriteria)
      .reduce((accum, [k, v]) =>
        k === key ? { ...accum, [k]: { ...v, value } } : accum,
        {} as AccountSearchCriteria,
      );

    commit(types.accounts.saveAccountSearchCriteria, updatedCriteria);
  },

  [types.accounts.updateTcUserFormField]({ commit }, field) {
    commit(types.accounts.updateTcUserFormField, field);
  },

};

const mutations: MutationTree<AccountsState> = {
  [types.accounts.resetForm](state) {
    Object.keys(state.form).forEach(key => {
      Vue.set(state.form, key, '');
    });
  },

  [types.accounts.saveAccounts](state, accounts: Account[]) {
    state.all = {
      ...state.all,
      ...accounts.reduce((accum, a) => ({ ...accum, [a.id]: a }), {}),
    };
  },

  [types.accounts.saveForm](state) {
    if (!state.all[state.form.accountId || state.form.id!]) {
      Vue.set(state.all, state.form.accountId, {});
    }
    Object.keys(state.form)
      .forEach(key => {
        const canonicalKey = key === 'accountId' ? 'id' : key;
        Vue.set(state.all[state.form.accountId], canonicalKey, state.form[key]);
      });
  },

  ['accounts/reset'](state) {
    state.all = {};
    state.form = AccountUtils.createEmptyAccountForm();
  },

  [types.accounts.saveAccountSearchCriteria](state, criteria: AccountSearchCriteria) {
    state.searchCriteria = {
      ...state.searchCriteria,
      ...criteria,
    };
  },

  [types.accounts.saveSearchResults](state, accounts: Account[]) {
    state.searchResults = accounts.reduce((accum, a) => ({ ...accum, [a.id]: a }), {});
  },

  [types.accounts.setAccountsFetched](state) {
    state.accountsFetched = true;
  },

  [types.accounts.setLatestSuccessfulSearchParams](state, params: Partial<AccountSearchParams>) {
    state.latestSuccessfulSearchParams = { ...params };
  },

  [types.accounts.setSelectedAccountId](state, id) {
    state.selectedAccountId = id;
  },

  [types.accounts.setSortAscending](state, asc: boolean) {
    state.sortAscending = asc;
  },

  [types.accounts.setSortColumnKey](state, columnKey: AccountColumnKey) {
    state.sortColumnKey = columnKey;
  },

  [types.accounts.toggleFavorite](state, accountId) {
    const account = state.all[accountId];
    const favorited = !account.isFavorited;
    Vue.set(state.all[accountId], 'isFavorited', favorited);
    if (accountId in state.searchResults) {
      Vue.set(state.searchResults[accountId], 'isFavorited', favorited);
    }
  },

  [types.accounts.updateFormField](state, field) {
    Object.keys(field).forEach(key => {
      Vue.set(state.form, key, field[key]);
    });
  },

  [types.accounts.updateTcUserFormField](state, field) {
    Object.keys(field).forEach(key => {
      Vue.set(state.tcUserForm, key, field[key]);
    });
  },
};

export default {
  actions,
  getters,
  mutations,
  state,
};
