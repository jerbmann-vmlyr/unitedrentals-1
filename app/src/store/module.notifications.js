import api from '@/api';

import types from './types';

const state = {
  count: 0,
};

const actions = {
  [types.notifications.fetch]({ commit }) {
    return api.notifications.fetch().then((response) => {
      commit(types.notifications.fetch, response);
    });
  },
};

const getters = {

};

const mutations = {
  [types.notifications.fetch](state, data) {
    state.count = data.count;
  },
};

export default {
  state,
  actions,
  getters,
  mutations,
};
