import {
  ActionTree, GetterTree,
  MutationTree,
} from 'vuex';
import api from '@/api';
import types from '@/store/types';

declare interface PopularEquipmentType {
  name: string,
  description: string,
  weight: number | string,
  image: string,
  url: string,
}

export interface PopularEquipmentTypesState {
  all: PopularEquipmentType[],
}

const state: PopularEquipmentTypesState = {
  all: [],
};

const actions: ActionTree<PopularEquipmentTypesState, any> = {
  async [types.popularEquipmentTypes.fetch]({ commit, state }) {
    return api.popularEquipmentTypes.fetchPopularEquipmentTypes()
      .then(data => {
        commit(types.popularEquipmentTypes.update, data);
      });
  },
};

const mutations: MutationTree<PopularEquipmentTypesState> = {
  [types.popularEquipmentTypes.update](state, data) {
    state.all = data;
  },
};

const getters: GetterTree<PopularEquipmentTypesState, any> = {
  [types.popularEquipmentTypes.fetch](state: PopularEquipmentTypesState) {
    return () => {
      return state.all || null;
    };
  },
};

export default {
  actions,
  getters,
  mutations,
  state,
};
