import types from '@/store/types';

export default {
  state: {},
  actions: {
    [types.branch.fetch]: jest.fn(),
  },
  mutations: {},
  getters: {},
};
