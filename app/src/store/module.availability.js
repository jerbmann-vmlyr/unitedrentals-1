import Vue from 'vue';

import api from '@/api';
import types from '@/store/types';

const state = {

  /**
   * Availability is returned from a api-appliance call, catClass, branchId
   * and detail=true is needed.
   *
   * This state gets persisted in the browsers Session Storage.
   *
   *
   */
  all: {},
};


// TODO: The following in not complete, and is to be left for Ryan Brewer.
const actions = {
  [types.availability.fetch]({ commit }, catClass, branchId) {
    if (!catClass && !branchId) {
      console.warn('Action Error: types.availability.fetch dispatched without a catClass or branchId.'); // eslint-disable-line
      return;
    }
    return api.availability.fetch(catClass, branchId)
      .then(({ data }) => {
        if (data === null) {
          const error = new Error(`No availability data returned for ${catClass} and ${branchId}. (Action: types.availability.fetch)`);
          return Promise.reject(error);
        }
        commit(types.availability.fetch, { data, catClass, branchId });
      });
  },
};

const mutations = {
  [types.availability.fetch](state, { data, catClass }) {
    if (!state.all[catClass]) {
      Vue.set(state.all, catClass, {});
    }
    data.forEach((availability) => {
      if (!availability.guid) {
        return;
      }

      Vue.set(state.all[catClass], availability.guid, availability);
    });
  },
};

export default {
  actions,
  mutations,
  state,
};
