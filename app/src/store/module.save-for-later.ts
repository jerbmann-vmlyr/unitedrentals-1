
import { forOwn } from 'lodash';
import Vue from 'vue';
import { ActionTree, MutationTree } from 'vuex';

import { cartUtils } from './module.cart';

import api from '@/api';
import { ChargeEstimateRequest } from '@/api/api.charge-estimates';

import CartItem from '@/lib/cartItem';

import types from '@/store/types';

export interface CartSaveForLaterState {
  items: {
    [id: string]: CartItem;
  },
}

const state: CartSaveForLaterState = {
  items: {},
};

const actions: ActionTree<CartSaveForLaterState, any> = {
  /*
  Fetches the user's Save For Later collection, and then fetches rates for those items.
   */
  async [types.cart.saveForLater.fetch]({ commit, dispatch, rootState }) {
    if (!rootState.user.isLoggedIn) {
      return;
    }

    const results = await api.cartSaveForLater.fetchItems();

    const items = results.data.items || {};

    // The datetime properties of each item need to be rebuilt as local datetimes.
    // (This is because Moment.js objects get serialized into UTC datetime strings when persisted to the server
    // and browser sessionStorage.)
    Object.values<CartItem>(items).forEach(item => {
      item.startDate = CartItem.getMomentFromSerializedMomentString(item.startDate);
      item.endDate = CartItem.getMomentFromSerializedMomentString(item.endDate);
    });

    commit(types.cart.saveForLater.fetch, items);
    dispatch(types.cart.saveForLater.fetchEstimates);
  },

  async [types.cart.saveForLater.fetchEstimates]({ commit, state }, item) {
    if (!item) {
      Object.values(state.items)
        .map(item => item.exportAsEstimateApiParams() as ChargeEstimateRequest)
        .forEach(async payload => {
          try {
            const data = await api.chargeEstimates.fetch(payload);
            commit(types.checkout.fetchEstimates, { payload, data });
          }
          catch (e) {
            console.warn('failed to fetch estimates');
          }
        });
    }
    else {
      const payload = item.exportAsEstimateApiParams() as ChargeEstimateRequest;
      try {
        const data = await api.chargeEstimates.fetch(payload);
        commit(types.checkout.fetchEstimates, { payload, data });
      }
      catch (e) {
        console.warn('failed to fetch estimates');
      }
    }
  },

  /*
  Moves a Cart Item into the Save for Later collection.
  This action will attempt to merge an incoming item with an existing SFL item.
   */
  async [types.cart.saveForLater.addItem]({ commit, dispatch }, item) {
    const cartedItem = new CartItem(item);
    cartedItem.changed = Date.now();
    let existingItem = cartUtils.canMerge(cartedItem, state.items, Object.keys(state.items));

    // Remove the item from the Cart
    dispatch(types.cart.removeItem, cartedItem.id);

    // Merge the incoming item with an existing item, if possible
    if (existingItem) {
      existingItem = {
        ...existingItem,
        quantity: existingItem.quantity + cartedItem.quantity,
        changed: Date.now(),
      };
    }

    commit(types.cart.saveForLater.addItem, existingItem || cartedItem);
    dispatch(types.cart.saveForLater.save);
    dispatch(types.cart.saveForLater.fetchEstimates, existingItem || cartedItem);
  },

  /*
  Moves a Save for Later Item into the Cart.
  This action will attempt to merge an incoming item with an existing Cart item.
   */
  async [types.cart.saveForLater.moveToCart]({ commit, dispatch, rootState }, item) {
    const savedForLaterItem = new CartItem(item);
    delete savedForLaterItem.changed; // Drupal's Cart API cannot save this field.
    const existingItem = cartUtils.canMerge(savedForLaterItem, rootState.cart.items, Object.keys(rootState.cart.items));

    if (existingItem) {
      const quantity = existingItem.quantity + savedForLaterItem.quantity;
      dispatch(types.cart.updateItem, {id: existingItem.id, field: { quantity }});
    }
    else {
      dispatch(types.cart.addItem, savedForLaterItem);
    }

    commit(types.cart.saveForLater.removeItem, savedForLaterItem.id);
    dispatch(types.cart.saveForLater.save);
  },

  /*
  Removes an item from the Save for Later collection.
   */
  async [types.cart.saveForLater.removeItem]({ commit, dispatch }, itemId) {
    commit(types.cart.saveForLater.removeItem, itemId);
    dispatch(types.cart.saveForLater.save);
  },

  /*
  Updates an item within the Save for Later collection.
   */
  async [types.cart.saveForLater.updateItem]({ commit, rootState, dispatch }, item) {
    const cartedItem = item
      ? new CartItem(item)
      : new CartItem(rootState.cartItemForm);

    const { id } = cartedItem;

    if (!cartedItem.branchId) {
      cartedItem.branchId = rootState.cartSaveForLater.items[id].branchId;
    }

    commit(types.cart.saveForLater.updateItem, { id, fields: cartedItem });
    dispatch(types.cart.saveForLater.fetchEstimates, cartedItem);
    return dispatch(types.cart.saveForLater.save);
  },

  /*
  This is our one-and-only persistence API.
  It persists the entire collection of items.

  It must be invoked any time the vuex collection changes (e.g., whenever an item is updated, removed, or added).

  Due to bugginess, the Drupal Cart API is not being leveraged at this time.  Instead, the Save For Later
  collection is persisted via the Drupal User Preferences API.
   */
  async [types.cart.saveForLater.save]({ state }) {
    return api.cartSaveForLater.setItems(state.items);
  },
};

const mutations: MutationTree<CartSaveForLaterState> = {
  [types.cart.saveForLater.fetch](state, items) {
    state.items = {};
    forOwn(items, item => Vue.set(state.items, item.id, new CartItem(item)));
  },
  [types.cart.saveForLater.addItem](state, item) {
    Vue.set(state.items, item.id, new CartItem(item));
  },
  [types.cart.saveForLater.removeItem](state, id) {
    Vue.delete(state.items, id);
  },
  [types.cart.saveForLater.updateItem](state, { id, fields }) {
    Object.entries(fields).forEach(([property, value]) => {
      state.items[id][property] = value;
    });
  },
};

export default {
  state,
  actions,
  mutations,
};
