import Vue from 'vue';
import types from '@/store/types';
import { cloneDeep } from 'lodash';

const state = {
  snapshot: {},
};

const actions = {
  [types.snapshot.take]({ commit }, { type, data }) {
    // need to clone in order to not overwrite our snapshot when we reset the form
    commit(types.snapshot.take, { type, data: cloneDeep(data) });
  },
  [types.snapshot.replay]({ commit, state }, { type }) {
    if (state.snapshot[type]) {
      try {
        commit(type, state.snapshot[type]);
      }
      catch (e) {
        // no op. Some snapshots may be of custom "types" that aren't a part of our Vuex types.
      }
      return state.snapshot[type];
    }
  },
  [types.snapshot.clear]({ commit }, type) {
    commit(types.snapshot.clear, { type });
  },
};

const mutations = {
  [types.snapshot.take](state, { type, data }) {
    Vue.set(state.snapshot, type, data);
  },
  [types.snapshot.clear](state, { type }) {
    Vue.set(state.snapshot, type, {});
  },
};

export default {
  state,
  actions,
  mutations,
};
