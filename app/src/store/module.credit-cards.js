/* eslint-disable object-curly-newline */

import api from '@/api';
import types from '@/store/types';
import { forEach } from 'lodash';
import Vue from 'vue';
import { mapLabels } from '@/utils';

const cardLabels = mapLabels({
  noAuthorize: 'card.noAuthorize',
  errorRetrievingCards: 'card.errorRetrievingCards',
  errorRemovingCard: 'card.errorRemovingCard',
  errorUpdateCard: 'card.errorUpdateCard',
});

const state = {
  all: [],
};

const actions = {
  [types.creditCards.fetch]({ commit }) {
    return api.creditCards.fetch()
      .then((response) => {
        if (response.data.code !== 200) {
          return Promise.reject(new Error(cardLabels.errorRetrievingCards));
        }
        commit(types.creditCards.fetch, response.data.data);
      });
  },
  [types.creditCards.add]({ dispatch }, card) {
    return api.creditCards.add(card)
      .then(({ data }) => {
        dispatch(types.creditCards.fetch);
        return data.data; // Return the actual data and NOT what was received to kick this off.
      })
      .catch(() => Promise.reject(new Error(cardLabels.noAuthorize)));
  },
  [types.creditCards.remove]({ commit }, cardId) {
    return api.creditCards.remove(cardId)
      .then((response) => {
        // Expect "data" to be a string (a card id). Errors are likely to come in as an Array, but if
        // they come in as a string, they'll likely be a message with spaces...
        if (response.data.code !== 200) {
          return Promise.reject(new Error(cardLabels.errorRemovingCard));
        }
        commit(types.creditCards.remove, { cardId });
      });
  },
  [types.creditCards.update]({ commit }, cardData) {
    return new Promise((resolve, reject) => {
      api.creditCards.update(cardData.cardId, cardData)
        // Should not be destructuring, should be taking the whole response object.  So sayeth Dietz.
        .then((response) => {
          if (response.data.code !== 200) {
            reject(new Error(cardLabels.errorUpdateCard));
          }
          commit(types.creditCards.update, { cardData: response.data.data });
          resolve(response.data.data);
        }, (error) => {
          reject(error);
        });
    });
  },
};

const mutations = {
  [types.creditCards.fetch](state, data) {
    state.all = Array.isArray(data) ? data.filter(card => card.status === true) : [];
  },
  [types.creditCards.add](state, card) {
    state.all.push(card);
  },
  [types.creditCards.update](state, payload) {
    const index = state.all.findIndex(singleCard => singleCard.id === payload.cardData.id);
    forEach(payload.cardData, (value, key) => {
      if (value !== null && (key in state.all[index])) {
        Vue.set(state.all[index], key, value);
      }
    });
  },
  [types.creditCards.remove](state, payload) { // eslint-disable-line
    const index = state.all.findIndex(card => card.id === payload.cardId);
    state.all.splice(index, 1);
  },
  'creditCards/reset': function (state) {
    state.all = [];
  },
};

export default {
  state,
  actions,
  mutations,
};
