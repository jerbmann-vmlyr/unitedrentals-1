import Vue from 'vue';

import api from '@/api';
import types from '@/store/types';

const state = {

  /**
   * Requesters returned from a api-appliance call, keyed by accountId, and then
   * requesterId.
   * This state gets persisted in the browsers Session Storage.
   *
   * Example: state.all['1234']['4321'] is {id: '4321', requesterName, requesterEmail, requesterPhone, ...}
   */
  all: {},

  fetching: false,
};

const actions = {
  [types.requesters.fetch]({ commit, state }, accountId) {
    if (!accountId) {
      return;
    }

    if (Object.keys(state.all[accountId] || {}).length > 0) {
      commit('requesters/fetching', false);
      return Promise.resolve(state.all[accountId]);
    }

    commit('requesters/fetching', true);
    return api.requesters.fetch(accountId)
      .then(({ data }) => {
        commit('requesters/fetching', false);
        if (data === null) {
          const error = new Error(`No requesters data returned for ${accountId}. (Action: types.requesters.fetch)`);
          return Promise.reject(error);
        }
        commit(types.requesters.fetch, { data, accountId });
      })
      .catch(() => {
        commit('requesters/fetching', false);
      });
  },
};

const mutations = {
  [types.requesters.fetch](state, { data, accountId }) {
    if (!state.all[accountId]) {
      Vue.set(state.all, accountId, {});
    }
    data.forEach((requester) => {
      if (!requester.guid) {
        return;
      }

      Vue.set(state.all[accountId], requester.guid, requester);
    });
  },
  'requesters/fetching': function (state, bool) {
    state.fetching = bool;
  },
  'requesters/reset': function (state) {
    state.all = {};
    state.fetching = false;
  },
};

export default {
  actions,
  mutations,
  state,
};
