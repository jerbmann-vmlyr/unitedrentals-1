
# Client-side Events

[Go here](https://docs.appcues.com/article/301-client-side-events-reference) for documentation source.

There are 3 "levels" of events which form a hiearchy:
1. Flow
2. Step-group
3. Step-child

_Flow_

Pertain to the overall lifecycle of a flow, which is composed of
separate step groups.

_Step-group_

Describe the lifecycle of an individual step-group in a flow. A step-group
will usually include some number of children.

_Step-child_

Describe the nitty-gritty details of what occurs during the process of
showing and interacting with a particular step-child.
