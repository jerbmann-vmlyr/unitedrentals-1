import { UserEventEmitter, UserEvent } from './types';

export const userEventEmitters: UserEventEmitter[] = [
  {
    events: [
      UserEvent.ItemAddedToCart,
      UserEvent.AdditionalItemIsAddedToCart,
    ],
    createEmitter(store, emit) {
      store.watch(
        state => Object.keys(state.cart.items).length,
        (is, was) => {
          if (is > was) emit(UserEvent.ItemAddedToCart);
          if (is > was && was > 0) emit(UserEvent.AdditionalItemIsAddedToCart);
        },
      );
    },
  },
  {
    events: [ UserEvent.SecondOrderCreated ],
    createEmitter(store, emit) {
      // There's not a clean way to watch a getter on a store,
      // so forgive the hacky
      const getReservationBlockCount = () => {
        return Object.values(store.getters.reservationBlocks).length;
      };

      let blockCount: number = getReservationBlockCount();
      store.watch(
        state => Object.values(state.cart.items).length,
        // Do this in a set timeout to make sure the getter has been updated
        () => setTimeout(() => {
          const updatedBlockCount = getReservationBlockCount();
          if (updatedBlockCount > blockCount && updatedBlockCount > 1) {
            emit(UserEvent.SecondOrderCreated);
          }
          blockCount = updatedBlockCount;
        }),
      );
    },
  },
  {
    events: [ UserEvent.UserAppliedAFilter ],
    createEmitter(store, emit) {
      store.watch(
        state => state.facets.selectedFilters.length,
        (is, was) => is > was && emit(UserEvent.UserAppliedAFilter),
      );
    },
  },
  {
    events: [ UserEvent.UserAccountSelected ],
    createEmitter(store, emit) {
      store.watch(
        state => state.user.defaultAccountId,
        () => emit(UserEvent.UserAccountSelected),
      );
    },
  },
  {
    events: [ UserEvent.UserJobsiteSelected ],
    createEmitter(store, emit) {
      store.watch(
        state => state.user.defaultJobId,
        () => emit(UserEvent.UserJobsiteSelected),
      );
    },
  },
  {
    events: [ UserEvent.UserSpecifiedLocation ],
    createEmitter(store, emit) {
      store.watch(
        state => state.user.ratesPlace,
        () => emit(UserEvent.UserSpecifiedLocation),
      );
    },
  },
  {
    events: [ UserEvent.SubmittedQuote ],
    createEmitter(store, emit) {
      store.watch(
        state => Object.values(state.checkout.billing)
          .filter(trans => trans.transType as any === 'X' || trans.transType === 'Q')
          .length,
        (is, was) => is > was && emit(UserEvent.SubmittedQuote),
      );
    },
  },
  {
    events: [ UserEvent.SubmittedReservation ],
    createEmitter(store, emit) {
      store.watch(
        state => Object.values(state.checkout.billing)
          .filter(trans => trans.transType === 'R')
          .length,
        (is, was) => is > was && emit(UserEvent.SubmittedReservation),
      );
    },
  },
  {
    events: [ UserEvent.ItemFavorited ],
    createEmitter(store, emit) {
      store.watch(
        state => state.favorites.all.length,
        (is, was) => is > was && emit(UserEvent.ItemFavorited),
      );
    },
  },
];
