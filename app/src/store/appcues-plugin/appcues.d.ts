export interface Appcues<U, E> {
  /**
   * Generates a session-based unique ID for the current user.
   */
  anonymous: () => void;
  /**
   * Puts the SDK in debug mode, showing more information about the SDK's
   * inner workings.
   */
  debug: () => void;
  /**
   * Identifies the current user with an ID and an optional set of properties.
   */
  identify: (userId: string, user: Partial<U>) => void;
  /**
   * Undo a corresponding `on` call.
   */
  off(event: 'all', cb: AllEventsCallback): void;
  off(event: EventId, cb: SingleEventCallback): void;
  /**
   * Fire the callback function when the given event is triggered by the SDK.
   */
  on(event: 'all', cb: AllEventsCallback): void;
  on(event: EventId, cb: SingleEventCallback): void;
  /**
   * Fire the callback function once the next time the given event is
   * triggered by the SDK.
   */
  once(event: 'all', cb: AllEventsCallback): void;
  once(event: EventId, cb: SingleEventCallback): void;
  /**
   * Notifies the Appcues SDK that the state of the application has changed.
   */
  page: () => void;
  /**
   * Clears all known information about the current user in this session.
   * This call will clear the flow in progress and wipe any data we generate
   * for a user. This is useful when your user logs out of your application.
   *
   * NOTE: When used in conjunction with the Anonymous call this can cause flows
   * to show twice, since it would wipe the generated ID for that anon guest.
   */
  reset: () => void;
  settings: () => Readonly<Settings>;
  /**
   * Force a specific Appcues flow to appear for the current user.
   * This method ignores any targeting that is set on the flow.
   */
  show: (flowId: string) => void;
  /**
   * Tracks a custom event (by name) taken by the current user, along with any
   * properties about that event.
   */
  track: (eventName: E, eventData?: object) => void;
  user: () => Promise<U>;
}

type SingleEventCallback = (event: StepEvent) => void;
type AllEventsCallback = (eventName: EventId, event: StepEvent) => void;

export type EventId
  /* Listens for all events emitted by the SDK */
  = 'all'
  /**
   * Fired when the first step of a flow is started or displayed on the page.
   * If this event is fired, then the Flow Aborted and Flow Error events should
   * not be fired. This should be fired before the Step Started event.
   */
  | 'flow_started'
  /**
   * Fired when the user completes the last step of the flow. This should be
   * fired after the Step Completed event.
   */
  | 'flow_completed'
  /**
   * Fired when the user chooses to skip a flow. This should fire after the
   * Step Skipped event is fired for the corresponding step.
   */
  | 'flow_skipped'
  /**
   * Fired when there is a "fatal" error that prevents starting or completing a
   * flow. If this event is fired before the first step is started, then
   * Flow Started should not be fired. This should be fired after the
   * Flow Error event.
   */
  | 'flow_aborted'
  /**
   * Fired when the step-group is run or displayed on the page. If this event is
   * fired, then the Step Aborted and Step Error events should not be fired.
   * This should be fired before Step Child Activated or Step Child Recovered
   * events are fired.
   */
  | 'step_started'
  /**
   * Fired when the user completes a step-group. For modals and tooltips,
   * this means closing the content with the "complete button". For hotspots,
   * this means clicking on the last hotspot. This should be fired before the
   * Flow Completed event.
   */
  | 'step_completed'
  /**
   * Fired when the user chooses to skip a step-group or flow. This should be
   * fired before the Flow Skipped event.
   */
  | 'step_skipped'
  /**
   * Fired when there is a "fatal" error that prevents starting or completing a
   * step-group. If this event is fired before we show the step-group, then
   * Step Started should not be fired. This should be fired before the
   * Flow Aborted event.
   */
  | 'step_aborted'
  /**
   * Fired when a user interacts with a step-group in some way. Currently we
   * track when a user does any of the following:
   * - Clicks a link in a flow.
   * - Clicks a button (built-in or custom) in a flow.
   * - Submits a form in a flow.
   * - Clicks on a hotspot to expand it. This should be fired before the
   *   step_child_deactivated or step_child_activated events that it may trigger.
   */
  | 'step_interacted'
  /**
   * Fired when a user submits a form in a modal. This should be fired before
   * the Step Child Deactivated event it may trigger and before the
   * Form Field Submitted event(s) that it will trigger.
   */
  | 'form_submitted'
  /**
   * Fired for each field in a form that a user submits in a modal.
   * This should be fired after the Form Submitted event.
   */
  | 'form_field_submitted'
  /**
   * Fired when the user successfully views the content of a particular step
   * child. For example: viewing a particular "page" in a modal; viewing the
   * content of a tooltip or hotspot (either by clicking the hotspot or next
   * button). This should fire after the Step Interacted event.
   */
  | 'step_child_activated'
  /**
   * Fired when a user closes/finishes a particular step child. For example,
   * leaving a particular "page" in a modal or closing a tooltip.
   * This should fire after the Step Interacted event.
   */
  | 'step_child_deactivated';

export interface Account {
  isTrial: boolean;
  isTrialExpired: boolean;
  keenScopedKeyWrite: string;
  stripePlanId: string;
  uuid: string;
}

export interface Settings {
  RELEASE_ID: string;
  VERSION: string;
  account: Account;
  accountId: string;
  integrations: {};
  styling: {
    globalBeaconColor: string;
    globalHotspotAnimation: string;
    globalStylings: string;
    id: string;
  }
}

export interface FlowEvent {
  /* Snake case identifier for the event type */
  id: EventId;
  /* Human-formatted name of the event type */
  name: string;
  /* The ID of the flow object */
  flowId: string;
  /* Human-readable title of this flow */
  flowName: string;
  /* Indicates a particular version of a flow (equal to updatedAt timestamp) */
  flowVersion: number;
  /* Identifier for a particular session of a user */
  sessionId: number;
  /* Timestamp at which the event was generated */
  timestamp: number;
}

export interface FlowError {
  error: string; // Test summary of the error
  details: string; // More detailed technical explanation of the error
}

export interface StepError {
  error: string; // Test summary of the error
  details: string; // More detailed technical explanation of the error
}

export interface StepChildError {
  error: string; // The error description
}

export interface StepEvent extends FlowEvent {
  /* The ID of the step-group object */
  stepId: string;
  /* The type of pattern of this step-group (hotspot-group, modal, etc) */
  stepType: string;
}

export type InteractionType = 'click' | 'submit' | 'ui_modified';

export interface StepInteractedEvent extends StepEvent {
  id: 'step_interacted';
  /* The ID of the step child in which the user performed an interaction */
  stepChildId: string;
  /* The index of the step child in which the user performed an interaction */
  stepChidNumber: number;
  /* How the user interacted with the step-group (click, hover, submit, etc) */
  interactionType: InteractionType;
  /* An object representing information about the user's interaction */
  interaction: object;
  /* Category of the thing upon which the interactionType was performed */
  category: string;
  /* Where the user is directed (for clicks on links or buttons) */
  destination?: string;
}

export interface StepChildEvent extends StepEvent {
  stepChildId: string; // The ID of the step child
  stepChildNumber: number; // The index of the step child
}

export interface StepChildDeactivatedEvent extends StepChildEvent {
  id: 'step_child_deactivated';
  timeSpent: number; // The amount of time in ms spent on the step child
}
