import { Plugin, Store } from 'vuex';
import { Transaction } from '@/lib/transactions';

import { AccountsState } from '../module.accounts';
import { CartSaveForLaterState } from '../module.save-for-later';
import { CartState } from '../module.cart';
import { FavoritesState } from '../module.favorites';
import { GoogleMapsState } from '../module.google-maps';
import { InFlightRequestsState } from '../module.in-flight-requests';
import { JobsState } from '../module.jobs';
import { QuotesState } from '../module.quotes';
import { RouteState } from '../module.route';
import { TransactionsState } from '../module.transactions';
import { UserState } from '../module.user';
import { WorkplaceState } from '../module.workplace';

export type AppcuesPluginStore = Store<PluginStoreState>;
export type AppcuesPlugin = Plugin<PluginStoreState>;

interface PluginStoreState {
  accounts: AccountsState;
  cart: CartState;
  chargeEstimates: any;
  checkout: {
    billing: { [id: string]: Transaction };
  };
  facets: any;
  favorites: FavoritesState;
  googleMaps: GoogleMapsState;
  inFlightRequests: InFlightRequestsState;
  jobs: JobsState;
  quotes: QuotesState;
  reservations: any;
  route: RouteState;
  saveForLater: CartSaveForLaterState;
  transactions: TransactionsState;
  user: UserState;
  workplace: WorkplaceState;
}

/**
 * A Profiler is a set of functions that generates pieces of information
 * (as objects) that gets passed to Appcues.indentify
 */
export type UserProfilerWatcher = (state: PluginStoreState) => any;

export interface UserProfiler {
  watchers: UserProfilerWatcher[];
  getPartialProfile: (store: Store<PluginStoreState>) => Promise<Partial<UserProfile>>;
}

export interface UserProfile {
  userId: string;
  /* property set as "true" either after login, or when a guest adds an item to cart) */
  cartContainsItems: boolean;
  /* property set as a count of items after login, if there are multiple items in cart */
  cartContainsMultipleOrders: number;
  /* property set as "true" after login, if cart is empty */
  cartIsEmpty: boolean;
  /* property is set as 0 or number of favorites after login */
  favoritesExist: number;
  /**
   * property set as "true" after login if user has zero accounts
   * (i.e. an authenticated guest user)
   */
  isOrsCustomer: boolean;
  /**
   * property set as "true" after login if the "tcLevel" from /customer/accounts
   * equals "C"
   */
  isTcCustomer: boolean;
  /**
   * property set as "true" after login if the "tcLevel" from /customer/accounts
   * equals "E"
   */
  isUrcCustomer: boolean;
  /**
   * property is set as "true" after login if one has been specified. Also, this
   * property is sent as "true" after a guest creates a jobsite
   */
  jobsiteIsSpecified: boolean;
  /**
   * property is set as "true" after geolocation completes. Also, this property
   * is sent as "true" after user interacts with a location widget
   */
  locationIsKnown: boolean;
  /* property is set as 0 or number of quotes after login */
  quotesExist: number;
  /**
   * property is set as 0 or number of quotes after user visits the Quote
   * listing page within Account Profile
   */
  reservationsExist: number;
  /* property is set as "true" after login if multiple accounts */
  userHasMultipleAccounts: boolean;
  /* property is set as "true" after login if only one account */
  userHasOneAccount: boolean;
  /* property set as "true" after login */
  userIsAuthenticated: boolean;
}


export interface UserEventEmitter {
  events: UserEvent[],
  createEmitter: (
    store: AppcuesPluginStore,
    emit: (event: UserEvent, data?: any) => void,
  ) => void;
}

export enum UserEvent {
  /**
   * event is fired after login
   */
  UserLoggedIn = 'userLoggedIn',
  /**
   * event is fired after sign out
   */
  UserLoggedOut = 'userLoggedOut',
  /**
   * event is fired after item is added to cart
   */
  ItemAddedToCart = 'itemAddedToCart',
  /**
   * event is fired if, when adding to cart, another item is already in the cart
   */
  AdditionalItemIsAddedToCart = 'additionalItemIsAddedToCart',
  /**
   * event is fired when reservation blocks compute a second order
   */
  SecondOrderCreated = 'secondOrderCreated',
  /**
   * event is fired after user applies a filter within marketplace catalog
   */
  UserAppliedAFilter = 'userAppliedAFilter',
  /**
   * event is fired when user.defaultAccountId changes (aka, when user changes an account via any selector)
   */
  UserAccountSelected = 'userAccountSelected',
  /**
   * event is fired when user.defaultJobsiteId changes (aka, jobsite-selector.vue changes)
   */
  UserJobsiteSelected = 'userJobsiteSelected',
  /**
   * whenever user.ratesPlace changes, aka when user manipulates an autocomplete vue component
   */
  UserSpecifiedLocation = 'userSpecifiedLocation',
  /**
   * when api confirms a reservation submission has succeeded
   */
  SubmittedReservation = 'submittedReservation',
  /**
   * when api confirms a quote submission succeeded
   */
  SubmittedQuote = 'submittedQuote',
  /**
   * when user activates a favorite icon in marketplace equipment listing
   */
  ItemFavorited = 'itemFavorited',
}
