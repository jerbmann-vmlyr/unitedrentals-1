// import { Store } from 'vuex';
// import store from './index';
// import types from '@/store/types';

const flattenValues = obj =>
  Object.entries(obj).reduce((accum: string[], [prop, value]) =>
    typeof value !== 'string' && Object.keys(value).length > 0
      ? [...accum, ...flattenValues(value)]
      : [...accum, value],
  []);


xdescribe('Vuex store', () => {
  /**
   * TODO: Commenting this out while trying to get the build to work
   */

  test('TODO: Fix the @/store/index.spec.ts file', () => {
    expect(true).toBe(true);
  });
  // test('Default export is an instance of vuex Store', () => {
  //   expect(store).toBeInstanceOf(Store);
  // });

  // const typeValues = flattenValues(types);

  // describe('Actions', () => {
  //   Object.keys(store['_actions']).map(action => {
  //     // TODO: Turn these back on when ready
  //     xtest(`${action} has a corresponding type`, () => {
  //       expect(typeValues.includes(action)).toBe(true);
  //     });
  //   });
  // });

  // describe('Mutations', () => {
  //   Object.keys(store['_mutations']).map(action => {
  //     // TODO: Turn these back on when ready
  //     xtest(`${action} has a corresponding type`, () => {
  //       expect(typeValues.includes(action)).toBe(true);
  //     });
  //   });
  // });

  // describe('Getters', () => {
  //   Object.keys(store['getters']).map(action => {
  //     // TODO: Turn these back on when ready
  //     xtest(`${action} has a corresponding type`, () => {
  //       expect(typeValues.includes(action)).toBe(true);
  //     });
  //   });
  // });

  // describe('Unused types', () => {
  //   const allUsedTypes = [
  //     ...Object.keys(store['_actions']),
  //     ...Object.keys(store['_mutations']),
  //     ...Object.keys(store['getters']),
  //   ];

  //   typeValues.map(type => {
  //     // TODO: Turn these back on when ready
  //     xtest(`${type} has been used`, () => {
  //       // expect(allUsedTypes).toContain(type);
  //       expect(allUsedTypes.includes(type)).toBe(true);
  //     });
  //   });

  // });

});
