/**
 * The actual store components (state, mutations, etc) of the route module are
 * implemented automatically by the vuex-router-sync package, which is wired
 * up in main.js.
 *
 * This file exists only as a placeholder for the RouteState interface.
 */
import { Route } from 'vue-router';

export interface RouteState extends Route {
  from?: Route;
}
