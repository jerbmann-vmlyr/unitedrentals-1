import Vue from 'vue';
import { ActionTree, MutationTree, GetterTree } from 'vuex';
import api from '@/api';
import { FlightToken } from '@/api/flight-track';
import types from '@/store/types';
import { InFlightRequestsState } from './module.in-flight-requests';
import { UserState } from './module.user';
import { Transaction, TransactionMap, TransactionUtils, RequisitionMap } from '@/lib/transactions';

export interface TransactionsState {
  all: { [accountId: string]: TransactionMap; };
  requisitions: { [accountId: string]: RequisitionMap };
}

type TransactionsRootState = {
  transactions: TransactionsState,
  inFlightRequests: InFlightRequestsState,
  user: UserState,
};

const state: TransactionsState = {
  /**
   * Reservations returned from a api-appliance call, keyed by guid, and
   * accountId(not required).
   */
  all: {},
  requisitions: {},
};

const getters: GetterTree<TransactionsState, TransactionsRootState> = {
  [types.transactions.fetchTransactionRequestsPending](state, getters): boolean {
    return getters[types.inFlightRequests.tokensAreInFlight](FlightToken.fetchTransactions);
  },
  [types.transactions.openContracts](state, _, rootState): Transaction[] {
    const transactionMap: TransactionMap = state.all[rootState.user.defaultAccountId] || {};
    return Object.values(transactionMap).filter(TransactionUtils.isOpenContract);
  },
  [types.transactions.closedContracts](state, _, rootState): Transaction[] {
    const transactionMap: TransactionMap = state.all[rootState.user.defaultAccountId] || {};
    return Object.values(transactionMap).filter(TransactionUtils.isClosedContract);
  },
  [types.transactions.reservations](state, _, rootState): Transaction[] {
    const transactionMap: TransactionMap = state.all[rootState.user.defaultAccountId] || {};
    return Object.values(transactionMap).filter(TransactionUtils.isReservation);
  },
  [types.transactions.quotes](state, _, rootState): Transaction[] {
    const transactionMap: TransactionMap = state.all[rootState.user.defaultAccountId] || {};
    return Object.values(transactionMap).filter(TransactionUtils.isQuote);
  },
};


const actions: ActionTree<TransactionsState, TransactionsRootState> = {
  async [types.transactions.fetch]({ commit }, { accountId, transType, skipCache }): Promise<TransactionMap> {
    if (!accountId) {
      console.warn('Action Error: types.transactions.fetch dispatched without a accountId.');
      return {};
    }

    const transactions = await api.transactions.fetch({ accountId, transType, skipCache });

    commit(types.transactions.fetch, { transactions, accountId });
    return transactions;
  },
};

const mutations: MutationTree<TransactionsState> = {
  [types.transactions.fetch](state, { transactions, accountId }) {
    Vue.set(state.all, accountId, transactions);
  },
  [types.transactions.saveRequisitions](state, { requisitions, accountId }) {
    Vue.set(state.requisitions, accountId, requisitions);
  },
  [types.transactions.reset](state) {
    state.all = {};
  },
};


export default {
  actions,
  mutations,
  state,
  getters,
};
