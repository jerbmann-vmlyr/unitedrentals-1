import Vue from 'vue';
import { ActionTree, GetterTree, MutationTree } from 'vuex';
import types from './types';
import { GoogleMapsUtils, WrappedPlaceResult } from '@/lib/google-maps';
import { loaded } from 'vue2-google-maps/dist/manager';

export interface GoogleMapsState {
  autocompleteService: Promise<google.maps.places.AutocompleteService>;
  placesService: Promise<google.maps.places.PlacesService>;
  places: {
    [query: string]: Promise<WrappedPlaceResult>;
  };
}

const state: GoogleMapsState = {
  autocompleteService: loaded.then(() => new google.maps.places.AutocompleteService()),
  placesService: loaded.then(() => new google.maps.places.PlacesService(document.createElement('div'))),
  places: {},
};

const actions: ActionTree<GoogleMapsState, any> = {

  async [types.googleMaps.getQueryPrediction](
    { commit, state },
    query: google.maps.places.QueryAutocompletionRequest,
  ): Promise<google.maps.places.QueryAutocompletePrediction> {
    const autocompleteService = await state.autocompleteService;

    return new Promise<google.maps.places.QueryAutocompletePrediction>((ok, error) =>
      autocompleteService.getQueryPredictions(query, (predictions = [], status) =>
        status === google.maps.places.PlacesServiceStatus.OK && predictions.length > 0
          ? ok(predictions[0])
          : error(new Error(`Failed to get place for: ${query.input}`)),
      ),
    );
  },

  async [types.googleMaps.getPlaceDetails](
    { commit, state },
    request: google.maps.places.PlaceDetailsRequest,
  ): Promise<google.maps.places.PlaceResult> {
    const placesService = await state.placesService;

    return new Promise<google.maps.places.PlaceResult>((ok, error) =>
      placesService.getDetails(request, (result, status) =>
        status === google.maps.places.PlacesServiceStatus.OK
          ? ok(result)
          : error(new Error(`Failed to get place details for: ${request.placeId}`)),
      ),
    );
  },

  async [types.googleMaps.getGeocode](
    { commit, state },
    request: { address: string },
  ): Promise<any> {
    const coder = new google.maps.Geocoder();

    return new Promise((ok, error) =>
      coder.geocode(request, result => {
        result !== null
            ? ok(result)
            : error(new Error(`Failed to get latitude/longitude for: ${request.address}`));
        },
      ),
    );
  },

  async [types.googleMaps.geocodeFromAddress](
    { commit, state, dispatch },
    unsanitizedQuery: string,
  ) {
    const query = GoogleMapsUtils.sanitizeInput(unsanitizedQuery);

    const wrappedLocation = new Promise(async ok => {
      try {
        const location = await dispatch(
          types.googleMaps.getGeocode,
          { address: query },
        );
        ok({ error: null, location: location[0], query });
      }
      catch (error) {
        ok({ error, location: null, query });
      }
    });

    return wrappedLocation;
  },

  async [types.googleMaps.getPlaceFromAddress](
    { commit, state, dispatch },
    unsanitizedQuery: string,
  ): Promise<WrappedPlaceResult> {
    // First sanitize the input
    const query = GoogleMapsUtils.sanitizeInput(unsanitizedQuery);

    // Now check the cache for an existing place result
    if (state.places[query]) {
      return state.places[query];
    }

    // Otherwise, create and store the pending place
    const wrappedPlace = new Promise<WrappedPlaceResult>(async ok => {
      try {
        const { place_id }: google.maps.places.QueryAutocompletePrediction = await dispatch(
          types.googleMaps.getQueryPrediction,
          { input: query },
        );

        const place: google.maps.places.PlaceResult = await dispatch(
          types.googleMaps.getPlaceDetails,
          { placeId: place_id },
        );

        ok({ error: null, place, query });
      }
      catch (error) {
        ok({ error, place: null, query });
      }
    });

    commit(types.googleMaps.savePlace, { query, place: wrappedPlace });
    return wrappedPlace;
  },
};

const getters: GetterTree<GoogleMapsState, any> = {
};

const mutations: MutationTree<GoogleMapsState> = {
  [types.googleMaps.savePlace](state, payload: { query: string, place: WrappedPlaceResult }) {
    Vue.set(state.places, payload.query, payload.place);
  },
};

export default {
  state,
  actions,
  getters,
  mutations,
};
