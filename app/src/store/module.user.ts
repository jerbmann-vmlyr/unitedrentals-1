import { ActionTree, MutationTree, GetterTree } from 'vuex';
import api from '@/api';
import geolocation from '@/utils/geolocation';
import types from '@/store/types';
import { get } from 'lodash';
import { USER_PREFERENCES_DEFAULTS, UserPreferences } from '@/lib/user-preferences';
import { ApiV2Utils } from '@/lib/apiv2';
import { CountryCode } from '@/lib/common-types';
import {
  WorkplaceEquipmentColumnKey,
  WorkplaceEquipmentColumnSettings,
  WorkplaceOrderColumnKey,
  WorkplaceOrderColumnSettings,
} from '@/lib/workplace';

export interface UserState extends UserPreferences {
  branchOnly: string;
  cardToRemove: string;
  changePasswordHref: string;
  defaultApproverId: string;
  defaultEndDate: string;
  defaultJobId: string;
  defaultRequesterId: string;
  defaultStartDate: string;
  forgotPasswordHref: string;
  geolocation: object;
  isEmployee: boolean;
  isLoggedIn: boolean;
  ratesPlaceBranch: string;
  ratesPlaceBranchError: boolean;
  uid: string;
  loggedInUsername: string;
}

const state: UserState = {
  ...USER_PREFERENCES_DEFAULTS,

  /**
   * @type {string} id of a "default job site" for some controls, such as the one in the Catalog
   * Listing page's Authentication Panel.
   */
  defaultJobId: '',
  defaultStartDate: '',
  defaultEndDate: '',
  defaultRequesterId: get(drupalSettings, ['ur', 'dal', 'guid'], ''),
  defaultApproverId: '',
  cardToRemove: '',

  /**
   * @type {promise} resolving to {Object.Position} returned by navigator.geolocation.getCurrentPosition()
   * NOTE: this object (a Promise) cannot be persisted into window.sessionStorage with IE 11.
   */
  geolocation: {},
  isEmployee: get(drupalSettings, ['user', 'isEmployee'], false),

  // DO NOT rely on drupalSettings.user, as this object is not immediately available. The <body> class, however, is.
  isLoggedIn: document.querySelector('body')!.classList.contains('user-logged-in'),

  /**
   * A Place Object returned from the Google Maps Autocomplete service.
   * This gets set by any Autocomplete component, and any other Location Autocomplete
   * component on the site. (E.g., the Global Header's Location Autocomplete, the Equipment Listing
   * Authentication Panel's Loc. AutoComplete, the Add To Cart Form's Loc. Autocomplete, etc.)
   *
   * @type {object} a Google Maps PlaceService result
   */
  ratesPlace: {},
  branchOnly: '',
  /**
   * A Branch ID associated with the state.ratesPlace
   * This module will automatically make an ajax call to find this branch
   * every time the state.ratesPlace gets updated.
   *
   * @type {string} branchId a branch id
   */
  ratesPlaceBranch: '',

  /**
   * An error is flagged if we are unable to find a Google Place from a user's location/jobsite-location
   */
  ratesPlaceBranchError: false,

  /**
   * Environment specific url's to the forgot/change password pages
   */
  changePasswordHref: get(drupalSettings, ['ur', 'change_password_path'], ''),
  forgotPasswordHref: get(drupalSettings, ['ur', 'forgot_password_path'], ''),

  /**
   * Drupal User Id
   * We watch for changes on this and flush the Vuex state when it changes
   */
  uid: get(drupalSettings, ['user', 'uid']),
  loggedInUsername: '',
};

const getters: GetterTree<UserState, any> = {
  [types.user.defaultAccount](state, getters, rootState) {
    return rootState.accounts.all[state.defaultAccountId] || {};
  },
  [types.user.defaultJob](state, getters, rootState) {
    return get(rootState.jobs.all, [state.defaultAccountId, state.defaultJobId], {});
  },
  [types.user.favoriteAccounts](state) {
    return state.favoriteAccounts.length > 0 ? state.favoriteAccounts : [];
  },
  [types.user.ratesPlaceCreated](state) {
    return Object.keys(state.ratesPlace).length > 0;
  },
  // TODO: Clean up this getter to remove the console err .find()
  [types.user.ratesPlaceAddress](state, getters) {
    if (!getters[types.user.ratesPlaceCreated]) {
      return '';
    }
    const { address_components } = state.ratesPlace as google.maps.places.PlaceResult;

    const city = address_components!.find(item => item.types.includes('locality')); // eslint-disable-line
    const st = address_components!.find(item => item.types.includes('administrative_area_level_1')); // eslint-disable-line
    const zip = address_components!.find(item => item.types.includes('postal_code')); // eslint-disable-line

    const cityLongName = city && city.long_name ? `${city.long_name}, ` : '';
    const stateShortName = (st && st.short_name) || '';
    const zipLongName = (zip && zip.long_name) || '';
    return `${cityLongName}${stateShortName} ${zipLongName}`;
  },
  [types.user.ratesPlaceCountryCode](state, getters): CountryCode | '' {
    const { address_components } = state.ratesPlace as google.maps.places.PlaceResult;
    // eslint-disable-next-line
    const country = address_components ? address_components.find(item => item.types.includes('country')) : '';

    return country && country.short_name as CountryCode || '';
  },
  [types.user.getLoggedInUsername](state) {
    return state.loggedInUsername || '';
  },
};

const actions: ActionTree<UserState, any> = {
  async [types.user.fetchDefaultAccountId]({ commit, state, rootState }) {
    let defaultAccountId = drupalSettings.user.defaultAccountId
      ? drupalSettings.user.defaultAccountId
      : Object.keys(rootState.accounts.all).length > 0
        ? Object.keys(rootState.accounts.all)[0]
        : await api.user.fetchUserPreferences('defaultAccountId');

    // Still don't have defaultAccountId?
    if (!defaultAccountId || typeof defaultAccountId === 'undefined' || defaultAccountId === null) {
      const defaultAccountObjects = await api.accounts.fetch();
      defaultAccountId = get(defaultAccountObjects, [0, 'id'], '');
    }

    if (Array.isArray(defaultAccountId) && defaultAccountId.length > 0) {
      [defaultAccountId] = defaultAccountId;
    }

    commit(types.user.setDefaultAccountId, defaultAccountId || '');
    return defaultAccountId || '';
  },

  async [types.user.fetchLoggedInUsername]({ commit }) {
    const loggedInUsername = await api.user.getLoggedInUsername();
    commit(types.user.setLoggedInUsername, loggedInUsername || '');
    return loggedInUsername || '';
  },

  async [types.user.updateDefaultAccountId]({ commit, rootState, getters, rootGetters }, id) {
    await api.user.saveUserPreferences('defaultAccountId', id);
    commit(types.user.setDefaultAccountId, id);
    return id;
  },

  async [types.user.resetEquipmentColumnsToDefault]({ commit, dispatch }) {
    const { equipmentColumns } = USER_PREFERENCES_DEFAULTS;
    return dispatch(types.user.updateEquipmentColumns, equipmentColumns);
  },

  async [types.user.resetOrderColumnsToDefault]({ commit, dispatch }) {
    const { orderColumns } = USER_PREFERENCES_DEFAULTS;
    return dispatch(types.user.updateOrderColumns, orderColumns);
  },

  async [types.user.fetchOrderColumns]({ state, commit  }) {
    const orderColumnsVersion = await api.user.fetchUserPreferences('orderColumnsVersion');

    if (!orderColumnsVersion || orderColumnsVersion.hash !== state.orderColumnsVersion.hash) {
      console.warn(`Updating equipment column preferences to latest schema version`);
      await api.user.saveUserPreferences('orderColumnsVersion', state.orderColumnsVersion);
      await api.user.saveUserPreferences('orderColumns', USER_PREFERENCES_DEFAULTS.orderColumns);
    }

    const orderColumns = await api.user.fetchUserPreferences('orderColumns');
    commit(types.user.setOrderColumns, orderColumns);
    return orderColumns;
  },
  async [types.user.fetchEquipmentColumns]({ state, commit }) {
    const equipmentColumnsVersion = await api.user.fetchUserPreferences('equipmentColumnsVersion');

    if (!equipmentColumnsVersion || equipmentColumnsVersion.hash !== state.equipmentColumnsVersion.hash) {
      console.warn(`Updating equipment column preferences to latest schema version`);
      await api.user.saveUserPreferences('equipmentColumnsVersion', state.equipmentColumnsVersion);
      await api.user.saveUserPreferences('equipmentColumns', USER_PREFERENCES_DEFAULTS.equipmentColumns);
    }

    const equipmentColumns = await api.user.fetchUserPreferences('equipmentColumns');
    commit(types.user.setEquipmentColumns, equipmentColumns);
    return equipmentColumns;
  },

  async [types.user.updateEquipmentColumns](
    { commit },
    columns: WorkplaceEquipmentColumnSettings[],
  ) {
    await api.user.saveUserPreferences('equipmentColumns', columns);
    commit(types.user.setEquipmentColumns, columns);
    return columns;
  },

  async [types.user.updateEquipmentColumn](
    { commit, state, dispatch },
    { columnKey, changes }: { columnKey: WorkplaceEquipmentColumnKey, changes: Partial<WorkplaceEquipmentColumnSettings> },
  ) {
    const columns = state.equipmentColumns.map(col => (
      col.columnKey !== columnKey
        ? col
        : { ...col, ...changes }
    ));

    return dispatch(types.user.updateEquipmentColumns, columns);
  },

  async [types.user.fetchOrderColumns]({ state, commit }) {
    const orderColumnsVersion = await api.user.fetchUserPreferences('orderColumnsVersion');

    if (!orderColumnsVersion || orderColumnsVersion.hash !== state.orderColumnsVersion.hash) {
      console.warn(`Updating equipment order preferences to latest schema version`);
      await api.user.saveUserPreferences('orderColumnsVersion', state.orderColumnsVersion);
      await api.user.saveUserPreferences('orderColumns', USER_PREFERENCES_DEFAULTS.orderColumns);
    }

    const orderColumns = await api.user.fetchUserPreferences('orderColumns');
    commit(types.user.setOrderColumns, orderColumns);
    return orderColumns;
  },

  async [types.user.updateOrderColumns](
    { commit },
    columns: WorkplaceOrderColumnSettings[],
  ) {
    await api.user.saveUserPreferences('orderColumns', columns);
    commit(types.user.setOrderColumns, columns);
    return columns;
  },

  async [types.user.updateOrderColumn](
    { commit, state, dispatch },
    { columnKey, changes }: { columnKey: WorkplaceOrderColumnKey, changes: Partial<WorkplaceOrderColumnSettings> },
  ) {
    const columns = state.orderColumns.map(col => (
      col.columnKey !== columnKey
        ? col
        : { ...col, ...changes }
    ));

    return dispatch(types.user.updateOrderColumns, columns);
  },

  async [types.user.getGeolocation](
    { commit, state },
  ) {
    const geo = state.geolocation;
    if (Object.keys(geo).length === 0) {
      geolocation().then(loc => {
        commit(types.user.setGeolocation, {
          allowed: true,
          coords: {
            latitude: loc.coords.latitude,
            longitude: loc.coords.longitude,
          },
        });
      },
      err => {
        commit(types.user.setGeolocation, {
            allowed: false,
          });
      });
    }
  },

  [types.user.clearRatesPlace]({ commit }) {
    commit(types.user.clearRatesPlace);
  },

  [types.user.clearRatesPlaceBranch]({ commit }) {
    commit(types.user.clearRatesPlaceBranch);
  },

  [types.user.clearDefaultJobId]({ commit }) {
    commit(types.user.clearDefaultJobId);
  },

  [types.user.defaultJobId]({ commit }, id) {
    commit(types.user.defaultJobId, id);
  },

  [types.user.defaultStartDate]({ commit }, date) {
    commit(types.user.defaultStartDate, date);
  },

  [types.user.defaultEndDate]({ commit }, date) {
    commit(types.user.defaultEndDate, date);
  },

  [types.user.defaultRequesterId]({ commit }, id) {
    commit(types.user.defaultRequesterId, id);
  },

  [types.user.defaultApprover]({ commit }, id) {
    commit(types.user.defaultApprover, id);
  },

  // Add an accountId to the list of favorite accounts.
  async [types.user.addFavoriteAccount]({ commit, state }, accountId: string) {
    // This is bad practice to be preemptively updating state without ensuring the api call was successful, but this was the ask by Argo.
    const favoriteAccounts = [ ...state.favoriteAccounts, accountId];
    commit(types.user.favoriteAccounts, favoriteAccounts);
    commit(types.accounts.toggleFavorite, accountId);
    await api.accounts.saveFavorited(accountId, true);
    return favoriteAccounts;
  },

  // Remove an accountId from the list of favorite accounts.
  async [types.user.removeFavoriteAccount]({ commit, state }, accountId: string) {
    // This is bad practice to be preemptively updating state without ensuring the api call was successful, but this was the ask by Argo.
    const favoriteAccounts = state.favoriteAccounts.filter(id => id !== accountId);
    commit(types.user.favoriteAccounts, favoriteAccounts);
    commit(types.accounts.toggleFavorite, accountId);
    await api.accounts.saveFavorited(accountId, false);
    return favoriteAccounts;
  },

  /**
   * Sets the Rates Place, then finds and loads the closest branch to that place.
   *
   * @param {function} commit - vuex
   * @param {object} place - a Google Place Service object
   * @return {Promise} errors should be caught in a component
   */
  async [types.user.ratesPlace]({ commit, dispatch, state }, place) {
    commit(types.user.ratesPlace, place);
    await api.user.saveUserPreferences('ratesPlace', place);
    return dispatch(types.user.ratesPlaceBranch, place);
  },

  /**
   * Find a branch nearby the ratesPlace.
   * @param {function} commit - vuex
   * @param {object} ratesPlace - a Google Place Service object
   * @return {Promise} resolving to api response
   */
  [types.user.ratesPlaceBranch]({ commit }, ratesPlace) {
    if (!ratesPlace || !ratesPlace.geometry) {
      commit(types.user.ratesPlaceBranch, '');
      const error = new Error('Cannot set Rates Place Branch. (No Rates Place provided.) Removing Rates Place Branch.');
      commit(types.user.ratesPlaceBranchError, true);
      return Promise.reject(error);
    }

    commit(types.user.ratesPlaceBranchError, false);

    const { lat, lng } = ratesPlace.geometry.location;
    return api.branches.getNearbyBranch({ lat, lng })
      .then(ApiV2Utils.unwrapResponseData)
      .then(data => {
        try {
          // getNearbyBranch now returns a collection of Branch
          // with the "closest" branch as the 0 member of the
          // collection. This was done so that some day we
          // could create a "fall back" state if the 0
          // member (closest) fails for some reason.
          commit(types.branch.fetchById, data[0]);

          commit(types.user.ratesPlaceBranch, data[0].branchId);
        }
        catch (error) {
          console.warn(`No branch found near ${ratesPlace.formatted_address}`); // eslint-disable-line
          console.error(error);
        }
      });
  },

  /**
   * Find a branch nearby the ratesPlace without commit.
   * @param {object} place - a Google Place Service object
   * @return {Promise} resolving to api response
   */
  [types.user.branchOnly]({ commit, dispatch, state }, place) {
    return dispatch(types.user.placeBranch, place);
  },

  [types.user.placeBranch]({ commit }, place) {
    const { lat, lng } = place.geometry.location;
    return api.branches.getNearbyBranch({ lat: lat(), lng: lng() }).then(response => {
      const branch = response.data.data[0];
      commit(types.user.placeBranch, place);
      commit(types.user.branchOnly, branch.branchId);
    });
  },

  [types.user.setCardIdForRemoval]({ commit }, cardId) {
    commit(types.user.setCardIdForRemoval, cardId);
  },

  [types.user.setIsLoggedIn]({ commit }, isLoggedIn) {
    commit(types.user.setIsLoggedIn, isLoggedIn);
  },
};

const mutations: MutationTree<UserState> = {
  [types.user.setUid](state) {
    state.uid = get(drupalSettings, ['user', 'uid']);
  },
  async [types.user.clearRatesPlace](state) {
    await api.user.saveUserPreferences('ratesPlace', {});
    state.ratesPlace = {};
  },
  // This should not be merged into clearRatesPlace, as it would introduce an error w/ the MELP (Marketplace Eqp Listing pg)
  [types.user.clearRatesPlaceBranch](state) {
    state.ratesPlaceBranch = '';
  },

  [types.user.clearDefaultJobId](state) {
    state.defaultJobId = '';
  },

  [types.user.setCardIdForRemoval](state, id) {
    state.cardToRemove = id;
  },

  [types.user.defaultJobId](state, id) {
    state.defaultJobId = id;
  },

  [types.user.favoriteAccounts](state, accounts: string[]) {
    state.favoriteAccounts = [...accounts];
  },

  [types.user.ratesPlace](state, place) {
    state.ratesPlace = place;
  },

  [types.user.ratesPlaceBranch](state, branchId) {
    state.ratesPlaceBranch = branchId;
    api.user.saveUserPreferences('ratesPlaceBranch', branchId);
  },

  [types.user.placeBranch](state, branchId) {
    state.branchOnly = branchId;
  },

  [types.user.ratesPlaceBranchError](state, error) {
    state.ratesPlaceBranchError = error;
  },

  [types.user.defaultRequesterId](state, id) {
    state.defaultRequesterId = id;
  },

  [types.user.defaultApprover](state, id) {
    state.defaultApproverId = id;
  },

  [types.user.defaultStartDate](state, date) {
    state.defaultStartDate = date;
  },

  [types.user.defaultEndDate](state, date) {
    state.defaultEndDate = date;
  },

  [types.user.setEquipmentColumns](
    state,
    data: any, // todo: set this back to EquipmentColumn, or whatever it should be
  ) {
    state.equipmentColumns = data;
  },

  [types.user.setDefaultAccountId](state, id) {
    if (typeof id !== 'undefined' && id !== null) {
      id = id.toString();
    }
    state.defaultAccountId = id;
  },

  [types.user.setLoggedInUsername](state, loggedInUsername) {
    state.loggedInUsername = loggedInUsername.toString();
  },

  [types.user.setIsLoggedIn](state, isLoggedIn) {
    state.isLoggedIn = isLoggedIn || document.querySelector('body')!.classList.contains('user-logged-in');
  },

  [types.user.setOrderColumns](state, data: WorkplaceOrderColumnSettings[]) {
    state.orderColumns = data;
  },

  [types.user.reset](state) {
    state.defaultAccountId = '';
    state.defaultJobId = '';
    state.defaultRequesterId = '';
    state.defaultApproverId = '';
    state.loggedInUsername = '';
  },

  [types.user.setGeolocation](state, geo) {
    state.geolocation = geo;
  },
};

export default {
  actions,
  getters,
  mutations,
  state,
};
