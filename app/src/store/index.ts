import Vue from 'vue';
import Vuex, { Plugin } from 'vuex';
import VuexPersistence from 'vuex-persist';
import * as LocalForage from 'localforage';
import createPersistedState from 'vuex-persistedstate';

/* Plugins */
import { AppcuesPlugin } from './appcues-plugin';
import { vuexPersistEmitter } from './vuex-persist-plugin';

/* Modules */
import accountingRequisitionCodes from './module.accounting-requisition-codes';
import accountPoNumbers from './module.accounts.po-numbers';
import accounts from './module.accounts';
import allocationCodes from './module.allocation-codes';
import approvers from './module.approvers';
import availability from './module.availability';
import branch from './module.branch';
import businessTypes from './module.business-types';
import breakpoint from './module.breakpoint';
import callUs from './module.call-us';
import cart from './module.cart';
import cartItemForm from './module.cart-item-form';
import cartSaveForLater from './module.save-for-later';
import catalog from './module.catalog';
import checkout from './module.checkout';
import convertQuotes from './module.convert-quotes';
import creditCards from './module.credit-cards';
import estimates from './module.charge-estimates';
import facets from './module.facets';
import favorites from './module.favorites';
import forgotPasswordJourney from './module.forgot-password-journey';
import googleMaps from './module.google-maps';
import inFlightRequests from './module.in-flight-requests';
import invoices from './module.invoices';
import jobs from './module.jobs';
import locationFinder from './module.locationFinder';
import notifications from './module.notifications';
import popularEquipmentTypes from './module.popular-equipment-types';
import quickView from './module.quickView';
import quotes from './module.quotes';
import readReservationDetails from './module.reservation-details';
import recentlyViewed from './module.recently-viewed';
import requesters from './module.requesters';
// import requestPickup from './module.request-pickup';
import requisitions from './module.requisitions';
import reservations from './module.reservations';
import snapshot from './module.snapshot';
import tcUser from './module.tc-user';
import transactions from './module.transactions';
import types from './types';
import urDropdown from './module.urDropdown';
import user from './module.user';
import workplace from './module.workplace';
import workplaceOrderListing from './module.workplace.order-listing';
import workplaceOrderDetails from './module.workplace.order-details';
import workplaceRentalTimeline from './module.workplace.rental-timeline';
import workplaceEquipmentDetails from './module.workplace.equipment-details';
import workplaceEquipmentListing from './module.workplace.equipment-listing';
import workplaceEquipmentLocation from './module.workplace.equipment-location';
import { WorkplaceRescheduleRequestModule } from './module.workplace.reschedule-request';
import pickupDelivery from './module.pickup-delivery';

Vue.use(Vuex);

export const enableSessionStorage = (() => {
  try {
    // Safari Private Browsing will throw an error if we call setItem
    window.sessionStorage.setItem('foo', 'bar');
    return true;
  }
  catch (e) {
    return false;
  }
})();

export const enableAsyncStorage = (() => {
  const supported = 'indexedDB' in window;
  const isManagePage = window.location.pathname.includes('/manage');
  if (!supported) console.warn('This browser doesn\'t support IndexedDB');
  return supported && enableSessionStorage && isManagePage;
})();

const plugins: Plugin<any>[] = [AppcuesPlugin];

const sessionStoragePlugin = createPersistedState({
  key: 'unitedrentals_kelex_2017',
  storage: window.sessionStorage,
  paths: [
    /**
     * Vuex state can be persisted in the browser's session storage by registering a path here.
     * All paths start at the module name level.
     *
     * For example, to persist state.accounts.all (from module.accounts.js) you would register
     * 'accounts.all' here. To persist an entire module's state, simply register the module name.
     * E.g. 'accounts'.
     */
    'accounts.all',
    'accounts.form',
    'accounts.searchCriteria',
    'accounts.searchParams',
    'accountPoNumbers',
    'allocationCodes',
    'approvers',
    'availability',
    'businessTypes',
    'cart.cartId',
    'cart.items',
    'cartItemForm.id',
    'cartItemForm.catClass',
    'cartItemForm.quantity',
    'cartItemForm.accountId',
    'cartItemForm.branchId',
    'cartItemForm.locationString',
    'cartItemForm.jobsiteId',
    'cartItemForm.projectId',
    'cartItemForm.requesterId',
    'cartItemForm.startDate',
    'cartItemForm.endDate',
    'cartItemForm.notes',
    'cartItemForm.delivery',
    'cartItemForm.pickup',
    'catalog.equipment',
    'catalog.rates',
    // Store all checkout state BUT progressOfAllReservationBlocks
    'checkout.billing',
    'checkout.estimates',
    'checkout.hasShownAuthenticateToContinueModal',
    'checkout.transactionResults',
    'checkout.transactionBlocks',
    'checkout.promotionCode',
    'checkout.startUnauthenticatedCheckout',
    'creditCards',
    'estimates',
    'favorites',
    'jobs',
    'recentlyViewed',
    'requesters',
    'tcUser',
    'user.defaultAccountId',
    'user.defaultJobId',
    'user.defaultStartDate',
    'user.defaultEndDate',
    'user.defaultRequesterId',
    'user.geolocation',
    'user.ratesPlace',
    'user.ratesPlaceBranch',
    'user.uid',
    'snapshot',
    'UserPreferences.equipmentColumns',
  ],
  filter(mutation, ...rest) {
    // A runtime switch for dis/enabling storage of vuex.
    // Note: You probably shouldn't make use of this,
    // edge cases only! See LocationFinder page.
    return !!store.state.sessionStoragePersisted;
  },
});

if (enableSessionStorage) {
  plugins.push(sessionStoragePlugin);
}

/**
 * Define the plugin responsible for storing Vuex state in IndexedDB storage.
 *
 * Note that since we're potentially dealing with large amounts of data, and
 * because IndexedDB storage is asynchronous, save events can be relatively
 * expensive (TODO: figure out if that's actualy true). Hence, we fine-tune
 * the save events by filtering them down to individual mutations inside the
 * `filter` function.
 */
const indexedDbPlugin = new VuexPersistence<any>({
  key: 'unitedrentals',
  asyncStorage: true,
  strictMode: true,
  storage: LocalForage,
  filter(mutation) {
    switch (mutation.type) {
      case types.workplace.fetchEquipmentOnRent:
      case types.workplace.setLeniencyRates:
      case types.workplace.setRentalCounts:
      case types.workplace.resetWorkplaceData:
        return true;
      default:
        return false;
    }
  },
  modules: [
    'workplace',
  ],
});

if (enableAsyncStorage) {
  plugins.push(indexedDbPlugin.plugin);
  plugins.push(vuexPersistEmitter());
}

// Setting this config before calling `new Vuex.Store` fixes an issue where devtools doesn't appear the vuex tab (sometimes)
Vue.config.devtools = new RegExp('dev|qa|stage|docksal').test(window.location.host);

// Workplace modules are so expensive they are only included in the
// Vuex Store if the user is on a Workplace page (or request pickup landing)
const workplaceModules = window.location.pathname.includes('/manage') || window.location.pathname.includes('/pickup')
  ? {
    workplace,
    workplaceOrderListing,
    workplaceOrderDetails,
    workplaceRentalTimeline,
    workplaceEquipmentDetails,
    workplaceEquipmentListing,
    workplaceEquipmentLocation,
    workplaceRescheduleRequest: WorkplaceRescheduleRequestModule,
  }
  : {} as any;

export const store = new Vuex.Store({
  plugins,
  strict: false,
  actions: {
    [types.store.clearAllButCart]({ commit }) {
      // console.log('dispatched types.store.clearAllButCart'); // tslint:disable-line
      commit('accounts/reset');
      commit('jobs/reset');
      commit('allocationCodes/reset');
      commit('approvers/reset');
      commit('creditCards/reset');
      commit('requesters/reset');
      commit(types.user.reset);
      commit('checkout/reset');
      commit('tcUser/reset');
      commit(types.workplace.resetWorkplaceData);
    },
    [types.store.clearAll]({ commit, dispatch }) {
      // console.log('dispatched types.store.clearAll'); // tslint:disable-line
      dispatch(types.store.clearAllButCart);
      commit('cart/reset');
    },
  },
  modules: {
    accountingRequisitionCodes,
    accountPoNumbers,
    accounts,
    allocationCodes,
    approvers,
    availability,
    branch,
    businessTypes,
    breakpoint,
    callUs,
    cart,
    cartItemForm,
    cartSaveForLater,
    catalog,
    checkout,
    convertQuotes,
    creditCards,
    estimates,
    facets,
    favorites,
    forgotPasswordJourney,
    googleMaps,
    inFlightRequests,
    invoices,
    jobs,
    pickupDelivery,
    locationFinder,
    notifications,
    popularEquipmentTypes,
    quickView,
    quotes,
    readReservationDetails,
    recentlyViewed,
    requesters,
    requisitions,
    reservations,
    snapshot,
    tcUser,
    transactions,
    urDropdown,
    user,
    ...workplaceModules,
  },
  mutations: {
    /**
     * This is the mutation committed by the vuex-persist plugin on page load.
     * It's responsible for pulling data (if any) out of IndexedDB storage
     * and loading it into state.
     *
     * WARNING: Vuex modules that rely on state stored in IndexedDB MUST wait
     * on the Vuex state's `storageReady` property to be `true` before using it.
     * (so far this only applies to some of the workplace state)
     */
    ['RESTORE_MUTATION'](state, payload) {
      /**
       * Use `.call` here so that `this` remains bound to the RESTORE_MUTATION receiver
       */
      indexedDbPlugin.RESTORE_MUTATION.call(this, state, payload);
      state.storageReady = true;
    },
  },
  state: {
    storageReady: false,
    sessionStoragePersisted: true,
  },
});

export * from './module.workplace';
export * from './module.workplace.reschedule-request';
export { types };
export default store;
