import Vue from 'vue';

import api from '@/api';
import types from '@/store/types';

const state = {

  /**
   * Approvers returned from a api-appliance call, keyed by accountId, and then
   * approverId.
   * This state gets persisted in the browsers Session Storage.
   *
   * Example: state.all['1234']['4321'] is {id: '4321', approverName, approverEmail, approverPhone, ...}
   */
  all: {},
  fetching: false,
};

const actions = {
  [types.approvers.fetch]({ commit, state }, accountId) {
    if (!accountId) {
      return;
    }

    if (Object.keys(state.all[accountId] || {}).length > 0) {
      commit('approvers/fetching', false);
      return Promise.resolve(state.all[accountId]);
    }

    commit('approvers/fetching', true);
    return api.approvers.fetch(accountId)
      .then(({ data }) => {
        commit('approvers/fetching', false);
        if (data === null) {
          const error = new Error(`No approvers data returned for ${accountId}. (Action: types.approvers.fetch)`);
          return Promise.reject(error);
        }
        commit(types.approvers.fetch, { data, accountId });
      });
  },
};

const mutations = {
  [types.approvers.fetch](state, { data, accountId }) {
    if (!state.all[accountId]) {
      Vue.set(state.all, accountId, {});
    }
    data.forEach((approver) => {
      if (!approver.guid) {
        return;
      }

      Vue.set(state.all[accountId], approver.guid, approver);
    });
  },
  'approvers/fetching': function (state, bool) {
    state.fetching = bool;
  },
  'approvers/reset': function (state) {
    state.all = {};
  },
};

export default {
  actions,
  mutations,
  state,
};
