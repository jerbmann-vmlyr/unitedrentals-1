/**
 * Mock Vuex Modules
 *
 * Each file contains mock data for State/Getters/Actions/Mutations as needed.
 * Should you want to modify the mocked data/fn for a particular test, you are
 * encouraged to make the change in your unit test before running it.
 *
 * These mocks are shared across all component tests.
 */

export { default as accounts } from './module.accounts.mocks';
export { default as branch } from './module.branch.mocks';
export { default as cart } from './module.cart.mocks';
export { default as cartItemForm } from './module.cart-item-form.mocks';
export { default as cartSaveForLater } from './module.save-for-later.mocks';
export { default as catalog } from './module.catalog.mocks';
export { default as checkout } from './module.checkout.mocks';
export { default as facets } from './module.facets.mocks';
export { default as inFlightRequests } from './module.in-flight-requests.mocks';
export { default as user } from './module.user.mocks';
