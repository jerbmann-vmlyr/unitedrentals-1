import types from '@/store/types';

export default {
  state: {

  },
  actions: {
    [types.facets.fetch]: jest.fn(),
  },
};
