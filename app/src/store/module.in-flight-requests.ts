import 'reflect-metadata';
import Vue from 'vue';
import types from './types';
import { ActionTree, MutationTree, GetterTree } from 'vuex';
import {
  Tracker,
  TrackerEventType,
  FlightToken,
  TrackerTokenMetadataKey,
} from '@/api/flight-track';

export interface InFlightRequestsState {
  trackers: {
    [K in FlightToken]?: Tracker
  }
  inFlightCounts: {
    [K in FlightToken]?: number;
  }
}

const state: InFlightRequestsState = {
  trackers: {},
  inFlightCounts: {},
};

const actions: ActionTree<InFlightRequestsState, any> = {
  [types.inFlightRequests.registerApisForTracking]({ state, commit }, apis: object[]) {
    const trackers: Tracker[] = apis.reduce<Tracker[]>((list, api) => {
      const tokens: FlightToken[] = Reflect.getMetadata(TrackerTokenMetadataKey, api) || [];
      const matchingTrackers = tokens.map(token => Reflect.getMetadata(token, api));

      if (matchingTrackers.every(t => t instanceof Tracker)) {
        return [...matchingTrackers, ...list];
      }
      else {
        // Find the offending token
        const unimplementedToken = tokens.find(token => !(Reflect.getMetadata(token, api) instanceof Tracker));
        throw new Error(`The "${unimplementedToken}" flight token exists, but it doesn't have a corresponding @TrackFlight in the ${api.constructor.name} module.`);
      }
    }, []);

    // Make sure all of the trackers have unique tokens
    trackers.reduce<FlightToken[]>((seen, { token }) => {
      if (seen.includes(token)) {
        throw new Error(`The flight token "${token}" has been used with more than one Trackable. Tokens can only be used once.`);
      }
      return [...seen, token];
    }, []);

    // Make sure all tokens have been used
    const tokensWithTrackers = trackers.map(tracker => tracker.token);
    Object.values(FlightToken).map(token => {
      if (!tokensWithTrackers.includes(token as FlightToken)) {
        throw new Error(`The FlightToken "${token}" is defined but is not being used!`);
      }
    });

    commit(types.inFlightRequests.setTrackers, trackers);

    // Reset the tracking counts and attach listeners to each tracker
    trackers.map(tracker => {
      const { token } = tracker;

      // Reset the count of each tracker
      commit(types.inFlightRequests.setInFlightCountByToken, { token, count: 0});

      // Thunk a function for commiting changes in in-flight counts
      const applyDelta = (delta: number) => {
        return () => commit(types.inFlightRequests.modifyInFlightCountByToken, { token, delta });
      };

      tracker.on(TrackerEventType.Departure, applyDelta(1));
      tracker.on(TrackerEventType.Arrival, applyDelta(-1));
      tracker.on(TrackerEventType.Crash, applyDelta(-1));
    });
  },
};

const getters: GetterTree<InFlightRequestsState, any> = {
  [types.inFlightRequests.tokensInFlight]: (state): FlightToken[] => {
    return Object.entries(state.inFlightCounts)
      .filter(([, count]) => typeof count === 'number' && count > 0)
      .map(([token]) => token as FlightToken);
  },
  [types.inFlightRequests.tokensAreInFlight]: state => (...tokens: FlightToken[]): boolean => {
    const currentlyTrackedTokens = Object.keys(state.trackers);
    // Make sure the tokens to be tracked are registered
    tokens.map(token => {
      if (!currentlyTrackedTokens.includes(token)) {
        console.warn(`Cannot track the flight of "${
          token
        }" because it's Tracker is not registered. These are the trackers I know about: ${
          currentlyTrackedTokens.map(key => `- ${key}`).join('\n')
        }`);
      }
    });
    const inFlightCountSum = tokens.reduce((sum, token) => sum + state.inFlightCounts[token]!, 0);
    return inFlightCountSum > 0;
  },
};

const mutations: MutationTree<InFlightRequestsState> = {
  [types.inFlightRequests.modifyInFlightCountByToken](state, payload: { token: FlightToken, delta: number }) {
    const currentCount = state.inFlightCounts[payload.token] || 0;
    Vue.set(state.inFlightCounts, payload.token, Math.max(currentCount + payload.delta, 0));
  },
  [types.inFlightRequests.setInFlightCountByToken](state, payload: { token: FlightToken, count: number }) {
    Vue.set(state.inFlightCounts, payload.token, Math.max(payload.count, 0));
  },
  [types.inFlightRequests.setTrackers](state, trackers: Tracker[]) {
    trackers.map(tracker => Vue.set(state.trackers, tracker.token, tracker));
  },
};

export default {
  actions,
  state,
  getters,
  mutations,
};
