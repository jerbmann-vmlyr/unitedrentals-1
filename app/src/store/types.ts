export default {
  store: {
    clearAllButCart: 'store/clearAllButCart',
    clearAll: 'store/clearAll',
  },
  accounts: {
    /* Actions */
    fetch: 'accounts/fetch',
    resetForm: 'accounts/resetForm',
    resetSearchResultsAndCriteria: 'accounts/resetSearchResultsAndCriteria',
    saveActiveSearch: 'accounts/saveActiveSearch',
    saveForm: 'accounts/saveForm',
    searchAccounts: 'accounts/searchAccounts',
    setSelectedAccountId: 'accounts/setSelectedAccountId',
    updateSearchCriteriaEnabled: 'accounts/updateSearchCriteriaEnabled',
    updateSearchCriteriaValue: 'accounts/updateSearchCriteriaValue',
    updateFormField: 'accounts/updateFormField',
    updateTcUserFormField: 'accounts/updateTcUserFormField',

    /* Getters */
    exist: 'accounts/exist',
    fetchRequestPending: 'accounts/fetchRequestPending',
    firstAccount: 'accounts/firstAccount',
    isValidAccountId: 'accounts/isValidAccountId',
    noAccounts: 'accounts/noAccounts',
    searchParams: 'accounts/searchParams',
    searchRequestPending: 'accounts/searchRequestPending',
    selectableAccounts: 'accounts/selectableAccounts',

    /* Mutations */
    saveAccounts: 'accounts/saveAccounts',
    saveAccountSearchCriteria: 'accounts/saveAccountSearchCriteria',
    saveSearchResults: 'accounts/saveSearchResults',
    setAccountsFetched: 'accounts/setAccountsFetched',
    setLatestSuccessfulSearchParams: 'accounts/setLatestSuccessfulSearchParams',
    setSortColumnKey: 'accounts/setSortColumnKey',
    setSortAscending: 'accounts/setSortAscending',
    toggleFavorite: 'accounts/toggleFavorite',
  },
  accountPoNumbers: {
    fetchNumbers: 'accountPoNumbers/fetchNumbers',
    fetchFormats: 'accountPoNumbers/fetchFormats',
    reset: 'accountPoNumbers/reset',

    /* getters */
    getPoNumbersByAccountId: 'accountPoNumbers/getPoNumbersByAccountId',
  },
  allocationCodes: {
    _fetch: 'allocationCodes/_fetch',
    fetchAllocationCodes: 'allocationCodes/fetchAllocationCodes',
    fetchRequisitionCodes: 'allocationCodes/fetchRequisitionCodes',
  },
  approvers: {
    fetch: 'approvers/fetch',
  },
  availability: {
    fetch: 'availability/fetch',
  },
  branch: {
    show: 'branch/show',
    fetch: 'branch/fetch',
    fetchById: 'branch/fetchById',
  },
  breakpoint: {
    watch: 'breakpoint/watch',
  },
  businessTypes: {
    fetch: 'businessTypes/fetch',
  },
  pickupDelivery: {
    select: 'pickupDelivery/select',
    options: 'pickupDelivery/options',
    selected: 'pickupDelivery/selected',
  },
  cart: {
    /* getters */
    fetching: 'cart/fetching',
    isLocal: 'cart/isLocal',

    /* actions(/mutations) */
    addItem: 'cart/addItem',
    fetch: 'cart/fetch',
    fetchEquipment: 'cart/fetchEquipment',
    fixHydrationFromVuexPersistedState: 'cart/fixHydrationFromVuexPersistedStatePlugin',
    removeItem: 'cart/removeItem',
    saveItemsByReservationId: 'cart/saveItemsByReservationId',
    saveLocalCart: 'cart/saveLocalCart',
    saveAnonymousCart: 'cart/saveAnonymousCart',
    updateAllItems: 'cart/updateAllItems',
    updateItemsByReservationId: 'cart-updateItemsByReservationId', // DO NOT REPLACE `-` with `/`. You'll break the callback for Create-Jobsite in Reservation Blocks
    updateItem: 'cart/updateItem',
    setConfirmed: 'cart/setConfirmed',
    setRpp: 'cart/setRpp',

    /* Save For Later */
    saveForLater: {
      fetch: 'cart/saveForLater/fetch',
      addItem: 'cart/saveForLater/addItem',
      fetchEstimates: 'cart/saveForLater/fetchEstimates',
      moveToCart: 'cart/saveForLater/moveToCart',
      removeItem: 'cart/saveForLater/removeItem',
      save: 'cart/saveForLater/save',
      updateItem: 'cart/saveForLater/updateItem',
    },
  },
  cartItemForm: {
    decrement: 'cartItemForm/decrement',
    increment: 'cartItemForm/increment',
    quantity: 'cartItemForm/quantity',

    loadCartItem: 'cartItemForm/loadCartItem',
    loadDefaults: 'cartItemForm/loadDefaults',

    updateActive: 'cartItemForm/updateActive',
    updateFormField: 'cartItemForm/updateFormField',
  },
  catalog: {
    currentEquipment: 'catalog/currentEquipment',
    equipment: 'catalog/equipment',
    fetchEquipment: 'catalog/fetchEquipment',
    fetchEquipmentByCatClass: 'catalog/fetchEquipmentByCatClass',
    fetchCategoricalEquipment: 'catalog/fetchCategoricalEquipment',
    fetchRates: 'catalog/fetchRates',
    fetchingRates: 'catalog/fetchingRates',
  },
  checkout: {
    billingAccountingCodes: 'checkout/billing/accountingCodes',
    billingApprover: 'checkout/billing/approver',
    billingCreditCard: 'checkout/billing/creditCard',
    billingDeliveryNote: 'checkout/billing/deliveryNote',
    billingPoNumber: 'checkout/billing/poNumber',
    billing: {
      reset: 'checkout/billing/reset',
    },
    billingRequester: 'checkout/billing/requester',
    billingRequisitionCodes: 'checkout/billing/requisitionCodes',
    billingRpp: 'checkout/billing/rpp',
    billingTransType: 'checkout/billing/transType',
    copyReservationBlockToTransactionBlock: 'checkout/copyReservationBlockToTransactionBlock',
    createAccountAndJobsite: 'checkout/createAccountAndJobsite',
    createTransaction: 'checkout/createTransaction',
    failedJobsiteCreation: 'checkout/failedJobsiteCreation',
    fetchEstimates: 'checkout/fetchEstimates',
    fetchProjectedEstimates: 'checkout/fetchProjectedEstimates',
    promotionCodeActive: 'checkout/promotionCodeActive',
    startUnauthenticatedCheckout: 'checkout/startUnauthenticatedCheckout',
    setSubmitting: 'checkout/setSubmitting',
    setHideOverlayImmediately: 'checkout/setHideOverlayImmediately',
    setButtonClickType: 'checkout/setButtonClickType',
    updatePromotionCode: 'checkout/updatePromotionCode',
    updateReservationBlockProgress: 'checkout/updateReservationBlockProgress',
    hasShownAuthenticateToContinueModal: 'checkout/hasShownAuthenticateToContinueModal',

    /* getters */
    getEstimate: 'checkout/getEstimate',
    hasEstimate: 'checkout/hasEstimate',
    progressCounts: 'checkout/progressCounts',

    /* actions */
    getUpdateReservationAction: 'checkout/getUpdateReservationAction',
    toggleSubmitting: 'checkout/toggleSubmitting',
    toggleHideOverlayImmedately: 'checkout/toggleHideOverlayImmedately',
    dispatchButtonClickType: 'checkout/dispatchButtonClickType',
  },
  convertQuotes: {
    jobsiteOutDated: 'convertQuotes/jobsiteOutDated',
    setJobsiteOutDated: 'convertQuotes/setJobsiteOutDated',
    setNewJobsiteId: 'convertQuotes-setNewJobsiteId', // DO NOT REPLACE `-` with `/`. You'll break the callback for Create-Jobsite in Cart Item Forms
    dateError: 'converQuotes/dateError',
    setDateError: 'converQuotes/setDateError',
    saveJobsite: 'types.convertQuotes.saveJobsite',
    billingCreditCard: 'types.convertQuotes.billingCreditCard',
    sendQuote: 'types.convertQuotes.sendQuote',
    setConvertSuccess: 'types.convertQuotes.setConvertSuccess',
    defaultJobId: 'user-defaultJobId', // DO NOT REPLACE `-` with `/`. You'll break the callback for Create-Jobsite in Cart Item Forms
  },
  creditCards: {
    fetch: 'creditCards/fetch',
    add: 'creditCards/add',
    update: 'creditCards/update',
    remove: 'creditCards/remove',
  },
  facets: {
    categoryByCatClass: 'facets/categoryByCatClass',
    collapse: 'facets/collapse',
    createContextualFilter: 'facets/createContextualFilter',
    createEquipmentFilter: 'facets/createEquipmentFilter',
    fetch: 'facets/fetch',
    fetchDescription: 'facets/fetchDescription',
    fetchBreadcrumbs: 'facets/fetchBreadcrumbs',
    removeFilter: 'facets/removeFilter',
    removeAllFilters: 'facets/removeAllFilters',
    toggle: 'facets/toggle',
  },
  favorites: {
    add: 'favorites/add',
    favoriteRequestsPending: 'favorites/favoriteRequestsPending',
    fetchAll: 'favorites/fetchAll',
    fetchList: 'favorites/fetchList',
    remove: 'favorites/remove',
    /* Mutations */
    setFavorites: 'favorites/setFavorites',
  },
  forgotPasswordJourney: {
    writeLocalStorage: 'forgotPasswordJourney/writeLocalStorage',
    destroyLocalStorage: 'forgotPasswordJourney/destroyLocalStorage',
    getStateFromLocalStorage: 'forgotPasswordJourney/getStateFromLocalStorage',
  },
  googleMaps: {
    /* Actions */
    getPlaceFromAddress: 'googleMaps/getPlaceFromAddress',
    geocodeFromAddress: 'googleMaps/geocodeFromAddress',
    getQueryPrediction: 'googleMaps/getQueryPrediction',
    getPlaceDetails: 'googleMaps/getPlaceDetails',
    getGeocode: 'googleMaps/getGeocode',
    /* Getters */
    /* Mutations */
    savePlace: 'googleMaps/savePlace',
  },
  inFlightRequests: {
    /* Actions */
    registerApisForTracking: 'inFlightRequests/registerApisForTracking',
    /* Getters */
    tokensAreInFlight: 'inFlightRequests/tokensAreInFlight',
    tokensInFlight: 'inFlightRequests/tokensInFlight',
    /* Mutations */
    setTrackers: 'inFlightRequests/setTrackers',
    setInFlightCountByToken: 'inFlightRequests/setInFlightCountByToken',
    modifyInFlightCountByToken: 'inFlightRequests/modifyInFlightCountByToken',
  },
  invoices: {
    /* Actions */
    fetch: 'invoices/fetch',

    /* Mutations */
    update: 'invoices/update',

    /* Getters */
    getLastInvoiceForItem: 'invoices/getLastInvoiceForItem',
  },
  jobs: {
    addJobsite: 'jobs/addJobsite',
    fetch: 'jobs/fetch',
    formError: 'jobs/formError',
    loadForm: 'jobs/loadForm',
    reloadForm: 'jobs/reloadForm',
    resetForm: 'jobs/resetForm',
    saveForm: 'jobs/saveForm',
    setClosestBranchToJobsite: 'jobs/setClosestBranchToJobsite',
    storeJobsite: 'jobs/storeJobsite',
    toggleFormError: 'jobs/toggleFormError',
    updateFormField: 'jobs/updateFormField',

    /* getters */
    createJobFormIsValid: 'jobs/createJobFormIsValid',
    getJobsiteCountOnAccount: 'jobs/getJobsiteCountOnAccount',
  },
  locationFinder: {
    activateBranchDetails: 'locationFinder/activateBranchDetails',
    appendBranchData: 'locationFinder/appendBranchData',
    cacheSearch: 'locationFinder/cacheSearch',
    enterMobileListView: 'locationFinder/enterMobileListView',
    enterMobileMapView: 'locationFinder/enterMobileMapView',
    getByBranchIndex: 'locationFinder/getByBranchIndex',
    getNearbyBranches: 'locationFinder/getNearbyBranches',
    load: 'locationFinder/load',
    loadStateBounds: 'locationFinder/loadStateBounds',
    nearbyBranches: 'locationFinder/nearbyBranches',
    pagingNext: 'locationFinder/pagingNext',
    pagingPrev: 'locationFinder/pagingPrev',
    previousRoute: 'locationFinder/previousRoute',
    resetActiveBranch: 'locationFinder/resetActiveBranch',
    resetBranchesLoaded: 'locationFinder/resetBranchesLoaded',
    saveNearbyBranches: 'locationFinder/saveNearbyBranches',
    setBranchMarkerGroups: 'locationFinder/setBranchMarkerGroups',
    setMarkerCluster: 'locationFinder/setMarkerCluster',
    setMarkers: 'locationFinder/setMarkers',
    setMarkerToSelectedState: 'locationFinder/setMarkerToSelectedState',
    setFilter: 'locationFinder/setFilter',
    setLoading: 'locationFinder/setLoading',
    setMap: 'locationFinder/setMap',
    setMapObject: 'locationFinder/setMapObject',
    setPaging: 'locationFinder/setPaging',
    setPlace: 'locationFinder/place',
    setPreviousRoute: 'locationFinder/setPreviousRoute',
    setSearch: 'locationFinder/setSearch',
    setStateMarkerGroups: 'locationFinder/setStateMarkerGroups',
    setSync: 'locationFinder/setSync',
    setVisibleMarkers: 'locationFinder/setVisibleMarkers',
    updateContentType: 'locationFinder/updateContentType',
    updateVisibleMarkers: 'locationFinder/updateVisibleMarkers',
    updateMarkerClusterMarkers: 'locationFinder/updateMarkerClusterMarkers',
    updateMarkerClusterStateMarkers: 'locationFinder/updateMarkerClusterStateMarkers',
    updateSearchDistances: 'locationFinder/updateSearchDistances',
    updateSearchMarker: 'locationFinder/updateSearchMarker',
    updateSearchQuery: 'locationFinder/updateSearchQuery',
  },
  notifications: {
    fetch: 'notifications/fetch',
  },
  popularEquipmentTypes: {
    fetch: 'popularEquipmentTypes/fetch',
    update: 'popularEquipmentTypes/update',
  },
  quickView: {
    add: 'quickView/add',
    remove: 'quickView/remove',
    toggle: 'quickView/toggle',
  },
  recentlyViewed: {
    fetch: 'recentlyViewed/fetch',
    fetchAll: 'recentlyViewed/fetchAll',
    fetchEquipment: 'recentlyViewed/fetchEquipment',
  },
  requesters: {
    fetch: 'requesters/fetch',
  },
  requestPickup: {
    fetchRequestPickupData: 'requestPickup/fetchRequestPickupData',
    submitPickupRequest: 'requestPickup/submitPickupRequest',
    selectAllEquipment: 'requestPickup/selectAllEquipment',
    setSelectedEquipmentQtys: 'requestPickup/setSelectedEquipmentQtys',
    setSelectedEquipmentQtyById: 'requestPickup/setSelectedEquipmentQtyById',
    setSelectedDateTime: 'requestPickup/setSelectedDateTime',
    pickupRequestRequest: 'requestPickup/pickupRequestRequest',
    setEquipmentSelectedById: 'requestPickup/setEquipmentSelectedById',
    initializePickupRequest: 'requestPickup/initializePickupRequest',
    setPickupAddressToJobsiteAddress: 'requestPickup/setPickupAddressToJobsiteAddress',
    setPickupContactToJobsiteContact: 'requestPickup/setPickupContactToJobsiteContact',
    isPickup: 'requestPickup/isPickup',
    branchHoursOffset: 'requestPickup/branchHoursOffset',
    originalStartDateTime: 'requestPickup/originalStartDateTime',
    originalReturnDateTime: 'requestPickup/originalReturnDateTime',
    unselectAllEquipment: 'requestPickup/unselectAllEquipment',
    saveEmailRecipients: 'requestPickup/saveEmailRecipients',
    accountId: 'requestPickup/accountId',
    accountName: 'requestPickup/accountName',
    allEquipmentIsSelected: 'requestPickup/allEquipmentIsSelected',
    atLeastOneEquipmentIdSelected: 'requestPickup/atLeastOneEquipmentIdSelected',
    equipmentListIsEmpty: 'requestPickup/equipmentListIsEmpty',
    reset: 'requestPickup/reset',
    initializeRescheduleRequest: 'requestPickup/initializeRescheduleRequest',
    equipmentForCurrentAccount: 'requestPickup/equipmentForCurrentAccount',
    selectedEquipment: 'requestPickup/selectedEquipment',
    jobsite: 'requestPickup/jobsite',
    transId: 'requestPickup/transId',
    saveAddress: 'requestPickup/saveAddress',
    saveContact: 'requestPickup/saveContact',
    saveInstructions: 'requestPickup/saveInstructions',
    /* Mutations */
    mutate: 'requestPickup/mutate',
  },
  reservations: {
    addFilter: 'reservations/addFilter',
    fetch: 'reservations/fetch',
    reservationsCount: 'reservations/reservationsCount',
    updateReservations: 'reservations/updateReservations',
    filters: 'reservations/filters',
    loading: 'reservations/loading',
    removeAllFilters: 'reservations/removeAllFilters',
    removeFilter: 'reservations/removeFilter',
    updateViewFilter: 'reservations/updateViewFilter',
    updateViewSort: 'reservations/updateViewSort',
  },
  quotes: {
    addFilter: 'quotes/addFilter',
    fetch: 'quotes/fetch',
    quotesCount: 'quotes/quotesCount',
    fetchEstimates: 'quotes/fetchEstimates',
    loading: 'quotes/loading',
    removeAllFilters: 'quotes/removeAllFilters',
    removeFilter: 'quotes/removeFilter',
    reset: 'quotes/reset',
    filters: 'quotes/filters',
    errors: 'quotes/errors',
    convertQuote: 'quotes/convertQuoteToCart',
    updateQuote: 'quotes/updateQuote',
    removeQuote: 'quotes/removeQuote',
    newBranchAndDatesUpdate: 'quotes/newBranchAndDatesUpdate',
    resetBranchAndDatesUpdate: 'quotes/resetBranchAndDatesUpdate',
    setNewJobsiteId: 'quotes-setNewJobsiteId', // DO NOT REPLACE `-` with `/`. You'll break the callback for Create-Jobsite in Cart Item Forms
    fetchingQuotesList: 'quotes/fetchingQuotesList',
    fetchingQuotesDetails: 'quotes/fetchingQuotesDetails',
    fetchingQuotesEstimates: 'quotes/fetchingQuotesEstimates',
  },
  readReservationDetails: {
    fetch: 'readReservationDetails/fetch',
    loading: 'readReservationDetails/loading',
    updateFormField: 'readReservationDetails/updateFormField',
    saveFormDateTime: 'readReservationDetails/saveFormDateTime',
    updateAccountId: 'readReservationDetails/updateAccountId',
    updateReservationFields: 'readReservationDetails/updateReservationFields',
    currentReservation: 'readReservationDetails/currentReservation',
    saveFormQuantity: 'readReservationDetails/saveFormQuantity',
    decrement: 'readReservationDetails/decrement',
    increment: 'readReservationDetails/increment',
    quantity: 'readReservationDetails/quantity',
    updateJobsiteInfoOrTransactionJobId: 'readReservationDetails/updateJobsiteInfoOrTransactionJobId',
  },
  requisitions: {
    update: 'requisitions/update',
    updateBoth: 'requisitions/updateBoth',
  },
  tcUser: {
    create: 'tcUser/create',
    fetch: 'tcUser/fetch',
    fetchOne: 'tcUser/fetchOne',
    reset: 'tcUser/reset',
    save: 'tcUser/save',
    updateDetails: 'tcUser/updateDetails',
    updateFormField: 'tcUser/updateFormField',
  },
  transactions: {
    /* Getters */
    fetchTransactionRequestsPending: 'transactions/fetchTransactionRequestsPending',
    closedContracts: 'transactions/closedContracts',
    openContracts: 'transactions/openContracts',
    quotes: 'transactions/quotes',
    reservations: 'transactions/reservations',
    /* Actions + Mutations */
    fetch: 'transactions/fetch',
    /* Mutations */
    reset: 'transactions/reset',
    saveRequisitions: 'transactions/saveRequisitions',
  },
  accountingRequisitionCodes: {
    fetch: 'accounting-requisition-codes/fetch',
    loading: 'accounting-requisition-codes/loading',
  },
  estimates: {
    fetch: 'estimates/fetch',
    fetchOnly: 'estimates/fetchOnly',
    loading: 'estimates/loading',
    setEmpty: 'estimates/setEmpty',
    fetchDefaultVal: 'estimates/fetchDefaultVal',
    empty: 'estimates/empty',
  },
  user: {
    /* Actions */
    addFavoriteAccount: 'user/addFavoriteAccount',
    clearRatesPlace: 'user/clearRatesPlace',
    clearRatesPlaceBranch: 'user/clearRatesPlaceBranch',
    clearDefaultJobId: 'user/clearDefaultJobId',
    defaultJobId: 'user-defaultJobId', // DO NOT REPLACE `-` with `/`. You'll break the callback for Create-Jobsite in Cart Item Forms
    defaultStartDate: 'user/defaultStartDate',
    defaultEndDate: 'user/defaultEndDate',
    defaultRequesterId: 'user/defaultRequesterId',
    defaultApprover: 'user/defaultApprover',
    logout: 'user/logout',
    ratesPlace: 'user/ratesPlace',
    ratesPlaceBranch: 'user/ratesPlaceBranch',
    branchOnly: 'user/branchOnly',
    placeBranch: 'user/placeBranch',
    ratesPlaceBranchError: 'user/ratesPlaceBranchError',
    setCardIdForRemoval: 'user/setCardIdForRemoval',
    setIsLoggedIn: 'user/setIsLoggedIn',
    cardToRemove: 'user/cardToRemove',
    getGeolocation: 'user/getGeolocation',
    fetchEquipmentColumns: 'user/fetchEquipmentColumns',
    fetchOrderColumns: 'user/fetchOrderColumns',
    fetchDefaultAccountId: 'user/fetchDefaultAccountId',
    fetchLoggedInUsername: 'user/fetchLoggedInUsername',
    removeFavoriteAccount: 'user/removeFavoriteAccount',
    resetEquipmentColumnsToDefault: 'user/resetEquipmentColumnsToDefault',
    resetOrderColumnsToDefault: 'user/resetOrderColumnsToDefault',
    updateDefaultAccountId: 'user/updateDefaultAccountId',
    updateEquipmentColumn: 'user/updateEquipmentColumn',
    updateEquipmentColumns: 'user/updateEquipmentColumns',
    updateOrderColumn: 'user/updateOrderColumn',
    updateOrderColumns: 'user/updateOrderColumns',
    reset: 'user/reset',
    setUid: 'user/setUid',
    /* Mutations */
    favoriteAccounts: 'user/favoriteAccounts',
    setEquipmentColumns: 'user/setEquipmentColumns',
    setDefaultAccountId: 'user/setDefaultAccountId',
    setLoggedInUsername: 'user/setLoggedInUsername',
    setOrderColumns: 'user/setOrderColumns',
    setGeolocation: 'user/setGeolocation',

    /* Getters */
    defaultAccount: 'user/defaultAccount',
    defaultJob: 'user/defaultJob',
    ratesPlaceAddress: 'user/ratesPlaceAddress',
    ratesPlaceCountryCode: 'user/ratesPlaceCountryCode',
    ratesPlaceCreated: 'user/ratesPlaceCreated',
    getLoggedInUsername: 'user/getLoggedInUsername',
  },
  snapshot: {
    take: 'snapshot/take',
    replay: 'snapshot/replay',
    clear: 'snapshot/clear',
  },
  workplace: {
    /* Actions + Mutations */
    toggleListingLayout: 'workplace/toggleListingLayout',
    fetchEquipmentOnRent: 'workplace/fetchEquipmentOnRent',
    fetchEquipmentOnRentCallCompleted: 'workplace/fetchEquipmentOnRentCallCompleted',
    fetchLeniencyRates: 'workplace/fetchLeniencyRates',
    fetchRentalCounts: 'workplace/fetchRentalCounts',
    resetWorkplaceData: 'workplace/resetData',
    setLeniencyRates: 'workplace/setLeniencyRates',
    setRentalCounts: 'workplace/setRentalCounts',
    updateEquipmentPo: 'workplace/updateEquipmentPo',
    rentalCountsInFlight: 'workplace.rentalCountsInFlight',

    /* Getters */
    collectionCatClasses: 'workplace/collectionCatClasses',
    groupers: 'workplace/groupers',
    sorters: 'workplace/sorters',
    filters: 'workplace/filters',
    columns: 'workplace/columns',
    allListingResultsRoute: 'workplace/allListingResultsRoute',
    collectionColumnVisibilityRouteName: 'workplace/collectionColumnVisibilityRouteName',
    detailPage: 'workplace/detailPage',
    listingPage: 'workplace/listingPage',
    collectionForCurrentAccount: 'workplace/collectionForCurrentAccount',
    accountIdIsValid: 'workplace/accountIdIsValid',
    accountId: 'workplace/accountId',
    currentAccountHasEquipment: 'workplace/currentAccountHasEquipment',
    currentWorkplaceCollectionType: 'workplace/currentWorkplaceCollectionType',
    equipmentForCurrentAccount: 'workplace/equipmentForCurrentAccount',
    equipmentForCurrentAccountHasBeenFetched: 'workplace/equipmentForCurrentAccountHasBeenFetched',
    equipmentLexicon: 'workplace/equipmentLexicon',
    equipmentOnRentRequestsPending: 'workplace/equipmentOnRentRequestsPending',
    rentalCountsForCurrentAccount: 'workplace/rentalCountsForCurrentAccount',
    timeOfCall: 'workplace/timeOfCall',
    listingLayout: 'workplace/listingLayout',
    collectionFilters: 'workplace/collectionFilters',
    /* Orders */
    fetchOrders: 'workplace/fetchOrders',
    ordersLexicon: 'workplace/ordersLexicon',
    ordersRequestPending: 'workplace/ordersRequestPending',
    ordersForCurrentAccount: 'workplace/ordersForCurrentAccount',
    fetchOrdersCallCompleted: 'workplace/fetchOrdersCallCompleted',
    ordersForCurrentAccountHasBeenFetched: 'workplace/ordersForCurrentAccountHasBeenFetched',
    /* Mutations */
    // setSelectedAccountId: 'workplace/setSelectedAccountId',

    /* Submodule */
    equipmentDetails: {
      /* Actions */
      fetchCatClassByEquipmentId: 'workplace/equipmentDetails/fetchCatClassByEquipmentId',
      fetchItemCoordinates: 'workplace/equipmentDetails/fetchItemCoordinates',
      fetchItemUsage: 'workplace/equipmentDetails/fetchItemUsage',
      getEstimates: 'workplace/equipmentDetails/getEstimates',

      /* Getters */
      detailItemOrNull: 'workplace/equipmentDetails/detailItemOrNull',
      estimatesOrNull: 'workplace/equipmentDetails/estimatesOrNull',
      fetchLeniencyRatesRequestOrNull: 'workplace/equipmentDetails/fetchLeniencyRatesRequestOrNull',
      leniencyRates: 'workplace/equipmentDetails/leniencyRates',
      orderStatuses: 'workplace/equipmentDetails/orderStatuses',
      reservationDetailsOrNull: 'workplace/equipmentDetails/reservationDetailsOrNull',
      resetItemCoordinates: 'workplace/equipmentDetails/resetItemCoordinates',
      resetItemUsage: 'workplace/equipmentDetails/resetItemUsage',

      /* Mutations */
      detailItem: 'workplace/workplaceDetails/detailItem',
      setDetailItemCoordinatesPending: 'workplace/equipmentDetails/setDetailItemCoordinatesPending',
      setItemCoordinates: 'workplace/equipmentDetails/setItemCoordinates',
      setItemUsage: 'workplace/equipmentDetails/setItemUsage',
    },

    /* Submodule */
    equipmentListing: {
      /* Getters */
      currentPage: 'workplace/equipmentListing/currentPage',
      filters: 'workplace/equipmentListing/filters',
      grouper: 'workplace/equipmentListing/grouper',
      sorter: 'workplace/equipmentListing/sorter',
      sortDirection: 'workplace/equipmentListing/sortDirection',
      filteredEquipment: 'workplace/equipmentListing/filteredEquipment',
      groupedEquipment: 'workplace/equipmentListing/groupedEquipment',
      sortedEquipment: 'workplace/equipmentListing/sortedEquipment',
    },

    /* Submodule */
    orderDetails: {
      detailItemOrNull: 'workplace/orderDetails/detailItemOrNull',
      detailItem: 'workplace/orderDetails/detailItem',
    },

    /* Submodule */
    orderListing: {
      /* Getters */
      totals: 'workplace/orderListing/totals',
      sorter: 'workplace/orderListing/sorter',
      grouper: 'workplace/orderListing/grouper',
      filters: 'workplace/orderListing/filters',
      currentPage: 'workplace/orderListing/currentPage',
      sortDirection: 'workplace/orderListing/sortDirection',
      sortedResources: 'workplace/orderListing/sortedResources',
      groupedResources: 'workplace/orderListing/groupedResources',
      filteredResources: 'workplace/orderListing/filteredResources',
    },

    /* Submodule */
    equipmentLocation: {
      /* Actions */
      getJobsiteLocationsFromEquipment: 'workplace/equipmentLocation/getJobsiteLocationsFromEquipment',

      /* Getters */
      jobsiteLocationsForCurrentAccount: 'workplace/equipmentLocation/jobsiteLocationsForCurrentAccount',
      equipmentWithValidJobsiteLocations: 'workplace/equipmentLocation/equipmentWithValidJobsiteLocations',
      jobsiteClusters: 'workplace/equipmentLocation/jobsiteClusters',
      jobsiteMapMarkersFromStatuses: 'workplace/equipmentLocation/jobsiteMapMarkersFromStatuses',

      /* Mutations */
      saveJobsiteLocation: 'workplace/equipmentLocation/saveJobsiteLocation',
    },

    /* Submodule */
    rentalTimeline: {
      /* Actions */
      update: 'workplace/rentalTimeline/update',
      reset: 'workplace/rentalTimeline/reset',
      /* Getters */
      /* Mutations */
    },

    /* Submodule */
    rescheduleRequest: {
      /* Actions */
      fetchLeniencyRates: 'workplace/rescheduleRequest/fetchLeniencyRates',
      initializeRescheduleRequest: 'workplace/rescheduleRequest/initializeRescheduleRequest',
      populateEquipmentListFromTransIds: 'workplace/rescheduleRequest/populateEquipmentListFromTransIds',
      reset: 'workplace/rescheduleRequest/reset',
      saveAddress: 'workplace/rescheduleRequest/saveAddress',
      saveContact: 'workplace/rescheduleRequest/saveContact',
      saveEmailRecipients: 'workplace/rescheduleRequest/saveEmailRecipients',
      saveInstructions: 'workplace/rescheduleRequest/saveInstructions',
      selectAllEquipment: 'workplace/rescheduleRequest/selectAllEquipment',
      setEquipmentSelectedById: 'workplace/rescheduleRequest/setEquipmentSelectedById',
      setSelectedEquipmentQtys: 'workplace/rescheduleRequest/setSelectedEquipmentQtys',
      setSelectedEquipmentQtyById: 'workplace/rescheduleRequest/setSelectedEquipmentQtyById',
      setPickupAddressToJobsiteAddress: 'workplace/rescheduleRequest/setPickupAddressToJobsiteAddress',
      setPickupContactToJobsiteContact: 'workplace/rescheduleRequest/setPickupContactToJobsiteContact',
      setSelectedDateTime: 'workplace/rescheduleRequest/setSelectedDateTime',
      submitExtensionRequest: 'workplace/rescheduleRequest/submitExtensionRequest',
      submitPickupRequest: 'workplace/rescheduleRequest/submitPickupRequest',
      unselectAllEquipment: 'workplace/rescheduleRequest/unselectAllEquipment',

      /* Getters */
      accountId: 'workplace/rescheduleRequest/accountId',
      accountName: 'workplace/rescheduleRequest/accountName',
      allEquipmentIsSelected: 'workplace/rescheduleRequest/allEquipmentIsSelected',
      atLeastOneEquipmentIdSelected: 'workplace/rescheduleRequest/atLeastOneEquipmentIdSelected',
      branchHoursOffset: 'workplace/rescheduleRequest/branchHoursOffset',
      equipmentListIsEmpty: 'workplace/rescheduleRequest/equipmentListIsEmpty',
      getEquipmentQtys: 'workplace/rescheduleRequest/getEquipmentQtys',
      jobsite: 'workplace/rescheduleRequest/jobsite',
      isExtension: 'workplace/rescheduleRequest/isExtension',
      isPickup: 'workplace/rescheduleRequest/isPickup',
      originalReturnDateTime: 'workplace/rescheduleRequest/originalReturnDateTime',
      originalStartDateTime: 'workplace/rescheduleRequest/originalStartDateTime',
      rescheduleType: 'workplace/equipmentDetails/rescheduleType',
      selectedEquipment: 'workplace/rescheduleRequest/selectedEquipment',
      pickupRequestRequest: 'workplace/rescheduleRequest/pickupRequestRequest',
      transId: 'workplace/rescheduleRequest/transId',
      transLineId: 'workplace/rescheduleRequest/transLineId',

      /* Mutations */
      mutate: 'workplace/rescheduleRequest/mutate',
    },

  },
  urDropdown: {
    setActiveId: 'urDropdown/setActiveId',
  },
};
