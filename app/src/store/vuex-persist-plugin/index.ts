import { Store, Plugin } from 'vuex';
import Vue from 'vue';

/**
 * Define a plugin that emits an event when the state from IndexedDB
 * has been retrieved and restored in the Vuex state
 */
export const vuexPersistEmitter = <S>() => {
  return ((store: Store<S> & { _vm: Vue }) => {
    store.subscribe(mutation => {
      if (mutation.type === 'RESTORE_MUTATION') {
        store._vm.$root.$emit('storageReady');
      }
    });
  }) as Plugin<S>;
};
