import types from '@/store/types';

export default {
  actions: {
    [types.cartItemForm.updateActive]: jest.fn(),
  },
};
