import CartModule from './module.cart';
import CartItem from '@/lib/cartItem';
import * as moment from 'moment';


/*
See module.checkout.mocks.ts for the predefined Estimates that
pair with this item.
*/
export const item1 = new CartItem({
  accountId: '',
  branchId: '639',
  catClass: '310-9801',
  changed: false,
  endDate: moment('2019-01-25T00:00:00'),
  id: 1,
  jobsiteId: '',
  locationString: 'Kansas City, MO 64118',
  notes: 'Hello',
  projectId: '',
  quantity: 1,
  requesterId: '',
  startDate: moment('2019-01-24T00:00:00'),
  delivery: false,
  pickup: false,
});

export default {
  state: {
    items: {},
    cartId: '',
    toastItem: {},
    confirmed: false,
  },
  getters: CartModule.getters,
};
