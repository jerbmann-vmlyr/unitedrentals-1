import types from '@/store/types';

export default {
  state: {
    defaultAccountId: 0,
    isLoggedIn: false,
    ratesPlaceBranch: '639',
  },
  actions: {
    [types.user.updateDefaultAccountId]: jest.fn(),
  },
};
