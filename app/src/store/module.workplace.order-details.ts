import { types } from '@/store';
import { WorkplaceOrder } from '@/lib/workplace';
import { WorkplaceRootState } from './module.workplace';
import { ActionTree, GetterTree, MutationTree } from 'vuex';

interface WorkplaceOrderDetailsRootState extends WorkplaceRootState {
  detailItem: WorkplaceOrder;
}

export interface WorkplaceOrderDetailsState {
  detailItem: WorkplaceOrder;
}

const { orderDetails } = types.workplace;

const state: WorkplaceOrderDetailsState = {
  detailItem: {} as WorkplaceOrder,
};

const actions: ActionTree<WorkplaceOrderDetailsState, WorkplaceOrderDetailsRootState> = {};
const getters: GetterTree<WorkplaceOrderDetailsState, WorkplaceRootState> = {
  [orderDetails.detailItemOrNull](state, getters, rootState): WorkplaceOrder | null {
    const { orderId } = rootState.route ? rootState.route.params : { orderId: '' };

    return getters[types.workplace.ordersForCurrentAccount].find(({ orderNumber }) => orderNumber === orderId) || null;
  },
};
const mutations: MutationTree<WorkplaceOrderDetailsState> = {
  [orderDetails.detailItem](state, item: WorkplaceOrder) {
    state.detailItem = item;

    return item;
  },
};

export default {
  state,
  actions,
  getters,
  mutations,
};
