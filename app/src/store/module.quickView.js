import Vue from 'vue';
import types from '@/store/types';

const state = {

  /**
   * A Collection of QuickView component states, keyed by catClass
   * @type {Object<catClass, boolean>}
   */
  isOpen: {},
};

const actions = {

  /**
   * Register a Quick View with this store
   * @param {method} commit vuex mutation invoker
   * @param {string} id a catclass
   */
  [types.quickView.add]({ commit }, id) {
    commit(types.quickView.add, id);
  },

  /**
   * Remove a Quick View from this store
   * @param {method} commit vuex mutation invoker
   * @param {string} id a catclass
   */
  [types.quickView.remove]({ commit }, id) {
    commit(types.quickView.remove, id);
  },

  /**
   * Toggle a Quick View open/closed
   * @param {method} commit vuex mutation invoker
   * @param {string} id a catclass
   */
  [types.quickView.toggle]({ commit }, id) {
    commit(types.quickView.toggle, id);
  },
};

const mutations = {
  [types.quickView.add](state, id) {
    Vue.set(state.isOpen, id, false);
  },
  [types.quickView.remove](state, id) {
    Vue.delete(state.isOpen, id);
  },
  [types.quickView.toggle](state, id) {
    state.isOpen[id] = !state.isOpen[id];
  },
};

export default {
  actions,
  mutations,
  state,
};
