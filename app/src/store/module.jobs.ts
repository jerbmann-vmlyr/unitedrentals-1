import Vue from 'vue';
import { ActionTree, MutationTree, GetterTree } from 'vuex';
import { Jobsite } from '@/lib/common-types';
import api from '@/api';
import types from '@/store/types';
import { autocomplete } from '@/utils/';

import { forOwn, get } from 'lodash';

import { email, required, minLength } from 'vuelidate/lib/validators';


const formDefaults = {
  accountId: '', // required
  name: '', // required
  contact: '', // required
  code: '',
  mobilePhone: '', // required
  email: '',
  address1: '', // required
  address2: '',
  zip: '', // required
  city: '', // required
  state: '', // required
  country: '', // default value
  notes: ''
};

export interface JobsState {
  all: {
    [accountId: string]: {
      [jobsiteId: string]: Jobsite;
    }
  };
  branchId: string;
  closestBranchToJobsite: {
    [jobsiteId: string]: string;
  }
  created: number[];
  form: { [target: string]: Object };
  fetching: boolean;
  formError: boolean;
}
const state = {

  /**
   * @type {object} a collection of jobsites keyed by account id then job id.
   * E.g. state.all[accountId][jobId] = { ... } // job data
   */
  all: {},

  /**
   * A list of jobsite IDs the user has created (this session).
   * This list gets updated whenever the "addJobsite" action is invoked.
   */
  created: [],

  /**
   * An object keyed by jobsite id's which hold string values of Branch Ids.
   */
  closestBranchToJobsite: {},

  /**
   * @type {object} form fields for a (current, edited) jobsite.
   * Note, this architecture requires users to edit one job at a time.
   */
  form: {},

  fetching: false,
  formError: false,
  isActive: false,
};

const getters: GetterTree<JobsState, any> = {
  [types.jobs.getJobsiteCountOnAccount]: state => accountId => {
    const jobsites = state.all[accountId] || {};
    return Object.keys(jobsites).length;
  },
  [types.jobs.createJobFormIsValid]: state => {
    const { form } = state;
    const validates = validation => validation;
    return [
      required(form.address1),
      required(form.city),
      required(form.country),
      email(form.email),
      required(form.contact),
      required(form.mobilePhone),
      minLength(form.mobilePhone, 14),
      required(form.name),
      required(form.state),
      required(form.zip),
    ].every(validates);
  },
};

const actions: any = {
  [types.jobs.addJobsite]({ commit, dispatch, state, rootState }, obj) {
    const blockId = obj.blockId || 'anon';
    const accountId = obj.accountId;

    return api.jobs.addJobsite({ ...state.form[blockId], ...{ accountId } })
      .then(async ({ data }) => {
        // The "Normal" Create Jobsite API returns an error like ["this is a message"].
        if ((Array.isArray(data) && typeof data[0] === 'string')) {
          throw new Error(data[0]);
        }

        // Add the new jobsite to the list of jobsites for this account
        commit(types.jobs.addJobsite, {
          accountId,
          job: data,
        });

        // Set the new Jobsite as the User's Default Jobsite
        commit(types.user.defaultJobId, data.id);

        try {
          await dispatch(types.jobs.setClosestBranchToJobsite, {accountId, jobsiteId: data.id});
        }
        catch (e) {
          console.warn('Unable to find a nearby branch for the newly created jobsite.');
        }

        /*
          Notice this action returns the promise with the new id.
          The modal which calls this action, create-jobsite-form-modal.vue, will dispatch another action after this one.
          The parent (either the Reservation Block > Edit Jobsite component, or the Cart Item Form)
          supplies the action to be called after this one.  Those actions update things for the parent
          (the Reservation Block, or the Default Job Id)
         */

        // Return the ID of the new jobsite
        return data.id;
      });
  },

  [types.jobs.fetch]({ commit, state, rootState }, accountId) {
    // Do not fetch jobs if no account was provided
    if (!accountId) {
      commit('jobs/fetching', false);
      return;
    }

    if (Object.keys(state.all[accountId] || {}).length > 0) {
      commit('jobs/fetching', false);

      return Promise.resolve(state.all[accountId]);
    }

    commit('jobs/fetching', true);

    return api.jobs.fetch(accountId)
      .then(({ data }) => {
        commit('jobs/fetching', false);
        if (!data[0].accountId) {
          return;
        }
        commit(types.jobs.fetch, { accountId, data });
      });
  },

  // Loads the form state with an existing job
  [types.jobs.loadForm]({ commit }, { accountId, jobId, blockId }) {
    blockId = blockId || 'anon';
    commit(types.jobs.loadForm, { accountId, jobId, blockId });
  },

  [types.jobs.reloadForm]({ commit }, payload) {
    commit(types.jobs.reloadForm, payload);
  },

  // Resets the form state
  [types.jobs.resetForm]({ commit }) {
    commit(types.jobs.resetForm);
  },

  // Updates a form field. @param {object<name:val>} field
  [types.jobs.updateFormField]({ commit, state }, field) {
    const blockId = field.blockId || 'anon';
    let obj = {
      ...field,
      blockId
    };

    // If creating a new form, prepopulate it first with default object
    if (typeof state.form[blockId] === 'undefined') {
      obj = {
        ...formDefaults,
        ...field,
        blockId
      }
      commit(types.jobs.updateFormField, obj);
    } else {
      obj = {
        ...field,
        blockId
      };
      commit(types.jobs.updateFormField, obj);
    }
    
  },

  // Sends data in state.form to PATCH /api/v1/jobsite/:jobsiteId
  [types.jobs.saveForm]({ commit, state }, { accountId, jobsiteId, reservationBlockId }) {
    const blockId = reservationBlockId || 'anon';
    const jobsite = { ...state.form[blockId] };
    return api.jobs.saveJobsite(jobsite, jobsiteId).then(({ data }) => {
      // The "Normal" Create Jobsite API returns an error like ["this is a message"].
      if ((Array.isArray(data) && typeof data[0] === 'string')) {
        throw new Error(data[0]);
      }

      commit(types.jobs.saveForm, { jobsite, accountId, jobsiteId });
    });
  },
  async [types.jobs.setClosestBranchToJobsite]({ commit, state }, { accountId, jobsiteId }) {
    const jobsite = state.all[accountId][jobsiteId];
    const { city, zip } = jobsite;

    const place = await autocomplete.getPlaceFromAddress(`${city}, ${jobsite.state} ${zip}`);

    const { lat, lng } = place.geometry.location;
    const response = await api.branches.getNearbyBranch({ lat, lng });
    const { branchId } = response.data.data[0];

    commit(types.jobs.setClosestBranchToJobsite, {
      jobsiteId,
      branchId,
    });

    return branchId;
  },

  [types.jobs.storeJobsite]({ commit }, input) {
    this.commit(types.jobs.storeJobsite, input);
  },

  [types.jobs.toggleFormError]({ commit }, bool) {
    commit(types.jobs.toggleFormError, bool);
  },
};

const mutations: any = {
  [types.jobs.addJobsite](state, { accountId, job }) {
    if (!state.all[accountId]) {
      Vue.set(state.all, accountId, {});
    }

    Vue.set(state.all[accountId], job.id, job);

    state.created.push(job.id);
  },

  [types.jobs.fetch](state, { accountId, data }) {
    if (!state.all[accountId]) {
      Vue.set(state.all, accountId, {});
    }

    data.forEach(job => {
      Vue.set(state.all[accountId], job.id, job);
    });
  },

  [types.jobs.loadForm](state, { accountId, jobId, blockId }) {
    blockId = blockId || 'anon';
    forOwn(state.form[blockId], (val, field) => {
      if (field !== 'country') {
        state.form[blockId][field] = get(state.all, [accountId, jobId, field], '');
      }
    });
  },

  [types.jobs.reloadForm](state, payload) {
    forOwn(state.form, (val, field) => {
      state.form[field] = get(payload, field, '');
    });
  },

  [types.jobs.resetForm](state) {
    state.form = { anon: {...formDefaults} };
  },

  [types.jobs.updateFormField](state, fields) {
    const target = fields.blockId;
    delete fields.blockId;
    Object.keys(fields).forEach(ea => {
      state.form[target] = {
        ...state.form[target],
        [ea]: fields[ea]
      }
    });
  },

  [types.jobs.saveForm](state, { accountId, jobsite, jobsiteId }) {
    state.all[accountId][jobsiteId] = { ...jobsite, id: jobsiteId };
  },

  [types.jobs.setClosestBranchToJobsite](state, { jobsiteId = 'Unknown', branchId }) {
    Vue.set(state.closestBranchToJobsite, jobsiteId, branchId);
  },

  [types.jobs.storeJobsite](state, input) {
    state.all[input.accountId][input.id] = input;
  },

  [types.jobs.toggleFormError](state, bool) {
    state.formError = bool;
  },

  ['jobs/fetching'](state, bool) {
    state.fetching = bool;
  },

  ['jobs/reset'](state) {
    state.all = {};
    state.form = { anon: {...formDefaults} };
  },
};

export default {
  actions,
  mutations,
  state,
  getters,
};
