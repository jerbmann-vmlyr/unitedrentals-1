
import { get } from 'lodash';
import { ActionTree, MutationTree, GetterTree, Mutation } from 'vuex';
import WorkplaceEquipmentApi, { PickupRequestRequest, PickupRequestResponse } from '@/api/api.workplace-equipment';
import api from '@/api';
import types from '@/store/types';
import { ApiV2Utils } from '@/lib/apiv2';
import * as moment from 'moment';
import DateTime from '@/utils/dateTime';
import { Address, Contact, Jobsite } from '@/lib/common-types';
import { WorkplaceEquipment } from '@/lib/workplace';
import { Branch } from '@/lib/branches';

export interface PickupInstructions {
  accessTimes: string;
  additional: string;
  equipmentLocation: string;
  keyLocation: string;
}

export interface EquipmentQuantities {
  [equipmentId: string]: number;
}

export interface RequestPickupState {
  /**
   *
   */
  all: { };
  accountId: string,
  transId: string,
  dataLoaded: boolean,
  address: NonNullableProps<Address>;
  branch: null | Branch;
  confirmationNumber: string;
  contact: NonNullableProps<Contact>;
  equipmentList: WorkplaceEquipment[];
  emailRecipients: string[];
  instructions: PickupInstructions;
  requestIsInitialized: boolean;
  selectedDateTime: moment.Moment;
  startDate: moment.Moment;
  returnDate: moment.Moment;
  selectedEquipmentQtys: EquipmentQuantities;
}

const stateFactory = (): RequestPickupState => ({
  all: {},
  accountId: '',
  transId: '',
  dataLoaded: false,
  address: {
    address1: '',
    address2: '',
    city: '',
    state: '',
    zip: '',
  },
  branch: null,
  confirmationNumber: '',
  contact: {
    contact: '',
    email: '',
    mobilePhone: '',
    phone: '',
  },
  emailRecipients: [],
  equipmentList: [],
  instructions: {
    accessTimes: '',
    additional: '',
    equipmentLocation: '',
    keyLocation: '',
  },
  requestIsInitialized: false,
  selectedDateTime: DateTime.round(moment(), moment.duration(30, 'minutes'), 'ceil'),
  startDate: DateTime.round(moment(), moment.duration(30, 'minutes'), 'ceil'),
  returnDate: DateTime.round(moment(), moment.duration(30, 'minutes'), 'ceil'),
  selectedEquipmentQtys: {},
});

const actions: ActionTree<RequestPickupState, any> = {
  async [types.requestPickup.initializePickupRequest]({commit, dispatch, getters, state}, data) {
    // Populate the list of equipment
    const requestData = await dispatch(types.requestPickup.fetchRequestPickupData, data);
    const { detailData, headerData } = requestData;
    commit(types.requestPickup.mutate, () => state.equipmentList = detailData);
    commit(types.requestPickup.mutate, () => state.transId = headerData.contractNumber);
    commit(types.requestPickup.mutate, () => state.accountId = headerData.accountNumber);
    commit(types.requestPickup.mutate, () => state.address.address1 = headerData.jobsiteAddress1);
    commit(types.requestPickup.mutate, () => state.address.city = headerData.jobsiteCity);
    commit(types.requestPickup.mutate, () => state.address.state = headerData.jobsiteState);
    commit(types.requestPickup.mutate, () => state.address.zip = headerData.jobsiteZip);
    commit(types.requestPickup.mutate, () => state.contact.contact = headerData.jobsiteContact);
    commit(types.requestPickup.mutate, () => state.contact.phone = headerData.jobsitePhone);

    // Populate the equipment quantity tracking object, only the item for which the reschedule request has
    // been opened for (identifed by transLineId in URL) should have a quantity of greater then zero
    const equipmentQtys: EquipmentQuantities = requestData.detailData
      .map(eqp => ({
        [eqp.equipmentId]: eqp.quantityAvailableForPickup,
      }))
      .reduce((accum, qty) => ({ ...accum, ...qty }), {});

    await dispatch(types.requestPickup.setSelectedEquipmentQtys, equipmentQtys);

    // Set the initial value of the selected date and time, based on the original return date and time
    // If that date has passed, set it to now
    const originalReturn = moment(headerData.returnDateTime);
    const initialSelectedDateTime = originalReturn.isBefore(moment(), 'day') ? moment() : originalReturn;

    commit(types.requestPickup.mutate, state => state.selectedDateTime = initialSelectedDateTime);
    commit(types.requestPickup.mutate, state => state.startDate = headerData.startDateTime);
    commit(types.requestPickup.mutate, state => state.returnDate = headerData.returnDateTime);
    commit(types.requestPickup.mutate, state => state.instructions = state.instructions);
    commit(types.requestPickup.mutate, state => state.emailRecipients = state.emailRecipients);

    // Fetch the branch of the first item and save it to state
    // const branch = await dispatch(types.branch.fetchById, equipment[0].branchId);
    commit(types.requestPickup.mutate, () => state.branch = requestData.branch);
    // Set the request to initialized
    commit(types.requestPickup.mutate, () => state.requestIsInitialized = true);
  },

  async [types.requestPickup.fetchRequestPickupData](
    { commit }, data: { transId: string, equipmentId: string, phone: string, email: string, accountId: string }) {
    const requestPickup = await api.requestPickup.getAll(
      data.transId, data.equipmentId, data.phone, data.email, data.accountId)
      .then(ApiV2Utils.unwrapResponseData);
    return requestPickup;
  },
  async [types.requestPickup.submitPickupRequest]({ commit, getters, state }): Promise<PickupRequestResponse> {
    const request: PickupRequestRequest = getters[types.requestPickup.pickupRequestRequest];
    const response = await WorkplaceEquipmentApi.sendPickupRequest(request);
    commit(types.requestPickup.mutate, () => state.confirmationNumber = response.pickupId);
    return response;
  },

  async [types.requestPickup.selectAllEquipment]({ state, dispatch }) {
    const equipmentQtys: EquipmentQuantities = state.equipmentList
      .map(eqp => ({ [eqp.equipmentId]: eqp.quantityOnRent }))
      .reduce((accum, qty) => ({ ...accum, ...qty }), {});

    await dispatch(types.requestPickup.setSelectedEquipmentQtys, equipmentQtys);
  },

  async [types.requestPickup.setEquipmentSelectedById]({ state, dispatch }, payload: { equipmentId: string, selected: boolean }) {
    const matchingEqp = state.equipmentList.find(eqp => eqp.equipmentId === payload.equipmentId);
    if (!matchingEqp) throw new Error(`There's not equipment item with id "${payload.equipmentId}"`);
    await dispatch(types.requestPickup.setSelectedEquipmentQtyById, {
      equipmentId: payload.equipmentId,
      quantity: payload.selected ? matchingEqp.quantity : 0,
    });
  },

  [types.requestPickup.setSelectedEquipmentQtyById]({ state, commit }, payload: { equipmentId: string; quantity: number}) {
    // Update the quantity of the provided item
    const equipmentQtys = { ...state.selectedEquipmentQtys, [payload.equipmentId]: payload.quantity };
    commit(types.requestPickup.mutate, () => state.selectedEquipmentQtys = equipmentQtys);
  },

  [types.requestPickup.setSelectedEquipmentQtys]({ state, commit }, quantities: EquipmentQuantities) {
    commit(types.requestPickup.mutate, () => state.selectedEquipmentQtys = quantities);
  },
  [types.requestPickup.setSelectedDateTime]({ commit, state }, dateTime: moment.Moment) {
    commit(types.requestPickup.mutate, () => state.selectedDateTime = dateTime.clone());
  },

  async [types.requestPickup.unselectAllEquipment]({ dispatch, state }) {
    const equipmentQtys: EquipmentQuantities = state.equipmentList
      .reduce((accum, eqp) => ({ ...accum, [eqp.equipmentId]: 0 }), {});

    await dispatch(types.requestPickup.setSelectedEquipmentQtys, equipmentQtys);
  },

  [types.requestPickup.saveEmailRecipients]({ commit, state }, emailRecipients: string[] = []) {
    commit(types.requestPickup.mutate, () => state.emailRecipients = emailRecipients);
  },
  [types.requestPickup.reset]({ commit, state }) {
    // tslint:disable-next-line:prefer-object-spread
    commit(types.requestPickup.mutate, () => Object.assign(state, stateFactory()));
  },
  [types.requestPickup.saveAddress]({ commit, state }, address: NonNullableProps<Address>) {
    commit(types.requestPickup.mutate, () => state.address = address);
  },
  [types.requestPickup.saveContact]({ commit, state }, contact: NonNullableProps<Contact>) {
    commit(types.requestPickup.mutate, () => state.contact = contact);
  },
  [types.requestPickup.saveInstructions]({ commit, state }, instructions: PickupInstructions) {
    commit(types.requestPickup.mutate, () => state.instructions = instructions);
  },
};

const mutations: MutationTree<RequestPickupState> = {
  [types.requestPickup.mutate](state, mutation: Mutation<RequestPickupState>) {
    mutation(state, null);
  },
};

const getters: GetterTree<RequestPickupState, any> = {

  [types.requestPickup.accountId](state): string {
    return state.accountId || '';
  },
  [types.requestPickup.accountName](state, getters, rootState): string {
    return get(rootState.accounts.all, [getters[types.requestPickup.accountId], 'name'], '');
  },
  [types.requestPickup.isPickup](): boolean {
    return true;
  },
  [types.requestPickup.jobsite](state): Jobsite {
    const [eqp] = state.equipmentList;
    return (eqp.jobsite || {}) as any;
  },
  [types.requestPickup.allEquipmentIsSelected](state): boolean {
    return Object.values(state.selectedEquipmentQtys).every(qty => qty > 0);
  },
  [types.requestPickup.atLeastOneEquipmentIdSelected](state): boolean {
    return Object.values(state.selectedEquipmentQtys).some(qty => qty > 0);
  },
  [types.requestPickup.selectedEquipment](state): WorkplaceEquipment[] {
    return state.equipmentList.filter(({ equipmentId }) => state.selectedEquipmentQtys[equipmentId] > 0);
  },
  [types.requestPickup.branchHoursOffset]({ branch }): number {
    /** The timeOffset returned by the branch API is actually relative to UR HQ in NY */
    const correction = moment().isDST() ? 4 : 5;

    if (!branch || !Number.isSafeInteger(+branch.timeOffset)) return correction;
    const skewedOffset = Math.abs(Math.round(+branch.timeOffset / 100));
    return correction + skewedOffset;
  },
  [types.requestPickup.originalStartDateTime](state): moment.Moment | null {
    return state.startDate;
  },

  [types.requestPickup.originalReturnDateTime](state): moment.Moment | null {
    return state.returnDate;
  },
  [types.requestPickup.equipmentListIsEmpty](state): boolean {
    return state.equipmentList.length < 1;
  },
  [types.requestPickup.pickupRequestRequest](state, getters, rootState): PickupRequestRequest {
    const pickupDateTime = moment.max(state.selectedDateTime.clone(), moment()).format('YYYY-MM-DDTHH:mm:ss');
    const transId = getters[types.requestPickup.transId];

    const normalizedPhone = state.contact.phone.replace(/\D/g, '');
    const phone = normalizedPhone.length === 10
      ? normalizedPhone.match(/^(\d{3})(\d{3})(\d{4})$/)!.slice(1, 4).join('-')
      : '';

    // Map the equipment quantities to their transaction IDs
    const quantitiesByTransId = Object.entries(state.selectedEquipmentQtys)
      .filter(([, quantity]) => quantity > 0)
      .reduce((accum, [equipmentId, quantity]) => {
        const { transLineId } = state.equipmentList.find(eqp => eqp.equipmentId === equipmentId)!;
        return { ...accum, [transLineId]: quantity };
      }, {});

    // Build the request objects
    return {
      address1: state.address.address1,
      address2: state.address.address2,
      city: state.address.city!,
      moreInstructions: state.instructions.additional,
      pickupDateTime,
      quantity: Object.values(quantitiesByTransId).join(','),
      state: state.address.state,
      transId,
      transLineId: Object.keys(quantitiesByTransId).join(','),
      zip: state.address.zip!,
      guid: rootState.tcUser.guid,
      contact: state.contact.contact,
      phone,
      requester: state.contact.contact,
      equipmentLocation: state.instructions.equipmentLocation,
      keyLocation: state.instructions.keyLocation,
      accessTimes: state.instructions.accessTimes,
      emailAddresses: [state.contact.email, ...state.emailRecipients]
        .filter(email => typeof email === 'string' && email.length > 0)
        .join(';'),
    };
  },
  [types.requestPickup.transId](state): string {
    return state.transId || '';
  },
};

export default {
  actions,
  mutations,
  getters,
};
