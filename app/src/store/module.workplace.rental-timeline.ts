import Vue from 'vue';
import {
  ActionTree,
  GetterTree,
  MutationTree,
} from 'vuex';

import { WorkplaceRentalTimelineDates, WorkplaceEquipment, Leniency } from '@/lib/workplace';

import { WorkplaceRootState } from './module.workplace';

import types from './types';

export type WorkplaceRentalTimelineState = WorkplaceRentalTimelineDates;

const state: WorkplaceRentalTimelineState = {
  billThru: '',
  leniencies: [],
  rentalEnd: '',
  rentalStart: '',
};

const actions: ActionTree<WorkplaceRentalTimelineState, WorkplaceRootState> = {
  [types.workplace.rentalTimeline.reset]({ commit, getters }) {
    commit(types.workplace.rentalTimeline.update, {
      billThru: '',
      leniencies: [],
      rentalEnd: '',
      rentalStart: '',
    });
  },

  [types.workplace.rentalTimeline.update]({ commit, getters }) {
    const detailItem: WorkplaceEquipment | null = getters[
      types.workplace.equipmentDetails.detailItemOrNull
      ];

    const leniencies: Leniency[] | null = getters[
      types.workplace.equipmentDetails.leniencyRates
      ];

    const dates: WorkplaceRentalTimelineState = {
      billThru: !!detailItem && detailItem.lastBilledDate || '',
      leniencies: leniencies || [],
      rentalEnd: !!detailItem && (detailItem.pickupDateTime || detailItem.returnDateTime) || '',
      rentalStart: !!detailItem && detailItem.startDateTime || '',
    };

    commit(types.workplace.rentalTimeline.update, dates);
  },
};

const getters: GetterTree<WorkplaceRentalTimelineState, WorkplaceRootState> = {};

const mutations: MutationTree<WorkplaceRentalTimelineState> = {
  [types.workplace.rentalTimeline.update](state, dates: WorkplaceRentalTimelineState) {
    Object.entries(dates).forEach(([prop, date]) => {
      Vue.set(state, prop, date);
    });
  },
};

export default {
  state,
  actions,
  getters,
  mutations,
};
