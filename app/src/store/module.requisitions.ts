import api from '@/api';
import types from '@/store/types';

const state = {
  all: [],
  requisition: {},
  branchId: '',
};

const actions = {
  [types.requisitions.update]({ commit }, input) {
    return api.requisitions.update(input)
      .then(() => {
        commit(types.requisitions.update, input.branchId);
      });
  },
  [types.requisitions.updateBoth]({ commit }, input) {
    return api.requisitions.updateBoth(input)
      .then(() => {
        commit(types.requisitions.updateBoth, input);
      });
  },
};

const mutations = {
  [types.requisitions.update](state, input) {
    state.branchId = input;
  },
  [types.requisitions.updateBoth](state, input) {
    state.requisition = input;
  },
};

const getters = {};

export default {
  actions,
  mutations,
  state,
  getters,
};
