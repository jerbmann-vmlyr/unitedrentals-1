import types from '@/store/types';

export default {
  getters: {
    [types.inFlightRequests.tokensAreInFlight]: () => () => false,
  },
};
