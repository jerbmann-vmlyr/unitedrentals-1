/* eslint-disable camelcase */
import get from 'lodash/get';

const state = {
  generalNumber: get(drupalSettings, ['ur', 'general_inquiries'], '1.844.873.4949'),
  rentalNumber: get(drupalSettings, ['ur', 'rental_inquiries'], '1.844.873.4949'),
  otherNumber: '1.844.873.4948', // TODO put this in the CMS and find an appropriate field-name for it!
  tertiaryNumber: '1.844.369.6270', // TODO put this in the CMS and find an appropriate field-name for it!
};

export default {
  state,
};
