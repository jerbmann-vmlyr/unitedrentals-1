import { mapState, mapActions } from 'vuex';
import types from './types';

export const authComputed = {
  ...mapState({
    tcUser: state => state.tcUser,
    user: state => state.user,
    isLoggedIn: state => state.user.isLoggedIn,
    defaultAccountId: state => state.user.defaultAccountId,
  }),
};

export const catalogComputed = {
  ...mapState({
    equipment: state => state.catalog.equipment,
  }),
};

export const catalogMethods = {
  ...mapActions({
    fetchEquipmentByCatClass: types.catalog.fetchEquipmentByCatClass,
  }),
};
