import types from '@/store/types';

export default {
  state: {
    items: {},
    cartId: '',
    toastItem: {},
    confirmed: false,
    equipment: {},
  },
  actions: {
    [types.catalog.fetchEquipmentByCatClass]: jest.fn(),
  },
};
