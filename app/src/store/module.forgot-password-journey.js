/*

This module supports the Guests journey through the Forgot Password Flow.

It is necessary because our app relies on sessionStorage for persistance, yet the Forgot PW flow routes a user
into a new tab (which does not have access to the user's sessionStorage).

To mitigate this,  all data required in the Forgot PW flow is written to localStorage, which IS accessible in a new tab.

 */
import types from '@/store/types';

const actions = {
  [types.forgotPasswordJourney.writeLocalStorage]({ rootState }) {
    const data = getAllJourneyData(rootState);
    Object.entries(data).forEach(writeToLocalStorage);
  },

  [types.forgotPasswordJourney.destroyLocalStorage]({ rootState }) {
    console.log('Destroying localStorage for Forgot Password Journey'); // eslint-disable-line
    const data = getAllJourneyData(rootState);
    Object.keys(data).forEach(key => localStorage.removeItem(`${baseKey}${key}`));
  },

  /*
  Populates Vuex stores with data from localStorage.
  NOTE: To be safe, this fn only commits a mutation if the vuex store is empty!!

  Assumptions:
    0. This method only supports the Guest's Forgot PW journey.
    1. the guest has made it to checkout and clicked the forgot pw link (billing step of checkout)
    2. clicking that link will write all the journey data to localStorage
    3. the guest will then log in to the site in a new tab. App.vue will invoke this method, and for the first time
       the user will NOT have any vuex data (due to being in a new tab, where sessionStorage has no data)
    4. this method will run through to the end, hydrating the vuex store with data from localStorage
       then deleting the localStorage.
   */
  [types.forgotPasswordJourney.getStateFromLocalStorage]({ commit, dispatch, rootState }) {
    // To be safe, get state from localStorage if-and-only-if there is nothing in the cart
    if (Object.keys(rootState.cart.items).length) {
      return;
    }

    // Get all local storage data
    const localCartItems = getFromLocalStorage('localCartItems');

    // If there is nothing in localStorage either, there's nothing to do.
    // (This is very unlikely to happen, since we're expecting users to start this journey at the end of checkout.)
    if (!localCartItems || !Object.keys(localCartItems).length) {
      return;
    }

    console.log('Hydrating cart from localStorage for Forgot Password Journey'); // eslint-disable-line
    // Shove these items into the cart state
    commit(types.cart.fetch, { items: localCartItems });

    // Now that we've used it, destroy the local storage!
    dispatch(types.forgotPasswordJourney.destroyLocalStorage);
  },
};

export default {
  actions,
};


const baseKey = 'kelex_guest_forgot_password_journey__';

const getAllJourneyData = rootState => ({
  localCartItems: rootState.cart.items,
});

const writeToLocalStorage = ([key, value]) => {
  console.log('Persisting cart to localStorage for Forgot Password Journey', key, JSON.stringify(value)); // eslint-disable-line
  localStorage.setItem(`${baseKey}${key}`, JSON.stringify(value));
  console.log('done'); // eslint-disable-line
};

const getFromLocalStorage = key => JSON.parse(localStorage.getItem(`${baseKey}${key}`));
