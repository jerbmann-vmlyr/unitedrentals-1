
import { ActionTree, MutationTree } from 'vuex';
import api from '@/api';
import types from '@/store/types';
import { ApiV2Utils } from '@/lib/apiv2';

export interface BusinessTypesState {
  /**
   * Business Type data returned from a businesstype api call.
   * This state gets persisted in the browsers Session Storage.
   *
   * Example: {tid: '1234', url: 'general-equipment', typeMap: 'GR', ...}
   */
  all: { };
}

const state: BusinessTypesState = {
  all: {},
};

const actions: ActionTree<BusinessTypesState, any> = {
  async [types.businessTypes.fetch]({ commit }) {
    const businessTypes = await api.businessTypes.getAll().then(ApiV2Utils.unwrapResponseData);
    commit(types.businessTypes.fetch, businessTypes);
    return businessTypes;
  },
};

const mutations: MutationTree<BusinessTypesState> = {
  [types.businessTypes.fetch](state, businessTypesData) {
    state.all = businessTypesData;
  },
};

export default {
  actions,
  mutations,
  state,
};
