import api from '@/api';
import types from '@/store/types';
import Vue from 'vue';
import { ActionTree, GetterTree, MutationTree } from 'vuex';
import { SearchByTermFilter, ListFilterType, SearchByTermLexicon } from '@/lib/listings';
import { stringify } from 'qs';
import { FlightToken } from '@/api/flight-track';

export interface QuotesState {
  all: { [requisitionId: string]: any },
  estimates: { [requisitionId: string]: any },
  filters: SearchByTermFilter[];
  fetching: boolean,
  errors: object;
  newBranchAndDatesUpdate: { [requisitionId: string]: any };
  resetBranchAndDatesUpdate: boolean;
  newJobsiteId: number;
}

const state: QuotesState = {
  /**
   * Quotes returned from a api-appliance call, keyed by guid, and
   * accountId(not required).
   */
  all: {},

  filters: [],

  estimates: {},

  fetching: false,

  errors: {},

  newBranchAndDatesUpdate: {},

  resetBranchAndDatesUpdate: true,

  newJobsiteId: 0,
};

const actions: ActionTree<QuotesState, any> = {
  async [types.quotes.fetch]({ commit }, guid) {
    const { data } = await api.quotes.fetch();
    if (data === null) {
      throw new Error(`No quotes data returned. (Action: types.quotes.fetch)`);
    }
    commit(types.quotes.fetch, { data });
    return data;
  },
  /* listing filters */
  [types.quotes.addFilter]({ commit, state }, filter) {
    if (state.filters.some(f => f.term === filter.term)) {
      return;
    }
    commit(types.quotes.filters, [...state.filters, filter]);
  },
  [types.quotes.removeFilter]({ commit, state }, filter) {
    commit(types.quotes.filters, state.filters.filter(f => f.id !== filter.id));
  },
  [types.quotes.removeAllFilters]({ commit }) {
    commit(types.quotes.filters, []);
  },
  async [types.quotes.resetBranchAndDatesUpdate]({ commit }) {
    commit(types.quotes.resetBranchAndDatesUpdate, true);
  },
  async [types.quotes.convertQuote]({ commit }, quoteItem) {
    const res = await api.quotes.convertQuoteToCart(quoteItem);
    return res;
  },
  [types.quotes.updateQuote]({ commit }, input) {
    const result = api.quotes.update(input).then(({ data }) => {
      commit(types.quotes.updateQuote, data);
    });
    return result;
  },
  [types.quotes.removeQuote]({ commit }, input) {
    commit(types.quotes.removeQuote, input);
  },
  async [types.quotes.newBranchAndDatesUpdate]({ commit }, {
    requisitionId, branchId,
  }): Promise<QuotesState> {
    commit(types.quotes.newBranchAndDatesUpdate, { data: branchId, requisitionId });
    commit(types.quotes.resetBranchAndDatesUpdate, false);
    return branchId;
  },
  [types.quotes.setNewJobsiteId]({ commit }, input) {
    commit(types.quotes.setNewJobsiteId, input);
  },
};

const mutations: MutationTree<QuotesState> = {
  [types.quotes.fetch](state, { data }) {
    /* store the list of quotes as an object whose keys are each quote's requisitionId */
    state.all = data.reduce(
      (quoteObj, item) => ({
        ...quoteObj,
        [item.requisitionId]: item,
      }),
      {},
    );
  },
  [types.quotes.newBranchAndDatesUpdate](state, { requisitionId, data }) {
    Vue.set(state.newBranchAndDatesUpdate, requisitionId, data);
  },
  [types.quotes.resetBranchAndDatesUpdate](state, bool) {
    state.resetBranchAndDatesUpdate = bool;
  },
  [types.quotes.reset](state) {
    state.all = {};
  },
  [types.quotes.filters](state, filters: SearchByTermFilter[]) {
    state.filters = filters;
  },
  [types.quotes.errors](state, error) {
    state.errors = error;
  },
  [types.quotes.updateQuote](state, input) {
    state.all[input.requisitionId] = input;
  },
  [types.quotes.removeQuote](state, input) {
    // Soft delete...
    Vue.set(state.all[input], 'converted', true);
  },
  [types.quotes.setNewJobsiteId](state, input) {
    state.newJobsiteId = input;
  },
};

const getters: GetterTree<QuotesState, any> = {
  /* quotes that belong to the currently selected account */
  quotesForCurrentAccount(state, getters, rootState) {
    return Object.values(state.all).filter(quote => quote.accountId === rootState.user.defaultAccountId && !quote.converted);
  },
  /* quotes that belong to the currently selected account AND with filters applied */
  filteredQuotes(state, getters) {
    return getters.quotesForCurrentAccount
      .filter(quote =>
        getters.quotesFilterFunctions.every(filter => filter(quote)),
      );
  },
  /* converts all the different filter types (search term, view, etc) into a list of functions */
  quotesFilterFunctions(state) {
    return state.filters.map(filter => filter.asFunction);
  },

  quotesSearchTermFilters(state) {
    return state.filters.filter(filter => filter.type === ListFilterType.SearchByTerm);
  },

  quoteLexicon(): SearchByTermLexicon {
    return ({ accountId, requisitionId, quoteId, jobName, orderStatus, jobId }) => {
      return [
        accountId,
        requisitionId,
        quoteId,
        jobName,
        orderStatus,
        jobId,
      ].map(term => term.toLowerCase());
    };
  },

  quoteConversionError(state) {
    return stringify(state.errors);
  },

  [types.quotes.quotesCount](state, getters) {
    return getters.filteredQuotes.length;
  },

  [types.quotes.fetchingQuotesList](state, getters): boolean {
    return getters[types.inFlightRequests.tokensAreInFlight](
      FlightToken.fetchQuotes,
    );
  },

  [types.quotes.fetchingQuotesDetails](state, getters): boolean {
    return getters[types.inFlightRequests.tokensAreInFlight](
      FlightToken.fetchTransactions,
      FlightToken.fetchReservationDetails,
    );
  },

  [types.quotes.fetchingQuotesEstimates](state, getters): boolean {
    return getters[types.inFlightRequests.tokensAreInFlight](
      FlightToken.fetchChargeEstimates,
    );
  },
};

export default {
  actions,
  mutations,
  state,
  getters,
};
