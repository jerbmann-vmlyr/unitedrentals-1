/**
 * Timer mixin
 *
 * Starts a timer for a component before it gets created and finishes the timer
 * on its first update.
 */
export default {
  beforeCreate() {
    if (!this.$options.name) {
      console.warn('Need to set name of component to use timer mixin'); // eslint-disable-line
      return;
    }
    console.time(this.$options.name); // eslint-disable-line
  },
  updated() {
    if (!this.$options.name) {
      return;
    }
    console.timeEnd(this.$options.name); // eslint-disable-line
  },
};
