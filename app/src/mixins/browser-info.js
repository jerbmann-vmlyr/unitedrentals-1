export default {
  methods: {
    isIE11() {
      return !!navigator.userAgent.match(/Trident\/7\./);
    },
    isMSEdge() {
      return !!navigator.userAgent.match(/Edge/);
    },
    isIOS() {
      // Why not MSStream? Microsoft injected the word iPhone in IE11's userAgent
      // in order to try and fool Gmail somehow. Therefore we need to exclude it.
      return (/iPad|iPhone|iPod/).test(navigator.userAgent) && !window.MSStream;
    },
    isMobile() {
      return (/iphone|ipod|android|ie|blackberry|fennec/i).test(navigator.userAgent);
    },
    // very specific to target an IOS 11.0 to 11.2.6 bug with input location problems in fixed containers
    // should match ipad and iphone version of ios 11
    isIOS11Pre113() {
      return (/ OS 11_0| OS 11_1| OS 11_2/i).test(navigator.userAgent);
    },
  },
};
