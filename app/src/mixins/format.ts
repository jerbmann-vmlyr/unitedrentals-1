export default {
  methods: {
    upperCaseFirstLetters(str: string) {
      return str.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
    },
    urlify(str: string) {
      return str.toLowerCase().replace(/[&\\#/,+()$~%.'":*?<>{}]/g, '').replace(' ', '-');
    },
    urPhoneNumber(str: string) {
      return str.replace(/^1-/, '').replace(/[-]/g, '.');
    },
  },
};
