import { mapLabels } from '@/utils';
import { forEach, get, isEmpty } from 'lodash';

export default {
  computed: {
    activeTitle() {
      if (window.location.origin.includes(get(this.translation, ['french']))) {
        return this.i18n.french;
      }
      else if (window.location.origin.includes(get(this.translation, ['spanish']))) {
        return this.i18n.spanish;
      }
      return this.i18n.english;
    },
    links() {
      const links = [];
      forEach(this.translation, (url, lang) => {
        if (window.location.pathname !== null) {
          url += window.location.pathname;
        }
        links.push({
          title: this.i18n[lang],
          url,
        });
      });
      return links;
    },
    swappedLinks() {
      const links = [];
      forEach(this.translation, (url, lang) => {
        if (this.i18n[lang] !== this.activeTitle) {
          if (window.location.pathname !== null) {
            url += window.location.pathname;
          }
          links.push({
            title: this.i18n[lang],
            url,
          });
        }
      });
      return links;
    },
  },
  data() {
    return {
      i18n: mapLabels({
        english: 'ur.english',
        french: 'ur.french',
        spanish: 'ur.spanish',
      }),
      translation: null,
      active: false,
    };
  },
  mounted() {
    if (!isEmpty(drupalSettings.ur.translation)) {
      this.translation = drupalSettings.ur.translation;
    }
    else {
      throw Error('Drupal translation settings not set');
    }
  },
};
