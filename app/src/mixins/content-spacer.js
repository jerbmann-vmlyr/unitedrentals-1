export default {
  methods: {
    getSpacingString(length) {
      return String.fromCharCode(0x12007).repeat(length);
    },
  },
};
