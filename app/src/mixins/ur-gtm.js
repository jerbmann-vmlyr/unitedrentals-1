import { assign, get, forEach } from 'lodash';

const ANONYMOUS = '(anonymous)';

/**
 * GTM
 *
 * Sitewide Nav Usage: currently tracks just the top bar
 * Sitewide Exit Links: currently tracks the social media icons in the footer, ...
 * Sitewide Phone Interaction: tracks any number links on the website. Currently: stickied contact us button numbers,
 *  location finder branch list, location finder branch details
 * Sitewide Carousel Usage: tracks what carousel and page they go to. Located on home page, probox components,
 */
export const UrGtm = {
  computed: {
    storeID() {
      const storeID = this.$store.state.user.ratesPlaceBranch;
      return (storeID && storeID !== '') ? storeID : '';
    },
    accountID: vm => get(vm.$store.getters, ['user/defaultAccount', 'id'], ''),
    // Rentalman GUID
    rmGuid: vm => get(vm.$store.state, 'tcUser.guid') || ANONYMOUS,
  },
  data() {
    return {
      gtmDefault: {
        event: 'click',
        category: null,
        action: null,
        label: null,
        value: '',
      },
      userID: get(drupalSettings, ['user', 'uid']) !== 0 ? get(drupalSettings, ['user', 'uid']) : ANONYMOUS, // eslint-disable-line
    };
  },
  methods: {
    _gtmFormatProduct({
      name, catClass, price, category, subCategory, quantity,
    }, transId, reqId) {
      return {
        name, // Name or ID is required.
        id: catClass, // Product SKU
        price, // Product Price
        category, // Product Category (Equipment)
        list: subCategory, // Product Sub Category
        quantity, // Product Quantity
        dimension1: this.rmGuid,
        dimension2: typeof reqId === 'string' ? reqId : '', // Requisition ID
        dimension3: this.storeID,
        dimension5: catClass, // Product SKU
        dimension6: price, // Product Price
        dimension7: `${catClass}|${price}`, // Product SKU|Price
        dimension9: transId, // Transaction ID
        dimension11: this.accountID,
      };
    },

    _gaUserQuery() {
      /* eslint-disable */
      try {
        const trackerName = ga.getAll()[0].get('name');
        return `?${ga.getByName(trackerName).get('linkerParam')}`;
      }
      catch (error) {
        return '';
      }
      /* eslint-enable */
    },

    /**
     * GTM Push method
     *
     * Pass through method for dataLayer.push
     *
     * @param {object} obj | gtm object to add to data layer
     */
    gtmPush(obj) {
      const dataLayer = window.dataLayer || [];

      dataLayer.push(obj);
    },

    gtmTrackEvent(config = null) {
      if (typeof config === 'undefined') {
        throw new Error('gtmTrackEvent: config param is undefined');
      }

      if (typeof config !== 'object') {
        throw new Error('gtmTrackEvent: config param must be an object');
      }

      this.$urgtm.trackEvent(assign(this.gtmDefault, config));
    },

    /**
     * GTM Track Page View
     */
    gtmTrackPageview() {
      this.gtmPush({
        event: 'gtm.js',
        dimension1: this.rmGuid,
        dimension3: this.storeID,
        dimension11: this.accountID,
        isLoggedIn: this.userIsLoggedIn, // Logged In status Boolean
      });
    },

    /**
     * GTM Optimize Initialize
     */
    gtmOptimizeInitialize() {
      this.gtmPush({
        event: 'optimize.activate',
      });
    },

    /**
     * GTM Track Main Nav method
     *
     * @param {string} area | the area of navigation
     * @param {string} name | name of the link/button clicked
     */
    gtmTrackMainNav(area, name) {
      if (typeof area !== 'string') {
        throw new Error('gtmTrackMainNav: area param must be a string');
      }

      if (typeof name !== 'string') {
        throw new Error('gtmTrackMainNav: name param must be a string');
      }

      this.gtmTrackEvent({
        category: 'navigation',
        action: area,
        label: name,
      });
    },

    /**
     * GTM Track Exit Link
     *
     * @param {string} page | exit page hyperlink name
     * @param {string} link | linked clicked
     */
    gtmTrackExitLink(page, link) {
      if (typeof page !== 'string') {
        throw new Error('gtmTrackExitLink: page param must be a string');
      }

      if (typeof link !== 'string') {
        throw new Error('gtmTrackExitLink: link param must be a string');
      }

      this.gtmTrackEvent({
        category: 'exit-link',
        action: page,
        label: link,
      });
    },

    /**
     * GTM Track Tap To Call
     *
     * @param {string} type | type of contact interaction
     */
    gtmTrackTapToCall(type) {
      if (typeof type !== 'string') {
        throw new Error('gtmTrackTapToCall: type param must be a string');
      }

      this.gtmTrackEvent({
        category: 'contact-us',
        action: 'click',
        label: type,
      });
    },

    /**
     * GTM Track Carousel Button
     *
     * @param {string} carousel | carousel name
     * @param {number} event | event data
     */
    gtmTrackCarouselButton(carousel, event) {
      if (typeof carousel !== 'string') {
        throw new Error('gtmTrackCarouselButton: pageLocation param must be a string');
      }

      this.gtmTrackEvent({
        category: 'carousel',
        action: carousel,
        label: (event.componentName === 'navigation' ? 'arrow button' : 'radio button'),
      });
    },

    /**
     * GTM Track Carousel Slide
     *
     * @param {string} carousel | carousel name
     * @param {string} slide | the slide
     */
    gtmTrackCarouselSlide(carousel, slide) {
      if (typeof carousel !== 'string') {
        throw new Error('gtmTrackCarouselSlide: pageLocation param must be a string');
      }

      this.gtmTrackEvent({
        category: 'carousel',
        action: carousel,
        label: slide,
      });
    },

    /**
     * GTM Track Checkout Step
     *
     * @param {number} step | checkout step number
     * @param {string} option | name of option in format of `Reservation|Quote RESERVATION_#`
     * @param {object} products | unformatted object with { name, catClass, price, category, subCategory, quantity }
     */
    gtmTrackCheckoutStep(step, option, products = []) {
      this.gtmPush({
        event: 'checkout',
        ecommerce: {
          checkout: {
            actionField: {
              step,
              option,
            },
            products: products.map(this._gtmFormatProduct),
          },
        },
      });
    },

    /**
     * GTM Track Cart Action
     *
     * @param {string} field | the id of the field in the form
     * @param {string} option | the option selected
     */
    gtmTrackCartAction(field, option) {
      this.gtmTrackEvent({
        category: 'cartAction',
        action: field,
        label: option,
      });
    },

    /**
     * GTM Track Form Submissions
     *
     * @param {string} formName | form name
     * @param {string} linkClicked | link clicked
     */
    gtmTrackFormSubmissions(formName, linkClicked) {
      if (typeof formName !== 'string') {
        throw new Error('gtmTrackFormSubmissions: formName param must be a string');
      }

      if (typeof linkClicked !== 'string') {
        throw new Error('gtmTrackFormSubmissions: linkClicked param must be a string');
      }

      this.gtmTrackEvent({
        category: 'form-submission',
        action: formName,
        label: linkClicked,
      });
    },

    /**
     * GTM Track Product Interest Interactions
     *
     * @param {string} interactionType | interaction type
     * @param {string} productName | product name
     */
    gtmTrackProductInterestInteractions(interactionType, productName) {
      if (typeof interactionType !== 'string') {
        throw new Error('gtmTrackProductInterestInteractions: interactionType param must be a string');
      }

      if (typeof productName !== 'string') {
        throw new Error('gtmTrackProductInterestInteractions: productName param must be a string');
      }

      this.gtmTrackEvent({
        category: 'product-interest-interaction',
        action: interactionType,
        label: productName,
      });
    },

    /**
     * GTM Track Filter Categories Interactions
     *
     * @param {string} equipmentAttributeCategory | equipment attribute category
     * @param {string} filterClicked | filter clicked
     */
    gtmTrackFilterCategoriesInteractions(equipmentAttributeCategory, filterClicked) {
      if (typeof equipmentAttributeCategory !== 'string') {
        throw new Error('gtmTrackFilterCategoriesInteractions: equipmentAttributeCategory param must be a string');
      }

      if (typeof filterClicked !== 'string') {
        throw new Error('gtmTrackFilterCategoriesInteractions: filterClicked param must be a string');
      }

      this.gtmTrackEvent({
        category: 'product-filter-interaction',
        action: equipmentAttributeCategory,
        label: filterClicked,
      });
    },

    /**
     * GTM Track Product Details Expand
     *
     * @param {string} productName | product name
     */
    gtmTrackProductDetailsExpand(productName) {
      if (typeof productName !== 'string') {
        throw new Error('gtmTrackProductDetailsExpand: productName param must be a string');
      }

      this.gtmTrackEvent({
        category: 'product-detail',
        action: 'detail-expand',
        label: productName,
      });
    },

    gtmTrackProductClick({
      name, catClass, price, category, subCategory, url,
    }) {
      this.gtmPush({
        event: 'productClick',
        ecommerce: {
          click: {
            actionField: { list: subCategory },
            products: [{
              name, // Name or ID is required.
              id: catClass, // Product SKU
              price, // Product Price
              category, // Product Category (Equipment)
              quantity: 1, // Product Quantity
              list: subCategory,
              dimension1: this.rmGuid,
              dimension3: this.storeID,
              dimension5: catClass, // Product SKU
              dimension6: price, // Product Price
              dimension7: `${catClass}|${price}`, // Product SKU|Price
              dimension11: this.accountID,
            }],
          },
        },
        eventCallback() {
          document.location = url;
        },
      });
    },

    gtmTrackProductDetails({
      name, catClass, price, category, subCategory,
    }) {
      this.gtmPush({
        event: 'productView',
        ecommerce: {
          detail: {
            actionField: { list: subCategory },
            products: [{
              name, // Name or ID is required.
              id: catClass, // Product SKU
              price, // Product Price
              category, // Product Category (Equipment)
              list: subCategory,
              dimension1: this.rmGuid,
              dimension3: this.storeID,
              dimension5: catClass, // Product SKU
              dimension6: price, // Product Price
              dimension7: `${catClass}|${price}`, // Product SKU|Price
              dimension11: this.accountID,
            }],
          },
        },
      });
    },

    gtmTrackProductImpression({
      categoryName, items, rates, ratesPlaceBranchId, subCategoryName,
    }) {
      const impressions = [];
      forEach(items, (item, key) => {
        const { catClass, title } = item;
        const price = get(rates, [catClass, ratesPlaceBranchId, 'dayRate'], '');

        impressions.push({
          name: title, // Name or ID is required
          id: catClass, // Product SKU
          price, // Product Price
          category: categoryName, // Product Category (Equipment)
          list: subCategoryName, // Product Sub Category
          position: key + 1, // Position in results
          dimension1: this.rmGuid,
          dimension3: this.storeID,
          dimension5: catClass, // Product SKU
          dimension6: price, // Product Price
          dimension7: `${catClass}|${price}`, // Product SKU|Price
          dimension11: this.accountID,
        });
      });

      this.gtmPush({
        event: 'productImpressions',
        ecommerce: {
          impressions,
        },
      });
    },

    gtmTrackAddProductToCart({
      name, catClass, price, category, subCategory, quantity,
    }) {
      this.gtmPush({
        event: 'addToCart',
        ecommerce: {
          add: {
            products: [{
              name, // Name or ID is required.
              id: catClass, // Product SKU
              price, // Product Price
              category, // Product Category (Equipment)
              list: subCategory, // Product Sub Category
              quantity, // Product Quantity
              dimension1: this.rmGuid,
              dimension3: this.storeID,
              dimension5: catClass, // Product SKU
              dimension6: price, // Product Price
              dimension7: `${catClass}|${price}`, // Product SKU|Price
              dimension11: this.accountID,
            }],
          },
        },
      });
    },

    gtmTrackRemoveProductFromCart({
      name, catClass, price, category, subCategory, quantity,
    }) {
      this.gtmPush({
        event: 'removeFromCart',
        ecommerce: {
          remove: {
            products: [{
              name, // Name or ID is required.
              id: catClass, // Product SKU
              price, // Product Price
              category, // Product Category (Equipment)
              list: subCategory, // Product Sub Category
              quantity, // Product Quantity
              dimension1: this.rmGuid,
              dimension3: this.storeID,
              dimension5: catClass, // Product SKU
              dimension6: price, // Product Price
              dimension7: `${catClass}|${price}`, // Product SKU|Price
              dimension11: this.accountID,
            }],
          },
        },
      });
    },

    gtmTrackReservationProgress(step, option) {
      this.gtmPush({
        event: 'checkout',
        userID: this.userID,
        rmGuid: this.rmGuid,
        storeID: this.storeID,
        ecommerce: {
          checkout: {
            actionField: {
              step,
              option,
            },
          },
        },
      });
    },

    /**
     * GTM Track Purchase
     *
     * Tracks the users purchase
     *
     * @param {object} transaction | transactional information
     * @param {array} products | array of products in the reservation
     * @param {string} transId | transaction id string
     * @param {string} reqId | request id string
     */
    gtmTrackPurchase(transaction, products, transId, reqId) {
      const formattedProducts = [];
      products.forEach(product => {
        formattedProducts.push(this._gtmFormatProduct(product, transId, reqId));
      });
      this.gtmPush({
        event: 'purchase',
        ecommerce: {
          purchase: {
            actionField: transaction,
            products: formattedProducts,
          },
        },
      });
    },
    gtmTrackQuote(transaction, products, transId, reqId) {
      const formattedProducts = [];
      products.forEach(product => {
        formattedProducts.push(this._gtmFormatProduct(product, transId, reqId));
      });
      this.gtmPush({
        event: 'quote',
        ecommerce: {
          quote: {
            actionField: transaction,
            products: formattedProducts,
          },
        },
      });
    },
    gtmTrackBranchPickup(eventAction, pickupBranch) {
      this.gtmPush({
        event: 'GAEvent',
        eventCategory: 'manage',
        eventAction,
        eventLabel: pickupBranch,
      });
    },

    /**
     * GTM Track Calculate Costs
     *
     * Tracks 'calculate costs' button clicked in cart estimator
     *
     * @param {string} productName | product name
     */
    gtmTrackCalculateCosts(productName) {
      if (typeof productName !== 'string') {
        throw new Error('gtmCalculateCosts: productName param must be a string');
      }
      this.gtmTrackEvent({
        category: 'ecommerce',
        action: 'calculate-costs',
        label: productName,
      });
    },

    /**
     * GTM Track Toast Click
     *
     * Tracks toasts' button and link clicks
     *
     * @param {string} clickText | 'View All Items', 'Start Checkout', or 'Get Estimate'
     */
    gtmTrackToastClick(clickText) {
      this.gtmTrackEvent({
        category: 'ecommerce',
        action: 'toast',
        label: clickText,
      });
    },

    /**
     * GTM Track Cart Drawer
     *
     * Tracks when a user clicks (no hover state tracking) the cart icon to open the drawer
     *
     * @param {number} cartSize | cart size
     */
    gtmTrackCartDrawer(cartSize) {
      this.gtmTrackEvent({
        category: 'ecommerce',
        action: 'cart-drawer',
        label: cartSize,
      });
    },

    /**
     * GTM Track Cart Drawer Start Checkout
     *
     * Tracks Cart Preview Drawer's 'Start Checkout' button click
     *
     * @param {number} cartSize | cart size
     */
    gtmTrackCartDrawerStartCheckout(cartSize) {
      this.gtmTrackEvent({
        category: 'ecommerce',
        action: 'cart-drawer-start-checkout',
        label: cartSize,
      });
    },
  },
};

export default UrGtm;
