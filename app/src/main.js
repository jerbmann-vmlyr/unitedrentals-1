import 'es6-promise/auto';
import '@babel/polyfill/noConflict'; // polyfills IE 11 (some plugins use Object.assign)
import 'reflect-metadata';
import Vue from 'vue';
import store from '@/store';
import router from '@/router';
import App from './app.vue';
import { UrGtmPlugin } from '@/lib/google-tag-manager';

import './setup.js';
import './views';

Vue.use(UrGtmPlugin, { store, debug: true });

/**
 * Instance
 */
(window)._app = new Vue({
  el: '#app',
  ...App,
  router,
  store,
});
