import Vue from 'vue';
import VuePortal from '@linusborg/vue-simple-portal';
import UrLegacyStyles from '@/components/common/ur-legacy-styles';
import ToastManager from '@/components/toasts/toast-manager';

Vue.use(VuePortal, { name: 'portal' });

Vue.component('ur-legacy-styles', UrLegacyStyles);


Vue.component('ur-view', {
  name: 'UrView',
  template: `<router-view :key="$route.fullPath" />`,
});

// Including this anywhere should cover bases for all modal/popup/overlay stuff
Vue.component('ur-overlays-container', {
  name: 'UrOverlaysContainer',
  components: { ToastManager },
  template: `
    <portal ref="portal-overlays">
      <ur-legacy-styles>
        <toast-manager ref="toasts"/>
        <router-view name="modal" ref="modals" />
      </ur-legacy-styles>
      <slot></slot>
    </portal>`,
});

export const UrLegacyPortal = {
  name: 'ur-legacy-portal',
  template: `
    <portal prepend>
      <template v-if="wrapStyles">
        <ur-legacy-styles>
          <slot/>
        </ur-legacy-styles>
      </template>
      <template v-else>
        <slot />
      </template>
    </portal>`,
  props: {
    wrapStyles: {
      type: Boolean,
      default: true,
    },
  },
};

Vue.component('ur-legacy-portal', UrLegacyPortal);


export default {
  UrLegacyStyles,
  UrLegacyPortal,
};
