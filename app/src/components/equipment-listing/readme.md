# Rental Equipment Listing Page

## 10,000ft view of the component tree

`<equipment-listing>` is the top-level, smart component (rendered by Drupal). It renders `<listing-item>`s.
It communicates with the `module.catalog.js` Vuex Store to get data from Drupal's API.

`<equipment-listing-item>`s render their own `<quick-view>` accordion and ID it by their `catClass`. These items
also have a trigger for the `<modal>`.

`<modal>`s. How will these work? We need to support modals-over-modals...
    - use the router query param
        - by using a query param we can tack on a modal anywhere, regardless of url structure
        - /modal/jobsite/:accountId/:jobId

        <modal><router-view name="modal"></router-view></modal>

Selected portions of Vuex State will be persisted via the vuex session plugin.

### Vuex Stores that play a part in a user's rental workflow, in the context of the Equipment Listing Page

1. `module.catalog.js` - lists equipment
2. `module.quickview.js` - tracks equipment quickview accordion states, organized by catClass
3. `module.cart.ts` - facilitates CRUD for a user's cart (adding/removing equipment)
4. `module.checkout.js` - facilitates moving the user through the checkout funnel
