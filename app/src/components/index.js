import AccountSelectorModal from '@/components/account-selector/account-selector-modal';
import AccountSelectorTrigger from '@/components/account-selector/account-selector-trigger';
import AddToCartButton from '@/components/add-to-cart/add-to-cart-button';
import AddToCartForm from '@/components/add-to-cart/add-to-cart-form';
import AuthenticationPanel from '@/components/faceted-search/authentication-panel';
import BasicPageHero from '@/components/hero/basic-page-hero';
import Checkout from '@/components/cart/checkout';
import CartIcon from '@/components/cart/cart-icon';
import CostEstimator from '@/components/add-to-cart/cost-estimator';
import Dashboard from '@/components/workplace/dashboard/dashboard.vue';
import EquipmentCarousel from '@/components/equipment-listing/equipment-carousel';
import EquipmentListing from '@/components/equipment-listing/equipment-listing';
import EquipmentSearch from '@/components/faceted-search/equipment-search';
import FacetedSearch from '@/components/faceted-search/faceted-search';
import FavoritesListing from '@/components/favorites-listing/favorites-listing';
import GtmPhoneNumber from '@/components/gtm/gtm-phone-number';
import ItemRates from '@/components/equipment-listing/item-rates';
import ItemRatesLoader from '@/components/equipment-listing/item-rates-loader';
import EquipmentDetailsRates from '@/components/equipment-listing/equipment-details-rates';
import LocationFinder from '@/components/location-finder/location-finder';
import LocationSelector from '@/components/add-to-cart/location-selector';
import Messaging from '@/components/messaging/messaging';
import UserProfile from '@/components/user-profile/user-profile';
import ObjectListingActions from '@/components/equipment-listing/object-listing-actions';
import ToastManager from '@/components/toasts/toast-manager';
import TranslationGlobalFooter from '@/components/translation/translation-global-footer';
import TranslationUserMenu from '@/components/translation/translation-user-menu';
import UrCategoryBlock from '@/components/common/ur-category-block/ur-category-block';
import UrGlobalSearch from '@/components/common/ur-global-search';
import UrGlobalSearchHero from '@/components/hero/ur-global-search-hero';
import UrDrawerBanner from '@/components/common/ur-drawer-banner.vue';
import UrFullBleedCard from '@/components/common/ur-full-bleed-card/ur-full-bleed-card';
import UrLocationSearchAutoComplete from '@/components/global-header/container.ur-autocomplete';
import UrModal from '@/components/common/ur-modal';
import UrQuickView from '@/components/common/ur-quickview';
import UrUseCaseBlock from '@/components/common/ur-use-case-block/ur-use-case-block';
import Reservations from '@/components/platform-account/reservations/reservations';
import Quotes from '@/components/platform-account/quotes/quotes';
import QuoteConvert from '@/components/platform-account/quotes/quote-convert';
import HomepageBanner from '@/components/homepage-banner/homepage-banner.vue';
import ActivityPreferences from '@/components/activity-preferences/activity-preferences';
import AutoloadLocationModal from '@/components/common/autoload-location-modal';
import EquipmentCatalogItemNotFound from '@/components/workplace/equipment-listing/equipment-catalog-item-not-found';
import SearchFetchEquipment from '@/components/equipment-listing/search-fetch-equipment';
import SearchObjectListingActions from '@/components/equipment-listing/search-object-listing-actions';
import Workplace from '@/components/workplace/workplace';

export default {
  AccountSelectorModal,
  AccountSelectorTrigger,
  AddToCartButton,
  AddToCartForm,
  AuthenticationPanel,
  BasicPageHero,
  Checkout,
  CartIcon,
  CostEstimator,
  Dashboard,
  EquipmentCarousel,
  EquipmentCatalogItemNotFound,
  EquipmentListing,
  EquipmentSearch,
  FacetedSearch,
  FavoritesListing,
  GtmPhoneNumber,
  ItemRates,
  ItemRatesLoader,
  EquipmentDetailsRates,
  LocationFinder,
  LocationSelector,
  Messaging,
  UserProfile,
  ObjectListingActions,
  ToastManager,
  TranslationGlobalFooter,
  TranslationUserMenu,
  UrCategoryBlock,
  UrGlobalSearch,
  UrGlobalSearchHero,
  UrDrawerBanner,
  UrFullBleedCard,
  UrLocationSearchAutoComplete,
  UrModal,
  UrQuickView,
  UrUseCaseBlock,
  Reservations,
  Quotes,
  QuoteConvert,
  HomepageBanner,
  ActivityPreferences,
  AutoloadLocationModal,
  SearchFetchEquipment,
  SearchObjectListingActions,
  Workplace,
};
