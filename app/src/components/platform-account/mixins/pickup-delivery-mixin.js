import { dateFormat, dateTime } from '@/utils';
import moment from 'moment';
import { get } from 'lodash';

const pdFunctions = {
  computed: {
    outOfRange() {
      return this.startOutOfRange || this.endOutOfRange || this.badDateRange || this.invalidTime;
    },
    startOutOfRange() {
      return moment(this.adjustedPriceValues.deliveryDate, dateFormat.iso)
        .isBefore(this.convertedDate);
    },
    endOutOfRange() {
      return moment(this.adjustedPriceValues.pickupDate, dateFormat.iso)
        .isBefore(this.convertedDate);
    },
    badDateRange() {
      return moment(this.adjustedPriceValues.deliveryDate, dateFormat.iso)
        .isSameOrAfter(moment(this.adjustedPriceValues.pickupDate, dateFormat.iso));
    },
    branchOffset() {
      if (!this.branches[this.adjustedPriceValues.branchId]) {
        return;
      }

      let branchOffset = this.branches[this.adjustedPriceValues.branchId].timeOffset;

      if (branchOffset) {
        branchOffset = Number(branchOffset) + (this.hoursToSubtract);
        branchOffset = branchOffset.toString().replace('00', '');
        branchOffset = branchOffset.toString().replace('-', '');
        return branchOffset;
      }

      return this.hoursToSubtract;
    },
    startDateTimeToDisplay() {
      const startDateTime = moment(this.quote.startDateTime, dateFormat.iso);
      return moment(startDateTime);
    },
    convertedDate() {
      // Default timeOffset = UTC Timezone
      const date = {
        time: moment.utc().format(dateFormat.iso),
        offset: this.branchOffset,
      };

      // default offset = UTC ==> only subtract
      return moment(moment(date.time).subtract(date.offset, 'hours').format(dateFormat.iso));
    },
    hoursToSubtract() {
      // Depending on the DST truthy or falsy, hours that we should subtract from UTC can be -400 or -500
      // HoursToSubtract finds the count of that hours
      return moment().isDST() ? -400 : -500;
    },
    branchOpenHours() {
      const hours = get(this.branches[this.quote.branchId], 'weekdayHours', {});
      const satHours = get(this.branches[this.quote.branchId], 'saturdayHours', {});
      const sunHours = get(this.branches[this.quote.branchId], 'sundayHours', {});

      const usualStartTime = dateTime.toDecimal(hours.open);
      const satStartTime = satHours.open ? dateTime.toDecimal(satHours.open) : '';
      const sunStartTime = sunHours.open ? dateTime.toDecimal(sunHours.open) : '';

      const durationOfUsualTime = dateTime.toDecimal(hours.close) - usualStartTime;
      const durationOfSaturdaysTime = satStartTime ? dateTime.toDecimal(satHours.close) - satStartTime : '';
      const durationOfSundaysTime = sunStartTime ? dateTime.toDecimal(sunHours.close) - sunStartTime : '';

      return {
        start: usualStartTime,
        satStart: satStartTime,
        sunStart: sunStartTime,
        duration: durationOfUsualTime,
        satDuration: durationOfSaturdaysTime,
        sunDuration: durationOfSundaysTime,
      };
    },
    minMoment() {
      // Default timeOffset = UTC Timezone
      const date = moment(this.convertedDate);
      /**
       * If it's "after hours" in the time zone of
       * the branch the minimum date is tomorrow.
       * Otherwise it's still today
       *
       * @TODO: The lastHour value should not be hard coded, but we haven't implemented the branch hours lookup
       * As soon as we look up the "real" branch operating hours this variable should be replaced.
       */
      const lastHour = 17;
      if (date.hour() >= lastHour) {
        return date.add(1, 'days');
      }
      return date;
    },
  },
};

export default pdFunctions;
