import Vue from 'vue';
import { Colors } from '@/lib/constants';

export default Vue.extend({
  data() {
    return {
      equipmentDueOptions: {
        legend: {
          display: false,
        },
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [{
            barThickness: 80,
            ticks: {
              autoSkip: false,
              callback: (this as any).formatXAxisTick,
            },
          }],
          yAxes: [{
            display: false,
            ticks: {
              beginAtZero: true,
            },
          }],
        },
        tooltips: {
          backgroundColor: Colors.White,
          bodyFontColor: Colors.Gray69,
          bodyFontSize: 15,
          borderColor: Colors.Gray11,
          borderWidth: 1,
          caretSize: 10,
          displayColors: false,
          titleFontColor: Colors.Black,
          titleFontSize: 40,
          titleFontStyle: 'normal',
          xPadding: 20,
          yPadding: 10,
          callbacks: {
            title(tooltipItem, data) {
              return data.datasets[0].data[tooltipItem[0].index];
            },
            label(tooltipItem, data) {
              return data.labels[tooltipItem.index];
            },
          },
        },
      },
      options: {
        legend: {
          display: false,
        },
        maintainAspectRatio: false,
        responsive: true,
        tooltips: {
          backgroundColor: Colors.White,
          bodyFontColor: Colors.Gray69,
          bodyFontSize: 15,
          borderColor: Colors.Gray11,
          borderWidth: 1,
          caretSize: 10,
          displayColors: false,
          titleFontColor: Colors.Black,
          titleFontSize: 40,
          titleFontStyle: 'normal',
          xPadding: 20,
          yPadding: 10,
          callbacks: {
            title(tooltipItem, data) {
              return data.datasets[0].data[tooltipItem[0].index];
            },
            label(tooltipItem, data) {
              return data.labels[tooltipItem.index];
            },
          },
        },
      },
    };
  },
  methods: {
    formatXAxisTick(value) {
      const FIRSTSPACE = /^([^ ]+) /;
      const LENGTH = 15;
      const SPLIT = ':::';
      const WIDTH = 425;

      const isComponentNarrow = this.$el.offsetWidth < WIDTH;
      const isValueLong = value.length > LENGTH;

      if (isComponentNarrow && isValueLong) {
        value = value.replace(FIRSTSPACE, `$1${SPLIT}`);
      }

      return value.split(SPLIT);
    },
  },
});
