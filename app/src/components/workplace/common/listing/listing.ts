import Vue from 'vue';
import { Prop } from 'vue/types/options';
import {
  ColumnConfig,
  ListingConfig,
  ListingConfigToken,
  ListingEmitter,
  ListingEmitterToken,
  ListingLayoutManager,
  ListingLayoutToken,
  ListingModule,
  ListingModuleState,
  ListingRow,
  ListingRowsToken,
  ListingSortDirection,
  ListingStateToken,
  ListingUtils,
} from './lib';
import ListingTableBody from './internal/listing-table-body.vue';
import ListingTableHeader from './internal/listing-table-header.vue';

export default Vue.extend({
  components: {
    ListingTableBody,
    ListingTableHeader,
  },
  props: {
    data: {
      type: Array as Prop<object[] | object[][]>,
      required: true,
      default: () => [],
    },
    dataHeight: {
      type: Number,
      default: 32,
    },
    defaultColumnWidth: {
      type: Number,
      default: 80,
    },
    hiddenColumns: {
      type: Array as Prop<string[]>,
      default: () => [],
    },
    getColumnIndex: {
      type: Function,
      default: () => Infinity,
    },
    getColumnWidth: {
      type: Function,
    },
    getColumnSortable: {
      type: Function,
      default: () => false,
    },
    getTableHeaderLabel: {
      type: Function,
      default: () => '',
    },
    groupHeaderHeight: {
      type: Number,
      default: 36,
    },
    loading: {
      type: Boolean,
      default: false,
    },
    sortColumnKey: {
      type: String,
      default: '',
    },
    sortDirection: {
      type: String as Prop<ListingSortDirection>,
      validator: ListingUtils.isValidSortDirection,
    },
    tableFooterHeight: {
      type: Number,
      default: 44,
    },
    tableHeaderHeight: {
      type: Number,
      default: 44,
    },
  },
  provide() {
    return {
      [ListingConfigToken]: (): ListingConfig => ({
        listingId: this.listingId,
        dataHeight: this.dataHeight,
        defaultColumnWidth: this.defaultColumnWidth,
        getColumnIndex: this.getColumnIndex,
        getColumnSortable: this.getColumnSortable,
        getColumnWidth: typeof this.getColumnWidth === 'function'
          ? this.getColumnWidth
          : () => this.defaultColumnWidth,
        getTableHeaderLabel: this.getTableHeaderLabel,
        groupHeaderHeight: this.groupHeaderHeight,
        hiddenColumns: this.hiddenColumns,
        sortColumnKey: this.sortColumnKey,
        sortDirection: this.sortDirection,
        tableFooterHeight: this.tableFooterHeight,
        tableHeaderHeight: this.tableHeaderHeight,
      }),
      [ListingEmitterToken]: (): ListingEmitter => (event => this.$emit('events', event)),
      [ListingLayoutToken]: (): ListingLayoutManager => this.layout,
      [ListingRowsToken]: (): ListingRow[] => this.$store.getters[`${this.listingId}/rows`],
      [ListingStateToken]: (): ListingModuleState => this.$store.state[this.listingId],
    };
  },
  computed: {
    dataIsEmpty(): boolean {
      return this.data.length < 1;
    },
    columns(): ColumnConfig[] {
      return this.$store.state[this.listingId].columns;
    },
  },
  mounted() {
    this.layout.saveListingElement(this.$el);
    this.$store.dispatch(`${this.listingId}/updateData`, this.data);
    this.layout.update();

    window.addEventListener('resize', () => this.layout.update());
  },
  created() {
    this.$store.registerModule(this.listingId, ListingModule);
    this.$watch('columns', columns => this.layout.updateColumns(columns || []), { immediate: true });
  },
  destroyed() {
    this.$store.unregisterModule(this.listingId);
  },
  data() {
    return {
      layout: new ListingLayoutManager(),
      listingId: `listing-${Math.floor(1e6 * Math.random())}`,
    };
  },
  watch: {
    data(data) {
      this.$store.dispatch(`${this.listingId}/updateData`, data);
    },
  },
});
