import Vue, { VNode, CreateElement } from 'vue';
import { get } from 'lodash';
import {
  ColumnConfig,
  DataCellScope,
  ListingConfig,
  ListingConfigToken,
  ListingUtils,
  TableHeaderCellScope,
} from './lib';

export default Vue.extend({
  name: 'grid-item',
  inject: { injectedConfig: ListingConfigToken },
  props: {
    align: {
      type: String,
      default: 'left',
      validator: ListingUtils.isValidCellAlignment,
    },
    columnKey: {
      type: String,
      required: true,
    },
    dataCellClass: {
      type: String,
      default: '',
    },
    label: {
      type: String,
    },
    link: {
      type: Boolean,
      default: false,
    },
    maxWidth: {
      type: Number,
      default: 400,
    },
    minWidth: {
      type: Number,
      default: 40,
    },
    pinned: {
      default: false,
      validator: ListingUtils.isValidPinnedState,
    },
    property: {
      type: [String, Function],
      default: '',
    },
    resizable: {
      type: Boolean,
      default: true,
    },
    resizeIncrements: {
      type: Number,
      default: 10,
    },
    tableHeaderCellClass: {
      type: String,
      default: '',
    },
    width: {
      type: Number,
    },
  },
  computed: {
    config(): ListingConfig {
      return (this as any).injectedConfig();
    },
    hasHeaderLabelOverride(): boolean {
      return typeof this.label === 'string';
    },
    hasWidthOverride(): boolean {
      return typeof this.width === 'number';
    },
    hidden(): boolean {
      return this.config.hiddenColumns.includes(this.columnKey);
    },
  },
  methods: {
    /**
     * Table header cell content precedence:
     * 1. Scoped `header` slot
     * 2. `header` slot
     * 3. `label` override prop
     * 4. `getTableHeaderLabel` function from injected config
     */
    renderTableHeaderCell(h: CreateElement, scope: TableHeaderCellScope): VNode {
      const slot = this.$scopedSlots.header || (this.$slots.header && (() => this.$slots.header));

      return slot
        ? slot(scope)
        : this.hasHeaderLabelOverride
          ? this.label
          : this.config.getTableHeaderLabel(scope);
    },
    /**
     * Data cell content precedence:
     * 1. Scoped `data` slot
     * 2. Scoped `default` slot
     * 3. `data` slot
     * 4. `default` slot
     * 5. `property` prop (if function) applied to item
     * 6. `property` prop (if string) used as getter on item
     * 7. Empty string
     */
    renderDataCell(h: CreateElement, scope: DataCellScope): VNode {
      const slot = this.$scopedSlots.data
        || this.$scopedSlots.default
        || (this.$slots.data && (() => this.$slots.data))
        || (this.$slots.default && (() => this.$slots.default));

      let contents = slot
        ? slot(scope)
        : typeof this.property === 'function'
          ? this.property(scope.item)
          : typeof this.property === 'string' && this.property.length > 0
            ? get(scope.item, this.property, '')
            : '';
      if (contents === '') contents = '-';
      return contents;
    },
    generateColumnConfig(): ColumnConfig {
      return {
        align: this.align,
        columnKey: this.columnKey,
        dataCellClass: this.dataCellClass,
        index: this.config.getColumnIndex(this.columnKey),
        isResizing: false,
        link: this.link,
        maxWidth: this.maxWidth,
        minWidth: this.minWidth,
        pinned: this.pinned,
        renderDataCell: this.renderDataCell.bind(this),
        renderTableHeaderCell: this.renderTableHeaderCell.bind(this),
        resizable: this.resizable,
        resizeIncrements: this.resizeIncrements,
        selected: false,
        sortable: this.config.getColumnSortable(this.columnKey),
        tableHeaderCellClass: this.tableHeaderCellClass,
        width: this.hasWidthOverride ? this.width : this.config.getColumnWidth(this.columnKey),
      };
    },
    async dispatchColumnUpdate(): Promise<void> {
      return this.$store.dispatch(`${this.config.listingId}/updateColumn`, this.generateColumnConfig());
    },
  },
  created() {
    this.$options.render = h => h('div');
  },
  beforeDestroy() {
    this.$store.dispatch(`${this.config.listingId}/removeColumn`, this.columnKey);
  },
  watch: {
    pinned() { this.dispatchColumnUpdate(); },
    width() { this.dispatchColumnUpdate(); },
    hidden: {
      immediate: true,
      handler(isHidden, wasHidden) {
        if (isHidden) {
          this.$store.dispatch(`${this.config.listingId}/removeColumn`, this.columnKey);
        }
        else {
          this.$store.dispatch(`${this.config.listingId}/insertColumn`, this.generateColumnConfig());
        }
      },
    },
  },
});
