import { ColumnConfig, ScrollPositionY, ScrollPositionX } from './listing.types';

export class ListingLayoutManager {
  static emptyClientRect(): ClientRect {
    return {
      bottom: 0,
      height: 0,
      top: 0,
      left: 0,
      right: 0,
      width: 0,
    };
  }

  static findParent(
    predicate: (el: HTMLElement) => boolean,
    el: HTMLElement,
  ): HTMLElement | null {
    if (predicate(el)) return el;
    else {
      return el.parentElement
        ? ListingLayoutManager.findParent(predicate, el.parentElement)
        : null;
    }
  }

  /* Root listing element */
  listingEl: HTMLElement | null = null;
  listingRect: ClientRect = ListingLayoutManager.emptyClientRect();

  /* Resize operations */
  resizeProxyLeft = 0;
  resizeProxyVisible = false;

  /* Column tracking */
  private columns: ColumnConfig[] = [];

  /* Column tracking --- Column configs grouped by partition */
  centerColumns: ColumnConfig[] = [];
  leftPinnedColumns: ColumnConfig[] = [];
  rightPinnedColumns: ColumnConfig[] = [];

  /* Column tracking --- Column widths grouped by partition */
  centerWidths: number[] = [];
  leftPinnedWidths: number[] = [];
  rightPinnedWidths: number[] = [];

  /* Column tracking --- Column width totals grouped by partition */
  centerTotalWidth = 0;
  leftPinnedTotalWidth = 0;
  rightPinnedTotalWidth = 0;
  rootTableColumnWidths: number[] = [];

  /* Column tracking --- Width of the center table wrapper (excluding overflow) */
  centerTableWidth = 0;

  /* Column tracking --- Column key of the currently-hovered table header */
  hoveredColumnKey: string | null = null;

  /* Scroll operations and getters */
  private internalScrollLeft = 0;
  scrollPositionY: ScrollPositionY = 'ally';
  readonly scrollLeftSyncClass = 'scroll-left-sync';
  readonly scrollTopSyncClass = 'scroll-top-sync';

  get isAtBottom() { return this.scrollPositionY === 'bottom'; }
  get isAtTop() { return this.scrollPositionY === 'top'; }
  get isAtMiddle() { return this.scrollPositionY === 'middle'; }


  startDragging(mousedownEvent: MouseEvent) {
    // No element or if a column resize event was triggered, abort mission
    if (!mousedownEvent.srcElement || this.resizeProxyVisible) return;
    mousedownEvent.preventDefault();

    // Search up the DOM tree for the element whose vertical scroll will be manipulated
    // If the user grabbed the listing header, we only have to worry about horizontal scrolling
    const vscrollRoot = ListingLayoutManager.findParent(
      el => el.classList.contains(this.scrollTopSyncClass),
      mousedownEvent.srcElement as HTMLElement,
    );

    // Save the inital client origin coordinates
    let { clientX, clientY } = mousedownEvent;

    const handleDrag = (e: MouseEvent) => {
      e.preventDefault();

      // Calculate the coordinate diffs
      const dClientX = clientX - e.clientX;
      const dClientY = clientY - e.clientY;

      // Reset the origin coordinates
      clientX = e.clientX;
      clientY = e.clientY;

      // If the shift key is being held, increase the scroll speed
      const speed = e.shiftKey ? 3 : 1;

      // Apply update horizontal scroll position
      this.scrollLeft = this.internalScrollLeft + speed * dClientX;

      // Apply update vertical scroll position
      if (vscrollRoot) {
        vscrollRoot.scrollTop = vscrollRoot.scrollTop + dClientY;
      }
    };

    const stopDragging = (e: MouseEvent) => {
      document.removeEventListener('mousemove', handleDrag);
      document.removeEventListener('mouseup', stopDragging);
    };

    document.addEventListener('mousemove', handleDrag);
    document.addEventListener('mouseup', stopDragging);
  }

  get scrollLeft() {
    return this.internalScrollLeft;
  }

  set scrollLeft(scrollLeft) {
    this.internalScrollLeft = scrollLeft;
    const els = document.querySelectorAll(`.${this.scrollLeftSyncClass}`) || [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < els.length; i++) {
      els[i].scrollLeft = scrollLeft;
    }
  }

  get scrollPositionX(): ScrollPositionX {
    return this.centerTotalWidth < this.centerTableWidth
      ? 'allx'
      : this.scrollLeft < 1
        ? 'left'
        : (this.scrollLeft + this.centerTableWidth) >= (this.centerTotalWidth - 1)
          ? 'right'
          : 'center';
  }

  handleSubtableScroll($event: Event) {
    $event.preventDefault();
    const srcElement = $event.srcElement!;
    const scrollLeft = srcElement.scrollLeft;

    if (scrollLeft !== this.internalScrollLeft) {
      this.scrollLeft = scrollLeft;
    }
  }

  saveListingElement(el: HTMLElement) {
    this.listingEl = el;
    this.update();
  }

  update() {
    this.listingRect = this.listingEl
      ? this.listingEl.getBoundingClientRect()
      : this.listingRect;

    const columns = this.columns;
    this.leftPinnedColumns = columns.filter(({ pinned }) => pinned === 'left');
    this.rightPinnedColumns = columns.filter(({ pinned }) => pinned === 'right');
    this.centerColumns = columns.filter(({ pinned }) => !pinned);

    this.leftPinnedWidths = this.leftPinnedColumns.map(col => col.width);
    this.rightPinnedWidths = this.rightPinnedColumns.map(col => col.width);
    this.centerWidths = this.centerColumns.map(col => col.width);

    this.centerTotalWidth = this.centerWidths.reduce((sum, w) => sum + w, 0);
    this.leftPinnedTotalWidth = this.leftPinnedWidths.reduce((sum, w) => sum + w, 0);
    this.rightPinnedTotalWidth = this.rightPinnedWidths.reduce((sum, w) => sum + w, 0);

    this.centerTableWidth = this.listingRect.width - this.leftPinnedTotalWidth - this.rightPinnedTotalWidth;

    this.rootTableColumnWidths = [
      ...this.leftPinnedWidths,
      this.centerTableWidth,
      ...this.rightPinnedWidths,
    ].filter(width => width > 0);
  }

  updateColumns(columns: ColumnConfig[]) {
    this.columns = columns;
    this.update();
  }
}
