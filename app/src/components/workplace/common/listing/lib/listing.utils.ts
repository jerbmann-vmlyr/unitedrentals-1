import {
  CellAlignment,
  CellType,
  ListingRow,
  ListingSortDirection,
  PinnedState,
  RowType,
} from './listing.types';
import { VNode, CreateElement } from 'vue';

export class ListingUtils {

  static dataIsGrouped(data: unknown[] | unknown[][]): data is unknown[][] {
    if (!Array.isArray(data) || data.length < 1) {
      return false;
    }
    return Array.isArray(data[0]);
  }

  static flattenOnce<T>(list: T[][]): T[] {
    return list.reduce((accum, value) => accum.concat(value), []);
  }

  static compileDataToRows(data: unknown[] | unknown[][]): ListingRow[] {
    // If the data isn't grouped, force it to look like grouped data (nested arrays)
    const groups = ListingUtils.dataIsGrouped(data) ? data : [data];
    const includeGroupHeaders = ListingUtils.dataIsGrouped(data);
    const itemTotal = groups.reduce((sum, { length }) => sum + length, 0);

    const compiled = groups.map((group, groupIndex, { length: groupCount }) => {
      const rows = group.map<ListingRow>((item, itemIndex, { length: itemCount }) => ({
        type: RowType.Data,
        groupCount,
        groupIndex,
        item,
        itemCount,
        itemIndex,
        itemTotal,
        isFirst: itemIndex === 0,
        isLast: itemIndex >= itemCount - 1,
        rowIndex: -1,
      }));

      // If the data was initially grouped, prepend a group-header row object
      if (includeGroupHeaders) {
        rows.unshift({
          type: RowType.GroupHeader,
          items: group,
          groupCount,
          groupIndex,
          isFirst: groupIndex === 0,
          isLast: groupIndex >= groupCount - 1,
          itemCount: group.length,
          itemTotal,
          rowIndex: -1,
        });
      }

      return rows;
    });

    return ListingUtils.flattenOnce(compiled).map((row, rowIndex) => ({ ...row, rowIndex }));
  }

  static cellTagFromType(type: CellType): 'td' | 'th' {
    switch (type) {
      case 'table-header':
      case 'group-header':
        return 'th';
      case 'data':
        return 'td';
    }
  }

  static isValidCellAlignment(value: any): value is CellAlignment {
    return ['left', 'right', 'center'].includes(value);
  }

  static isValidCellType(value: any): value is CellType {
    const cellTypes: CellType[] = [ 'data', 'table-header', 'group-header' ];
    return cellTypes.includes(value);
  }

  static isValidPinnedState(value: any): value is PinnedState {
    return value === false || value === 'left' || value === 'right';
  }

  static isValidSortDirection(value: any): value is ListingSortDirection {
    return ['asc', 'desc', false].includes(value);
  }

  static normalizeCellContents(
    h: CreateElement,
    node: string | VNode | VNode[],
  ): VNode {
    node = Array.isArray(node) ? node[0] : node;

    if (typeof node !== 'object' || !node.tag) {
      return h('span', [ node ]);
    }
    else {
      return node;
    }
  }
}
