import { sortBy } from 'lodash';
import { Module } from 'vuex';
import {
  ColumnConfig,
  GroupConfig,
  ListingModuleState,
  ListingRow,
} from './listing.types';
import { ListingUtils } from './listing.utils';

export const ListingModule: Module<ListingModuleState, unknown> = {
  namespaced: true,
  state: () => ({
    columns: [],
    data: [],
    groupConfig: null,
  }),
  actions: {
    insertColumn({ commit, state }, column: ColumnConfig) {
      // Since this is action is dispatched by a watcher, make sure the column doesn't already exist
      if (state.columns.some(({ columnKey }) => column.columnKey === columnKey)) {
        console.warn(`Column with key ${column.columnKey} already exists!`);
        return;
      }
      commit('setColumns', [...state.columns, column]);
    },
    removeColumn({ commit, state }, key: string) {
      commit('setColumns', state.columns.filter(({ columnKey }) => columnKey !== key));
    },
    setSelectedColumn({ commit, state }, { columnKey, selected = false }: { columnKey: string, selected?: boolean }) {
      commit('setColumns', state.columns.map(column =>
        column.columnKey !== columnKey
          ? { ...column, selected: false }
          : { ...column, selected },
      ));
    },
    updateColumn({ commit, state }, update: PartialExclude<'columnKey', ColumnConfig>) {
      commit('setColumns', state.columns.map(column =>
        column.columnKey !== update.columnKey
          ? column
          : { ...column, ...update },
      ));
    },
    updateData({ commit }, data: unknown[] | unknown[][]) {
      commit('setData', data || []);
    },
    updateGroup({ commit, state }, group: Partial<GroupConfig> | null) {
      if (!group) {
        commit('setGroupConfig', null);
      }
      else {
        const currentGroup = state.groupConfig || {};
        commit('setGroupConfig', { ...currentGroup, ...group });
      }
    },
  },
  getters: {
    rows({ data }): ListingRow[] {
      return ListingUtils.compileDataToRows(data);
    },
  },
  mutations: {
    setColumns(state, columns: ColumnConfig[]) {
      state.columns = sortBy([ ...columns ], 'index');
    },
    setData(state, data: unknown[] | unknown[][]) {
      state.data = data;
    },
    setGroupConfig(state, group: GroupConfig | null) {
      state.groupConfig = group;
    },
  },
};
