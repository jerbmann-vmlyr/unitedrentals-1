import { VNode, CreateElement } from 'vue';
import { VirtualScrollRange } from '@/components/common/virtual-scroll';

export type PinnedState = false | 'left' | 'right';

export type CellType = 'data' | 'table-header' | 'group-header';
export type CellAlignment = 'data' | 'table-header' | 'group-header';

export type ScrollPositionX = 'left' | 'center' | 'right' | 'allx';
export type ScrollPositionY = 'top' | 'middle' | 'bottom' | 'ally';

export enum RowType {
  Data = 'data',
  GroupHeader = 'group-header',
  TableHeader = 'table-header',
}

export interface DataRow {
  readonly type: RowType.Data;
  readonly rowIndex: number;
  readonly groupCount: number;
  readonly groupIndex: number;
  readonly item: unknown;
  readonly itemCount: number;
  readonly itemIndex: number;
  readonly itemTotal: number;
  readonly isFirst: boolean;
  readonly isLast: boolean;
}

export interface GroupHeaderRow {
  readonly type: RowType.GroupHeader;
  readonly rowIndex: number;
  readonly items: unknown[];
  readonly groupCount: number;
  readonly groupIndex: number;
  readonly itemCount: number;
  readonly itemTotal: number;
  readonly isFirst: boolean;
  readonly isLast: boolean;
}

export type ListingRow = GroupHeaderRow | DataRow;

export interface ColumnConfig {
  readonly columnKey: string;
  readonly align: CellAlignment;
  readonly dataCellClass: string;
  readonly isResizing: boolean;
  readonly index: number;
  readonly link: boolean;
  readonly maxWidth: number;
  readonly minWidth: number;
  readonly pinned: PinnedState;
  readonly selected: boolean;
  readonly sortable: boolean;
  readonly resizable: boolean;
  readonly resizeIncrements: number;
  readonly tableHeaderCellClass: string;
  readonly width: number;
  readonly renderTableHeaderCell: (h: CreateElement, scope: TableHeaderCellScope) => VNode;
  readonly renderDataCell: (h: CreateElement, scope: DataCellScope) => VNode;
}

export interface GroupConfig {
  readonly renderGroupHeaderCell: (
    h: CreateElement,
    scope: GroupHeaderCellScope,
    listeners: { [key: string]: (scope: GroupHeaderCellScope) => void },
  ) => VNode;
}

export interface GroupHeaderCellScope extends GroupHeaderRow {
  readonly collapsed: boolean;
  readonly group: GroupConfig;
}

export interface TableHeaderCellScope {
  readonly column: ColumnConfig;
  readonly items: unknown[];
}

export interface DataCellScope extends DataRow {
  readonly column: ColumnConfig;
}

export interface ListingModuleState {
  columns: ColumnConfig[];
  groupConfig: GroupConfig | null;
  data: unknown[] | unknown[][];
}

export type ListingSortDirection = 'asc' | 'desc' | false;

export interface ListingConfig {
  dataHeight: number;
  defaultColumnWidth: number;
  groupHeaderHeight: number;
  hiddenColumns: string[];
  listingId: string;
  sortColumnKey: string;
  sortDirection: ListingSortDirection;
  tableFooterHeight: number;
  tableHeaderHeight: number;
  getColumnIndex: (columnKey: string) => number;
  getColumnSortable: (columnKey: string) => boolean;
  getColumnWidth: (columnKey: string) => boolean;
  getTableHeaderLabel: (scope: TableHeaderCellScope) => string;
}

// Event payloads
export interface ColumnResizedPayload {
  column: ColumnConfig,
  newWidth: number,
  previousWidth: number,
}

export type ScrollRangeChangedPayload = VirtualScrollRange;

export interface SortRequestPayload {
  columnKey: string;
  sortDirection: ListingSortDirection;
}

export type ListingEventMap = {
  columnResized: ColumnResizedPayload,
  sortRequest: SortRequestPayload,
  scrollRangeChanged: ScrollRangeChangedPayload,
};

export type ListingEvent<T extends keyof ListingEventMap = keyof ListingEventMap> = {
  event: T,
  payload: ListingEventMap[T],
};

export type ListingEmitter = <K extends keyof ListingEventMap>(event: ListingEvent<K>) => void;

export const ListingConfigToken = Symbol('config');
export const ListingRowsToken = Symbol('row');
export const ListingEmitterToken = Symbol('emitter');
export const ListingStateToken = Symbol('state');
export const ListingLayoutToken = Symbol('layout');
