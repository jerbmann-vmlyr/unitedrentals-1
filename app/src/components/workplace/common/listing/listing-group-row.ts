/* tslint:disable ban-types */
import Vue, { VNode, CreateElement } from 'vue';
import { GroupHeaderCellScope, ListingConfig, ListingConfigToken } from './lib';

export default Vue.extend({
  name: 'listing-group-row',
  inject: { injectedConfig: ListingConfigToken },
  computed: {
    config(): ListingConfig {
      return (this as any).injectedConfig();
    },
  },
  methods: {
    renderGroupHeaderCell(
      h: CreateElement,
      scope: GroupHeaderCellScope,
      listeners: { [key: string]: Function | Function[] },
    ): VNode {
      return this.$scopedSlots.default({ ...scope, ...listeners });
    },
  },
  created() {
    this.$options.render = h => h('div', this.$slots.default);
  },
  mounted() {
    if (typeof this.$scopedSlots.default !== 'function') {
      throw new Error('<listing-group-row> requires use of slot-scope template insertion!');
    }
    this.$store.dispatch(`${this.config.listingId}/updateGroup`, {
      renderGroupHeaderCell: this.renderGroupHeaderCell.bind(this),
    });
  },
  beforeDestroy() {
    this.$store.dispatch(`${this.config.listingId}/updateGroup`, null);
  },
});
