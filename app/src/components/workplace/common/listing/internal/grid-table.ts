import Vue, { VNode, VNodeData } from 'vue';
import { Prop } from 'vue/types/options';

export default Vue.extend({
  name: 'grid-table',
  functional: true,
  props: {
    columnWidths: {
      type: Array as Prop<number[]>,
      required: true,
    },
    isSubtable: {
      type: Boolean,
      default: false,
    },
  },
  render(h, context): VNode {
    const { props, slots } = context;

    const colgroup = h('colgroup', props.columnWidths.map((w, i) =>
      h('col', {
        key: i,
        attrs: { width: `${w}px`, span: 1 },
        style: { width: `${w}px` },
      }),
    ));

    const { default: defaultSlot = [], head = [], body = [] } = slots();

    const thead = h('thead', head);
    const tbody = h('tbody', [...defaultSlot, ...body]);

    const tableWidth = props.columnWidths.reduce((sum, w) => sum + w, 0);

    const additionalClasses = !context.data.class || typeof context.data.class === 'string'
      ? context.data.class || ''
      : Object.entries(context.data.class).filter(([, use]) => use).map(([cssClass]) => cssClass).join(' ');

    const tableData: VNodeData = {
      key: context.data.key,
      class: `listing-row ${additionalClasses || ''}`,
      style: {
        ...(context.data.style as object || {}),
        tableLayout: 'fixed',
        width: `${tableWidth}px`,
      },
      attrs: {
        ...(context.data.attrs as object  || {}),
        cellspacing: 0,
        cellpadding: 0,
        border: 0,
      },
    };

    return h('table', tableData, [
      colgroup,
      thead,
      tbody,
    ]);
  },
});
