import Vue, { VNode, VNodeData } from 'vue';
import { Prop } from 'vue/types/options';
import { ColumnConfig } from '../lib';

export default Vue.extend({
  name: 'listing-cell-resize-handle',
  functional: true,
  props: {
    column: {
      type: Object as Prop<ColumnConfig>,
      required: true,
    },
  },
  render(h, { props, data }): VNode {
    if (props.column.resizable && props.column.selected) {
      const classes = ['cell__resize-handle'];

      if (props.column.isResizing) classes.push('cell__resize-handle--is-resizing');

      const handleData: VNodeData = {
        ...data,
        class: classes.join(' '),
      };

      return h('div', handleData);
    }
    else {
      return h('span', { class: 'hidden' });
    }
  },
});
