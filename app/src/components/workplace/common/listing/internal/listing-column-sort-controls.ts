import Vue, { VNode, VNodeData } from 'vue';
import { Prop } from 'vue/types/options';
import { ListingConfigToken, ListingConfig, ColumnConfig, ListingEmitterToken, ListingEmitter } from '../lib';

export default Vue.extend({
  name: 'listing-column-sort-controls',
  functional: true,
  inject: {
    injectedConfig: ListingConfigToken,
    injectedEmitter: ListingEmitterToken,
  },
  props: {
    column: {
      type: Object as Prop<ColumnConfig>,
      required: true,
    },
  },
  render(h, { props: { column }, injections }): VNode {
    const { sortDirection, sortColumnKey }: ListingConfig = injections.injectedConfig();
    const emitter: ListingEmitter = injections.injectedEmitter();

    const arrowPath = h('path', {
      attrs: {
        d: sortDirection === 'desc'
          ? 'M 0 5 l 5 -5 5  5z'
          : 'M 0 0 l 5  5 5 -5z',
      },
    });

    const svg = h('svg', {
      attrs: {
        version: '1.1',
        role: 'presentation',
        viewBox: '0 0 10 5',
      },
    }, [ arrowPath ]);

    const classes = ['listing-sort-controls', `listing-sort-controls--${sortDirection}`];

    if (column.columnKey === sortColumnKey) classes.push('listing-sort-controls--active');

    const sortControlsData: VNodeData = {
      class: classes.join(' '),
      on: {
        click() {
          emitter({
            event: 'sortRequest',
            payload: {
              columnKey: column.columnKey,
              sortDirection: sortDirection === 'asc' ? 'desc' : 'asc',
            },
          });
        },
      },
    };

    return h('div', sortControlsData, [ svg ]);
  },
});
