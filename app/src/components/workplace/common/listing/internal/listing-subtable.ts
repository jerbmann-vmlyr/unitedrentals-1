import Vue, { VNode, VNodeData } from 'vue';
import {
  ListingLayoutToken, ListingLayoutManager,
} from '../lib';

export default Vue.extend({
  name: 'listing-subtable',
  functional: true,
  inject: {
    injectedLayout: ListingLayoutToken,
  },
  props: {
    index: {
      type: Number,
      default: -1,
    },
  },
  render(h, { props: { index }, data, slots, listeners, injections }): VNode {
    const layout: ListingLayoutManager = injections.injectedLayout();

    const classes = [data.class || '', 'subtable-wrapper', 'subtable-wrapper--data'];
    if (index >= 0 && index % 2 === 0) classes.push('subtable-wrapper--even');
    if (index >= 0 && index % 2 !== 0) classes.push('subtable-wrapper--odd');

    const subtableId = index >= 0 ? `${index}` : 'header';

    const tableData: VNodeData = {
      ...data,
      class: classes.join(' '),
      key: data.key,
      attrs: { subtable: subtableId },
      on: {
        ...data.on,
        mousedown($event: MouseEvent) {
          // We don't wants to trigger drag events if the user clicked on the
          // actual contents of a cell (in case they want to copy and paste or something)
          // So, check to make sure the srcElement has class `cell`
          if ($event.srcElement && $event.srcElement.classList.contains('cell')) {
            $event.preventDefault();
            layout.startDragging($event);
          }
        },
      },
    };

    return h('div', tableData, [ slots().default ]);
  },
});
