import Vue, { VNodeData, VNode } from 'vue';
import { Prop } from 'vue/types/options';
import {
  ColumnConfig,
  DataRow,
  ListingUtils,
  CellType,
  ListingConfigToken,
  ListingConfig,
  ListingLayoutToken,
  ListingLayoutManager,
} from '../lib';
import ListingCellResizeHandle from './listing-cell-resize-handle';
import ListingColumnSortControls from './listing-column-sort-controls';

export default Vue.extend({
  name: 'listing-cell',
  functional: true,
  inject: {
    injectedConfig: ListingConfigToken,
    injectedLayout: ListingLayoutToken,
  },
  props: {
    type: {
      type: String as Prop<CellType>,
      validator: ListingUtils.isValidCellType,
      required: true,
    },
    column: {
      type: Object as Prop<ColumnConfig>,
      required: true,
    },
    colIndex: {
      type: Number,
      required: true,
    },
    items: { // table-header only
      type: Array as Prop<unknown[] | unknown[][]>,
    },
    pinned: {
      type: [String, Boolean],
      default: false,
      validator: ListingUtils.isValidPinnedState,
    },
    row: { // data only
      type: Object as Prop<DataRow>,
    },
  },
  render(h, { props, data, slots, injections, listeners }) {
    const { type, pinned, colIndex, items, row, column } = props;

    if (type === 'table-header' && !items) {
      throw new Error(`Listing cells of type "table-header" require an :items list input!`);
    }
    if (type === 'data' && !row) {
      throw new Error(`Listing cells of type "data" require a :row object input!`);
    }

    const config: ListingConfig = injections.injectedConfig();
    const layout: ListingLayoutManager = injections.injectedLayout();

    const partitionLength = (() => {
      switch (pinned) {
        case 'left': return layout.leftPinnedColumns.length;
        case 'right': return layout.rightPinnedColumns.length;
        default: return layout.centerColumns.length;
      }
    })();

    const height = (() => {
      switch (type) {
        case 'group-header': return config.groupHeaderHeight;
        case 'table-header': return config.tableHeaderHeight;
        case 'data':
        default:
          return config.dataHeight;
      }
    })();

    const classes = ['cell', `cell--${type}`, `cell--align-${column.align}`, 'truncate'];

    if (pinned) classes.push(`cell--${pinned}-pinned`);
    if (colIndex === 0) classes.push(`cell--first`);
    if (colIndex >= partitionLength - 1) classes.push(`cell--last`);
    if (column.selected) classes.push(`cell--column-selected`);
    if (column.sortable) classes.push(`cell--sortable`);

    // Call the corresponding render functions: renderTableHeaderCell, renderDataCell
    // The render functions are what generate the cell content, which can take the form
    // of undefined, string, number, VNode, array of VNodes... the `normalizeCellContents`
    // method of the listing utils will massage the data so that `cell` is ONLY of type `VNode`.
    // It also wraps primitive cell contents (strings, numbers, etc) in a <span> element.
    const cell: VNode = (() => {
      switch (type) {
        case 'table-header':
          classes.push(column.tableHeaderCellClass);
          return ListingUtils.normalizeCellContents(h, column.renderTableHeaderCell(h, { column, items: [] }));
        case 'data':
          if (column.link) classes.push(`cell--link`);
          classes.push(column.dataCellClass);
          return ListingUtils.normalizeCellContents(h, column.renderDataCell(h, { column, ...row! }));
        case 'group-header':
        default:
          throw new Error(`<listing-cell> doesn't know how to handle group headers yet`);
      }
    })();

    const elementData: VNodeData = {
      ...data,
      key: data.key,
      class: classes.join(' '),
      on: { ...(data.on || {}) },
      style: {
        ...data.style as object,
        height: `${height}px`,
      },
    };

    const children = [ cell, ...(slots().default || []) ];

    if (type === 'table-header' && column.resizable && column.selected) {
      const resizeHandle = h(ListingCellResizeHandle, {
        props: { column },
        on: {
          mousedown: ($event: MouseEvent) =>
            $event.preventDefault() as unknown
            || $event.stopPropagation()
            || typeof listeners.resizeBegin === 'function'
            && listeners.resizeBegin($event, column),
        },
      });
      children.push(resizeHandle);
    }

    if (type === 'table-header' && column.sortable) {
      const sortControls = h(ListingColumnSortControls, { props: { column } });
      children.push(sortControls);
    }

    return h(ListingUtils.cellTagFromType(type), elementData, children);
  },
});
