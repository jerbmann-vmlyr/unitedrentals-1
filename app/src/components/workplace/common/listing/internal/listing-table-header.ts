import Vue from 'vue';
import {
  ColumnConfig,
  ListingConfig,
  ListingConfigToken,
  ListingEmitter,
  ListingEmitterToken,
  ListingLayoutManager,
  ListingLayoutToken,
  ListingModuleState,
  ListingStateToken,
} from '../lib';
import ListingCell from './listing-cell';
import ListingSubtable from './listing-subtable';
import ListingTable from './listing-table';

export default Vue.extend({
  name: 'listing-table-header',
  inject: {
    injectedConfig: ListingConfigToken,
    injectedEmitter: ListingEmitterToken,
    injectedLayout: ListingLayoutToken,
    injectedState: ListingStateToken,
  },
  components: {
    ListingCell,
    ListingSubtable,
    ListingTable,
  },
  computed: {
    config(): ListingConfig {
      return (this as any).injectedConfig();
    },
    emitter(): ListingEmitter {
      return (this as any).injectedEmitter();
    },
    layout(): ListingLayoutManager {
      return (this as any).injectedLayout();
    },
    state(): ListingModuleState {
      return (this as any).injectedState();
    },
    isResizing(): boolean {
      return this.state.columns.some(c => c.isResizing);
    },
  },
  methods: {
    handleSubtableScroll($event) {
      this.layout.handleSubtableScroll($event);
    },
    async selectColumn({columnKey, resizable }: ColumnConfig, selected: boolean) {
      if (this.isResizing || !resizable) return;

      this.$store.dispatch(`${this.config.listingId}/setSelectedColumn`, {
        columnKey,
        selected,
      });
    },
    async updateColumn(column: ColumnConfig) {
      await this.$store.dispatch(`${this.config.listingId}/updateColumn`, column);
    },
    async handleResizeHandleMouseDown(event: MouseEvent, column: ColumnConfig) {

      // Gather up the elements needed for a resize operation
      const resizeHandle = event.srcElement!;
      const th = resizeHandle.parentElement!;
      const listing = this.layout.listingEl!;

      // Take their measurements
      const { left: listingLeft } = listing.getBoundingClientRect();
      const { left: thLeft, width: thWidth } = th!.getBoundingClientRect();

      const resizeProxyStartLeft = thLeft + thWidth - listingLeft;
      const minLeft = thLeft - listingLeft + column.minWidth;
      const maxLeft = thLeft - listingLeft + column.maxWidth;
      const startMouseOffset = resizeProxyStartLeft + listingLeft - event.clientX;

      await this.updateColumn({ ...column, isResizing: true });
      this.layout.resizeProxyVisible = true;
      this.layout.resizeProxyLeft = resizeProxyStartLeft;

      (document as any).onselectstart = () => false;
      document.ondragstart = () => false;

      const handleMouseMove = async (event: MouseEvent) => {
        const newLeft = event.clientX - listingLeft + startMouseOffset;
        const clampedLeft = Math.min(maxLeft, Math.max(minLeft, newLeft));
        const incrementedLeft = clampedLeft - clampedLeft % column.resizeIncrements;
        this.layout.resizeProxyLeft = incrementedLeft;
      };

      const handleMouseUp = async () => {
        if (this.isResizing) {
          const diff = this.layout.resizeProxyLeft - resizeProxyStartLeft;
          const previousWidth = column.width;
          const columnWidth = thWidth + diff;
          await this.updateColumn({ ...column, width: columnWidth, isResizing: false });
          this.emitter({
            event: 'columnResized',
            payload: {
              column,
              newWidth: columnWidth,
              previousWidth,
            },
          });

          setTimeout(() => this.layout.resizeProxyVisible = false, 200);
        }

        document.removeEventListener('mousemove', handleMouseMove);
        document.removeEventListener('mouseup', handleMouseUp);
        (document as any).onselectstart = null;
        document.ondragstart = null;
      };

      document.addEventListener('mousemove', handleMouseMove);
      document.addEventListener('mouseup', handleMouseUp);
    },
  },
});
