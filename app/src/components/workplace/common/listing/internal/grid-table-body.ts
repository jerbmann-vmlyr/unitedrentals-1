import Vue from 'vue';
import {
  GroupHeaderCellScope,
  ListingConfig,
  ListingConfigToken,
  ListingEmitter,
  ListingEmitterToken,
  ListingLayoutManager,
  ListingLayoutToken,
  ListingModuleState,
  ListingStateToken,
  ListingRowsToken,
  ListingRow,
  RowType,
} from '../lib';
import GridTable from './grid-table';
import ListingCell from './listing-cell';
import ListingSubtable from './listing-subtable';
import ListingGroupHeader from './listing-group-header';
import GridListingField from '@/components/workplace/shared/fields/grid/listing-field.vue';

export default Vue.extend({
  name: 'grid-table-body',
  inject: {
    injectedConfig: ListingConfigToken,
    injectedEmitter: ListingEmitterToken,
    injectedLayout: ListingLayoutToken,
    injectedRows: ListingRowsToken,
    injectedState: ListingStateToken,
  },
  components: {
    GridTable,
    ListingCell,
    ListingSubtable,
    GridListingField,
    ListingGroupHeader,
  },
  data() {
    return {
      collapsedIndices: [] as number[],
      linkHoverRowIndex: -1,
      mutationObserver: null as MutationObserver | null,
      rowTypes: RowType,
    };
  },

  computed: {
    config(): ListingConfig {
      return (this as any).injectedConfig();
    },
    emitter(): ListingEmitter {
      return (this as any).injectedEmitter();
    },
    rows(): ListingRow[] {
      const rows: ListingRow[] = (this as any).injectedRows();
      return rows.filter(({ type, groupIndex }) =>
        type !== 'data' || !this.collapsedIndices.includes(groupIndex),
      );
    },
    layout(): ListingLayoutManager {
      return (this as any).injectedLayout();
    },
    state(): ListingModuleState {
      return (this as any).injectedState();
    },
  },
  methods: {
    groupIsCollapsed(groupIndex: number): boolean {
      return this.collapsedIndices.includes(groupIndex);
    },
    toggleCollapsed({ groupIndex }: GroupHeaderCellScope) {
      if (this.collapsedIndices.includes(groupIndex)) {
        this.collapsedIndices = this.collapsedIndices.filter(i => i !== groupIndex);
      }
      else {
        this.collapsedIndices.push(groupIndex);
      }
    },
  },
  watch: {
    '$route.query.groupBy'() {
      // clear collapsed groups on change
      this.collapsedIndices = [];
    },
  },
});
