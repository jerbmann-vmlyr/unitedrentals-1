/* tslint:disable ban-types */
import Vue, { VNodeData } from 'vue';
import { Prop } from 'vue/types/options';
import { GroupHeaderCellScope, GroupHeaderRow, GroupConfig, ListingConfigToken, ListingConfig } from '../lib';

export default Vue.extend({
  name: 'listing-group-header-cell',
  functional: true,
  inject: {
    injectedConfig: ListingConfigToken,
  },
  props: {
    collapsed: {
      type: Boolean,
      required: true,
    },
    group: {
      type: Object as Prop<GroupConfig>,
      required: true,
    },
    row: {
      type: Object as Prop<GroupHeaderRow>,
      required: true,
    },
  },
  render(h, { props, data, injections, listeners }) {
    const classes = ['cell', 'cell--group-header', `cell--${props.collapsed ? 'collapsed' : 'expanded'}`];
    const config: ListingConfig = injections.injectedConfig();

    const scope: GroupHeaderCellScope = {
      group: props.group,
      collapsed: props.collapsed,
      ...props.row,
    };

    const boundListeners: { [key: string]: (scope: GroupHeaderCellScope) => void } = {};
    for (const l in listeners) {
      if (listeners.hasOwnProperty(l)) {
        const listener = listeners[l];
        boundListeners[l] = (Array.isArray(listener) ? listener[0] : listener).bind(null, scope);
      }
    }

    const elementData: VNodeData = {
      ...data,
      class: classes.join(' '),
      style: {
        height: `${config.groupHeaderHeight}px`,
      },
    };

    return h('th', elementData, [
      scope.group.renderGroupHeaderCell(h, scope, boundListeners),
    ]);
  },
});
