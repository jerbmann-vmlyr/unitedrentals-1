/* eslint-disable no-console */
/* eslint-disable quote-props */
import filters from '@/lib/filters';
import i18n from '@/data/i18n';

import { TransactionType } from '@/lib/transactions';
import { WorkplaceEquipmentStatusUtils } from '@/lib/workplace';


/*
 *  During checkout a user defines how they'd like to receive and return their order/equipment.
 *  * Start:
 *      * Example: Branch Pickup (Customer picks up equipment from UR Branch)
 *      * Example: UR Delivery (United Rentals delivers equipment to customers location)
 *  * Est End:
 *      * Example: UR Pickup (United Rentals picks up equipment from customers location)
 *      * Example: Branch Delivery (Customer delivers equipment back to UR Branch)
 */
export function urStartDeliveryStatus({ urDeliver }) {
  return i18n.checkout[urDeliver ? 'urDelivery' : 'userPickup'];
}

export function urEndDeliveryStatus({ pickup }) {
  return i18n.checkout[pickup ? 'urPickup' : 'userReturn'];
}

export function urTime(v) {
  const res = filters.momentSanitized(v);
  return filters.momentFriendlyRoundedDownHourFormat(res);
}

export function urDate(v) {
  const res = filters.momentSanitized(v);
  return filters.momentSlashYYFormat(res);
}

export function urDateRange(start, end) {
  return `${urDate(start)}-${urDate(end)}`;
}

export function urCurrency(v) {
  return filters.currency(v);
}
export function urStatusLabel(status, item) {
  return WorkplaceEquipmentStatusUtils.getEquipmentStatusLabel(status, item);
}

export function urPhoneNumber(v, { delimeter = '.' } = {}) {
  /**
   *  From https://unitedrentals.atlassian.net/wiki/spaces/KD/pages/191725593/Editorial+Guidelines+1.0
   *
   *  * If shown in read-only format (i.e. a message, Global Header/Footer)
   *      * Format: {{ ### }}.{{ ### }}.{{ #### }}
   *          * Example: 123.456.7891
   *          * Example: 1.234.567.8910
   *  * If shown as editable
   *      * If phone number is 10 digits
   *          * {{ ### }}-{{ ### }}-{{ #### }}
   *              * Example: 123-456-7890
   *      * If phone number isn't 10 digits
   *          * Varies. Format will be determined based on API.
   */
  const normalized = v.replace(/\D/g, ''); // (123) 456-7890 -> 1234567890
  const parts = normalized.length === 10
    ? normalized.match(/^(\d{3})(\d{3})(\d{4})$/).slice(1, 4) // 1234567890  -> 123-456-7890
    : normalized.match(/^(\d{1})(\d{3})(\d{3})(\d{4})$/).slice(1, 5); // 11234567890 -> 1-123-456-7890
  return parts.join(delimeter);
}

export const urPhoneNumberDotted = v => urPhoneNumber(v, { delimeter: '.' });
export const urPhoneNumberDashed = v => urPhoneNumber(v, { delimeter: '-' });

export function urTransactionTypeLabel(v, { key = 'transType' } = {}) {
  const transType = v[key] || v;
  switch (transType) {
    case TransactionType.Closed:
    case TransactionType.Rental: return 'Contract';
    case TransactionType.Reservation: return 'Reservation';
    case TransactionType.Quote: return 'Quote';
    default: {
      console.warn(`No TransactionType label found for: ${transType}`);
      return '';
    }
  }
}


export const urFormats = {
  urStartDeliveryStatus,
  urEndDeliveryStatus,
  urTime,
  urDate,
  urDateRange,
  urCurrency,
  urPhoneNumber,
  urPhoneNumberDotted,
  urPhoneNumberDashed,
  urStatusLabel,
};

export default urFormats;
