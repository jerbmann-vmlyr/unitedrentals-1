import { WorkplaceAction, WorkplaceCollectionType as Type } from '@/lib/workplace';

export interface ActionHandler {
  [Type.Equipment]?: (evt: void) => void;
  [Type.Orders]?: (evt: void) => void;
}
export interface ActionMiddleware {
  [Type.Equipment]?: boolean;
  [Type.Orders]?: boolean;
}

export interface ItemAction {
  label: string;
  type: WorkplaceAction;
  use?: ActionMiddleware;
  handle: ActionHandler;
  beforeHandle?: ActionHandler;
  afterHandle?: ActionHandler;
}
