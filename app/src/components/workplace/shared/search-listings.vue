<template>
  <div
    class="global-search__field equipment-filter-search no-grow"
    :class="{
      'global-search__search-box' : !isFleet,
      'global-search__search-box--expanded': hasFocus,
      'show-results': showResultsPanel,
      'show-no-results': !hasResults && showResultsPanel
    }">

    <input
      class="global-search-input"
      type="text"
      :placeholder="placeholder"
      v-model="searchText"
      @keyup.enter="selectFirstResult"
      @keyup.esc="clearSearchText"
      @focus="setFocus(true)"
      @blur="setFocus(false)">

    <i
      @click="selectFirstResult"
      class="cursor-pointer global-search__search-box--icon global-search__field--icon icon icon-before-search">
    </i>

    <span
      v-if="searchText.length > 0"
      class="global-search__clear"
      @click="clearSearchText">
      &times;
    </span>

    <div
      class="global-search__search-results global-search__search-results--labelled"
      style="z-index: 23"
      v-if="hasFocus">

      <div class="global-search__results-group">
        <div class="global-search__results-group--label">
          {{ $i18n('ur.matchingTerms') }}
          <span>{{ searchResults.length }}</span>
        </div>

        <div class="global-search__results-objects" v-if="hasResults">
          <ur-query-link
            v-for="(result, index) in searchResults"
            :handle-externally="true"
            :key="index"
            :param="searchFilter.id"
            :replace="true"
            :value="result.token"
            @blur="setFocus(false)"
            @triggered="tokenClicked($event, result)"
            class="global-search__results-object"
            mode="multivalue"
            tag="div"
            v-html="resultLabel(result)"
          />
        </div>
        <div class="global-search__no-results" v-else>
          <cite>{{ $i18n('ur.noResultsFound') }}</cite>
        </div>
      </div>
    </div>
  </div>

</template>

<script lang="ts">
  /* eslint-disable */
  import { types } from '@/store';
  import { replace } from 'lodash';
  import { mapGetters } from 'vuex';
  import { SearchHelper, Result } from '@/lib/collections';
  import UrQueryLink from '@/components/common/ur-query-link.vue';

  /*
   * @vuese
   * @group Workplace
   */
  export default {
    name: 'searchListings',
    components: { UrQueryLink },
    data() {
      return {
        /*
         * @vuese
         * Search Input is focused
         */
        hasFocus: false,
        /*
         * @vuese
         * Search Input Text
         */
        searchText: '',
      };
    },
    computed: {
      ...mapGetters({
        /*
         * @vuese
         * Current Collection Filters
         */
        filters: types.workplace.filters,
        /*
         * @vuese
         * Current Collection Detail Page
         */
        detailPage: types.workplace.detailPage,
        /*
         * @vuese
         * Current Collection Listing Page
         */
        listingPage: types.workplace.listingPage,
        /*
         * @vuese
         * Current Collection For Current Account
         */
        collection: types.workplace.collectionForCurrentAccount,
      }),
      /*
       * @vuese
       * @type Boolean
       * Search Input has results
       */
      hasResults(): boolean {
        return this.searchResults.length > 0;
      },
      /*
       * @vuese
       * Find the collections search filter
       */
      searchFilter() {
        return this.filters.find(({ id }) => id === 'search')!;
      },
      /*
       * @vuese
       * Initialize search helper based on this collections search filter
       */
      searchHelper() {
        if (!this.collection) return;

        return new SearchHelper(
          this.searchFilter.tokenizer,
          this.uniqueResourceKey,
          this.collection,
        );
      },
      /*
       * @vuese
       * @type Result[]
       * Search Results For Input Search Query
       */
      searchResults(): Result[] {
        const searchText = this.searchText.toLowerCase();

        return this.searchHelper.search(searchText);
      },
      /*
       * @vuese
       * @type Boolean
       * Determine if we should display the search dropdown panel
       */
      showResultsPanel(): boolean {
        return this.hasFocus && this.searchText.length >= 2;
      },
    },

    methods: {
      /*
       * @vuese
       * Reset search query to empty
       */
      clearSearchText() {
        this.searchText = '';
      },
      /*
       * @vuese
       * Formats the item name text so that the search term is
       * highlighted where the search text is found in the item name.
       */
      resultLabel({ isBroad, ...result }: Result) {
        const isActive = this.tokenIsActive(result.token);
        const count = result.keys.length;

        if (!isBroad) {
          const activeSearchText = replace(result.token, this.searchText, `<span class="fw-bold">${this.searchText}</span>`);
          return `
          <span class="fw-light">${activeSearchText} (${count})</span>
          ${isActive ? '<i class="icon icon-before-check"/>' : ''}
        `;
        } else {
          return `
          <span class="fw-light">Search text: <span class="fw-bold">${result.query}</span> (${count})</span>
          ${isActive ? '<i class="icon icon-before-check"/>' : ''}
        `;
        }
      },
      /*
       * @vuese
       * Select the first result from current queries search results
       */
      selectFirstResult($event) {
        if (!this.hasResults || !this.showResultsPanel) return;

        // Add the token from the first search result to the searchFilter.id
        // property of the route's query object
        const [result]: Result[] = this.searchResults;
        const currentTokens = this.$route.query[this.searchFilter.id] || [];

        // If the token is already active, do nothing
        if (currentTokens.includes(result.token)) return;

        // Generate a new query object with our new token applied
        // so that we can emulate a tokenClicked event
        const toggledQuery = {
          ...this.$route.query,
          [this.searchFilter.id]: [...currentTokens, result.token],
        };

        // Fire off the token click handler
        this.tokenClicked({ toggledQuery }, result);
        this.setFocus(false);
      },
      /*
       * @vuese
       * Focus on the search input
       */
      setFocus(value) {
        setTimeout(() => this.hasFocus = value, value ? 0 : 250);
      },
      /*
       * @vuese
       * Determine if a given search token is within the search query results
       */
      tokenIsActive(token: string) {
        const queryTokens = this.$route.query[this.searchFilter.id] || [];
        return queryTokens.includes(token);
      },
      /*
       * @vuese
       * When search results/token is clicked, implement the search result logic
       */
      tokenClicked({ toggledQuery }, { keys }: Result) {
        this.$router[this.goToListingOnMultiMatch ? 'push' : 'replace']({
          name: this.goToListingOnMultiMatch
            ? this.listingPage
            : this.$route.name,
          query: toggledQuery,
        });
        this.clearSearchText();
      },
    },
    props: {
      /*
       * @vuese
       * Determine if the results are the results for an accounts fleet
       */
      isFleet: Boolean,
      /*
       * @vuese
       * Go to a listing with the given token defined query parameters
       */
      goToListingOnMultiMatch: Boolean,
      /*
       * @vuese
       * Unique resource key so we can re-use the search bar for all workplace resources
       */
      uniqueResourceKey: {
        required: true,
        type: Function,
      },
      /*
       * @vuese
       * Search Input Placeholder when search results are empty
       */
      placeholder: {
        type: String,
        required: false,
        default: 'Search',
      },
    },
  };
</script>

<style lang="scss" scoped>
  .global-search__search-box--icon {
    z-index: 1 !important;
  }
</style>
