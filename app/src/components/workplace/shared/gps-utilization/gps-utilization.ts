import Vue from 'vue';
import moment from 'moment';
import { WorkplaceEquipmentUtils, WorkplaceEquipmentGpsUtilization as Utilization } from '@/lib/workplace';

export default Vue.extend({
  name: 'GpsUtilization',
  props: {
    item: Object,
    longFormat: {
      type: Boolean,
      default: false,
    },
  },
  computed: {
    contextualText(): string | void {
      // Determine which contextual text to display based on status.
      const now = moment(new Date());

      // When displaying any time value, we were told to take the floor value
      // rather than the ceiling value
      switch (this.item.gpsUtilization) {
        case Utilization.NoSignal:
        case Utilization.Offline:
          const reportedDuration = now.diff(this.item.reportedDateTime, 'days', false);
          return this.$i18n('workplace.gpsUtilization.forXDays',
            { '@count': reportedDuration });
        case Utilization.Idle:
          const lastMeterDuration = now.diff(this.item.lastMeterChange, 'days', false);
          return this.$i18n('workplace.gpsUtilization.forXDays',
            { '@count': lastMeterDuration });
        case Utilization.Online:
          return this.$i18n('workplace.gpsUtilization.noUsageToday');
        case Utilization.Active:
          return this.$i18n('workplace.gpsUtilization.today');
        case Utilization.Overtime:
          const overtimeUsage = moment.duration(this.item.usageToday - 8, 'hours');
          return this.$i18n('workplace.gpsUtilization.forXHours',
            { '@count': overtimeUsage.asHours().toFixed(0) });
        default:
          return;
      }
    },
    contextualTextDisplay(): boolean {
      return !this.longFormat
        // If we're displaying in normal format (equipment-list) we will never show contextual text.
        ? false
        // N/A or Online statuses do not display contextual text.
        : ![ Utilization.NA, Utilization.Online ].includes(this.item.gpsUtilization);
    },
    displayInLongFormat(): boolean {
      // N/A status doesn't display in longFormat, but it does in table.
      return !(this.item.gpsUtilization === Utilization.NA && this.longFormat);
    },
    getGpsUtilizationIcon(): string {
      // Map utilization enum to quark gps icon name.
      switch (this.item.gpsUtilization) {
        case Utilization.Offline:
        case Utilization.NoSignal:
          return 'gps-off';
        case Utilization.Idle:
        case Utilization.Online:
          return 'gps-not-fixed';
        case Utilization.Active:
        case Utilization.Overtime:
          return 'gps-fixed';
        default:
          return 'gps-off';
      }
    },
    gpsUtilizationLabel(): string {
      return WorkplaceEquipmentUtils.getGpsUtilizationLabel(this.item.gpsUtilization);
    },
    utilizationIsNA(): boolean {
      return this.item.gpsUtilization === Utilization.NA;
    },
  },
});
