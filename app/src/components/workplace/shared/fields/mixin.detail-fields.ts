import Vue from 'vue';
import { DetailField } from './field/boot';
import { DateDetailField } from './date/boot';
import { LinkDetailField } from './link/boot';
import { PhoneDetailField } from './phone/boot';
import { NumberDetailField } from './number/boot';
import { StatusDetailField } from './status/boot';
import { ActionsDetailField } from './actions/boot';
import { CallbackDetailField } from './callback/boot';
import { CurrencyDetailField } from './currency/boot';

export default Vue.extend({
  components: {
    DetailField,
    DateDetailField,
    LinkDetailField,
    PhoneDetailField,
    NumberDetailField,
    StatusDetailField,
    ActionsDetailField,
    CallbackDetailField,
    CurrencyDetailField,
  },
});
