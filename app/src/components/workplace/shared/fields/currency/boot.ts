import CurrencyListingField from './listing-field.vue';
import CurrencyDetailField from './detail-field.vue';

export { CurrencyListingField, CurrencyDetailField };
export default { CurrencyListingField, CurrencyDetailField };
