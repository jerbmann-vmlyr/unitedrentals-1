export default class Column {
  static make(key, props = {}) {
    const defaultColumnProps = {
      label: null, // default to calculate from column
      align: 'left', // default 'left'
      columnKey: key, // required
      dataCellClass: '', // default ''
      link: false, // default false
      maxWidth: 400, // default 400
      minWidth: 40, // default 40
      pinned: false, // default false
      property: '', // type String Function
      resizable: true, // default true
      resizeIncrements: 10, // default 10
      tableHeaderCellClass: '', // default ''
    };
    return { ...defaultColumnProps, ...props };
  }
}
