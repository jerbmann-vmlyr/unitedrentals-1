import StatusListingField from './listing-field.vue';
import StatusDetailField from './detail-field.vue';

export { StatusListingField, StatusDetailField };
export default { StatusListingField, StatusDetailField };
