import DateListingField from './listing-field.vue';
import DateDetailField from './detail-field.vue';

export { DateListingField, DateDetailField };
export default { DateListingField, DateDetailField };
