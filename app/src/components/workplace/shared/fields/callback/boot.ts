import CallbackDetailField from './detail-field.vue';
import CallbackListingField from './listing-field.vue';

export { CallbackListingField, CallbackDetailField };
export default { CallbackListingField, CallbackDetailField };
