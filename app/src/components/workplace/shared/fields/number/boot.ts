import NumberListingField from './listing-field.vue';
import NumberDetailField from './detail-field.vue';

export { NumberListingField, NumberDetailField };
export default { NumberListingField, NumberDetailField };
