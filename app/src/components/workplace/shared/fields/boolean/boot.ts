import BooleanListingField from './listing-field.vue';
import BooleanDetailField from './detail-field.vue';

export {
  BooleanListingField,
  BooleanDetailField,
};

export default {
  BooleanListingField,
  BooleanDetailField,
};
