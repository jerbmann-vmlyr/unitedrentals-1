import ActionsListingField from './listing-field.vue';
import ActionsDetailField from './detail-field.vue';

export { ActionsListingField, ActionsDetailField };
export default { ActionsListingField, ActionsDetailField };
