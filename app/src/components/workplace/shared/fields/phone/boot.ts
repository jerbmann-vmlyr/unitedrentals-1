import PhoneDetailField from './detail-field.vue';
import PhoneListingField from './listing-field.vue';

export {
  PhoneDetailField,
  PhoneListingField,
};

export default {
  PhoneDetailField,
  PhoneListingField,
};
