import LinkListingField from './listing-field.vue';
import LinkDetailField from './detail-field.vue';

export { LinkListingField, LinkDetailField };
export default { LinkListingField, LinkDetailField };
