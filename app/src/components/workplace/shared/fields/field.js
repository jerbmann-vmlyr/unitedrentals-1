import {
  setField,
  ListingField,
  DateListingField as Date,
  LinkListingField as Link,
  GridListingField as Grid,
  PhoneListingField as Phone,
  NumberListingField as Number,
  StatusListingField as Status,
  BooleanListingField as Boolean,
  ActionsListingField as Actions,
  CallbackListingField as Callback,
  CurrencyListingField as Currency,
} from './fields.ts';

export default class Field {
  static make(property, props = {}) {
    props = { ...props, component: ListingField };

    return setField(property, props);
  }

  static date(property, props = {}) {
    props = { ...props, component: Date };

    return setField(property, props);
  }

  static currency(property, props = {}) {
    props = { ...props, component: Currency };

    return setField(property, props);
  }

  static status(property, props = {}) {
    props = { ...props, component: Status };

    return setField(property, props);
  }

  static link(property, props = {}) {
    props = { ...props, component: Link };

    return setField(property, props);
  }

  static callback(property, props = {}) {
    props = { ...props, component: Callback };

    return setField(property, props);
  }

  static number(property, props = {}) {
    props = { ...props, component: Number };

    return setField(property, props);
  }

  static actions(property, props = {}) {
    props = { ...props, component: Actions };

    return setField(property, props);
  }

  static grid(property, props = {}) {
    props = { ...props, component: Grid };

    return setField(property, props);
  }

  static phone(property, props = {}) {
    props = { ...props, component: Phone };

    return setField(property, props);
  }

  static boolean(property, props = {}) {
    props = { ...props, component: Boolean };

    return setField(property, props);
  }
}
