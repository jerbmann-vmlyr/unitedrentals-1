/* Register Fields For Listing and Details Pages */
import Field from './field.js';
import Column from './column.js';

export { Field, Column };
export default { Field, Column };

