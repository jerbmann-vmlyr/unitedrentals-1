import FieldValue from './slots/field-value.slot.vue';

export default {
  components: {
    FieldValue,
  },
};
