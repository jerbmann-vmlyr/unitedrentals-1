import moment from 'moment';

/* Defined Filters Here */

/* 1. Transform Date String Into UTC Date String */
const utcDate = date => date ? moment(date).toDate().toUTCString() : {};

export default {
  /* Register Filters Defined Above To Component Methods */
  methods: {
    utcDate: date => utcDate(date),
  },
  /* Register Filters defined Above To Component Template Filters */
  filters: {
    utcDate: date => utcDate(date),
  },
};
