import ListingField from './listing-field.vue';
import DetailField from './detail-field.vue';

export {
  ListingField,
  DetailField,
};
export default {
  ListingField,
  DetailField,
};
