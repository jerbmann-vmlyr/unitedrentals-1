/* Export All Fields */
export * from './link/boot';
export * from './date/boot';
export * from './grid/boot';
export * from './field/boot';
export * from './phone/boot';
export * from './status/boot';
export * from './number/boot';
export * from './currency/boot';
export * from './actions/boot';
export * from './callback/boot';
export * from './boolean/boot';
/* Set Default Field Object Properties */
import ListingField from './field/boot';

export function setField(property, props = {}, component = ListingField) {
  const defaultFieldProps = {
    component,
    value: false, // can also pass a value: ({ item, field, column }) => {} to overwrite value
    // append classes to the component
    classes: [],
    append: false,
    filter: false,
    prepend: false,
    resourceProperty: property,
    fallbackResourceProperty: false,
  };
  // the array of this object type is iterated through in the vue template section
  // The component property of the returned value below is used to dynamically
  // use components in that given vue template
  // Most likely, it is being dynamically rendered using component :is='$component'
  return { ...defaultFieldProps, ...props };
}
