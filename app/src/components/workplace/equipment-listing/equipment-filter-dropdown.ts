import Vue from 'vue';
import { mapGetters } from 'vuex';
import { get, set, cloneDeep, sortBy } from 'lodash';
import { FilterUtils, FilterType, FilterPrimitive, DynamicFilter } from '@/lib/collections';
import { WORKPLACE_EQUIPMENT_FILTERS } from '@/lib/workplace';
import types from '@/store/types';
import UrMenu from '@/components/common/ur-menu';
import UrQueryLink from '@/components/common/ur-query-link.vue';

export default Vue.extend({
  name: 'equipmentFilterDropdown',
  components: {
    UrMenu,
    UrQueryLink,
  },
  computed: {
    ...mapGetters({
      equipment: types.workplace.equipmentForCurrentAccount,
    }),
    ...mapGetters([
      'small',
      'medium',
      'large',
    ]),
    simpleFiltersWithCounts() {
      return FilterUtils.selectSimple(WORKPLACE_EQUIPMENT_FILTERS)
        .map(f => ({ ...f, count: this.equipment.filter(f.filter).length }));
    },
    mergedFilters() {
      return WORKPLACE_EQUIPMENT_FILTERS
        .filter(f => f.type === FilterType.Dynamic || f.type === FilterType.Static)
        .map(f =>
          f.type !== FilterType.Dynamic
          ? f
          : {
              ...f,
              values: sortBy(
                f.valueFactory(this.equipment).filter(value => {
                  if (this.searchText.length < 1) {
                    return true;
                  }
                  else {
                    const label = Array.isArray(value.label)
                      ? value.label.join().toLowerCase()
                      : value.label.toLowerCase();

                    const searchText = this.searchText.toLowerCase();
                    return label.includes(searchText);
                  }
                }),
                'label',
              ),
            },
        );
    },
  },
  data() {
    return {
      searchText: '',
    };
  },
  methods: {
    getCount(filterFn) {
      return this.equipment.filter(filterFn).length;
    },
    nestedMenuOpened(menu) {
      const input = menu.$el.querySelector('input');
      if (input && input.tagName === 'INPUT') {
        setTimeout(() => input.focus());
      }
    },
    clearSearchText() {
      this.searchText = '';
    },
    getOffset(length) {
      if (length === 1) {
        return '';
      }
      else if (length > 12) {
        return `overflow-offset-12`;
      }
      else {
        return `overflow-offset-${length}`;
      }
    },
    onFilterSearchEnter({ id, values: [filter] }: DynamicFilter<any, any> & { values: FilterPrimitive<any>[] }) {
      // Copy the current query parameter object
      const currentQuery = cloneDeep(this.$route.query || {});

      // Get the values of the existing query parameter array with this ID (if any)
      const currentQueryValues = get(this.$route.query, [id], []);

      // Determine if this filter is already active
      const isActive = currentQueryValues.includes(filter.queryValue);

      const newValue = isActive
        ? [...currentQueryValues.filter(value => value !== filter.queryValue)]
        : [...currentQueryValues, filter.queryValue];

      // Update the URL
      this.$router.replace({
        ...this.$route,
        query: set({ ...currentQuery }, id, newValue),
      });
    },
  },
});
