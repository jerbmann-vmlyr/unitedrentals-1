import Vue from 'vue';
import { mapGetters } from 'vuex';
import { WorkplaceRouteNames } from '../../../lib/workplace';
import EquipmentFilterDropdown from './equipment-filter-dropdown.vue';
import EquipmentSearch from '../shared/equipment-search.vue';
import GroupByDropdown from '../shared/group-by-dropdown.vue';

export default Vue.extend({
  components: {
    EquipmentFilterDropdown,
    EquipmentSearch,
    GroupByDropdown,
  },
  computed: {
    ...mapGetters([
      'small',
      'medium',
      'large',
    ]),
  },
  methods: {
    openColumnCustomization() {
      this.$router.push({
        name: WorkplaceRouteNames.CustomizeEquipmentVisibility,
        query: { ...this.$route.query },
      });
    },
  },
});
