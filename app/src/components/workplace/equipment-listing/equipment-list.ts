import Vue from 'vue';
import { mapActions, mapState, mapGetters } from 'vuex';
import * as moment from 'moment';
import {
  WORKPLACE_EQUIPMENT_GROUPERS,
  WorkplaceEquipmentColumnKey,
  WorkplaceEquipmentGroup,
  WorkplaceEquipment,
  WorkplaceRouteNames,
  WorkplaceEquipmentActionTypes,
  WorkplaceUtils,
} from '@/lib/workplace';
import { types } from '@/store';
import { WorkplaceRootState } from '@/store/module.workplace';
import { UserState } from '@/store/module.user';
import IconButton from '@/components/common/ur-icon-button.vue';
import UrCheckbox from '@/components/common/ur-checkbox.vue';
import UrLoadingSpinner from '@/components/common/ur-loading-spinner.vue';
import UrSectionCollapseIcon from '@/components/common/ur-section-collapse-icon';
import {
  ColumnResizedPayload,
  GroupHeaderCellScope,
  Listing,
  ListingColumn,
  ListingEvent,
  ListingGroupRow,
  ScrollRangeChangedPayload,
  SortRequestPayload,
  TableHeaderCellScope,
} from '@/components/workplace/common/listing';
import ActionsListingField from '@/components/workplace/shared/fields/actions/listing-field.vue';
import EquipmentFilterBadges from '../shared/equipment-filter-badges.vue';
import { AugmentedEquipment, EquipmentListComponentUtils } from './equipment-list.utils';
import DashboardCarousel from '../dashboard/dashboard-carousel/dashboard-carousel.vue';
import EquipmentListControls from './equipment-list-controls.vue';
import GpsUtilization from '../shared/gps-utilization/gps-utilization';
import SummaryResultsDivider from '../shared/dividers/summary-results-divider.vue';
import ActionsMenu from '@/components/workplace/shared/actions/actions-menu.vue';

type WorkplaceEquipmentListingState = WorkplaceRootState & {
  userPreferences: UserState,
};

export default Vue.extend({
  name: 'EquipmentList',
  components: {
    Listing,
    ListingColumn,
    ListingGroupRow,
    DashboardCarousel,
    SummaryResultsDivider,
    EquipmentFilterBadges,
    ActionsListingField,
    EquipmentListControls,
    GpsUtilization,
    IconButton,
    UrCheckbox,
    UrLoadingSpinner,
    UrSectionCollapseIcon,
    ActionsMenu,
  },
  created() {
    this.hidePageFooter();
  },
  destroyed() {
    this.showPageFooter();
  },
  mounted() {
    this.fetchEquipmentColumns();
    window.scrollTo(0, 0);
  },
  data() {
    return {
      sortBy: this.$route.query.sortBy || '',
      disabledMenuOptions: [
        WorkplaceEquipmentActionTypes.PrintDetails,
      ],
      columnKeys: WorkplaceEquipmentColumnKey,
      scrollRange: {},
    };
  },
  computed: {
    ...mapGetters({
      sortedEquipment: types.workplace.equipmentListing.sortedEquipment,
      sortDirection: types.workplace.equipmentListing.sortDirection,
      equipmentRequestsPending: types.workplace.equipmentOnRentRequestsPending,
    }),
    ...mapState<WorkplaceEquipmentListingState>({
      equipmentColumns: state => state.user.equipmentColumns,
    }),
    /**
     * We need to augment each equipment object with properties that we'll use
     * in this component's template. This'll save us from having to do a bunch
     * of expensive in-template method calls and hurting performance.
     */
    items(): AugmentedEquipment[][] {
      const items = this.sortedEquipment.map(group => {
        return group.items.map(EquipmentListComponentUtils.augmentEquipmentObject);
      });
      return items.length > 1 ? items : items[0];
    },
    /**
     * Build a set of cached maps in advance so that the listing getter methods
     * don't have to search for a matching column on each invocation.
     */
    columnIndexMap() { return WorkplaceUtils.createColumnPropertyMap('index', this.equipmentColumns); },
    columnSortableMap() { return WorkplaceUtils.createColumnPropertyMap('sortable', this.equipmentColumns); },
    columnTableHeaderLabelMap() { return WorkplaceUtils.createColumnPropertyMap('label', this.equipmentColumns); },
    columnWidthMap() { return WorkplaceUtils.createColumnPropertyMap('width', this.equipmentColumns); },
    equipmentResultsCount() {
      return !!this.sortedEquipment[0]
        ? this.sortedEquipment[0].length.total
        : 0;
    },
    groupNoLabel() {
      const queryGroupBy = this.$route.query.groupBy;
      const grouper = WORKPLACE_EQUIPMENT_GROUPERS.filter(sorter => sorter.id === queryGroupBy)[0];
      return `No ${grouper.label}`;
    },
    hiddenColumns(): string {
      return this.equipmentColumns.filter(col => !col.visible).map(col => col.columnKey);
    },
    showLoadingOverlay() {
      return this.equipmentRequestsPending && this.equipmentResultsCount < 1;
    },
    sortedColumnKey(): string {
      const matchingColumn = this.equipmentColumns.find(col => col.sortable === this.sortBy);
      return matchingColumn ? matchingColumn.columnKey : '';
    },
  },
  methods: {
    ...mapActions({
      fetchEquipmentColumns: types.user.fetchEquipmentColumns,
      updateEquipmentColumn: types.user.updateEquipmentColumn,
    }),
    leniencyStartDate(item) {
      if (item.leniencyStartDate) {
        return moment(item.leniencyStartDate).toDate().toUTCString();
      }
      return {};
    },
    leniencyEndDate(item) {
      if (item.leniencyEndDate) {
        return moment(item.leniencyEndDate).toDate().toUTCString();
      }
      return {};
    },
    startDateTime(item) {
      if (item.startDateTime) {
        return moment(item.startDateTime).toDate().toUTCString();
      }
      return {};
    },
    returnDateTime(item) {
      if (item.returnDateTime) {
        return moment(item.returnDateTime).toDate().toUTCString();
      }
      return {};
    },
    billThruDateTime(item) {
      if (item.lastBilledDate) {
        return moment(item.lastBilledDate).toDate().toUTCString();
      }
      return null;
    },
    columnIsSortable(columnKey: string): string | boolean {
      return this.columnSortableMap[columnKey];
    },
    getColumnIndex(columnKey: string): number {
      return this.columnIndexMap[columnKey] || -1;
    },
    getColumnWidth(columnKey: string): number {
      return this.columnWidthMap[columnKey] || 1;
    },
    getTableHeaderLabel({ column }: TableHeaderCellScope): string {
      const matchingLabel = this.columnTableHeaderLabelMap[column.columnKey];
      return matchingLabel ? matchingLabel.short : '';
    },
    getGroupLabel(scope: GroupHeaderCellScope): string {
      const group: WorkplaceEquipmentGroup = this.sortedEquipment[scope.groupIndex];
      if (!group) throw new Error(`There is not group with the index "${scope.groupIndex}!"`);
      return Array.isArray(group.label) ? group.label.join(' ') : group.label;
    },
    handleListingEvent({ event, payload }: ListingEvent) {
      switch (event) {
        case 'sortRequest': return this.handleSortRequest(payload);
        case 'scrollRangeChanged': return this.handleScrollRangeChanged(payload);
        case 'columnResized': return this.handleColumnResized(payload);
      }
    },
    async handleColumnResized({ column: { columnKey }, newWidth, previousWidth }: ColumnResizedPayload) {
      if (newWidth !== previousWidth) {
        await this.updateEquipmentColumn({
          columnKey,
          changes: { width: newWidth },
        });
      }
    },
    handleScrollRangeChanged(payload: ScrollRangeChangedPayload) {
      this.scrollRange = payload;
    },
    handleSortRequest({ columnKey, sortDirection }: SortRequestPayload) {
      // Get the matching sortable ID
      const column = this.equipmentColumns.find(col => col.columnKey === columnKey);
      if (!column || !column.sortable) throw new Error(`The column key ${columnKey} is not sortable!`);

      // Set the route
      this.$router.replace({
        ...this.$route,
        query: {
          ...this.$route.query,
          sortBy: column.sortable,
          sortDirection,
        },
      });
    },
    detailsRoute(item: WorkplaceEquipment) {
      return {
        name: WorkplaceRouteNames.EquipmentDetails,
        params: {
          ...this.$route.params,
          equipmentId: item.equipmentId,
          transId: item.transId,
          transLineId: item.transLineId,
        },
        query: { ...this.$route.query },
      };
    },
    setElDisplay(query: string, display = 'block')  {
      const el = document.querySelector(query);
      if (el instanceof HTMLElement) {
        el.style.display = display;
      }
    },
    hidePageFooter() {
      this.setElDisplay('footer.footer', 'none');
      this.setElDisplay('#egot_iframe', 'none');
    },
    showPageFooter() {
      this.setElDisplay('footer.footer', 'block');
      this.setElDisplay('#egot_iframe', 'block');
    },
  },
  watch: {
    ['$route.query.sortBy']: {
      immediate: true,
      handler(sortBy: string) {
        this.sortBy = sortBy || '';
      },
    },
  },
});
