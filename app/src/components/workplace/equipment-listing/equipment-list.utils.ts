/**
 * Keep component-specific util functions for the WELP here
 */
import * as moment from 'moment';
import { get } from 'lodash';
import { dateFormat } from '@/utils';
import { StatusUtils } from '@/lib/statuses';
import { TransactionType } from '@/lib/transactions';
import {
  WorkplaceEquipment,
  WorkplaceEquipmentUtils,
  WorkplaceEquipmentStatusColor,
  WorkplaceEquipmentStatusUtils,
  WorkplaceEquipmentStatusId as WorkEqpStatusId,
} from '@/lib/workplace';
import { BranchUtils } from '@/lib/branches';

export interface AugmentedEquipment extends WorkplaceEquipment {
  branchDetailUrl: string | null;
  equipmentIdOrEmptyString: string;
  hasMatchingBranch: boolean;
  hasGpsEnabled: boolean;
  isContract: boolean;
  isInLeniency: boolean;
  jobsitePhone: string | null;
  leniencyStatus: string | null;
  orderType: string;
  pickupDate: string | null;
  pickupQuantity: number;
  statusIndicatorClass: string;
  statusLabel: string;
  statusSpecificQuantity: number;
}

export class EquipmentListComponentUtils {
  static augmentEquipmentObject(item: WorkplaceEquipment): AugmentedEquipment {
    const statuses = item.equipmentStatuses || [];
    const hasStatuses = statuses.length > 0;
    const isPickup = StatusUtils.statusListIncludesAnyOf([ WorkEqpStatusId.AdvancedPickup, WorkEqpStatusId.PickupRequested ], statuses);
    const jobsitePhone = WorkplaceEquipmentUtils.formatJobsitePhone(item);
    const leniencyStatus = WorkplaceEquipmentUtils.findLeniencyStatus(item);
    const maybePickupDate = moment(isPickup ? item.pickupDateTime || '' : item.nextPickupDateTime || '');
    const pickupDate = maybePickupDate.isValid() ? maybePickupDate.format(dateFormat.slash) : '';
    const hasMatchingBranch = item.branch && typeof item.branch.branchId === 'string';
    const branchDetailUrl = hasMatchingBranch && typeof get(item, ['branch', 'primaryBusinessType', 'url']) === 'string'
      ? `/locations/${item.branch.state!}/${item.branch.city!}/${BranchUtils.getBusinessTypeUrlParam(item.branch)}/${item.branchId}`
      : null;

    return {
      ...item,
      branchDetailUrl,
      equipmentIdOrEmptyString: item.transType === TransactionType.Rental ? item.equipmentId : '',
      hasGpsEnabled: item.simActive === true,
      hasMatchingBranch,
      isContract: WorkplaceEquipmentUtils.isContract(item),
      isInLeniency: StatusUtils.statusListIncludesAnyOf(WorkEqpStatusId.InLeniency, statuses),
      jobsitePhone,
      leniencyStatus,
      orderType: WorkplaceEquipmentStatusUtils.getOrderTypeLabel(item),
      pickupDate,
      pickupQuantity: isPickup ? item.quantityPickedUp : item.quantityNextPickup,
      statusIndicatorClass: EquipmentListComponentUtils.statusIndicatorClass(hasStatuses ? statuses[0].color : ''),
      statusLabel: hasStatuses ? WorkplaceEquipmentStatusUtils.getStatusIdLabel(statuses[0].id) : '',
      statusSpecificQuantity: WorkplaceEquipmentUtils.getStatusSpecificQuantity(item),
    };
  }

  private static statusIndicatorClass(color: WorkplaceEquipmentStatusColor | string): string {
    switch (color as WorkplaceEquipmentStatusColor) {
      case WorkplaceEquipmentStatusColor.BlueHighlight: return 'status-indicator-blue-highlight';
      case WorkplaceEquipmentStatusColor.BrightRed: return 'status-indicator-bright-red';
      case WorkplaceEquipmentStatusColor.Grey: return 'status-indicator-gray-light';
      case WorkplaceEquipmentStatusColor.Green: return 'status-indicator-green';
      case WorkplaceEquipmentStatusColor.OrangeHighlight: return 'status-indicator-orange-highlight';
      default: return '';
    }
  }
}
