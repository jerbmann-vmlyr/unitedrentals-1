import { urTransactionTypeLabel, urFormats } from '../common/details/editorial';
import { StatusUtils } from '@/lib/statuses';
import { CONTRACT_STATUSES, QUOTE_STATUSES, RESERVATION_STATUSES } from '@/lib/transactions';


export function itemTransComputeds({ itemKey = 'item', keyPrefix = 'item' } = {}) {
/**
 * Returns a handful of computed properties to help convert transactions:
 *
 * {
 *  itemTransTypeLabel: "Contract"
 *  itemTransStatusLabel: "Due Soon on 08/20/2019"
 *  itemTransStatus: {
 *      color: "#DDE0E2"
 *      id: "DueSoon"
 *      label: "DueSoon"
 *      priority: 1
 *  }
 * }
 */
  const itemTransTypeLabel = `${keyPrefix}TransTypeLabel`;
  const itemTransStatus = `${keyPrefix}TransStatus`;
  const itemTransStatusLabel = `${itemTransStatus}Label`;

  return {
    [itemTransTypeLabel]: vm => !vm.loading && urTransactionTypeLabel(vm[itemKey]), // transType
    [itemTransStatus]: (vm) => {
      if (!vm.loading) {
        const status = getOrderStatus(vm[itemKey]);
        // Add label alias
        return {
          label: status.id,
          ...status,
        };
      }
    },
    [itemTransStatusLabel]: vm => !vm.loading && urFormats.urStatusLabel(vm[itemTransStatus], vm[itemKey]),
  };
}

export function urOrderApplicableStatuses(v) {
  return {
    contractStatuses: StatusUtils.getApplicableStatuses(CONTRACT_STATUSES, v),
    quoteStatuses: StatusUtils.getApplicableStatuses(QUOTE_STATUSES, v),
    reservationStatuses: StatusUtils.getApplicableStatuses(RESERVATION_STATUSES, v),
  };
}

export function getOrderStatus(item) {
  const label = urTransactionTypeLabel(item, { key: 'transType' });
  const statuses = urOrderApplicableStatuses(item);
  const status = statuses[`${label.toLowerCase()}Statuses`];
  return (status && status[0]) || {};
}
