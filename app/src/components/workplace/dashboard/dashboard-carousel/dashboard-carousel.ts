import Vue from 'vue';
import { get } from 'lodash';
import { mapGetters, mapState } from 'vuex';

import { WorkplacePredefinedRoutes, WorkplaceRouteNames, WorkplaceEquipmentStatusId } from '@/lib/workplace';
import { WorkplaceRootState } from '@/store/module.workplace';
import types from '@/store/types';
import UrGtm from '@/mixins/ur-gtm';
import DashboardCard from '@/components/workplace/dashboard/dashboard-carousel/dashboard-carousel-card.vue';

export default Vue.extend({
  components: {
    DashboardCard,
  },
  computed: {
    ...mapGetters({
      accountIdIsValid: types.workplace.accountIdIsValid,
      filteredReservations: 'filteredReservations',
      filteredQuotes: 'filteredQuotes',
      accountId: types.workplace.accountId,
      equipmentForCurrentAccount: types.workplace.equipmentForCurrentAccount,
      equipmentOnRentRequestsPending: types.workplace.equipmentOnRentRequestsPending,
      filteredEquipment: types.workplace.equipmentListing.filteredEquipment,
      rentalCountsInFlight: types.workplace.rentalCountsInFlight,
      rentalCounts: types.workplace.rentalCountsForCurrentAccount,
    }),
    ...mapState<WorkplaceRootState>({
      allAccounts: state => state.accounts.all,
    }),
    outstandingDue() {
      return +get(this.allAccounts[this.accountId], 'openRentalAmount', 0);
    },
    center() {
      return this.carouselRef.slideCount < this.perPage
        ? 'center'
        : '';
    },
    navigationNextLabel() {
      return '<i class="icon icon-before-arrow-right"></i>';
    },
    navigationPrevLabel() {
      return '<i class="icon icon-before-arrow-left"></i>';
    },
    perPage() {
      return this.small ? 2 : 4;
    },
    spacePadding() {
      if (this.windowInnerWidth >= 1440) return 80 - 8;
      if (this.windowInnerWidth >= 1024) return 60 - 4;
      if (this.windowInnerWidth >= 640) return 28;
      return 12;
    },
    cards() {
      return {
        onRent: {
          route: {
            ...this.$route,
            ...WorkplacePredefinedRoutes.onRent,
          },
          active: false,
        },
        scheduledForPickup: {
          route: {
            ...this.$route,
            ...WorkplacePredefinedRoutes.scheduledForPickup,
          },
          active: false,
        },
        allItems: {
          route: {
            ...this.$route,
          },
          name: WorkplaceRouteNames.EquipmentList,
          active: false,
        },
        itemsOverdue: {
          route: {
            ...this.$route,
            ...WorkplacePredefinedRoutes.overdue,
            },
          active: false,
        },
        quotes: {
          route: {
            ...this.$route,
            ...WorkplacePredefinedRoutes.quotes,
          },
          active: false,
        },
        reservations: {
          route: {
            ...this.$route,
            ...WorkplacePredefinedRoutes.reservations,
          },
          active: false,
        },
      };
    },
  },
  data() {
    return {
      carouselRef: '',
      overdueStatus: '',
      navigationEnabled: true,
      windowInnerWidth: window.innerWidth,
    };
  },
  methods: {
    handleResize() {
      this.windowInnerWidth = window.innerWidth;
    },
    setActive() {
      const equipmentStatuses = get(this.$route.query, ['equipmentStatus'], []);
      const orderTypes = get(this.$route.query, ['orderType'], []);
      if (equipmentStatuses.includes(WorkplaceEquipmentStatusId.OnRent)) {
        this.cards.onRent.active = true;
      }
      if (orderTypes.includes('reservation')) {
        this.cards.reservations.active = true;
      }
      if (orderTypes.includes('quote')) {
        this.cards.quotes.active = true;
      }
      if (equipmentStatuses.includes(WorkplaceEquipmentStatusId.Overdue)) {
        this.cards.itemsOverdue.active = true;
      }
      if (
        equipmentStatuses.includes(WorkplaceEquipmentStatusId.AdvancedPickup)
        || equipmentStatuses.includes(WorkplaceEquipmentStatusId.PickupRequested)
      ) {
        this.cards.scheduledForPickup.active = true;
      }
    },
  },
  mixins: [
    UrGtm,
  ],
  mounted() {
    window.addEventListener('resize', this.handleResize);
    this.carouselRef = this.$refs.carousel;
    this.setActive();
  },
  beforeDestroy() {
    window.removeEventListener('resize', this.handleResize);
  },
  watch: {
    'carouselRef.pageCount': {
      handler() {
        this.navigationEnabled = this.carouselRef.pageCount > 1;
      },
      deep: true,
    },
    '$route.query': {
      handler() {
        Object.values(this.cards).forEach(ea => {
          const temp: any = ea;
          if (temp.active) {
            temp.active = false;
          }
        });
        this.setActive();
      },
      deep: true,
    },
  },
});
