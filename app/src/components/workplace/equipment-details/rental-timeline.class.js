/* eslint-disable class-methods-use-this,valid-jsdoc */

import { dateFormat } from '@/utils';

import { WorkplaceRentalTimelineDefaultOptions } from '@/lib/workplace/rental-timeline';

export default class RentalTimeline {
  constructor(vm) {
    this._vm = {};

    Object.assign(this, WorkplaceRentalTimelineDefaultOptions, {
      active: {
        end: WorkplaceRentalTimelineDefaultOptions.visibleDayCount - 1,
        start: 0,
      },
      bar: {},
      bookend: {},
      dayGuides: [],
      label: {},
      marker: {},
    }, vm);
  }

  get center() {
    return {
      x: this.width / 2,
      y: this.height / 2,
    };
  }

  get dayWidth() {
    return this._vm.dayWidth || 0;
  }

  set dayWidth(dayWidth) {
    this._vm.dayWidth = Number(dayWidth);
  }

  get end() {
    return this._vm.end || this.today;
  }

  set end(end) {
    if (this.start && end.diff(this.start, 'days') < WorkplaceRentalTimelineDefaultOptions.totalDayCount) {
      end = this.start.clone().add(WorkplaceRentalTimelineDefaultOptions.totalDayCount, 'days');
    }

    this._vm.end = end;
  }

  get filteredLeniencies() {
    return this.leniencies;
  }

  get hasEndLabel() {
    const end = this.start.clone().add(this.active.end, 'days');

    const fromEnd = end.diff(this.rentalEnd, 'days');
    const fromStart = end.diff(this.rentalStart, 'days');

    return fromStart > this.minDayDistance
      && (fromEnd > this.minDayDistance || fromEnd < 0);
  }

  get hasStartLabel() {
    const start = this.start.clone().add(this.active.start, 'days');

    const fromEnd = start.diff(this.rentalEnd, 'days');
    const fromStart = start.diff(this.rentalStart, 'days');

    return (fromStart > this.minDayDistance || fromStart > 0)
      && (fromEnd > 0 || -fromEnd > this.minDayDistance || fromEnd > this.minDayDistance);
  }

  get isStarted() {
    return this.today.diff(this.rentalStart, 'days') >= 0;
  }

  get isOverdue() {
    return this.today.diff(this.rentalEnd, 'days') >= 0;
  }

  get margin() {
    return this.markerSize / 2;
  }

  get minDayDistance() {
    return this.dayWidth > 0
      ? Math.ceil(this.labelWidth / this.dayWidth)
      : 0;
  }

  get start() {
    return this._vm.start || this.today;
  }

  set start(start) {
    this._vm.start = start;
  }

  get strokeWidth() {
    return this._vm.strokeWidth || this.markerSize / 2;
  }

  set strokeWidth(strokeWidth) {
    this._vm.strokeWidth = strokeWidth;
  }

  get totalDays() {
    return this.start && this.end
      ? this.calculateDayNumber(this.end)
      : 0;
  }

  get visibleDayCount() {
    return this.totalDays > WorkplaceRentalTimelineDefaultOptions.visibleDayCount
      ? WorkplaceRentalTimelineDefaultOptions.visibleDayCount
      : this.totalDays;
  }

  set visibleDayCount(visibleDayCount) {
    this._vm.visibleDayCount = Number(visibleDayCount);
  }

  get xPosition() {
    return this.calculateX(this.active.start, false);
  }

  calculateActive() {
    const halfVisible = Math.floor(this.visibleDayCount / 2);

    const fromStartCount = this.today.diff(this.start, 'days');
    const toEndCount = this.end.diff(this.today, 'days');

    let start = 0;
    let end = this.visibleDayCount;

    if (toEndCount >= 0 && toEndCount <= halfVisible) {
      start = this.totalDays - this.visibleDayCount;
      end = this.totalDays;
    }

    if (fromStartCount > halfVisible && toEndCount > halfVisible) {
      const todayNumber = this.calculateDayNumber(this.today);

      start = todayNumber - halfVisible;
      end = start + this.visibleDayCount;
    }

    return { start, end };
  }

  calculateDayNumber(fromDay) {
    return fromDay.clone().startOf('day').diff(this.start.clone().startOf('day'), 'days');
  }

  calculateX(dayNumber, hasMargin) {
    const margin = hasMargin ? this.margin : 0;

    return dayNumber > 0
      ? (this.dayWidth * dayNumber) + margin
      : margin;
  }

  draw() {
    this.drawBars();
    this.drawBookends();
    this.drawMarkers();
    this.drawLabels();
  }

  drawBar(start, end, yPosition = this.center.y) {
    const dayStart = this.calculateDayNumber(start);
    const dayEnd = this.calculateDayNumber(end) - dayStart;

    return { d: `m${this.calculateX(dayStart, true)},${yPosition} l${this.calculateX(dayEnd, false)},0` };
  }

  drawBars() {
    const progressEnd = this.today.isAfter(this.rentalEnd) ? this.rentalEnd : this.today;
    const barEnd = this.start.clone().add(this.active.end, 'days');
    const barStart = this.start.clone().add(this.active.start, 'days');

    this.bar = {
      leniencies: this.filteredLeniencies.map((len) => {
        const endDate = barEnd.isBefore(len.endDate) ? barEnd : len.endDate;
        const startDate = barStart.isAfter(len.startDate) ? barStart : len.startDate;
        return {
          paths: [
            this.drawBar(startDate, endDate, this.center.y - this.strokeWidth - 1),
            this.drawBar(startDate, endDate),
            this.drawBar(startDate, endDate, this.center.y + this.strokeWidth + 1),
          ],
          tooltip: `<p>
            <span class="fs-small c-gray-dark">
              Leniency
            </span><br/>
            <span class="fs-xsmall c-gray">
              ${len.startDate.clone().format(dateFormat.rentalTimeline)} - ${len.endDate.clone().format(dateFormat.rentalTimeline)}
            </span>
          </p>`,
        };
      }),
      overdue: this.drawBar(this.rentalEnd, this.today),
      progress: this.drawBar(this.rentalStart, progressEnd),
      timeline: this.drawBar(this.start, this.end),
    };
  }

  drawBookends() {
    const strokeHeight = (this.strokeWidth / 2) + this.strokeWidth + 1;
    const lineStart = this.center.y - strokeHeight;

    this.bookend = {
      end: {
        d: `m${this.calculateX(this.active.end, true)},${lineStart}
          l0,${this.bookendHeight}`,
      },
      start: {
        d: `m${this.calculateX(this.active.start, true)},${lineStart}
          l0,${this.bookendHeight}`,
      },
    };
  }

  /**
   * Additional functionality can be added and isolated in the reusable method,
   * but for now it's purpose is to shorten the crowded label assignments that call it.
   * @param {Number} xPosition Starting x-point for the text element
   * @param {Number} yPosition Starting y-point for the text element
   * @param {String} text The label displayed in the text element
   * @return {{text: string, x: number, y: number}} object
   */
  drawLabel(xPosition, yPosition, text, textAnchor) {
    return {
      text,
      textAnchor,
      x: xPosition,
      y: yPosition,
    };
  }

  /**
   * This acts as the initial setup controller for all labels.
   * Start and End labels will be reset when the visible days are changed.
   * Bill Thru and Rental Start/End labels are permanent and do not get reset.
   * @todo Consolidate similar drawLabels functionality for:
   *
   * 1. Start and End methods
   * 2. Rental and Bill Thru methods
   */
  drawLabels() {
    const rentalDiff = this.rentalEnd.diff(this.rentalStart, 'days');
    const hasBothLabels = rentalDiff > this.minDayDistance;
    const isSingleDay = rentalDiff === 0;
    const isVisibleBillThru = this.billThru && this.calculateDayNumber(this.billThru) >= this.active.start
      && this.active.end >= this.calculateDayNumber(this.billThru);
    const isVisibleEnd = this.calculateDayNumber(this.rentalEnd) >= this.active.start
      && this.active.end >= this.calculateDayNumber(this.rentalEnd);
    const isVisibleStart = this.calculateDayNumber(this.rentalStart) === this.active.start;

    this.label = {
      billThru: isVisibleBillThru ? this.drawLabelsBillThru() : {},
      end: this.drawLabelsEnd(),
      rentalEnd: isVisibleEnd ? this.drawLabelsRentalEnd(isSingleDay) : {},
      rentalStart: isVisibleStart ? this.drawLabelsRentalStart(isSingleDay, hasBothLabels) : {},
      start: this.drawLabelsStart(),
    };
  }

  drawLabelsBillThru() {
    const dayNumber = this.calculateDayNumber(this.billThru);
    const x = this.calculateX(dayNumber, true);
    const y = this.center.y - this.markerLineHeight;

    const labelBottom = this.billThru.clone().format(dateFormat.rentalTimeline);
    const labelTop = 'Bill Thru';

    const textAnchor = dayNumber - this.active.start > this.minDayDistance
      ? 'end'
      : 'start';

    return {
      bottom: this.drawLabel(x, y, labelBottom, textAnchor),
      top: this.drawLabel(x, y - (this.fontSize * 1.25), labelTop, textAnchor),
    };
  }

  drawLabelsEnd() {
    const day = this.start.clone().add(this.active.end, 'days');
    const x = this.calculateX(this.active.end, true) - 4;
    const y = this.center.y + this.markerLineHeight;

    const label = day.format(dateFormat.rentalTimeline);

    return this.drawLabel(x, y, label, 'end');
  }

  drawLabelsRentalEnd(isSingleDay) {
    if (isSingleDay) {
      return {};
    }

    const dayNumber = this.calculateDayNumber(this.rentalEnd);
    const x = this.calculateX(dayNumber, true);
    const y = this.center.y + this.markerLineHeight;

    const labelBottom = this.rentalEnd.clone().format(dateFormat.rentalTimeline);
    const labelTop = 'Est. End';

    const textAnchor = dayNumber - this.active.start > this.minDayDistance + 1
      ? 'end'
      : 'start';

    return {
      bottom: this.drawLabel(x, y + (this.fontSize * 1.25), labelBottom, textAnchor),
      top: this.drawLabel(x, y, labelTop, textAnchor),
    };
  }

  drawLabelsRentalStart(isSingleDay, hasBothLabels) {
    if (!isSingleDay && !hasBothLabels) {
      return {};
    }

    const dayNumber = this.calculateDayNumber(this.rentalStart);
    const x = this.calculateX(dayNumber, true);
    const y = this.center.y + (this.markerLineHeight);

    const labelBottom = this.rentalStart.clone().format(dateFormat.rentalTimeline);
    const labelTop = isSingleDay ? 'Start/End' : 'Start';

    return {
      bottom: this.drawLabel(x, y + (this.fontSize * 1.25), labelBottom, 'start'),
      top: this.drawLabel(x, y, labelTop, 'start'),
    };
  }

  drawLabelsStart() {
    const day = this.start.clone().add(this.active.start, 'days');
    const x = this.calculateX(this.active.start, true) + 4;
    const y = this.center.y + this.markerLineHeight;

    const label = day.format(dateFormat.rentalTimeline);

    return this.drawLabel(x, y, label, 'start');
  }

  drawMarker(date, isSmall) {
    const dayStart = this.calculateDayNumber(date);
    const lineStart = this.center.y - (this.markerLineHeight / 2);

    return {
      inner: {
        cx: this.calculateX(dayStart, true),
        cy: this.center.y,
        r: isSmall ? this.markerSize / 6 : this.markerSize / 4,
      },
      line: {
        d: `m${this.calculateX(dayStart, true)},${lineStart}
          l0,${this.markerLineHeight}`,
      },
      outer: {
        cx: this.calculateX(dayStart, true),
        cy: this.center.y,
        r: isSmall ? this.markerSize / 4 : this.markerSize / 2,
      },
    };
  }

  drawMarkers() {
    this.marker = {
      billThru: this.billThru ? this.drawMarker(this.billThru, true) : '',
      overdue: this.isOverdue ? this.drawMarker(this.rentalEnd, false) : '',
      rentalEnd: this.drawMarker(this.rentalEnd, false),
      rentalStart: this.drawMarker(this.rentalStart, false),
      today: this.today.diff(this.start) >= 0 ? {
        path: this.drawMarker(this.today, true),
        tooltip: `<p>
          <span class="fs-small c-gray-dark">
            Today
          </span><br/>
          <span class="fs-xsmall c-gray">
            ${this.today.clone().format(dateFormat.rentalTimeline)}
          </span>
        </p>`,
      } : {},
    };
  }

  hasBookend(bookend) {
    const bookendDay = this.start.clone().add(this.active[bookend], 'days');

    const fromEnd = bookendDay.diff(this.rentalEnd, 'days');
    const fromStart = bookendDay.diff(this.rentalStart, 'days');

    return fromStart !== 0 && fromEnd !== 0;
  }

  nextActive() {
    let dayStart = this.active.start + this.visibleDayCount;
    let dayEnd = dayStart + this.visibleDayCount;

    if (dayEnd > this.totalDays) {
      dayStart = this.totalDays - this.visibleDayCount;
      dayEnd = this.totalDays;
    }

    return {
      end: dayEnd,
      start: dayStart,
    };
  }

  previousActive() {
    let dayStart = this.active.start - this.visibleDayCount;
    let dayEnd = dayStart + this.visibleDayCount;

    if (dayStart < 0) {
      dayStart = 0;
      dayEnd = this.visibleDayCount;
    }

    return {
      end: dayEnd,
      start: dayStart,
    };
  }

  resetActive(direction) {
    this.active = this[`${direction}Active`]();

    this.label.end = this.drawLabelsEnd();
    this.label.start = this.drawLabelsStart();

    this.drawBars();
    this.drawBookends();
    this.drawLabels();
  }

  resize() {
    const width = this.$root ? this.$root.offsetWidth : 0;

    const frameWidth = width - (this.margin * 2);
    const daysToShow = this.visibleDayCount > 0
      ? this.visibleDayCount
      : WorkplaceRentalTimelineDefaultOptions.totalDayCount;

    this.active = this.calculateActive();

    this.dayWidth = Math.floor(frameWidth / daysToShow);
    this.width = (this.dayWidth * daysToShow) + (this.margin * 2);

    this.dayGuides = _drawDayGuides({
      calcX: this.calculateX.bind(this),
      height: this.height,
      dayCount: this.totalDays,
    }, [], 0);
  }
}

/**
 * This is mostly for sanity while developing, it's a way to visualize
 * the days in a timeline. It's not required for the component to work.
 */
function _drawDayGuides(data, paths, index) {
  paths.push({
    d: `m${data.calcX(index, true)},0 l0,${data.height}`,
  });

  index += 1;

  return data.dayCount >= index
    ? _drawDayGuides(data, paths, index)
    : paths;
}
