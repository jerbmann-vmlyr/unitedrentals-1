import Vue from 'vue';
import { loaded } from 'vue2-google-maps';
import { mapState, mapGetters } from 'vuex';
import { get } from 'lodash';
import types from '@/store/types';
import { WorkplaceEquipmentDetailsState } from '@/store/module.workplace.equipment-details';
import { WorkplaceRootState } from '@/store/module.workplace';
import UrSpinner from '@/components/common/ur-loading-spinner.vue';

export default Vue.extend({
  components: {
    UrSpinner,
  },
  async mounted() {
    await loaded;
    this.googleIsReady = true;
    window.addEventListener('resize', this.recenterMap.bind(this));
  },
  beforeDestroy() {
    window.removeEventListener('resize', this.recenterMap.bind(this));
  },
  data() {
    return {
      googleIsReady: false,
    };
  },
  computed: {
    ...mapState<WorkplaceRootState & { workplaceEquipmentDetails: WorkplaceEquipmentDetailsState }>({
      detailItemCoordinates: state => state.workplaceEquipmentDetails.detailItemCoordinates,
      detailItemCoordinatesPending: state => state.workplaceEquipmentDetails.detailItemCoordinatesPending,
      catalogInfoOrNull: state => state.catalog.equipment,
    }),
    ...mapGetters({
      detailItemOrNull: types.workplace.equipmentDetails.detailItemOrNull,
    }),
    itemImageSrc(): any {
      const { catClass = '' } = this.detailItemOrNull || {};

      // Sometimes the image src is in a string, sometimes it's inside
      // an object like this: { link: "src..." }
      const src = get(this.catalogInfoOrNull, [catClass, 'images', 0], '');
      if (typeof src === 'string' && src.length > 0) {
        return src;
      }
      const srcFromObject = get(this.catalogInfoOrNull, [catClass, 'images', 0, 'link'], '');

      if (typeof srcFromObject === 'string' && srcFromObject.length > 0) {
        return srcFromObject;
      }

      return '/themes/custom/unitedrentals/images/icons/icon-equipment.png';
    },
    coordinates(): { lat: number, lng: number } {
      return {
        lat: this.detailItemCoordinates.latitude || 39.110298,
        lng: this.detailItemCoordinates.longitude || -94.581078,
      };
    },
    markers(): any[] {
      return [
        {
          position: this.coordinates,
          animation: google.maps.Animation.DROP,
          icon: {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 35,
            fillColor: '#FFFFFF',
            fillOpacity: 1,
            strokeColor: '#008CFF',
            strokeWeight: 2,
          },
        },
        {
          position: this.coordinates,
          animation: google.maps.Animation.BOUNCE,
          icon: {
            scaledSize: new google.maps.Size(42, 42),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(20, 20),
            url: this.itemImageSrc,
          },
        },
      ];
    },
  },
  methods: {
    recenterMap() {
      if (this.$refs.map) {
        setTimeout(() => this.$refs.map.panTo(this.$refs.map.center));
      }
    },
  },
});
