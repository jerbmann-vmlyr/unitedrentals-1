<script>
/* eslint-disable */
import { mapState, mapMutations } from 'vuex';
import { WorkplaceRouteNames } from '@/lib/workplace';
import { WorkplaceRoutesPathRoot } from '../../routes/workplace.routes';
import types from '@/store/types';
import { WorkplaceRouteUtils } from '../../lib/workplace/routes';
import Breadcrumbs from './mixins/breadcrumbs';

/*
 * @vuese
 * @group Mixins
 * Workplace Routes Manager Mixin
 */
export default {
  name: 'WorkplaceRoutesManager',
  mixins: [Breadcrumbs],

  data() {
    return {
      /*
       * @vuese
       * @type Object
       * Determine if we have deferred incoming route
       */
      deferredIncomingRoute: {
        to: null,
        from: null,
        next: null,
      },
    };
  },

  computed: {
    ...mapState({
      /*
       * @vuese
       * @type Boolean
       * Determine if accounts for current user have been fetched
       */
      accountsReady: state => state.accounts.accountsFetched,
      /*
       * @vuese
       * @type Account[]
       * All user accounts
       */
      allAccounts: state => state.accounts.all,
      /*
       * @vuese
       * Get the globally selected account id displayed in top right nav on desktop
       */
      selectedGlobalAccountId: state => state.accounts.selectedAccountId,
      /*
       * @vuese
       * @type Boolean
       * Determine if user is authenticated
       */
      userIsLoggedIn: state => state.user.isLoggedIn,
      /*
       * @vuese
       * @type Boolean
       * Determine if session storage is ready
       */
      storageReady: state => state.storageReady,
      /*
       * @vuese
       * Get the default accountId for the current user
       */
      userDefaultAccountId: state => state.user.defaultAccountId,
    }),

    /*
     * @vuese
     * @type Boolean
     * Determine if user is using authenticated account
     */
    accountValidationReady() {
      if (!this.userIsLoggedIn) {
        return true;
      }

      const allDataReady = this.accountsReady && this.storageReady;

      return allDataReady && (!!this.userDefaultAccountId || this.userHasNoAccount);
    },

    /*
     * @vuese
     * @type Boolean
     * Determined if user does not have an account
     */
    userHasNoAccount() {
      return this.accountsReady && Object.keys(this.allAccounts).length === 0;
    },
  },

  methods: {
    ...mapMutations({
      /*
       * @vuese
       * @type commit
       * Set default account id for current user
       */
      setDefaultAccountId: types.user.setDefaultAccountId,
    }),
    /*
     * @vuese
     * @type Middleware
     * Middleware to alter route before user is allowed to access workplace routes
     */
    alterIncomingRoute(incomingRoute) {
      this.$root.$emit('previous-incoming-route', { previousIncomingRoute: incomingRoute })
      let alterableIncomingRoute = { ...incomingRoute };

      if (!this.userIsLoggedIn) {
        alterableIncomingRoute = { name: WorkplaceRouteNames.Unauth };
      }

      // Users w/o an account must see the No Equipment On Rent dashboard.
      // We also supply an intentionally undefined account id.
      else if (this.userHasNoAccount) {
        alterableIncomingRoute = {
          name: WorkplaceRouteNames.NoAccount,
        };
      }

      // If the account id is invalid, substitute the user's
      // default account id and throw them to the dashboard
      else if (!this.validateRouteAccountId(alterableIncomingRoute)) {
        alterableIncomingRoute = {
          name: WorkplaceRouteNames.Dashboard,
          params: {
            accountId: this.userDefaultAccountId,
          },
        };
      }

      // Set up the query params
      const noQueryParams = !Object.keys(alterableIncomingRoute.query || {}).length;
      if (noQueryParams && this.userIsLoggedIn) {
        // If there are no query params AND this is the equipment list page
        if (incomingRoute.name === 'workplace:equipment:list') {
          alterableIncomingRoute.query = WorkplaceRouteUtils.specialQueryParams({});
        }
        else {
          alterableIncomingRoute.query = WorkplaceRouteUtils.normalizeQueryParams({});
        }
      }

      const resolved = this.$router.resolve(alterableIncomingRoute);

      return resolved.route;
    },

    /*
     * @vuese
     * @type Middleware
     * Business Rules Middleware that may potentially defer a route this is not ready to be processed
     */
    beforeRouteEnterInterceptor(to, from, next) {
      // Authenticated Users can go to any route, but only after we can
      // validate the account. If we can't do that yet, we defer the route change.
      if (!this.accountValidationReady) {
        this.deferredIncomingRoute = { to, from, next };
        return;
      }

      // Otherwise we validate and potentially alter the route before changing to it.
      // Note: by checking for No Alterations before invoking `next` we prevent an infinite loop
      // Note2: `next` does not respond to a "fullPath" option-key. However, sending in the "path" option-key
      // w/a "fullPath" value seems to work just fine.  (The full path includes query params.)
      const alteredRoute = this.alterIncomingRoute(to);
      const noAlterations = to.fullPath === alteredRoute.fullPath;

      return noAlterations ? next() : next({ path: alteredRoute.fullPath });
    },

    /*
     * @vuese
     * @type Middleware
     * Validate route after user has entered into the workplace component
     */
    afterRouteEnterCallback() {
      const { accountId } = this.$route.params;

      if (accountId) {
        this.setDefaultAccountId(accountId);
      }
    },

    /*
     * @vuese
     * @type Boolean
     * Determine if route has a valid account id
     */
    validateRouteAccountId(route) {
      return Object.keys(this.allAccounts).includes(route.params.accountId);
    },
  },
  created() {
    this.$router.beforeEach(this.beforeRouteEnterInterceptor);
    this.$router.afterEach(this.afterRouteEnterCallback);
  },

  mounted() {
    // If the user isn't at the Root Workplace Route, force them there.
    // (This occurs when users go to `www.unitedrentals.com/manage`)
    // Note: we cannot rely on `this.$route.fullPath`, so we read the location hash.

    if (!window.location.hash.includes(WorkplaceRoutesPathRoot)) {
      this.$router.push({
        name: WorkplaceRouteNames.Root,
        params: {
          accountId: this.userDefaultAccountId,
        },
      });
    }
  },

  watch: {
    /*
     * @vuese
     * Once account is validated, we invoke any deferred route changes
     */
    accountValidationReady: {
      immediate: true,
      handler(accountValidationReady) {
        if (!accountValidationReady) return;

        // If we deferred a route change we are now ready to invoke it.
        const { to, next } = this.deferredIncomingRoute;
        if (to && next) {
          // Validate and potentially alter the deferred route before going to it.
          const alteredRoute = this.alterIncomingRoute(to);
          return next({ path: alteredRoute.fullPath }); // setting "path" to "fullPath" is intentional. "fullPath" as a key does not work
        }

        // Our "Before Route Enter Interceptor" will NOT be invoked for deep routes entered directly into the browser.
        // So if the original URL (aka, Bookmark or Copy/Pasted-URL) has an invalid accountId, replace that account id!
        if (this.$route.params.accountId && !this.validateRouteAccountId(this.$route)) {
          this.$router.replace({
            ...this.$router.currentRoute,
            params: {
              accountId: this.userDefaultAccountId,
            },
          });
        }
      },
    },

    /*
     * @vuese
     * If global account id is changed, change the route with the updated accountId
     * and redirect the user to the dashboard or given resource listing page
     */
    selectedGlobalAccountId(accountId) {
      const { name } = this.$route;
      const isWorkplaceRouteName = Object.values(WorkplaceRouteNames).includes(name);
      const isEquipmentDetails = name === WorkplaceRouteNames.EquipmentDetails;

      this.$router.replace({
        name: isWorkplaceRouteName && !isEquipmentDetails
          ? name
          : WorkplaceRouteNames.Default,
        params: { accountId },
        query: { ...(this.$route.query || {}) },
      });
    },
  },
};
</script>
