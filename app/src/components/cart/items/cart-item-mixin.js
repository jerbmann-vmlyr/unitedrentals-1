import { mapGetters, mapState } from 'vuex';
import { find, get } from 'lodash';

export default {
  computed: {
    ...mapGetters([
      'getCartItemEstimate',
    ]),
    ...mapState({
      equipment(state) {
        if (!this.item) return {};
        return state.catalog.equipment[this.item.catClass] || {};
      },
      item(state) {
        // The Static Cart Item is given a transaction block id. We have to search there to find the catclass.
        if (this.transactionBlockId) {
          return find(state.checkout.transactionBlocks[this.transactionBlockId].items, item => item.id === this.itemId);
        }

        if (this.isSavedForLater) {
          return state.cartSaveForLater.items[this.itemId];
        }

        // Otherwise, search the cart to find the cat class.
        const cartModule = state.cart;
        return get(cartModule, ['items', this.itemId], {});
      },
      rate(state) {
        return get(state.catalog.rates, [this.item.catClass, this.item.branchId], {}).dayRate;
      },
    }),
    equipmentImgSrc() {
      const img = get(this.equipment, ['images', [0]]);
      return get(img, 'link');
    },
    equipmentImgAlt() {
      const img = get(this.equipment, ['images', [0]]);
      return get(img, 'alt');
    },
    estimateIsZero() {
      return this.estimatedRentalAmount === 0 || this.estimatedRentalAmount === '0';
    },
    estimatedRentalAmount() {
      return get(this.getCartItemEstimate(this.itemId, this.transactionBlockId), ['total']);
    },
  },
};
