import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import CartIcon from '@/components/cart/cart-icon';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('The Cart Icon', () => {
  let getters;
  let store;

  beforeEach(() => {
    getters = {
      small: () => false,
      medium: () => false,
      large: () => true,
      xlarge: () => false,
      cartSize: () => '2',
      localCartSize: () => '2',
    };

    store = new Vuex.Store({
      getters,
    });
  });

  it('shows how many items are in the cart.', () => {
    const wrapper = shallowMount(CartIcon, { store, localVue });
    const p = wrapper.find('.pill-count');
    expect(p.text()).toBe(getters.cartSize());
  });

  it('matches snapshot', () => {
    const wrapper = shallowMount(CartIcon, { store, localVue });
    expect(wrapper.html()).toMatchSnapshot();
  });
});
