import { Progress } from '@/lib/cart';

export default {
  computed: {
    inactive() {
      return this.progress === Progress.INACTIVE;
    },
    ready() {
      return this.progress === Progress.READY;
    },
    active() {
      return this.progress === Progress.ACTIVE;
    },
    summary() {
      return this.progress === Progress.SUMMARY;
    },

    // this.progress is statically set to Progress.COMPLETE via the transaction-block.vue
    complete() {
      return this.progress === Progress.COMPLETE;
    },
  },
  methods: {
    gotoInactive() {
      this.$emit('update:progress', Progress.INACTIVE);
    },
    gotoReady() {
      this.$emit('update:progress', Progress.READY);
    },
    gotoActive() {
      this.$emit('update:progress', Progress.ACTIVE);
    },
    gotoSummary() {
      this.$emit('update:progress', Progress.SUMMARY);
    },
  },
  props: {
    progress: {
      type: String,
    },
  },
};
