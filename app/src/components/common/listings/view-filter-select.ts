import Vue from 'vue';
import UrSelect from '@/components/common/form/ur-select.vue';
import { ViewFilter } from '@/lib/listings';
import { Prop } from 'vue/types/options';

export default Vue.extend({
  components: {
    UrSelect,
  },
  props: {
    emptyOptionLabel: {
      required: false,
      type: String,
      default: 'View all',
    },
    filters: {
      required: true,
      type: Array as Prop<ViewFilter[]>,
    },
    items: {
      required: false,
      type: Array as Prop<any[]>,
      default: [],
    },
    value: {
      required: false,
      type: String,
      default: '',
    },
  },
  data() {
    return {
      selectedFilterId: '',
    };
  },
  mounted() {
    this.selectedFilterId = this.value || '';
  },
  computed: {
    filterIsSelected(): boolean {
      return this.selectedFilterId.length > 0;
    },
    showCounts(): boolean {
      return this.items.length > 0;
    },
  },
  methods: {
    getCurrentFilter(): ViewFilter|void {
      return this.filters.find(f => `${f.id}` === this.selectedFilterId);
    },
    filterChange(filterId: string) {
      const newFilter = this.filters.find(f => `${f.id}` === filterId);
      if (this.getCurrentFilter()) {
        this.$emit('filterDeselected', this.getCurrentFilter());
      }
      if (newFilter) {
        this.$emit('filterSelected', newFilter);
      }
      this.selectedFilterId = filterId;
    },
    getCountOf(filter: ViewFilter): number {
      return this.items.reduce(
        (sum, item) => sum + (filter.asFunction(item) ? 1 : 0),
        0,
      );
    },
  },
});
