import Vue from 'vue';
import { SearchByTermFilter } from '@/lib/listings';
import { Prop } from 'vue/types/options';

export default Vue.extend({
  computed: {
    hasFilter(): boolean {
      return this.filters.length > 0;
    },
  },
  props: {
    filters: {
      type: Array as Prop<SearchByTermFilter[]>,
      required: true,
    },
  },
  methods: {
    removeFilter(filter: SearchByTermFilter): void {
      this.$emit('removeFilter', filter);
    },
    removeAllFilters(): void {
      this.$emit('removeAllFilters', this.filters);
    },
  },
});
