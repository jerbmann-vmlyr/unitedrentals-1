import Card from './card.vue';
import CardHeader from './card-header.vue';
import CardBody from './card-body.vue';
import CardFooter from './card-footer.vue';

export { Card, CardHeader, CardBody, CardFooter };

/* istanbul ignore next */
Card.install = vue => {
  vue.component(Card.name, Card);
  vue.component(CardHeader.name, CardHeader);
  vue.component(CardBody.name, CardBody);
  vue.component(CardFooter.name, CardFooter);
};

export default Card;
