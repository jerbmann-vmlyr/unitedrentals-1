import { shallowMount } from '@vue/test-utils';

import UrTabs from '@/components/common/ur-tabs';

describe('ur-tabs.vue', () => {
  const wrapper = shallowMount(UrTabs, {
    stubs: ['router-link'],
    propsData: {
      tabs: [
        {
          path: '/path-one',
          label: 'path one',
        },
        {
          path: '/path-two',
          label: 'path two',
        },
        {
          path: '/path-three',
          label: 'path three',
        },
      ],
    },
  });

  it('has 3 tabs', () => {
    const $listItems = wrapper.findAll('li');
    expect($listItems.length).toBe(wrapper.vm.tabs.length);
  });

  it('matches snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});
