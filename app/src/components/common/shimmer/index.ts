import UrShimmer from './shimmer-overlay.vue';

export { UrShimmer };

/* istanbul ignore next */
UrShimmer.install = vue => {
 vue.component(UrShimmer.name, UrShimmer);
};

export default UrShimmer;
