import Vue from 'vue';

export default Vue.extend({
  template: `<i :class="classes" />`,
  name: 'ur-section-collapse-icon',
  computed: {
    classes(): { [cssClass: string]: boolean } {
      return {
        icon: true,
        'icon-chevron-down': !this.collapsed,
        'icon-chevron-right': this.collapsed,
      };
    },
  },
  props: {
    collapsed: {
      type: Boolean,
      required: true,
    },
  },
});
