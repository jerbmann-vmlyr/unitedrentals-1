import UrErrorLabel from '@/components/common/form/ur-error-label';

export default {
  components: {
    UrErrorLabel,
  },
  data() {
    return {
      localValue: this.value,
    };
  },
  methods: {
    onInput(val) {
      this.localValue = val;
      this.$emit('input', val);
    },
  },
  props: {
    disabled: {
      type: Boolean,
      required: false,
    },
    placeholder: {
      type: String,
      required: false,
    },
    value: {
      required: true,
    },
  },
  watch: {
    value(parentValue) {
      if (parentValue !== this.localValue) {
        this.localValue = parentValue;
      }
    },
  },
};
