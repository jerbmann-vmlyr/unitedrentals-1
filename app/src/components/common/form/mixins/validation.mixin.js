export default {
  mounted() {
    // This component's `v.$params` & `vMessages` props must have the same shape.
    // (Why? Because error messages are linked to errors by these objects' keys.)

    // A custom Vue Props Validator function cannot communicate with other props nor the component instance,
    // so we validate v.$params and vMessages here.

    if (!this.v) {
      return true;
    }

    const vParamsKeys = this.v && typeof this.v.$flattenParams === 'function' ? this.v.$flattenParams() : [];
    const vMessagesKeys = Object.keys(this.vMessages || {});

    const isValid = vMessagesKeys.reduce((valid, messageKey) => {
      vParamsKeys.forEach(({ path }) => {
        if (path.includes(messageKey)) {
          valid = true;
        }
      });
      return valid;
    }, false);

    if (!isValid) {
      console.error(`Bad Validation Configuration in a "@/components/common/form" component. (${this.label}.) Please fix props for v and vMessages so that vMessages keys are a subset of v.$params keys.`); // eslint-disable-line
    }
  },
  props: {
    v: {
      type: Object,
      required: false,
    },
    vMessages: {
      type: Object,
      required: false,
    },
  },
};
