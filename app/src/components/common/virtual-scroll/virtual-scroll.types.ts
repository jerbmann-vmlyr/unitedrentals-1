export type VirtualScrollPosition = 'top' | 'middle' | 'bottom' | 'ally';

export interface VirtualScrollRange {
  scrollPosition: VirtualScrollPosition;
  startIndex: number;
  endIndex: number;
  itemTotal: number;
}
