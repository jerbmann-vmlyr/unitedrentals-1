import { range } from 'lodash';
import Vue, { VNode, VNodeData } from 'vue';
import { VirtualScrollPosition, VirtualScrollRange } from './virtual-scroll.types';

export default Vue.extend({
  name: 'virtual-scroll',
  props: {
    /**
     * The height of each item in the list.
     * If all of the rows are the same height, just give it a number.
     * If the rows are of varying height, give it a function.
     * The function should be of type `(index: number) => number`, where `index`
     * is the item index, and the number returned is that item's
     * height (in number of pixels).
     */
    itemHeight: {
      type: [Number, Function],
      required: true,
    },
    /**
     * The total number of items--including items not visible--wrapped
     * by this component.
     */
    itemTotal: {
      type: Number,
      required: true,
    },
    /**
     * The list's root element tag name.
     */
    rootTag: {
      type: String,
      default: 'div',
    },
    /**
     * The list's viewport element tag name.
     */
    viewportTag: {
      type: String,
      default: 'div',
    },
    /**
     * The initial scroll position of the list, in units of number of items.
     * Must be an integer and less than the length of items in the list.
     */
    gotoIndex: {
      type: Number,
      default: 0,
    },
  },

  data() {
    return {
      clientHeight: 0,
      scrollHeight: 0,
      scrollTop: 0,
    };
  },

  render(h): VNode {
    const viewportData: VNodeData = {
      style: {
        display: 'block',
        paddingTop: `${this.paddingTop}px`,
        paddingBottom: `${this.paddingBottom}px`,
      },
      class: `vscroll vscroll__viewport`,
    };

    const itemWrapper = h(this.viewportTag, viewportData, this.getChildren());

    const rootData: VNodeData = {
      ref: 'vsl',
      class: `vscroll vscroll__root vscroll--${this.scrollPosition}`,
      on: { scroll: () => this.updateElementMeasurements() },
      style: {
        display: 'block',
        overflowY: 'auto',
        height: '100%',
        marginRight: '-1rem',
      },
    };

    return h(this.rootTag, rootData, [ itemWrapper ]);
  },

  mounted() {
    (window as any).vscroll = this;
    window.addEventListener('resize', this.updateElementMeasurements.bind(this));
    if (this.gotoIndex) {
      this.$nextTick(() => this.scrollTo(this.gotoIndex));
    }
    this.updateElementMeasurements();
    this.emitRange();
  },
  destroyed() {
    window.removeEventListener('resize', this.updateElementMeasurements.bind(this));
  },
  watch: {
    scrollPosition: {
      immediate: true,
      handler(current, previous) {
        this.$emit('scrollPositionChange', { current, previous });
      },
    },
    itemTotal(is, was) {
      this.updateElementMeasurements();
      this.$forceUpdate();
      this.$nextTick(() => this.emitRange());
    },
    gotoIndex(gotoIndex) {
      if (typeof gotoIndex === 'number') this.scrollTo(gotoIndex);
    },
    visibleRange(newRange, oldRange) {
      if (newRange.start !== oldRange.start || newRange.end !== oldRange.end) {
        this.emitRange();
      }
    },
  },

  computed: {
    // Returns the item index at which the iterative summation of item heights' exceeds scrollTop
    visibleRange(): { start: number, end: number } {
      let heightAccum = 0;

      return this.itemHeights.reduce(({ start, end }, h, i) => {
        heightAccum += h;
        return {
          start: heightAccum <= this.scrollTop ? (i + 1) : start,
          end: heightAccum <= (this.scrollTop + this.clientHeight) ? (i + 1) : end,
        };
      }, { start: 0, end: 0 });
    },
    atEnd(): boolean {
      return this.scrollHeight - (this.clientHeight + this.scrollTop) < 5;
    },
    virtualTopItemHeights(): number[] {
      return this.itemHeights.slice(0, this.visibleRange.start);
    },
    virtualBottomItemHeights(): number[] {
      return this.itemHeights.slice(this.visibleRange.end, this.itemTotal);
    },
    itemHeights(): number[] {
      return range(0, this.itemTotal - 1).map(i =>
        typeof this.itemHeight === 'function'
          ? this.itemHeight(i)  // Dynamic item heights
          : this.itemHeight,    // Static item heights
      );
    },
    paddingTop(): number {
      return this.virtualTopItemHeights.reduce((s, n) => s + n, 0);
    },
    paddingBottom(): number {
      return this.virtualBottomItemHeights.reduce((s, n) => s + n, 0);
    },
    virtualHeight(): number {
      return this.itemHeights.reduce((s, n) => s + n, 0);
    },
    scrollPosition(): VirtualScrollPosition {
      const paddingTotal = this.paddingTop + this.paddingBottom;

      return paddingTotal < 1
        ? 'ally'
        : this.scrollTop < 1
          ? 'top'
          : this.scrollTop >= paddingTotal
            ? 'bottom'
            : 'middle';
    },
  },

  methods: {
    emitRange(): void {
      if (this.$listeners && this.$listeners.rangeChange) {
        const event: VirtualScrollRange = {
          startIndex: this.visibleRange.start,
          endIndex: this.visibleRange.end,
          scrollPosition: this.scrollPosition,
          itemTotal: this.itemTotal,
        };

        this.$emit('rangeChange', event);
      }
    },

    getChildren(): VNode[] {
      const items: VNode[] = this.$slots.default || [];
      return items.slice(this.visibleRange.start, this.visibleRange.end + 1);
    },

    getEl(): Element {
      const vsl = this.$el;
      if (!this.isEl(vsl)) throw new Error(`Virtual scroll $el not on the DOM!`);
      return vsl;
    },

    isEl(value: any): value is Element {
      return value instanceof Element;
    },

    scrollTo(gotoIndex: number): void {
      const gotoHeight = this.itemHeights.slice(0, Math.max(0, gotoIndex) + 1).reduce((sum, h) => sum + h, 0);
      this.$nextTick(() => this.getEl().scrollTo({
        top: gotoHeight,
        behavior: 'smooth',
      }));
    },

    /**
     * Updates this component's element measurements properties according to
     * this component's host element.
     */
    updateElementMeasurements() {
      const el: HTMLElement = this.getEl();
      this.scrollHeight = el.scrollHeight || 0;
      this.clientHeight = el.clientHeight || 0;
      this.scrollTop = el.scrollTop || 0;
    },
  },
});
