import UrMenu from './ur-menu.vue';

export { UrMenu };

/* istanbul ignore next */
UrMenu.install = vue => {
  vue.component(UrMenu.name, UrMenu);
};

export default UrMenu;
