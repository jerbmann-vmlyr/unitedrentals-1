import Vue from 'vue';
import { orderBy as _orderBy } from 'lodash';
import { mapState } from 'vuex';
import { InFlightRequestsState } from '@/store/module.in-flight-requests';
import UrLoadingSpinner from '@/components/common/ur-loading-spinner.vue';
import types from '@/store/types';

interface InFlightCount {
  token: string;
  count: number;
}

export default Vue.extend({
  name: 'flightTrackingDemo',
  components: {
    UrLoadingSpinner,
  },
  methods: {
    sortFlights(flights: InFlightCount[]): InFlightCount[] {
      return _orderBy(flights, ['count', 'token'], ['desc', 'asc']);
    },
  },
  computed: {
    ...mapState<{ inFlightRequests: InFlightRequestsState }>({
      inFlightCounts: state => ({ ...state.inFlightRequests.inFlightCounts }),
    }),

    tokensInFlight(): string[] {
      return this.$store.getters[types.inFlightRequests.tokensInFlight];
    },

    inFlightCountsAsList(): InFlightCount[] {
      type InFlightCounts = { [token: string]: number };
      return Object.entries(this.inFlightCounts as InFlightCounts).map(([token, count]) => ({ token, count }));
    },

    pendingFlights(): InFlightCount[] {
      return this.inFlightCountsAsList.filter(({ count }) => count > 0);
    },

    noPendingFlights(): boolean {
      return this.pendingFlights.length < 1;
    },

    demoStyle() {
      return {
        top: '1rem',
        right: '1rem',
        zIndex: 99999,
        minWidth: '300px',
        overflow: 'scroll',
      };
    },
  },
});
