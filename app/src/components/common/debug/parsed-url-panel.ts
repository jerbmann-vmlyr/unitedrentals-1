import Vue from 'vue';

export default Vue.extend({
  name: 'parsedUrlPanel',
  computed: {
    queryParamString(): string {
      return this.$route.fullPath.split('?')[1] || '';
    },
    demoStyle() {
      return {
        borderRadius: '8px',
        boxShadow: 'rgba(0, 0, 0, 0.4) 0px 4px 8px',
        maxWidth: '33%',
        minWidth: '200px',
        overflow: 'scroll',
        right: '1rem',
        bottom: '2rem',
        zIndex: 99999,
      };
    },
  },
});
