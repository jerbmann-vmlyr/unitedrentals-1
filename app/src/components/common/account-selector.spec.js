import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import Vuelidate from 'vuelidate';
import { getModules, plugins } from '@/store/mocks';
import AccountSelector from '@/components/common/account-selector';
import types from '@/store/types';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Vuelidate);


// eslint-disable-next-line
xdescribe('Common Account Selector (FilterableDropdown)', () => {
  let store;
  let wrapper;
  let modules;

  beforeEach(() => {
    modules = getModules();
    store = new Vuex.Store({ modules });
    wrapper = mount(AccountSelector, {
      localVue,
      mocks: { $i18n: plugins.$i18n },
      store,
    });
  });


  it('should, when opened, show accounts sorted by account names and then ids', () => {
    // Transform the api data into a sorted list of account ids
    const sortedAccountIds = Object.values(modules.accounts.state.all)
      .sort(accountSorter)
      .map(({ id }) => id);

    // open the search results dropdown
    wrapper.find('input').trigger('click');

    // Get the list of option elements
    const optionElements = wrapper.findAll('.filterable-dropdown-results:not(.height-test) li').wrappers;

    // Expect the option elements to be in the proper sort-order.
    expect(sortedAccountIds.every((sortedId, index) => optionElements[index].text().includes(sortedId)))
      .toBeTruthy();
  });


  it('should emit the id of the selected account in a change event', () => {
    wrapper.find('input').trigger('click');

    const optionElement = wrapper.find('.filterable-dropdown-results li');
    optionElement.trigger('click');

    const emission = wrapper.emitted('change')[0][0]; // https://vue-test-utils.vuejs.org/api/wrapper/#emitted
    expect(optionElement.text().includes(emission)).toBeTruthy();
  });


  it('should update the module.user.defaultAccountId on change', () => {
    wrapper.find('input').trigger('click');
    wrapper.find('.filterable-dropdown-results li').trigger('click');

    // https://vue-test-utils.vuejs.org/guides/#mocking-actions
    expect(modules.user.actions[types.user.updateDefaultAccountId]).toHaveBeenCalled();
  });
});


// Test utils:

const accountSorter = (a, b) => {
  if (a.name < b.name) return -1;
  if (a.name > b.name) return 1;
  return a.id < b.id ? -1 : 1;
};
