import Vue from 'vue';
import { mapActions, mapGetters, mapState } from 'vuex';
import { WorkplaceRouteNames } from '@/lib/workplace';
import types from '@/store/types';
import ModalFrame from '@/components/common/ur-modal-frame.vue';
import UrErrorLabel from '@/components/common/form/ur-error-label.vue';
import UrLoadingSpinner from '@/components/common/ur-loading-spinner.vue';
import DatetimeSelect from './datetime-select.vue';
import EmailRecipients from './email-recipients.vue';
import EquipmentSelectList from './equipment-select-list.vue';
import LocationDetails from './location-details/location-details.vue';

export default Vue.extend({
  name: 'PickupRequestModal',
  components: {
    DatetimeSelect,
    EmailRecipients,
    EquipmentSelectList,
    LocationDetails,
    ModalFrame,
    UrErrorLabel,
    UrLoadingSpinner,
  },
  created() {
    this.reset();

    this.$on('initalized', () => {
      this.initalized = true;
    });
  },
  mounted() {
    const { transId, equipmentId, phone, email, accountId } = this.$route.params;
    this.initializePickupRequest({ transId, equipmentId, phone, email, accountId }).then(async () => {
      const catClasses = this.equipmentList.map(equip => equip.catClass);
      if (catClasses) {
        await this.fetchEquipment(catClasses);
      }
      this.$emit('initalized');
      return Promise.resolve();
    }).catch(e => {
      // eslint-disable-next-line no-console
      console.error(e);
      this.errorOccurred = true;
    });

  },
  data() {
    return {
      editingLocationDetails: false,
      requestSubmissionInProgress: false,
      requestFetchingInProgress: false,
      submissionError: '',
      bus: new Vue(),
      errorOccurred: false,
      initalized: false,
    };
  },
  computed: {
    ...mapState({
      equipmentList: state => (state as any).requestPickup.equipmentList,
      tcUser: state => (state as any).tcUser,
      requestData: state => (state as any).requestPickup.all,
    }),
    ...mapGetters({
      accountId: types.requestPickup.accountId,
      accountName: types.requestPickup.accountName,
      atLeastOneEquipmentIdSelected: types.requestPickup.atLeastOneEquipmentIdSelected,
      equipmentForCurrentAccount: types.workplace.equipmentForCurrentAccount,
      isPickup: types.requestPickup.isPickup,
      transId: types.requestPickup.transId,
    }),
    enableSubmitButton(): boolean {
      return !this.requestSubmissionInProgress && this.atLeastOneEquipmentIdSelected && this.requestIsInitialized;
    },
    hasSubmissionError(): boolean {
      return !!this.submissionError && this.submissionError.length > 0;
    },
    isLoading() {
      return !(this.initalized || this.$store.state.requestPickup.requestIsInitialized);
    },
  },
  methods: {
    ...mapActions({
      initializePickupRequest: types.requestPickup.initializePickupRequest,
      reset: types.requestPickup.reset,
      sendPickupRequest: types.requestPickup.submitPickupRequest,
      fetchEquipment: types.catalog.fetchEquipmentByCatClass,
    }),
    async runWrappedSubmission<T>(runner: () => Promise<T>): Promise<T | Error> {
      let submissionResult: T | Error;
      try {
        this.submissionError = '';
        this.requestSubmissionInProgress = true;
        this.requestFetchingInProgress = false;
        submissionResult = await runner();
      } catch (error) {
        if (error) {
          if (error.response && typeof error.response.status === 'number') {
            const {status, statusText} = error.response;
            this.submissionError = `${status}: ${statusText}`;
          } else {
            if (error.message) {
              try {
                this.submissionError = JSON.parse(error.message.substring(error.message.lastIndexOf('response:') + 9).trim()).message;
              } catch (e) {
                error.message.length ? this.submissionError = error.message : this.submissionError = this.$i18n('ur.formSubmissionError');
              }
            } else {
              const pos = error.lastIndexOf('response:');
              pos !== -1 ? this.submissionError = JSON.parse((error.substring(pos + 9)).trim()).message : this.submissionError = this.$i18n('ur.formSubmissionError');
            }
          }
        } else {
          this.submissionError = this.$i18n('ur.formSubmissionError');
        }
        this.requestSubmissionInProgress = false;
        return error;
      }
      this.requestFetchingInProgress = true;
      this.requestSubmissionInProgress = false;
      this.requestFetchingInProgress = false;
      // Redirect to the confirmation modal
      this.$router.replace({
        name: WorkplaceRouteNames.ConfirmPickup,
        params: {...this.$route.params},
        query: {...this.$route.query},
      });
      return submissionResult;
    },
    async onSubmitPickupClicked() {
      this.submissionError = '';
      let invalid = false;
      if (this.editingLocationDetails) {
        const saveLocationDetails = () => new Promise((resolve, reject) => {
          this.bus.$emit('saveLocationDetailsFromParent', resolve, reject);
        });
        try {
          await saveLocationDetails();
        } catch (err) {
          invalid = true;
        }
      }
      const saveEmail = () => new Promise((resolve, reject) => {
        this.bus.$emit('saveEmailFromParent', null, resolve, reject);
      });
      try {
        await saveEmail();
      } catch (err) {
        invalid = true;
      }
      if (invalid) return;
      return this.runWrappedSubmission(() => this.sendPickupRequest());
    },
  },
});
