/* eslint-disable */
function newObj(o) {
  const params = Array.prototype.slice.call(arguments, 1);

  function F() {
  }

  F.prototype = o;
  const obj = new F();
  if (params.length) {
    obj.init(...params);
  }
  return obj;
}

const UrMapMarkerProto = (function () {
  const proto = new google.maps.Marker(new google.maps.LatLng(0, 0));

  proto.init = function (data) {
    this.setPosition(new google.maps.LatLng(parseFloat(data.lat), parseFloat(data.lng)));
  };

  proto.enlarge = function() {
    // this.
  }

  proto.makeBig = function () {
    // const animationID;

    const targetSize = 1;
    const increment = .125;
    let count = 0;
    while (currentSize < targetSize) {

    }
    window.setInterval(() => {
      count = (count + increment);
      this.setSize(count);
      icons[0].offset = (count / 2) + '%';
      line.set('icons', icons);
    }, 20);
  };

  proto.makeSmall = function () {

  };

  return proto;
}());

const myMarker = newObj(UrMapMarkerProto, {
  lat: 51,
  lng: 48,
  otherData: 'some other value',
});
