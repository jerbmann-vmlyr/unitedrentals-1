# To Do
- [ ] Mobile view needs work KV-154: 14, 15; KV-155: 7, 9, 10
- [ ] Give selected state to the corresponding branch in the branch listing view KV-155: 11

# KV-148: Landing page
1. ~~The two main content areas that comprise the Location Finder are responsive:~~
  - ~~Large (Desktop): 1024px~~
  - ~~Medium (Tablet): 640px~~
  - ~~Small (Mobile): 320px~~
2. ~~On mobile views, the content areas change positions to facilitate viewing.~~
 
# KV-153: Conent Panel
1. [Functional] ~~User has page elements that allow them to search for branches by City, State/Province or Zip/Postal Code~~
2. [Functional] UR content admins can provide custom messaging within the Media Block
3. ~~User can select from a list of all 50 U.S. states, or by Canadian provinces~~
4. ~~Users can see the number of branches by state~~

# KV-154: Result Content Block
1. [Design] ~~Result blocks look like the approved creative~~
2. [Design] ~~Result panel is scrollable~~
3. ~~User can narrow the search results by selecting a branch type~~
4. [Design] ~~[Functional] System provides a Did You Mean state if there are multiple city matches~~
5. [Design] ~~[Functional] System provides an error state if the user enters a location that cannot be found~~
6. [Design] ~~The Content Panel - Fixed Footer remains in place during scrolling~~
7. [Design] ~~[Functional] ~~Once first content block is scrolled out of viewport the Scroll to Top Indicator is drawn at the top of Content Panel~~
8. [Functional] ~~Results update when the result parameters are updated.~~
  - ~~State~~
  - ~~City~~
  - ~~Zip~~
  - ~~Branch Filter~~
9. [Functional] Result parameters are updated from the following interactions: 
  - ~~Search Query input~~
  - ~~Manual query selection~~
  - ~~Branch Filter Type selection~~
  - ~~Map Pan~~
  - ~~Map Zoom~~
  - ~~Branch Selection~~
  - ~~Branch de-selection~~
  - ~~Pagination selection~~
10. [Design] [Functional] Result blocks provide:
  - [x] Branch type
  - [x] location
  - [x] distance 
  - [x] contact information
11. [Design] ~~Each result block provides a CTA that allows the user to Get Directions to that branch location~~
12. [Design] [Functional] ~~Pagination controls within the Fixed Footer (KV-148) get updated when results are updated~~
13. [Design] [Functional] ~~A visual indicator is next to each result that aligns to map pins within the Map View (KV-155)~~
14. [Design] On mobile views, the results are placed below the Map View
15. [Design] In Mobile view, user can see the results as Content Blocks or listviews
16. [Functional] ~~Info Only: Will need to account for the results updating when the Map gets panned or zoomed.~~

# KV-155: Map View
1. ~~Upon landing at the Location Finder page, the user will see a default view of the map showing CONUS view of the US without Alaska and Hawaii.~~
2. ~~The number of branches per state are overlaid on to their respective states~~
3. ~~User can pan/zoom using Google's default controls~~
4. [Functional] ~~As the user pans/zooms the map, and if they have selected the toggle to have results update when the map changes, the results will update. If the user has selected to not have the results updated, then the results will not update when the map moves.~~
5. [Functional] Map shows a minimum number of results based on branch type used in search query
  G: Last thing to do
6. [Functional] ~~Users can tap map pins to display branch details; existing branch details and active pins transitions to inactive state.~~
7. [Functional] [Mobile Only] Panning/zooming a map with pin selected will dismiss the information related to that map pin
8. ~~Users can see/select map cluster pins and the system will respond according to Google's map cluster api.~~
9. [Functional] In Mobile view, map interactions can cause transitions in the Content Block
10. [Functional] In Mobile view, as the user toggles between listview and Content Block view, the state of the Location Finder gets inherited between the two.
11. [Functional] When a map pin is selected, the corresponding branch result Block in the Content Panel is drawn as a selected state. (background changes color)
  G: confused as to when this behavior would occur
12. ~~When the user selects a branch title within the results area of Content Panel, the map will zoom in on that location.~~
13. ~~User has a toggle within a fixed area to enable a feature that updates the results when the map moves.~~

# KV-156: Branch Detail Content Block
1. [Functional] ~~Users arriving from a branch deep link URL will have a contextual link within the Branch Details Content Block that sends the user back to a search results View nearby results~~ (minimum of 10 nearby results).
  G: Minimum can wait
2. [Functional] ~~Users arriving from a Location Finder search query will have a contextual link within the Branch Details Content Block that sends the user back to the results view with the previous search parameters.~~
3. [Functional] Branch content managers will be able to update a branch-specific video, headline, and promo paragraph
  G: Moved to KV-157
4. [Design] ~~Users can see the store manager, fax, hours of operations, and a list of products/services provided by the branch.~~
5. [Functional] ~~These branch details are shown when a user taps a map pin on the map view.~~
6. [Functional] ~~If the user interacts with the search query input above the details content block, the details content block is dismissed to see the results.~~

# KV-157: Backend Coding
1. [Functional] Completed Branch content research
   - Drupal Editable Fields:
     * Main paragraph on content block (? KV-153 #2)
     * Branch specific video (KV-156 #3 If these are not already managed in the Placeable import)
     * Branch specific headline
     * Branch specific promo paragraph
     ^ check if this is returned in placeable
2. [Functional] Completed Branch content type creation
3. [Functional] Completed Branch placeable API integration
4. [Functional] Completed Branch import functionality.
5. [Functional] Completed Branch data migration

## Notes

#### Data Requirements
Branch Markers:
- Branch ID
- Latitude
- Longitude
- Business Type
- State

Branch Listings:
  ...Branch Markers
  - Phone Number
  - Address
  - Distance
G: do a data test to determine size between branch markers and branch listings minus distance.


Branch Details:
 ...Branch Listings
 ...Drupal Editable Fields
 - Hours
 - Fax
 - Manager
 - Hours
 - Products Offered
 - Holidays
 
#### Google Maps API
google.maps.Map.getBounds() to determine bounding box of map, then
google.maps.LatLngBounds.contains() to check if marker is in those bounds
 
#### Behaviors

Below is a checklist for the behaviors for the Location Finder

##### GENERAL
- User scrolls Content Panel
 - [ ] Panel scrolls
 - [ ] Content Navigation - Fixed Footer remains fixed to bottom and content scrolls underneath
 - [ ] Once first content block is scrolled out of viewport the Scroll to Top Indicator is drawn at the top of Content Panel
- User selects Scroll to Top indicator
 - [ ] The Content Panel is automatically scrolled back to top, 0 Y Position
- User selects Branch Title
 - [ ] Content Panel is redrawn with Branch Detail content
 - [ ] Map zooms according to Google Map API
 - [ ] All other results are hidden
 - [ ] Selected Branch Map Pin is drawn as Map Pin - Detail (General or Specialty, depending on branch type)
- User selects Interaction Toggle from Content Navigation - Fixed Footer
 - [ ] If inactive, Toggle becomes active
 - [ ] If active, Toggle becomes inactive
- User pans map
 - [ ] Map updates according to Google API
 - [ ] If Interaction Toggle is active, Results Update
 - [ ] Else, Results do not update
- User zooms map
 - [ ] Map updates according to Google API
 - [ ] If Interaction Toggle is active, Results Update
 - [ ] Else, Results do not update
 - [ ] User selects active Paginate Right  from Content Navigation - Fixed Footer
 - [ ] Results update to show next (up to 25) results
- User selects active Paginate Left  from Content Navigation - Fixed Footer
 - [ ] Results update to show previous (up to 25) results
- User selects Map Cluster
 - [ ] Map updates according to Google Marker Cluster API
 - [ ] Results update
- User selects Map Pin - No Results Shown (General or Specialty)
 - [ ] Map updates according to Google API to show a minimum of 10 results including the selected result
 - [ ] Map Pin is drawn as Map Pin - Active (General or Specialty, depending on branch type)
 - [ ] Content Panel scrolls until Content Block that represents selected Map Pin is fully visible within viewport
 - [ ] Content Block that represents selected Map Pin is drawn in Selected State
- User selects inactive Map Pin (General or Specialty)
 - [ ] Map Pin is drawn as Map Pin - Active (General or Specialty, depending on branch type)
 - [ ] Content Panel scrolls until Content Block that represents selected Map Pin is fully visible within viewport
 - [ ] Content Block that represents selected Map Pin is drawn in Selected State
- User selects active Map Pin (General or Specialty)
 - [ ] Content Panel is redrawn with Branch Detail content
 - [ ] Map zooms according to Google Map API
 - [ ] All other results are hidden
 - [ ] Selected Branch Map Pin is drawn as Map Pin - Detail (General or Specialty, depending on branch type)

##### SEARCH
- User selects Search Input
 - [ ] See Global Search#LOCATIONINPUT.1
- User inputs alpha-numeric characters
 - [ ] See Global Search#LOCATIONINPUT.1
- User hits "ESC"
 - [ ] Search Input is dismissed. 
 - [ ] Existing query is maintained
- User selects a results
 - [ ] Results Update
- User hits "enter"
 - [ ] Results Update
- User selects a result from Did You Mean block
 - [ ] Results Update

##### BRANCH TYPE FILTER
- User selects Branch Type Filter
 - [ ] The Branch Type Filter dropdown is drawn (native dropdown for mobile devices) containing "All Branches, General Equipment & Tools, Power & HVAC, Pump Solutions, Trench Safety, Reliable Onsite Services"
 - [ ] Dropdown is dismissed if user hits ESC or selects outside of the dropdown
- User selects a filter from the Branch Type Filter dropdown
 - [ ] The Branch Type Filter is dismissed
 - [ ] Branch Type Filter label updates to read: "Show [selected filter]"
 - [ ] Results Update

##### BRANCH DETAILS
- User selects Return to Results
 - [ ] Results Update back to previous parameters
- User selects View Nearby Results
 - [ ] Results Update to display nearby results (Minimum Results rule)
- User selects Get Directions
 - [ ] A new tab is opened with Google Maps
 - [ ] Destination address is pre-populated with Branch address
 - [ ] Starting point address is empty unless it can be populated from future stories from cookies or user account information
- User selects Contact Us
 - [ ] A new tab is opened with general contact form - NEED LINK
- User selects Rent Online
 - [ ] A new tab is opened with Equipment Landing page - NEED LINK
- User scrolls Content Panel
 - [ ] Panel scrolls
- User pans map
 - [ ] Map updates according to Google API
 - [ ] Results do not update
- User zooms map
 - [ ] Map updates according to Google API
 - [ ] Results do not update

##### SMALL VIEW
- User taps view toggle while no map pin is active
 - [ ] If map view is active, list view is drawn displaying results inherited from pins visible within map view  
 - [ ] If list view is active, map view is drawn displaying pins inherited list view
- User taps view toggle while a map pin is active
 - [ ] If map view is active
list view is drawn displaying results inherited from pins visible within map view
list view scrolls so that content block representing selected map pin scrolls into view 
content block is drawn in the selected state
 - [ ] If list view is active, map view is drawn displaying pins inherited list view and map
selected map pin is drawn in active state
- User taps inactive Map Pin (General or Specialty) with no other map pin currently selected
 - [ ] Map Pin is drawn in active state
 - [ ] All other map pins display inactive state
 - [ ] Result content block transition from bottom of viewport into view covering Content Navigation - Fixed Footer
- User taps inactive Map Pin (General or Specialty) with other map pin currently selected
 - [ ] Newly tapped Map Pin is drawn in active state
 - [ ] All other map pins display inactive state
 - [ ] Result content block is redrawn to display content for newly selected pin 
- User pans map with no map pin currently selected
 - [ ] Map pans according to Google Maps API
- User zooms map with no map pin currently selected
 - [ ] Map zooms according to Google Maps API
- User pans map while a map pin is currently selected and Content Block is displayed
 - [ ] Content Block is dismissed, transitions downward out of viewport
 - [ ] Map pans according to Google Maps API
- User zooms map while a map pin is currently selected and Content Block is displayed
 - [ ] Content Block is dismissed, transitions downward out of viewport
 - [ ] Map zooms according to Google Maps API
- User interacts with Search Input while a map pin is currently selected and Content Block is displayed
 - [ ] Content Block is dismissed, transitions downward out of viewport
 - [ ] Default search behavior is followed
- User interacts with Branch Type Filter while a map pin is currently selected and Content Block is displayed
 - [ ] Content Block is dismissed, transitions downward out of viewport
 - [ ] Default Branch Type Filter behavior is followed
- User taps Map Pin in active state
 - [ ] Branch detail is drawn
- User taps branch title in Result Content Block
 - [ ] Branch Detail is drawn
- User taps phone number in Result Content Block
 - [ ] native tel: is invoked
