# Route Handling for the Location Finder

## Paths

#### Base Path

    - `/location-finder`

#### Child paths

The following `child routes` can be appended to the base path. All but `filter` are optional, and the query can
appear after any route.

  - `/filter/:id` - required. the router can render this to a viewport passing it a prop.
  - `/state/:id`  - optional. if this is in the URL, and not city, the app enters dispatches locationFinder.getByState
  - `/city/:id`   - optional, but makes state required if it exists. The app autocompletes the city/state
  - `?branch`     - optional. Renders the branch details view

## Behavior

- `location-finder.vue` will render the client-side route `/#/location-finder/filter/all`.
  It will also dispatch a branch details action if a branch query exists in the URL.
- `branch-filter.vue` manages the `filter/:filter` route
- `branch-list.vue` manages the `?branch` query param
- Changing any part of the path removes the query (and exits the details view)

# Bugs

1.TheBug: If I view details, then back out and view Kansas City, the app still thinks I have a branch selected.
1.TheFix: model `currentBranch` and use that instead of map.active.branchId & search.branchId

General

1. Can we disable the router for some urls?  It'd be nice not to have the /#/ on every page that doesn't use routing
2. Alternatively, can we use html5 routing? (Test this in drupal and dev/stage env, don't want to break demos!)