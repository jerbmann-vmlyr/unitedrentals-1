const OFF = 0;
const WARN = 2;
const ERROR = 2;
const WARN_IF_DEV = process.env.NODE_ENV === 'development' ? WARN : ERROR;

module.exports = {
  root: true,

  env: {
    browser: true,
  },

  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript',
  ],

  plugins: [
    '@typescript-eslint', // Required to apply rules which need type information
    'vue',                // Required to lint *.vue files
  ],

  parserOptions: {
    parser: '@typescript-eslint/parser',
  },

  globals: {
    Drupal: 'readonly',
    drupalSettings: 'readonly',
    google: 'readonly',
  },

  overrides: [
    {
      files: [
        '*.spec.js',
        '*.spec.ts',
        '*.mocks.ts',
        '*.mocks.js'
      ],
      env: {
        jest: true,
      },
      rules: {
        'max-nested-callbacks': OFF,
      },
    },
    // Disable some rules for all TS files
    {
      files: ['*.ts'],
      rules: {
        'default-case': OFF,            // TS provides exhaustiveness checking
        'no-dupe-class-members': OFF,   // TS has function overloading
        'no-empty-function': OFF,       // TS has constructor member parameters
        'no-extra-boolean-cast': OFF,   // Stupid rule
        'no-useless-constructor': OFF,  // TS has constructor member parameters
        'prefer-destructuring': OFF,    // Strips out types when on
      },
    },
    // Disable some rules for all vue files
    {
      files: ['*.vue'],
      rules: {
        indent: OFF,
        '@typescript-eslint/indent': OFF,
      },
    },
    {
      files: ['*.js', '*.vue'],
      rules: {
        'max-len': OFF,
      },
    },
  ],

  rules: {
    /**
     * Built-in ESLint rules
     */
    'array-bracket-spacing': ERROR,
    'array-callback-return': OFF,
    'arrow-body-style': [ERROR, 'as-needed'],
    'arrow-parens': [ERROR, 'as-needed', {
      requireForBlockBody: false,
    }],
    'brace-style': [ERROR, 'stroustrup'],
    'camelcase': OFF,
    'class-methods-use-this': OFF,
    'comma-spacing': [ERROR, {
      before: false,
      after: true,
    }],
    'consistent-return': OFF,
    'func-names': OFF,
    'function-paren-newline': OFF,
    'guard-for-in': OFF,
    'implicit-arrow-linebreak': OFF,
    'import/newline-after-import': ERROR,
    'indent': OFF, // Must be off for @typescript-eslint/indent to work!
    'key-spacing': [ERROR, {
      beforeColon: false,
      afterColon: true,
      mode: 'strict',
    }],
    'lines-between-class-members': [ERROR, 'always', {
      exceptAfterSingleLine: true,
    }],
    'max-len': [ERROR, {
      code: 140,
      comments: 140,
      tabWidth: 2,
      ignoreStrings: true,
      ignoreTemplateLiterals: true,
      ignoreRegExpLiterals: true,
    }],
    'max-nested-callbacks': [1, 3],
    'new-cap': OFF,
    'no-await-in-loop': OFF,
    'no-bitwise': OFF,
    'no-case-declarations': OFF,
    'no-confusing-arrow': OFF,
    'no-console': [WARN_IF_DEV, {
      allow: ['warn', 'error', 'trace'],
    }],
    'no-debugger': WARN_IF_DEV,
    'no-else-return': OFF,
    'no-extra-parens': OFF, // Must be off for @typescript-eslint/no-extra-parens to work!
    'no-mixed-operators': OFF,
    'no-multi-spaces': ERROR,
    'no-multiple-empty-lines': [ERROR, {
      max: 2,
      maxEOF: 1,
    }],
    'no-nested-ternary': OFF,
    'no-param-reassign': OFF,
    'no-plusplus': OFF,
    'no-prototype-builtins': OFF,
    'no-restricted-globals': OFF,
    'no-restricted-syntax': OFF,
    'no-return-assign': OFF,
    'no-shadow': OFF,
    'no-underscore-dangle': OFF,
    'no-unreachable': WARN_IF_DEV,
    'no-unused-expressions': OFF,
    'no-unused-vars': OFF, // Must be off for @typescript-eslint/no-unused-vars to work!
    'no-use-before-define': OFF,
    'no-useless-computed-key': OFF,
    'no-useless-escape': OFF,
    'no-useless-return': OFF,
    'object-curly-newline': [ERROR, {
      consistent: true,
    }],
    'object-curly-spacing': OFF,
    'object-shorthand': [ERROR, 'always'],
    'operator-assignment': [ERROR, 'always'],
    'operator-linebreak': [ERROR, 'before', {
      overrides: { '=': 'ignore' },
    }],
    'padded-blocks': OFF,
    'prefer-destructuring': ERROR,
    'prefer-promise-reject-errors': OFF,
    'prefer-template': ERROR,
    'quotes': [ERROR, 'single', {
      allowTemplateLiterals: true,
    }],
    'space-before-function-paren': [ERROR, {
      anonymous: 'always',
      named: 'never',
      asyncArrow: 'always',
    }],
    'template-curly-spacing': [ERROR, 'never'],

    /**
     * ESLint import plugin rules
     */
    'import/extensions': OFF,
    'import/first': OFF,
    'import/no-cycle': OFF, /* NOTE: We'd be wise to turn this on at some point */
    'import/no-named-as-default-member': OFF,
    'import/no-named-as-default': OFF,
    'import/no-unresolved': OFF,
    'import/order': OFF,
    'import/prefer-default-export': OFF,

    /**
     * ESLint Vue plugin rules
     */
    'vue/no-side-effects-in-computed-properties': 0,
    'vue/return-in-computed-property': 0,
    'vue/script-indent': [ERROR, 2, {
      baseIndent: 1,
    }],

    /**
     * Typescript/ESLint plugin rules
     */
    '@typescript-eslint/array-type': OFF,
    '@typescript-eslint/camelcase': [ERROR, {
      ignoreDestructuring: true,
      properties: 'never',
    }],
    '@typescript-eslint/class-name-casing': ERROR,
    '@typescript-eslint/explicit-function-return-type': OFF,
    '@typescript-eslint/explicit-member-accessibility': [ERROR, {
      accessibility: 'no-public',
    }],
    '@typescript-eslint/indent': [ERROR, 2, {
      SwitchCase: 1,
      VariableDeclarator: 0,
      outerIIFEBody: 1,
      MemberExpression: 1,
      FunctionDeclaration: {
        parameters: 1,
        body: 1,
      },
      FunctionExpression: {
        parameters: 1,
        body: 1,
      },
      CallExpression: {
        arguments: 1,
      },
      ArrayExpression: 1,
      ObjectExpression: 1,
      ImportDeclaration: 1,
      flatTernaryExpressions: false,
      ignoreComments: false
    }],
    '@typescript-eslint/member-delimiter-style': OFF,
    '@typescript-eslint/no-explicit-any': OFF,
    '@typescript-eslint/no-extra-parens': OFF,
    '@typescript-eslint/no-non-null-assertion': OFF,
    '@typescript-eslint/no-object-literal-type-assertion': OFF,
    '@typescript-eslint/no-parameter-properties': OFF,
    '@typescript-eslint/no-unused-vars': [WARN_IF_DEV, {
      args: 'none',
    }],
    '@typescript-eslint/no-use-before-define': OFF,
    '@typescript-eslint/prefer-includes': OFF,
    '@typescript-eslint/prefer-interface': OFF,
  },

};
