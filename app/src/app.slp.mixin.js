/*
 This mixin handles SLP-specific
 functionality (for any/all SLP pages).

 Currently it only applies some stickyness to the Hero Lead Gen
 component.
 */

export default {
  data() {
    return {
      sticky: false,
      leadGen: null,
    };
  },
  methods: {
    handleScroll() {
      const bounds = this.leadGen.getBoundingClientRect();
      this.sticky = bounds.bottom <= 0;
    },
  },
  mounted() {
    this.leadGen = document.getElementById('heroLeadGen');

    if (!this.leadGen) {
      return;
    }

    window.addEventListener('scroll', this.handleScroll);
  },
};
