/**
 *
 * This module converts a single call to geolocation#getCurrentPosition into a promise.
 *
 * Example:
 *
 * var geolocation = require('geolocation');
 *
 * geolocation.then(function(position){
 *   console.log( position.coords ); // -> { latitude: 123.456, longitude: 987.654 }
 * }).catch(function(error){
 *   console.log( error.message ); // -> "Geolocation failed because reasons!"
 * });
 *
 */
const isIe11 = navigator.userAgent.toLowerCase().indexOf('trident/7.0') !== -1;
const options = {
  timeout: 15000,
  enableHighAccuracy: true,
};

export default () => new Promise<Position>((resolve, reject) => {
  return isIe11
    ? reject()
    : navigator.geolocation.getCurrentPosition(resolve, reject, options);
});
