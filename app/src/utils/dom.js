/* eslint-disable no-console */
/* eslint-disable dot-notation */
import { camelCase } from 'lodash';

/* eslint-disable no-return-assign */
const Dom = {
  appendToBody: el => document.body.appendChild(el),
  addBodyClass: className => document.body.classList.add(className),
  removeBodyClass: className => document.body.classList.remove(className),
  setBodyStyle: (name, value) => (document.body.style[name] = value),
  attributesObject: el => {
    const data = {};
    for (const attr of el.attributes) {
      const name = camelCase(attr.name);
      try {
        data[name] = JSON.parse(attr.value);
      }
      catch (err) {
        data[name] = attr.value;
      }
    }
    return data;
  },
};

export function createObservedContent(path, tag = 'span') {
  return {
    template: `<${tag} v-html="contentValue"></${tag}>`,
    data() {
      return { contentValue: '', contentPath: `$root.${path}` };
    },
    mounted() {
      this.$watch(this.contentPath, {immediate: true,
        handler: newVal => {
          this.contentValue = newVal;
        }});
    },
  };
}

export function enhanceDomElements(parentVm) {
  /**
   * Searches through dom, adding vue-rendered components onto
   * HTML elements without the fuss of a full app
   */
  // Everything we make gets added here
  console.log('enhancing');
  const instances = [];
  document.querySelectorAll('[data-vue]').forEach(el => {

    // Get the component being referenced
    let componentOpts;
    const data = Dom.attributesObject(el);
    if (data['dataContentPath']) {
      componentOpts = createObservedContent(data['dataContentPath']);
      delete data['dataContentPath'];
    }

    if (data['dataComponentName']) {
      // Try to fetch from given VM first, the look in components
      const cname = data['dataComponentName'];
      if (Object.keys(parentVm.$options.components).includes(cname)) {
        componentOpts = parentVm.$options.components[cname];
      }
      else if (Object.keys(components).includes(cname)) {
        componentOpts = components[cname];
      }
    }

    // If we found something, render and insert into DOM
    if (componentOpts) {
      try {
        const Constructor = Vue.extend(componentOpts);
        const instance = new Constructor({
          propsData: data,
          parent: parentVm, // Make it as a subcomponent of app
        });
        instances.push(instance);

        // Render it and append to current element
        instance.$mount();
        el.appendChild(instance.$el);
      }
      catch (err) {
        console.error(`Unable to mount component: ${el}`);
        console.error(err);
      }
    }
  });

  return instances;
}

export default Dom;
