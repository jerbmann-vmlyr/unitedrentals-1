import moment from 'moment';

export default {
  daysOfWeek: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  nextWeekday() {
    if (this.isWeekday() && this.isNot('Friday')) {
      return 1;
    }
    else if (this.day('Friday')) {
      return 3;
    }
    else if (this.day('Saturday')) {
      return 2;
    }
    else if (this.day('Sunday')) {
      return 1;
    }
  },
  nextDay() {
    return 1;
  },
  isNot(day) {
    return (this.name(day) === false);
  },
  moment() {
    return moment();
  },
  month() {
    return moment().month();
  },
  year() {
    return moment().year();
  },
  name(name = false) {
    if (!name) {
      return this.daysOfWeek[this.key()];
    }

    return (this.daysOfWeek[this.key()] === name);
  },
  dayOfMonth() {
    return moment().date();
  },
  key() {
    return moment().day();
  },
  isWeekend() {
    return ['Sunday', 'Saturday'].includes(this.name());
  },
  isWeekday() {
    return ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'].includes(this.name());
  },
};
