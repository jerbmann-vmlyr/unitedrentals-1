import * as moment from 'moment';

interface ObjectWithLatLng {
  lat: number;
  lng: number;
  [prop: string]: any;
}

export default {

  /**
   * Distance function
   *
   * Returns the distance between to coords in miles
   *
   * @param {object} start | starting point in form {lat, lng}
   * @param {object} end | end point in form {lat, lng}
   * @return {number} the distance between the points in miles
   */
  distance(start: ObjectWithLatLng, end: ObjectWithLatLng) {
    const { sin, cos, atan2, sqrt, PI } = Math;
    const R = 6371e3; // earth's radius, in meters
    const lat1 = start.lat * (PI / 180);
    const lat2 = end.lat * (PI / 180);
    const difLat = (start.lat - end.lat) * (PI / 180);
    const difLng = (start.lng - end.lng) * (PI / 180);

    const a = (sin(difLat / 2) * sin(difLat / 2)) + (cos(lat1) * cos(lat2) * sin(difLng / 2) * sin(difLng / 2));
    const c = 2 * atan2(sqrt(a), sqrt(1 - a));

    return (R * c) * 0.000621371; // meter -> mile
  },

  /**
   * Timer Code for Testing Speed of Things
   *
   * let start = new Date().getTime();
   * ...
   * let end = new Date().getTime();
   * let dif = end - start;
   */

  /**
   * Converts Degrees to Radians
   * @param {number} angle in Degrees
   * @return {number} angle in Radians
   */
  toRadians(angle: number) {
    return angle * (Math.PI / 180);
  },

  /**
   * Converts Radians to Degrees
   * @param {number} angle in Radians
   * @return {number} angle in Degrees
   */
  toDegrees(angle: number) {
    return angle * (180 / Math.PI);
  },

  /**
   * Marker Plotter
   *
   * Given a starting point and markers, this will calculate evenly spaced markers
   * around the starting point with increasing distance.
   *
   * @param {*} point { lat, lng }
   * @param {array} markers object
   * @param {number} distance - base distance in meters to space the markers (increases with more markers)
   * @return {array} of markers
   */
  markerPlotter<T extends google.maps.MarkerOptions>(
    point: ObjectWithLatLng,
    markers: T[],
    distance: number,
  ): T[] {
    const spacing = (markers.length - 1) * distance; // distance per marker starting at 2 markers
    const mds = 360 / markers.length; // marker degree spacing = 360 / count;
    const sd = mds / 2; // (offset) starting degree = marker degree spacing / 2;

    for (let i = 0; i < markers.length; i++) {
      const bearing = (mds * i) + sd; // bearing = marker degree spacing * i + starting degree
      markers[i].position = this.destinationPoint(point, spacing, bearing);
    }
    return markers;
  },

  /**
   * Destination Point
   *
   * http://www.movable-type.co.uk/scripts/latlong.html
   *
   * @param {object} start - starting point
   * @param {String} distance - Distance travelled, in same units as earth radius (default: metres).
   * @param {String} bearing - bearing info
   * @return {object} {lat, lng} latitude, longitude
   */
  destinationPoint(
    start: ObjectWithLatLng,
    distance: number,
    bearing: ObjectWithLatLng,
  ): ObjectWithLatLng {
    const radius = 6371e3;

    // sinÏ†2 = sinÏ†1â‹…cosÎ´ + cosÏ†1â‹…sinÎ´â‹…cosÎ¸
    // tanÎ”Î» = sinÎ¸â‹…sinÎ´â‹…cosÏ†1 / cosÎ´âˆ’sinÏ†1â‹…sinÏ†2
    // see mathforum.org/library/drmath/view/52049.html for derivation

    const delta = Number(distance) / radius; // angular distance in radians
    const theta = this.toRadians(Number(bearing));

    const phi1 = this.toRadians(start.lat);
    const lambda1 = this.toRadians(start.lng);

    const sinPhi1 = Math.sin(phi1);
    const cosPhi1 = Math.cos(phi1);
    const sinDelta = Math.sin(delta);
    const cosDelta = Math.cos(delta);
    const sinTheta = Math.sin(theta);
    const cosTheta = Math.cos(theta);

    const sinPhi2 = (sinPhi1 * cosDelta) + (cosPhi1 * sinDelta * cosTheta);
    const phi2 = Math.asin(sinPhi2);
    const y = sinTheta * sinDelta * cosPhi1;
    const x = cosDelta - (sinPhi1 * sinPhi2);
    const lambda2 = lambda1 + Math.atan2(y, x);

    return {
      lat: this.toDegrees(phi2),
      lng: ((this.toDegrees(lambda2) + 540) % 360) - 180,
    }; // normalise to âˆ’180..+180Â°
  },

  farthestDateFromDates(farthestDate: moment.Moment, [date, ...dates]: moment.Moment[]): moment.Moment {
    if (date && date.isAfter(farthestDate)) {
      farthestDate = date;
    }

    return dates.length
      ? this.farthestDateFromDates(farthestDate, dates)
      : farthestDate;
  },
};
