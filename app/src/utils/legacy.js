
export function mobileMenuIsOpen() {
  // Some components directly check for this element as a way
  // of detecting mobile browser....
  const menu = document.querySelector('.mobile-menu__open');
  return menu && menu.offsetParent !== null;
}

export function parseDrupalSettings() {
  const settingsElement = document.querySelector('script[type="application/json"][data-drupal-selector="drupal-settings-json"]');
  return settingsElement && JSON.parse(settingsElement.textContent);
}

export default {
  mobileMenuIsOpen,
  parseDrupalSettings,
};
