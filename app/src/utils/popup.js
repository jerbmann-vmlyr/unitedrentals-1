class Popup {
  constructor(Vue) {
    this.Vue = Vue;
    this.popup = {
      data() {
        return {
          promiseResolver: {},
          promiseRejecter: {},
          defaultScroll: '',
        };
      },
      template: '<component :is="component" @close="close" @cancel="cancel" @resolve="resolve"></component>',
      props: {
        component: Object,
      },
      methods: {
        cancel() {
          this.promiseRejecter();
        },
        close() {
          setTimeout(() => {
            this.$destroy();
          }, 150);
        },
        resolve(data) {
          this.promiseResolver(data);
          this.close();
        },
      },
      name: 'popup',
      beforeDestroy() {
        document.body.style.overflow = this.defaultScroll;
        document.body.style.width = 'auto';
        // Setting to static as IE has no concept of "initial".
        document.body.style.position = 'static';
        this.$el.parentNode.removeChild(this.$el);
      },
      beforeMount() {
        this.$parent.$el.appendChild(this.$el);
        this.defaultScroll = document.body.style.overflow;
        document.body.style.overflow = 'hidden';
        // Explicitly setting width: 100% due to position fixed on a flex element.
        document.body.style.width = '100%';
        document.body.style.position = 'fixed';
      },
    };
  }

  open(params, self) {
    const propsData = Object.assign({}, params);
    const PopupComponent = this.Vue.extend(this.popup);
    const comp = new PopupComponent({
      parent: self,
      el: document.createElement('div'),
      propsData,
    });

    return new Promise((resolve, reject) => {
      comp.promiseResolver = resolve;
      comp.promiseRejecter = reject;
    });
  }
}

function plugin(Vue) {
  const popup = new Popup(Vue);
  Object.defineProperties(Vue.prototype, {
    $popup: {
      get() {
        return popup;
      },
    },
  });
}

plugin.version = '__VERSION__';

export default plugin;

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(plugin);
}
