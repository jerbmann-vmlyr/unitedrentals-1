import Vue from 'vue';
import { get, transform, isEqual, isObject } from 'lodash';

/**
 * Make an object reactive.
 *
 * Copied from Vue 2.6, which we are not yet on:
 * https://vuejs.org/v2/api/#Vue-observable
 *
 * @param {Object} data
 */
export function observable(data) {
  if (Vue.observable) {
    return Vue.observable(data);
  }
  return new Vue({
    data() {
      return data;
    },
  }).$data;
}

/**
 * Deep diff between two object, using lodash
 * @param  {Object} object Object compared
 * @param  {Object} base   Object to compare with
 * @return {Object}        Return a new object who represent the diff
 */
export function difference(object, base) {
	return transform(object, (result, value, key) => {
		if (!isEqual(value, base[key])) {
			result[key] = isObject(value) && isObject(base[key]) ? difference(value, base[key]) : value;
		}
	});
}

/**
 * Normalize the map
 * normalizeMap([1, 2, 3]) => [ { key: 1, val: 1 }, { key: 2, val: 2 }, { key: 3, val: 3 } ]
 * normalizeMap({a: 1, b: 2, c: 3}) => [ { key: 'a', val: 1 }, { key: 'b', val: 2 }, { key: 'c', val: 3 } ]
 *
 * Copied from Vuex:
 * https://github.com/vuejs/vuex/blob/91f3e69ed9e290cf91f8885c6d5ae2c97fa7ab81/src/helpers.js#L131
 *
 * @param {Array|Object} map
 * @return {Object}
 */
export function normalizeMap(map) {
  return Array.isArray(map)
    ? map.map(key => ({ key, val: key }))
    : Object.keys(map).map(key => ({ key, val: map[key] }));
}

export default {
  observable,
  normalizeMap,
  difference
};
