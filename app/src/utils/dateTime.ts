import moment from 'moment';
import dateFormats from './dateFormat';

export interface TimeOption {
  value: string;
  text: string;
  status: boolean;
}

export default class DateTime {
  // Create an array of time options
  static units(total: number, increment: number) {
    const slots: string[] = [];
    for (let i = 0; i < (total * increment); i += increment) {
      const time = i / 60;
      const timeHours = Math.floor(time);
      const timeMinutes = (60 * (time - timeHours)) * 1;
      const startTimeHours = DateTime.zeroPad(timeHours);
      const startTimeMinutes = DateTime.zeroPad(timeMinutes);
      slots.push(`${startTimeHours}:${startTimeMinutes}`);
    }
    return slots;
  }

  static unitsAmPm(total: number, increment: number) {
    const rawUnits = DateTime.units(total, increment);
    const slots = {};
    rawUnits.forEach(ea => {
      const tempMoment = moment();
      const [startTimeHours, startTimeMinutes] = ea.split(':');
      const adjMoment = tempMoment.set({
        hour: +startTimeHours,
        minute: +startTimeMinutes,
        second: 0,
      });
      slots[`${startTimeHours}${startTimeMinutes}`] = adjMoment.format('h:mma');
    });
    return slots;
  }

  static chunkify(
    start: moment.Moment,
    end: moment.Moment,
    chunk: moment.Duration = moment.duration(30, 'minutes'),
  ): moment.Moment[] {
    const next = start.clone().add(chunk);
    return next.isBefore(end)
      ? [start.clone(), ...DateTime.chunkify(next, end, chunk)]
      : [start.clone()];
  }

  static splitWorkingHours(amount: number, unit: moment.unitOfTime.Base, start: number, length: number) {
    const msPerDuration = length * 60 * 60 * 1000;
    const minutes = start % 1 !== 0 ? Math.floor(Math.abs((start % 1) * 60)) : 0;
    const init = moment().hour(start).minute(minutes).seconds(0);
    const dates: TimeOption[] = [];

    let nextDate = init.clone();
    while (nextDate.diff(init) < msPerDuration) {
      const startTime = nextDate.format('HH:mm') === '00:00' ? '00:01' : nextDate.format('HH:mm');
      dates.push({
        value: startTime,
        text: `${nextDate.format('h:mma')}`,
        status: true,
      });
      nextDate = nextDate.clone().add(amount, unit);
    }

    return dates;
  }

  // Round the date / time to a specific duration
  static round(
    date: moment.Moment,
    duration: moment.Duration,
    method: 'floor' | 'round' | 'ceil' = 'ceil',
  ) {
    return moment(Math[method]((+date) / (+duration)) * (+duration));
  }

  // Format a moment.js date/time using common outputs
  static format = (date: moment.Moment, name: keyof typeof dateFormats) => date.format(dateFormats[name]);

  // Convert a date from hours (07:00) to decimal (7.5)
  static toDecimal(time: string) {
    const temp = time.split(/[.:]/);
    const hour = parseInt(temp[0], 10);
    const minute = temp[1] ? parseInt(temp[1], 10) : 0;
    const out = hour + (minute / 60);
    return out;
  }

  static zeroPad(value: string | number = 0, zeroes = 2) {
    return `000000${value}`.slice(zeroes);
  }
}
