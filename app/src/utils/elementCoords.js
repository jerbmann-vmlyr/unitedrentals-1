/**
 * Element coordinates on the screen function
 *
 * Returns the bottom and left coordinates of {element} on the screen
 *
 * @param {object} element | html element
 * @return {object} The bottom and left coordinates of {element} on the screen
 */
export default (element) => {
  const box = element.getBoundingClientRect();

  const { body } = document;
  const docEl = document.documentElement;

  const scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
  const scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

  const clientTop = docEl.clientTop || body.clientTop || 0;
  const clientLeft = docEl.clientLeft || body.clientLeft || 0;

  const bottom = (box.bottom + scrollTop) - clientTop;
  const left = (box.left + scrollLeft) - clientLeft;

  return { bottom: Math.round(bottom), left: Math.round(left) };
};
