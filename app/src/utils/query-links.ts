import { get, set, omit, has, cloneDeep } from 'lodash';
import { Dictionary } from 'vue-router/types/router';

export type Query = Dictionary<any>;

export enum QueryLinkMode {
  NoValue = 'valueless',
  SingleValue = 'singlevalue',
  MultiValue = 'multivalue',
}

export class QueryLinkUtils {
  static extractCurrentValue(query: Query, param: string) {
    return get(query, param);
  }

  static validateMode(mode): mode is QueryLinkMode {
    return Object.values(QueryLinkMode).includes(mode);
  }

  static paramIsActive(
    mode: QueryLinkMode,
    query: Query,
    param: string,
    value: any,
  ) {
    const currentParamValue = QueryLinkUtils.extractCurrentValue(query, param);
    switch (mode as QueryLinkMode) {
      case QueryLinkMode.NoValue:
        return has(query, param);
      case QueryLinkMode.SingleValue:
        return currentParamValue === value;
      case QueryLinkMode.MultiValue:
        return Array.isArray(currentParamValue) && currentParamValue.includes(value);
    }
  }

  static createToggledQuery(
    mode: QueryLinkMode,
    query: Query,
    param: string,
    value: any,
  ): Query {
    const currentQuery = cloneDeep(query || {});
    const isActive = QueryLinkUtils.paramIsActive(mode, query, param, value);

    switch (mode) {
      case QueryLinkMode.NoValue: {
        return isActive
          ? omit({ ...currentQuery }, param)
          : set({ ...currentQuery }, param, '');
      }
      case QueryLinkMode.SingleValue: {
        const newValue: any = value;

        return set({ ...currentQuery }, param, newValue);
      }
      case QueryLinkMode.MultiValue: {
        const currentParamValue = QueryLinkUtils.extractCurrentValue(query, param);

        const newValue = isActive
          ? [...(currentParamValue || [])].filter(v => v !== value)
          : [...(currentParamValue || []), value];

        return set({ ...currentQuery }, param, newValue);
      }
    }
  }

  static eventIsSafe(e: MouseEvent) {
    // don't redirect with control keys
    if (e.metaKey || e.altKey || e.ctrlKey || e.shiftKey) return;
    // don't redirect when preventDefault called
    if (e.defaultPrevented) return;
    // don't redirect on right click
    if (e.button !== undefined && e.button !== 0) return;
    // don't redirect if `target="_blank"`
    const currentTarget = e.currentTarget as any;
    if (currentTarget && typeof currentTarget.getAttribute === 'function') {
      const target = currentTarget.getAttribute('target');
      if (/\b_blank\b/i.test(target)) return;
    }
    // this may be a Weex event which doesn't have this method
    if (e.preventDefault) {
      e.preventDefault();
    }
    return true;
  }
}
