// Given a string, returns a promise that resolves to a place
function getPlaceFromAddress(input: string): Promise<google.maps.places.PlaceResult> {
  input = sanitizeInput(input);

  const autocompleteService = new google.maps.places.AutocompleteService();
  const placesService = new google.maps.places.PlacesService(document.createElement('div'));

  return new Promise(((resolve, reject) => {
    autocompleteService.getQueryPredictions({ input }, (predictions, status) => {
      if (status !== google.maps.places.PlacesServiceStatus.OK || !predictions) {
        reject(new Error(`Failed to get place for: ${input}`));
        return;
      }
      const placeId = predictions[0].place_id;
      placesService.getDetails({ placeId }, (place, status) => {
        if (status !== google.maps.places.PlacesServiceStatus.OK) {
          reject(new Error(`Failed to get place for: ${input}`));
          return;
        }
        resolve(place);
      });
    });
  }));
}

// Google dies if we send US 5+4 zips to its place service. This guards against that.
function sanitizeInput(input: string): string {
  const hasUsZipPlusFour = /(\d{5}-\d{4})/.test(input);

  if (hasUsZipPlusFour) {
    input = input.replace(/(\d{5}-\d{4})/, match => match.substring(0, 5));
  }

  // strip out "undefined"
  input = input.replace(/undefined/g, '').trim();

  return input;
}

export default {
  getPlaceFromAddress,
};
