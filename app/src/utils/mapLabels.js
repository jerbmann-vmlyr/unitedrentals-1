import { dictionary } from '@/data/i18n';
import { get } from 'lodash';

const Drupal = typeof window.Drupal !== 'undefined' ? window.Drupal : { t: str => str };

/**
 * Given an object, returns a new object that has mapped Drupal.t to its values.
 *
 *  Example:
 *
 *  // in some-component.vue
 *  <template>
 *    <p>{{ exampleText }}</p>
 *  </template>
 *  <script>
 *  import mapLabels from '@/utils/mapLabels';
 *  export default {
 *    data() {
 *      return {
 *        ...mapLabels({
 *          exampleText: 'just.another.example' // points to a field in the i18n.js dictionary object.
 *        })
 *      };
 *    }
 *  }
 *  </script>
 *
 * @param {object} options Required - provides a string reference to a property in the dictionary.
 *  Keys on the options object are arbitrary, and should be contextualized for the Vue.js Template.
 *  Values on the options object should be a string reference that maps to a property of UR's i18n
 *  dictionary (@/data/i18n.js).
 * @param {object} d Optional - currently only used by our test suite
 * @param {function} t Optional - override the Drupal.t method. Should only used by the test suite
 * @return {object} A new object identical to the one passed in, but with its values mapped to the dictionary
 *  and processed by Drupal's "t" function
 */
const mapLabels = (options, d = dictionary, t = Drupal.t) => {
  const reducer = (accumulated, key) => {
    const str = get(d, options[key]);
    if (!str) {
      console.warn(`Could not find ${options[key]} in the dictionary. Typo?`); // eslint-disable-line no-console
    }
    accumulated[key] = t(str || `UNDEFINED LABEL: ${options[key]}`);

    return accumulated;
  };

  return Object.keys(options).reduce(reducer, {});
};

export default mapLabels;

/**
 * Similar to mapLabels (see above), but allows you to pass in placeholders.
 * Use this when you want to insert observables into your translation string.
 *
 * It uses Drupal.t's '@placeholder' syntax.
 *
 * Example:
 *
 * // in i18n.js
 * _example.computed = "Starting at @cost!!!!"
 *
 * // in component options
 * computed: {
 *   proBoxCost() {
 *     return '$9,999'; // just another observable in Vue...
 *   },
 *   i18n() {
 *     return mapComputedLabels({
 *       costLabel: ['_example.computed', {'@cost': this.proBoxCost}]
 *     });
 *   }
 * }
 *
 * // in component template
 * <span>{{ costLabel }}</span>  // renders to "Starting at $9,999!!!!"
 *
 * @param {object} options Required - provides a string reference to a property in the dictionary.
 *  and a list of placeholder replacements.
 *  E.g.: {myKey: ['cart.proboxCost', {'@cost': '$1,300'}]
 * @param {object} d Optional - currently only used by our test suite
 * @param {function} t Optional - override the Drupal.t method. Should only used by the test suite
 * @return {object} A new object identical to the one passed in, but with its values mapped to the dictionary
 *  and processed by Drupal's "t" function
 */
export const mapComputedLabels = (options, d = dictionary, t = Drupal.t) => {
  const reducer = (accumulated, key) => {
    const str = get(d, options[key][0]);
    const strArgs = options[key][1];

    if (!str) {
      console.warn(`Could not find ${options[key][0]} in the dictionary. Typo?`); // eslint-disable-line no-console
    }
    accumulated[key] = t(str || `UNDEFINED LABEL: ${options[key][0]}`, strArgs);

    return accumulated;
  };

  return Object.keys(options).reduce(reducer, {});
};


/**
 * Returns an object in the shape of Vue Component props. For usage in Functional Components. (Because
 * in Functional Components, we do not have access to the VM nor our standard $i18n plugin.)
 *
 * Usage:
 *
 * // in component options
 * export default {
 *   functional: true,
 *   props: {
 *     ...mapFnComponentLabels({
 *       title: 'addToCart.title,
 *     }),
 *   },
 * };
 *
 *
 * // the code above is identical to this:
 * export default {
 *   props: {
 *     title: {
 *       type: String,
 *       default: 'the value of addToCart.title as processed by our $i18n plugin'
 *       required: false,
 *     },
 *   },
 * };
 *
 * @param {object} options Required - the same options object for `mapLabels`. (See above.)
 * @return {object} a verbose Vue Props Object with `type`, `default`, and `required` key/values. Spread it into a component's props.
 */
export function mapFnComponentLabels(options) {
  const labels = mapLabels(options);

  return Object.entries(labels).reduce((props, [key, value]) => {
    props[key] = {
      type: String,
      default: value,
      required: false,
    };

    return props;
  }, {});
}
