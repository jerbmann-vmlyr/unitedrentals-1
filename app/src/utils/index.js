import autocomplete from './autocomplete';
import calculator from './calculator';
import dateFormat from './dateFormat';
import dateTime from './dateTime';
import legacy from './legacy';
import today from './today';
import geolocation from './geolocation';
import mapLabels, { mapComputedLabels, mapFnComponentLabels } from './mapLabels';
import parseMask from './parseMask';
import elementCoords from './elementCoords';
import uid from './uid';
import url from './url';
import parseToUrl from './parseToUrl';
import objects from './objects';

export {
  today,
  autocomplete,
  calculator,
  dateFormat,
  dateTime,
  elementCoords,
  legacy,
  geolocation,
  mapComputedLabels,
  mapFnComponentLabels,
  mapLabels,
  parseMask,
  uid,
  url,
  parseToUrl,
  objects,
};

export default {
  today,
  autocomplete,
  calculator,
  dateFormat,
  dateTime,
  elementCoords,
  legacy,
  geolocation,
  mapComputedLabels,
  mapFnComponentLabels,
  mapLabels,
  parseMask,
  uid,
  url,
  parseToUrl,
  objects,
};
