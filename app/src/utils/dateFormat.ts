export default {
  friendly: 'MMM D, YYYY',
  invoice: 'MMM Do, YYYY',
  iso: 'YYYY-MM-DD\THH:mm:ss',
  rentalTimeline: 'MM/DD/YY',
  shortTime: 'h:mma',
  shortSlash: 'M/D/YY',
  slash: 'MM/DD/YYYY',
  workplaceGps: 'YYYYMMDD',
};
