import { forOwn } from 'lodash';

/**
 * Utility method to parse an object of values into url encoded string
 *
 * @param {object} the object to parse
 * @param {string} the optional param prefix, defaults to '?'
 * @return string the url encoded result
 */

export default (obj, prefix = '?') => {
  let str = prefix;
  forOwn(obj, (value, key) => {
    if (value) {
      str += `${key}=${value}&`;
    }
  });
  str = str.replace(/&$/, '');
  return str;
};
