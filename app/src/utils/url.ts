/**
 * The pathname, split by `/`s
 * @type {string[]}
 * Example: Given localhost:8080/equipment/aerial-platforms/boom-lifts/
 * pathParams is['equipment', 'aerial-platforms', 'boom-lifts']
 */
const pathParams = window.location.pathname
  .split('/')
  .slice(1)
  .map(param => param.replace(/#(.*)/g, ''));


/**
 * Returns first path param after `/equipment`
 * E.g. given a url /equipment/boomlifts/aerial-boomlifts, this returns 'boomlifts'.
 * @return {string} path param
 */
function getCategory() {
  if (getPathParamByDepth(0) === 'pro-box') {
    return 'tools---power,-hand-&-surveying';
  }
  return getPathParamByDepth(2);
}

/**
 * Returns second path param after `/equipment`
 * E.g. given a url /equipment/boomlifts/aerial-boomlifts, this returns 'aerial-boomlifts'.
 * @return {string} path param
 */
function getSubcategory() {
  return getPathParamByDepth(3);
}

/**
 * General utility that returns a part of the pathname that has been split by `/`s
 * @param {number} depth integer
 * @return {string} path param
 */
function getPathParamByDepth(depth: number) {
  return pathParams[depth];
}

function removeQuery() {
  window.location.search = '';
}

function reload() {
  window.location.reload();
}

/**
 * This is a hack to support a Drupal page that won't render a drupalSettings.vueRouter variable.
 * Ideally this FN dies a quick death and Drupal renders its setting, and
 * main.js's routing condition is cleaned up.
 *
 * Note: currently, object-listing items link to details pages @ /node/xx.  Ideally this changes
 * but for now a document.querySelector check is required.
 *
 * @return {boolean} truthiness of being on an equipment details page
 */
function isEquipmentDetailsPage() {
  return (pathParams[1] === 'equipment' && typeof pathParams[4] !== 'undefined') || document.querySelector('.object-detail');
}

function isAnyEquipmentPage() {
  return pathParams[1] === 'equipment';
}

function isAnySolutionsPage() {
  return pathParams[1] === 'solutions';
}

function urlify(str: string) {
  return str.toLowerCase().replace(/[&\\#/,+()$~%.'":*?<>{}]/g, '').replace(' ', '-');
}


export default {
  getCategory,
  getPathParamByDepth,
  getSubcategory,
  isEquipmentDetailsPage,
  isAnyEquipmentPage,
  isAnySolutionsPage,
  removeQuery,
  reload,
  urlify,
};
