/**
 * Transforms a simple mask into the mask our plugin needs.
 *
 * In a "simple mask", "a" stands for alpha, "0" stands for numeric. ANything else is a literal.
 *
 * E.G.
 *
 * <masked-input
 *   :mask="parseMask('(999) 999-9999')
 * >
 *
 * This function will transform that simple mask into something
 * the ":mask" directive understands:
 *
 * [ '(', /[1-9]/, /[1-9]/, /[1-9]/, ')', ' ', /[1-9]/, /[1-9]/, /[1-9]/, '-', /[1-9]/, /[1-9]/, /[1-9]/, /[1-9]/ ]
 *
 *
 * There are known limitations!!! (ie, passing in a simple mask with a `\`.)
 * But this should work for most if not all of our use cases (telephone numbers, credit card numbers, ?)
 *
 *
 * Mask Plugin:
 * https://text-mask.github.io/text-mask/
 *
 * @param simpleMask
 */

export default (simpleMask) => {
  if (!simpleMask.length) {
    console.warn('Warning! parseMask was invoked without a simple mask!');// eslint-disable-line
    return [];
  }

  return simpleMask.split('')
    .map((character) => {
      switch (character) {
        case 'a':
        case 'A':
          return /[a-zA-Z]/;
        case '0':
        case '9':
          return /[0-9]/;
        default:
          return character;
      }
    });
};
