import Vue from 'vue';
import VueRouter from 'vue-router';
import get from 'lodash/get';

import routes from '@/routes';
import store, { enableAsyncStorage } from '@/store';
import { protectedRoutes, protectedNamespaces } from '@/data/route-guards';

import Qs from 'qs';
// import { routes as pageRoutes } from '@/pages';

import { sync } from 'vuex-router-sync';

Vue.use(VueRouter);

export const router = new VueRouter({
  base: process.env.BASE_URL,
  routes, // [...routes, ...pageRoutes],
  mode: 'hash',
  parseQuery: Qs.parse,
  stringifyQuery(query) {
    return Qs.stringify(query, { addQueryPrefix: true });
  },
  // Simulate native-like scroll behavior when navigating to a new
  // route and using back/forward buttons.
  scrollBehavior(to, from, savedPosition) {

    // Disable this within same-route workplace to allow query changes without scroll (e.g. sort).
    const isWorkplace = to.name && to.name.split(':')[0] === 'workplace';

    return (isWorkplace && to.name === from.name) ? false : (savedPosition || { x: 0, y: 0 });
  },
});

router.beforeResolve(async (routeTo, routeFrom, next) => {
  // Create a `beforeResolve` hook, which fires whenever
  // `beforeRouteEnter` and `beforeRouteUpdate` would. This
  // allows us to ensure data is fetched even when params change,
  // but the resolved route does not. We put it in `meta` to
  // indicate that it's a hook we created, rather than part of
  // Vue Router (yet?).
  try {
    // For each matched route...
    for (const route of routeTo.matched) {
      await new Promise((resolve, reject) => {
        // If a `beforeResolve` hook is defined, call it with
        // the same arguments as the `beforeEnter` hook.
        if (route.meta && route.meta.beforeResolve) {
          route.meta.beforeResolve(routeTo, routeFrom, (...args) => {
            // If the user chose to redirect...
            if (args.length) {
              // If redirecting to the same route we're coming from...
              if (routeFrom.name === args[0].name) {
                // Complete the animation of the route progress bar.
                // NProgress.done()
              }
              // Complete the redirect.
              next(...args);
              reject(new Error('Redirected'));
            }
            else {
              resolve();
            }
          });
        }
        else {
          // Otherwise, continue resolving the route.
          resolve();
        }
      });
    }
    // If a `beforeResolve` hook chose to redirect, just return.
  }
  catch (error) {
    return;
  }

  // If we reach this point, continue resolving the route.
  next();
});


const deepCheck = (to, from, parent) => {
  // We have to be looking for more than just a base route...
  if (protectedRoutes.indexOf(to.name) > -1) { // this is a deep link attempt
    // Check if this is a deep link attempt
    // pass back the login route, complete with ReturnTo value
    const deepLink = `${window.location.origin}${parent}#${to.fullPath}`;
    localStorage.setItem('deepLink', deepLink);
    return `${window.location.origin}${drupalSettings.ur.sign_in_register_path}`;
  }
  return false;
};

router.beforeEach((to, from, next) => {
  // Check to see if the user is logged in
  if (get(drupalSettings, ['user', 'uid'], '') === 0) {
    // Is this a protected namespace?
    const found = protectedNamespaces.filter(namespace => window.location.pathname.indexOf(namespace) > -1);
    if (found.length) {
      const redirectUrl = deepCheck(to, from, window.location.pathname);
      if (redirectUrl) { // If we get a route string...
        window.addEventListener('DOMContentLoaded', () => {
          window.location.href = redirectUrl;
        });
        return;
      }
      // If it's a redirect, cancel the next callback, otherwise, just go...
      return redirectUrl ? next(false) : next();
    }

    next();
  }
  else { // If the user is logged in they can go wherever they want
    const willRedirect = from.fullPath === '/' && localStorage.getItem('deepLink');
    if (willRedirect) {
      const deepLink = localStorage.getItem('deepLink');
      localStorage.removeItem('deepLink');
      console.log('DEEP LINK FOUND: waiting for page load'); // eslint-disable-line
      window.addEventListener('load', () => {
        console.log('DEEP LINK FOUND: refreshing now'); // eslint-disable-line
        (window).location.href = deepLink;
      });
    }
    return willRedirect ? next(false) : next();
  }
});

/**
 * If the Vuex store is using IndexedDB to store state (i.e. if session storage
 * is enabled), setup a navigation guard that makes sure async state from storage
 * is loaded into state before continuing.
 * The `storageReady` event will be emitted by the VuexPersistPlugin we wire
 * up in store/index.ts. Without this, there are issues with reactivity.
 */
if (enableAsyncStorage) {
  let storageReady = false;
  router.beforeEach((to, from, next) => {
    if (!storageReady) {
      (store)._vm.$root.$once('storageReady', () => {
        storageReady = true;
        next();
      });
    }
    else {
      next();
    }
  });
}

// Sync up the App's current $route with a module in the Vuex store named 'route'
sync(store, router, {
  moduleName: 'route',
});


export default router;
