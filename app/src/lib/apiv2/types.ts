export interface UrApiResponse<T> {
  code: number;
  data: T;
  dataSource: string;
  error: boolean;
}
