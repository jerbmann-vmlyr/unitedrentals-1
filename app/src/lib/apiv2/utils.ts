import { UrApiResponse } from './types';
import { AxiosResponse } from 'axios';

export class ApiV2Utils {
  static responseHasErrors<T>(response: AxiosResponse<UrApiResponse<T>>) {
    // error checking logic (returns true or false)
    /** TODO: Check which errors we want to pass down, and which ones we want to catch here. */
  }
  static unwrapResponseData<T>(response: AxiosResponse<UrApiResponse<T>>): T {
    return response.data.data;
  }
}
