import { Address } from '../common-types';
import { BusinessType } from '@/lib/business-types';

export type BranchBusinessType = 'AR' | 'FS' | 'GR' | 'IR' | 'PR' | 'SS' | 'TS';

export interface BranchBusinessLink {
  branch_name: string;
  contact_us_link: string;
  rent_online_link: null;
  contact_us_link_show: string;
  rent_online_link_show: null;
}


export interface Branch extends Address {
  branchId: string;
  businessType: string;
  businessTypes: string;
  countryCode: 'CA' | 'US';
  district: string;
  fax: string;
  latitude: string;
  longitude: string;
  managerEmail: string;
  managerName: string;
  name: string;
  onlineOrders: '0' | '1';
  phone: string;
  primaryBusinessType: BusinessType;
  region: string;
  saturdayHours: string;
  sundayHours: string;
  timeOffset: string;
  videoUrl: string;
  webContentBlock: string;
  webDescription: string;
  webDisplay: '0' | '1';
  webProductType: string;
  weekdayHours: string;
  distance: number | null;
}
