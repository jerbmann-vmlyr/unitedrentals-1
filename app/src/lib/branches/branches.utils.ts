import urlUtils from '@/utils/url';
import { Branch } from './branches.types';

export class BranchUtils {
  static mapByBranchId(branches: Branch[]): { [branchId: string]: Branch } {
    return branches.reduce((mapped, branch) => ({ ...mapped, [branch.branchId]: branch }), {});
  }

  static getBusinessTypeUrlParam(branch: Branch): string {
    return urlUtils.urlify(branch.primaryBusinessType.url);
  }
}
