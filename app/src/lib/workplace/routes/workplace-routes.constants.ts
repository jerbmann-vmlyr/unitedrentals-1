import { SortDirection } from '@/lib/collections';
import { WorkplaceEquipmentStatusId } from '../equipment-statuses';
import {
  WorkplaceOrderFilterId,
  WorkplaceOrderGrouperId,
  WorkplaceOrderSorterId,
} from '@/lib/workplace';
import { WorkplaceRouteNames, WorkplaceQueryParams } from './workplace-routes.types';
import { WorkplaceEquipmentFilterId, WorkplaceEquipmentSorterId } from '../equipment-collection';
import { WorkplaceOrderStatusName } from '@/lib/workplace/workplace-order.types';

/**
 * The following interfaces define how all of the various filters are
 * integrated into a routing scheme.
 */
type PredefinedRoute = {
  name: WorkplaceRouteNames;
  query: Partial<WorkplaceQueryParams>;
};

export class WorkplacePredefinedRoutes {
  static readonly reservedForDelivery: PredefinedRoute = {
    name: WorkplaceRouteNames.EquipmentList,
    query: {
      sortBy: WorkplaceEquipmentSorterId.StartDate,
      sortDirection: SortDirection.Ascending,
      [WorkplaceEquipmentFilterId.EquipmentStatus]: [ WorkplaceEquipmentStatusId.ReservedForDelivery ],
    },
  };

  static readonly reservedForPickup: PredefinedRoute = {
    name: WorkplaceRouteNames.EquipmentList,
    query: {
      sortBy: WorkplaceEquipmentSorterId.StartDate,
      sortDirection: SortDirection.Ascending,
      [WorkplaceEquipmentFilterId.EquipmentStatus]: [ WorkplaceEquipmentStatusId.ReservedForBranchPickup ],
    },
  };

  static readonly overdue: PredefinedRoute = {
    name: WorkplaceRouteNames.EquipmentList,
    query: {
      sortBy: WorkplaceEquipmentSorterId.ReturnDate,
      sortDirection: SortDirection.Ascending,
      [WorkplaceEquipmentFilterId.EquipmentStatus]: [ WorkplaceEquipmentStatusId.Overdue ],
    },
  };

  static readonly dueSoon: PredefinedRoute = {
    name: WorkplaceRouteNames.EquipmentList,
    query: {
      sortBy: WorkplaceEquipmentSorterId.ReturnDate,
      sortDirection: SortDirection.Ascending,
      [WorkplaceEquipmentFilterId.EquipmentStatus]: [ WorkplaceEquipmentStatusId.DueSoon ],
    },
  };

  static readonly pickupRequested: PredefinedRoute = {
    name: WorkplaceRouteNames.EquipmentList,
    query: {
      sortBy: WorkplaceEquipmentSorterId.ReturnDate,
      sortDirection: SortDirection.Ascending,
      [WorkplaceEquipmentFilterId.EquipmentStatus]: [ WorkplaceEquipmentStatusId.PickupRequested ],
    },
  };

  static readonly scheduledForPickup: PredefinedRoute = {
    name: WorkplaceRouteNames.EquipmentList,
    query: {
      sortBy: WorkplaceEquipmentSorterId.ReturnDate,
      sortDirection: SortDirection.Ascending,
      [WorkplaceEquipmentFilterId.EquipmentStatus]: [
        WorkplaceEquipmentStatusId.PickupRequested,
        WorkplaceEquipmentStatusId.AdvancedPickup,
      ],
    },
  };

  static readonly pickupAdvanced: PredefinedRoute = {
    name: WorkplaceRouteNames.EquipmentList,
    query: {
      sortBy: WorkplaceEquipmentSorterId.ReturnDate,
      sortDirection: SortDirection.Ascending,
      [WorkplaceEquipmentFilterId.EquipmentStatus]: [ WorkplaceEquipmentStatusId.AdvancedPickup ],
    },
  };

  static readonly onRent: PredefinedRoute = {
    name: WorkplaceRouteNames.EquipmentList,
    query: {
      sortBy: WorkplaceEquipmentSorterId.ReturnDate,
      sortDirection: SortDirection.Ascending,
      [WorkplaceEquipmentFilterId.EquipmentStatus]: [ WorkplaceEquipmentStatusId.OnRent ],
    },
  };

  static readonly quotes: PredefinedRoute = {
    name: WorkplaceRouteNames.EquipmentList,
    query: {
      sortBy: WorkplaceEquipmentSorterId.StartDate,
      sortDirection: SortDirection.Ascending,
    },
  };

  static readonly reservations: PredefinedRoute = {
    name: WorkplaceRouteNames.EquipmentList,
    query: {
      sortBy: WorkplaceEquipmentSorterId.StartDate,
      sortDirection: SortDirection.Ascending,
    },
  };

  /* Order Routes */
  static readonly allOrders: PredefinedRoute = {
    name: WorkplaceRouteNames.OrderList,
    query: {
      sortBy: WorkplaceOrderSorterId.OrderNumber,
      sortDirection: SortDirection.Ascending,
      groupBy: WorkplaceOrderGrouperId.OrderStatus,
    },
  };

  static readonly overdueOrders: PredefinedRoute = {
    name: WorkplaceRouteNames.OrderList,
    query: {
      sortBy: WorkplaceOrderSorterId.OrderEstimatedEndDate,
      sortDirection: SortDirection.Ascending,
      [WorkplaceOrderFilterId.OrderStatus]: [WorkplaceOrderStatusName.Overdue, WorkplaceOrderStatusName.DueSoon],
      groupBy: WorkplaceOrderGrouperId.OrderStatus,
    },
  };

  static readonly contractOrders: PredefinedRoute = {
    name: WorkplaceRouteNames.OrderList,
    query: {
      sortBy: WorkplaceOrderSorterId.OrderNumber,
      sortDirection: SortDirection.Ascending,
      [WorkplaceOrderFilterId.OrderStatus]: [ WorkplaceOrderStatusName.Open ],
      groupBy: WorkplaceOrderGrouperId.JobsiteName,
    },
  };

  static readonly reservationOrders: PredefinedRoute = {
    name: WorkplaceRouteNames.OrderList,
    query: {
      sortBy: WorkplaceOrderSorterId.OrderStartDate,
      sortDirection: SortDirection.Ascending,
      [WorkplaceOrderFilterId.OrderStatus]: [ WorkplaceOrderStatusName.ReservedForBranchPickup, WorkplaceOrderStatusName.ReservedForDelivery ],
    },
  };

  static readonly quoteOrders: PredefinedRoute = {
    name: WorkplaceRouteNames.OrderList,
    query: {
      sortBy: WorkplaceOrderSorterId.OrderStartDate,
      sortDirection: SortDirection.Ascending,
      [WorkplaceOrderFilterId.OrderStatus]: [ WorkplaceOrderStatusName.Quoted ],
    },
  };
}
