import { isEmpty, isEqual } from 'lodash';
import {  Location, NavigationGuard, Route } from 'vue-router';
import { CollectionUtils, SortDirection } from '@/lib/collections';
import { WORKPLACE_EQUIPMENT_GROUPERS, WORKPLACE_EQUIPMENT_SORTERS, WorkplaceEquipmentSorterId } from '@/lib/workplace';
import { WorkplaceQueryParams } from './workplace-routes.types';

export class WorkplaceRouteUtils {
  static specialQueryParams(query: { [param: string]: any }): WorkplaceQueryParams {
    return {
      groupBy: CollectionUtils.selectOne(WORKPLACE_EQUIPMENT_GROUPERS, query.groupBy || '').id,
      sortBy:  WorkplaceEquipmentSorterId.ReturnDate,
      sortDirection: SortDirection.Ascending,
      equipmentStatus: ['OnRent'],
    };
  }

  static normalizeQueryParams(query: { [param: string]: any }): WorkplaceQueryParams {
    return {
      ...query,
      groupBy: CollectionUtils.selectOne(WORKPLACE_EQUIPMENT_GROUPERS, query.groupBy || '').id,
      sortBy: CollectionUtils.selectOne(WORKPLACE_EQUIPMENT_SORTERS, query.sortBy).id,
      sortDirection: query.sortDirection || SortDirection.Ascending,
    };
  }

  static routerNavigationGuard: NavigationGuard = (
    to: Route,
    from: Route,
    next: (location?: Location) => void,
  ) => {
    const toQuery = { ...(to.query || {}) };
    const query = to.name === 'workplace:equipment:list' && isEmpty(toQuery)
      ? WorkplaceRouteUtils.specialQueryParams(toQuery)
      : WorkplaceRouteUtils.normalizeQueryParams(toQuery);

    return isEqual(toQuery, query)
      ? next()
      : next({ path: to.fullPath, query });
  }
}
