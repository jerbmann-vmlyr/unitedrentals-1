import { GroupUtils, Grouper, Group } from '@/lib/collections';
import { WorkplaceEquipmentStatusUtils } from '../equipment-statuses';
import { WorkplaceEquipment, WorkplaceEquipmentGpsUtilization as Utilization } from '../workplace-equipment.types';
import { WorkplaceEquipmentUtils } from '../workplace-equipment.utils';
import i18n from '@/data/i18n';


export enum WorkplaceEquipmentGrouperId {
  Jobsite = 'jobsite',
  OrderType = 'orderType',
  OrderNumber = 'orderNumber',
  Requester = 'requester',
  EquipmentType = 'equipmentType',
  EquipmentStatus = 'equipmentStatus',
  Gps = 'gps',
  Po = 'poNumber',
  None = 'none',
}

export type WorkplaceEquipmentGrouper = Grouper<WorkplaceEquipment, WorkplaceEquipmentGrouperId>;
export type WorkplaceEquipmentGroup = Group<WorkplaceEquipment, WorkplaceEquipmentGrouperId>;


export const WORKPLACE_EQUIPMENT_GROUPERS: WorkplaceEquipmentGrouper[] = GroupUtils.validate([
  {
    id: WorkplaceEquipmentGrouperId.Jobsite,
    label: i18n.workplace.groupers.jobsite,
    classifier: ({ jobsite }) => jobsite.id,
    classificationLabel: (key, [{ jobsite, customerJobId }]) => [
      `${jobsite.name}`,
      customerJobId.length > 0 ? customerJobId : '',
      `${jobsite.city}, ${jobsite.state}`,
    ].filter(s => s.length > 0).join(' - '),
  },
  {
    id: WorkplaceEquipmentGrouperId.OrderType,
    label: i18n.workplace.groupers.transactionType,
    classificationLabel: (key, [item]) => item.status === 'Rental' ? 'Contract' : item.status,
    classifier: item => item.status,
  },
  {
    id: WorkplaceEquipmentGrouperId.OrderNumber,
    label: i18n.workplace.groupers.transactionNum,
    classificationLabel: (key, [item]) => item.transId,
    classifier: item => item.transId,
  },
  {
    id: WorkplaceEquipmentGrouperId.Requester,
    label: i18n.workplace.groupers.requester,
    classificationLabel: (key, [item]) => item.requesterName,
    classifier: item => item.requesterName,
  },
  {
    id: WorkplaceEquipmentGrouperId.EquipmentType,
    label: i18n.workplace.groupers.equipmentType,
    classificationLabel: (key, [item]) => item.eqpType !== null ? item.eqpType : 'Equipment type not provided',
    classifier: item => item.eqpType || '',
  },
  {
    id: WorkplaceEquipmentGrouperId.EquipmentStatus,
    label: i18n.workplace.groupers.equipmentStatus,
    classificationLabel: (key, [item]) => WorkplaceEquipmentStatusUtils.getStatusIdLabel(item.equipmentStatuses[0].id),
    classifier: item => item.equipmentStatuses[0].id,
  },
  {
    id: WorkplaceEquipmentGrouperId.Gps,
    label: i18n.workplace.groupers.GPS,
    classificationLabel: (key, [item]) => WorkplaceEquipmentUtils.getGpsUtilizationLabel(item.gpsUtilization),
    classifier: item => item.gpsUtilization === Utilization.Offline
      ? Utilization.NoSignal
      : item.gpsUtilization,
  },
  {
    id: WorkplaceEquipmentGrouperId.Po,
    label: i18n.workplace.groupers.PO,
    classificationLabel: (key, [item]) => item.po,
    classifier: item => item.po,
  },
  {
    id: WorkplaceEquipmentGrouperId.None,
    label: i18n.workplace.groupers.none,
    classificationLabel: () => '',
    isDefault: true,
    classifier: () => '',
  },
]);
