import { sortBy } from 'lodash';
import { Sorter } from '@/lib/collections';
import { WorkplaceEquipment } from '../workplace-equipment.types';
import { WorkplaceEquipmentUtils } from '../workplace-equipment.utils';
import { SortUtils } from '../../collections/sort/sorts.utils';
import { StatusUtils } from '../../statuses/statuses.utils';
import { WorkplaceEquipmentStatusId } from '../equipment-statuses/equipment-statuses.types';
import { WorkplaceEquipmentStatusUtils } from '../equipment-statuses/equipment-statuses.utils';

export enum WorkplaceEquipmentSorterId {
  Branch = 'branch',
  CatClass = 'catClass',
  DailyRate = 'dailyRate',
  DaysOnRent = 'daysOnRent',
  EquipmentId = 'equipmentId',
  EquipmentName = 'equipmentName',
  EquipmentType = 'equipmentType',
  Gps = 'gps',
  InLeniency = 'inLeniency',
  JobsiteAddress = 'jobsiteAddress',
  JobsiteCity = 'jobsiteCity',
  JobsiteId = 'jobsiteId',
  JobsiteName = 'jobsiteName',
  JobsiteState = 'jobsiteState',
  JobsiteZip = 'jobsiteZip',
  LastBilledDate = 'lastBilledDate',
  MonthlyRate = 'monthlyRate',
  OrderedBy = 'orderedBy',
  OrderType = 'orderType',
  Phone = 'phone',
  PickupDate = 'pickupDate',
  PickupId = 'pickupId',
  PickupQuantity = 'pickupQty',
  PoNumber = 'poNumber',
  Quantity = 'quantity',
  Requester = 'requester',
  RequisitionId = 'requisitionId',
  ReturnDate = 'returnDate',
  StartDate = 'startDate',
  Status = 'status',
  TransactionNumber = 'transactionNumber',
  UsageToday = 'usageToday',
  WeeklyRate = 'weeklyRate',
}

export type WorkplaceEquipmentSorter = Sorter<WorkplaceEquipment, WorkplaceEquipmentSorterId>;

export const WORKPLACE_EQUIPMENT_SORTERS: WorkplaceEquipmentSorter[] = SortUtils.validate([
  {
    id: WorkplaceEquipmentSorterId.Branch,
    sort: equipment => sortBy(equipment, eqp => eqp.branchId),
  },
  {
    id: WorkplaceEquipmentSorterId.CatClass,
    sort: equipment => sortBy(equipment, eqp => eqp.catClass),
  },
  {
    id: WorkplaceEquipmentSorterId.DailyRate,
    sort: equipment => sortBy(equipment, 'dayRate'),
  },
  {
    id: WorkplaceEquipmentSorterId.DaysOnRent,
    sort: equipment => sortBy(equipment, ({ daysOnRent }) => daysOnRent || 0),
  },
  {
    id: WorkplaceEquipmentSorterId.EquipmentId,
    sort: equipment => sortBy(equipment, 'equipmentId'),
  },
  {
    id: WorkplaceEquipmentSorterId.EquipmentName,
    isDefault: true,
    sort: equipment => sortBy(equipment, 'dalDescription'),
  },
  {
    id: WorkplaceEquipmentSorterId.EquipmentType,
    sort: equipment => sortBy(equipment, eqp => eqp.eqpType),
  },
  {
    id: WorkplaceEquipmentSorterId.Gps,
    sort: equipment => sortBy(equipment, eqp => WorkplaceEquipmentUtils.getGpsUtilizationLabel(eqp.gpsUtilization)),
  },
  {
    id: WorkplaceEquipmentSorterId.InLeniency,
    sort: equipment => sortBy(equipment, eqp => StatusUtils.statusListIncludesAnyOf(
      WorkplaceEquipmentStatusId.InLeniency,
      eqp.equipmentStatuses || [],
    )),
  },
  {
    id: WorkplaceEquipmentSorterId.JobsiteAddress,
    sort: equipment => sortBy(equipment, eqp => eqp.jobsite.address1),
  },
  {
    id: WorkplaceEquipmentSorterId.JobsiteCity,
    sort: equipment => sortBy(equipment, eqp => eqp.jobsite.city),
  },
  {
    id: WorkplaceEquipmentSorterId.JobsiteId,
    sort: equipment => sortBy(equipment, eqp => eqp.customerJobId),
  },
  {
    id: WorkplaceEquipmentSorterId.JobsiteName,
    sort: equipment => sortBy(equipment, eqp => eqp.jobsite.name),
  },
  {
    id: WorkplaceEquipmentSorterId.JobsiteState,
    sort: equipment => sortBy(equipment, eqp => eqp.jobsite.state),
  },
  {
    id: WorkplaceEquipmentSorterId.JobsiteZip,
    sort: equipment => sortBy(equipment, eqp => eqp.jobsite.zip),
  },
  {
    id: WorkplaceEquipmentSorterId.LastBilledDate,
    sort: equipment => sortBy(equipment, eqp => eqp.lastBilledDate),
  },
  {
    id: WorkplaceEquipmentSorterId.MonthlyRate,
    sort: equipment => sortBy(equipment, 'monthRate'),
  },
  {
    id: WorkplaceEquipmentSorterId.OrderedBy,
    sort: equipment => sortBy(equipment, eqp => eqp.orderedBy),
  },
  {
    id: WorkplaceEquipmentSorterId.OrderType,
    sort: equipment => sortBy(equipment, WorkplaceEquipmentStatusUtils.getOrderTypeLabel),
  },
  {
    id: WorkplaceEquipmentSorterId.Phone,
    sort: equipment => sortBy(equipment, eqp => eqp.phone),
  },
  {
    id: WorkplaceEquipmentSorterId.PickupDate,
    sort: equipment => sortBy(equipment, 'pickupDateTime'),
  },
  {
    id: WorkplaceEquipmentSorterId.PickupId,
    sort: equipment => sortBy(equipment, 'pickupId'),
  },
  {
    id: WorkplaceEquipmentSorterId.PickupQuantity,
    sort: equipment => sortBy(equipment, 'quantityPickedUp'),
  },
  {
    id: WorkplaceEquipmentSorterId.PoNumber,
    sort: equipment => sortBy(equipment, eqp => eqp.po),
  },
  {
    id: WorkplaceEquipmentSorterId.Quantity,
    sort: equipment => sortBy(equipment, WorkplaceEquipmentUtils.getStatusSpecificQuantity),
  },
  {
    id: WorkplaceEquipmentSorterId.Requester,
    sort: equipment => sortBy(equipment, eqp => eqp.requesterName),
  },
  {
    id: WorkplaceEquipmentSorterId.RequisitionId,
    sort: equipment => sortBy(equipment, 'requisitionId'),
  },
  {
    id: WorkplaceEquipmentSorterId.ReturnDate,
    sort: equipment => sortBy(equipment, 'returnDateTime'),
  },
  {
    id: WorkplaceEquipmentSorterId.StartDate,
    sort: equipment => sortBy(equipment, 'startDateTime'),
  },
  {
    id: WorkplaceEquipmentSorterId.Status,
    sort: equipment => sortBy(equipment, eqp =>
      eqp.equipmentStatuses.length > 0
        ? WorkplaceEquipmentStatusUtils.getStatusIdLabel(eqp.equipmentStatuses[0].id)
        : '',
    ),
  },
  {
    id: WorkplaceEquipmentSorterId.TransactionNumber,
    sort: equipment => sortBy(equipment, eqp => eqp.transId || eqp.requisitionId),
  },
  {
    id: WorkplaceEquipmentSorterId.UsageToday,
    sort: equipment => sortBy(equipment, eqp => eqp.usageToday),
  },
  {
    id: WorkplaceEquipmentSorterId.WeeklyRate,
    sort: equipment => sortBy(equipment, eqp => eqp.weekRate),
  },
]);
