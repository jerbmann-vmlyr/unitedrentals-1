import { AxiosResponse } from 'axios';
import * as OnRentDataCCJV from '@/data/mocks/ccjv/on-rent.json';
import { WorkplaceEquipmentApi } from '@/api/api.workplace-equipment';
import { WORKPLACE_EQUIPMENT_FILTERS, WorkplaceEquipmentFilterId } from './equipment.filters';
import { WORKPLACE_EQUIPMENT_GROUPERS, WorkplaceEquipmentGrouperId } from './equipment.groupers';
import { WORKPLACE_EQUIPMENT_SORTERS, WorkplaceEquipmentSorterId } from './equipment.sorters';

const EquipmentListCcjv = WorkplaceEquipmentApi.transformOnRentEquipmentResponse(
  { data: OnRentDataCCJV } as AxiosResponse<any>,
  '50343',
);

describe('Equipment groupers', () => {
  test('all group IDs are unique', () => {
    const ids = WORKPLACE_EQUIPMENT_GROUPERS.map(sorter => sorter.id);
    const uniqueIds = new Set(ids);
    expect(ids.length).toEqual(uniqueIds.size);
  });

  test('all defined sort IDs are used', () => {
    const definedIds = Object.values(WorkplaceEquipmentGrouperId);
    const usedIds = WORKPLACE_EQUIPMENT_GROUPERS.map(grouper => grouper.id);
    definedIds.forEach(id => {
      expect(usedIds).toContain(id);
    });
  });

  test('one and only one grouper has an isDefault property with a value of true', () => {
    const isDefault = WORKPLACE_EQUIPMENT_GROUPERS.filter(sorter => sorter.isDefault === true);
    expect(isDefault.length).toEqual(1);
  });
});


describe('Equipment filters', () => {
  test('all filter IDs are unique', () => {
    const ids = WORKPLACE_EQUIPMENT_FILTERS.map(sorter => sorter.id);
    const uniqueIds = new Set(ids);
    expect(ids.length).toEqual(uniqueIds.size);
  });

  test('all defined filter IDs are used', () => {
    const definedIds = Object.values(WorkplaceEquipmentFilterId);
    const usedIds = WORKPLACE_EQUIPMENT_FILTERS.map(grouper => grouper.id);
    definedIds.forEach(id => {
      expect(usedIds).toContain(id);
    });
  });
});

describe('Equipment sorters', () => {
  test('all sort IDs are unique', () => {
    const ids = WORKPLACE_EQUIPMENT_SORTERS.map(sorter => sorter.id);
    const uniqueIds = new Set(ids);
    expect(ids.length).toEqual(uniqueIds.size);
  });

  test('all defined sort IDs are used', () => {
    const definedIds = Object.values(WorkplaceEquipmentSorterId);
    const usedIds = WORKPLACE_EQUIPMENT_SORTERS.map(sorter => sorter.id);
    definedIds.forEach(id => {
      expect(usedIds).toContain(id);
    });
  });

  test('one and only one sorter has an isDefault property with a value of true', () => {
    const isDefault = WORKPLACE_EQUIPMENT_SORTERS.filter(sorter => sorter.isDefault === true);
    expect(isDefault.length).toEqual(1);
  });

  test(`sort functions don't mutate nor change the length of an equipment list`, () => {
    WORKPLACE_EQUIPMENT_SORTERS.forEach(({ sort }) => {
      const sorted = sort(EquipmentListCcjv);
      expect(sorted).toHaveLength(EquipmentListCcjv.length);
      expect(EquipmentListCcjv).not.toBe(sorted);
    });
  });
});
