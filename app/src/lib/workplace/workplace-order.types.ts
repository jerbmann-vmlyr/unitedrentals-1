import * as common from '@/lib/common-types';
import { WorkplaceEquipment } from './workplace-equipment.types';
import { WorkplaceOrderStatusId } from '@/lib/workplace/order-statuses';
import { WorkplaceEquipmentStatus } from '@/lib/workplace/equipment-statuses';

export interface WorkplaceOrder {
  accountCodes: null,
  accountId: string,
  active: null,
  approver: string,
  approverGuid: string|'',
  billThru: string,
  branchId: string|'',
  currency: common.CurrencyCode,
  customerJobId: string|'',
  customerName: string,
  equipment: WorkplaceEquipment[],
  jobsite: common.Jobsite,
  jobsiteAddress: string,
  jobsiteContact: string,
  jobsiteId: string,
  jobsiteName: string,
  jobsitePhone: string,
  jobsiteState: string,
  jobsiteZip: string,
  orderDeliveryMethod: boolean,
  orderedBy: string|'',
  orderEstimatedEndDate: string,
  orderNumber: string,
  orderPickupMethod: string,
  orderRentalDuration: string,
  orderStartDate: string,
  status: WorkplaceOrderStatusName,
  statuses: WorkplaceOrderStatusName[],
  orderStatus: WorkplaceOrderStatusId,
  orderStatuses: WorkplaceEquipmentStatus[],
  orderType: WorkplaceOrderTransactionType,
  purchaseOrder: string,
  requester: string,
  requesterGuid: string|'',
  requisitionNumber: string,
  totalDailyEstimate: string,
  totalItems: number,
}

export enum WorkplaceOrderTransactionType {
  Reservation = 'Reservation',
  Quote = 'Quote',
  Contract = 'Contract',
}

export enum WorkplaceOrdersActionTypes {
  ViewDetails = 'View Details',
  ConvertQuote = 'Convert Quote',
  EditPurchaseOrder = 'Edit PO',
  ViewDocuments = 'View Documents',
  RequestPickup = 'Request Pickup',
  ExtendRental = 'Extend Rental',
}

export enum WorkplaceOrderStatusName {
  Open = 'Open',
  Reserved = 'Reserved',
  ReservedForBranchPickup = 'Reserved For Branch Pickup',
  ReservedForDelivery = 'Reserved For Delivery',
  Quoted = 'Quoted',
  Overdue = 'Overdue',
  DueSoon = 'Due Soon',
}
