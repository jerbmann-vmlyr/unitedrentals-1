import {
  ClusterIconStyle,
  MapMarkerIconPaths,
} from '@/lib/google-maps';

export const defaultMapOptions: google.maps.MapOptions = {
  disableDoubleClickZoom: true,
  draggable: true,
  fullscreenControl: false,
  mapTypeControl: false,
  maxZoom: 16,
  scrollwheel: true,
  streetViewControl: false,
  zoomControl: false,
};

export const defaultClusterIconStyle: ClusterIconStyle = {
  textColor: 'white',
  url: MapMarkerIconPaths.ClusterBlue,
  width: 53,
  height: 53,
};
