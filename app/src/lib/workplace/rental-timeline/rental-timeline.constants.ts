
import {
  WorkplaceRentalTimelineOptions,
} from './rental-timeline.types';

export const WorkplaceRentalTimelineDefaultOptions: WorkplaceRentalTimelineOptions = {
  bookendHeight: 40,
  fontSize: 12,
  height: 160,
  labelWidth: 60,
  leniencyThreshold: 5,
  markerLineHeight: 28,
  markerSize: 20,
  strokeWidth: 6,
  totalDayCount: 7,
  visibleDayCount: 30,
  width: 400,
};
