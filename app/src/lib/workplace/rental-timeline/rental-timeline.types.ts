
export interface Leniency {
  endDate: string,
  rate: number,
  startDate: string,
}

export interface WorkplaceRentalTimelineOptions {
  bookendHeight: number,
  fontSize: number,
  height: number,
  labelWidth: number,
  leniencyThreshold: number,
  markerLineHeight: number,
  markerSize: number,
  strokeWidth: number,
  totalDayCount: number,
  visibleDayCount: number,
  width: number,
}

export interface WorkplaceRentalTimelineDates {
  billThru: string;
  leniencies: Leniency[];
  rentalEnd: string;
  rentalStart: string;
}
