/**
 * Generic listing column settings type
 * K: Column keys
 * G: Group IDs
 * S: Sort IDs
 */
export interface ListingColumnSettings<K, G, S> {
  readonly columnKey: K;
  group: G;
  index: number;
  label: { short: string; long: string; };
  locked: boolean;
  pinned: 'left' | 'right' | false;
  sortable: S | false;
  visible: boolean;
  width: number;
}

export interface ListingColumnLabel {
  short: string;
  long: string;
}

export enum ListingColumnWidths {
  xxxSmall = (40 + 32),
  xxSmall = (56 + 32),
  xSmall = (72 + 32),
  Small = (88 + 32),
  Medium = (112 + 32),
  Large = (140 + 32),
  xLarge = (200 + 32),
}

export enum WorkplaceCollectionType {
  Equipment = 'equipment',
  Orders = 'orders',
  Unspecified = 'unspecified',
}

/**
 * Workplace Listing Layouts
 */
export enum WorkplaceListingLayout {
  Table = 'table',
  Cards = 'cards',
}

/**
 * Workplace Resources Defined By Confluence Document
 * https://unitedrentals.atlassian.net/wiki/spaces/KD/pages/644513793/Status+Action+Taxonomy+1.0
 */
export enum WorkplaceResource {
  Equipment = 'equipment',
  Billing = 'billing',
  Orders = 'orders',
}

/**
 * Workplace Colors Defined By Confluence Document
 * https://unitedrentals.atlassian.net/wiki/spaces/KD/pages/644513793/Status+Action+Taxonomy+1.0
 */
export enum WorkplaceColor {
  BlueHighlight = '#008CFF',
  BrightRed = '#E10000',
  Grey = '#DDE0E2',
  Green = '#48BD00',
  OrangeHighlight = '#FFCF3D',
}

/**
 * Workplace Statuses Defined By Confluence Document
 * https://unitedrentals.atlassian.net/wiki/spaces/KD/pages/644513793/Status+Action+Taxonomy+1.0
 */
export enum WorkplaceStatus {
  Quoted = 'Quoted',
  ReservedForBranchPickup = 'ReservedForBranchPickup',
  ReservedForDelivery = 'ReservedForDelivery',
  OnRent = 'OnRent',
  PickupRequested = 'PickupRequested',
  AdvancedPickup = 'AdvancedPickup',
  InLeniency = 'InLeniency',
  NotYetInLeniency = 'NotYetInLeniency',
  DueSoon = 'DueSoon',
  Overdue = 'Overdue',
  Open = 'Open',
  Online = 'Online',
  Active = 'Active',
  Idle = 'Idle',
  Overtime = 'Overtime',
}

/**
 * Statuse Colors Defined By Confluence Document
 * https://unitedrentals.atlassian.net/wiki/spaces/KD/pages/644513793/Status+Action+Taxonomy+1.0
 */
export enum WorkplaceStatusColor {
  /* Quote */
    Quoted = WorkplaceColor.Grey,
  /* Reservation */
    ReservedForBranchPickup = WorkplaceColor.Grey,
    ReservedForDelivery = WorkplaceColor.Grey,
  /* Contract */
    OnRent = WorkplaceColor.Grey,
    PickupRequested = WorkplaceColor.OrangeHighlight,
    AdvancedPickup = WorkplaceColor.OrangeHighlight,
    InLeniency = WorkplaceColor.Green,
    NotYetInLeniency = WorkplaceColor.Grey,
    DueSoon = WorkplaceColor.OrangeHighlight,
    Overdue = WorkplaceColor.BrightRed, // Overdue Status Replaces Outdated Pending Due Status
  /* Order Status */
    // Quoted = WorkplaceColor.Grey
    QuoteExpiringSoon = WorkplaceColor.BrightRed,
  /* Reservation */
    // ReservedForBranchPickup = WorkplaceColor.Grey,
    // ReservedForDelivery = WorkplaceColor.Grey,
  /* Contract */
    Open = WorkplaceColor.Grey,
    // DueSoon = WOrkplaceColor.OrangeHighlight,
    // Overdue = WorkplaceColor.BrightRed,
  /* Billing */
    Unpaid = WorkplaceColor.OrangeHighlight,
    PartiallyPaid = WorkplaceColor.OrangeHighlight,
    Paid = WorkplaceColor.Green,
  /* GPS Status */
    Online = WorkplaceColor.BlueHighlight,
    Active = WorkplaceColor.BlueHighlight,
    Idle = WorkplaceColor.BrightRed,
    Overtime = WorkplaceColor.BrightRed,
    NoSignal = WorkplaceColor.BrightRed,
  /* Aliases */
  Reservation = WorkplaceColor.Grey,
  Reserved = WorkplaceColor.Grey,
  Reserve = WorkplaceColor.Grey,
  Quote = WorkplaceColor.Grey,
  QuoteExpiresSoon = WorkplaceColor.BrightRed,
  Contract = WorkplaceColor.Grey,
  OnContract = WorkplaceColor.Grey,
  Pickup = WorkplaceColor.OrangeHighlight,
  PickupRequest = WorkplaceColor.OrangeHighlight,
  ExpiresSoon = WorkplaceColor.BrightRed,
}

/**
 * Workplace Actions Defined By Confluence Documentation (Bottom Of Page, Actions Section)
 * https://unitedrentals.atlassian.net/wiki/spaces/KD/pages/644513793/Status+Action+Taxonomy+1.0
 */
export enum WorkplaceAction {
  EditPurchaseOrder = 'Edit PO',
  ConvertQuote = 'Convert Quote',
  ContactBranch = 'Contact Branch',
  ExtendRental = 'Extend Rental',
  Favorite = 'Favorite',
  RequestPickup = 'Request Pickup',
  ViewDetails = 'View Details',
  PrintQuote = 'Print Quote',
  PrintDetails = 'Print Details',
  ViewDocuments = 'View Documents',
  ViewOrPrintContract = 'View Or Print Contract',
  ViewCurrentContract = 'View Current Contract',
  ViewOriginalContract = 'View Original Contract',
  ViewReservation = 'View Reservation',
  RentAnother = 'Rent Another',
  ViewOrPrintInvoice = 'View Or Print Invoice',
  EmailInvoice = 'Email Invoice',
  ViewAndEditQuote = 'View And Edit Quote',
  /* Future Work */
    // EmailContract = 'emailContract',
    // ViewConditionReport = 'viewConditionReport',
}
