import { CurrencyCode } from '../common-types';

export interface Invoice {
  accountId: string;
  amount: string;
  balance: number;
  bilLFromDate: string;
  billToDate: string;
  currency: CurrencyCode;
  customerJobId: string;
  date: string;
  hasFASTPdf: boolean;
  id: string;
  jobId: string;
  jobName: string;
  jobsiteAddress1: string;
  jobsiteCity: string;
  jobsiteState: string;
  paidNotPosted: boolean;
  po: string;
  reqId: string;
  seqId: string;
  status: string; // TODO: This should probably be an enum (need API docs)
  type: string; // TODO: Ditto
}
