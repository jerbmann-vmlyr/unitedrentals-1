// import { AxiosResponse } from 'axios';
// import { WorkplaceOrderApi } from '@/api/api.workplace-order';
// import * as OrderDataCCJV from '@/data/mocks/ccjv/on-rent.json';
import { WORKPLACE_ORDER_FILTERS, WorkplaceOrderFilterId } from './order.filters';
// import { WORKPLACE_ORDER_SORTERS, WorkplaceOrderSorterId } from './order.sorters';
import { WORKPLACE_ORDER_GROUPERS, WorkplaceOrderGrouperId } from './order.groupers';
// const EquipmentListCcjv = WorkplaceOrderApi.transformOnRentEquipmentResponse(
//   { data: OrderDataCCJV } as AxiosResponse<any>,
//   '50343',
// );

describe('Order groupers', () => {
  test('all group IDs are unique', () => {
    const ids = WORKPLACE_ORDER_GROUPERS.map(sorter => sorter.id);
    const uniqueIds = new Set(ids);
    expect(ids.length).toEqual(uniqueIds.size);
  });

  test('all defined sort IDs are used', () => {
    const definedIds = Object.values(WorkplaceOrderGrouperId);
    const usedIds = WORKPLACE_ORDER_GROUPERS.map(grouper => grouper.id);
    definedIds.forEach(id => {
      expect(usedIds).toContain(id);
    });
  });

  test('one and only one grouper has an isDefault property with a value of true', () => {
    const isDefault = WORKPLACE_ORDER_GROUPERS.filter(sorter => sorter.isDefault === true);
    expect(isDefault.length).toEqual(1);
  });
});


describe('Order filters', () => {
  test('all filter IDs are unique', () => {
    const ids = WORKPLACE_ORDER_FILTERS.map(sorter => sorter.id);
    const uniqueIds = new Set(ids);
    expect(ids.length).toEqual(uniqueIds.size);
  });
  test('all defined filter IDs are used', () => {
    const definedIds = Object.values(WorkplaceOrderFilterId);
    const usedIds = WORKPLACE_ORDER_FILTERS.map(grouper => grouper.id);
    definedIds.forEach(id => {
      expect(usedIds).toContain(id);
    });
  });
});

// describe('ORDER sorters', () => {
//   test('all orders sort IDs are unique', () => {
//     const ids = WORKPLACE_ORDER_SORTERS.map(sorter => sorter.id);
//     const uniqueIds = new Set(ids);
//     expect(ids.length).toEqual(uniqueIds.size);
//   });
//
//   test('all orders defined sort IDs are used', () => {
//     const definedIds = Object.values(WorkplaceOrderSorterId);
//     const usedIds = WORKPLACE_ORDER_SORTERS.map(sorter => sorter.id);
//     definedIds.forEach(id => {
//       expect(usedIds).toContain(id);
//     });
//   });
//
//   test('one and only one sorter has an isDefault property with a value of true', () => {
//     const isDefault = WORKPLACE_ORDER_SORTERS.filter(sorter => sorter.isDefault === true);
//     expect(isDefault.length).toEqual(1);
//   });
//   // test(`sort functions don't mutate nor change the length of an equipment list`, () => {
//   //   WORKPLACE_ORDER_SORTERS.forEach(({ sort }) => {
//   //     const sorted = sort(EquipmentListCcjv);
//   //     expect(sorted).toHaveLength(EquipmentListCcjv.length);
//   //     expect(EquipmentListCcjv).not.toBe(sorted);
//   //   });
//   // });
// });
