import { sortBy } from 'lodash';
import { Sorter } from '@/lib/collections';
import { SortUtils } from '@/lib/collections/sort/sorts.utils';
import { WorkplaceOrder } from '@/lib/workplace/workplace-order.types';
import { SORT_ORDER_STATUSES } from '@/lib/workplace';

export enum WorkplaceOrderSorterId {
  Approver = 'approver',
  BillThru = 'billThru',
  BranchId = 'branchId',
  JobsiteId = 'customerJobId',
  JobsiteAddress = 'jobsiteAddress',
  JobsiteCity = 'jobsiteCity',
  JobsiteContact = 'jobsiteContact',
  JobsiteName = 'jobsiteName',
  JobsitePhone = 'jobsitePhone',
  JobsiteState = 'jobsiteState',
  JobsiteZip = 'jobsiteZip',
  OrderDeliveryMethod = 'orderDeliveryMethod',
  OrderEstimatedEndDate = 'orderEstimatedEndDate',
  OrderNumber = 'orderNumber',
  OrderPickupMethod = 'orderPickupMethod',
  OrderRentalDuration = 'orderRentalDuration',
  OrderStartDate = 'orderStartDate',
  OrderStatus = 'status',
  OrderType = 'orderType',
  OrderedBy = 'orderedBy',
  PurchaseOrder = 'purchaseOrder',
  Requester = 'requester',
  RequisitionNumber = 'requisitionNumber',
  TotalItems = 'totalItems',
}

export type WorkplaceOrderSorter = Sorter<WorkplaceOrder, WorkplaceOrderSorterId>;

export const WORKPLACE_ORDER_SORTERS: WorkplaceOrderSorter[] = SortUtils.validate([
  {
    id: WorkplaceOrderSorterId.BranchId,
    sort: items => sortBy(items, ({ branchId }) => branchId),
  },
  {
    id: WorkplaceOrderSorterId.TotalItems,
    sort: items => sortBy(items, ({ totalItems }) => totalItems),
  },
  {
    id: WorkplaceOrderSorterId.OrderedBy,
    sort: items => sortBy(items, ({ orderedBy }) => orderedBy),
  },
  {
    id: WorkplaceOrderSorterId.JobsiteId,
    sort: items => sortBy(items, ({ jobsite }) => jobsite.customerJobId),
  },
  {
    id: WorkplaceOrderSorterId.JobsiteName,
    sort: items => sortBy(items, ({ jobsite }) => jobsite.name),
  },
  {
    id: WorkplaceOrderSorterId.JobsiteAddress,
    sort: items => sortBy(items, ({ jobsite }) => jobsite.address1),
  },
  {
    id: WorkplaceOrderSorterId.JobsiteCity,
    sort: items => sortBy(items, ({ jobsite }) => jobsite.city),
  },
  {
    id: WorkplaceOrderSorterId.JobsiteState,
    sort: items => sortBy(items, ({ jobsite }) => jobsite.state),
  },
  {
    id: WorkplaceOrderSorterId.JobsiteZip,
    sort: items => sortBy(items, ({ jobsite }) => jobsite.zip),
  },
  {
    id: WorkplaceOrderSorterId.JobsiteContact,
    sort: items => sortBy(items, ({ jobsite }) => jobsite.contact),
  },
  {
    id: WorkplaceOrderSorterId.JobsitePhone,
    sort: items => sortBy(items, ({ jobsite }) => jobsite.mobilePhone),
  },
  {
    id: WorkplaceOrderSorterId.Requester,
    sort: items => sortBy(items, ({ requester }) => requester),
  },
  {
    id: WorkplaceOrderSorterId.Approver,
    sort: items => sortBy(items, ({ approver }) => approver),
  },
  {
    id: WorkplaceOrderSorterId.OrderStartDate,
    sort: items => sortBy(items, ({ orderStartDate }) => orderStartDate),
  },
  {
    id: WorkplaceOrderSorterId.OrderEstimatedEndDate,
    sort: items => sortBy(items, ({ orderEstimatedEndDate }) => orderEstimatedEndDate),
  },
  {
    id: WorkplaceOrderSorterId.OrderRentalDuration,
    sort: items => sortBy(items, ({ orderRentalDuration }) => Number(orderRentalDuration)),
  },
  {
    id: WorkplaceOrderSorterId.OrderDeliveryMethod,
    sort: items => sortBy(items, ({ orderDeliveryMethod }) => orderDeliveryMethod),
  },
  {
    id: WorkplaceOrderSorterId.OrderPickupMethod,
    sort: items => sortBy(items, ({ orderPickupMethod }) => orderPickupMethod),
  },
  {
    id: WorkplaceOrderSorterId.BillThru,
    sort: items => sortBy(items, ({ billThru }) => billThru),
  },
  {
    id: WorkplaceOrderSorterId.OrderType,
    sort: items => sortBy(items, ({ orderType }) => orderType),
  },
  {
    id: WorkplaceOrderSorterId.OrderNumber,
    sort: items => sortBy(items, ({ orderNumber }) => orderNumber),
  },
  {
    id: WorkplaceOrderSorterId.RequisitionNumber,
    sort: items => sortBy(items, ({ requisitionNumber }) => requisitionNumber),
  },
  {
    isDefault: true,
    id: WorkplaceOrderSorterId.OrderStatus,
    sort: items => items.sort(({ status: firstStatus }, { status: nextStatus }) => SORT_ORDER_STATUSES(nextStatus, firstStatus)),
  },
  {
    id: WorkplaceOrderSorterId.PurchaseOrder,
    sort: items => sortBy(items, ({ purchaseOrder }) => purchaseOrder),
  },
]);
