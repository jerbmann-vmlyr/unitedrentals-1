import { uniqBy } from 'lodash';
import { MomentUtils } from '@/lib/moment.utils';
import { Filter, FilterType, ParsedFilter } from '@/lib/collections';
import { WorkplaceOrder, WorkplaceOrderStatusName } from '../workplace-order.types';

export enum WorkplaceOrderFilterId {
  /* Search */ Search = 'search',
  /* Static */ OrderStatus = 'orderStatus',
  /* Static */ StartDate = 'startDate',
  /* Simple */ MyOrders = 'myOrders',
  /* Dynamic */ OrderedBy = 'orderedBy',
  /* Dynamic */ Jobsite = 'jobsite',
  /* Dynamic */ Approver = 'approver',
  /* Dynamic */ Requester = 'requester',
  /* Dynamic */ PurchaseOrder = 'purchaseOrder',
}

export type WorkplaceOrderFilter = Filter<WorkplaceOrder, WorkplaceOrderFilterId>;
export type ParsedWorkplaceOrderFilter = ParsedFilter<WorkplaceOrder, WorkplaceOrderFilterId>;

export const WORKPLACE_ORDER_FILTERS: WorkplaceOrderFilter[] = [
  /**
   * 1. My Orders
   * Defined by user acting as requester, or approver, ordered by user, or user as job contact
   */
  {
    type: FilterType.Simple,
    id: WorkplaceOrderFilterId.MyOrders,
    queryValue: true,
    label: 'My Orders',
    filter: ({ requester, approver, orderedBy, jobsite }) => {
      const tcUser = (window as any)._app.$store.state.tcUser;
      const loggedInUserName = (`${tcUser.firstName || ''} ${tcUser.lastName || ''}`).toUpperCase().trim();
      const jobsiteContact = (jobsite.contact || '').toUpperCase().trim();
      requester = (requester || '').toUpperCase().trim();
      approver = (approver || '').toUpperCase().trim();
      orderedBy = (orderedBy || '').toUpperCase().trim();
      return [requester, approver, orderedBy, jobsiteContact].includes(loggedInUserName);
    },
  },
  /**
   * 2. Jobsite
   */
  {
    type: FilterType.Dynamic,
    id: WorkplaceOrderFilterId.Jobsite,
    label: 'Jobsite',
    filterFactory: queryValue => resource => resource.jobsite.id === queryValue,
    valueFactory: resources => uniqBy(
      resources.map(({ jobsite, customerJobId }) => ({
        queryValue: jobsite.id,
        label: [
          jobsite.name,
          customerJobId && customerJobId.length
            ? customerJobId
            : `${jobsite.city}, ${jobsite.state}`,
        ],
        filter: resource => resource.jobsite.id === jobsite.id,
      })),
      value => value.queryValue,
    ),
  },
  /**
   * 3. Order Status: Static
   *        Quote
   *        Reservation
   *        Contract
   *        Due Soon
   *        Overdue
   */
  {
    type: FilterType.Static,
    id: WorkplaceOrderFilterId.OrderStatus,
    label: 'Order Status',
    filters: [
      { // Quotes
        queryValue: WorkplaceOrderStatusName.Quoted,
        label: 'Quoted',
        filter: ({ statuses }) => statuses.includes(WorkplaceOrderStatusName.Quoted),
      },
      { // Reserved For Branch Pickup
        queryValue: WorkplaceOrderStatusName.ReservedForBranchPickup,
        label: 'Reserved For Branch Pickup',
        filter:  ({ statuses }) => statuses.includes(WorkplaceOrderStatusName.ReservedForBranchPickup),
      },
      { // Reserved For Delivery
        queryValue: WorkplaceOrderStatusName.ReservedForDelivery,
        label: 'Reserved For Delivery',
        filter:  ({ statuses }) => statuses.includes(WorkplaceOrderStatusName.ReservedForDelivery),
      },
      {
        // Open
        queryValue: WorkplaceOrderStatusName.Open,
        label: 'Open',
        filter: ({ statuses }) => statuses.includes(WorkplaceOrderStatusName.Open),
      },
      { // Due Soon
        queryValue: WorkplaceOrderStatusName.DueSoon,
        label: 'Due Soon',
        filter:  ({ statuses }) => statuses.includes(WorkplaceOrderStatusName.DueSoon),
      },
      { // Overdue
        queryValue: WorkplaceOrderStatusName.Overdue,
        label: 'Overdue',
        filter:  ({ statuses }) => statuses.includes(WorkplaceOrderStatusName.Overdue),
      },
    ],
  },
  /**
   * 4. Requester: Names of people who requested an order. (Same type as Jobsite, Different than ordered by)
   */
  {
    type: FilterType.Dynamic,
    id: WorkplaceOrderFilterId.Requester,
    label: 'Requester',
    filterFactory: queryValue => resource => resource.requester === queryValue,
    valueFactory: resources => uniqBy(
      resources.map(({ requester }) => ({
        queryValue: requester,
        label: requester,
        filter: resource => resource.requester === requester,
      })),
      value => value.queryValue,
    ),
  },
  /**
   * 5. Approver: If defined by customer, the names of users who approve an order (??)
   */
  {
    type: FilterType.Dynamic,
    id: WorkplaceOrderFilterId.Approver,
    label: 'Approver',
    filterFactory: queryValue => resource => resource.approver === queryValue,
    valueFactory: resources => uniqBy(
      resources.map(({ approver }) => ({
        queryValue: approver,
        label: approver,
        filter: resource => resource.approver === approver,
      })),
      value => value.queryValue,
    ),
  },
  /**
   * 6. OrderedBy: Names of users who have placed orders (Same type as jobsite)
   */
  {
    type: FilterType.Dynamic,
    id: WorkplaceOrderFilterId.OrderedBy,
    label: 'Ordered By',
    filterFactory: queryValue => resource => resource.orderedBy === queryValue,
    valueFactory: resources => uniqBy(
      resources.map(({ orderedBy }) => ({
        queryValue: orderedBy,
        label: orderedBy,
        filter: resource => resource.orderedBy === orderedBy,
      })),
      value => value.queryValue,
    ),
  },
  /**
   * 7. Purchase Order: (Same type as jobsite)
   */
  {
    type: FilterType.Dynamic,
    id: WorkplaceOrderFilterId.PurchaseOrder,
    label: 'PO',
    filterFactory: queryValue => item => item.purchaseOrder === queryValue,
    valueFactory: items => uniqBy(
      items
        .filter(({ purchaseOrder }) => !!purchaseOrder && purchaseOrder.length > 0)
        .map(({ purchaseOrder }) => ({
          queryValue: purchaseOrder,
          label: purchaseOrder,
          filter: item => item.purchaseOrder === purchaseOrder,
        })),
      value => value.queryValue,
    ),
  },
  /**
   * 8. Start Date: Filters orders depending on their starting date.
   *      Each option should display count of orders within each parameter.
   *      This Week  (#)
   *      This Month (#)
   *      Next 6 Months (#)
   */
  {
    type: FilterType.Static,
    id: WorkplaceOrderFilterId.StartDate,
    label: 'Start Date',
    filters: [
      {
        queryValue: 'withinWeek',
        label: 'This Week',
        filter: ({ orderStartDate }) => MomentUtils.happensThisWeek(orderStartDate),
      },
      {
        queryValue: 'withinMonth',
        label: 'This Month',
        filter: ({ orderStartDate }) => MomentUtils.happensThisMonth(orderStartDate),
      },
      {
        queryValue: 'withinSixMonths',
        label: 'Next 6 Months',
        filter: ({ orderStartDate }) => MomentUtils.happensInNextSixMonths(orderStartDate),
      },
    ],
  },
  /**
   * 9. Search
   */
  {
    type: FilterType.Search,
    id: WorkplaceOrderFilterId.Search,
    label: 'Search Term',
    tokenizer: ({ jobsite, ...resource }) => [
      resource.orderNumber,
      resource.requisitionNumber,
      resource.status,
      resource.orderType,
      resource.requester,
      resource.approver,
      resource.orderedBy,
      resource.accountId,
      jobsite.name,
      jobsite.id,
      resource.purchaseOrder,
    ]
      .filter((term): term is string => term !== null)
      .map(term => `${term}`.toLowerCase()),
  },
];
