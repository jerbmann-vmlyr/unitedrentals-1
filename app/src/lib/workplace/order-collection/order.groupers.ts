import { GroupUtils, Grouper, Group } from '@/lib/collections';
import { WorkplaceOrder } from '../workplace-order.types';
import i18n from '@/data/i18n';


export enum WorkplaceOrderGrouperId {
  JobsiteName = 'jobsiteName',
  OrderStatus = 'orderStatus',
  Requester = 'requester',
  Approver = 'approver',
  OrderedBy = 'orderedBy',
  PurchaseOrder = 'purchaseOrder',
  None = 'none',
}

export type WorkplaceOrderGrouper = Grouper<WorkplaceOrder, WorkplaceOrderGrouperId>;
export type WorkplaceOrderGroup = Group<WorkplaceOrder, WorkplaceOrderGrouperId>;
// export const WORKPLACE_ORDER_GROUPERS = [];
export const WORKPLACE_ORDER_GROUPERS: WorkplaceOrderGrouper[] = GroupUtils.validate([
  {
    label: i18n.workplace.groupers.jobsite,
    classifier: ({ jobsite }) => jobsite.name,
    id: WorkplaceOrderGrouperId.JobsiteName,
    classificationLabel: (key, [{ jobsite }]) => jobsite.name,
  },
  {
    label: i18n.workplace.groupers.orderStatus,
    classifier: ({ status }) => status,
    id: WorkplaceOrderGrouperId.OrderStatus,
    classificationLabel: (key, [{ status }]) => status,
  },
  {
    label: i18n.workplace.groupers.requester,
    classifier: ({ requester }) => requester,
    id: WorkplaceOrderGrouperId.Requester,
    classificationLabel: (key, [{ requester }]) => requester,
  },
  {
    label: i18n.workplace.groupers.approver,
    classifier: ({ approver }) => approver,
    id: WorkplaceOrderGrouperId.Approver,
    classificationLabel: (key, [{ approver }]) => approver,
  },
  {
    label: i18n.workplace.groupers.orderedBy,
    classifier: ({ orderedBy }) => orderedBy,
    id: WorkplaceOrderGrouperId.OrderedBy,
    classificationLabel: (key, [{ orderedBy }]) => orderedBy,
  },
  {
    label: i18n.workplace.groupers.PO,
    classifier: ({ purchaseOrder }) => purchaseOrder,
    id: WorkplaceOrderGrouperId.PurchaseOrder,
    classificationLabel: (key, [{ purchaseOrder }]) => purchaseOrder,
  },
  {
    id: WorkplaceOrderGrouperId.None,
    label: i18n.workplace.groupers.none,
    classificationLabel: () => '',
    isDefault: true,
    classifier: () => '',
  },
]);
