import * as moment from 'moment';
import { MomentUtils } from '@/lib/moment.utils';
import i18n from '@/data/i18n';
import { StatusUtils } from '@/lib/statuses';
import { Address, Jobsite } from '@/lib/common-types';
import { TransactionType } from '@/lib/transactions';
import { WorkplaceEquipmentStatusId } from './equipment-statuses';
import {
  WorkplaceEquipment,
  WorkplaceRentalCounts,
  WorkplaceEquipmentCoordinates,
  WorkplaceEquipmentGpsUtilization,
} from './workplace-equipment.types';
import { WorkplaceEquipmentJobsiteLocation } from './equipment-maps';

export class WorkplaceEquipmentUtils {
  static isContract(eqp: WorkplaceEquipment) {
    return eqp.transType === TransactionType.Rental;
  }

  static isClosed(eqp: WorkplaceEquipment) {
    return eqp.transType === TransactionType.Closed;
  }

  static isReservation(eqp: WorkplaceEquipment) {
    return eqp.transType === TransactionType.Reservation;
  }

  static isQuote(eqp: WorkplaceEquipment) {
    return eqp.transType === TransactionType.Quote;
  }

  /**
   * For items with a status of PickupRequested OR AdvancedPickup, the quantity
   * is the number of items/pieces to be picked up for that specific pickup request.
   * AKA the equipment item's `quantityPickedUp` property.
   *
   * Otherwise, the quantity is the number of discrete pieces of equipment that
   * are currently OnRent (or will go OnRent for quotes and reservations).
   * AKA the equipment item's `quantityOnRent` property.
   */
  static getStatusSpecificQuantity(eqp: WorkplaceEquipment): number {
    return StatusUtils.statusListIncludesAnyOf([
      WorkplaceEquipmentStatusId.AdvancedPickup,
      WorkplaceEquipmentStatusId.PickupRequested,
    ], eqp.equipmentStatuses)
      ? eqp.quantityPickedUp || 0
      : eqp.quantityOnRent;
  }

  static getEmptyEquipmentCounts(): WorkplaceRentalCounts {
    return {
      quotes: 0,
      reservations: 0,
      onRent: 0,
      scheduledForPickup: 0,
      overdue: 0,
      allOpen: 0,
      closedContracts: 0,
      custOwn: false,
    };
  }

  static getEmptyEquipmentCoordinates(): WorkplaceEquipmentCoordinates {
    return {
      valid: false,
      latitude: null,
      longitude: null,
    };
  }

  static parseCoordinates(eqp: WorkplaceEquipment): WorkplaceEquipmentCoordinates {
    const latitude = parseFloat(eqp.latitude) || 0;
    const longitude = parseFloat(eqp.longitude) || 0;
    const valid = !isNaN(latitude) && !isNaN(longitude) && latitude !== 0 && longitude !== 0;
    return {
      valid,
      latitude: valid ? latitude : null,
      longitude: valid ? longitude : null,
    };
  }

  static formatJobsitePhone(eqp: WorkplaceEquipment): string | null {
    if (eqp.jobsite.jobPhone) {
      return eqp.jobsite.jobPhone;
    }
    return null;
  }

  static findLeniencyStatus(eqp: WorkplaceEquipment): string | null {
    if (eqp.leniencyStartDate && eqp.leniencyEndDate) {
      if (MomentUtils.happensTodayOrInThePast(eqp.leniencyStartDate) && MomentUtils.happensTodayOrInTheFuture(eqp.leniencyEndDate)) {
        return 'inLeniency';
      }
      if (MomentUtils.happensInTheFuture(eqp.leniencyStartDate)) {
        return 'futureLeniency';
      }
    }
    return null;
  }

  static calculateDaysOnRent(eqp: WorkplaceEquipment): number | null {
    /**
     * Ignore items that aren't `Rental` transactions
     */
    if (eqp.transType !== TransactionType.Rental) return null;

    /**
     * If `pickupDateTime` is populated and is in the past, use `pickupDateTime` as end time
     * Otherwise, use `now` as the end time
     */
    const usePickupDateTime =
      typeof eqp.pickupDateTime === 'string'
      && MomentUtils.happensInThePast(eqp.pickupDateTime, 'minutes');

    const start = moment(eqp.startDateTime);
    const end = usePickupDateTime
      ? moment(eqp.pickupDateTime!)
      : moment();

    const minutesOnRent = end.diff(start, 'minutes');
    return Math.max(0, Math.round(minutesOnRent / 1440));
  }

  static serializeJobsiteAddress(
    { address1, city, state }: Address,
  ): string {
    return `${address1}, ${city}, ${state}`;
  }

  static convertJobsiteIntoEquipmentLocation(jobsite: Jobsite): WorkplaceEquipmentJobsiteLocation {
    const { accountId, latitude, longitude } = jobsite;
    return {
      accountId,
      address: WorkplaceEquipmentUtils.serializeJobsiteAddress(jobsite),
      jobsite,
      lat: +latitude!, // cast to type number and assert the prop as non-null
      lng: +longitude!, // cast to type number and assert the prop as non-null
    };
  }

  static canRequestPickupAndExtendContract(eqp: WorkplaceEquipment): boolean {
    return !StatusUtils.statusListIncludesAnyOf(
      [ WorkplaceEquipmentStatusId.AdvancedPickup, WorkplaceEquipmentStatusId.PickupRequested],
      eqp.equipmentStatuses,
    )
    && !WorkplaceEquipmentUtils.isReservation(eqp)
    && !WorkplaceEquipmentUtils.isQuote(eqp);
  }

  static canEditAndConvertQuote(eqp: WorkplaceEquipment): boolean {
    return WorkplaceEquipmentUtils.isQuote(eqp);
  }

  static getGpsUtilizationLabel(utilization: WorkplaceEquipmentGpsUtilization): string {
    switch (utilization) {
      case WorkplaceEquipmentGpsUtilization.Offline:
      case WorkplaceEquipmentGpsUtilization.NoSignal:
        return i18n.workplace.gpsUtilization.noSignal;
      case WorkplaceEquipmentGpsUtilization.Overtime:
        return i18n.workplace.gpsUtilization.overtime;
      case WorkplaceEquipmentGpsUtilization.Idle:
        return i18n.workplace.gpsUtilization.idle;
      case WorkplaceEquipmentGpsUtilization.Active:
        return i18n.workplace.gpsUtilization.active;
      case WorkplaceEquipmentGpsUtilization.Online:
        return i18n.workplace.gpsUtilization.online;
      default:
        return i18n.workplace.noInfo;
    }
  }
}
