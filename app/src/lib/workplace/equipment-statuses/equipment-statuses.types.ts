import { WorkplaceEquipment } from '../workplace-equipment.types';
import { Status, StatusLabeler, StatusDescriptor } from '@/lib/statuses';

export type WorkplaceEquipmentStatusDescriptor = StatusDescriptor<
  WorkplaceEquipment,
  WorkplaceEquipmentStatusId,
  {
    color: WorkplaceEquipmentStatusColor;
    label?: string;
  }
>;

export type WorkplaceEquipmentStatus = Status<
  WorkplaceEquipmentStatusId,
  {
    color: WorkplaceEquipmentStatusColor;
    label?: string;
  }
>;

export type WorkplaceEquipmentStatusLabeler = StatusLabeler<
  WorkplaceEquipment,
  WorkplaceEquipmentStatusId
>;

export enum WorkplaceEquipmentStatusColor {
  BlueHighlight = '#008CFF',
  BrightRed = '#E10000',
  Grey = '#DDE0E2',
  Green = '#48BD00',
  OrangeHighlight = '#FFCF3D',
}

export enum WorkplaceEquipmentStatusId {
  AdvancedPickup = 'AdvancedPickup',
  AtJobsite = 'AtJobsite',
  Exchanged = 'Exchanged',
  ExchangeInProgress = 'ExchangeInProgress',
  ExchangeRequested = 'ExchangeRequested',
  Idle = 'Idle',
  InLeniency = 'InLeniency',
  OffRent = 'OffRent',
  OnRent = 'OnRent',
  Overdue = 'Overdue',
  Overtime = 'Overtime',
  DueSoon = 'DueSoon',
  PickupCanceled = 'PickupCanceled',
  PickupRequested = 'PickupRequested',
  Quote = 'Quote',
  ReservedForDelivery = 'ReservedForDelivery',
  ReservedForBranchPickup = 'ReservedForBranchPickup',
  ServiceInProgress = 'ServiceInProgress',
  ServiceRequested = 'ServiceRequested',
}
