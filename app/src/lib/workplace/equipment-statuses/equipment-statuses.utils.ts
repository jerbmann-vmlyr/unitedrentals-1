import i18n from '@/data/i18n';
import { DateFormat } from '@/lib/constants';
import { MomentUtils } from '@/lib/moment.utils';
import { StatusUtils } from '@/lib/statuses';
import { WorkplaceEquipmentStatusId, WorkplaceEquipmentStatusLabeler } from './equipment-statuses.types';
import { WorkplaceEquipment } from '../workplace-equipment.types';

export class WorkplaceEquipmentStatusUtils {

  static equipmentIsContract(eqp: WorkplaceEquipment) {
    return StatusUtils.statusListIncludesAnyOf(
      [
        WorkplaceEquipmentStatusId.OnRent,
        WorkplaceEquipmentStatusId.DueSoon,
        WorkplaceEquipmentStatusId.Overdue,
        WorkplaceEquipmentStatusId.InLeniency,
        WorkplaceEquipmentStatusId.PickupRequested,
        WorkplaceEquipmentStatusId.AdvancedPickup,
      ],
      eqp.equipmentStatuses,
    );
  }

  static equipmentIsQuote(eqp: WorkplaceEquipment) {
    return StatusUtils.statusListIncludesAnyOf(
      WorkplaceEquipmentStatusId.Quote,
      eqp.equipmentStatuses,
    );
  }

  static equipmentIsReservation(eqp: WorkplaceEquipment) {
    return StatusUtils.statusListIncludesAnyOf(
      [ WorkplaceEquipmentStatusId.ReservedForBranchPickup, WorkplaceEquipmentStatusId.ReservedForDelivery ],
      eqp.equipmentStatuses,
    );
  }

  static getOrderTypeLabel(item: WorkplaceEquipment) {
    if (WorkplaceEquipmentStatusUtils.equipmentIsContract(item)) return 'Contract';
    if (WorkplaceEquipmentStatusUtils.equipmentIsQuote(item)) return 'Quote';
    if (WorkplaceEquipmentStatusUtils.equipmentIsReservation(item)) return 'Reservation';
    return '';
  }

  static getStatusIdLabel(
    statusId: WorkplaceEquipmentStatusId,
  ): string {
    /**
     * TODO: Consolidate these into i18n (which is currently a mess)
     */
    switch (statusId) {
      case WorkplaceEquipmentStatusId.AdvancedPickup: return i18n.equipmentStatuses.AdvancedPickup;
      case WorkplaceEquipmentStatusId.AtJobsite: return i18n.equipmentStatuses.AtJobsite;
      case WorkplaceEquipmentStatusId.Exchanged: return i18n.equipmentStatuses.Exchanged;
      case WorkplaceEquipmentStatusId.ExchangeInProgress: return i18n.equipmentStatuses.ExchangeInProgress;
      case WorkplaceEquipmentStatusId.ExchangeRequested: return i18n.equipmentStatuses.ExchangeRequested;
      case WorkplaceEquipmentStatusId.Idle: return i18n.equipmentStatuses.Idle;
      case WorkplaceEquipmentStatusId.InLeniency: return i18n.equipmentStatuses.InLeniency;
      case WorkplaceEquipmentStatusId.OffRent: return i18n.equipmentStatuses.OffRent;
      case WorkplaceEquipmentStatusId.OnRent: return i18n.equipmentStatuses.OnRent;
      case WorkplaceEquipmentStatusId.Overdue: return i18n.equipmentStatuses.Overdue;
      case WorkplaceEquipmentStatusId.Overtime: return i18n.equipmentStatuses.Overtime;
      case WorkplaceEquipmentStatusId.DueSoon: return i18n.equipmentStatuses.DueSoon;
      case WorkplaceEquipmentStatusId.PickupCanceled: return i18n.equipmentStatuses.PickupCanceled;
      case WorkplaceEquipmentStatusId.PickupRequested: return i18n.equipmentStatuses.PickupRequested;
      case WorkplaceEquipmentStatusId.Quote: return i18n.workplace.quoted;
      case WorkplaceEquipmentStatusId.ReservedForDelivery: return i18n.equipmentStatuses.ReservedForDelivery;
      case WorkplaceEquipmentStatusId.ReservedForBranchPickup: return i18n.equipmentStatuses.ReservedForBranchPickup;
      case WorkplaceEquipmentStatusId.ServiceInProgress: return i18n.equipmentStatuses.ServiceInProgress;
      case WorkplaceEquipmentStatusId.ServiceRequested: return i18n.equipmentStatuses.ServiceRequested;
      default: return '¯\\_(ツ)_/¯';
    }
  }

  static getEquipmentStatusLabel: WorkplaceEquipmentStatusLabeler = (status, eqp) => {
    const statusId = typeof status === 'string' ? status : status.id;
    const slash = date => MomentUtils.formatAs(DateFormat.Slash, date);

    switch (statusId) {
      case WorkplaceEquipmentStatusId.AdvancedPickup:
        const pickupDateTime: string = eqp.pickupDateTime ? eqp.pickupDateTime : eqp.returnDateTime;
        return `
          ${i18n.workplace.advancedPickupOn}
          ${slash(pickupDateTime)}
          ${i18n.workplace.at}
          ${MomentUtils.formatAs(DateFormat.FriendlyHour, pickupDateTime)}
        `;

      case WorkplaceEquipmentStatusId.OnRent: return `${i18n.workplace.onRentLabel}: until ${slash(eqp.returnDateTime)}`;
      case WorkplaceEquipmentStatusId.DueSoon: return `${i18n.workplace.dueSoon}: on ${slash(eqp.returnDateTime)}`;
      case WorkplaceEquipmentStatusId.Quote: return `${i18n.workplace.quoted}: for ${slash(eqp.startDateTime)}`;
      case WorkplaceEquipmentStatusId.ReservedForBranchPickup: return `${i18n.equipmentStatuses.ReservedForBranchPickup}: ${i18n.workplace.estimatedArrival} ${slash(eqp.startDateTime)}`;
      case WorkplaceEquipmentStatusId.InLeniency: return `${i18n.workplace.inLeniency}: until ${slash(eqp.leniencyEndDate)}`;
      case WorkplaceEquipmentStatusId.OffRent: return `${i18n.workplace.offRent}: on ${slash(eqp.returnDateTime)}`;

      case WorkplaceEquipmentStatusId.Overdue: {
        // determine number of days the eqp is overdue
        const daysOverdue = Math.abs(MomentUtils.daysFromNow(eqp.returnDateTime));
        // if daysOverdue is 0 then return 1 else round up to next integer
        const daysOverdueWithMin = daysOverdue === 0
          ? 1
          : Math.ceil(daysOverdue);
        // string day or days depending on dayWithMin 1 or greater
        const dayOrDays = daysOverdueWithMin === 1
          ? i18n.workplace.day
          : i18n.workplace.days;
        return `${i18n.workplace.overdueBy} ${daysOverdueWithMin} ${dayOrDays}`;
      }

      case WorkplaceEquipmentStatusId.ReservedForDelivery:
        return `
          ${i18n.equipmentStatuses.ReservedForDelivery} ${i18n.workplace.estimatedArrival} ${slash(eqp.startDateTime)}
        `;

      case WorkplaceEquipmentStatusId.PickupRequested:
        return !eqp.pickupDateTime
          ? `Pickup Requested`
          : `${i18n.workplace.pickupRequestedFor} ${slash(eqp.pickupDateTime)} ${i18n.workplace.at}
            ${MomentUtils.formatAs(DateFormat.FriendlyHour, eqp.pickupDateTime)}`;

      case WorkplaceEquipmentStatusId.ServiceInProgress:
      case WorkplaceEquipmentStatusId.Overtime:
      case WorkplaceEquipmentStatusId.PickupCanceled:
      case WorkplaceEquipmentStatusId.AtJobsite:
      case WorkplaceEquipmentStatusId.Exchanged:
      case WorkplaceEquipmentStatusId.ExchangeInProgress:
      case WorkplaceEquipmentStatusId.ExchangeRequested:
      case WorkplaceEquipmentStatusId.Idle:
      case WorkplaceEquipmentStatusId.ServiceRequested:
      default: return '';
    }
  }

}
