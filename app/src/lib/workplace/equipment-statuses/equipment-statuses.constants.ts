import { WorkplaceEquipmentUtils } from '../workplace-equipment.utils';
import { MomentUtils } from '@/lib/moment.utils';
import { StatusPriority, StatusRule } from '@/lib/statuses';
import {
  WorkplaceEquipmentStatusDescriptor,
  WorkplaceEquipmentStatusId,
  WorkplaceEquipmentStatusColor,
} from './equipment-statuses.types';
import { WorkplaceEquipmentPickupType } from '../workplace-equipment.types';

export const WORKPLACE_EQUIPMENT_STATUSES: WorkplaceEquipmentStatusDescriptor[] = [
  /**
   * When I order it or when I request pickup I choose to have a
   * firm pickup time - no flexibility
   */
  {
    id: WorkplaceEquipmentStatusId.AdvancedPickup,
    color: WorkplaceEquipmentStatusColor.OrangeHighlight,
    priority: StatusPriority.Medium,
    isApplicable: eqp =>
      eqp.pickupType === WorkplaceEquipmentPickupType.Advanced
      && eqp.type === 6,
  },
  /**
   * Delivery truck is at the jobsite
   */
  {
    id: WorkplaceEquipmentStatusId.AtJobsite,
    color: WorkplaceEquipmentStatusColor.Green,
    priority: StatusPriority.Medium,
    isApplicable: eqp => false, // currently unsupported
  },
  /**
   * An equipment item has been exchanged
   */
  {
    id: WorkplaceEquipmentStatusId.Exchanged,
    color: WorkplaceEquipmentStatusColor.OrangeHighlight,
    priority: StatusPriority.Medium,
    isApplicable: eqp => false, // currently unsupported
  },
  /**
   * An equipment item is being exchanged
   */
  {
    id: WorkplaceEquipmentStatusId.ExchangeInProgress,
    color: WorkplaceEquipmentStatusColor.OrangeHighlight,
    priority: StatusPriority.Medium,
    isApplicable: eqp => false, // currently unsupported
  },
  /**
   * An equipment item is not working and needs to be exchanged
   */
  {
    id: WorkplaceEquipmentStatusId.ExchangeRequested,
    color: WorkplaceEquipmentStatusColor.OrangeHighlight,
    priority: StatusPriority.Medium,
    isApplicable: eqp => false, // currently unsupported
  },
  /**
   * GPS-enabled equipment that hasn't run in 72 hours, excluding weekends
   */
  {
    id: WorkplaceEquipmentStatusId.Idle,
    color: WorkplaceEquipmentStatusColor.BrightRed,
    priority: StatusPriority.High,
    isApplicable: eqp => false, // currently unsupported
  },
  /**
   * An equipment item goes into a Leniency period of not being charged extra
   */
  {
    id: WorkplaceEquipmentStatusId.InLeniency,
    color: WorkplaceEquipmentStatusColor.Green,
    priority: StatusPriority.Medium,
    isApplicable: eqp =>
      MomentUtils.happeningNow(eqp.leniencyStartDate, eqp.leniencyEndDate)
      && WorkplaceEquipmentUtils.isContract(eqp),
  },
  /**
   * The equipment item has been returned
   */
  {
    id: WorkplaceEquipmentStatusId.OffRent,
    color: WorkplaceEquipmentStatusColor.Grey,
    priority: StatusPriority.Low,
    isApplicable: eqp => WorkplaceEquipmentUtils.isClosed(eqp),
  },
  /**
   * Equipment has been delivered to the jobsite and the contract has
   * been handed to the jobsite
   */
  {
    id: WorkplaceEquipmentStatusId.OnRent,
    color: WorkplaceEquipmentStatusColor.Grey,
    priority: StatusPriority.Low,
    isApplicable: eqp => eqp.type === 1,
  },
  /**
   * Equipment kept passed the end date
   */
  {
    id: WorkplaceEquipmentStatusId.Overdue,
    color: WorkplaceEquipmentStatusColor.BrightRed,
    priority: StatusPriority.High,
    isApplicable: eqp =>
      MomentUtils.happensInThePast(eqp.returnDateTime)
      && WorkplaceEquipmentUtils.isContract(eqp),
  },
  /**
   * GPS-enabled equipment that has run for more than 8 hours in a day
   */
  {
    id: WorkplaceEquipmentStatusId.Overtime,
    color: WorkplaceEquipmentStatusColor.BrightRed,
    priority: StatusPriority.High,
    isApplicable: eqp => false, // currently unsupported
  },
  /**
   * Delivery is scheduled for that date, but delivery has not started yet
   */
  {
    id: WorkplaceEquipmentStatusId.ReservedForDelivery,
    color: WorkplaceEquipmentStatusColor.Grey,
    priority: StatusPriority.Medium,
    isApplicable: eqp => WorkplaceEquipmentUtils.isReservation(eqp) && eqp.urDeliver,
  },
  /**
   * 	Equipment within 3 days of its end date
   */
  {
    id: WorkplaceEquipmentStatusId.DueSoon,
    color: WorkplaceEquipmentStatusColor.OrangeHighlight,
    priority: StatusPriority.Medium,
    isApplicable: eqp =>
      WorkplaceEquipmentUtils.isContract(eqp)
      && MomentUtils.happensWithinThreeDays(eqp.returnDateTime),
  },
  /**
   * An equipment item no longer needs to be picked up
   */
  {
    id: WorkplaceEquipmentStatusId.PickupCanceled,
    color: WorkplaceEquipmentStatusColor.BrightRed,
    priority: StatusPriority.High,
    isApplicable: eqp => false, // currently unsupported
  },
  /**
   * An equipment item needing pick up (reached its end date,
   * budget has changed, utilization is low etc)
   */
  {
    id: WorkplaceEquipmentStatusId.PickupRequested,
    color: WorkplaceEquipmentStatusColor.OrangeHighlight,
    priority: StatusPriority.Medium,
    isApplicable: eqp =>
      eqp.pickupType === WorkplaceEquipmentPickupType.Scheduled
      && eqp.type === 6,
  },
  /**
   * Order is in Quote status.
   */
  {
    id: WorkplaceEquipmentStatusId.Quote,
    color: WorkplaceEquipmentStatusColor.Grey,
    priority: StatusPriority.Low,
    isApplicable: eqp => WorkplaceEquipmentUtils.isQuote(eqp),
  },
  /**
   * Service has been requested and is currently in progress
   */
  {
    id: WorkplaceEquipmentStatusId.ServiceInProgress,
    color: WorkplaceEquipmentStatusColor.OrangeHighlight,
    priority: StatusPriority.Medium,
    isApplicable: eqp => false, // currently unsupported
  },
  /**
   * An equipment item that malfunctions and needs service
   */
  {
    id: WorkplaceEquipmentStatusId.ServiceRequested,
    color: WorkplaceEquipmentStatusColor.BrightRed,
    priority: StatusPriority.High,
    isApplicable: eqp => false, // currently unsupported
  },
  /**
   * Equipment is ready for pickup at the designated branch the
   * customer will pickup
   */
  {
    id: WorkplaceEquipmentStatusId.ReservedForBranchPickup,
    color: WorkplaceEquipmentStatusColor.Grey,
    priority: StatusPriority.Medium,
    isApplicable: eqp => WorkplaceEquipmentUtils.isReservation(eqp) && !eqp.urDeliver,
  },
];

export const workplaceEquipmentStatusRules: StatusRule[] = [
  // if the statuses array contains Pickup Requested then filter out Pending Due
  // and Overdue statuses
  statuses => {
    // 1. check for Pickup Requested
    const containsPickupStatus = statuses.some(({ id }) =>
      id === WorkplaceEquipmentStatusId.PickupRequested,
    );
    // 2. if step 1. is true filter Pending Due and Overdue otherwise return the status object
    return containsPickupStatus
      ? statuses.filter(({ id }) =>
          id !== WorkplaceEquipmentStatusId.DueSoon
          && id !== WorkplaceEquipmentStatusId.Overdue,
        )
      : statuses;
  },

  // inLeniency should display as the second status if a second Medium priority status exists in the array
  // this method looks for inLeniency and if it's in index position 0 moves it to index position 1
  statuses => {
    const indexOfInLeniency = statuses.findIndex(({id}) =>
      id === WorkplaceEquipmentStatusId.InLeniency,
    );

    if (indexOfInLeniency > 0) {
      return statuses;
    }
    const moveFirstToSecond = ([first, second, ...rest]: typeof statuses) => [second, first, ...rest];
    return indexOfInLeniency === 0 && statuses.length > 1
      ? moveFirstToSecond(statuses)
      : statuses;
  },
  // AdvancedPickup should display as the highest priority status if it's already overdue
  statuses => {
    const hasAdvancedPickup = statuses.some(({id}) =>
      id === WorkplaceEquipmentStatusId.AdvancedPickup,
    );

    const hasOverdue = statuses.some(({id}) =>
      id === WorkplaceEquipmentStatusId.Overdue,
    );

    // Must have both statuses before we change array index order.
    if (hasAdvancedPickup && hasOverdue) {
      const indexOfAdvancedPickup = statuses.findIndex(({id}) =>
        id === WorkplaceEquipmentStatusId.AdvancedPickup,
      );

      const moveFirstToSecond = ([first, second, ...rest]: typeof statuses) => [second, first, ...rest];

      return indexOfAdvancedPickup === 1 && statuses.length > 1
        ? moveFirstToSecond(statuses)
        : statuses;
    }

    return statuses;
  },
];
