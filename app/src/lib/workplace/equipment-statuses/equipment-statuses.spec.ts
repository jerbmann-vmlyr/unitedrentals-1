import { WorkplaceEquipmentStatusId } from './equipment-statuses.types';
import { WORKPLACE_EQUIPMENT_STATUSES } from './equipment-statuses.constants';

describe('Workplace equipment statuses', () => {
  test('all status IDs are unique', () => {
    const ids = WORKPLACE_EQUIPMENT_STATUSES.map(sorter => sorter.id);
    const uniqueIds = new Set(ids);
    expect(ids.length).toEqual(uniqueIds.size);
  });

  test('all defined sort IDs are used', () => {
    const definedIds = Object.values(WorkplaceEquipmentStatusId);
    const usedIds = WORKPLACE_EQUIPMENT_STATUSES.map(grouper => grouper.id);
    definedIds.forEach(id => {
      expect(usedIds).toContain(id);
    });
  });
});
