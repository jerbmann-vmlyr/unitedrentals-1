// import { WorkplaceOrderStatusId } from './order-statuses.types';
import { WORKPLACE_ORDER_STATUSES } from './order-statuses.constants';

describe('Workplace orders statuses', () => {
  test('all status IDs are unique', () => {
    const ids = WORKPLACE_ORDER_STATUSES.map(sorter => sorter.id);
    const uniqueIds = new Set(ids);
    expect(ids.length).toEqual(uniqueIds.size);
  });

  // test('all defined sort IDs are used', () => {
  //   const definedIds = Object.values(WorkplaceOrderStatusId);
  //   const usedIds = WORKPLACE_ORDER_STATUSES.map(grouper => grouper.id);
  //   definedIds.forEach(id => {
  //     expect(usedIds).toContain(id);
  //   });
  // });
});
