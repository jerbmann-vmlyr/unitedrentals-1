import { SORT_ORDER_STATUSES, WorkplaceOrder } from '@/lib/workplace';

export class WorkplaceOrderStatusUtils {
  static transform(item: WorkplaceOrder) {
    // 1. EquipmentStatus To OrderStatus
    // 2. OrderStatus To OrderType
    // https://unitedrentals.atlassian.net/wiki/spaces/KD/pages/644513793/Status+Action+Taxonomy+1.0
    const transformer = {
      status: {
        PickupRequested: 'Open',
        AdvancedPickup: 'Open',
        InLeniency: 'Open',
        NotYetInLeniency: 'Open',
        LeniencyRatesText: 'Open',
        OnRent: 'Open',
        Quote: 'Quoted',
        Quoted: 'Quoted',
        QuoteExpiresSoon: 'Quote Expiring Soon',
        QuoteExpiringSoon: 'Quote Expiring Soon',
        ReservedForBranchPickup: 'Reserved For Branch Pickup',
        ReservedForDelivery: 'Reserved For Delivery',
        Open: 'Open',
        DueSoon: 'Due Soon',
        Overdue: 'Overdue',
      },
      type: {
        Quoted: 'Quote',
        QuoteExpiredSoon: 'Quote',
        QuoteExpiringSoon: 'Quote',
        'Quote Expiring Soon': 'Quote',
        ReservedForBranchPickup: 'Reservation',
        ReservedForDelivery: 'Reservation',
        Open: 'Contract',
        DueSoon: 'Contract',
        Overdue: 'Contract',
        'Due Soon': 'Contract',
        'Pickup Requested': 'Contract',
        'Reserved For Delivery': 'Reservation',
        'Reserved For Branch Pickup': 'Reservation',
      },
    };
    const itemWithStatus = {
      ...item,
      statuses: [...item.orderStatuses]
        .map(({ id }) => transformer.status[id] || id)
        .filter(s => typeof s !== undefined)
        .reduce((list, item) => list.includes(item) ? [...list] : [...list, item], [])
        .sort(SORT_ORDER_STATUSES),
    };
    return {
      ...itemWithStatus,
      status: itemWithStatus.statuses[0],
      orderType: transformer.type[itemWithStatus.statuses[0]] || `${itemWithStatus.statuses[0]} not found`,
    };
  }
}
