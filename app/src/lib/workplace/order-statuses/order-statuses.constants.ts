import { StatusRule } from '@/lib/statuses';
import { WorkplaceOrderStatusDescriptor } from './order-statuses.types';
import { WorkplaceOrderStatusName } from '@/lib/workplace/workplace-order.types';

export const ORDER_STATUS_PRIORITY = [
  WorkplaceOrderStatusName.Overdue,
  WorkplaceOrderStatusName.DueSoon,
  WorkplaceOrderStatusName.Open,
  WorkplaceOrderStatusName.ReservedForBranchPickup,
  WorkplaceOrderStatusName.ReservedForDelivery,
  WorkplaceOrderStatusName.Quoted,
];

export const SORT_ORDER_STATUSES = (first: WorkplaceOrderStatusName, next: WorkplaceOrderStatusName) => {
  const firstPriority = ORDER_STATUS_PRIORITY.indexOf(first);
  const nextPriority = ORDER_STATUS_PRIORITY.indexOf(next);

  return firstPriority >= nextPriority ? 1 : -1;
};

// order array of StatusDescriptor Objects in order of given status priority (top = lowest priority)
export const WORKPLACE_ORDER_STATUSES: WorkplaceOrderStatusDescriptor[] = [];

export const WORKPLACE_ORDER_STATUS_RULES: StatusRule[] = [];
