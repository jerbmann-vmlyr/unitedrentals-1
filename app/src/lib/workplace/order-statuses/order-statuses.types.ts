import { WorkplaceOrder } from '../workplace-order.types';
import { Status, StatusLabeler, StatusDescriptor } from '@/lib/statuses';

export type WorkplaceOrderStatusDescriptor = StatusDescriptor<
  WorkplaceOrder,
  WorkplaceOrderStatusId,
  {
    color: WorkplaceOrderStatusColor;
    label?: string;
  }
>;

export type WorkplaceOrderStatus = Status<
  WorkplaceOrderStatusId,
  {
    color: WorkplaceOrderStatusColor;
    label?: string;
  }
>;

export type WorkplaceOrderStatusLabeler = StatusLabeler<
  WorkplaceOrder,
  WorkplaceOrderStatusId
>;

/**
 *  Status Colors
 * ~~~~~~~~~~~~~~~~~~~~~
 * 1. Reserved: Grey
 * 2. Quoted: Grey
 * 3. Contract: Grey
 * 4. Pickup Requested: OrangeHighlight
 * 5. Advanced Pickup: OrangeHighlight
 * 6. InLeniency: Green
 * 7. NotYetInLeniency: Grey
 * 8. DueSoon: OrangeHighlight
 * 9. Overdue: BrightRed
 * 10. QuoteExpiresSoon: BrightRed
 */
export enum WorkplaceOrderStatusColor {
  BlueHighlight = '#008CFF',
  BrightRed = '#E10000',
  Grey = '#DDE0E2',
  Green = '#48BD00',
  OrangeHighlight = '#FFCF3D',
}

export enum WorkplaceOrderStatusId {
  /* Quote */
  Quoted = 'Quoted',
  QuoteExpiringSoon = 'Quote Expiring Soon',
  /* Reservation */
  ReservedForBranchPickup = 'Reserved For Branch Pickup',
  ReservedForDelivery = 'Reserved For Delivery',
  /* Contract */
  Open = 'Open',
  DueSoon = 'Due Soon',
  Overdue = 'Overdue',
}
