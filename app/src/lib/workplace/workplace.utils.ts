import { ListingColumnSettings, ListingColumnLabel } from './workplace.types';

type AnyColumnSettings = ListingColumnSettings<any, any, any>;

type PreprocessedListingColumnSettings
  = Optional<'visible' | 'locked' | 'sortable' | 'pinned' | 'index', Omit<'label', AnyColumnSettings>>
  & { label: string | ListingColumnLabel };

export class WorkplaceUtils {
  static createColumn(
    col: PreprocessedListingColumnSettings,
    index: number,
  ): AnyColumnSettings {
    const label = typeof col.label === 'string'
      ? { short: col.label, long: col.label }
      : col.label!;

    const visible = col.visible || col.locked || false;
    const sortable = col.sortable || false;
    const pinned = col.pinned || false;

    return {
      columnKey: col.columnKey!,
      locked: false,
      ...col,
      index,
      label,
      pinned,
      sortable,
      visible,
    };
  }

  static columnsFrom(cols: PreprocessedListingColumnSettings[]): AnyColumnSettings[] {
    return cols.map(WorkplaceUtils.createColumn);
  }

  static createColumnPropertyMap<K extends keyof AnyColumnSettings>(
    prop: K,
    columns: AnyColumnSettings[],
  ): Record<string, AnyColumnSettings[K]> {
    return columns.reduce(
      (map, column) => {
        return {
          ...map,
          [column.columnKey]: column[prop],
        };
      },
      {} as Record<string, AnyColumnSettings[K]>,
    );
  }
}
