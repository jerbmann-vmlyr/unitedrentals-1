export enum StatusPriority {
  Low,
  Medium,
  High,
}

/**
 * Meaning of the generic types:
 *
 * `T` (Type):
 * The "main" object type we're giving a status to.
 * e.g. For `WorkplaceEquipmentStatus`, `T` is of type `WorkplaceEquipment`.
 *
 * `S` (Status ID):
 * Generally an enum of string values, but can be just a string in a pinch.
 *
 * `E` (Extension):
 * Use this for attaching arbitrary properties to the `Status` type.
 * e.g. The `WorkplaceEquipmentStatus` type extends the `Status` type with a
 * `color` property and an optional `label` property.
 *
 */
export type Status<S extends string, E extends object = {}> = E & {
  id: S;
  priority?: StatusPriority;
};

export type StatusDescriptor<T, S extends string, E extends object = {}> = Status<S, E> & {
  isApplicable: <I extends T>(item: I) => boolean;
};

export type StatusRule = <S extends string, E extends object = {}>(statuses: Status<S, E>[]) => Status<S, E>[];

export type StatusLabeler<T, S extends string> = (status: S | StatusDescriptor<T, S>, item: T) => string;
