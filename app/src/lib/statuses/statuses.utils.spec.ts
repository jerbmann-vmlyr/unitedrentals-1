import { Status, StatusDescriptor, StatusPriority, StatusRule } from './statuses.types';
import { StatusUtils } from './statuses.utils';

enum ColorName {
  Black = 'Black' ,
  DeepPink = 'Deep Pink',
  Red = 'Red',
  TransparentBlack = 'Transparent Black',
  TransparentBlue = 'Transparent Blue',
  TransparentWhite = 'Transparent White',
  Turquoise = 'Turquoise' ,
  White = 'White',
}

interface Color {
  name: ColorName;
  r: number;
  g: number;
  b: number;
  a: number;
}

export enum ColorStatusId {
  Reddish = 'Reddish',
  Blueish = 'Blueish',
  Opaque = 'Opaque',
  ShadeOfGray = 'ShadeOfGray',
  Transparent = 'Transparent',
}

type ColorStatus = Status<ColorStatusId>;
type ColorStatusDescriptor = StatusDescriptor<Color, ColorStatusId>;

interface ColorWithStatuses extends Color {
  statuses: ColorStatus[];
}

const colorStatuses: ColorStatusDescriptor[] = [
  {
    id: ColorStatusId.Opaque,
    priority: StatusPriority.Medium,
    isApplicable: color => color.a >= 1,
  },
  {
    id: ColorStatusId.Transparent,
    priority: StatusPriority.High,
    isApplicable: color => color.a < 1,
  },
  {
    id: ColorStatusId.Reddish,
    isApplicable: ({ r, g, b }) => r / 2 > g && r / 2 > b,
  },
  {
    id: ColorStatusId.Blueish,
    isApplicable: ({ r, g, b }) => b / 2 > r && b / 2 > g,
  },
  {
    id: ColorStatusId.ShadeOfGray,
    isApplicable: ({ r, g, b }) => r === g && g === b,
  },
];

const maxOneStatus: StatusRule = statuses => {
  return statuses.slice(0, 1);
};


const colors: Color[] = [
  { r: 0x00, g: 0x00, b: 0x00, a: 1, name: ColorName.Black },
  { r: 0xFF, g: 0x69, b: 0xB4, a: 1, name: ColorName.DeepPink },
  { r: 0xFF, g: 0x00, b: 0x00, a: 1, name: ColorName.Red },
  { r: 0x00, g: 0x00, b: 0x00, a: 0.5, name: ColorName.TransparentBlack },
  { r: 0x00, g: 0x00, b: 0xFF, a: 0.5, name: ColorName.TransparentBlue },
  { r: 0xFF, g: 0xFF, b: 0xFF, a: 0.5, name: ColorName.TransparentWhite },
  { r: 0x40, g: 0xE0, b: 0xD0, a: 1, name: ColorName.Turquoise },
  { r: 0xFF, g: 0xFF, b: 0xFF, a: 1, name: ColorName.White },
];



describe('Generic statuses', () => {

  describe('priority comparator', () => {
    test('can sort a list of statuses by priority', () => {
      const sortedStatusIds = [...colorStatuses]
        .sort(StatusUtils.priorityComparator)
        .map(({ id }) => id);

      expect(sortedStatusIds).toEqual([
        ColorStatusId.Transparent,
        ColorStatusId.Opaque,
        ColorStatusId.Reddish,
        ColorStatusId.Blueish,
        ColorStatusId.ShadeOfGray,
      ]);
    });
  });

  const colorsWithStatuses: ColorWithStatuses[] = colors.map(color => ({
    ...color,
    statuses: StatusUtils.getApplicableStatuses(colorStatuses, color),
  }));

  describe('getting applicable statuses', () => {
    test('getApplicableStatuses returns a list of color status objects', () => {
      colorsWithStatuses.forEach(color => {
        expect(Array.isArray(color.statuses)).toBe(true);

        color.statuses.forEach(status => {
          expect(Object.values(ColorStatusId)).toContain(status.id);
        });
      });
    });
  });

  describe('checking status existence', () => {
    test('statusListIncludesAnyOf can check a list of statuses for any specified status IDs', () => {
      const black = colorsWithStatuses.find(c => c.name === ColorName.TransparentBlack)!;
      expect(StatusUtils.statusListIncludesAnyOf(ColorStatusId.Transparent, black.statuses)).toBe(true);
      expect(StatusUtils.statusListIncludesAnyOf(ColorStatusId.ShadeOfGray, black.statuses)).toBe(true);
      expect(StatusUtils.statusListIncludesAnyOf(ColorStatusId.Reddish, black.statuses)).toBe(false);
      expect(StatusUtils.statusListIncludesAnyOf(
        [ColorStatusId.ShadeOfGray, ColorStatusId.Reddish],
        black.statuses,
      )).toBe(true);
    });

    test('statusListIncludesAllOf can check a list of statuses for all specified status IDs', () => {
      const black = colorsWithStatuses.find(c => c.name === ColorName.TransparentBlack)!;
      expect(StatusUtils.statusListIncludesAllOf(
        ColorStatusId.ShadeOfGray,
        black.statuses,
      )).toBe(true);
      expect(StatusUtils.statusListIncludesAllOf(
        [ColorStatusId.ShadeOfGray, ColorStatusId.Transparent],
        black.statuses,
      )).toBe(true);
      expect(StatusUtils.statusListIncludesAllOf(
        [ColorStatusId.ShadeOfGray, ColorStatusId.Reddish],
        black.statuses,
      )).toBe(false);
    });

    test('firstIncludedStatusOrNull can check a list of statuses and get the first status found from another list of statuses', () => {
      const black = colorsWithStatuses.find(c => c.name === ColorName.TransparentBlack)!;
      expect(
        StatusUtils.firstIncludedStatusOrNull(
          [ColorStatusId.Transparent, ColorStatusId.ShadeOfGray],
          black.statuses,
        ),
      ).toBe(ColorStatusId.Transparent);

      expect(
        StatusUtils.firstIncludedStatusOrNull(
          [ColorStatusId.ShadeOfGray, ColorStatusId.Transparent],
          black.statuses,
        ),
      ).toBe(ColorStatusId.Transparent);

      expect(
        StatusUtils.firstIncludedStatusOrNull(
          [ColorStatusId.Reddish, ColorStatusId.Blueish],
          black.statuses,
        ),
      ).toBeNull();

      expect(StatusUtils.firstIncludedStatusOrNull([], [])).toBeNull();
    });
  });

  describe('selecting items by statuses', () => {
    test('selectItemsWithStatuses returns a list of items that have any of the specified statuses', () => {
      const selected = StatusUtils.selectItemsWithStatuses(
        [ColorStatusId.Transparent, ColorStatusId.ShadeOfGray],
        color => color.statuses,
        colorsWithStatuses,
      );

      const selectedColorNames = selected.map(({ name }) => name);
      expect(selectedColorNames).toContain(ColorName.Black);
      expect(selectedColorNames).toContain(ColorName.White);
      expect(selectedColorNames).toContain(ColorName.TransparentBlack);
      expect(selectedColorNames).toContain(ColorName.TransparentWhite);
      expect(selectedColorNames).not.toContain(ColorName.Red);
    });
  });

  describe('grouping items by statuses', () => {
    const groups = StatusUtils.groupByStatuses(
      status => status.statuses,
      colorsWithStatuses,
    );

    test('groupByStatuses returns an object whose keys are status IDs and values are lists of items', () => {
      Object.keys(groups).forEach(key => {
        expect(Object.values(ColorStatusId)).toContain(key);
      });

      Object.values(groups).forEach(value => {
        expect(Array.isArray(value)).toBe(true);
      });
    });

    test('groupByStatuses properly groups the items it receives', () => {
      const transparentColorNames = groups[ColorStatusId.Transparent]!.map(({ name }) => name);
      expect(transparentColorNames).toContain(ColorName.TransparentBlack);
      expect(transparentColorNames).toContain(ColorName.TransparentBlue);
      expect(transparentColorNames).toContain(ColorName.TransparentWhite);
      expect(transparentColorNames).not.toContain(ColorName.Red);
    });

    test('items with multiple statuses belong to multiple groups', () => {
      const transparentColorNames = groups[ColorStatusId.Transparent]!.map(({ name }) => name);
      const shadesOfGrayColorNames = groups[ColorStatusId.ShadeOfGray]!.map(({ name }) => name);

      expect(transparentColorNames).toContain(ColorName.TransparentWhite);
      expect(shadesOfGrayColorNames).toContain(ColorName.TransparentWhite);
      expect(transparentColorNames).toContain(ColorName.TransparentBlack);
      expect(shadesOfGrayColorNames).toContain(ColorName.TransparentBlack);
      expect(transparentColorNames).toContain(ColorName.TransparentBlue);
      expect(shadesOfGrayColorNames).not.toContain(ColorName.TransparentBlue);
      expect(transparentColorNames).not.toContain(ColorName.Red);
      expect(shadesOfGrayColorNames).not.toContain(ColorName.Red);
    });
  });

  describe('status rules', () => {
    test('getApplicableStatuses applies a max one status rule', () => {
      colors
        .map(color => StatusUtils.getApplicableStatuses(colorStatuses, color, [maxOneStatus]))
        .forEach(statuses => {
          expect(statuses.length <= 1).toBe(true);
        });
    });
  });
});
