import { Status, StatusDescriptor, StatusRule, StatusPriority } from '@/lib/statuses';

export class StatusUtils {
  static statusListIncludesAnyOf<S extends string>(
    targetStatuses: S | S[],
    statusArray: Status<S>[],
  ): boolean {
    const statusesToCheckFor = Array.isArray(targetStatuses)
      ? targetStatuses
      : [ targetStatuses ];

    if (!Array.isArray(statusArray)) return false;
    return statusArray.some(status => statusesToCheckFor.includes(status.id));
  }

  static statusListIncludesAllOf<S extends string>(
    targetStatuses: S | S[],
    statusArray: Status<S>[],
  ): boolean {
    const statusesToCheckFor = Array.isArray(targetStatuses)
      ? targetStatuses
      : [ targetStatuses ];

    const givenStatusIds = statusArray.map(({ id }) => id);
    return statusesToCheckFor.every(status => givenStatusIds.includes(status));
  }

  static firstIncludedStatusOrNull<S extends string = string>(
    statusIdsToCheckFor: S[],
    statusArray: Status<S>[],
  ): S | null {
    const match = [...statusArray]
      .sort(StatusUtils.priorityComparator)
      .find(s => statusIdsToCheckFor.includes(s.id));

    return (match && match.id) || null;
  }

  static priorityComparator<S extends string = string>(
    { priority: p1 = StatusPriority.Low }: Status<S>,
    { priority: p2 = StatusPriority.Low }: Status<S>,
  ): number {
    if (p1 === p2) return 0;
    else return p2 < p1 ? -1 : 1;
  }

  private static applyRules<S extends string = string, E extends object = {}>(
    rules: StatusRule[],
    statuses: Status<S, E>[],
  ): Status<S, E>[] {
    return rules.reduce((result, rule) => rule(result), statuses);
  }

  private static makeStatusSerializable<T, S extends string = string, E extends object = {}>(
    descriptor: StatusDescriptor<T, S, E>,
  ): Status<S, E> {
    /**
     * HACK:
     * A TS bug means we can't destruct the descriptor and remove the isApplicable property
     * Instead, we have to break up the object and reassemble it using entries-->reduce
     */
    return Object.entries(descriptor)
      .filter(([k]) => k !== 'isApplicable')
      .reduce((status, [k, v]) => ({ ...status, [k]: v }), {} as any);
  }

  static getApplicableStatuses<T, S extends string = string, E extends object = {}>(
    statuses: StatusDescriptor<T, S, E>[],
    item: T,
    rules: StatusRule[] = [],
  ): Status<S, E>[] {
    const filteredStatuses = [...statuses]
      .filter(({ isApplicable }) => isApplicable(item))
      .sort(StatusUtils.priorityComparator)
      .map(StatusUtils.makeStatusSerializable);

    return StatusUtils.applyRules(rules, filteredStatuses);
  }

  static selectItemsWithStatuses<T, S extends string = string>(
    statuses: S[],
    getter: (item: T) => Status<S>[],
    items: T[],
  ): T[] {
    return items.filter(item =>
      getter(item).some(({ id }) => statuses.includes(id)),
    );
  }

  /**
   * Transforms the list of items of type T into an object, wherein each
   * property key is a variant of S (an enum value), and whose value is a list
   * of items that have a matching S in their statuses list (got using `getter`).
   *
   * NOTE: Items that don't have any statuses will not be included in any of the
   * generated object's lists.
   *
   * NOTE: The generated lists are NOT mutually exclusive. That is, if a single
   * item has more than one status, it will be included in more than one list.
   */
  static groupByStatuses<T, S extends string>(
    getter: (item: T) => Status<S>[],
    items: T[],
  ): { [K in S]?: T[] } {
    return items.reduce(
      (groups, item) =>
        getter(item).reduce(
          (regroups, { id }) => {
            return {
              ...regroups,
              [id]: [...(regroups[id] || []), item],
            };
          },
          groups as { [K in S]: T[] },
        ),
      {},
    );
  }
}
