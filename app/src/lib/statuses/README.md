This section of lib is designated for **generic** statuses applied sitewide, particularly workplace.

Different data entities (transactions, equipment, etc) are organized by a relative complex ruleset of [status taxonomy](https://unitedrentals.atlassian.net/wiki/spaces/KD/pages/28278821/Status+Taxonomy).

The goal is to provide a unified approach and API to deal with complex status logic and rules.
