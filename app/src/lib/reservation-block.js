import moment from 'moment';
import { dateFormat } from '@/utils';
import { isEmpty, get } from 'lodash';

export default class ReservationBlock {
  constructor(items, rootState) {
    // Null block
    if (!items) {
      return this;
    }

    const item = items[0];

    this.locationString = item.locationString;
    this.accountId = item.accountId;
    this.branchId = item.branchId;
    this.delivery = item.delivery;
    this.items = items;
    this.pickupFirm = item.pickupFirm ? item.pickupFirm : null;
    this.id = makeUid(this.items);
    this.pickup = item.pickup;
    this.jobsiteId = item.jobsiteId;
    this.startMoment = moment(item.startDate);
    this.endMoment = moment(item.endDate);
    this.requesterId = item.requesterId;

    this.poNumber = '';
    this.subtitle = _createSubtitle(this.items, this.accountId, rootState);

    this.rpp = _initialRppValue(this.accountId, rootState, this.id);
    this.promotionCode = get(rootState, ['checkout', 'billing', this.id, 'promotionCode'], null);
  }

  exportEstimateAPIParams(override = {}) {
    const {
      accountId, items, branchId, delivery, jobsiteId, pickup, startMoment, endMoment, rpp, promotionCode,
    } = { ...this, ...override };

    const formattedItems = items.reduce((items, { catClass, quantity }) => {
      items[catClass] = {
        quantity,
      };
      return items;
    }, {});

    const jobsiteLocationParams = accountId && jobsiteId
      ? ({ accountId, jobsiteId })
      : this.items[0]._locationStringToObject();

    return {
      branchId,
      delivery,
      pickup,
      items: formattedItems,
      startDate: startMoment.format(dateFormat.iso),
      returnDate: endMoment.format(dateFormat.iso),
      rpp,
      promotionCode,
      ...jobsiteLocationParams,
    };
  }

  // Updates the promotion code value
  updatePromotionCode(promotionCode) {
    this.promotionCode = promotionCode;
  }
}

const _createSubtitle = (items, accountId, rootState) => {
  const account = rootState.accounts.all[accountId];
  const count = items
    .map(({ quantity }) => quantity)
    .reduce((acc, iVal) => acc + iVal);

  const subject = Drupal.formatPlural(count, '1 item', '@count items');
  const predicate = isEmpty(account) ? items.locationString : `${account.name} ${account.id}`;

  return { subject, predicate };
};

// Sets the block's initial RPP value,
// which is False for user's w/o an account OR
// If the account's rppDefault is Y the block's rpp value is TRUE,
// and if the rppDefault is C or N, it's FALSE.
const _initialRppValue = (accountId, rootState, reservationBlockId) => {
  const userSelection = get(rootState.checkout.billing, [reservationBlockId, 'rpp']);
  const account = rootState.accounts.all[accountId];

  if (typeof userSelection !== 'undefined') {
    return userSelection;
  }

  return isEmpty(account) ? false : account.rppDefault === 'Y';
};

// Creates a UID as the concatenation of all item id's in the block, dash delimited for developer
// readability. The UID is what links a Reservation Block to its Billing Info & its Estimate.
// By computing the UID off a subset of the facets that constitute a block, we retain this linkage
// as long as possible.  Only when items change is the linkage to a block's billing info / estimate
// lost. https://unitedrentals.atlassian.net/browse/KV-513
const makeUid = items => items.map(({ id }) => id).join('-');

export { makeUid };
