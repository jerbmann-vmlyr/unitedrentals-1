export interface BusinessType {
    tid: string
    name: string,
    typeMap: string,
    url: string,
    contactUsLink: string,
    showContactUs: boolean,
    rentOnlineLink: string,
    showRentOnline: boolean
}
