export interface CatalogEquipmentImage {
  link: string;
  alt: string;
}

export interface CatalogEquipment {
  catClass: string;
  field_images_unlimited_2: string;
  field_item_not_rentable_online: 'true'|'false';
  highlights: string[];
  title: string;

  /* Created by an API transforms! */
  imageUrls: string[];
  images: CatalogEquipmentImage;
}
