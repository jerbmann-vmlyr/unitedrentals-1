export interface RequestPickup {
  headerData: HeaderData;
  detailData: DetailData[];
}

export interface HeaderData {
  accountNumber: string;
  contractNumber: string;
  startDateTime: string;
  returnDateTime: string;
  guid: string;
  jobsiteId: string;
  jobsiteName: string;
  jobsiteAddress1: string;
  jobsiteCity: string;
  jobsiteState: string;
  jobsiteZip: string;
  jobsiteCountry: string;
  jobsiteContact: string;
  jobsitePhone: string;
  approvalType: string;
}

export interface DetailData {
  equipmentId: string;
  catClass: string;
  description: string;
  transLineId: string;
  quantityAvailableForPickup: string;
  dayRate: string;
  weekRate: string;
  monthRate: string;
  leniencyStartDate: string;
  leniencyEndDate: string;
}
