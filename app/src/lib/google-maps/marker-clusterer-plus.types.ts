
/**
 * marker-clusterer-plus is the library utilized internally by the vue2-google-maps library.
 *
 * These type definitions are extrapolated from the marker-clusterer-plus documentation:
 * http://htmlpreview.github.io/?https://github.com/googlemaps/v3-utility-library/blob/master/markerclustererplus/docs/reference.html
 */

/**
 * This is an object containing general information about a cluster icon.
 * It's the object that a calculator function returns.
 */
export interface ClusterIconInfo {
  /**
   * The index plus 1 of the element in the styles array to be used to style the cluster icon.
   */
  index: number;
  /**
   * The text of the label to be shown on the cluster icon.
   */
  text: string;
  /**
   *  The tooltip to display when the mouse moves over the cluster icon.
   *  If this value is undefined or "", title is set to the value of the title
   *  property passed to the MarkerClusterer.
   */
  title?: string;
}

export type ClusterCalculator = (
  markers: google.maps.Marker[],
  index: number,
) => ClusterIconInfo;

/**
 * This represents the object for values in the styles array passed to the
 * MarkerClusterer constructor. The element in this array that is used to style
 * the cluster icon is determined by calling the calculator function.
 */
export interface ClusterIconStyle {
  /**
   * The anchor position (in pixels) of the cluster icon. This is the spot on
   * the cluster icon that is to be aligned with the cluster position. The format
   * is [yoffset, xoffset] where yoffset increases as you go down and xoffset
   * increases to the right of the top-left corner of the icon.
   * The default anchor position is the center of the cluster icon.
   */
  anchorIcon?: [number, number];
  /**
   * The position (in pixels) from the center of the cluster icon to where the
   * text label is to be centered and drawn. The format is [yoffset, xoffset]
   * where yoffset increases as you go down from center and xoffset increases
   * to the right of center. The default is [0, 0].
   */
  anchorText?: [number, number];
  /**
   * The position of the cluster icon image within the image defined by url.
   * The format is "xpos ypos" (the same format as for the CSS background-position
   * property). You must set this property appropriately when the image defined
   * by url represents a sprite containing multiple images. Note that the
   * position must be specified in px units. The default value is "0 0".
   */
  backgroundPosition?: string;
  /**
   * The value of the CSS font-family property for the label text shown on
   * the cluster icon. The default value is "Arial,sans-serif".
   */
  fontFamily?: string;
  /**
   * The value of the CSS font-style property for the label text shown on
   * the cluster icon. The default value is "normal".
   */
  fontStyle?: string;
  /**
   * The value of the CSS font-weight property for the label text shown on the
   * cluster icon. The default value is "bold".
   */
  fontWeight?: string;
  /**
   * The display height (in pixels) of the cluster icon. Required.
   */
  height: number;
  /**
   * The color of the label text shown on the cluster icon.
   * The default value is "black".
   */
  textColor?: string;
  /**
   * The value of the CSS text-decoration property for the label text shown on
   * the cluster icon. The default value is "none".
   */
  textDecoration?: string;
  /**
   * The size (in pixels) of the label text shown on the cluster icon.
   * The default value is 11.
   */
  textSize?: number;
  /**
   * The URL of the cluster icon image file. Required.
   */
  url: string;
  /**
   * The display width (in pixels) of the cluster icon. Required.
   */
  width: number;
}

/**
 * This represents the optional parameter passed to the MarkerClusterer
 * constructor.
 */
export interface MarkerClustererOptions {
  /**
   * Whether the position of a cluster marker should be the average position of
   * all markers in the cluster. If set to false, the cluster marker is
   * positioned at the location of the first marker added to the cluster.
   * The default value is false.
   */
  averageCenter?: boolean;
  /**
   * Set this property to the number of markers to be processed in a single
   * batch when using a browser other than Internet Explorer (for Internet
   * Explorer, use the batchSizeIE property instead).
   * The default value is MarkerClusterer.BATCH_SIZE.
   */
  batchSize?: number;
  /**
   * When Internet Explorer is being used, markers are processed in several
   * batches with a small delay inserted between each batch in an attempt to
   * avoid Javascript timeout errors. Set this property to the number of markers
   * to be processed in a single batch; select as high a number as you can
   * without causing a timeout error in the browser. This number might need to
   * be as low as 100 if 15,000 markers are being managed, for example.
   * The default value is MarkerClusterer.BATCH_SIZE_IE.
   */
  batchSizeIE?: number;
  /**
   * The function used to determine the text to be displayed on a cluster marker
   * and the index indicating which style to use for the cluster marker.
   *
   * The input parameters for the function are
   * (1) the array of markers represented by a cluster marker and
   * (2) the number of cluster icon styles.
   *
   * It returns a ClusterIconInfo object. The default calculator returns a text
   * property which is the number of markers in the cluster and an index
   * property which is one higher than the lowest integer such that 10^i
   * exceeds the number of markers in the cluster, or the size of the styles
   * array, whichever is less. The styles array element used has an index of
   * index minus 1. For example, the default calculator returns a text value
   * of "125" and an index of 3 for a cluster icon representing 125 markers so
   * the element used in the styles array is 2.
   * A calculator may also return a title property that contains the text of the
   * tooltip to be used for the cluster marker.
   * If title is not defined, the tooltip is set to the value of the title
   * property for the MarkerClusterer.
   * The default value is MarkerClusterer.CALCULATOR.
   */
  calculator?: ClusterCalculator;
  /**
   * The name of the CSS class defining general styles for the cluster markers.
   * Use this class to define CSS styles that are not set up by the code that
   * processes the styles array.
   * The default value is "cluster".
   */
  clusterClass?: string;
  /**
   * Whether to allow the use of cluster icons that have sizes that are some
   * multiple (typically double) of their actual display size. Icons such as
   * these look better when viewed on high-resolution monitors such as Apple's
   * Retina displays. Note: if this property is true, sprites cannot be used as
   * cluster icons.
   * The default value is false.
   */
  enableRetinaIcons?: boolean;
  /**
   * The grid size of a cluster in pixels. The grid is a square.
   * The default value is 60.
   */
  gridSize?: number;
  /**
   * Whether to ignore hidden markers in clusters. You may want to set this to
   * true to ensure that hidden markers are not included in the marker count
   * that appears on a cluster marker (this count is the value of the text
   * property of the result returned by the default calculator). If set to true
   * and you change the visibility of a marker being clustered, be sure to also
   * call MarkerClusterer.repaint().
   * The default value is false.
   */
  ignoreHidden?: boolean;
  /**
   * The extension name for the cluster icon image files (e.g., "png" or "jpg").
   * The default value is MarkerClusterer.IMAGE_EXTENSION.
   */
  imageExtension?: string;
  /**
   * The full URL of the root name of the group of image files to use for
   * cluster icons. The complete file name is of the form imagePathn.imageExtension
   * where n is the image file number (1, 2, etc.).
   * The default value is MarkerClusterer.IMAGE_PATH.
   */
  imagePath?: string;
  /**
   * An array of numbers containing the widths of the group of imagePathn.imageExtension
   * image files. (The images are assumed to be square.)
   * The default value is MarkerClusterer.IMAGE_SIZES.
   */
  imageSizes?: number[];
  /**
   * The maximum zoom level at which clustering is enabled or null if clustering
   * is to be enabled at all zoom levels.
   * The default value is null.
   */
  maxZoom?: number;
  /**
   * The minimum number of markers needed in a cluster before the markers are
   * hidden and a cluster marker appears.
   * The default value is 2.
   */
  minimumClusterSize?: number;
  /**
   * An array of ClusterIconStyle elements defining the styles of the cluster
   * markers to be used. The element to be used to style a given cluster marker
   * is determined by the function defined by the calculator property.
   * The default is an array of ClusterIconStyle elements whose properties are
   * derived from the values for imagePath, imageExtension, and imageSizes.
   */
  styles?: ClusterIconStyle[];
  /**
   * The tooltip to display when the mouse moves over a cluster marker.
   * (Alternatively, you can use a custom calculator function to specify a
   * different tooltip for each cluster marker.)
   * The default value is "".
   */
  title?: string;
  /**
   * Whether to zoom the map when a cluster marker is clicked. You may want to
   * set this to false if you have installed a handler for the click event and
   * it deals with zooming on its own.
   * The default value is true.
   */
  zoomOnClick?: boolean;
}
