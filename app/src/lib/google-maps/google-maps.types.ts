export type WrappedPlaceResult = {
  query: string;
  error: null;
  place: google.maps.places.PlaceResult;
} | {
  query: string;
  error: Error;
  place: null;
};
