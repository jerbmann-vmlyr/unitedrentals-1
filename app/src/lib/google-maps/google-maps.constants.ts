export enum MapMarkerIconPaths {
  ClusterBlue = '/themes/custom/unitedrentals/dist/vue/img/map-markers/cluster-small.png',
  ClusterGray = '/themes/custom/unitedrentals/dist/vue/img/map-markers/cluster-small-grey.png',
  ClusterBlack = '/themes/custom/unitedrentals/dist/vue/img/map-markers/cluster-small-black.png',
}
