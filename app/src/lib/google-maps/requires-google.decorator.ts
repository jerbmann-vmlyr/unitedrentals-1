/**
 * Using RequiresGoogle as a method decorator will wrap the method in a function
 * that ensures the google library is available on the window before the
 * original function is executed. If it's not, an Error will be thrown.
 *
 * Usage:
 * class MyMapUtilClass {
 *  @RequiresGoogle()
 *  static methodThatNeedsGoogle() {
 *    console.log(`I'll never be executed unless window.google is a thing.`);
 *  }
 * }
 *
 */
export function RequiresGoogle(): MethodDecorator {
  return (target, propertyKey, descriptor): void => {
    const originalMethod = descriptor.value;
    if (typeof originalMethod !== 'function') {
      throw new Error(`@RequiresGoogle() can only be used on class methods. ${propertyKey.toString()} is not a function.`);
    }

    descriptor.value = ((...args: any[]) => {
      if (!(window as any).google) {
        throw new Error(`GoogleMapsUtils.${propertyKey.toString()} cannot be used until the google library is available on the window.`);
      }
      return (originalMethod as any)(...args);
    }) as any;
  };
}
