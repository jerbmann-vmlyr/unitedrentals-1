import calculator from '@/utils/calculator';
import { RequiresGoogle } from './requires-google.decorator';

export class GoogleMapsUtils {
  /**
   * Creates a LatLngBounds object.
   * If given an optional list of objects that have lat and lng properties,
   * the returned LatLngsBounds will span those coordinates.
   */
  @RequiresGoogle()
  static createBounds<T extends google.maps.LatLngLiteral>(
    locations: T[] = [],
  ): google.maps.LatLngBounds & {} {
    return locations.reduce(
      (bounds, { lat, lng }) => bounds.extend({ lat, lng }),
      new google.maps.LatLngBounds(),
    );
  }

  @RequiresGoogle()
  static latLngLiteralsFromMarkers(
    markers: (google.maps.MarkerOptions | google.maps.Marker)[] = [],
  ): google.maps.LatLngLiteral[] {
    return markers.map(marker => {
      const position = marker instanceof google.maps.Marker
        ? marker.getPosition()
        : marker.position;

      return position instanceof google.maps.LatLng
        ? position.toJSON()
        : position;
    });
  }

  @RequiresGoogle()
  static adjustMarkerSpread<T extends google.maps.MarkerOptions>(
    markers: T[] = [],
  ): T[] {
    if (markers.length < 1) return [];

    const positions = GoogleMapsUtils.latLngLiteralsFromMarkers(markers);
    const bounds = GoogleMapsUtils.createBounds(positions);
    const distance = calculator.distance(
      bounds.getSouthWest().toJSON(),
      bounds.getNorthEast().toJSON(),
    );
    const distanceMax = (300 * distance) || 50;
    const distanceMultiplier = Math.min(distanceMax, 250000);
    // Use the center of the bounds as the starting point
    const origin = bounds.getCenter().toJSON();
    return calculator.markerPlotter(
      origin,
      markers,
      distanceMultiplier,
    );
  }

  /**
   * Determines if a LatLngBounds object is comprised of a single point.
   * i.e. it's lat and lng dimensions are zero.
   */
  static boundsAreSinglePoint(bounds: google.maps.LatLngBounds): boolean {
    const span = bounds.toSpan();
    return bounds.isEmpty() || (span.lat() === 0 && span.lng() === 0);
  }

  /**
   * Google dies if we send US 5+4 zips to its place service. This guards against that:
   * 1. Remove the four-digit suffix from the end of a zip (if any)
   * 2. Strip out "undefined"
   */
  static sanitizeInput(input: string): string {
    return input
      .replace(/(\d{5}-\d{4})/, match => match.substring(0, 5))
      .replace(/undefined/g, '').trim();
  }
}
