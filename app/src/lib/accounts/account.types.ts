/*

  TODO: The following type definitions are derived from raw API
  response data and as such, are almost certainly incomplete or
  innaccurate. When API documentation for the frontend does become
  available, these should be updated accordingly.

  In the interim, to prevent unnecessary usage friction,
  the types should be provided with an index signature.
  (i.e. [prop: string]: any )

*/
import * as common from '../common-types';

export interface Account {
  address1: string;
  address2: string;
  amounts: {
    [prop: string]: any;
  };
  averageDays: string;
  city: string;
  collector: {
    [prop: string]: any;
  };
  consolidatedBill: {
    [prop: string]: any;
  };
  contact: {
    [prop: string]: any;
  };
  countryCode: string;
  creditCardAuthorized: common.YesOrNo;
  creditCode: 'Credit'|'Cash';
  creditLimit: number;
  email: string;
  id: string;
  isFavorited: boolean;
  jobCost: boolean;
  lastPaymentDate: string;
  lastTransactionDate: any;
  name: string;
  openRentalAmount: string;
  parentId: string;
  phone: string;
  quoteRedirectLoc: string;
  rental: {
    [prop: string]: any;
  };
  requirePo: common.YesOrNo;
  reservationRedirectLoc: string;
  rppDefault: common.YesOrNo;
  state: string;
  status: 'A'|'I'|'B'|'C'|'D'|'F'|'G'|'H'|'L'|'P'|'R'|'S';
  statusDesc: string;
  tcLevel: string;
  terms: string;
  zip: string;

  /* Must be added as part of the fetch accounts API transform! */
  tcLevelLabel?: string;
}

export type AccountForm = common.Address & {
  accountId: string;
  id?: string;
  countryCode?: string;
  email: string;
  individual?: boolean;
  name: string;
  phone: string;
};

export interface TcUserForm {
  firstName: string;
  lastName: string;
}

export interface AccountSearchParams {
  city: string;
  favorites: boolean;
  id: string;
  name: string;
  name_filter: 'contains' | 'startswith';
  parentId: string;
  state: string;
}

export type AccountSearchCriteria = {
  [K in keyof AccountSearchParams]: {
    enabled: boolean;
    value: any;
    label: string;
    readonly possibleValues?: string[];
  }
};

export enum AccountColumnKey {
  AccountId = 'id',
  AccountName = 'name',
  CityState = 'cityState',
  Favorite = 'isFavorited',
  Level = 'tcLevel',
  ParentAccountId = 'parentId',
  Status = 'statusDesc',
}
