import { dictionary as i18n } from '@/data/i18n';
import { AccountForm, TcUserForm, AccountSearchCriteria, AccountColumnKey } from './account.types';

export class AccountUtils {
  static createEmptyAccountForm(): AccountForm {
    return {
      accountId: '', // GET sends `id` field, PUT uses `accountId` field. POST has no field.
      address1: '',
      address2: '',
      city: '',
      state: '',
      zip: '',
      // contactName: '',     // In Argo Design but not a DAL field
      countryCode: '',     // In Argo Design but not a DAL field
      email: '',
      name: '',
      phone: '',
    };
  }

  static createEmptyAccountSearchCriteria(): AccountSearchCriteria {
    return {
      name: {
        enabled: false,
        value: '',
        label: 'Account Name',
      },
      name_filter: {
        enabled: false,
        value: 'startswith',
        label: 'contains|starts with',
        possibleValues: ['startswith', 'contains'],
      },
      favorites: {
        enabled: false,
        value: true,
        label: 'Favorites',
      },
      city: {
        enabled: false,
        value: '',
        label: 'City contains',
      },
      state: {
        enabled: false,
        value: '',
        label: 'State/Prov.',
      },
      parentId: {
        enabled: false,
        value: '',
        label: 'Parent Account #',
      },
      id: {
        enabled: true,
        value: '',
        label: '',
      },
    };
  }

  static createEmptyTcUserForm(): TcUserForm {
    return {
      firstName: '',
      lastName: '',
    };
  }

  static getAccountColumnLabel(columnKey: AccountColumnKey): string {
    switch (columnKey) {
      case AccountColumnKey.AccountId: return i18n.accountSelector.labels.id;
      case AccountColumnKey.AccountName: return i18n.accountSelector.labels.name;
      case AccountColumnKey.CityState: return i18n.accountSelector.labels.cityState;
      case AccountColumnKey.Favorite: return i18n.accountSelector.labels.favorite;
      case AccountColumnKey.Level: return i18n.accountSelector.labels.level;
      case AccountColumnKey.ParentAccountId: return i18n.accountSelector.labels.parentId;
      case AccountColumnKey.Status: return i18n.accountSelector.labels.status;
    }
  }
}
