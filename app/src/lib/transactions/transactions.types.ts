/*

  TODO: The following type definitions are derived from raw API
  response data and as such, are almost certainly incomplete or
  innaccurate. When API documentation for the frontend does become
  available, these should be updated accordingly.

  In the interim, to prevent unnecessary usage friction,
  the types should be provided with an index signature.
  (i.e. [prop: string]: any )

*/
import * as common from '../common-types';
import { RequisitionStatusCode } from './requisitions.types';

export enum TransactionType {
  Reservation = 'R',
  Rental = 'O',
  Closed = 'C',
  Quote = 'Q',
}

export type TransactionMap = { [transId: string]: Transaction };

export interface Transaction {
  accountId: string;
  accountingCodes: common.AccountingCodes;
  approver: string|null;
  approverGuid: string|null;
  approverName: string|null;
  branchId: string;
  createdDateTime: string;
  currency: common.CurrencyCode;
  customerName: string;
  delivery: common.YesOrNo;
  email: string|null;
  endDateTime: string|null;
  hasFASTPdf: boolean,
  items: { [catClass: string]: any };
  jobsite: common.Jobsite;
  nextPickupDateTime: string|null;
  openTrans: boolean;
  pickup: common.YesOrNo;
  pickupDateTime: string|null;
  pickupFirm: common.YesOrNo;
  po: string;
  quoteId: string|null;
  reqContact: string|null;
  reqDateTime: string|null;
  reqGuid: string|null;
  requester: string|null;
  requesterGuid: string;
  requesterName: string;
  requesterPhone: string|null;
  requisitionId: string;
  reservationQuoteId: null;
  returnDateTime: string;
  rpp: common.YesOrNo;
  startDateTime: string;
  transId: string;
  transType: TransactionType;
  updateType: string|null;
  urWillPickup: common.YesOrNo;
}

export interface Contract extends Transaction {
  transType: TransactionType.Rental | TransactionType.Closed;
}

export interface Reservation extends Transaction {
  transType: TransactionType.Reservation;

  // Must be added manually by calling the requisitions API!
  requisitionStatusCode: RequisitionStatusCode;
}

export interface Quote extends Transaction {
  transType: TransactionType.Quote;

  // Must be added manually by calling the requisitions API!
  requisitionStatusCode: RequisitionStatusCode;
}
