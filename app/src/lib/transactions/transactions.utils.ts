import { Transaction, TransactionType, Contract, Quote, Reservation } from './transactions.types';

export class TransactionUtils {
  static isOpenContract(tx: Transaction): tx is Contract {
    return tx.transType === TransactionType.Rental;
  }

  static isClosedContract(tx: Transaction): tx is Contract {
    return tx.transType === TransactionType.Closed;
  }

  static isQuote(tx: Transaction): tx is Quote {
    return tx.transType === TransactionType.Quote;
  }

  static isReservation(tx: Transaction): tx is Reservation {
    return tx.transType === TransactionType.Reservation;
  }
}
