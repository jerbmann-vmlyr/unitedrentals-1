import { StatusPriority } from '@/lib/statuses';
import { TransactionUtils } from '../transactions.utils';
import {
  ReservationStatus,
  ReservationStatusId,
  TransactionStatusColor,
} from './transaction-statuses.types';
import { RequisitionStatusCode } from '../requisitions.types';

export const RESERVATION_STATUSES: ReservationStatus[] = [
  {
    id: ReservationStatusId.Submitted,
    color: TransactionStatusColor.Grey,
    isApplicable: tx =>
      TransactionUtils.isReservation(tx)
      && tx.requisitionStatusCode === RequisitionStatusCode.ReservationRequested,
  },
  {
    id: ReservationStatusId.RequestConfirmed,
    color: TransactionStatusColor.Grey,
    isApplicable: tx =>
      TransactionUtils.isReservation(tx)
      && tx.requisitionStatusCode === RequisitionStatusCode.ReservationConfirmed,
  },
  {
    id: ReservationStatusId.PendingApproval,
    priority: StatusPriority.High,
    color: TransactionStatusColor.BrightRed,
    isApplicable: tx =>
      TransactionUtils.isReservation(tx)
      && tx.requisitionStatusCode === RequisitionStatusCode.PendingApproval,
  },
  {
    id: ReservationStatusId.Confirmed,
    color: TransactionStatusColor.Grey,
    isApplicable: tx =>
      TransactionUtils.isReservation(tx)
      && tx.requisitionStatusCode === RequisitionStatusCode.ReservationConfirmed,
  },
];
