import { StatusLabeler, StatusDescriptor } from '@/lib/statuses';
import { Contract, Quote, Reservation } from '../transactions.types';

export enum ContractStatusId {
  /* Equipment delivered and the contract has officially started */
  Open = 'Open',
  /* Equipment within 3 days of its end date */
  DueSoon = 'DueSoon',
  /* Equipment kept passed the end date */
  Overdue = 'Overdue',
  /* End of contract reached, equipment returned, and final invoice paid */
  Closed = 'Closed',
}

export enum QuoteStatusId {
  /* Quote request submitted */
  Requested = 'Requested',
}

export enum ReservationStatusId {
  /* Reservation request was submitted */
  Submitted = 'Submitted',
  /* Reservation request submitted is confirmed but the equipment is not available */
  RequestConfirmed = 'Request Confirmed',
  /* Reservations Submitted but requires hard approval before it can be confirmed */
  PendingApproval = 'Pending Approval',
  /* Reservation request for an order is confirmed and converted into a Reservation by UR */
  Confirmed = 'Confirmed',
}

export type ContractStatus = StatusDescriptor<Contract, ContractStatusId, { color: TransactionStatusColor }>;
export type ContractStatusLabeler = StatusLabeler<Contract, ContractStatusId>;

export type QuoteStatus = StatusDescriptor<Quote, QuoteStatusId, { color: TransactionStatusColor }>;
export type QuoteStatusLabeler = StatusLabeler<Quote, QuoteStatusId>;

export type ReservationStatus = StatusDescriptor<Reservation, ReservationStatusId, { color: TransactionStatusColor }>;
export type ReservationStatusLabeler = StatusLabeler<Reservation, ReservationStatusId>;

export enum TransactionStatusColor {
  BrightRed = '#E10000',
  Grey = '#DDE0E2',
}
