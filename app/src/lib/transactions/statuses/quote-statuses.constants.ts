import { TransactionUtils } from '../transactions.utils';
import {
  QuoteStatus,
  QuoteStatusId,
  TransactionStatusColor,
} from './transaction-statuses.types';
import { RequisitionStatusCode } from '../requisitions.types';


export const QUOTE_STATUSES: QuoteStatus[] = [
  {
    id: QuoteStatusId.Requested,
    color: TransactionStatusColor.Grey,
    isApplicable: tx =>
      TransactionUtils.isQuote(tx)
      && tx.requisitionStatusCode === RequisitionStatusCode.QuoteRequested,
  },
];
