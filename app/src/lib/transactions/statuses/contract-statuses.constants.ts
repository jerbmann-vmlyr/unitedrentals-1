import { MomentUtils } from '@/lib/moment.utils';
import { StatusPriority } from '@/lib/statuses';
import { TransactionUtils } from '@/lib/transactions/transactions.utils';
import {
  ContractStatus,
  ContractStatusId,
  TransactionStatusColor,
} from './transaction-statuses.types';

export const CONTRACT_STATUSES: ContractStatus[] = [
  {
    id: ContractStatusId.Open,
    color: TransactionStatusColor.Grey,
    isApplicable: tx =>
      TransactionUtils.isOpenContract(tx)
      && MomentUtils.happensInTheFuture(tx.returnDateTime!),
  },
  {
    id: ContractStatusId.DueSoon,
    color: TransactionStatusColor.Grey,
    priority: StatusPriority.Medium,
    isApplicable: tx =>
      TransactionUtils.isOpenContract(tx)
      && MomentUtils.happensWithinThreeDays(tx.returnDateTime!),
  },
  {
    id: ContractStatusId.Overdue,
    color: TransactionStatusColor.BrightRed,
    priority: StatusPriority.High,
    isApplicable: tx =>
      TransactionUtils.isOpenContract(tx)
      && MomentUtils.happensInThePast(tx.returnDateTime),
  },
  {
    id: ContractStatusId.Closed,
    color: TransactionStatusColor.Grey,
    isApplicable: tx => TransactionUtils.isClosedContract(tx),
  },
];
