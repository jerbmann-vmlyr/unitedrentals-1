import * as common from '@/lib/common-types';

export enum RequisitionStatusCode {
  Unsubmitted = 'OR', // Open Requisition Unsubmitted
  PendingApproval = 'OA', //  Req Submitted for Approval
  Closed = 'OC', // Requisition Closed
  InternallyDeclined = 'OD', // Req Internally Declined
  Quoted = 'QT', // Requisition Quoted
  QuoteRequested = 'QR', // Quote Requested
  QuoteRequestCancelled = 'QC', // Quote Request Cancelled
  QuoteRequestDeclined = 'QD', // Quote Request Declined
  ReservationRequested = 'RR', // Reservation Requested
  ReservationConfirmed = 'RS', // Requisition Reserved
  ReservationRequestCancelled = 'RC', // Reservation Request Cancelled
  ReservationRequestDeclined = 'RD', // Reservation Request Declined
}

export interface Requisition {
  accountId: string;
  accountingCode: common.AccountingCodes;
  approver: null;
  approverGuid: string;
  approverName: string;
  branchId: string;
  coeStatusCode: '';
  createdDateTime: null;
  creditCard: null;
  currency: null;
  customerName: null;
  delivery: common.YesOrNo;
  email: null;
  endDate: string;
  endDateTime: string;
  hasFASTPdf: null;
  id: string; // Same as accountId
  items: { [catClass: string]: any };
  jobsite: common.Jobsite;
  nextPickupDateTime: null;
  openTrans: null;
  pickup: common.YesOrNo;
  pickupDateTime: null;
  pickupFirm: common.YesOrNo;
  po: string;
  quoteId: string | '';
  reqContact: string;
  reqDateTime: string;
  reqGuid: string;
  requester: null;
  requesterGuid: null;
  requesterName: null;
  requesterPhone: null;
  requestId: string;
  requisitionId: null;
  reservationQuoteId: '';
  returnDate: string;
  returnDateTime: null;
  rpp: common.YesOrNo;
  startDate: string;
  statusCode: RequisitionStatusCode;
  statusCodeDesc: string;
  timestamp: string;
  totals: null;
  transId: null;
  transType: null;
  updateType: null;
  urWillPickup: common.YesOrNo;
}

export interface RequisitionMap {
  [reqId: string]: Requisition;
}
