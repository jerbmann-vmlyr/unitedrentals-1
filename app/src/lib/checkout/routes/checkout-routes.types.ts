export enum CheckoutRouteNames {
  RppDisclaimerModal = 'checkout:rpp-disclaimer-modal',
  LocationFinderFilter = 'lfFilterMidCheckout',
  LocationFinderSearch = 'lfSearchMidCheckout',
  LocationFinderState = 'lfStateMidCheckout',
  LocationFinderCity = 'lfCityMidCheckout',
  LocationFinderType = 'lfTypeMidCheckout',
  LocationFinderBranch = 'lfBranchMidCheckout',
}
