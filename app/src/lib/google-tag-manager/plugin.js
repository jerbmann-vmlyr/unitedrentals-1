// import { TagManager } from './core';
import events from './events';
import { createEvent } from './utils';
import { StateMixin, ManagerMixin, UrUtilsMixin } from './mixins';

export const UrGtmComponent = {
  mixins: [StateMixin, ManagerMixin, UrUtilsMixin],
  methods: {
    track(event, ...rest) {
      const handler = events[event] || createEvent(event);
      const res = handler.call(this, ...rest);
      this.push(res);
    },
  },
};

export default UrGtmComponent;
