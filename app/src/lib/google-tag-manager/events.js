/* eslint-disable object-shorthand */
import { castArray, defaults, sum } from 'lodash';

export function ecomAction({ type, transformer, products, actionField, ...rest }) {
  return {
    [type]: {
      actionField,
      products: castArray(products).map(product => transformer({ product, ...rest })),
    },
  };
}

export const Events = {
  testing({ name }) {
    return {
      event: 'Testing',
      ecommerce: {
        detail: {
          dimension1: name,
        },
      },
    };
  },
  'ecom:purchase': function ({ transaction, products, totals = {}, action = {}, ...rest } = {}) {
    const transId = rest.transId || transaction.transId;
    const reqId = rest.reqId || transaction.reqId;

    const actionField = defaults({}, action, {
      id: (transId || reqId),
      affiliation: 'Online Store',
      coupon: null,
      revenue: totals.totalRentalAmount,
      tax: totals.tax,
      shipping: sum([totals.damageWaiverFee, totals.deliveryFee, totals.environmentFee, totals.pickupFee]),
    });

    return {
      event: 'purchase',
      ecommerce: ecomAction({
        type: 'purchase',
        transformer: this.formatEcomProduct,
        products,
        actionField,
        transId,
        reqId,
        ...rest,
      }),
    };
  },

};

export default Events;
