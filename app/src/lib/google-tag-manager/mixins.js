import { mapGetters, mapState } from 'vuex';
import { defaultTo, get, isNil, toInteger, pickBy } from 'lodash';

import types from '@/store/types';
import * as utils from './utils';


/**
 * This is a thin wrapper around GTM dataLayer to add some
 * logging, configuration, couple of default tracks, etc.
 */
export const ManagerMixin = {
  data() {
    return {
      config: {
        debug: false,
        enabled: true,
        layer: 'dataLayer',
      },
    };
  },
  methods: {
    _push(payload) {
      if (typeof payload !== 'object') throw new Error('event should be an object');
      window[this.config.layer].push(payload);
    },

    push(payload) {
      if (utils.inBrowser && this.config.enabled) {
        this._push(payload);
        this.log('push:sent', payload);
      }
    },

    log(...rest) {
      if (this.config.debug) utils.logDebug(...rest);
    },

    trackView(screenName, path) {
      this.log('Dispatching trackView', { screenName, path });
      this.push({
        event: 'content-view',
        'content-name': path,
      });
    },

    trackEvent({
      event = null,
      category = null,
      action = null,
      label = null,
      value = null,
      noninteraction = false,
      ...rest
    } = {}) {
      this.push({
        event: event || 'interaction',
        target: category,
        action,
        value,
        'target-properties': label,
        'interaction-type': noninteraction,
        ...rest,
      });
    },
  },
};


/**
 * This mixin is used for making $store values available to $gtm.
 * Vuex isn't the ideal way to get these, there's an ongoing effort
 * to reduce the coupling in the new theme. For now this works to
 * be compatible and isolated to allow future replacement.
 */
export const StateMixin = {
  data() {
    return {
      userID: get(drupalSettings, ['user', 'uid']) !== 0 ? get(drupalSettings, ['user', 'uid']) : '(anonymous)', // eslint-disable-line
    };
  },
  computed: {
    ...mapGetters([
      'categoryByCatClass',
      'categoryIdByCatClass',
      'reservationBlocks',
      'subCategoryByCatClass',
      'branchRates',
    ]),
    ...mapGetters({ getEstimate: types.checkout.getEstimate }),
    ...mapState({
      equipment: state => state.catalog.equipment,
      estimates: state => state.checkout.estimates,
    }),
    storeID() {
      const storeID = this.$store.state.user.ratesPlaceBranch;
      return (storeID && storeID !== '') ? storeID : '';
    },
  },
};

/**
 * These are Utility/Helper methods to be used by the tracking
 * methods. Taken from some components that copy/pasted them,
 * they could use some cleanup at some point. @todo
 */
export const UrUtilsMixin = {
  mixins: [StateMixin],
  methods: {
    exportGtmProducts() {
      const reservationBlockIds = Object.keys(this.reservationBlocks);
      let products = [];

      reservationBlockIds.forEach((reservationBlockId) => {
        const blockProducts = this.exportGtmProductsByBlock(reservationBlockId);
        products = products.concat(blockProducts);
      });

      return products;
    },
    exportGtmProductsByBlock(reservationBlockId, block) {
      if (!Object.keys(this.equipment).length || !Object.keys(this.estimates).length) {
        return [];
      }
      if (typeof block === 'undefined') {
        // block is defined in transaction-block.vue (to avoid error in testing when HMR), otherwise undefined
        block = this.reservationBlocks[reservationBlockId];
      }
      const data = {
        blockId: reservationBlockId,
        equipment: this.equipment,
        estimate: this.getEstimate(reservationBlockId),
        categoryByCatClass: this.categoryByCatClass,
        subCategoryByCatClass: this.subCategoryByCatClass,
      };
      const products = this.parseItemProducts(data, [], block.items, Object.keys(block.items));

      return products;
    },
    parseItemProducts(data, products, items, [itemKey, ...itemKeys]) {
      const item = items[itemKey];

      try {
        const product = {
          name: data.equipment[item.catClass].title,
          catClass: item.catClass,
          price: data.estimate.items[item.catClass].dayRate,
          category: data.categoryByCatClass(item.catClass),
          subCategory: data.subCategoryByCatClass(item.catClass),
          quantity: item.quantity,
        };

        products.push(product);

        return itemKeys.length
          ? this.parseItemProducts(data, products, items, itemKeys)
          : products;
      }
      catch (e) {
        return [{}];
      }
    },

    /**
     * Factory function for making dimensions.
     *
     * @return {object} GTM dimensions
     */
    formatDimensions({ reqId, transId, catClass, price } = {}) {
      const res = {
        dimension1: this.userID,
        dimension2: reqId,
        dimension3: this.storeID,
        dimension5: catClass, // Product SKU
        dimension6: price, // Product Price
        dimension7: `${catClass}|${price}`, // Product SKU|Price
        dimension9: transId, // Transaction ID
      };

      return pickBy(res, v => !isNil(v));
    },

    getPriceForProduct(catClass, rates, reservationBlockId) {
      // There's different places that prices are determined,
      // so there's a chance this fails. Best bet is to pass
      // in the rates yourself
      try {
        return get(rates || this.getEstimate(reservationBlockId), [catClass, 'dayRate']);
      }
      catch (error) {
        utils.logDebug('Unable to infer price of item', { catClass, error }, 'error');
      }
    },

    getTitleForProduct(item) {
      return this.$store.state.catalog.equipment[item.catClass].title;
    },

    formatEcomProduct({ product, transId, reqId, rates, price, ...rest }) {
      const { catClass } = product;
      const fProduct = {
        id: catClass,
        name: this.equipment[catClass] ? this.equipment[catClass].title : product.title,
        price: defaultTo(price, product.price),
        category: defaultTo(product.category, this.categoryByCatClass(catClass)), // Product Category (Equipment)
        list: defaultTo(product.subcategory, this.subCategoryByCatClass(catClass)), // Product Sub Category
        quantity: toInteger(product.quantity), // Product Quantity
      };

      fProduct.price = defaultTo(fProduct.price, this.getPriceForProduct(catClass, rates, rest.reservationBlockId));

      const dims = this.formatDimensions({ reqId, transId, catClass, ...fProduct });

      return { ...fProduct, ...dims };
    },
  },
};

export default { ManagerMixin, StateMixin, UrUtilsMixin };
