import { UrGtmComponent } from './plugin';

export const UrGtmPlugin = {
  install(Vue, options = {}) {
    const { router, store, ...conf } = options;

    const defaultConfig = {
      debug: false,
      enabled: true,
      layer: 'dataLayer',
    };
    const config = { ...defaultConfig, ...conf };

    // window[config.layer] = window[config.layer] || [];
    const urgtm = new Vue({
      store,
      data: { config },
      extends: UrGtmComponent,
    });

    Vue.urgtm = urgtm;
    Vue.prototype.$urgtm = urgtm;

    if (router) setupRouterTracking(Vue, router);
  },
};


export function setupRouterTracking(Vue, router) {
  // Register a guard on the router that tracks GTM Views after route changes.
  // Can set a custom view name on the route config for clarity.
  router.afterEach((to) => {
    const name = to.meta.gtm || to.name;
    Vue.urgtm.trackView(name, to.fullPath);
  });
}

export default UrGtmPlugin;
