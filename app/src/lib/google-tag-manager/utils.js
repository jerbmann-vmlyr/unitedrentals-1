/* eslint-disable no-console */
import { isUndefined } from 'lodash';

export const inBrowser = !isUndefined(window);

export function createEvent(event) {
  return (...rest) => ({
    event,
    ...rest,
  });
}

export function logDebug(message, payload, type = 'log') {
  if (inBrowser && !isUndefined(console)) {
    console[type](`[URGTM]:${message}`, payload);
  }
}
