export interface Approver {
  accountId?: string;
  altPhone?: string;
  email: string;
  firstName?: string;
  guid: string;
  lastName?: string;
  name: string | null;
  phone?: string;
  status?: string;
}
