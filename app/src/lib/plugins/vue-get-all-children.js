export default {
  install: (Vue) => {
    Vue.prototype.$getAllChildren = function () {
      return this.$children.reduce(reducer, []);
    };
  },
};

const reducer = (all, component) => {
  component.$children.forEach((child) => {
    reducer(all, child);
  });

  all.push(component);

  return all;
};
