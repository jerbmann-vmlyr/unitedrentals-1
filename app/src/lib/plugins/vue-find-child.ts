/**
 * Recurses through a component's entire $children tree
 * to find a matching child.  If multiple are found, returns an array of matches
 * (along with a warning that you might try a more specific search).
 *
 * This function is parametized by any prop of the child's vm.  This includes data, computeds, methods, etc.
 * Alternatively, you can pass in a filter function that gets invoked with the component.
 *
 * It is really only of use in IE, since we don't have Vue DevTools that allow us to inspect component state.
 */
export default {
  install: Vue => {
    Vue.prototype.$findChild = function(...args) {
      const matches = this.$children.reduce(getReducer(args), []);
      const hasManyMatches = matches.length > 1;
      if (hasManyMatches) {
        console.warn('Multiple children found. To narrow your search, try searching w/a more granular set of vm properties.'); // tslint:disable-line
      }

      if (!matches.length) {
        console.warn('No child component found. Try invoking this form a higher-order component or changing your search params.'); // tslint:disable-line
      }

      return hasManyMatches
        ? matches
        : matches[0];
    };
  },
};

const getReducer = args => {
  function reducer(all, component) {
    component.$children.forEach(child => {
      reducer(all, child);
    });

    const vmProps: any[] = [];
    for (const prop in component) vmProps.push(prop);

    const isMatch = typeof args[0] === 'function' ? args[0](component) : args.every(param => vmProps.includes(param));

    if (isMatch) {
      all.push(component);
    }

    return all;
  }

  return reducer;
};


