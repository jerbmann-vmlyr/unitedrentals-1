// Requires our other plugin, vue-get-all-children.

/*
  Exposes are $validateChildren() method on all components.  The method iterates over all children,
  "dirties" their form-field(s), triggering error messaging on the components.

  Returns a boolean. False if any child is invalid, otherwise true.
 */
export default {
  install: (Vue) => {
    Vue.prototype.$validateChildren = function () {
      // A list of invalid child component names.
      this.$invalidChildren = [];

      return this.$getAllChildren().reduce((valid, component) => {
        const { $v } = component;

        if (!$v) {
          return valid;
        }

        // "Dirty" the component's form controls, flagging them as "having been used by the user"
        // (Templates hide errors on the control so long as the control isn't $dirty)
        $v.$touch();

        // If any are invalid, the form is invalid. Record the invalid child component's name.
        if ($v.$invalid) {
          valid = false;
          const { name, $el } = component;
          this.$invalidChildren.push({ name, $el });
        }

        return valid;
      }, true);
    };

    Vue.prototype.$validateChildrenAndScrollToError = function () {
      const isValid = this.$validateChildren();

      if (!isValid) {
        const [firstInvalidComponent] = this.$invalidChildren;
        const { $el } = firstInvalidComponent;
        const $elTop = $el.getBoundingClientRect().top - $el.offsetHeight;
        const pageTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);

        const destination = $elTop + pageTop;

        // Assign to both the body and documentElement for an iOS Safari bug.
        document.documentElement.scrollTop = document.body.scrollTop = destination; // eslint-disable-line
      }

      return isValid;
    };
  },
};
