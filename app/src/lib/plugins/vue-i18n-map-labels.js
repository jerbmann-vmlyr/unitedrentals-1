/**
 * This is a new helper that exposes our UR utility Map Labels
 * to all components.
 *
 * With it, we can continue to do i18n the old way,
 * in a `data() {}` block using the `mapLabels()` method.
 *
 * And we can do i18n a new way: skipping the `data() {}` block
 * completely and just using `$i18n('some.key')` in the template!
 *
 * NOTE: if the label from the dictionary contains HTML, make sure to render it with a Vue "v-html" binding
 * instead of a regular template binding ("{{label.key}}")
 */
import { mapLabels, mapComputedLabels } from '@/utils';

export default {
  install: (Vue) => {
    Vue.prototype.$i18n = function (i18nProperty, computations) {
      return computations
        ? mapComputedLabels({ i18nProperty: [i18nProperty, computations] }).i18nProperty
        : mapLabels({ i18nProperty }).i18nProperty;
    };
  },
};
