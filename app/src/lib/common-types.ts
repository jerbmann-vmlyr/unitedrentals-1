/*
  Put any of the commonly-used API data types here.
  Do not put actual code in here, just types and interfaces. If and when this
  file gets big enough, it should be moved to its own directory and split.

  TODO: The following type definitions are derived from raw API
  response data and as such, are almost certainly incomplete or
  innaccurate. When API documentation for the frontend does become
  available, these should be updated accordingly.

  In the interim, to prevent unnecessary usage friction,
  the types should be provided with an index signature.
  (i.e. [prop: string]: any )

*/

export type YesOrNo = 'Y' | 'N';

export type CurrencyCode = 'CAD' | 'USA';

export type CountryCode = 'US' | 'CA';

export interface Address {
  address1: string|null;
  address2: string|null,
  city: string|null;
  state: string|null;
  zip: string|null;
}

export interface Contact {
  contact: string|null;
  email: string|null;
  mobilePhone: string|null;
  phone: string|null;
}

export interface Jobsite extends Address, Contact {
  accountId: string;
  code?: string;
  customerJobId: string;
  id: string;
  latitude?: string;
  longitude?: string;
  name: string;
  notes: string|null;
  jobPhone: string;
}

export interface AccountingCodes {
  1: string;
  2: string;
  3: string;
  4: string;
  5: string;
  6: string;
}
