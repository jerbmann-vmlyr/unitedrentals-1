import { DirectiveOptions } from 'vue';
import * as Stickyfill from 'stickyfilljs'; // allow 'sticky' class to function in IE11

/**
 * v-sticky
 *
 * Makes an element have a CSS "sticky" position. This directive applies the
 * stickyfill polyfill for older browsers.
 *
 * You can optionally pass the directive modifiers: 'top', 'bottom', 'left', 'right'
 * The default modifier is 'top'
 */
const stickyDirective: DirectiveOptions = {
  bind(el, binding) {
    Stickyfill.addOne(el);
    el.classList.add('sticky');

    let mods = Object.keys(binding.modifiers);
    mods = mods.length < 1 ? ['top'] : mods;

    mods.forEach(mod => {
      el.style[mod] = '0px';
    });
  },
  unbind(el) {
    Stickyfill.removeOne(el);
  },
};

export default stickyDirective;
