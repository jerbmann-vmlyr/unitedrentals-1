import { DirectiveOptions } from 'vue';

const isArrayOfStrings = (value): value is string[] => Array.isArray(value) && value.every(v => typeof v === 'string');

type ApplyClassOnHoverElement = HTMLElement & {
  __applyClassOnHoverListeners__: any;
};

const applyClassesOnHoverDirective: DirectiveOptions = {
  bind(el: HTMLElement, binding, vnode) {
    const classes = binding.value;

    if (!isArrayOfStrings(classes)) {
      throw new Error(`applyClassesOnHoverDirective expects an Array of strings, ${classes} is not valid`);
    }

    const addClassesHandler = () => el.classList.add(...classes);
    const removeClassesHandler = () => el.classList.remove(...classes);

    const eventListeners = {
      mouseover: addClassesHandler,
      mousemove: addClassesHandler,
      mouseout: removeClassesHandler,
    };

    (el as ApplyClassOnHoverElement).__applyClassOnHoverListeners__ = eventListeners;
    Object.entries(eventListeners)
      .map(([event, listener]) => el.addEventListener(event, listener));
  },
  unbind(el) {
    Object.entries((el as ApplyClassOnHoverElement).__applyClassOnHoverListeners__ || {})
      .map(([event, listener]) => el.addEventListener(event, listener as any));
  },
};

export default applyClassesOnHoverDirective;
