import Vue, { DirectiveOptions, CreateElement, VNode, VueConstructor } from 'vue';

export class OpenOver {
  static readonly createId = (() => {
    let id = 0;
    return () => `${id++}`;
  })();

  static readonly registry = new Map<string, OpenOver>();

  private readonly componentConstructor: VueConstructor;
  private renderedComponent: Vue | null = null;

  constructor(
    private readonly context: Vue,
    private readonly hostEl: HTMLElement,
    private readonly component: ReturnType<typeof Vue.extend>,
    private readonly openEvents: string[],
    private readonly hostComponent: Vue | null = null,
  ) {
    this.componentConstructor = Vue.extend({ render: this.render.bind(this) });
  }

  listen() {
    this.openEvents.forEach(e => {
      this.hostEl.addEventListener(e, this.open.bind(this));
    });
  }

  open() {
    if (this.renderedComponent) {
      return this.close();
    }

    const componentInstance = new this.componentConstructor({ parent: this.context });
    componentInstance.$mount();
    this.context.$root.$el.appendChild(componentInstance.$el);
    this.renderedComponent = componentInstance;
    if (this.hostComponent) {
      this.hostComponent.$emit('open', this);
    }
  }

  close() {
    if (this.renderedComponent) {
      this.renderedComponent.$destroy();
      const { $el } = this.renderedComponent;
      this.context.$root.$el.removeChild($el);
      this.renderedComponent = null;
      if (this.hostComponent) {
        this.hostComponent.$emit('close', this);
      }
    }
  }

  private render(h: CreateElement): VNode {
    const { left, right, top, bottom }: ClientRect = this.hostEl.getBoundingClientRect();
    const openRight = window.innerWidth - right > left;
    const horizontalPositioning = openRight
      ? { left: `${left}px` }
      : { right: `${document.body.clientWidth - window.pageXOffset - right}px` };
    const openDown = window.innerHeight - bottom > top;
    const verticalPositioning = openDown
      ? { top: `${window.pageYOffset + bottom}px` }
      : { top: 'auto', bottom: `${document.body.clientHeight - window.pageYOffset - top}px` };

    return h(this.component, {
      on: {
        close: () => this.close(),
      },
      style: {
        ...verticalPositioning,
        ...horizontalPositioning,
        position: 'absolute',
        zIndex: 999999,
      },
      transition: {
        name: openDown ? 'tbSlideFade' : 'btSlideFade',
        appear: true,
      },
    });
  }
}

const openoverDirective: DirectiveOptions = {
  bind(el, binding, vnode) {
    if (!vnode.context) {
      throw new Error('The component context of a openover directive is not defined!');
    }
    const componentName = binding.expression;
    const component = (vnode.context.$options.components || {})[componentName] as VueConstructor;
    if (!component) {
      throw new Error(`The context of the v-openover directive does not have a component registered as ${componentName}`);
    }
    const id = OpenOver.createId();
    const openover = new OpenOver(vnode.context, el, component, (binding.arg || 'click').split(':'), vnode.componentInstance);
    el.dataset.openoverId = id;
    OpenOver.registry.set(id, openover);
  },

  inserted(el, binding, vnode) {
    const id = el.dataset.openoverId;
    const openover = OpenOver.registry.get(id!);
    if (!(openover instanceof OpenOver)) {
      throw new Error(`OpenOver instance registry has no instance with the id "${id}"!`);
    }

    openover.listen();
  },

  unbind(el) {
    const id = el.dataset.openoverId;
    const openover = OpenOver.registry.get(id!);
    if (!(openover instanceof OpenOver)) {
      throw new Error(`OpenOver instance registry has no instance with the id "${id}"!`);
    }
    OpenOver.registry.delete(id!);
    openover.close();
  },
};

export default openoverDirective;
