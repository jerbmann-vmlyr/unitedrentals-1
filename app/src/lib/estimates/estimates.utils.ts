import { EstimateTotals } from './estimates.types';

export class ChargeEstimateUtils {
  static calculateSumTotal({ pickupFee, deliveryFee, damageWaiverFee, environmentFee, tax, totalRentalAmount, miscellaneousFees }: EstimateTotals): number {
    return pickupFee + deliveryFee + damageWaiverFee + environmentFee + tax + totalRentalAmount + miscellaneousFees;
  }
}
