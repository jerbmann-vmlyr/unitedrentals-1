export default class Transaction {
  constructor({
    approver, transId, transType, requesterName, requisitionId,
  } = {}) {
    this.transId = transId;
    this.transType = transType;
    this.requisitionId = requisitionId;
    this.requesterName = requesterName;
    this.approver = approver;
  }

  isReservation() {
    return !!this.transId;
  }

  isQuote() {} // eslint-disable-line

  isRequisition() {} // eslint-disable-line
}

export const TRANSACTION_TYPE_QUOTE = 'X';
export const TRANSACTION_TYPE_RESERVATION = 'R';
