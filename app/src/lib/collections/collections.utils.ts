import { HasDefault, HasId } from './collections.types';

export class CollectionUtils {

  /**
   * Verifies that at least one item in the last has
   * property `isDefault` set to true.
   */
  static validateHasDefault<T extends HasDefault>(
    items: T[],
  ): T[] {
    if (!items.some(g => g.isDefault === true)) {
      throw new Error(`At least one object of type Grouper must have a property isDefault set to true!`);
    }
    return items;
  }

  /**
   * Utility function for selecting a single item by ID from a list whose items
   * are of a type that extends the HasDefault and HasId types.
   */
  static selectOne<ID extends string, T extends HasDefault & HasId<ID>>(
    items: T[],
    id?: string,
  ): T {
    if (!Array.isArray(items) || items.length < 1) {
      throw new Error(`Cannot select from an undefined or empty list!`);
    }

    const matchById = items.find(i => i.id === id);
    if (matchById) return matchById;

    const matchByDefault = items.find(i => i.isDefault === true);
    if (matchByDefault) return matchByDefault;

    return items[0];
  }
}
