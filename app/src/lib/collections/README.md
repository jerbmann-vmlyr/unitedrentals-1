> This is our generic framework for applying complex operations to collections (lists) of data. It's the successor to the Listings framework.

# Collection Filtering

We have a lot of different kinds of filters. They're listed below in ascending order of implementation complexity.


## Predicate Filters

## Static Filters

## Dynamic Filters

## Search Filters
