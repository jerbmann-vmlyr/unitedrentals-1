import { HasLabel, HasId, HasDefault } from '../collections.types';

export enum SortDirection {
  Ascending = 'asc',
  Descending = 'desc',
}

export type SortFunction<T> = (items: T[]) => T[];

export type Sorter<T, ID extends string> = HasId<ID> & Partial<HasLabel> & HasDefault & Readonly<{
  sort: SortFunction<T>;
}>;
