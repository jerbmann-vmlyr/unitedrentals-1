import { CollectionUtils } from '../collections.utils';
import { Group } from '../group';
import { Sorter, SortDirection } from './sorts.types';

export class SortUtils {
  static validate<T, ID extends string>(
    sortrs: Sorter<T, ID>[],
  ): Sorter<T, ID>[] {
    return CollectionUtils.validateHasDefault(sortrs);
  }

  static applySorterToGroups<T, GID extends string, SID extends string>(
    sorter: Sorter<T, SID>,
    direction: SortDirection,
    groups: Group<T, GID>[],
  ): Group<T, GID>[] {
    if (!SortUtils.isSortDirection(direction)) {
      console.error(`"${direction}" is not a valid sort direction.`);
      direction = SortDirection.Ascending;
    }

    return groups.map(group => {
      // Use a COPY of the items array to prevent accidental mutation
      const sortedItems = sorter.sort([...group.items]);

      return {
        ...group,
        items: direction === SortDirection.Ascending
          ? sortedItems
          : sortedItems.reverse(),
      };
    });
  }

  static isSortDirection(value: any): value is SortDirection {
    return Object.values(SortDirection).includes(value);
  }
}
