import { Tokenizer } from '../search';
import { HasLabel, HasId } from '../collections.types';


/**
 * Atomic filter types
 */
export enum FilterType {
  Simple = 'simple',
  Static = 'static',
  Dynamic = 'dynamic',
  Search = 'search',
}

export type FilterFunction<T> = (item: T) => boolean;
export type HasFilterFunction<T> = { filter: FilterFunction<T> };

export type FilterPrimitive<T> = HasFilterFunction<T> & HasLabel & Readonly<{
  queryValue: string | number | boolean;
}>;

/**
 * Simple filters
 */
export type SimpleFilter<T, ID extends string> = HasId<ID> & FilterPrimitive<T> & Readonly<{
  type: FilterType.Simple;
}>;

/**
 * Static filters
 */
export type StaticFilter<T, ID extends string> = HasId<ID> & HasLabel & Readonly<{
  type: FilterType.Static;
  filters: FilterPrimitive<T>[];
}>;

/**
 * Dynamic filters
 */
export type DynamicFilter<T, ID extends string> = HasId<ID> & HasLabel & Readonly<{
  type: FilterType.Dynamic;
  valueFactory: (item: T[]) => FilterPrimitive<T>[];
  filterFactory: (queryValue: any) => FilterFunction<T>;
}>;

/**
 * Search filters
 */
export type SearchFilter<T, ID extends string> = HasId<ID> & HasLabel & Readonly<{
  type: FilterType.Search;
  tokenizer: Tokenizer<T>;
}>;

/**
 * Merged filter type
 */
export type Filter<T, ID extends string>
  = SimpleFilter<T, ID>
  | StaticFilter<T, ID>
  | DynamicFilter<T, ID>
  | SearchFilter<T, ID>;

/**
 * Parsed filter type
 */
export type ParsedFilter<T, ID extends string>
  = Filter<T, ID>
  & HasFilterFunction<T>
  & { filters?: FilterPrimitive<T>[] };
