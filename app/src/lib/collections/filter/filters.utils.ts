import {
  DynamicFilter,
  Filter,
  FilterFunction,
  FilterType,
  HasFilterFunction,
  ParsedFilter,
  SearchFilter,
  SimpleFilter,
  StaticFilter,
} from './filters.types';

export class FilterUtils {

  static applyFilters<T, ID extends string>(
    filters: HasFilterFunction<T>[],
    items: T[],
  ): T[] {
    return items.filter(eqp => filters.every(f => f.filter(eqp)));
  }

  static selectStatic<T, ID extends string>(filters: Filter<T, ID>[]): StaticFilter<T, ID>[] {
    return filters.filter((f): f is StaticFilter<T, ID> => f.type === FilterType.Static);
  }

  static selectSimple<T, ID extends string>(filters: Filter<T, ID>[]): SimpleFilter<T, ID>[] {
    return filters.filter((f): f is SimpleFilter<T, ID> => f.type === FilterType.Simple);
  }

  static selectDynamic<T, ID extends string>(filters: Filter<T, ID>[]): DynamicFilter<T, ID>[] {
    return filters.filter((f): f is DynamicFilter<T, ID> => f.type === FilterType.Dynamic);
  }

  static selectSearch<T, ID extends string>(filters: Filter<T, ID>[]): SearchFilter<T, ID>[] {
    return filters.filter((f): f is SearchFilter<T, ID> => f.type === FilterType.Search);
  }

  static mergeFilterPrimitives<T>(primitives: HasFilterFunction<T>[]): FilterFunction<T> {
    return primitives.length < 1
      ? () => true
      : item => primitives.map(f => f.filter).some(fn => fn(item));
  }

  /**
   * Given a list of predefined filter definition objects and an object
   * representing the currently-applied query parameters in the URL,
   * this function returns a list of the matching filters that should
   * be applied to the underlying collection data.
   *
   * For the more complex filter types, like static and dynamic, their
   * individually-defined filter functions need to be merged into a single
   * predicate function by calling `mergeFilterPrimitives`. A filter object
   * that has a merged filter function property is considered "parsed", hence
   * this function's return type of `ParsedFilter`.
   */
  static parseFiltersFromQuery<T, ID extends string>(
    allFilters: Filter<T, ID>[],
    query: any = {},
  ): ParsedFilter<T, ID>[] {

    return allFilters
      .filter(f =>
        Object.keys(query).includes(f.id)
        && Array.isArray(query[f.id])
        && query[f.id].length > 0,
      )
      .map(f => {
        const queryValues: any[] = query[f.id] || [];

        switch (f.type) {
          /* Simple filter transformation */
          case FilterType.Simple: {
            return f;
          }

          /* Static filter transformation */
          case FilterType.Static: {
            const filters = f.filters.filter(({ queryValue }) => queryValues.includes(queryValue));

            return {
              ...f,
              filters,
              filter: FilterUtils.mergeFilterPrimitives(filters),
            };
          }

          /* Dynamic filter transformation */
          case FilterType.Dynamic: {
            const filters = queryValues.map(queryValue => ({
              queryValue,
              label: queryValue,
              filter: f.filterFactory(queryValue),
            }));

            return {
              ...f,
              filters,
              filter: FilterUtils.mergeFilterPrimitives(filters),
            };
          }

          /* Search filter transformation */
          case FilterType.Search: {
            /**
             * If a search token in the query values array is prefixed with `*`,
             * it's considered a broad search token, which means that the token
             * only needs to be a _partial_ match. Otherwise, the token needs
             * to match one of the item's tokenized values exactly.
             */
            const filters = queryValues.map((queryValue: string) => {
              const sanitized = queryValue.replace(/\*/g, '');
              return {
                queryValue,
                label: sanitized,
                filter: (item: T) => !queryValue.startsWith('*')
                  ? f.tokenizer(item).includes(queryValue)
                  : f.tokenizer(item).some(token => token.includes(sanitized)),
              };
            });

            return {
              ...f,
              filters,
              filter: FilterUtils.mergeFilterPrimitives(filters),
            };
          }
        }
      });
  }
}
