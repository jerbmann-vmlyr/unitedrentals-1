import { ForwardIndex, Keygen, Tokenizer, InvertedIndex } from './search.types';

export class SearchUtils {

  static uniqueTokens(forwardIndex: ForwardIndex): string[] {
    return Array.from(new Set(
      Object.values(forwardIndex).reduce((ts, t) => [ ...ts, ...t], []),
    ));
  }

  static buildForwardIndex<T>(
    keygen: Keygen<T>,
    tokenizer: Tokenizer<T>,
    items: T[],
  ): ForwardIndex {
    return items.reduce(
      (index, item) => ({
        ...index,
        [keygen(item)]: tokenizer(item),
      }),
      {},
    );
  }

  static invertForwardIndex<T>(
    forwardIndex: ForwardIndex,
  ): InvertedIndex {
    const tokens = SearchUtils.uniqueTokens(forwardIndex);

    const tokenKeyIndexInit: InvertedIndex = tokens.reduce(
      (index, token) => ({ ...index, [token]: [] }),
      {},
    );

    return Object.entries(forwardIndex)
      .reduce<[string, string][]>(
        (pairs, [key, tokens]) => [
          ...pairs,
          ...tokens.map<[string, string]>(token => [key, token]),
        ],
        [],
      )
      .reduce(
        (index, [key, token]) => ({
          ...index,
          [token]: [ ...index[token], key ],
        }),
        tokenKeyIndexInit,
      );
  }

  /**
   * This function takes the above three utility methods and
   * smashes them into a single method. Necessary for large datasets
   * to avoid performance bottlenecks.
   */
  static fastBuildInvertedIndex<T>(
    keygen: Keygen<T>,
    tokenizer: Tokenizer<T>,
    items: T[],
  ): InvertedIndex {
    const forwardIndex: ForwardIndex = {};
    const invertedIndex: InvertedIndex = {};

    items.forEach(item => {
      const key = keygen(item);
      const tokens = tokenizer(item);

      forwardIndex[key] = tokens;
      tokens.forEach(token => {
        invertedIndex[token] = [];
      });
    });


    Object.keys(forwardIndex).forEach(key => {
      forwardIndex[key].forEach(token => {
        invertedIndex[token].push(key);
      });
    });

    return invertedIndex;
  }
}
