import { Tokenizer, Keygen, Result, InvertedIndex } from './search.types';
import { SearchUtils } from './search.utils';

export class SearchHelper<T> {
  private readonly invertedIndex: InvertedIndex;
  private readonly tokens: string[];
  private readonly resultCache = new Map<string, Result[]>();

  static unique<T>(values: T[]): T[] {
    return Array.from(new Set<T>(values));
  }

  constructor(
    private readonly tokenizer: Tokenizer<T>,
    private readonly keygen: Keygen<T>,
    private readonly items: T[],
  ) {
    this.invertedIndex = SearchUtils.fastBuildInvertedIndex(this.keygen, this.tokenizer, this.items);
    this.tokens = Object.keys(this.invertedIndex);
  }

  search(query = ''): Result[] {
    // Check the results cache before processing query
    if (this.resultCache.has(query)) {
      return this.resultCache.get(query)!;
    }
    // Otherwise, process the query then cache and return the results
    else {
      const matchingTokens = this.tokens.filter(token => token.includes(query));

      const results: Result[] = matchingTokens.map(token => ({
        keys: SearchHelper.unique(this.invertedIndex[token]),
        isBroad: false,
        query,
        token,
      }));

      if (results.length > 0) {
        results.unshift({
          keys: SearchHelper.unique(matchingTokens.reduce<string[]>((accum, token) => [...accum, ...this.invertedIndex[token]], [])),
          isBroad: true,
          query,
          token: `*${query}`,
        });
      }

      this.resultCache.set(query, results);
      return results;
    }
  }
}
