# Collection Search

The files contained in this directory provide the tools and functionality necessary when you have a large set of data (IE, a collection), and the user needs to be able to quickly find items in that data, Google-style.

Our implementation of search functionality really is just a primitive search engine. If you're not familiar with how search engines and indexing works, I'd suggest [taking a gander at this Wikipedia page.](https://en.wikipedia.org/wiki/Search_engine_indexing)

Specifically, there are 3 concepts to be familiar with:
1. [Tokenization](https://en.wikipedia.org/wiki/Search_engine_indexing#Tokenization)
2. [Forward indices](https://en.wikipedia.org/wiki/Search_engine_indexing#The_forward_index)
3. [Inverted indices](https://en.wikipedia.org/wiki/Search_engine_indexing#Inverted_indices)

I'll try to provide a brief explanation of these terms below.

> NOTE: I try to be concise in terminology, but note that when I use the word "data", I mean the whole collection or list of data that we're working with. When I say "item", I mean an item within that collection.

## Inverted indices

TODO

## Forward indices

TODO
