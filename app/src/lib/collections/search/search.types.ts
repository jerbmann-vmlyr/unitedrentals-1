/**
 * Check out the ./README.md file for an explanation of these interfaces!
 */
export type Keygen<T> = (item: T) => string;
export type Tokenizer<T> = (item: T) => string[];
export type InvertedIndex = { [key: string]: string[] };
export type ForwardIndex = { [token: string]: string[] };

export interface Result {
  isBroad: boolean;
  keys: string[];
  query: string;
  token: string;
}
