export type HasId<ID extends string> = Readonly<{ id: ID }>;
export type HasDefault = Readonly<{ isDefault?: boolean }>;
export type HasLabel = { label: string | string[] };
