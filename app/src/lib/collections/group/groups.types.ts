import { HasLabel, HasId, HasDefault } from '../collections.types';

/**
 * Atomic group types
 */
export type GroupClassifier<T> = (item: T) => string;
export type GroupLabeler<T> = (classification: string, items: T[]) => string;


/**
 * Grouper and Group output type
 */
export type Grouper<T, ID extends string> = HasId<ID> & HasLabel & HasDefault & Readonly<{
  classifier: GroupClassifier<T>;
  classificationLabel: GroupLabeler<T>;
}>;

export type Group<T, ID extends string> = HasId<ID> & HasLabel & Readonly <{
  classification: string;
  index: { this: number; total: number };
  length: { this: number; total: number };
  items: T[];
}>;
