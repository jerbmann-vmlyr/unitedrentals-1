import { sortBy } from 'lodash';
import { Grouper, Group } from '@/lib/collections';
import { CollectionUtils } from '../collections.utils';

export class GroupUtils {

  static validate<T, ID extends string>(
    groupers: Grouper<T, ID>[],
  ): Grouper<T, ID>[] {
    return CollectionUtils.validateHasDefault(groupers);
  }

  static applyGrouper<T, ID extends string>(
    grouper: Grouper<T, ID>,
    items: T[],
  ): Group<T, ID>[] {
    const groups: { [groupKey: string]: T[] } = items.reduce((grouped, item) => {
      const groupKey = grouper.classifier(item);
      return {
        ...grouped,
        [groupKey]: [...(grouped[groupKey] || []), item],
      };
    }, {});

    const groupCount = Object.keys(groups).length;
    const groupList = Object.entries(groups).map<Group<T, ID>>(([groupKey, groupItems], index, list) => {
      return {
        id: grouper.id,
        classification: groupKey,
        label: grouper.classificationLabel(groupKey, groupItems),
        index: { this: index, total: groupCount },
        length: { this: groupItems.length, total: items.length },
        items: groupItems,
      };
    });

    return sortBy(groupList, 'label');
  }

}
