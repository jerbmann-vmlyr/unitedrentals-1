import numeral from 'numeral';

function formatted(value, formatString) {
  return numeral(value).format(formatString);
}

export default n => formatted(n, '0,0');
