
// Adds parentheses to phone numbers as needed
export default (phoneNumber) => {
  phoneNumber = String(phoneNumber);

  if (phoneNumber.includes('(')) {
    return phoneNumber;
  }

  const areaCode = phoneNumber.slice(0, 3);
  const phone = phoneNumber.slice(4);

  return `(${areaCode}) ${phone}`;
};

export const telLink = phoneNumber => `tel:${String(phoneNumber).replace(/\D/g, '')}`;
