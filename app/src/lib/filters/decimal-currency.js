export default function wholeNumberCurrency(num) {
  // Allow type Number and type String that can be cast as Number. Otherwise return dashes.
  if (Number.isNaN(Number(num))) {
    return '--';
  }

  const strComma = Number(num).toFixed(2).toString().split('.');

  if (strComma[0].length >= 4) {
    const str = strComma[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return strComma.length > 1 ? `$${str}.${strComma[1]}` : `$${str}`;
  }

  const price = Number(num).toFixed(2);
  return `$${price}`;
}
