export default function wholeNumberCurrencyDefaultZero(num) {
  // Allow type Number and type String that can be cast as Number. Otherwise return dashes.
  if (Number.isNaN(Number(num))) {
    return '$0.00';
  }

  const str = Number(num).toFixed(2).toString().split('.');
  if (str[0].length >= 4) {
    str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
  return `$${str[0]}`;
}
