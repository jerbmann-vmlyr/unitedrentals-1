/**
 * Returns a substring of the text w/an ellipses (if and only if the string was shortened)
 * @param {string} text the original text
 * @param {number} length the length of the substring to return
 * @return {string} a string respecting a length, potentially shortened with an ellipses
 */
export default function ellipses(text, length) {
  if (!text) {
    return '';
  }
  return `${text.substring(0, length)}${text.length > length ? '…' : ''}`;
}
