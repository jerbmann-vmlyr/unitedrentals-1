const ccMonthYear = (monthYear) => {
  const month = String(monthYear).slice(0, -2);
  const year = String(monthYear).slice(-2);
  return `${month}/${year}`;
};

const ccTypeName = (cardType) => {
  const cardLowerCase = (cardType || '').toLowerCase();
  switch (cardLowerCase) {
    case 'm':
      return 'Mastercard';
    case 'v':
      return 'Visa';
    case 'a':
      return 'American Express';
    case 'd':
      return 'Discover';
    default:
      return '';
  }
};

const ccTypeImg = (cardType) => {
  const cardLowerCase = (cardType || '').toLowerCase();
  switch (cardLowerCase) {
    case 'm':
      return '/themes/custom/unitedrentals/images/icons/src/card-type-mastercard.svg';
    case 'v':
      return '/themes/custom/unitedrentals/images/icons/src/card-type-visa.svg';
    case 'a':
      return '/themes/custom/unitedrentals/images/icons/src/card-type-amex.svg';
    case 'd':
      return '/themes/custom/unitedrentals/images/icons/src/card-type-discover.svg';
    default:
      return '';
  }
};

export {
  ccMonthYear,
  ccTypeName,
  ccTypeImg,
};
