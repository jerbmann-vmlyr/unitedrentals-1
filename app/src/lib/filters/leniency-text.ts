import * as moment from 'moment';
import { DateFormat } from '@/lib/constants';
import { MomentUtils } from '@/lib/moment.utils';

type WithLeniencyDates<T extends object> = T & { leniencyStartDate: string, leniencyEndDate: string};

enum WorkplaceEquipmentLeniencyState {
  Passed = 'passed',
  Started = 'started',
  NotStarted = 'not-started',
}

export const datesToLeniencyText = ({ leniencyStartDate, leniencyEndDate }: WithLeniencyDates<any>): string => {
  const leniencyState = MomentUtils.happensInThePast(leniencyEndDate)
    ? WorkplaceEquipmentLeniencyState.Passed
    : MomentUtils.happensTodayOrInThePast(leniencyStartDate)
      ? WorkplaceEquipmentLeniencyState.Started
      : WorkplaceEquipmentLeniencyState.NotStarted;

  switch (leniencyState) {
    case WorkplaceEquipmentLeniencyState.Passed: {
      return '';
    }
    case WorkplaceEquipmentLeniencyState.Started: {
      const formatted = moment(leniencyEndDate, DateFormat.IntDate).format(DateFormat.Friendly);
      return `In leniency until ${formatted}`;
    }
    default:
      const formatted = moment(leniencyStartDate, DateFormat.IntDate).format(DateFormat.Friendly);
      return `Leniency starts ${formatted}`;
  }
};
