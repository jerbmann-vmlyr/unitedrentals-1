export enum DateFormat {
  Friendly = 'MMM D, YYYY',
  FriendlyHour = 'h:mma',
  IntDate = 'YYYYMMDD',
  Invoice = 'MMM Do, YYYY',
  Iso = 'YYYY-MM-DD\THH:mm:ss',
  RentalTimeline = 'MM/DD/YY',
  Slash = 'MM/DD/YYYY',
}
