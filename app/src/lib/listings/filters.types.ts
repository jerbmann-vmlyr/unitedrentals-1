export enum ListFilterType {
  SearchByTerm = 'SEARCH_BY_TERM',
  View = 'VIEW',
}

export interface ListingFilter<T = any, I extends string = string> {
  readonly type: ListFilterType;
  readonly id: I;
  asFunction: (item: T) => boolean;
}

/**
 * View (predicate) filters
 */
export interface ViewFilter<T = any, U extends string = string> extends ListingFilter<T, U> {
  readonly type: ListFilterType.View;
  readonly label: string;
}

/**
 * Search-by-term filters
 */
export interface SearchByTermFilter<T = any> extends ListingFilter<T> {
  term: string;
  query: string;
  itemKey: keyof T;
  matchingItemKeyValues: (T[keyof T])[];
}
export type SearchByTermLexicon = (item: any) => string[];
