# Listings

## Filters

### Filter utility functions

#### `getFilterDescriptorPossibleValues`

*Purpose:*

Given a filter descriptor and a list of items, this function will return a list of possible values and the labels to be used for those values.

*Example:*

In our UI we're already showing the user a long list of developers. The list takes too long to scroll through, so above the list, we want to show the user a set of checkboxes whose values and labels are the various job titles of the developers in the list. Each checkbox represents a filter for the list; when the user checks a job title, only developers with that job title will be visible. But, our developer list data is dynamic and comes from an opaque API, so we don't have the benefit of knowing all of the possible job titles in advance.

This means that given a list of developers, we need to create our job title filter and generate all of the possible job title filter values on-the-fly. That's where `getFilterDescriptorPossibleValues` comes in.

Here is the interface for a `Developer`:

```
interface Developer {
  name: string;
  jobTitle: string;
}
```

This is the list of developers that came back from an API. Remember, we don't get to see this data in advance:

```
const developers: Developer[] = [
  { name: 'Alex Eden', jobTitle: 'Developer' },
  { name: 'Ryan Brewer', jobTitle: 'Lead Developer' },
  { name: 'Luke Hovee', jobTitle: 'Developer' },
];
```

Here is the filter descriptor we would define that'd allow us to filter `Developer`s by `jobTitle`. Filter descriptors have a type of `FilterByDescriptor`, with an optional generic type that specifies the type of the object being filtered. In this case, we're filtering `Developer` objects, so our filter descriptor type is `FilterByDescriptor<Developer>`.

```
const filterDevelopersByTitle: FilterByDescriptor<Developer> = {
  type: 'filterBy',
  id: 'jobTitle',
  label: 'Developer Title',
  valueGenerator: ({ jobTitle }) => [{ label: jobTitle, value: jobTitle }],
};
```

Now all we need to do is get the labels and values for our filter checkboxes, which we'll assign to the `jobTitleFilters` property of the component we're creating to display the filter checkboxes:

```
this.jobTitleFilters = ListingUtils.getFilterDescriptorPossibleValues(
  filterDevelopersByTitle,
  developers
);
```

The above invocation will return this (redacted for simplicity) data:

```
[
  {
    "descriptor": (copy of descriptor provided [filterDevelopersByTitle]),
    "label": "Developer",
    "value": "Developer"
  },
  {
    "descriptor": (copy of descriptor provided [filterDevelopersByTitle]),
    "label": "Lead Developer",
    "value": "Lead Developer"
  }
]
```

At this point, all that's left is the templating:

```
<div v-for="possbility in jobTitleFilters">
  <input type="checkbox" :value="possbility.value" @input="descriptorHandler(possbility.descriptor)">
  <label>{{ possbility.label }}</label>
</div>
```
