import moment from 'moment';
import utils from '@/utils';

export default class CartItem {
  constructor({
    accountId,
    branchId,
    catClass,
    changed,
    delivery,
    endDate,
    id,
    jobsiteId,
    locationString,
    notes,
    pickup,
    projectId,
    quantity,
    startDate,
    requesterId,
  } = {}) {
    this.accountId = accountId;
    this.branchId = branchId;
    this.catClass = catClass;
    this.changed = changed;
    this.endDate = endDate instanceof moment ? endDate : moment(endDate, utils.dateFormat.iso);
    this.id = id || makeUid();
    this.jobsiteId = jobsiteId;
    this.locationString = locationString;
    this.notes = notes;
    this.quantity = Number(quantity) || 1;
    this.projectId = projectId;
    this.requesterId = requesterId;
    this.startDate = startDate instanceof moment ? startDate : moment(startDate, utils.dateFormat.iso);
    this.delivery = delivery;
    this.pickup = pickup;
  }

  export() {
    const {
      accountId,
      branchId,
      catClass,
      changed,
      delivery,
      endDate,
      id,
      jobsiteId,
      locationString,
      notes,
      pickup,
      projectId,
      quantity,
      requesterId,
      startDate,
    } = this;
    return JSON.stringify({
      accountId,
      branchId,
      catClass,
      changed,
      delivery,
      endDate: endDate.format(utils.dateFormat.iso),
      id,
      jobsiteId,
      locationString,
      notes,
      pickup,
      projectId,
      quantity,
      requesterId,
      startDate: startDate.format(utils.dateFormat.iso),
    });
  }

  exportAsEstimateApiParams() {
    return {
      branchId: this.branchId,
      items: {
        [this.catClass]: {
          quantity: this.quantity,
        },
      },
      delivery: this.delivery,
      pickup: this.pickup,
      returnDate: this.endDate.format(utils.dateFormat.iso),
      rpp: false,
      startDate: this.startDate.format(utils.dateFormat.iso),
      ...this._jobsiteLocationEstimateApiParams(),
    };
  }

  static getMomentFromSerializedMomentString(date) {
    return moment.utc(date).local();
  }

  _jobsiteLocationEstimateApiParams() {
    const { accountId, jobsiteId } = this;
    return accountId && jobsiteId
      ? ({ accountId, jobsiteId })
      : this._locationStringToObject();
  }

  // Given something like "Kansas City, MO 64116", get an object.
  _locationStringToObject() {
    // This try catch was added to fix the Cart of any PRODUCTION, AUTHENTICATED USER
    // who had experienced the UR-5551 bug (which permanently bricked their cart).
    // This bug, however, appears to have been rare, and only happened when a user
    // took an awkward, edge-case journey into the cart.
    try {
      const [city, stateAndpostalCode] = this.locationString.split(',');
      const [state, postalCode = ''] = stateAndpostalCode.trim().split(' ');
      return {
        address: '',
        city,
        state,
        postalCode,
      };
    }
    catch (e) {
      console.error(e); // eslint-disable-line
      return {
        address: '',
        city: '',
        state: '',
        postalCode: '',
      };
    }
  }
}

export const makeUid = (() => {
  let uid = Math.round(Math.random() * 1000000);
  return () => String(uid++);
})();
