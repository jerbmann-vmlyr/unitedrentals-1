import { YesOrNo } from '../common-types';

export enum Progress {
  INACTIVE = 'inactive',
  READY = 'ready',
  ACTIVE = 'active',
  SUMMARY = 'summary',
  COMPLETE = 'complete',
}

export enum CheckoutSection {
  Billing = 'billing',
  Jobsite = 'jobsite',
  PickupDelivery = 'pickupDelivery',
}

export type ProgressBlock = {
  [K in CheckoutSection]: Progress
};

export interface ReservationBillingData {
    accountingCodes: string[];
    approverId: string;
    cardId: string;
    deliveryNote: string;
    poNumber: string;
    promotionCode?: string;
    requesterId: string;
    requisitionCodes: string[];
    rpp: YesOrNo;
    transType: 'R' | 'X'; // R is Reservation, X is Quote
}
