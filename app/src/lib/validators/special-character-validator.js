export default value => /^[a-zA-Z0-9\-\s,.']+$/.test(value);
