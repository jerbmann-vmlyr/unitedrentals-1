import { WorkplaceEquipmentColumnSettings, WorkplaceOrderColumnSettings } from '../workplace';

export interface UserPreferences {
  equipmentColumns: WorkplaceEquipmentColumnSettings[];
  orderColumns: WorkplaceOrderColumnSettings[];
  readonly equipmentColumnsVersion: { hash: number };
  readonly orderColumnsVersion: { hash: number };
  defaultAccountId: string;
  favoriteAccounts: string[];
  ratesPlace: google.maps.places.PlaceResult | {};
  ratesPlaceBranch: string;
}
