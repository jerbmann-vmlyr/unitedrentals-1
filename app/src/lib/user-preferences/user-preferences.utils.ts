export class UserPreferencesUtils {
  static calculateVersion<T extends object>(obj: T): number {
    const serial = JSON.stringify(obj);
    let hash = 0;
    for (const char of serial) {
      hash = ((hash << 5) - hash) + char.charCodeAt(0); // tslint:disable-line
      hash = hash & hash; // tslint:disable-line
    }
    return Math.abs(hash);
  }

  static defaultAccountId(): string {
    return '';
  }

  static ratesPlaceBranch(): string {
    return '';
  }
}
