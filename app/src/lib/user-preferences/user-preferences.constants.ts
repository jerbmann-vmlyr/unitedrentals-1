import { UserPreferences } from './user-preferences.types';
import { UserPreferencesUtils } from './user-preferences.utils';
import { WORKPLACE_EQUIPMENT_COLUMNS, WORKPLACE_ORDER_COLUMNS } from '../workplace';


export const USER_PREFERENCES_DEFAULTS: UserPreferences = {
  equipmentColumnsVersion: {
    hash: UserPreferencesUtils.calculateVersion(WORKPLACE_EQUIPMENT_COLUMNS),
  },
  equipmentColumns: WORKPLACE_EQUIPMENT_COLUMNS,
  orderColumnsVersion: {
    hash: UserPreferencesUtils.calculateVersion(WORKPLACE_ORDER_COLUMNS),
  },
  orderColumns: WORKPLACE_ORDER_COLUMNS,
  defaultAccountId: UserPreferencesUtils.defaultAccountId(),
  favoriteAccounts: [],
  ratesPlace: {},
  ratesPlaceBranch: UserPreferencesUtils.ratesPlaceBranch(),
};
