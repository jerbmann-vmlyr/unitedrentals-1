import { USER_PREFERENCES_DEFAULTS } from './user-preferences.constants';
import {
  WorkplaceEquipmentColumnGroupId,
  WorkplaceEquipmentColumnKey,
  WorkplaceEquipmentSorterId,
  WorkplaceOrderColumnGroupId,
  WorkplaceOrderColumnKey,
  WorkplaceOrderSorterId,
} from '../workplace';


describe('Workplace user preferences', () => {
  describe('equipment columns', () => {
    test('all equipment columns have unique keys', () => {
      const columnKeys = USER_PREFERENCES_DEFAULTS.equipmentColumns.map(col => col.columnKey);
      const uniqueKeys = new Set(columnKeys);
      expect(columnKeys.length).toEqual(uniqueKeys.size);
    });

    test('all equipment columns have a unique index', () => {
      const indices = USER_PREFERENCES_DEFAULTS.equipmentColumns.map(col => col.index);
      const uniqueIndices = new Set(indices);
      expect(indices.length).toEqual(uniqueIndices.size);
    });

    test('all defined equipment column keys are used', () => {
      const definedKeys = Object.values(WorkplaceEquipmentColumnKey);
      const usedKeys = USER_PREFERENCES_DEFAULTS.equipmentColumns.map(col => col.columnKey);
      definedKeys.forEach(key => {
        expect(usedKeys).toContain(key);
      });
    });

    test('all defined equipment column group keys are used', () => {
      const usedKeys = USER_PREFERENCES_DEFAULTS.equipmentColumns.map(col => col.group);
      const definedKeys = Object.values(WorkplaceEquipmentColumnGroupId);
      definedKeys.forEach(key => {
        expect(usedKeys).toContain(key);
      });
    });

    test('all defined equipment sort IDs are used', () => {
      const usedKeys = USER_PREFERENCES_DEFAULTS.equipmentColumns
        .filter(({ sortable }) => typeof sortable === 'string')
        .map(col => col.sortable);
      const definedKeys = Object.values(WorkplaceEquipmentSorterId);
      definedKeys.forEach(key => {
        expect(usedKeys).toContain(key);
      });
    });
  });

  describe('order columns', () => {
    test('all order columns have unique keys', () => {
      const columnKeys = USER_PREFERENCES_DEFAULTS.orderColumns.map(col => col.columnKey);
      const uniqueKeys = new Set(columnKeys);
      expect(columnKeys.length).toEqual(uniqueKeys.size);
    });

    test('all order columns have a unique index', () => {
      const indices = USER_PREFERENCES_DEFAULTS.orderColumns.map(col => col.index);
      const uniqueIndices = new Set(indices);
      expect(indices.length).toEqual(uniqueIndices.size);
    });

    test('all defined order column keys are used', () => {
      const definedKeys = Object.values(WorkplaceOrderColumnKey);
      const usedKeys = USER_PREFERENCES_DEFAULTS.orderColumns.map(col => col.columnKey);
      definedKeys.forEach(key => {
        expect(usedKeys).toContain(key);
      });
    });

    test('all defined order column group keys are used', () => {
      const usedKeys = USER_PREFERENCES_DEFAULTS.orderColumns.map(col => col.group);
      const definedKeys = Object.values(WorkplaceOrderColumnGroupId);
      definedKeys.forEach(key => {
        expect(usedKeys).toContain(key);
      });
    });

    test('all defined order sort IDs are used', () => {
      const usedKeys = USER_PREFERENCES_DEFAULTS.orderColumns
        .filter(({ sortable }) => typeof sortable === 'string')
        .map(col => col.sortable);
      const definedKeys = Object.values(WorkplaceOrderSorterId);
      definedKeys.forEach(key => {
        expect(usedKeys).toContain(key);
      });
    });
  });
});
