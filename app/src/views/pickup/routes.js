import UrModal from '@/components/common/ur-modal.vue';

import RequestPickupModal from '@/components/workplace/request-pickup/pickup-request.vue';
import RequestPickupConfirmationModal from '@/components/workplace/request-pickup/pickup-confirmation.vue';

export default [
  {
    path: 'pickup/reschedule/:rescheduleType',
    name: 'workplace:pickup:request-reschedule',
    component: UrModal,
    props: route => ({
      content: RequestPickupModal,
      contentProps: {...route.params},
    }),
  },
  {
    path: 'pickup/confirm/:rescheduleType',
    name: 'workplace:pickup:confirm-reschedule',
    component: UrModal,
    props: route => ({
      content: RequestPickupConfirmationModal,
      contentProps: {...route.params},
    }),
  },
];
