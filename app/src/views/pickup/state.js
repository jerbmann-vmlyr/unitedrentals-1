import { assignState, simpleSetters, instanceMutations } from 'spyfu-vuex-helpers';

import { get, head, isEmpty, isString, forEach } from 'lodash';

import WorkplaceEquipmentApi, { } from '@/api/api.workplace-equipment';
import api from '@/api';
import types from '@/store/types';
import { ApiV2Utils } from '@/lib/apiv2';
import * as moment from 'moment';
import DateTime from '@/utils/dateTime';

const type = types.requestPickup;

const stateFactory = () => ({
  all: {},
  accountId: '',
  transId: '',
  dataLoaded: false,
  address: {
    address1: '',
    address2: '',
    city: '',
    state: '',
    zip: '',
  },
  branch: null,
  confirmationNumber: '',
  contact: {
    contact: '',
    email: '',
    mobilePhone: '',
    phone: '',
  },
  emailRecipients: [],
  equipmentList: [],
  instructions: {
    accessTimes: '',
    additional: '',
    equipmentLocation: '',
    keyLocation: '',
  },
  requestIsInitialized: false,
  selectedDateTime: DateTime.round(moment(), moment.duration(30, 'minutes'), 'ceil'),
  startDate: DateTime.round(moment(), moment.duration(30, 'minutes'), 'ceil'),
  returnDate: DateTime.round(moment(), moment.duration(30, 'minutes'), 'ceil'),
  selectedEquipmentQtys: [],
  headerData: {},
});
export const state = stateFactory();

export const actions = {
  async [type.initializePickupRequest]({commit, dispatch, getters, state}, data) {
    // Populate the list of equipment
    const requestData = await dispatch(type.fetchRequestPickupData, data);
    const { detailData, headerData } = requestData;

    Vue.set(state, 'equipmentList', detailData);
    Vue.set(state, 'headerData', headerData);

    commit(type.saveAddress, {
      ...state.address,
      address1: headerData.jobsiteAddress1,
      city: headerData.jobsiteCity,
      state: headerData.jobsiteState,
      zip: headerData.jobsiteZip,
    });

    commit(type.saveContact, {
      ...state.contact,
      contact: headerData.jobsiteContact,
      phone: headerData.jobsitePhone,
    });


    const equipmentQtys = detailData.map(({ equipmentId, quantityAvailableForPickup }) => ({
      equipmentId,
      quantity: quantityAvailableForPickup, // select all by default
    }));
    commit(type.setSelectedEquipmentQtys, equipmentQtys);

    // Fetch the branch of the first item and save it to state
    // const branch = await dispatch(types.branch.fetchById, equipment[0].branchId);
    commit(type.mutate, state => state.branch = requestData.branch);
    // Set the request to initialized
    commit(type.mutate, state => state.requestIsInitialized = true);
  },

  async [type.fetchRequestPickupData](
    { commit }, data) {
    const requestPickup = await api.requestPickup.getAll(
      data.transId, data.equipmentId, data.phone, data.email, data.accountId)
      .then(ApiV2Utils.unwrapResponseData);
    return requestPickup;
  },
  async [type.submitPickupRequest]({ commit, getters, state }) {
    const request = getters[type.pickupRequestRequest];
    const response = await WorkplaceEquipmentApi.sendPickupRequest(request);
    commit(type.mutate, () => state.confirmationNumber = response.pickupId);
    return response;
  },

  [type.selectAllEquipment]({ state, dispatch }, selected = true) {
    forEach(state.equipmentList, ({ equipmentId }) => dispatch(type.setEquipmentSelectedById, {
      equipmentId,
      selected,
    }));
  },
  [type.unselectAllEquipment]({ dispatch }) {
    dispatch(type.selectAllEquipment, false);
  },

  [type.setEquipmentSelectedById]({ state, commit, getters }, { equipmentId, selected = true } = {}) {
    return commit(type.setSelectedEquipmentQtyById, {
      equipmentId,
      quantity: selected ? getters.getMaxQuantityById(equipmentId) : 0,
    });
  },
};

export const mutations = {
  ...simpleSetters({
    [type.saveAddress]: 'address',
    [type.saveContact]: 'contact',
    [type.saveInstructions]: 'instructions',
    [type.saveEmailRecipients]: 'emailRecipients',
    [type.setSelectedEquipmentQtys]: 'selectedEquipmentQtys',
    [type.setSelectedDateTime]: 'selectedDateTime',
  }),
  ...instanceMutations({ stateKey: 'selectedEquipmentQtys', instanceKey: 'equipmentId' }, {
    [type.setSelectedEquipmentQtyById](instance, payload) {
      instance.quantity = payload.quantity;
    },
  }),
  [type.reset]: assignState(stateFactory),
  [type.mutate](state, mutation) {
    mutation(state, null);
  },
};

export const getters = {
  allEquipmentIds: state => state.equipmentList.map(eqp => eqp.equipmentId),
  getEquipmentById: state => id => state.equipmentList.find(eqp => eqp.equipmentId === id),
  getMaxQuantityById: (state, getters) => id => getters.getEquipmentById(id).quantityAvailableForPickup,
  getSelectedQuantityById: state => id => state.selectedEquipmentQtys.find(eqp => eqp.equipmentId === id).quantity,
  [type.isPickup]: () => true,
  [type.accountId]: state => get(state, 'headerData.accountNumber', ''),
  [type.transId]: state => get(state, 'headerData.contractNumber', ''),
  [type.jobsite]: state => get(head(state.equipmentList), 'jobsite', {}),
  [type.originalStartDateTime]: state => state.headerData.startDateTime,
  [type.originalReturnDateTime]: state => state.headerData.returnDateTime,
  [type.equipmentListIsEmpty]: state => isEmpty(state.equipmentList),
  [type.accountName](state, getters, rootState) {
    return get(rootState.accounts.all, [getters[type.accountId], 'name'], '');
  },
  [type.allEquipmentIsSelected](state, getters) {
    return getters.allEquipmentIds.every(id => getters.getSelectedQuantityById(id) > 0);
    // return state.equipmentList.every(eqp => getters.getSelectedQuantityById(eqp.equipmentId) > 0);
    // return Object.values(state.selectedEquipmentQtys).every(qty => qty > 0);
  },
  [type.atLeastOneEquipmentIdSelected](state, getters) {
    return getters.allEquipmentIds.some(id => getters.getSelectedQuantityById(id) > 0);
    // return Object.values(state.selectedEquipmentQtys).some(qty => qty > 0);
  },
  [type.selectedEquipment](state, getters) {
    // return getters.allEquipmentIds.every(id => getters.getSelectedQuantityById(id) > 0);
    return state.equipmentList.filter(eqp => getters.getSelectedQuantityById(eqp.equipmentId) > 0);
  },
  [type.branchHoursOffset]({ branch }) {
    /** The timeOffset returned by the branch API is actually relative to UR HQ in NY */
    const correction = moment().isDST() ? 4 : 5;

    if (!branch || !Number.isSafeInteger(+branch.timeOffset)) return correction;
    const skewedOffset = Math.abs(Math.round(+branch.timeOffset / 100));
    return correction + skewedOffset;
  },
  [type.pickupRequestRequest](state, getters, rootState) {
    const pickupDateTime = moment.max(state.selectedDateTime.clone(), moment()).format('YYYY-MM-DDTHH:mm:ss');
    const transId = getters[type.transId];

    const normalizedPhone = state.contact.phone.replace(/\D/g, '');
    const phone = normalizedPhone.length === 10
      ? normalizedPhone.match(/^(\d{3})(\d{3})(\d{4})$/).slice(1, 4).join('-')
      : '';

    // Map the equipment quantities to their transaction IDs
    const quantitiesByTransId = Object.entries(state.selectedEquipmentQtys)
      .filter(([, quantity]) => quantity > 0)
      .reduce((accum, [equipmentId, quantity]) => {
        const { transLineId } = state.equipmentList.find(eqp => eqp.equipmentId === equipmentId);
        return { ...accum, [transLineId]: quantity };
      }, {});

    // Build the request objects
    return {
      ...state.address,
      pickupDateTime,
      quantity: Object.values(quantitiesByTransId).join(','),
      transId,
      transLineId: Object.keys(quantitiesByTransId).join(','),
      guid: rootState.tcUser.guid,
      contact: state.contact.contact,
      phone,
      requester: state.contact.contact,

      equipmentLocation: state.instructions.equipmentLocation,
      keyLocation: state.instructions.keyLocation,
      accessTimes: state.instructions.accessTimes,
      moreInstructions: state.instructions.additional,

      emailAddresses: [state.contact.email, ...state.emailRecipients]
        .filter(email => isString(email) && !isEmpty(email))
        .join(';'),
    };
  },
};

export default {
  actions,
  getters,
  mutations,
  state,
};
