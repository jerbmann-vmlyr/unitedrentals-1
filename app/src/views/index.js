import Vue from 'vue';

function loadView(view) {
  return () => import(`@/views/${view}`);
}

Vue.component('view-manage', loadView('ViewManage.vue'));
Vue.component('view-pickup', loadView('pickup/ViewPickup.vue'));
