declare const drupalSettings: {
  ajaxTrustedUrl: string[];
  path: {
    baseUrl: string;
    scriptPath: string|null;
    pathPrefix: string;
    currentPath: string;
    currentPathIsAdmin: boolean;
    isFront: boolean;
    currentLanguage: 'en';
  }
  pluralDelimiter: string;
  ajaxPageState: {
    libraries: string;
    theme: string;
    theme_token: string|null;
  }
  history: {
    nodesToMarkAsRead: {
      [nodeId: string]: boolean;
    };
  }
  service_account_id: number;
  ur: {
    drupal: {
      uuid: string;
    };
    dal: {
      guid: string;
    };
    sign_in_register_path: string,
    change_password_path: string,
    forgot_password_path: string,
    translation: {
      english: string|null;
      spanish: string|null;
      french: string|null;
    };
    general_inquiries: string;
    rental_inquiries: string;
    location_branches: {
      [B in 'AR' | 'FS' | 'GR' | 'IR' | 'PR' | 'SS' | 'TS']: {
        branch_name: string;
        contact_us_link: string;
        rent_online_link: null;
        contact_us_link_show: string;
        rent_online_link_show: null;
      };
    };
  }
  urVoucher: boolean;
  user: {
    uid: string;
    isEmployee: boolean;
    permissionsHash: string;
    defaultAccountId: string;
  }
  vueRouter: boolean;
};


/**
 * Generic type helper for indicating that in a given context,
 * certain properties, specified by `K`, of a type `T` are optional.
 *
 * For example, given an interface describing a 4D coordinate:
 * ```
 * interface Coordinate4D {
 *   x: number;
 *   y: number;
 *   z: number;
 *   t: number;
 * }
 * ```
 * All properties: `x`, `y`, `z`, `t` are required. But assume we're in a
 * context where we don't need the `z` and `t` properties.
 *
 * We can make them optional using the `Optional<K, T>` helper:
 * ```
 * const coord2D: Optional<'z' | 't', Coordinate4D> = {
 *   x: 1,
 *   y: 2,
 * };
 * ```
 */
type Optional<K extends keyof T, T>
  = { [P in Exclude<keyof T, K>]: T[P] }
  & { [P in K]+?: T[P] };

type PartialExclude<K extends keyof T, T> = { [P in K]: T[P] } & Partial<T>;

type NonNullableProps<T> = {
  [P in keyof T]: NonNullable<T[P]>;
};

type Omit<K, T> = Pick<T, Exclude<keyof T, K>>;

/* This is necessary so typescript finds and can import your .vue files */
declare module '*.vue' {
  import Vue from 'vue';

  export default Vue;
}

/**
 * This allows us to import .json files into our .ts files
 * Typescript will recognize the import as having a type of "any"
 */
declare module '*.json' {
  const value: any;
  export default value;
}

/**
 * This allows us to import .html files into our .ts files
 * Typescript will recognize the import as having a type of "string"
 * so that we can use the import as a Vue component template
 */
declare module '*.html' {
  const template: string;
  export = template;
}
