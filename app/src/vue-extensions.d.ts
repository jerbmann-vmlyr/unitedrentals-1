/**
 * This is where we can extend Vue with custom properties.
 * We can extend Vue itself, Vue components, etc.
 */
import Vue, { VueConstructor } from 'vue';

declare module 'vue/types/vue' {
  /* Global Vue object extensions */
  interface VueConstructor {
    install?(vue: VueConstructor): void;
  }

  /* Vue instance extensions */
  interface Vue {
    $i18n: (
      i18nProperty: string,
      computations?: { [compute: string]: any },
    ) => string;
  }
}

declare module 'vue-router/types/router' {
  interface RouteConfig {
    group?: string;
  }
}
