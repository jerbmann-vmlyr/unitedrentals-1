/**
 * protectedNamespaces - the name spaces "protected" by the route guard
 *
 * @type {string[]}
 */
const protectedNamespaces = [
  'manage',
  'user',
  'messaging',
];

/**
 * protectedRoutes - the specific routes within the name spaces that are secure
 *
 * @type {string[]}
 */
const protectedRoutes = [
  'workplace:equipment:details',
  'workplace:no-equipment',
  'workplace:equipment:update-po',
  'workplace:equipment:request-reschedule',
  'workplace:equipment:confirm-reschedule',
  'workplace:equipment:customize',
  'ObjectListingDetail',
  'ReservationsList',
  'QuotesList',
  'EditQuote',
  'EditQuoteCreateJobsite',
  'EditQuoteEditBranch',
  'lfFilterQuoteDetails',
  'lfStateQuoteDetails',
  'lfCityQuoteDetails',
  'lfTypeQuoteDetails',
  'lfBranchQuoteDetails',
  'ConvertQuote',
  'ConvertQuoteEditAccount',
  'ConvertQuoteSelectCard',
  'ConvertQuoteEditRpp',
  'ConvertQuoteChangeAlert',
  'ConvertQuoteCreateJobsite',
  'ConvertQuoteAccountDetails',
  'lfBaseConvertQuote',
  'lfFilterMid',
  'lfStateConvertQuote',
  'lfCityConvertQuote',
  'lfTypeConvertQuote',
  'lfBranchConvertQuote',
  'ConvertQuoteAddCard',
  'ConvertQuoteConfirmConvert',
  'MessagingRouter',
];

export {
  protectedNamespaces,
  protectedRoutes,
};
