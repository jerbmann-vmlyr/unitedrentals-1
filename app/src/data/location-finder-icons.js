export default {
  leafletCluster: {
    iconSize: [40, 40],
    iconAnchor: [20, 40],
  },
  leafletGeneral: {
    iconSize: [20, 30],
    iconAnchor: [10, 25],
  },
  searchLocation: {
    iconUrl: '/themes/custom/urone/images/search-marker.png',
    iconSize: [20, 40],
    iconAnchor: [10, 35],
  },
};
