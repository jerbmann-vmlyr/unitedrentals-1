(function ($, Drupal) {

  Drupal.behaviors.styleguide = {
    attach: function (context, settings) {
      $('.js-sg-menu-toggle').once('styleguide').on('click', function () {
        $('body').toggleClass('jsa-styleguide-menu--open');
      });

      $('.js-sg-grid-overlay-toggle').once('styleguide').on('click', function () {
        $('body').toggleClass('jsa-styleguide-grid-overlay--on');
      });

      $('.menu--styleguide span').once('styleguide').on('click', function () {
        $(this).parents('li').toggleClass('jsa-styleguide-menu--open');
        $(this).parents('li').find('ul').slideToggle();
      });
    }
  };
})(jQuery, Drupal);
