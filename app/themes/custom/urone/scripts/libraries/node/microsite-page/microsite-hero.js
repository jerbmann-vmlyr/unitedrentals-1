(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.microsite_responsive_hero_images = {
    attach: function (context) {
      var elements = $('[responsive-hero-image]', context);
      for (var i = 0; i < elements.length; i++) {
        new Drupal.ResponsiveBackgroundHero(elements[i]);
      }
      $('.microsite-hero--media-container').once('hero--with-video').on('click', function (e) {
        $(this).toggleClass('video-playing');
        // Grab base URI w/o query string params.
        var src = $(this).find('.microsite-hero--video iframe').attr('src').split('?')[0];
        // Add autoplay even to the end.
        $(this).find('.microsite-hero--video iframe').attr('src', src + '?autoplay=1&rel=0&start=0&frameborder=0&enablejsapi=1');
      });
    }
  };
  Drupal.ResponsiveBackgroundHero = function (element) {
    this.element = element;
    this.img = element.querySelector('img');
    this.src = '';
    this.img.addEventListener('load', function () {
      this.update();
    }
      .bind(this));
    if (this.img.complete) {
      this.update();
    }
  };
  Drupal.ResponsiveBackgroundHero.prototype.update = function () {
    var src = typeof this.img.currentSrc !== 'undefined' ? this.img.currentSrc : this.img.src;
    if (this.src !== src) {
      this.src = src;
    }
  };
}
)(jQuery, Drupal, drupalSettings);
