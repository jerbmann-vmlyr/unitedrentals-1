(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.show_featured_slide = {
    attach: function (context, drupalSettings) {
      $('.microsite-slide', context).once('slide-clicked').on('click', function () {
        var slideshowId = $(this).parents('.microsite-carousel').attr('data-slideshow-id');
        $('.microsite-carousel[data-slideshow-id=' + slideshowId + '] .microsite-slide').removeClass('is-active');
        $(this).addClass('is-active');
        if ($('.microsite-carousel[data-slideshow-id=' + slideshowId + '] .microsite-slide--media-container.video-playing').length) {
          var src = $('.microsite-carousel[data-slideshow-id=' + slideshowId + ']  .microsite-slide--media-container.video-playing').find('.microsite-slide--video iframe')[0].src.split('?')[0];
          $('.microsite-carousel[data-slideshow-id=' + slideshowId + ']  .microsite-slide--media-container.video-playing .microsite-slide--video iframe')[0].src = src + '?autoplay=0&rel=0&start=0&frameborder=0';
        }
        $('.microsite-carousel[data-slideshow-id=' + slideshowId + ']  .microsite-slide--media-container.video-playing').removeClass('video-playing');
        $('.microsite-carousel[data-slideshow-id=' + slideshowId + '] .microsite-slide--featured-slide').removeClass('is-active');
        var slideId = $(this).data('slide-id');
        $('.microsite-carousel[data-slideshow-id=' + slideshowId + ']  .microsite-slide--featured-slide[data-slide-id="' + slideId + '"]').addClass('is-active');
      });
    }
  };
  Drupal.behaviors.play_featured_slide = {
    attach: function (context, drupalSettings) {
      $('.microsite-slide--media-container', context).once('featured-clicked').on('click', function () {
        $(this).toggleClass('video-playing');
        var src = $(this).find('.microsite-slide--video iframe')[0].src.split('?')[0];
        $(this).find('.microsite-slide--video iframe')[0].src = src + '?autoplay=1&rel=0&start=0&frameborder=0';
      });
    }
  };
}
)(jQuery, Drupal, drupalSettings);
