(function ($, Drupal) {

  Drupal.behaviors.itemSlider = {
    attach: function (context, settings) {

      $('.field--name-field-images-unlimited:first-of-type').addClass('jsa-slider-for');
      $('.field--name-field-images-unlimited:last-of-type').addClass('jsa-slider-nav');
      var imageCount = $('.jsa-slider-nav .field__item').length;
      var navImageScroll = 4;

      $('.jsa-slider-for').each(function (index) {
        var $slick_slider = $(this);
        var $settings = {
          slidesToShow: 1,
          slidesToScroll: 1,
          asNavFor: '.jsa-slider-nav',
          dots: true,
          arrows: false,
          fade: false,
          adaptiveHeight: false,
          mobileFirst: true,
          responsive: [
            {
              breakpoint: 767,
              settings: {
                dots: false,
                arrows: true,
                fade: true
              }
            }
          ]
        };

        $slick_slider.slick($settings);
      });

      $('.jsa-slider-nav').each(function (index) {
        var $slick_slider = $(this);
        var $settings;

        if (navImageScroll >= imageCount) {
          $settings = {
            slidesToShow: navImageScroll,
            slidesToScroll: imageCount,
            asNavFor: '.jsa-slider-for',
            arrows: false,
            focusOnSelect: true,
            infinite: false
          };

        }
        else {
          $settings = {
            slidesToShow: navImageScroll,
            slidesToScroll: 1,
            asNavFor: '.jsa-slider-for',
            arrows: false,
            focusOnSelect: true,
            infinite: true
          };
        }

        $slick_slider.slick($settings);
      });
    }
  };
})(jQuery, Drupal);
