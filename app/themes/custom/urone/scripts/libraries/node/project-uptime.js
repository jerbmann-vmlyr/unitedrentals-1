(function ($, Drupal) {

  // Add a wrapper for the special blocks
  $('#block-projectuptimesubscribe, #block-helpfulnessratingblock').wrapAll('<div class="jsa-block-wrap"></div>');

})(jQuery, Drupal);
