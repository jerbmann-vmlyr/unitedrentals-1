(function ($, Drupal) {

  Drupal.behaviors.regionContent = {
    attach: function (context, settings) {

      $(document).ready(function () {
        $('.slick-arrow').attr('tabindex', '-1');
      });
      if (drupalSettings && drupalSettings.ur && drupalSettings.ur.primaryRole && drupalSettings.ur.primaryRole.length > 0) {
        if (window.dataLayer) {
          window.dataLayer.push({
            event: 'defineUser',
            userRole: drupalSettings.ur.primaryRole
          });
        }
      }
    }
  };

})(jQuery, Drupal);
