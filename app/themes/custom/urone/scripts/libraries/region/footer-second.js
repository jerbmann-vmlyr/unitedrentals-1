(function ($, Drupal) {

  Drupal.behaviors.regionFooterSecond = {
    attach: function (context, settings) {
      $('.menu--footer > ul > li > a').once('regionFooterSecond').each(function (index) {

        // if the link has a submenu
        if ($(this).next('ul').length) {

          // add a class to add the icon
          $(this).addClass('jsa-dropdown');

          // on click
          $(this).on('click', function (e) {
            e.preventDefault();
            // if link has active class
            // remove active class
            if ($(this).is('.jsa-active')) {
              $(this).removeClass('jsa-active');
            }
            // else
            // hide any opened items
            // show the clicked one
            else {
              $('.menu--footer > ul > li > a').removeClass('jsa-active');
              $(this).addClass('jsa-active');
            }
          });
        }
      });
    }
  };

})(jQuery, Drupal);
