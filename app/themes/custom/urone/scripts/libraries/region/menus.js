(function ($, Drupal) {

  Drupal.behaviors.regionMenus = {
    attach: function (context, settings) {
      var behavior_object = this;
      var hover_delay_timer;
      var selector_header_menu_button = '.block-menu__toggle';
      var selector_header_menu_item_buttons = '.menu-item__button';
      var selector_header_menu_items_with_children = '.region-header .menu-item--depth-0.menu-item--has-children, .region-pre-content .menu-item--has-children';

      // Open mobile menu.
      $(selector_header_menu_button).once('header').on({
        click: function () {
          var $this = $(this);
          var $menu = $this.parent().find('.menu--depth-0');

          // Either expand or collapse the menu.
          behavior_object.togglePanel($this);

          // Close all sub menus.
          $menu.find('.menu-item').removeClass('menu-item--expanded');
          $menu.find('.menu-item__button').attr('aria-expanded', 'false');

          // Toggle the mobile nav
          if ($this.attr('aria-controls') === 'header-main-navigation') {
            // Open the menu button in the mobile menu
            $('.region-mobile-menu .block-menu__toggle').attr('aria-expanded', 'true');

            // Open the mobile menu flyout
            $('body').addClass('jsa-body-lock jsa-mobile-menu-open');
          }

          if ($this.attr('aria-controls') === 'mobile-main-navigation') {
            // Close the menu button in the header
            $('.region-header-second .block-menu__toggle').attr('aria-expanded', 'false');

            // Close the mobile menu flyout
            $('body').removeClass('jsa-body-lock jsa-mobile-menu-open');
          }
        }
      });

      // Trigger button's panel to open.
      $('.js-search-buttons').once('.region-header-second__content').on({
        click: function (e) {

          $(this).toggleClass('jsa-active');

          if ($(this).hasClass('jsa-active')) {
            $(this).attr('aria-expanded', 'true');

            setTimeout(function () {
              $('.region-header-second .global-search').addClass('jsa-open');
            }, 600);

            // focus input when global search expands
            var location_input = document.getElementById('locationSearch');
            location_input.focus();
          }
          else {
            $(this).attr('aria-expanded', 'false');
            $('.region-header-second .global-search').removeClass('jsa-open').addClass('jsa-closing');

            setTimeout(function () {
              $('.region-header-second .global-search').remove();
            }, 600);
          }
        }
      });

      $(window).on('resize', function () {
        if ($(window).width() >= 768) {
          $('.js-search-buttons').removeClass('jsa-active');
          $('.js-search-buttons').attr('aria-expanded', 'false');
          $('.region-header-second .global-search').addClass('jsa-closing');

          setTimeout(function () {
            $('.region-header-second .global-search').remove();
          }, 600);
        }

        if ($(window).width() >= 768 && $('body').hasClass('jsa-mobile-menu-open')) {
          // Close the menu button in the header
          $('.region-header-second .block-menu__toggle').attr('aria-expanded', 'false');

          // Close the mobile menu flyout
          $('body').removeClass('jsa-body-lock jsa-mobile-menu-open');
        }
      });

      // Trigger button's panel to open.
      $(selector_header_menu_item_buttons).once('header').on({
        click: function () {
          behavior_object.toggleMenuPanel($(this));
        }
      });

      // Trigger button panel to open based on hover (done here instead of css
      // :hover to reduce amount of code and make issues easier to troubleshoot.
      // Trigger button panel to open based on hover (done here instead of css
      // :hover to reduce amount of code and make issues easier to troubleshoot.
      $(selector_header_menu_items_with_children).once('header').on({
        mouseenter: function () {
          var $menu_item = $(this);

          // Make sure user continues to hover over the item before showing.
          hover_delay_timer = setTimeout(function () {
            var $button = $menu_item.find('.menu-item__button').first();

            // Only toggle panel for desktop and if not already expanded.
            if (behavior_object.isDesktop() && $button.attr('aria-expanded') !== 'true') {
              behavior_object.toggleMenuPanel($button);
            }
          }, 250);
        },
        mouseleave: function () {
          var $menu_item = $(this);
          var $button = $menu_item.find('.menu-item__button').first();

          // Only toggle panel for desktop and if not already expanded.
          if (behavior_object.isDesktop() && $button.attr('aria-expanded') === 'true') {
            behavior_object.toggleMenuPanel($button);
          }
          clearTimeout(hover_delay_timer);
        }
      });
    },
    isDesktop: function () {
      return ($('body').width() >= 568);
    },
    isMobile: function () {
      return ($('body').width() < 568);
    },
    toggleMenuPanel: function ($button, to_expand) {
      to_expand = (typeof to_expand === 'boolean') ? to_expand : ($button.attr('aria-expanded') !== 'true');
      var $menu_item = $button.parent();

      // Collapse sibling and sibling children menus.
      $menu_item.siblings('.menu-item--has-children').removeClass('menu-item--expanded');
      $menu_item.siblings('.menu-item--has-children').find('.menu-item__button').attr('aria-expanded', 'false');

      // Either expand or collapse the component's panel.
      $button.attr('aria-expanded', to_expand ? 'true' : 'false');
      $menu_item.toggleClass('menu-item--expanded', to_expand);

      // Revalidate Blazy.
      Drupal.blazy.init.revalidate();

      return to_expand;
    },
    togglePanel: function ($button, to_expand) {
      to_expand = (typeof to_expand === 'boolean') ? to_expand : ($button.attr('aria-expanded') !== 'true');
      var $body = $('body');
      var current_panel_id = $body.attr('data-panel-open');
      var panel_id = $button.attr('aria-controls');

      // Close panel if one is currently open.
      if (current_panel_id && current_panel_id !== panel_id) {
        var $current_panel_button = $('button[aria-controls="' + current_panel_id + '"]');
        $current_panel_button.attr('aria-expanded', 'false');
        $body.attr('data-panel-open', null);
      }

      // Either expand or collapse the panel.
      $button.attr('aria-expanded', to_expand ? 'true' : 'false');
      $body.attr('data-panel-open', (to_expand) ? panel_id : null);
      $body.toggleClass('jsa-body-lock', to_expand);
    }
  };

})(jQuery, Drupal);
