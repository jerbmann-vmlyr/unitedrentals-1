(function ($, Drupal) {

  Drupal.behaviors.eventCalculator = {
    getCount: function ($attendance, $hours) {
      if ($attendance === void 0) {$attendance = null;}
      if ($hours === void 0) {$hours = null;}

      if (!$attendance) {
        return 0;
      }
      switch ($attendance) {
        case 250:
          if ($hours && $hours <= 5) {
            return 2;
          }
          return 3;
        case 500:
          if ($hours === 1) {
            return 2;
          }
          if ($hours && $hours <= 3) {
            return 3;
          }
          if ($hours && $hours <= 9) {
            return 4;
          }
          return 5;
        case 1000:
          switch ($hours) {
            case 1:
              return 3;
            case 2:
              return 4;
            case 3:
              return 5;
            case 4:
            case 5:
              return 6;
            case 6:
              return 7;
            case 7:
              return 8;
            default:
              return 9;
          }
        case 2000:
          switch ($hours) {
            case 1:
              return 5;
            case 2:
              return 8;
            case 3:
              return 10;
            case 4:
              return 11;
            case 5:
              return 12;
            case 6:
            case 7:
              return 13;
            default:
              return 14;
          }
        case 3000:
          switch ($hours) {
            case 1:
              return 7;
            case 2:
              return 12;
            case 3:
              return 15;
            case 4:
              return 16;
            case 5:
            case 6:
              return 18;
            case 7:
              return 19;
            case 8:
            case 9:
              return 20;
            default:
              return 21;
          }
        case 4000:
          switch ($hours) {
            case 1:
              return 10;
            case 2:
              return 16;
            case 3:
              return 19;
            case 4:
              return 22;
            case 5:
              return 24;
            case 6:
            case 7:
              return 25;
            case 8:
            case 9:
              return 27;
            default:
              return 28;
          }
        case 5000:
          switch ($hours) {
            case 1:
              return 12;
            case 2:
              return 20;
            case 3:
              return 24;
            case 4:
              return 27;
            case 5:
              return 29;
            case 6:
              return 31;
            case 7:
              return 32;
            case 8:
            case 9:
              return 33;
            default:
              return 34;
          }
        case 6000:
          switch ($hours) {
            case 1:
              return 14;
            case 2:
              return 24;
            case 3:
              return 28;
            case 4:
              return 33;
            case 5:
              return 35;
            case 6:
            case 7:
              return 37;
            case 8:
              return 39;
            default:
              return 41;
          }
        case 7000:
          switch ($hours) {
            case 1:
              return 17;
            case 2:
              return 27;
            case 3:
              return 34;
            case 4:
              return 38;
            case 5:
              return 41;
            case 6:
              return 42;
            case 7:
            case 8:
              return 46;
            case 9:
              return 47;
            default:
              return 48;
          }
        case 8000:
          switch ($hours) {
            case 1:
              return 17;
            case 2:
              return 27;
            case 3:
              return 34;
            case 4:
              return 38;
            case 5:
              return 41;
            case 6:
              return 42;
            case 7:
            case 8:
              return 46;
            case 9:
              return 47;
            default:
              return 48;
          }
        case 10000:
          switch ($hours) {
            case 1:
              return 24;
            case 2:
              return 39;
            case 3:
              return 47;
            case 4:
              return 54;
            case 5:
              return 58;
            case 6:
              return 62;
            case 7:
              return 64;
            case 8:
              return 66;
            case 9:
              return 67;
            default:
              return 68;
          }
        case 12500:
          switch ($hours) {
            case 1:
              return 30;
            case 2:
              return 48;
            case 3:
              return 61;
            case 4:
              return 68;
            case 5:
              return 73;
            case 6:
              return 78;
            case 7:
              return 80;
            case 8:
              return 81;
            case 9:
              return 82;
            default:
              return 85;
          }
        case 15000:
          switch ($hours) {
            case 1:
              return 37;
            case 2:
              return 57;
            case 3:
              return 70;
            case 4:
              return 81;
            case 5:
              return 87;
            case 6:
              return 92;
            case 7:
              return 94;
            case 8:
              return 99;
            case 9:
              return 102;
            default:
              return 104;
          }
        case 17500:
          switch ($hours) {
            case 1:
              return 42;
            case 2:
              return 68;
            case 3:
              return 84;
            case 4:
              return 95;
            case 5:
              return 100;
            case 6:
              return 110;
            case 7:
              return 111;
            case 8:
              return 113;
            case 9:
              return 116;
            default:
              return 118;
          }
        case 20000:
          switch ($hours) {
            case 1:
              return 48;
            case 2:
              return 77;
            case 3:
              return 95;
            case 4:
              return 107;
            case 5:
              return 115;
            case 6:
              return 120;
            case 7:
              return 127;
            case 8:
              return 131;
            case 9:
              return 133;
            default:
              return 136;
          }
        case 25000:
          switch ($hours) {
            case 1:
              return 60;
            case 2:
              return 96;
            case 3:
              return 122;
            case 4:
              return 133;
            case 5:
              return 146;
            case 6:
              return 156;
            case 7:
              return 159;
            case 8:
              return 162;
            case 9:
              return 165;
            default:
              return 171;
          }
        case 30000:
          switch ($hours) {
            case 1:
              return 73;
            case 2:
              return 114;
            case 3:
              return 141;
            case 4:
              return 163;
            case 5:
              return 174;
            case 6:
              return 184;
            case 7:
              return 188;
            case 8:
              return 194;
            case 9:
              return 197;
            default:
              return 201;
          }
        case 40000:
          switch ($hours) {
            case 1:
              return 95;
            case 2:
              return 156;
            case 3:
              return 188;
            case 4:
              return 217;
            case 5:
              return 231;
            case 6:
              return 243;
            case 7:
              return 249;
            case 8:
              return 257;
            case 9:
              return 266;
            default:
              return 271;
          }
        case 50000:
          switch ($hours) {
            case 1:
              return 120;
            case 2:
              return 192;
            case 3:
              return 238;
            case 4:
              return 267;
            case 5:
              return 290;
            case 6:
              return 305;
            case 7:
              return 312;
            case 8:
              return 322;
            case 9:
              return 330;
            default:
              return 337;
          }
        case 75000:
          switch ($hours) {
            case 1:
              return 177;
            case 2:
              return 292;
            case 3:
              return 357;
            case 4:
              return 403;
            case 5:
              return 432;
            case 6:
              return 455;
            case 7:
              return 470;
            case 8:
              return 485;
            case 9:
              return 491;
            default:
              return 508;
          }
        case 100000:
          switch ($hours) {
            case 1:
              return 239;
            case 2:
              return 378;
            case 3:
              return 475;
            case 4:
              return 515;
            case 5:
              return 542;
            case 6:
              return 562;
            case 7:
              return 583;
            case 8:
              return 593;
            case 9:
              return 620;
            default:
              return 633;
          }
        default:
          return 0;
      }
    },

    attach: function (context, settings) {
      $('.event-calculator').each(function () {
        var $calculator = $(this);
        var attendanceOptions = [
          250,
          500,
          1000,
          2000,
          3000,
          4000,
          5000,
          6000,
          7000,
          8000,
          10000,
          12500,
          15000,
          17500,
          20000,
          25000,
          30000,
          40000,
          50000,
          75000,
          100000
        ];

        var hoursOptions = [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10
        ];

        // loop through all attendance options and add as <option> within select
        $(attendanceOptions).each(function (index, value) {
          $('.event-calculator__attendance').append($('<option></option>').val(value).text(value));
        });

        // loop through all hours options and add as <option> within select
        $(hoursOptions).each(function (index, value) {
          $('.event-calculator__hours').append($('<option></option>').val(value).text(value));
        });

        // on select value change to attendance or hours
        $('.event-calculator__attendance, .event-calculator__hours').change(function (context) {
          var $attendanceValue = $('.event-calculator__attendance').val();
          var $hoursValue = $('.event-calculator__hours').val();
          var $attendance = parseInt($attendanceValue);
          var $hours = parseInt($hoursValue);

          // calculate and show results
          var $resultsContainer = $calculator.find('.event-calculator__results');
          var results = Drupal.behaviors.eventCalculator.getCount($attendance, $hours);

          $resultsContainer.text('You need ' + results + ' units');
        });
      });
    }
  };

})(jQuery, Drupal);
