(function ($, Drupal) {

  Drupal.behaviors.callUsBlock = {
    attach: function (context, settings) {
      $('.call-us__trigger').once('nestedNavigation').each(function (index) {
        $(this).on('click', function (e) {
          $(this).toggleClass('jsa-active');
          $('.call-us__info').slideToggle();
        });
      });
    }
  };

})(jQuery, Drupal);
