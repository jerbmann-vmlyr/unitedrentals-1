(function ($, Drupal) {

  Drupal.behaviors.equipmentcardslide = {
    attach: function (context, settings) {
      $('.block-views-blockequipment-categories-tile-cards').each(function (index) {
        var $window = $(window);
        var $slick_slider = $(this).find('.view-content');
        var $settings = {
          arrows: false,
          infinite: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          rows: 2,
          responsive: [
            {
              breakpoint: 5000,
              settings: 'unslick'
            },
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2
              }
            }
          ]
        };

        function breakpointSlider() {
          if ($window.width() >= 1024) {
            if ($slick_slider.hasClass('slick-initialized')) {
              $slick_slider.removeClass('slick-initialized');
            }
          }
          else {
            if (!$slick_slider.hasClass('slick-initialized')) {
              return $slick_slider.slick($settings);
            }
          }
        }

        $window.on('load', function () {
          breakpointSlider();
        });

        $window.on('resize', function () {
          breakpointSlider();
        });
      });
    }
  };
})(jQuery, Drupal);
