(function ($, Drupal) {

  Drupal.behaviors.componentNestedNavigation = {
    attach: function (context, settings) {
      var $window = $(window);

      $('.block-nested-navigation-block .navigation button, .block-nested-navigation-block .block-taxonomymenu__menu-item button').once('nestedNavigation').each(function (index) {
        $(this).on('click', function (e) {
          $(this).closest('li').children('ul').slideToggle();
          $(this).closest('li').toggleClass('jsa-active');
        });

        var $isOpen = $('.field--name-field-nested-navigation-block .block-menu-block ul li').children().children().find('.is-active');

        if ($isOpen) {
          var $requestedPanel = $isOpen.parent().parent();
          $requestedPanel.parent().addClass('jsa-active');
          $requestedPanel.slideDown();
        }
      });

      $('.block-nested-navigation-block__header').once('nestedNavigationBlockHeader').each(function (index) {
        $(this).on('click', function (e) {
          $(this).parent().find('.block-nested-navigation-block__content').find('.navigation, .block-hierarchical-taxonomy-menu__content').children('ul').slideToggle();
          $(this).toggleClass('jsa-active');
        });
      });

      $window.on('resize', function () {
        if ($window.width() >= 768) {
          $('.block-nested-navigation-block__header').parent().find('.block-nested-navigation-block__content').find('nav').children('ul').slideDown();
          $('.block-nested-navigation-block__header').removeClass('jsa-active');
        }
      });
    }
  };

})(jQuery, Drupal);
