(function ($, Drupal) {

  Drupal.behaviors.equipmentlisting = {
    attach: function (context, settings) {

      /**
       * GTM Push method
       *
       * Pass through method for dataLayer.push
       *
       * @param {object} obj | gtm object to add to data layer
       */
      function gtmPush(obj) {
        var dataLayer = window.dataLayer;

        dataLayer.push(obj);
      }

      /**
       * GTM Track Product Click
       *
       * @param {string} name | product name
       * @param {integer} quantity | product quantity
       * @param {string} category | product category
       * @param {string} subCategory | product subCategory
       * @param {string} catClass | product catClass
       * @param {string} url | product detail page url
       */
      function gtmTrackProductClick(name, quantity, category, subCategory, catClass, url) {
        gtmPush({
          event: 'productClick',
          ecommerce: {
            click: {
              actionField: {list: subCategory},
              products: [{
                'name': name,
                'id': catClass,
                'price': '',
                'brand': '',
                'category': category,
                'variant': subCategory,
                'position': '',
                'user-role': window.drupalSettings.ur.primaryRole,
                'account-id': window.drupalSettings.user.defaultAccountId,
                'branch-id': window._app.storeID,
                'rentalman-guid': window._app.rmGuid
              }]
            }
          },
          eventCallback: function eventCallback() {
            document.location = url;
          }
        });
      }

      /**
       * GTM Track Product Details Expand
       *
       * @param {string} event | event name
       * @param {string} name | product name
       */
      function gtmTrackDetails(event, name) {
        gtmPush({
          'event': event,
          'data-product-name': name
        });
      }

      /**
       * GTM Track Product Interest Interactions
       *
       * @param {string} name | product name
       * @param {string} interactionType | interaction type
       */
      function gtmTrackProductInterestInteractions(name, interactionType) {
        gtmPush({
          'event': 'productInterest',
          'data-product-name': name,
          'data-interaction': interactionType
        });
      }

      /**
       * Equipment Listing Page
       *
       * GTM Events: productClick, productDetailClose, productDetailExpand, productInterest
       */
      $('.views-row').once('.equipment-listing-full').each(function () {
        // Find data layer event targets
        var $productName = $(this).find('.object_listing__details__name');
        var $viewDetails = $(this).find('.ur-quick-view-toggle');
        var $expandedViewDetails = $viewDetails.siblings('.ur-quick-view');

        // Get data layer attributes
        var $productListing = $(this).find('.object_listing__details');
        var $productRow = $(this).parent('.list__container').find('.product-category-attributes');
        var $name = $productListing.attr('data-product-name');
        var $url = $(this).find('.object_listing__details__name').attr('href');

        $productName.on('click', function () {
          gtmTrackProductClick(
            $name,
            $productListing.attr('data-product-quantity'),
            $productRow.attr('data-product-category'),
            $productRow.attr('data-product-sub-category'),
            $productListing.attr('data-product-cat-class'),
            $url
          );
        });

        // On click of view details
        $viewDetails.on('click', function () {
          if ($expandedViewDetails.hasClass('visible')) {
            gtmTrackDetails(
              'productDetailClose',
              $name
            );
          }

          else {
            gtmTrackDetails(
              'productDetailExpand',
              $name
            );
          }
        });

        // Find data layer product interest event targets
        var $favorite = $(this).find('.flag-favorites');
        var $email = $(this).find('.icon-before-mail-outline');
        var $print = $(this).find('.icon-before-print');

        $favorite.on('click', function () {
          if ($favorite.hasClass('action-unflag')) {
            gtmTrackProductInterestInteractions(
              $name,
              'unfavorite'
            );
          }

          else {
            gtmTrackProductInterestInteractions(
              $name,
              'favorite'
            );
          }
        });

        // On email click
        $email.on('click', function () {
          gtmTrackProductInterestInteractions(
            $name,
            'email'
          );
        });

        // On print click
        $print.on('click', function () {
          gtmTrackProductInterestInteractions(
            $name,
            'print'
          );
        });
      });

      /**
       * Equipment Detail Page
       *
       * GTM Event: productInterest
       */
      $('.page-node-type-item').each(function () {
        var $interactionOptions = $(this).find('.object_listing__actions--object-detail');
        var $name = $(this).find('.page-title .field--name-title').text();

        // Find data layer product interest event targets
        var $favorite = $interactionOptions.find('.flag-favorites');
        var $email = $interactionOptions.find('.icon-before-mail-outline');
        var $print = $interactionOptions.find('.icon-before-print');

        $favorite.on('click', function () {
          if ($favorite.hasClass('action-unflag')) {
            gtmTrackProductInterestInteractions(
              $name,
              'unfavorite'
            );
          }

          else {
            gtmTrackProductInterestInteractions(
              $name,
              'favorite'
            );
          }
        });

        // On email click
        $email.on('click', function () {
          gtmTrackProductInterestInteractions(
            $name,
            'email'
          );
        });

        // On print click
        $print.on('click', function () {
          gtmTrackProductInterestInteractions(
            $name,
            'print'
          );
        });
      });

      /**
       * Home Page - Favorites Equipment Tab
       *
       * GTM Event: productInterest
       */
      $(document).ajaxSuccess(function (event, xhr, settings) {
        if (settings.url.indexOf('flag/unflag/favorites') >= 0) {

          $('.views-row').once('block-views-blockitem-cards-favorites').each(function () {
            var $name = $(this).find('.node__footer .field--name-title').text();
            var $favorite = $(this).find('.flag-favorites');

            if ($favorite.hasClass('action-flag')) {
              gtmTrackProductInterestInteractions(
                $name,
                'unfavorite'
              );
            }
          });
        }
      });
    }
  };

})(jQuery, Drupal);

