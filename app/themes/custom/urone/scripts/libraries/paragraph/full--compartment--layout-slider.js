(function ($, Drupal) {

  Drupal.behaviors.compartmentslider = {
    attach: function (context, settings) {

      // When the slide is changing
      function playPauseVideo(slick, control) {
        var currentSlide;
        var player;

        currentSlide = slick.find('.slick-current');
        player = currentSlide.find('iframe').get(0);

        switch (control) {
          case 'play':
            // When we tell youtube api to play, pause the slider (will disable autoplay).
            slick.slick('slickPause');
            // Uncomment to have video play, but muted. Will save sanity when testing video.
            // postMessageToPlayer(player, {
            //   'event': 'command',
            //   'func': 'mute'
            // });
            postMessageToPlayer(player, {
              event: 'command',
              func: 'playVideo'
            });
            break;
            // Not using, but could make use of this later.
          case 'pause':
            postMessageToPlayer(player, {
              event: 'command',
              func: 'pauseVideo'
            });
            break;
          case 'stop':
            // When we tell youtube api to stop, play the slider (will start autoplay).
            slick.slick('slickPlay');
            postMessageToPlayer(player, {
              event: 'command',
              func: 'stopVideo'
            });
            break;
        }
      }

      // POST commands to YouTube or Vimeo API
      function postMessageToPlayer(player, command) {
        if (player == null || command == null) { return; } // eslint-disable-line curly
        player.contentWindow.postMessage(JSON.stringify(command), '*');
      }

      $('.ca-component-compartment--slider').each(function (index) {
        var $slick_slider = $(this).find('.component-compartment__content > div');

        // Before the slider changes to a new slide stop any video.
        $slick_slider.on('beforeChange', function (event, slick) {
          slick = $(slick.$slider);
          playPauseVideo(slick, 'stop');
        });

        var $settings = {
          arrows: false,
          centerMode: false,
          dots: false,
          infinite: false,
          autoplay: false,
          mobileFirst: true,
          slidesToShow: 1,
          responsive: [
            {
              breakpoint: 1023,
              settings: {
                arrows: true,
                autoplay: true,
                autoplaySpeed: 8000,
                infinite: true,
                slidesToScroll: 1
              }
            }
          ]
        };

        $slick_slider.slick($settings);

      });

      // prevent iframe from being tabbed inside of
      $('iframe').attr('tabindex', '-1');

    }
  };
})(jQuery, Drupal);
