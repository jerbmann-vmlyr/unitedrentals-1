(function ($, Drupal) {

  Drupal.behaviors.stickyLeadGen = {
    attach: function (context, settings) {
      $('.component-hero--lead-generation-bar__wrapper').once('stickyLeadGen').each(function () {
        var layoutBar = $(this).find('.component-lead-generation-bar:not(.component-lead-generation-bar--layout-brief)');
        var layoutBrief = $(this).find('.component-lead-generation-bar');
        var leadGenBar = $(window).width() < 768 || !layoutBar.length ? layoutBrief : layoutBar;
        var barPosition = leadGenBar === layoutBar || $(window).width() < 768 ? leadGenBar.offset().top : leadGenBar.offset().top + layoutBrief.outerHeight();

        // While the window is scrolling...
        $(window).scroll(function () {
          var scrollPosition = $(this).scrollTop();

          if (scrollPosition >= barPosition) {
            // Add the jsa-sticky class to make the bar sticky...
            $(leadGenBar).addClass('jsa-sticky');

            if (leadGenBar !== layoutBar) {
              // In brief layout, remove this class to have it look like a horizontal lead gen bar
              $(leadGenBar).removeClass('component-lead-generation-bar--layout-brief');
            }
          }
          else {
            // Remove the jsa-sticky class so that the lead gen bar will stay where it needs to...
            $(leadGenBar).removeClass('jsa-sticky');

            if (leadGenBar !== layoutBar) {
              // In brief layout, readd this class back so it is a vertical style again
              $(leadGenBar).addClass('component-lead-generation-bar--layout-brief');
            }
          }
        });


        // Add padding to hero slide inner when there is a lead gen bar...
        var heroSlideInner = $(this).parent().find('.component-hero-slide__inner');
        var heroSlideInnerPadding = parseFloat(heroSlideInner.css('padding-bottom'));
        var leadGenBarHeight = leadGenBar === layoutBar ? leadGenBar.height() : null;
        var paddingSum = heroSlideInnerPadding + leadGenBarHeight;

        heroSlideInner.once().css('padding-bottom', paddingSum + 'px');
      });
    }
  };
})(jQuery, Drupal);
