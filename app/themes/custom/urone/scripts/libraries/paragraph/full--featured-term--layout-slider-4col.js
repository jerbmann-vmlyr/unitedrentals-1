(function ($, Drupal) {

  Drupal.behaviors.featuredtermslider4col = {
    attach: function (context, settings) {
      $('.component-featured-content--slider-4col, .microsite-slideshow').each(function (index) {
        var $slick_slider = $(this).find('.component-featured-term__list, .component-featured-content__list, .microsite-slides');
        var $settings = {
          arrows: false,
          autoplay: false,
          centerMode: false,
          infinite: false,
          mobileFirst: true,
          slidesToShow: 2,
          responsive: [
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 1023,
              settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 1439,
              settings: {
                arrows: true,
                slidesToShow: 4
              }
            }
          ]
        };

        $slick_slider.slick($settings);

        // After change revalidate blazy.
        $slick_slider.on('afterChange', function (event, slick, currentSlide) {
          if (Drupal.blazy && Drupal.blazy.init !== null) {
            Drupal.blazy.init.revalidate();
          }
        });

      });
    }
  };

  Drupal.behaviors.slidetabindex = {
    attach: function (context, settings) {
      // remove tabindex on li generated from slick, as a href on the slide will always have a tabindex
      $('.component-featured-content__list, .component-featured-term__list').once('component-featured-content--slider-4col').each(function (index) {
        $('.component-featured-content__list-item').removeAttr('tabindex');
        $('.component-featured-term__list-item').removeAttr('tabindex');
      });
    }
  };
})(jQuery, Drupal);
