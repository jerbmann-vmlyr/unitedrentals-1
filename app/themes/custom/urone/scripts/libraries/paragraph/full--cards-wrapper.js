(function ($, Drupal) {

  Drupal.behaviors.cardsWrapper = {
    attach: function (context, settings) {
      // Load More / Less cards...
      $('.cp-view-toggle .component-cards-wrapper__collection').each(function () {
        var $hiddenCards = $(this).find('.component-card:hidden');

        // If there are hidden cards...
        if ($hiddenCards.length !== 0) {
          var $loadButton = '<div class="jsa-button-div"><button class="jsa-button">View <span class="jsa-button__more">More</span><span class="jsa-button__less">Less</span></button></div>';

          // Check if the last child of the wrapper is the jsa-button-div...
          // if it isn't, append the button to the end of the "card__collection"...
          // else, replace the button with a new button.
          if ($(this).children().last().is('.jsa-button-div') !== true) {
            $(this).append($loadButton);
            $(this).find('.jsa-button__less').hide();
          }
          else {
            $(this).children().last().replaceWith($loadButton);
            $(this).find('.jsa-button__less').hide();
          }
        }
      });

      $('.cp-view-toggle .component-cards-wrapper__collection').find('.jsa-button').click(function () {
        var $hiddenCards = $(this).parent().parent().find('.component-card:hidden');

        if ($hiddenCards.length) {
          $hiddenCards.addClass('jsa-show-card').slideDown();
          $(this).find('.jsa-button__less').show();
          $(this).find('.jsa-button__more').hide();
        }
        else {
          var $cards = $(this).parent().parent().find('.jsa-show-card');

          $cards.removeClass('jsa-show-card').slideUp();
          $(this).find('.jsa-button__less').hide();
          $(this).find('.jsa-button__more').show();
        }
      });

      $('.component-cards-wrapper--layout.cp-view-slider').each(function (index) {
        var $slick_slider = $(this).find('.component-cards-wrapper__collection');
        var $settings = {
          arrows: false,
          centerMode: false,
          infinite: false,
          mobileFirst: true,
          slidesToShow: 2,
          responsive: [
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 1023,
              settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 1439,
              settings: {
                arrows: true,
                slidesToShow: 3
              }
            }
          ]
        };

        $slick_slider.slick($settings);
      });

      // Adding / removing slider functionality at the proper breakpoints
      $('.component-cards-wrapper--layout.cp-view-all').each(function (index) {
        var $window = $(window);
        var $slick_slider = $(this).find('.component-cards-wrapper__collection');
        var $settings = {
          arrows: false,
          infinite: false,
          mobileFirst: true,
          slidesToShow: 2,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 1023,
              settings: 'unslick'
            }
          ]
        };

        function breakpointSlider() {
          if ($window.width() >= 1024) {
            if ($slick_slider.hasClass('slick-initialized')) {
              $slick_slider.removeClass('slick-initialized');
            }
          }
          else {
            if (!$slick_slider.hasClass('slick-initialized')) {
              return $slick_slider.slick($settings);
            }
          }
        }

        $window.on('load', function () {
          breakpointSlider();
        });

        $window.on('resize', function () {
          breakpointSlider();
        });
      });
    }
  };
})(jQuery, Drupal);
