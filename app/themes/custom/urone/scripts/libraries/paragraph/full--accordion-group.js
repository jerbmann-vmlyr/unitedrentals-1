(function ($, Drupal) {

  Drupal.behaviors.componentAccordion = {
    attach: function (context, settings) {
      $('.component-accordion-group:not(.component-accordion-group--js-initialized), .component-faq-list:not(.component-accordion-group--js-initialized)').each(function (index) {
        // Initialize variables.
        var $widget = $(this);
        var $accordionContainer = $widget.children('.component-accordion-group__content, .component-faq-list__content');
        var $accordions = $accordionContainer.children('.component-accordion-item, .faq-row');
        var $accordionHeaders = $accordions.children('.component-accordion-item__title, .field--name-node-title');

        // Mark that the tabs component has been initialized.
        $widget.addClass('component-accordion-group--js-initialized');

        // Add static roles to elements.
        $widget.attr('role', 'tablist');

        // Attach each accordion item header to its content and hide content
        // that should be hidden.
        $accordions.each(function (accordionIndex) {
          var $accordion = $(this);
          var $accordionHeader = $accordion.children('.component-accordion-item__title, .node--type-faq .field--name-node-title');
          var $accordionContent = $accordion.children('.component-accordion-item__content, .faq__answer');

          // Generate the accordion tab (header) and panel (content) IDs.
          var accordionId = $accordion.attr('data-entity-id');
          var headerId = 'component-accordion-item-' + accordionId + '__header';
          var panelId = 'component-accordion-item-' + accordionId + '__panel';

          // Determine whether this accordion needs to be open by default.
          var openByDefault = $accordion.hasClass('component-accordion-item--open');

          // Link tab to panel.
          $accordionHeader
            .attr('aria-controls', panelId)
            .attr('aria-selected', (openByDefault) ? 'true' : 'false')
            .attr('id', headerId)
            .attr('role', 'tab')
            .attr('tabindex', (accordionIndex === 0) ? '0' : '-1');

          // Link panel to tab.
          $accordionContent
            .attr('aria-hidden', (!openByDefault) ? 'true' : 'false')
            .attr('aria-labelledby', headerId)
            .attr('hidden', (!openByDefault))
            .attr('id', panelId)
            .attr('role', 'tabpanel');
        });

        // Initialize the roving tabindex.
        $accordionContainer.rovingTabindex('[role=tab]');

        // Track when tab is changed.
        $accordionContainer.on('rovingTabindexChange', '[role=tab]', function (e, data) {
          var $accordion = $(this);
          var $accordionHeader = $accordion.children('.component-accordion-item__title, .node--type-faq .field--name-node-title');

          $accordionHeaders.attr('aria-selected', 'false');
          $accordionHeader.attr('aria-selected', 'true');
        });

        // Track when tab needs to be opened or closed.
        $accordionHeaders.on('click keyup', function (e) {
          if (e.type === 'click' || (e.type === 'keyup' && [13, 32].indexOf(e.keyCode) !== -1)) {
            var $accordionHeader = $(this);
            var $accordionContent = $accordionHeader.siblings('.component-accordion-item__content, .faq__answer');
            var $accordion = $accordionHeader.parent();
            var open = $accordion.hasClass('component-accordion-item--open');

            $accordion.toggleClass('component-accordion-item--open', (!open));
            $accordionHeader.attr('aria-selected', (open) ? 'false' : 'true');
            $accordionContent
              .attr('aria-hidden', (open) ? 'true' : 'false')
              .attr('hidden', open);
          }
        });
      });
    }
  };

})(jQuery, Drupal);
