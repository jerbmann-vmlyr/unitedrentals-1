(function ($, Drupal) {

  Drupal.behaviors.listingItem = {
    attach: function (context, settings) {
      $('.component-listing-item').once().each(function () {
        $(this).find('button').click(function () {
          $(this).parents('.component-listing-item').toggleClass('jsa-expanded');
          $(this).parents('.component-listing-item').find('.component-listing-item__accordion-item').slideToggle();
        });
      });
    }
  };
})(jQuery, Drupal);
