/**
 * This looks for images within a responsive-background-image data attribute div
 * and pulls child image source and makes it a background image on the div.
 * This works with responsive images as well as it ties into the image's load
 * event causing the background image to be updated as well at the various
 * breakpoints.
 *
 * Followed mostly from this tutorial:
 * https://aclaes.com/responsive-background-images-with-srcset-and-sizes/
 */

(function ($, Drupal) {


  Drupal.behaviors.componentContentVideo = {
    attach: function (context, settings) {
      // When the slide is changing
      function playPauseVideo(control) {
        var player;

        player = $('.component-content').find('iframe').get(0);

        switch (control) {
          case 'play':
            // Uncomment to have video play, but muted. Will save sanity when testing video.
            // postMessageToPlayer(player, {
            //   'event': 'command',
            //   'func': 'mute'
            // });
            postMessageToPlayer(player, {
              event: 'command',
              func: 'playVideo'
            });
            break;
            // Not using, but could make use of this later.
          case 'pause':
            postMessageToPlayer(player, {
              event: 'command',
              func: 'pauseVideo'
            });
            break;
          case 'stop':
            // When we tell youtube api to stop, play the slider (will start autoplay).
            postMessageToPlayer(player, {
              event: 'command',
              func: 'stopVideo'
            });
            break;
        }
      }

      // POST commands to YouTube or Vimeo API
      function postMessageToPlayer(player, command) {
        if (player == null || command == null) { return; } // eslint-disable-line curly
        player.contentWindow.postMessage(JSON.stringify(command), '*');
      }

      var video_wrapper = $('.component-content__video-wrapper');

      // prevent iframe from being tabbed inside of
      $('iframe').attr('tabindex', '-1');

      // add title to iframe for accessibility
      $('iframe').attr('title', 'United Rentals Ad');

      // On click of the video wrapper we want to...
      video_wrapper.on('click', function (e) {
        // Toggle that the slide is playing video.
        $(this).parents('.component-content').toggleClass('jsa-video-playing', true);
        // and tell it to play video.
        playPauseVideo('play');

        // If it's a slider need to pause the autoplay.
        if ($(this).parents('.ca-component-compartment--slider').length !== 0) {
          var $parent = $(this).parents('.ca-component-compartment--slider');
          var $slider = $($parent).find('.component-compartment__content > div');
          $slider.slick('slickPause');
        }
      });

      /**
       * Helper function stop video and reset back to image showing.
       *
       * @param {object} slick | Slick slider object
       */
      function closeItDown(slick) {
        // Remove class and stop the video.
        $('.component-content').removeClass('jsa-video-playing');
        // If it's a slider we need to restart autoplay.
        if (slick && typeof slick !== 'undefined') {
          slick.slick('slickPlay');
        }

        playPauseVideo('stop');
      }

      // Stop playing the video when scrolled past
      $(window).scroll(function () {
        $('.component-content.jsa-video-playing').each(function () {
          var windowScroll = $(window).scrollTop(); // how many pixels you've scrolled
          var containerDistance = $(this).offset().top; // pixels to the top of the container
          var containerHeight = $(this).height(); // height of container in pixels

          // if you've scrolled further than the top of the container + it's height
          // stop the video
          if (windowScroll > containerDistance + containerHeight) {
            // If it's a slider we need to grab the slideshow and pass it along.
            if ($(this).hasClass('ca-component-compartment--slider')) {
              var $slick = $(this).find('.component-compartment__content > div');
              closeItDown($slick);
            }
            else {
              closeItDown();
            }
          }
        });
      });

      // If we click anywhere else
      $(document).mouseup(function (e) {
        if (!video_wrapper.is(e.target) // Check if the target of the click isn't the container...
            && video_wrapper.has(e.target).length === 0 // ... nor a descendant of the container
            && $('.component-content').hasClass('jsa-video-playing')) { // ... and component has the video playing class

          // Check if it's also a slider.
          if ($('.component-content').hasClass('ca-component-compartment--slider')) {
            var $slick = $('.ca-component-compartment--slider .component-compartment__content > div');
            closeItDown($slick);
          }
          else {
            closeItDown();
          }
        }
      });
    }
  };

})(jQuery, Drupal);
