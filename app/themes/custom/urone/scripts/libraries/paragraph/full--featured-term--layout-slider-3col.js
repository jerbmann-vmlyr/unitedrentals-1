(function ($, Drupal) {

  Drupal.behaviors.featuredtermslider3col = {
    attach: function (context, settings) {
      $('.component-featured-content--slider-3col, .component-article-carousel, .block-views-blockproject-uptime-related-articles').each(function (index) {
        var $slick_slider = $(this).find('.component-featured-term__list, .view-content, .component-featured-content__list');
        var $settings = {
          arrows: false,
          centerMode: false,
          infinite: false,
          mobileFirst: true,
          slidesToShow: 2,
          responsive: [
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 1023,
              settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 1439,
              settings: {
                arrows: true,
                slidesToShow: 3
              }
            }
          ]
        };

        $slick_slider.slick($settings);

        // After change revalidate blazy.
        $slick_slider.on('afterChange', function (event, slick, currentSlide) {
          Drupal.blazy.init.revalidate();
        });

      });
    }
  };

  Drupal.behaviors.slidetabindex = {
    attach: function (context, settings) {
      // remove tabindex on li generated from slick, as a href on the slide will always have a tabindex
      $('.component-featured-content__list, .view-content').once('component-featured-content--slider3col, component-article-carousel').each(function (index) {
        $('.component-featured-content__list-item').removeAttr('tabindex');
        $('.views-row').removeAttr('tabindex');
      });
    }
  };
})(jQuery, Drupal);
