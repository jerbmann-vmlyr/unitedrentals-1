(function ($, Drupal) {

  Drupal.behaviors.projectUptimeSearchAll = {
    attach: function (context, settings) {
      $('.block-views-exposed-filter-blockproject-uptime-search-all-page').once('region-left-sidebar').each(function (index) {
        var $window = $(window);
        var $block = $(this);
        var $filter = $('<button class="jsa-is-mobile">Project Uptime - Filters</button>');
        var $filterContent = $block.children('.block-views-exposed-filter-blockproject-uptime-search-all-page__content');

        // if window is > 768
        function toggleFilter() {
          // remove filter button, if exists
          if ($window.width() >= 768) {
            if ($(':has(button)')) {
              $filter.remove();
            }
          }
          // add filter button, if does not exist
          else {
            if ($(':not(button)')) {
              $block.prepend($filter);
            }
          }
        }

        $window.on('load', function () {
          toggleFilter();
        });

        $window.on('resize', function () {
          toggleFilter();
        });

        $(document).on('click', function (event) {
          // open filter drop down on  click to button
          if ($filter.is(event.target)) {
            $filter.toggleClass('jsa-is-open');
            $filterContent.toggleClass('jsa-is-open');
            event.stopPropagation();

            // keep drop down open unless button is clicked again
            if (!$filter.hasClass('jsa-is-open')) {
              $filterContent.removeClass('jsa-is-open');
            }
          }
        });
      });

      $('input:checked').closest('li').append('<a id="clear_btn" class="btn btn-style--primary btn-size--xsmall" href="#">Clear</a>');

      $('#views-exposed-form-project-uptime-search-all-page').on('click keypress', '#clear_btn', function (e) {
        e.preventDefault();
        $('#edit-reset').click();
      });
    }
  };

})(jQuery, Drupal);
