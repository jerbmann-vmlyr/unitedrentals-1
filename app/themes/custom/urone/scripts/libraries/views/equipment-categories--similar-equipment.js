(function ($, Drupal) {

  Drupal.behaviors.equipmentCategoriesSimilarEquipment = {
    attach: function (context, settings) {
      var $slick_slider = $('.view-display-id-similar_equipment .view-content .similar-equipment__list');
      var $settings = {
        arrows: false,
        autoplay: false,
        centerMode: false,
        infinite: false,
        mobileFirst: true,
        slidesToShow: 2,
        responsive: [
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 3,
              arrows: false
            }
          },
          {
            breakpoint: 1023,
            settings: {
              arrows: true,
              slidesToShow: 4,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 1439,
            settings: {
              arrows: true,
              slidesToShow: 5
            }
          }
        ]
      };

      $slick_slider.slick($settings);

      // After change revalidate blazy.
      $slick_slider.on('afterChange', function (event, slick, currentSlide) {
        Drupal.blazy.init.revalidate();
      });

    }
  };

  Drupal.behaviors.slidetabindex = {
    attach: function (context, settings) {
      // remove tabindex on li generated from slick, as a href on the slide will always have a tabindex
      $('.view-display-id-similar_equipment .view-content .similar-equipment__list').once('similar-equipment-slider').each(function (index) {
        $('article', $(this)).removeAttr('tabindex');
      });
    }
  };
})(jQuery, Drupal);
