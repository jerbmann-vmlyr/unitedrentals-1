// -----------------------------------------------------------------------------
// Abstracts/Mixin: Button Styles
// Button style mixins
// @include btn() is the primary one that should be used
// -----------------------------------------------------------------------------

/**
* Button RESET
* Resets <a>, <button>, <input type="submit"> styles to build custom styling
* @include btn-reset();
*/
@mixin btn-reset() {
  background-color: transparent;
  border: 0 none;
  border-radius: 0;
  color: inherit;
  cursor: pointer;
  font-style: normal;
  font-weight: inherit;
  hyphens: auto;
  line-height: normal;
  margin: 0;
  padding: 0;
  text-decoration: none;
  text-transform: none;
  transition: inherit;
  user-select: none;
  white-space: normal;

  @include on-event() {
    font-style: normal;
    text-decoration: none;
  }
}

/**
* Button DISABLED
* @include btn-disabled();
*/
@mixin btn-disabled($bg) {
  &[disabled],
  &.btn--disabled {
    pointer-events: none;

    @include on-event($self: true) {
      color: rgba($color-text, 0.4);
    }

    @if $bg == true {
      background-color: $color-grey-11;
    }
  }
}

/**
* Button BASE
* Common styles for all button types
* @include btn-base();
*/
@mixin btn-base() {
  @include btn-reset();
  @include border(light, med);
  cursor: pointer;
  display: inline-block;
  font-weight: $font-weight-bold;
  overflow: hidden;
  position: relative;
  text-align: center;
  // !important is for overriding the transition that is set on the type-presets
  transition: $transition-base !important; // sass-lint:disable-line no-important

  // text and icon
  &::before,
  &::after {
    line-height: 1;
    vertical-align: -0.2376em;
  }

  // icon only
  &:empty {
    &::before,
    &::after {
      display: block;
      margin: 0;
      vertical-align: inherit;
    }
  }
}

/**
* Button STYLES
* Specific styles for different button looks
* @include btn-style();
*/
@mixin btn-style($style) {
  @if $style == primary {
    @include btn-disabled($bg: true);
    background-color: $color-brand-primary;

    @include on-event($self: true) {
      color: $color-text-reverse;
    }

    &:hover {
      background-color: mix($color-blue, $color-blue-lowlight, 45%);
    }

    &:active {
      background-color: $color-blue-lowlight;
    }

    &:visited {
      color: $color-text-reverse;
    }
  }
  @elseif $style == primary-reverse {
    @include btn-disabled($bg: false);
    background-color: $color-white;

    @include on-event($self: true) {
      color: $color-brand-primary;
    }

    &:hover {
      background-color: $color-grey-2;
    }

    &:active {
      background-color: $color-grey-5;
    }

    &:visited {
      color: $color-brand-primary;
    }
  }
  @elseif $style == primary-alternative {
    @include btn-disabled($bg: true);
    background-color: $color-brand-secondary;

    @include on-event($self: true) {
      color: $color-text-reverse;
    }

    &:hover {
      background-color: mix($color-brand-secondary, $color-blue, 45%);
    }

    &:active {
      background-color: $color-blue;
    }

    &:visited {
      color: $color-text-reverse;
    }
  }
  @elseif $style == secondary {
    @include btn-disabled($bg: false);
    background-color: $color-grey-2;
    box-shadow: 0 0 0 $border-width-light $color-utility-border;

    @include on-event($self: true) {
      color: $color-text;
    }

    &:hover {
      background-color: $color-grey-5;
    }

    &:active {
      background-color: $color-grey-11;
    }

    &:visited {
      color: $color-text;
    }
  }
  @elseif $style == secondary-alternative {
    @include btn-disabled($bg: false);
    background-color: $color-grey-2;
    box-shadow: 0 0 0 $border-width-light $color-utility-border;

    @include on-event($self: true) {
      color: $color-text;
    }

    &:hover {
      background-color: $color-brand-primary;
      color: $color-text-reverse;
    }

    &:active {
      background-color: $color-blue-lowlight;
    }

    &:visited {
      color: $color-text;
    }
  }
  @elseif $style == attention {
    @include btn-disabled($bg: true);
    background-color: $color-orange;

    @include on-event($self: true) {
      color: $color-text-reverse;
    }

    &:hover {
      background-color: mix($color-orange, $color-orange-lowlight, 45%);
    }

    &:active {
      background-color: $color-orange-lowlight;
    }

    &:visited {
      color: $color-text-reverse;
    }
  } @else {
    @warn not a valid style option;
  }
}

/**
* Button SIZES
* Specific sizes for different button needs
* @include btn-size();
*/
@mixin btn-size($size) {
  @if $size == regular {
    @include type-preset(40);
    font-weight: $font-weight-bold;
    padding: rem(10) rem(12);

    // text and icon
    &::before,
    &::after {
      font-size: $icon-size-small;
    }

    // icon only
    &:empty {
      padding: rem(8);

      &::before,
      &::after {
        font-size: $icon-size-regular;
      }
    }

  } @else if $size == small {
    @include type-preset(30);
    font-weight: $font-weight-bold;
    padding: rem(6) rem(12);

    // text and icon
    &::before,
    &::after {
      font-size: $icon-size-xsmall;
    }

    // icon only
    &:empty {
      padding: rem(6);

      &::before,
      &::after {
        font-size: $icon-size-small;
      }
    }

  } @else if $size == xsmall {
    @include type-preset(20);
    font-weight: $font-weight-bold;
    padding: rem(4) rem(12);

    // text and icon
    &::before,
    &::after {
      font-size: $icon-size-xsmall;
    }

    // icon only
    &:empty {
      padding: rem(6);

      &::before,
      &::after {
        font-size: $icon-size-xsmall;
      }
    }
  } @else {
    @warn not a valid size option;
  }
}

/**
* Buttons
* Combines base, styles, and sizes
* @include btn();
* Option(s):
*   $style(value = primary | primary-reverse | primary-alternative | secondary | secondary-alternative | attention)
        default = primary
        The style of the button
*   $size(value = regular | small | xsmall)
        default = regular
        The size of the button
*   $icon(value = false | filename)
        default = false
        The name of the needed icon
*   $icon-position(value = before | after)
        default = after
        Where the icon should be placed
*/
@mixin btn(
  $style: primary,
  $size: regular,
  $icon: false,
  $icon-position: after,
  $style-only: false
) {
  @if $style-only == false {
    @include btn-base();
  }

  // button styles -----------------------------------------------------------------
  @if $style == primary {
    @include btn-style(primary);
  } @else if $style == primary-reverse {
    @include btn-style(primary-reverse);
  } @else if $style == primary-alternative {
    @include btn-style(primary-alternative);
  } @else if $style == secondary {
    @include btn-style(secondary);
  } @else if $style == secondary-alternative {
    @include btn-style(secondary-alternative);
  } @else if $style == attention {
    @include btn-style(attention);
  }

  // button sizes ------------------------------------------------------------------
  @if $size == regular {
    @include btn-size(regular);
  } @else if $size == small {
    @include btn-size(small);
  } @else if $size == xsmall {
    @include btn-size(xsmall);
  }

  // button icon -------------------------------------------------------------------
  @if $icon != false {
    @if $size == regular {
      @include icon(
        $filename: $icon,
        $size: $icon-size-small,
        $insert: $icon-position
      )
    } @else if $size == small or $size == xsmall {
      @include icon(
        $filename: $icon,
        $size: $icon-size-xsmall,
        $insert: $icon-position
      )
    }
  }
}
