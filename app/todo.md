# TODO
This file will lay out anything that we notice as an outstanding todo during the process of migrating the site from the 
old theme to the new urone theme. Sections should be added to the table of contents below, and should be alphabetical, 
grouped by component.

## Table of Contents
1. [Locations](#locations)
2. [User Profile](#userprofile)
3. [GTM Node Package](#gtmnode)

<a name="locations"></a>
### Locations
1. Move styles and js from docroot/modules/custom/locations to the theme.
2. Move templates outlined in docroot/modules/custom/locations to the theme.
3. Refactor js to be cleaner and implement functions instead of repeating code where possible.
4. Break styles into specific location-entity--VIEW-MODE--BUNDLE libraries (partially done).
5. Move preprocesses that are in the 

<a name="userprofile"></a>
### User Profile
1. Move away from using Vue.js template to render entirety of user profile page. Could be reduced to just the modals being in Vue.js (see urone/templates/user/user.html.twig).
2. Create custom hook_theme implementations for each of the individual sections (Personal Info, Account Info, Payment Info)
3. Rework how those lists get populated, i.e Don't make API calls from the front-end but rather make those API calls on the backend so we can render them immediately.
4. Upon creating/updating information ajax load the updated lists (accounts/cards) in.

<a name="gtmnode"></a>
### GTM Node Package
1. Remove VueGtm package as we're including this via the Drupal module. See main.js: 
`Vue.use(VueGtm, { id: 'GTM-xxxxxxx', enabled: true, debug: false, vueRouter: router });`
