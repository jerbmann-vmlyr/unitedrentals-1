/* eslint-disable quote-props */
module.exports = {
  presets: [
    '@vue/app',
  ],
  plugins: [
    ['transform-imports', {
      'lodash': {
        // eslint-disable-next-line no-template-curly-in-string
        'transform': 'lodash/${member}',
        'preventFullImport': false,
      },
    }],
  ],
};
